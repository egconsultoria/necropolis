�
 TFRMETIQUETAESPECIFICA 0
  TPF0TfrmEtiquetaEspecificafrmEtiquetaEspecificaLeftrTop.WidthWHeight� Caption   Etiquetas EspecíficasColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	Position	poDefaultVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidthAHeight� TabOrder  	TGroupBoxgrpEtiquetaEspecificaLeftTopWidth3Height� TabOrder  TLabel	lblCodigoLeftTopWidthRHeightCaption   Número Proposta  TLabellblProponente_NomeLefthTopWidthHeightCaptionNome  TLabellblProponente_EnderecoLefthTop8Width.HeightCaptionEndereco  TLabellblProponente_CEPLeft�Top8WidthHeightCaptionCEP  TLabellblProponente_EstadoLeft�Top8WidthHeightCaptionUF  TLabellblProponente_CidadeLefthTop_Width!HeightCaptionCidade  TLabellblProponente_BairroLeft>Top_WidthHeightCaptionBairro  	TPaintBoxPntComunicadorLeftTopWidth)Height� OnPaintPntComunicadorPaint  	TMaskEdit	mskCodigoLeftTop WidthJHeightEditMask9999999-9;0;_	MaxLength	TabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtProponente_NomeLefthTopWidth'Height	MaxLength0TabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtProponente_EnderecoLefthTopGWidth'Height	MaxLength0TabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskProponente_CEPLeft�TopGWidthPHeightEditMask99999\-999;1; 	MaxLength	TabOrderText	     -   OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtProponente_EstadoLeft�TopGWidth)Height	MaxLengthTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtProponente_CidadeLefthTopoWidth� Height	MaxLengthTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtProponente_BairroLeft>TopoWidth� Height	MaxLengthTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress     