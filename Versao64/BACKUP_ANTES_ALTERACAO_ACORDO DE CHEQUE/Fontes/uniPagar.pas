unit uniPagar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Mask, Variants,
  uniConst, OleCtrls, FPSpread_TLB;

const
  EDT_CODIGO: string = 'mskCodigo';

  COL_PARC = 1;
  COL_VALOR = 2;
  COL_DATA = 3;
  COL_HIST = 4;

  VLR_MULTA = 0.02;
  VLR_JUROS = 0.00034;

type
  TfrmPagar = class(TForm)
    pnlFundo: TPanel;
    grpPagar: TGroupBox;
    lblDataLancamento: TLabel;
    mskDataLancamento: TMaskEdit;
    grpPagamento: TGroupBox;
    lblDataPagamento: TLabel;
    lblDataCredito: TLabel;
    lblValorReceber: TLabel;
    lblValorDesconto: TLabel;
    lblValorPago: TLabel;
    lblSaldoDevedor: TLabel;
    grpCheques: TGroupBox;
    grpValor: TGroupBox;
    lblValorVencimento: TLabel;
    grpDocumento: TGroupBox;
    lblDuplicata: TLabel;
    lblHistorico: TLabel;
    lblNotaFiscal: TLabel;
    mskDuplicata: TMaskEdit;
    edtHistorico: TEdit;
    lblReferencia: TLabel;
    mskReferencia: TMaskEdit;
    cboDespesa: TComboBox;
    lblDespesa: TLabel;
    lblTipoPagamento: TLabel;
    cboFornecedor: TComboBox;
    lblFornecedor: TLabel;
    lblVencimento: TLabel;
    mskVencimento: TMaskEdit;
    mskValorVencimento: TMaskEdit;
    lblJuros: TLabel;
    lblMulta: TLabel;
    mskJuros: TMaskEdit;
    mskMulta: TMaskEdit;
    mskValorReceber: TMaskEdit;
    mskValorDesconto: TMaskEdit;
    mskValorPago: TMaskEdit;
    mskSaldoDevedor: TMaskEdit;
    mskDataPagamento: TMaskEdit;
    mskDataCredito: TMaskEdit;
    lblCodigo: TLabel;
    mskCodigo: TMaskEdit;
    lblDescricao: TLabel;
    edtDescricao: TEdit;
    PntComunicador: TPaintBox;
    lblCaixaBanco: TLabel;
    edtCaixaBanco: TEdit;
    lblPagto: TLabel;
    edtPagto: TEdit;
    lblMotivoCancelado: TLabel;
    edtMotivoCancelado: TEdit;
    lblCheque: TLabel;
    mskCheque: TMaskEdit;
    lblChequeCancelado: TLabel;
    lblDataCancelado: TLabel;
    mskDataCancelado: TMaskEdit;
    cboTipoPagamento: TComboBox;
    mskNotaFiscal: TMaskEdit;
    cboChequeCancelado: TComboBox;
    lblBanco: TLabel;
    cboBanco: TComboBox;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure cboComboExit(Sender: TObject);
    procedure cboComboKeyPress(Sender: TObject; var Key: Char);
    procedure edtTextoChange(Sender: TObject);
    procedure edtTextoEnter(Sender: TObject);
    procedure edtTextoExit(Sender: TObject);
    procedure sprGridChange(Sender: TObject; Col, Row: Integer);
    procedure sprGridEditChange(Sender: TObject; Col, Row: Integer);
    procedure chkCheckClick(Sender: TObject);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;

    procedure mpSaldo_Devedor;
    procedure mpValor_Receber;
//    procedure mpMostra_Bloqueio;
//    procedure mpMostra_Baixa;
  public
    { Public declarations }
(*
    function gpValor_Atual(psNumeroProposta: string;
                           psTipoDoc: string;
                           psValorOriginal: string;
                           psMulta: string;
                           psJuros: string;
                           psVencimento: string;
                           psNumeroParcela: string): string;
*)
  end;

var
  frmPagar: TfrmPagar;

implementation

uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos, uniFuncoes;
{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.FormClose(Sender: TObject; var Action: TCloseAction);
var
  liConta:  Integer;
begin
  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - uniPagar.dfm',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;
    gfrmTV := nil;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniPagar.PAS');
  end;

  // destroi a janela
  Action := caFree;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, gfrmTV);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormDeactivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.FormResize(Sender: TObject);
begin
  if WindowState <> wsMinimized Then
  begin
    if (Width < pnlFundo.Width + 39) and (WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      Width := pnlFundo.Width + 39;

    Refresh;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

//    { redimensiona a tela}
//    Width  := 527;
//    Height := 276;}

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
//    mrTelaVar.P.Titulo_Tela := 'titulo da aplicacao';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
    untEdit.gp_Edit_EstufaCombos(mrTelaVar.P.Sigla_Tabela, TForm(Sender));

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);

    mpSaldo_Devedor;
    mpValor_Receber;
//    mpMostra_Bloqueio;
//    mpMostra_Parcial;

    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniPagar.pas');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 0);


end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  {' muda o ponteiro do mouse para ampulheta}
  Screen.cursor := crHOURGLASS;

  lcboCombo := TComboBox(Sender);

  {' se nao for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
  begin
    if lcboCombo.Text <> VAZIO then
    begin
      {' altera��o efetuada}
      untEdit.gp_Edit_Alterado(True, mrTelaVar);
    end;
  end
  else
    If (Not mrTelaVar.Reposicionando) Then
      {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

  {' retorna o ponteiro do mouse para o default}
  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TformAssinante.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  liConta: Integer;
begin

  lcboCombo := TComboBox(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := lcboCombo.Text;
  giAreaDesfaz := -1;
  if lcboCombo.Items.Count > 0 then
    {'Executa um la�o nos itens comparando-os com o pConteudo}
    for liConta := 0 to lcboCombo.Items.Count - 1 do
      if lcboCombo.Items[liConta] = gsAreaDesfaz then
      begin
        giAreaDesfaz := liConta;
        break;
      end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.cboComboKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.edtTextoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.edtTextoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = EDT_CODIGO) And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    {' atualiza o caption da janela}
    Caption := mrTelaVar.P.Titulo_Tela + ' ' + ledtEdit.Text + ' - Inclus�o';
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.edtTextoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.edtTextoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprGridChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.sprGridChange(Sender: TObject; Col,
  Row: Integer);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.sprGridEditChange       Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.sprGridEditChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
  lgrdGrid.Col := Col;
  if lgrdGrid.CellType = SS_CELL_TYPE_COMBOBOX then
    untCombos.gp_Combo_SpreadSeleciona(lgrdGrid, 0);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.PntComunicadorPaint(Sender: TObject);
var
  liComando_MDI: Integer;
begin

  liComando_MDI := giComando_MDI;

  if (liComando_MDI = TV_COMANDO_GRAVAR) then
  begin
//    mpMostra_Baixa;
  end;

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, gfrmTV);

  if (liComando_MDI = TV_COMANDO_IR_PRIMEIRO) or
     (liComando_MDI = TV_COMANDO_IR_PROXIMO) or
     (liComando_MDI = TV_COMANDO_IR_ANTERIOR) or
     (liComando_MDI = TV_COMANDO_PROCURAR) or
     (liComando_MDI = TV_COMANDO_IR_ULTIMO) then
  begin
    mpSaldo_Devedor;
    mpValor_Receber;
//    mpMostra_Bloqueio;
//    mpMostra_Parcial;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.edtTextoExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.edtTextoExit(Sender: TObject);
var
  lmskEdit: TMaskEdit;
  lsCodigo: string;
  liConexao:  Integer;
  lsSql:      string;
  liIndCol:   array[0..1] of Integer;
  i, j:  Integer;
begin

  lmskEdit := TMaskEdit(Sender);

  if Sender is TMaskEdit then
  begin
    { procura pela entidade }
    i := untTelaVar.gf_TelaVar_Retorna_Tabela(mrTelaVar.P.Sigla_Tabela);
    { se nao achou }
    if i = 0 then
      Exit;

    j := 1;
    { percorre todos os campos da tabela principal }
    while gaLista_Campos[i][j].sSigla_Tabela = gaLista_Parametros[i].Sigla_Tabela do
    begin
      if gaLista_Campos[i][j].sNome_Controle = lmskEdit.Name then
      begin
        if gaLista_Campos[i][j].sAlinha = ALINHA_ZERO then
          lmskEdit.Text := untFuncoes.gf_Zero_Esquerda(lmskEdit.Text, gaLista_Campos[i][j].iTamanho);
        break;
      end;
      j := j + 1;
    end;
  end;

(*  if lmskEdit.Name = 'mskNumeroProposta' then
  begin
    { ler os registros da tabela proponentes }
    gaParm[0] := 'PROPONEN';
    gaParm[1] := 'NUM_PROP = ''' + lmskEdit.Text + '''';
    gaParm[2] := 'NUM_PROP';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;
    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    dmDados.gsRetorno := dmDados.Primeiro(liConexao);
    if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
    begin
      { pega o conteudo do campo }
      edtDescricao.Text := dmDados.ValorColuna(liConexao, 'NOME_PROP', liIndCol[1]);
    end;
    dmDados.FecharConexao(liConexao);
  end;

  if mrTelaVar.Comando_MDI = ED_NOVO then
  begin
    if lmskEdit.Name = 'mskNumeroParcela' then
    begin
      lsCodigo := mskNumeroProposta.Text;
      lsCodigo := lsCodigo + untCombos.gf_Combo_SemDescricao(cboTipoDocumento);
      if untCombos.gf_Combo_SemDescricao(cboTipoDocumento) = 'T' then
        if (Copy(mskNumeroParcela.Text,4,2) >= '00') and (Copy(mskNumeroParcela.Text,4,2) <= '30') then
          lsCodigo := lsCodigo + '20'
        else
          lsCodigo := lsCodigo + '19'
      else
        lsCodigo := lsCodigo + '00';

      lsCodigo := lsCodigo + Copy(mskNumeroParcela.Text,4,2) + Copy(mskNumeroParcela.Text,1,2);
      mskCodigo.Text := lsCodigo;
      mskDocumento.Text := lsCOdigo;
    end;
  end;
(*
  if lmskEdit.Name = 'mskValorOriginal' then
  begin
    if Trim(mskValorOriginal.Text) <> VAZIO then
    begin
      mskJuros.Text := FormatFloat(MK_VALOR, StrToFloat(dmDados.TirarSeparador(mskValorOriginal.Text)) * VLR_JUROS);
      mskMulta.Text := FormatFloat(MK_VALOR, StrToFloat(dmDados.TirarSeparador(mskValorOriginal.Text)) * VLR_MULTA);
    end;
  end;

  if lmskEdit.Name = 'mskRecebParcial' then
    mpMostra_Parcial;
*)
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpSaldo_Devedor       Mostra o saldo devedor
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.mpSaldo_Devedor();
var
  lnParcPagto: Double;
  liConta: Integer;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  { se ainda nao foi paga }
  if mskDataPagamento.Text = VAZIO_DATA then
    mskSaldoDevedor.Text := FormatFloat(MK_VALOR, StrToFloat(dmDados.TirarSeparador(mskValorVencimento.Text)))
  else
    mskSaldoDevedor.Text := VAZIO;

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpValor_Receber       Mostra o valor a receber
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.mpValor_Receber();
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  { se ainda nao foi pago }
  if mskDataPagamento.Text = VAZIO_DATA then
  begin
    { se nao tem vencimento nao calcula }
    if mskVencimento.Text = VAZIO_DATA then
      mskValorReceber.Text := mskValorVencimento.Text
    else
    begin
      { tolerancia de cinco dias }
//      if StrToDate(mskVencimento.Text) >= (Date - DIAS_ATRASO) then
//      begin
        mskValorReceber.Text := mskValorVencimento.Text;
//      end
//      else
//      begin
//        mskValorReceber.Text := untFuncoes.gf_Funcoes_ValorAtual(mskNumeroProposta.Text,
//                                              untCombos.gf_Combo_SemDescricao(cboTipoDocumento),
//                                              mskValorOriginal.Text,
//                                              mskMulta.Text,
//                                              mskJuros.Text,
//                                              mskVencimento.Text,
//                                              mskNumeroParcela.Text);
//      end;
    end;
  end
  else
    mskValorReceber.Text := VAZIO;

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.gpValor_Atual       Calcula o valor atualizado
 *
 *-----------------------------------------------------------------------------}
(*
function TfrmRecebSeguro.gpValor_Atual(psNumeroProposta: string;
                                          psTipoDoc: string;
                                          psValorOriginal: string;
                                          psMulta: string;
                                          psJuros: string;
                                          psVencimento: string;
                                          psNumeroParcela: string): string;
var
  liConta: Integer;
  lsSql: string;
  liConexao: Integer;
  lsTabela: string;
  lsTipo: string;
  liIndCol:   array[0..7] of Integer;
  aSqlParms:  array[0..2] of Variant;
  lnRefStand: Double;
  lnVlrAtual: Double;
  lnPercTaxa: Double;
  lsCodigo: string;
  lbTaxaFixa: Boolean;
  lsTaxaRec: string;
  lsNumTaxa1: string;
  lsNumTaxa2: string;
  lsTaxaAtr1: string;
  lsTaxaAtr2: string;
  lnVlrTaxaFixa1: Double;
  lnVlrTaxaFixa2: Double;
  lsNumTaxa3: string;
  lsNumTaxa4: string;
  lnVlrTaxaVar1: Double;
  lnVlrTaxaVar2: Double;
begin

  { se nao for Taxa }
  if (psTipoDoc = 'L') or (psTipoDoc = 'G') or (psTipoDoc = 'N') or (psTipoDoc = 'R') then
  begin
    if psTipoDoc = 'L' then
    begin
      lsTabela := 'PropLote';
      lsTipo := 'Tipo_Lote';
    end;
    if psTipoDoc = 'G' then
    begin
      lsTabela := 'PropGave';
      lsTipo := 'Tipo_Gave';
    end;
    if psTipoDoc = 'N' then
    begin
      lsTabela := 'PropNich';
      lsTipo := '';
    end;
    if psTipoDoc = 'R' then
    begin
      lsTabela := 'PropRemi';
      lsTipo := 'Tipo_Lote';
    end;

    aSqlParms[0] := lsTabela + '.Cod_Plano, ';
    if lsTipo <> VAZIO then
      aSqlParms[0] := aSqlParms[0] + lsTabela + '.' + lsTipo + ', ' ;
    aSqlParms[0] := aSqlParms[0] + lsTabela + '.Data_Plano, PlanoVlr.Tipo_Plano';
    aSqlParms[1] := lsTabela + ', PlanoVlr';
    aSqlParms[2] := lsTabela + '.Cod_Plano = PlanoVlr.Cod_Plano And ';
    aSqlParms[2] := aSqlParms[2] + lsTabela + '.Data_Plano = PlanoVlr.Data_Incl And ';
    aSqlParms[2] := aSqlParms[2] + '(PlanoVlr.Tipo_Plano = ''U'' Or PlanoVlr.Tipo_Plano = ''R'') And ';
    aSqlParms[2] := aSqlParms[2] + 'Num_Prop = ''' + psNumeroProposta + '''';
    lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;
    { se encontrou a op��o }
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
    begin
      if Date > StrToDate(psVencimento) then
        Result := FormatFloat(MK_VALOR,
                   StrToFloat(dmDados.TirarSeparador(psValorOriginal)) +
                   StrToFloat(dmDados.TirarSeparador(psJuros)) * (Date - StrToDate(psVencimento)) +
                   StrToFloat(dmDados.TirarSeparador(psMulta)))
      else
        Result := psValorOriginal;
    end
    else
    begin
      Result := psValorOriginal;
    end;
    dmDados.FecharConexao(liConexao);
  end
  else
  begin
    for liConta := 0 to High(liIndCol) do
      liIndCol[liConta] := IOPTR_NOPOINTER;
    aSqlParms[0] := 'Ref_Stand, Vlr_Taxa, Perc_Taxa, Tx_Fixa';
    aSqlParms[1] := 'PropLote, Tab_Lote';
    aSqlParms[2] := 'Proplote.Tipo_Lote = Tab_Lote.Codigo And ';
    aSqlParms[2] := aSqlParms[2] + 'Num_Prop = ''' + psNumeroProposta + '''';
    lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;
    { se encontrou a op��o }
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
    begin
      lnRefStand := dmDados.ValorColuna(liConexao, 'Ref_Stand', liIndCol[0]);
      lnVlrAtual := dmDados.ValorColuna(liConexao, 'Vlr_Taxa', liIndCol[1]);
      lnPercTaxa := dmDados.ValorColuna(liConexao, 'Perc_Taxa', liIndCol[2]);
      lbTaxaFixa := dmDados.ValorColuna(liConexao, 'Tx_Fixa', liIndCol[3]);
    end
    else
    begin
      lnRefStand := 0;
      lnVlrAtual := 0;
      lnPercTaxa := 0;
      lbTaxaFixa := False;
    end;
    dmDados.FecharConexao(liConexao);

    if (Copy(psNumeroParcela,4,2) <= '29') and (Copy(psNumeroParcela,4,2) >= '10') then
      lsTaxaRec := '00'
    else
      if (Copy(psNumeroParcela,4,2) >= '00') and (Copy(psNumeroParcela,4,2) <= '30') then
        lsTaxaRec := '20'
      else
        lsTaxaRec := '19';
      lsTaxaRec := lsTaxaRec + Copy(psNumeroParcela,4,2) + Copy(psNumeroParcela,1,2);

    for liConta := 0 to High(liIndCol) do
      liIndCol[liConta] := IOPTR_NOPOINTER;
    lsSql := 'SELECT Max(Codigo) As Codigo From Tab_Taxa';
    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;
    { se n�o encontrou a op��o }
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
    begin
      lsCodigo := dmDados.ValorColuna(liConexao, 'Codigo', liIndCol[0]);
    end
    else
    begin
      lsCodigo := VAZIO;
    end;
    dmDados.FecharConexao(liConexao);

    for liConta := 0 to High(liIndCol) do
      liIndCol[liConta] := IOPTR_NOPOINTER;
    aSqlParms[0] := 'Num_Taxa1, Num_Taxa2, Num_Taxa3, Num_Taxa4, Vlr_TaxVar, Vlr_TaxVr2, Vlr_TaxFx1, Vlr_TaxFx2';
    aSqlParms[1] := 'Tab_Taxa';
    aSqlParms[2] := 'Codigo = ''' + lsCodigo + '''';
    lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;
    { se n�o encontrou a op��o }
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
    begin
      lsNumTaxa1 := dmDados.ValorColuna(liConexao, 'Num_Taxa1', liIndCol[0]);
      lsNumTaxa2 := dmDados.ValorColuna(liConexao, 'Num_Taxa2', liIndCol[1]);
      lsNumTaxa3 := dmDados.ValorColuna(liConexao, 'Num_Taxa3', liIndCol[2]);
      lsNumTaxa4 := dmDados.ValorColuna(liConexao, 'Num_Taxa4', liIndCol[3]);
      lnVlrTaxaVar1 := dmDados.ValorColuna(liConexao, 'Vlr_TaxVar', liIndCol[4]);
      lnVlrTaxaVar2 := dmDados.ValorColuna(liConexao, 'Vlr_TaxVr2', liIndCol[5]);
      lnVlrTaxaFixa1 := dmDados.ValorColuna(liConexao, 'Vlr_TaxFx1', liIndCol[6]);
      lnVlrTaxaFixa2 := dmDados.ValorColuna(liConexao, 'Vlr_TaxFx2', liIndCol[7]);

      if lnVlrAtual = 0 then
      begin
        if lbTaxaFixa then
        begin
          if (Copy(lsNumTaxa1,4,2) >= '00') and (Copy(lsNumTaxa1,4,2) <= '30') then
            lsTaxaAtr1 := '20'
          else
            lsTaxaAtr1 := '19';
          lsTaxaAtr1 := lsTaxaAtr1 + Copy(lsNumTaxa1,4,2) + Copy(lsNumTaxa1,1,2);
          if (Copy(lsNumTaxa2,4,2) >= '00') and (Copy(lsNumTaxa2,4,2) <= '30') then
            lsTaxaAtr2 := '20'
          else
            lsTaxaAtr2 := '19';
          lsTaxaAtr2 := lsTaxaAtr2 + Copy(lsNumTaxa2,4,2) + Copy(lsNumTaxa2,1,2);

          if lsTaxaRec <= lsTaxaAtr1 then
          begin
            if (Copy(psNumeroParcela,4,2) <= '21') and (Copy(psNumeroParcela,4,2) >= '10') then
              lnVlrAtual := lnVlrTaxaFixa1 * lnRefStand / 3
            else
              lnVlrAtual := lnVlrTaxaFixa1 * lnRefStand;
          end
          else
            if (lsTaxaRec <= lsTaxaAtr2) and (lsTaxaRec > lsTaxaAtr1) then
              lnVlrAtual := lnVlrTaxaFixa2 * lnRefStand;
        end
        else
        begin
          if (Copy(lsNumTaxa3,4,2) >= '00') and (Copy(lsNumTaxa3,4,2) <= '30') then
            lsTaxaAtr1 := '20'
          else
            lsTaxaAtr1 := '19';
          lsTaxaAtr1 := lsTaxaAtr1 + Copy(lsNumTaxa3,4,2) + Copy(lsNumTaxa3,1,2);
          if (Copy(lsNumTaxa4,4,2) >= '00') and (Copy(lsNumTaxa4,4,2) <= '30') then
            lsTaxaAtr2 := '20'
          else
            lsTaxaAtr2 := '19';
          lsTaxaAtr2 := lsTaxaAtr2 + Copy(lsNumTaxa4,4,2) + Copy(lsNumTaxa4,1,2);

          if lsTaxaRec <= lsTaxaAtr1 then
            lnVlrAtual := lnVlrTaxaVar1 * lnRefStand
          else
            if (lsTaxaRec <= lsTaxaAtr2) and (lsTaxaRec > lsTaxaAtr1) then
              lnVlrAtual := lnVlrTaxaVar2 * lnRefStand
            else
              lnVlrAtual := lnVlrTaxaVar2 * lnRefStand;
        end;
        if lnPercTaxa <> 0 then
          lnVlrAtual := lnVlrAtual - (lnVlrAtual * (lnPercTaxa/100));
      end;
    end;
    dmDados.FecharConexao(liConexao);

    Result := FormatFloat(MK_VALOR, lnVlrAtual);
  end;

end;
*)

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpMostra_Bloqueio       Mostra o valor a receber
 *
 *-----------------------------------------------------------------------------}
(*
 procedure TfrmRecebSeguro.mpMostra_Bloqueio();
var
  liConta: Integer;
  lsSql: string;
  liConexao: Integer;
  lsTabela: string;
  liIndCol:   array[0..1] of Integer;
  aSqlParms:  array[0..2] of Variant;
  lvDataBloq: Variant;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  for liConta := 0 to High(liIndCol) do
    liIndCol[liConta] := IOPTR_NOPOINTER;

  { todos os outros }
  lsTabela := 'PropLote';
  if Copy(cboTipoDocumento.Text,1,1) = 'G' then
    lsTabela := 'PropGave';
  if Copy(cboTipoDocumento.Text,1,1) = 'N' then
    lsTabela := 'PropNich';
  if Copy(cboTipoDocumento.Text,1,1) = 'R' then
    lsTabela := 'PropRemi';

  aSqlParms[0] := lsTabela + '.Bloqueada, ' + lsTabela + '.Data_Bloq';
  aSqlParms[1] := lsTabela;
  aSqlParms[2] := 'Num_Prop = ''' + mskNumeroProposta.Text + '''';
  lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);

  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;
  { se encontrou a op��o }
  if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
  begin
    cboBloqueada.Text := dmDados.ValorColuna(liConexao, 'Bloqueada', liIndCol[0]);
    lvDataBloq := dmDados.ValorColuna(liConexao, 'Data_Bloq', liIndCol[1]);
    if lvDataBloq <> 0 then
      mskDataBloqueada.Text := FormatDateTime(MK_DATATELA, lvDataBloq)
    else
      mskDataBloqueada.Text := VAZIO;
  end
  else
  begin
    cboBloqueada.Text := VAZIO;
    mskDataBloqueada.Text := VAZIO;
  end;
  dmDados.FecharConexao(liConexao);

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;

end;
*)

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.cboComboExit(Sender: TObject);
var
  liConexao: Integer;
  lsSql: string;
  liIndCol:   array[0..1] of Integer;
  
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  if mrTelaVar.Comando_MDI = ED_NOVO then
  begin
    if lcboCombo.Name = 'cboFornecedor' then
    begin
      { ler os registros da tabela pagar }
      gaParm[0] := 'PAGAR';
      gaParm[1] := 'Substring(REFERENCIA,6,4) + Substring(REFERENCIA,4,2) = ''' + Copy(mskDataLancamento.Text,7,4) + Copy(mskDataLancamento.Text,4,2) + '''';
      gaParm[2] := 'Substring(REFERENCIA,6,4), Substring(REFERENCIA,4,2), Substring(REFERENCIA,1,3)';
      lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;
      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      dmDados.gsRetorno := dmDados.Primeiro(liConexao);
      if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
      begin
        { pega o conteudo do campo }
        mskReferencia.Text := FormatFloat('000', StrToInt(Copy(dmDados.ValorColuna(liConexao, 'REFERENCIA', liIndCol[1]),1,3))) +
                              Copy(mskDataLancamento.Text,4,2) + Copy(mskDataLancamento.Text,7,4);
      end
      else
      begin
        mskReferencia.Text := '001' +
                              Copy(mskDataLancamento.Text,4,2) + Copy(mskDataLancamento.Text,7,4);
      end;
      dmDados.FecharConexao(liConexao);
      edtDescricao.Text := untCombos.gf_Combo_SemCodigo(cboFornecedor) + ' - ' + mskDataLancamento.Text;
    end;
  end;
  if lcboCombo.Name = 'cboTipoPagamento' then
  begin
    if untCombos.gf_Combo_SemDescricao(cboTipoPagamento) = 'C' then
      grpCheques.Enabled := True
    else
      grpCheques.Enabled := False;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.chkCheckClick       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPagar.chkCheckClick(Sender: TObject);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;






end.
