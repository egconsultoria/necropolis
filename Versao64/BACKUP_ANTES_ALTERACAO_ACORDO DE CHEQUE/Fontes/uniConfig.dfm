�
 TFRMCONFIG 0	  TPF0
TfrmConfig	frmConfigLeftTop� Width�Height� Caption	frmConfigColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	Position	poDefaultVisible	
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidth�HeightaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TLabel	lblCodigoLeftTopWidth!HeightCaption   CódigoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel	lblConfigLeftTop0Width?HeightCaption   ConfiguraçãoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblDescricaoLeft� TopWidth0HeightCaption   DescriçãoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel
lblTamanhoLeftTop`Width-HeightCaptionTamanhoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabellblTipoControleLeftPTop`Width*HeightCaption
Tipo Ctrl.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabellblComboTabelaLeft� Top`WidthEHeightCaptionCombo TabelaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabellblComboFiltroLeftTop`Width:HeightCaptionCombo FiltroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  	TPaintBoxPntComunicadorLeftTopWidthyHeightUOnPaintPntComunicadorPaint  TEdit	edtConfigLeftTop@Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeedtCodigoChangeOnEnteredtCodigoEnter
OnKeyPressedtCodigoKeyPress  	TMaskEdit	mskCodigoLeftTopWidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnChangeedtCodigoChangeOnEnteredtCodigoEnterOnExitmskCodigoExit
OnKeyPressedtCodigoKeyPress  	TComboBox	cboConfigLeftTop@Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnter
OnKeyPressedtCodigoKeyPress  	TMaskEdit	mskConfigLeftTop@WidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeedtCodigoChangeOnEnteredtCodigoEnterOnExitmskCodigoExit
OnKeyPressedtCodigoKeyPress  TEditedtDescricaoLeft� TopWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnChangeedtCodigoChangeOnEnteredtCodigoEnter
OnKeyPressedtCodigoKeyPress  	TvaSpread	sprConfigLeftTop� WidthyHeight� ParentShowHintShowHint	TabOrderVisibleControlData
�     �&  �  @                         R������ � K�Q   �DB MS Sans Serif��� ���                          �  �  ��  �  e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif               ����MbP?E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d                B          �����������         �������� %      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����                @0        �����                $@*       ����� <                  @4@*       ����� <                   $@*       ����� <      0    ����    �   8           Chave 8           Descri��o 8           Valor *          � <              ����                         �              TEdit
edtTamanhoLeftToppWidth9HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisibleOnChangeedtCodigoChangeOnEnteredtCodigoEnter
OnKeyPressedtCodigoKeyPress  TEditedtComboTabelaLeft� ToppWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisibleOnChangeedtCodigoChangeOnEnteredtCodigoEnter
OnKeyPressedtCodigoKeyPress  TEditedtComboFiltroLeftToppWidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	VisibleOnChangeedtCodigoChangeOnEnteredtCodigoEnter
OnKeyPressedtCodigoKeyPress  TEditedtTipoControleLeftPToppWidth9HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisibleOnChangeedtCodigoChangeOnEnteredtCodigoEnter
OnKeyPressedtCodigoKeyPress    