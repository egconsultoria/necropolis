unit uniContratos;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, jpeg,
  ExtCtrls, StdCtrls, ComCtrls, ToolWin;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const
  ZOOM_MAIS = 100;
  ZOOM_MENOS = 100;

  LARGURA_FIG = 581;
  ALTURA_FIG = 437;

  DEF_SCROLL_UNITS = 8;
  
type
 {*======================================================================
 *            RECORDS
 *======================================================================}

 {*======================================================================
 *            CLASSES
 *======================================================================}
  TfrmImagens = class(TForm)
    tobImagens: TToolBar;
    btnZoomMais: TToolButton;
    btnZoomMenos: TToolButton;
    imgLista: TImageList;
    btnSeparador1: TToolButton;
    btnImagemAnterior: TToolButton;
    btnImagemProxima: TToolButton;
    btnSeparador2: TToolButton;
    btnImagemImprimir: TToolButton;
    pnlFundo: TPanel;
    pnlImagem: TPanel;
    pnlAmbiente: TPanel;
    pnlProposta: TPanel;
    lblNumeroProposta: TLabel;
    lblNumProp: TLabel;
    lblNomeProp: TLabel;
    lblNomeProposta: TLabel;
    imgFigura: TImage;
    slbHorizontal: TScrollBar;
    slbVertical: TScrollBar;
    Bevel1: TBevel;
    lblLeft: TLabel;
    lblTop: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnZoomMaisClick(Sender: TObject);
    procedure btnZoomMenosClick(Sender: TObject);
    procedure btnImagemAnteriorClick(Sender: TObject);
    procedure btnImagemProximaClick(Sender: TObject);
    procedure slbGenericoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure imgFiguraMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imgFiguraMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
    maFiguras: array[1..20] of string;
    miTotFiguras: Integer;
    miFiguras: Integer;

    mpUnits: TPoint;

    miPosY: Integer;
    miPosX: Integer;

    procedure mpAtualizaBotoes;
    procedure mpAtualizaBarras;
    procedure mpMostraFigura;

    procedure mpAjustaBarras;
  public
    { Public declarations }
  end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  frmImagens: TfrmImagens;

implementation

uses uniDados, uniConst, uniGlobal;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TfrmImagens.FormShow       Redimensiona a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.FormShow(Sender: TObject);
var
  liConta: Integer;
  liConexao: Integer;
  liIndCol: array[0..2] of Integer;
  lsSql: string;
begin

  try
    { mostra o numero da proposta }
    lblNumProp.Caption := Copy(gsNumProposta,1,7) + '-' + Copy(gsNumProposta,8,1);

    for liConta := 0 to High(liIndCol) do
      liIndCol[liConta] := IOPTR_NOPOINTER;

    { ler os registros do nome do proponente }
    gaParm[0] := 'PROPONEN';
    gaParm[1] := 'NUM_PROP = ''' + gsNumProposta + '''';
    gaParm[2] := 'NUM_PROP';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;
    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    dmDados.gsRetorno := dmDados.Primeiro(liConexao);
    if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
    begin
      { pega o conteudo do campo }
      lblNomeProp.Caption := dmDados.ValorColuna(liConexao, 'NOME_PROP', liIndCol[0]);
    end
    else
    begin
      { limpa os campo }
      lblNomeProp.Caption := VAZIO;
    end;
    dmDados.FecharConexao(liConexao);

    { ler os registros dos ocupantes das gavetas }
    gaParm[0] := 'CONTRATOS';
    gaParm[1] := 'NUM_PROP = ''' + gsNumProposta + '''';
    gaParm[2] := 'TIPO, SEQUENCIA';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;
    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    { limpa o vetor de figuras }
    for liConta := 1 to High(maFiguras) do
    begin
      maFiguras[liConta] := VAZIO;
    end;
    miTotFiguras := 0;
    liConta := 1;
    dmDados.gsRetorno := dmDados.Primeiro(liConexao);
    while (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) do
    begin
      { pega o conteudo do campo }
      maFiguras[liConta] := dmDados.ValorColuna(liConexao, 'Caminho', liIndCol[1]);
      liConta := liConta + 1;
      dmDados.gsRetorno := dmDados.Proximo(liConexao);
    end;
    dmDados.FecharConexao(liConexao);
    miTotFiguras := liConta - 1;
    miFiguras := 1;

    mpAtualizaBotoes;
    mpMostraFigura;

  except
    dmDados.ErroTratar('Form_Load - uniContratos');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmImagens.btnZoomMaisClick       Aumenta a imagem
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.btnZoomMaisClick(Sender: TObject);
begin

  Screen.Cursor := crHourGlass;

  imgFigura.Width := imgFigura.Width + ZOOM_MAIS;
  imgFigura.Height := imgFigura.Height + ZOOM_MAIS;

  imgFigura.Invalidate;

  mpAjustaBarras;

  Screen.Cursor := crDefault;
  
end;

{*-----------------------------------------------------------------------------
 *  TfrmImagens.btnZoomMenosClick       Diminui a imagem
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.btnZoomMenosClick(Sender: TObject);
begin

  Screen.Cursor := crHourGlass;

  { para nao sumir a imagem }
  if (imgFigura.Width - ZOOM_MENOS > 0) and (imgFigura.Height - ZOOM_MENOS > 0) then
  begin
    imgFigura.Width := imgFigura.Width - ZOOM_MENOS;
    imgFigura.Height := imgFigura.Height - ZOOM_MENOS;

    imgFigura.Invalidate;

    mpAjustaBarras;
  end;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TfrmImagens.mpAtualizaBotoes       Habilita os botoes
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.mpAtualizaBotoes;
begin

  { se nao tem figuras }
  if miTotFiguras = 0 then
  begin
    btnZoomMais.Enabled := False;
    btnZoomMenos.Enabled := False;
    btnImagemAnterior.Enabled := False;
    btnImagemProxima.Enabled := False;
    btnImagemImprimir.Enabled := False;
  end
  else
  begin
    btnZoomMais.Enabled := True;
    btnZoomMenos.Enabled := True;
    btnImagemImprimir.Enabled := True;
    { se passou da primeira }
    if miFiguras > 1 then
      btnImagemAnterior.Enabled := True
    else
      btnImagemAnterior.Enabled := False;
    { se chegou na ultima }
    if miFiguras = miTotFiguras then
      btnImagemProxima.Enabled := False
    else
      btnImagemProxima.Enabled := True;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmImagens.mpMostraFigura       Mostra a imagem
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.mpMostraFigura;
var
  jpgFigura: TJPEGImage;
  picFigura: TPicture;
begin

  { se tem figuras }
  if miTotFiguras > 0 then
  begin
    { redimensiona a imagem para os valores originais }
    imgFigura.Width := LARGURA_FIG;
    imgFigura.Height := ALTURA_FIG;
    try
      jpgFigura := TJPEGImage.Create;
      picFigura := TPicture.Create;

      jpgFigura.Scale := jsHalf;
      jpgFigura.LoadFromFile(maFiguras[miFiguras]);
      picFigura.Assign(jpgFigura);

      imgFigura.Height := jpgFigura.Height;
      imgFigura.Width := jpgFigura.Width;
      imgFigura.Picture := picFigura;

      imgFigura.Refresh;

      mpAtualizaBarras;
    finally
      jpgFigura.Free;
      picFigura.Free;
    end;
  end
  else
    imgFigura.Picture := nil;

end;

{*-----------------------------------------------------------------------------
 *  TfrmImagens.btnImagemAnteriorClick       Mostra a imagem anterior
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.btnImagemAnteriorClick(Sender: TObject);
begin

  Screen.Cursor := crHourGlass;

  { se nao estiver na primeira }
  if miFiguras > 1 then
  begin
    miFiguras := miFiguras - 1;
    mpAtualizaBotoes;
    mpMostraFigura;
  end;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TfrmImagens.btnImagemProximaClick       Mostra a proxima imagem
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.btnImagemProximaClick(Sender: TObject);
begin

  Screen.Cursor := crHourGlass;

  { se tiver mais de uma }
  if miFiguras < miTotFiguras then
  begin
    miFiguras := miFiguras + 1;
    mpAtualizaBotoes;
    mpMostraFigura;
  end;

  Screen.Cursor := crDefault;

end;

//===========================================================================
// O alcance das barras de rolagem devem mudar quando o tamanho da janela
// visivel mudar.
//===========================================================================
function Escala(n, u: Integer): Integer;
begin
  Result := (n + u - 1) div u;
end;

function ValorMaximo(a, b: Integer): Integer;
begin
  if a >= b then
    Result := a
  else
    Result := b;
end;

//
// Reajusta o alcance de rolagem para coincidir com o novo tamanho
// da janela visivel.  (Tambem e chamada pela mpAtualizaBarras para evitar
// duplicidade de codigo).
//
{*-----------------------------------------------------------------------------
 *  TfrmImagens.mpAjustaBarras       Redimensiona as barras para o tamanho da figura
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.mpAjustaBarras();
begin
  // ajusta o maximo para rolar a figura toda,
  // mas nao de mais para nao ultrapassar a janela.
  // dividimos pelo numero de unidades (>1) porque rolar um pixel
  // por vez e muito lento para a maioria das figuras.

  if ImgFigura.Picture <> nil then
  begin
    slbHorizontal.Max := ValorMaximo(0, Escala(imgFigura.Width - pnlAmbiente.Width, mpUnits.x));
    slbVertical.Max := ValorMaximo(0, Escala(imgFigura.Height - pnlAmbiente.Height, mpUnits.y));

    slbHorizontal.LargeChange := Escala(slbHorizontal.Max, mpUnits.x);
    slbVertical.LargeChange := Escala(slbVertical.Max, mpUnits.y);
  end;
end;

//
// mpAtualizaBarras() e usada para colocar as barras de rolagem em um estado
// inicial.  isto precisa ser feito toda vez que a imagem mudar,
// pois o tamanho da rolagem depende do tamanho da imagem.  para
// simplificar, o caso especial em que o "tamanho da nova imagem" == "tamanho da imagem antiga"
// e ignorado.
//
{*-----------------------------------------------------------------------------
 *  TfrmImagens.mpAtualizaBarras       Sincroniza as barras de rolagem
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.mpAtualizaBarras;
begin
  // estes tamanhos fixos funcionam bem, entretanto em alguns casos basear
  // as unidades de rolagem no tamanho da imagem tambem funciona bem.
  mpUnits.x := DEF_SCROLL_UNITS;
  mpUnits.y := DEF_SCROLL_UNITS;

  if imgFigura.Picture <> nil then
  begin
    // move a imagem e as barras para a posicao inicial
    imgFigura.Top := 0;
    imgFigura.Top := 0;
    slbHorizontal.Position := 0;
    slbVertical.Position := 0;

    // nao vale a pena tratar valores de rolagem negativos, entao
    // atribuimos valores de [0, M].  Veja 'mpAjustaBarras' para ver como M
    // e calculado.
    slbHorizontal.Min := 0;
    slbVertical.Min := 0;
    mpAjustaBarras;

    slbHorizontal.LargeChange := Escala(slbHorizontal.Max, mpUnits.x);
    slbVertical.LargeChange := Escala(slbVertical.Max, mpUnits.y);

    slbHorizontal.Visible := True;
    slbVertical.Visible := True;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmImagens.slbGenericoChange       Desloca a imagem quando mexe na rolagem
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.slbGenericoChange(Sender: TObject);
begin

  if Sender as TScrollBar = slbVertical then
    ImgFigura.Top := -mpUnits.y * slbVertical.Position;

  if Sender as TScrollBar = slbHorizontal then
    imgFigura.Left := -mpUnits.x * slbHorizontal.Position;

end;

{*-----------------------------------------------------------------------------
 *  TfrmImagens.FormCreate       Para inicializar os valores das barras
 *
 *-----------------------------------------------------------------------------}
procedure TfrmImagens.FormCreate(Sender: TObject);
begin
  mpAtualizaBarras;
end;

procedure TfrmImagens.imgFiguraMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin

  if ssLeft in Shift then
  begin
    miPosX := X;
    miPosY := Y;
  end;

end;

procedure TfrmImagens.imgFiguraMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin

  if ssLeft in Shift then
  begin
    if (imgFigura.Top - (miPosY - Y) <= 0) and
       (ALTURA_FIG - (imgFigura.Top - (miPosY - Y)) <= imgFigura.Height) then
      imgFigura.Top := imgFigura.Top - (miPosY - Y);
    if (imgFigura.Left - (miPosX - X) <= 0) and
       (LARGURA_FIG - (imgFigura.Left - (miPosX - X)) <= imgFigura.Width) then
      imgFigura.Left := imgFigura.Left - (miPosX - X);
    imgFigura.Refresh;
//    lblTop.Caption := ' miPosY:' + IntToStr(miPosY) + ' Y:' + IntToStr(Y) + ' Top:' + IntToStr(imgFigura.Top);
//    lblLeft.Caption := ' miPosX:' + IntToStr(miPosX) + ' X:' + IntToStr(X) + ' Left:' + IntToStr(imgFigura.Left);
  end;

end;

end.
