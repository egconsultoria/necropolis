unit uniProponente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, uniConst, Mask, OleCtrls, FPSpread_TLB, ComCtrls, Variants;

const
  EDT_CODIGO: string = 'mskCodigo';

  COL_PARC = 1;
  COL_DATA = 2;
  COL_VALOR = 3;
  COL_VALORVENC = 4;
  COL_DIAS = 5;
  COL_CODIGO = 6;

  VLR_MULTA = 0.02;
  VLR_JUROS = 0.00034;

  COL_FOL_OPER = 1;
  COL_FOL_MOTI = 2;
  COL_FOL_FINA = 3;
  COL_FOL_HIST = 4;
  COL_FOL_DATA = 5;
  COL_FOL_RETO = 6;

  DIAS_ATRASO = 0;

type
  TfrmProponente = class(TForm)
    pnlFundo: TPanel;
    grpClientes: TGroupBox;
    pagProponentes: TPageControl;
    tabProponentes: TTabSheet;
    tabBeneficiarios: TTabSheet;
    sprBeneficiarios: TvaSpread;
    tabOcupacao: TTabSheet;
    tabDebitos: TTabSheet;
    sprProponentes: TvaSpread;
    grpContrato: TGroupBox;
    lblCodigo: TLabel;
    mskCodigo: TMaskEdit;
    cboPlano_Lote: TComboBox;
    Label4: TLabel;
    grpGaveta: TGroupBox;
    Label5: TLabel;
    Label13: TLabel;
    Label6: TLabel;
    mskQuadra: TMaskEdit;
    mskSetor: TMaskEdit;
    mskLote: TMaskEdit;
    sprGavetaPessoas: TvaSpread;
    sprDebitoLote: TvaSpread;
    sprDebitoGaveta: TvaSpread;
    sprDebitoTaxa: TvaSpread;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    edtDebitoLote: TEdit;
    edtDebitoGaveta: TEdit;
    edtDebitoTaxa: TEdit;
    PntComunicador: TPaintBox;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label29: TLabel;
    sprDebitoServico: TvaSpread;
    Label30: TLabel;
    edtDebitoServico: TEdit;
    sprCheques: TvaSpread;
    Label31: TLabel;
    mskDataProposta: TMaskEdit;
    Label32: TLabel;
    mskDataPropostaGaveta: TMaskEdit;
    Label38: TLabel;
    edtNumeroGaveta: TEdit;
    TabSheet3: TTabSheet;
    Label33: TLabel;
    sprParcelas: TvaSpread;
    Label34: TLabel;
    sprParcelasServico: TvaSpread;
    tabFollowUpCobranca: TTabSheet;
    sprFollowCobranca: TvaSpread;
    btnAlterar: TButton;
    btnSalvar: TButton;
    Label35: TLabel;
    edtAtrasoLote: TEdit;
    Label36: TLabel;
    edtAtrasoGaveta: TEdit;
    Label37: TLabel;
    edtAtrasoTaxa: TEdit;
    tabEndereco: TTabSheet;
    grpEndereco: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label28: TLabel;
    edtProponente_Endereco: TEdit;
    mskProponente_CEP: TMaskEdit;
    edtProponente_Estado: TEdit;
    edtProponente_Cidade: TEdit;
    edtProponente_Bairro: TEdit;
    edtProponente_DDD: TEdit;
    mskProponente_Telefone: TMaskEdit;
    edtProponente_Ramal: TEdit;
    mskProponente_RG: TMaskEdit;
    mskProponente_CPF: TMaskEdit;
    edtProponente_Observacao: TEdit;
    grpCadastro: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtProponente_Nome: TEdit;
    mskProponente_DataNascimento: TMaskEdit;
    edtProponente_Profissao: TEdit;
    grpBloqueadaLote: TGroupBox;
    Label46: TLabel;
    Label47: TLabel;
    cboBloqueadaLote: TComboBox;
    mskDataBloqueadaLote: TMaskEdit;
    grpBloqueadaGaveta: TGroupBox;
    Label48: TLabel;
    Label49: TLabel;
    cboBloqueadaGaveta: TComboBox;
    mskDataBloqueadaGaveta: TMaskEdit;
    Label24: TLabel;
    Label25: TLabel;
    edtDebitoTotal: TEdit;
    edtAtrasoTotal: TEdit;
    sprTipoServico: TvaSpread;
    Label26: TLabel;
    edtAtrasoServico: TEdit;
    cboSituacao: TComboBox;
    lblSituacao: TLabel;
    cboQuadra: TComboBox;
    grpEnderecoComercial: TGroupBox;
    lblEnderecoComercial: TLabel;
    lblCEPComercial: TLabel;
    lblCidadeComercial: TLabel;
    lblFoneComercial: TLabel;
    lblBairroComercial: TLabel;
    lblEstadoComercial: TLabel;
    lblFormaContato: TLabel;
    edtEnderecoComercial: TEdit;
    mskCEPComercial: TMaskEdit;
    edtCidadeComercial: TEdit;
    mskFoneComercial: TMaskEdit;
    edtBairroComercial: TEdit;
    edtEstadoComercial: TEdit;
    cboFormaContato: TComboBox;
    chkCorrespondencia: TCheckBox;
    Label27: TLabel;
    edtDebitoServicoTotal: TEdit;
    Label39: TLabel;
    edtAtrasoServicoTotal: TEdit;
    mskLote2: TMaskEdit;
    Label40: TLabel;
    Label41: TLabel;
    edtEMail: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure cboComboExit(Sender: TObject);
    procedure cboComboKeyPress(Sender: TObject; var Key: Char);
    procedure edtTextoChange(Sender: TObject);
    procedure edtTextoEnter(Sender: TObject);
    procedure edtTextoExit(Sender: TObject);
    procedure sprGridChange(Sender: TObject; Col, Row: Integer);
    procedure sprGridEditChange(Sender: TObject; Col, Row: Integer);
    procedure chkCheckClick(Sender: TObject);
    procedure sprGavetaPessoasDblClick(Sender: TObject; Col, Row: Integer);
    procedure sprProponentesDblClick(Sender: TObject; Col, Row: Integer);
    procedure btnBotaoClick(Sender: TObject);
    procedure sprFollowCobrancaLeaveCell(Sender: TObject; Col, Row, NewCol,
      NewRow: Integer; var Cancel: WordBool);
    procedure sprFollowCobrancaKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;
    mbSalvar: Boolean;

    procedure mpValor_Receber;
    procedure mpValor_Total;
    procedure mpAjustar_FollowUp;

  public
    { Public declarations }
  end;

var
  frmProponente: TfrmProponente;

implementation

uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos,
    uniVisualizacaoGavetas, uniEntradaManual, uniContratos, uniFuncoes,
  uniRecebServico;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.FormClose(Sender: TObject; var Action: TCloseAction);
var
  liConta:  Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - uniProponente',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;
    gfrmTV := nil;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniPropostaLote');
  end;

  // destroi a janela
  Action := caFree;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, TForm(Sender));

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.FormResize(Sender: TObject);
begin
  if TForm(Sender).WindowState <> wsMinimized Then
  begin
    if (TForm(Sender).Width < pnlFundo.Width) and (TForm(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TForm(Sender).Width := pnlFundo.Width;

    TForm(Sender).Refresh;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
//    mrTelaVar.P.Titulo_Tela := 'Proposta de Lote';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
    untEdit.gp_Edit_EstufaCombos(mrTelaVar.P.Sigla_Tabela, TForm(Sender));

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);

    mpValor_Receber;
    mpValor_Total;
    mpAjustar_FollowUp;

    mbSalvar := False;

    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniPropostaLote');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 3);

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  {' muda o ponteiro do mouse para ampulheta}
  Screen.cursor := crHOURGLASS;

  lcboCombo := TComboBox(Sender);

  {' se nao for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
  begin
    if lcboCombo.Text <> VAZIO then
    begin
      {' altera��o efetuada}
      untEdit.gp_Edit_Alterado(True, mrTelaVar);
    end;
  end
  else
    If (Not mrTelaVar.Reposicionando) Then
      {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

  {' retorna o ponteiro do mouse para o default}
  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  liConta: Integer;
begin

  lcboCombo := TComboBox(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := lcboCombo.Text;
  giAreaDesfaz := -1;
  if lcboCombo.Items.Count > 0 then
    {'Executa um la�o nos itens comparando-os com o pConteudo}
    for liConta := 0 to lcboCombo.Items.Count - 1 do
      if lcboCombo.Items[liConta] = gsAreaDesfaz then
      begin
        giAreaDesfaz := liConta;
        break;
      end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.cboComboKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.cboComboKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.edtTextoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.edtTextoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = EDT_CODIGO) And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.edtTextoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.edtTextoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.sprGridChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.sprGridChange(Sender: TObject; Col,
  Row: Integer);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.sprGridEditChange       Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.sprGridEditChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
  lgrdGrid.Col := Col;
  if lgrdGrid.CellType = SS_CELL_TYPE_COMBOBOX then
    untCombos.gp_Combo_SpreadSeleciona(lgrdGrid, 0);

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.chkCheckClick       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.chkCheckClick(Sender: TObject);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.PntComunicadorPaint(Sender: TObject);
var
  liComandoMDI: Integer;
begin

  liComandoMDI := giComando_MDI;

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, frmProponente);

  if (liComandoMDI = TV_COMANDO_IR_PROXIMO) or
     (liComandoMDI = TV_COMANDO_IR_ANTERIOR) or
     (liComandoMDI = TV_COMANDO_IR_PRIMEIRO) or
     (liComandoMDI = TV_COMANDO_PROCURAR) or
     (liComandoMDI = TV_COMANDO_IR_ULTIMO) then
  begin
    mpValor_Receber;
    mpValor_Total;
    mpAjustar_FollowUp;
    mbSalvar := False;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.cboComboExit(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.edtTextoExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.edtTextoExit(Sender: TObject);
var
  ledtEdit: TMaskEdit;
begin

  ledtEdit := TMaskEdit(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprGavetaPessoasDblClick       Visualiza gavetas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.sprGavetaPessoasDblClick(Sender: TObject; Col,
  Row: Integer);
begin

  { guarda o numero da proposta }
  gsNumProposta := mskCodigo.Text;
  { chama a tela de visual gavetas }
  uniVisualizacaoGavetas.formVisualGaveta.Show;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpValor_Receber       Mostra o valor a receber
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.mpValor_Receber();
var
  liConta: Integer;
  lsParcela: string;
  lsDataVenc: string;
  lsValorVenc: string;
  lsCodigo: string;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  { Lotes }
  sprDebitoLote.ReDraw := False;
  for liConta := 1 to sprDebitoLote.DataRowCnt do //.MaxRows - 1 do
  begin
    sprDebitoLote.Row := liConta;

    { codigo }
    sprDebitoLote.Col := COL_CODIGO;
    lsCodigo := sprDebitoLote.Text;
    { parcela }
    sprDebitoLote.Col := COL_PARC;
    lsParcela := sprDebitoLote.Text;
    { data }
    sprDebitoLote.Col := COL_DATA;
    lsDataVenc := sprDebitoLote.Text;
    { valor }
    sprDebitoLote.Col := COL_VALORVENC;
    lsValorVenc := sprDebitoLote.Text;
    if lsValorVenc <> VAZIO then
      sprDebitoLote.Text := FormatFloat(MK_VALOR,
                            StrToFloat(dmDados.TirarSeparador(lsValorVenc)) -
                            untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                          Copy(lsCodigo,9,1),
                                                          Copy(lsCodigo,10,6)));

    { se ainda nao foi pago }
    if lsParcela = VAZIO then
      break;
    { se nao tem vencimento nao calcula }
    if lsDataVenc = VAZIO then
    begin
      sprDebitoLote.Col := COL_VALOR;
      sprDebitoLote.Text := lsValorVenc;
    end
    else
    begin
      { tolerancia de cinco dias }
      if StrToDate(lsDataVenc) >= (Date - DIAS_ATRASO) then
      begin
        sprDebitoLote.Col := COL_VALOR;
//        sprDebitoLote.Text := lsValorVenc;
        sprDebitoLote.Text := FormatFloat(MK_VALOR,
                              StrToFloat(dmDados.TirarSeparador(lsValorVenc)) -
//                              frmEntradaManual.gfSaldo_Parc(mskCodigo.Text,
//                                                            'L',
//                                                            lsParcela));
                              untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                            Copy(lsCodigo,9,1),
                                                            Copy(lsCodigo,10,6)));
      end
      else
      begin
        sprDebitoLote.Col := COL_VALOR;
        sprDebitoLote.Text := untFuncoes.gf_Funcoes_ValorAtual(mskCodigo.Text,
                                            'L',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela);
        if StrToFloat(dmDados.TirarSeparador(sprDebitoLote.Text)) = 0 then
          sprDebitoLote.Text := lsValorVenc
        else
          sprDebitoLote.Text := FormatFloat(MK_VALOR,
                                StrToFloat(dmDados.TirarSeparador(sprDebitoLote.Text)) -
//                                frmEntradaManual.gfSaldo_Parc(mskCodigo.Text,
//                                                              'L',
//                                                              lsParcela));
                                untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                              Copy(lsCodigo,9,1),
                                                              Copy(lsCodigo,10,6)));
      end;
    end;
    sprDebitoLote.Col := COL_DIAS;
    if Date - StrToDate(lsDataVenc) >= 0 then
      sprDebitoLote.Text := FormatFloat('00', Date - StrToDate(lsDataVenc))
    else
      sprDebitoLote.Text := '00';
  end;
  sprDebitoLote.ReDraw := True;

  { Gavetas }
  sprDebitoGaveta.ReDraw := False;
  for liConta := 1 to sprDebitoGaveta.DataRowCnt do //.MaxRows - 1 do
  begin
    sprDebitoGaveta.Row := liConta;

    { codigo }
    sprDebitoGaveta.Col := COL_CODIGO;
    lsCodigo := sprDebitoGaveta.Text;
    { parcela }
    sprDebitoGaveta.Col := COL_PARC;
    lsParcela := sprDebitoGaveta.Text;
    { data }
    sprDebitoGaveta.Col := COL_DATA;
    lsDataVenc := sprDebitoGaveta.Text;
    { valor }
    sprDebitoGaveta.Col := COL_VALORVENC;
    lsValorVenc := sprDebitoGaveta.Text;
    if lsValorVenc <> VAZIO then
      sprDebitoGaveta.Text := FormatFloat(MK_VALOR,
                              StrToFloat(dmDados.TirarSeparador(lsValorVenc)) -
                              untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                            Copy(lsCodigo,9,1),
                                                            Copy(lsCodigo,10,6)));

    { se ainda nao foi pago }
    if lsParcela = VAZIO then
      break;
    { se nao tem vencimento nao calcula }
    if lsDataVenc = VAZIO then
    begin
      sprDebitoGaveta.Col := COL_VALOR;
      sprDebitoGaveta.Text := lsValorVenc;
    end
    else
    begin
      { tolerancia de cinco dias }
      if StrToDate(lsDataVenc) >= (Date - DIAS_ATRASO) then
      begin
        sprDebitoGaveta.Col := COL_VALOR;
//        sprDebitoGaveta.Text := lsValorVenc;
        sprDebitoGaveta.Text := FormatFloat(MK_VALOR,
                              StrToFloat(dmDados.TirarSeparador(lsValorVenc)) -
//                              frmEntradaManual.gfSaldo_Parc(mskCodigo.Text,
//                                                            'G',
//                                                            lsParcela));
                              untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                            Copy(lsCodigo,9,1),
                                                            Copy(lsCodigo,10,6)));
      end
      else
      begin
        sprDebitoGaveta.Col := COL_VALOR;
        sprDebitoGaveta.Text := untFuncoes.gf_Funcoes_ValorAtual(mskCodigo.Text,
                                            'G',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela);
        if StrToFloat(dmDados.TirarSeparador(sprDebitoGaveta.Text)) = 0 then
          sprDebitoGaveta.Text := lsValorVenc
        else
          sprDebitoGaveta.Text := FormatFloat(MK_VALOR,
                                  StrToFloat(dmDados.TirarSeparador(sprDebitoGaveta.Text)) -
//                                  frmEntradaManual.gfSaldo_Parc(mskCodigo.Text,
//                                                                'G',
//                                                                lsParcela));
                                  untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                                Copy(lsCodigo,9,1),
                                                                Copy(lsCodigo,10,6)));
      end;
    end;
    sprDebitoGaveta.Col := COL_DIAS;
    if Date - StrToDate(lsDataVenc) >= 0 then
      sprDebitoGaveta.Text := FormatFloat('00', Date - StrToDate(lsDataVenc))
    else
      sprDebitoGaveta.Text := '00';
  end;
  sprDebitoGaveta.ReDraw := True;

  { Taxas }
  sprDebitoTaxa.ReDraw := False;
  for liConta := 1 to sprDebitoTaxa.DataRowCnt do   //.MaxRows - 1 do
  begin
    sprDebitoTaxa.Row := liConta;

    { Codigo }
    sprDebitoTaxa.Col := COL_CODIGO;
    lsCodigo := sprDebitoTaxa.Text;
    { parcela }
    sprDebitoTaxa.Col := COL_PARC;
    lsParcela := sprDebitoTaxa.Text;
    { data }
    sprDebitoTaxa.Col := COL_DATA;
    lsDataVenc := sprDebitoTaxa.Text;
    { valor }
    sprDebitoTaxa.Col := COL_VALORVENC;
    lsValorVenc := sprDebitoTaxa.Text;
    if lsValorVenc <> VAZIO then
      sprDebitoTaxa.Text := FormatFloat(MK_VALOR,
                            StrToFloat(dmDados.TirarSeparador(lsValorVenc)) -
                            untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                          Copy(lsCodigo,9,1),
                                                          Copy(lsCodigo,10,6)));

    { se ainda nao foi pago }
    if lsParcela = VAZIO then
      break;
    { se nao tem vencimento nao calcula }
    if lsDataVenc = VAZIO then
    begin
      sprDebitoTaxa.Col := COL_VALOR;
      sprDebitoTaxa.Text := lsValorVenc;
    end
    else
    begin
      { tolerancia de cinco dias }
      if StrToDate(lsDataVenc) >= (Date - DIAS_ATRASO) then
      begin
        sprDebitoTaxa.Col := COL_VALOR;
//        sprDebitoTaxa.Text := lsValorVenc;
        sprDebitoTaxa.Text := FormatFloat(MK_VALOR,
                              StrToFloat(dmDados.TirarSeparador(lsValorVenc)) -
//                              frmEntradaManual.gfSaldo_Parc(mskCodigo.Text,
//                                                            'T',
//                                                            lsParcela));
                              untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                            Copy(lsCodigo,9,1),
                                                            Copy(lsCodigo,10,6)));
      end
      else
      begin
        sprDebitoTaxa.Col := COL_VALOR;
        sprDebitoTaxa.Text := untFuncoes.gf_Funcoes_ValorAtual(mskCodigo.Text,
                                            'T',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela);
        if StrToFloat(dmDados.TirarSeparador(sprDebitoTaxa.Text)) = 0 then
          sprDebitoTaxa.Text := lsValorVenc
        else
          sprDebitoTaxa.Text := FormatFloat(MK_VALOR,
                                StrToFloat(dmDados.TirarSeparador(sprDebitoTaxa.Text)) -
//                                frmEntradaManual.gfSaldo_Parc(mskCodigo.Text,
//                                                              'T',
//                                                              lsParcela));
                                untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                              Copy(lsCodigo,9,1),
                                                              Copy(lsCodigo,10,6)));
      end;
    end;
    sprDebitoTaxa.Col := COL_DIAS;
    if Date - StrToDate(lsDataVenc) >= 0 then
      sprDebitoTaxa.Text := FormatFloat('00', Date - StrToDate(lsDataVenc))
    else
      sprDebitoTaxa.Text := '00';
  end;
  sprDebitoTaxa.ReDraw := True;

  { Servicos }
  sprDebitoServico.ReDraw := False;
  for liConta := 1 to sprDebitoServico.DataRowCnt do  //.MaxRows - 1 do
  begin
    sprDebitoServico.Row := liConta;

    { Codigo }
    sprDebitoServico.Col := COL_CODIGO;
    lsCodigo := sprDebitoServico.Text;
    { parcela }
    sprDebitoServico.Col := COL_PARC;
    lsParcela := sprDebitoServico.Text;
    { data }
    sprDebitoServico.Col := COL_DATA;
    lsDataVenc := sprDebitoServico.Text;
    { valor }
    sprDebitoServico.Col := COL_VALORVENC;
    lsValorVenc := sprDebitoServico.Text;
    if lsValorVenc <> VAZIO then
      sprDebitoServico.Text := FormatFloat(MK_VALOR,
                               StrToFloat(dmDados.TirarSeparador(lsValorVenc)) -
                               untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                            'V',
                                                            Copy(lsCodigo,9,7)));


    { se ainda nao foi pago }
    if lsParcela = VAZIO then
      break;
    { se nao tem vencimento nao calcula }
    if lsDataVenc = VAZIO then
    begin
      sprDebitoServico.Col := COL_VALOR;
      sprDebitoServico.Text := lsValorVenc;
    end
    else
    begin
      { tolerancia de cinco dias }
      if StrToDate(lsDataVenc) >= (Date - DIAS_ATRASO) then
      begin
        sprDebitoServico.Col := COL_VALOR;
//        sprDebitoLote.Text := lsValorVenc;
        sprDebitoServico.Text := FormatFloat(MK_VALOR,
                                 StrToFloat(dmDados.TirarSeparador(lsValorVenc)) -
//                                 frmRecebServico.gfSaldo_Parc(mskCodigo.Text,
//                                                              'L',
//                                                              lsParcela));
                                 untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                              'V',
                                                              Copy(lsCodigo,9,7)));
      end
      else
      begin
        sprDebitoServico.Col := COL_VALOR;
        sprDebitoServico.Text := untFuncoes.gf_Funcoes_ValorAtual(mskCodigo.Text,
                                            'L',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela);
        if StrToFloat(dmDados.TirarSeparador(sprDebitoServico.Text)) = 0 then
          sprDebitoServico.Text := lsValorVenc
        else
          sprDebitoServico.Text := FormatFloat(MK_VALOR,
                                StrToFloat(dmDados.TirarSeparador(sprDebitoServico.Text)) -
//                                frmRecebServico.gfSaldo_Parc(mskCodigo.Text,
//                                                              'L',
//                                                              lsParcela));
                               untFuncoes.gf_Funcoes_SaldoParc(Copy(lsCodigo,1,8),
                                                            'V',
                                                            Copy(lsCodigo,9,7)));
      end;
    end;
    sprDebitoServico.Col := COL_DIAS;
    if Date - StrToDate(lsDataVenc) >= 0 then
      sprDebitoServico.Text := FormatFloat('00', Date - StrToDate(lsDataVenc))
    else
      sprDebitoServico.Text := '00';
  end;
  sprDebitoServico.ReDraw := True;

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpValor_Receber       Mostra o valor a receber
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.mpValor_Total();
var
  liConta:      Integer;
  ldValor: Double;
  ldDebitoLote: Double;
  ldDebitoGave: Double;
  ldDebitoTaxa: Double;
  ldDebitoServ: Double;
  ldAtrasoLote: Double;
  ldAtrasoGave: Double;
  ldAtrasoTaxa: Double;
  ldAtrasoServ: Double;
begin

    { para nao ativar o salvar }
    mrTelaVar.Reposicionando := True;

    ldDebitoLote := 0;
    ldDebitoGave := 0;
    ldDebitoTaxa := 0;
    ldDebitoServ := 0;
    ldAtrasoLote := 0;
    ldAtrasoGave := 0;
    ldAtrasoTaxa := 0;
    ldAtrasoServ := 0;

    for liConta := 1 to sprDebitoLote.DataRowCnt do
    begin
      sprDebitoLote.Row := liConta;
      sprDebitoLote.Col := COL_VALOR;
      if sprDebitoLote.Text <> VAZIO then
        ldValor := StrToFloat(dmDados.TirarSeparador(sprDebitoLote.Text))
      else
        ldValor := 0;
      ldDebitoLote := ldDebitoLote + ldValor;
      { soma dos atrasados }
      sprDebitoLote.Col := COL_DATA;
      if sprDebitoLote.Text <> VAZIO then
        if StrToDate(sprDebitoLote.Text) < Date() - 1 then
          ldAtrasoLote := ldAtrasoLote + ldValor;
    end;
    for liConta := 1 to sprDebitoGaveta.DataRowCnt do
    begin
      sprDebitoGaveta.Row := liConta;
      sprDebitoGaveta.Col := COL_VALOR;
      if sprDebitoGaveta.Text <> VAZIO then
        ldValor := StrToFloat(dmDados.TirarSeparador(sprDebitoGaveta.Text))
      else
        ldValor := 0;
      ldDebitoGave := ldDebitoGave + ldValor;
      { soma dos atrasados }
      sprDebitoGaveta.Col := COL_DATA;
      if sprDebitoGaveta.Text <> VAZIO then
        if StrToDate(sprDebitoGaveta.Text) < Date() - 1 then
          ldAtrasoGave := ldAtrasoGave + ldValor;
    end;
    for liConta := 1 to sprDebitoTaxa.DataRowCnt do
    begin
      sprDebitoTaxa.Row := liConta;
      sprDebitoTaxa.Col := COL_VALOR;
      if sprDebitoTaxa.Text <> VAZIO then
        ldValor := StrToFloat(dmDados.TirarSeparador(sprDebitoTaxa.Text))
      else
        ldValor := 0;
      ldDebitoTaxa := ldDebitoTaxa + ldValor;
      { soma dos atrasados }
      sprDebitoTaxa.Col := COL_DATA;
      if sprDebitoTaxa.Text <> VAZIO then
        if StrToDate(sprDebitoTaxa.Text) < Date() - 1 then
          ldAtrasoTaxa := ldAtrasoTaxa + ldValor;
    end;
    for liConta := 1 to sprDebitoServico.DataRowCnt do
    begin
      sprDebitoServico.Row := liConta;
      sprDebitoServico.Col := COL_VALOR;
      if sprDebitoServico.Text <> VAZIO then
        ldValor := StrToFloat(dmDados.TirarSeparador(sprDebitoServico.Text))
      else
        ldValor := 0;
      ldDebitoServ := ldDebitoServ + ldValor;
      { soma dos atrasados }
      sprDebitoServico.Col := COL_DATA;
      if sprDebitoServico.Text <> VAZIO then
        if StrToDate(sprDebitoServico.Text) < Date() - 1 then
          ldAtrasoServ := ldAtrasoServ + ldValor;
    end;

    edtDebitoLote.Text := FormatFloat(MK_VALOR, ldDebitoLote);
    edtDebitoGaveta.Text := FormatFloat(MK_VALOR, ldDebitoGave);
    edtDebitoTaxa.Text := FormatFloat(MK_VALOR, ldDebitoTaxa);
    edtDebitoServico.Text := FormatFloat(MK_VALOR, ldDebitoServ);
    edtDebitoServicoTotal.Text := FormatFloat(MK_VALOR, ldDebitoServ);

    edtAtrasoLote.Text := FormatFloat(MK_VALOR, ldAtrasoLote);
    edtAtrasoGaveta.Text := FormatFloat(MK_VALOR, ldAtrasoGave);
    edtAtrasoTaxa.Text := FormatFloat(MK_VALOR, ldAtrasoTaxa);
    edtAtrasoServico.Text := FormatFloat(MK_VALOR, ldAtrasoServ);
    edtAtrasoServicoTotal.Text := FormatFloat(MK_VALOR, ldAtrasoServ);

    edtDebitoTotal.Text := FormatFloat(MK_VALOR, ldDebitoLote+ldDebitoGave+ldDebitoTaxa+ldDebitoServ);
    edtAtrasoTotal.Text := FormatFloat(MK_VALOR, ldAtrasoLote+ldAtrasoGave+ldAtrasoTaxa+ldAtrasoServ);

    { para nao ativar o salvar }
    mrTelaVar.Reposicionando := False;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprProponentesDblClick       Visualiza documentos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.sprProponentesDblClick(Sender: TObject; Col,
  Row: Integer);
begin

  { guarda o numero da proposta }
  gsNumProposta := mskCodigo.Text;
  { chama a tela de visual gavetas }
  uniContratos.frmImagens.Show;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.btnBotaoClick       Alterar follow-up
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.btnBotaoClick(Sender: TObject);
var
  lbtnBotao: TButton;

  liConta: Integer;
  liContaCol: Integer;
  liConexao: Integer;
  lsSql: string;
  liIndCol: array[0..7] of Integer;
begin

  lbtnBotao := TButton(Sender);

  if lbtnBotao.Name = 'btnAlterar' then
  begin
    sprFollowCobranca.Row := 1;
    sprFollowCobranca.Col := 1;
    sprFollowCobranca.Row2 := sprFollowCobranca.MaxRows;
    sprFollowCobranca.Col2 := sprFollowCobranca.MaxCols;
    sprFollowCobranca.BlockMode := True;
    sprFollowCobranca.Lock := False;
    sprFollowCobranca.ForeColor := clBlack;
    sprFollowCobranca.BlockMode := False;
    mbSalvar := True;
  end;

  if (lbtnBotao.Name = 'btnSalvar') and (mbSalvar) then
  begin
    for liConta :=  1 to sprFollowCobranca.DataRowCnt do
    begin
      { ler os registros da tabela proplote }
      gaParm[0] := 'FOLLOWCO';
      gaParm[1] := 'NUM_PROP = ''' + mskCodigo.Text + ''' AND CODIGO = ' + IntToStr(liConta);
      gaParm[2] := 'NUM_PROP, CODIGO';
      lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;
      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      for liContaCol := 0 to High(liIndCol) do
        liIndCol[liContaCol] := IOPTR_NOPOINTER;

      sprFollowCobranca.Row := liConta;

      { nao achou grava }
      if dmDados.Status(liConexao, IOSTATUS_NOROWS) then
        { incluir o acomp }
        dmDados.gsRetorno := dmDados.AtivaModo(liConexao,IOMODE_INSERT)
      else
        { incluir o acomp }
        dmDados.gsRetorno := dmDados.AtivaModo(liConexao,IOMODE_EDIT);

      { nao achou grava }
      if dmDados.Status(liConexao, IOSTATUS_NOROWS) then
      begin
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'Num_Prop', liIndCol[0], mskCodigo.Text);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'Codigo', liIndCol[1], liConta);
      end;
      sprFollowCobranca.Col := COL_FOL_OPER;
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'OPERADORA', liIndCol[2], untCombos.gf_Combo_SpreadSemDescricao(sprFollowCobranca));
      sprFollowCobranca.Col := COL_FOL_MOTI;
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'MOTIVO', liIndCol[3], untCombos.gf_Combo_SpreadSemDescricao(sprFollowCobranca));
      sprFollowCobranca.Col := COL_FOL_FINA;
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'SIT_FINAN', liIndCol[4], sprFollowCobranca.Text);
      sprFollowCobranca.Col := COL_FOL_HIST;
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'HISTORICO', liIndCol[5], sprFollowCobranca.Text);
      sprFollowCobranca.Col := COL_FOL_DATA;
      if sprFollowCobranca.Text <> VAZIO then
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'DATA', liIndCol[6], StrToDate(sprFollowCobranca.Text))
      else
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'DATA', liIndCol[6], VAZIO);
      sprFollowCobranca.Col := COL_FOL_RETO;
      if sprFollowCobranca.Text <> VAZIO then
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'RETORNO', liIndCol[7], StrToDate(sprFollowCobranca.Text))
      else
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'RETORNO', liIndCol[7], VAZIO);

      { nao achou grava }
      if dmDados.Status(liConexao, IOSTATUS_NOROWS) then
        { faz a atualizacao}
        dmDados.gsRetorno := dmDados.Incluir(liConexao, 'FOLLOWCO')
      else
        { faz a atualizacao}
        dmDados.gsRetorno := dmDados.Alterar(liConexao, 'FOLLOWCO', VAZIO);


      dmDados.FecharConexao(liConexao);
    end;
    sprFollowCobranca.Row := 1;
    sprFollowCobranca.Col := 1;
    sprFollowCobranca.Row2 := sprFollowCobranca.MaxRows;
    sprFollowCobranca.Col2 := sprFollowCobranca.MaxCols;
    sprFollowCobranca.BlockMode := True;
    sprFollowCobranca.Lock := True;
    sprFollowCobranca.ForeColor := COR_CONSULTA;
    sprFollowCobranca.BlockMode := False;
    mbSalvar := True;
  end;

end;

{*-----------------------------------------------------------------------------
 *  mpAjustar_FollowUp       Aumenta o tamanho da linha para caber o texto todo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.mpAjustar_FollowUp();
var
  liConta: Integer;
  liLinha: Integer;
  lnTamanho: Double;
begin

  { muda o tamanho de todas as linhas }
  sprFollowCobranca.RowHeight[-1] := sprProponentes.RowHeight[1];  { muda a altura de todas as linhas }
  for liConta := 1 to sprFollowCobranca.DataRowCnt  do
  begin
    liLinha := sprFollowCobranca.Row;
    { retorna o tamanho da linha }    lnTamanho := sprFollowCobranca.MaxTextRowHeight[liLinha];    { muda o tamanho da linha }    sprFollowCobranca.RowHeight[liLinha] := lnTamanho;    sprFollowCobranca.Row := sprFollowCobranca.Row + 1  ;  end;
end;

{*-----------------------------------------------------------------------------
 *  sprFollowCobrancaLeaveCell       Preenche a data automaticamente
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.sprFollowCobrancaLeaveCell(Sender: TObject; Col,
  Row, NewCol, NewRow: Integer; var Cancel: WordBool);
begin

  if Col = COL_FOL_HIST then
  begin
    sprFollowCobranca.Row := Row;
    sprFollowCobranca.Col := Col;
    { se preencheu o historico }
    if sprFollowCobranca.Text <> VAZIO then
    begin
      sprFollowCobranca.Col := COL_FOL_DATA;
      if sprFollowCobranca.Text = VAZIO then
        sprFollowCobranca.Text := FormatDateTime(MK_DATATELA, Date);
    end;
    sprFollowCobranca.EditModePermanent := False;
  end;

end;

{*-----------------------------------------------------------------------------
 *  sprFollowCobrancaKeyUp       Aumenta o tamanho do Historico conforme digita
 *
 *-----------------------------------------------------------------------------}
procedure TfrmProponente.sprFollowCobrancaKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  liConta: Integer;
  lnTamanho: Double;
begin

    if sprFollowCobranca.ActiveCol = COL_FOL_HIST then
    begin
      if Key = 13 then
      begin
        liConta := sprFollowCobranca.ActiveRow;
        { retorna o tamanho da linha }
        lnTamanho := sprFollowCobranca.MaxTextRowHeight[liConta];        { muda o tamanho da linha }        sprFollowCobranca.RowHeight[liConta] := lnTamanho;        sprFollowCobranca.Row := sprFollowCobranca.ActiveRow;        sprFollowCobranca.Col := COL_FOL_HIST;        sprFollowCobranca.EditModePermanent := True;        sprFollowCobranca.SelStart := Length(sprFollowCobranca.Text);      end;    end;

end;





end.
