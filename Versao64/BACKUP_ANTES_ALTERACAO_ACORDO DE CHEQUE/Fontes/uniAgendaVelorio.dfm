�
 TFRMAGENDAVELORIO 0�  TPF0TfrmAgendaVeloriofrmAgendaVelorioLeft� Top� Width�Height`Caption   Agenda VelórioColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	Position	poDefaultVisible	
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidth�Height5Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TLabellblSalasLeftToplWidthXHeightCaption   Salas Disponíveis:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TPaintBoxPntComunicadorLeft@Top�WidthYHeight� OnPaintPntComunicadorPaint  TPanelpnlAgendaVelorioLeftTopWidth�Height]
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder  	TGroupBox
grpEntradaLeftTopWidthqHeight)CaptionEntradaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabellblHoraEntradaLeft� TopWidthHeightCaptionHora:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblDataEntradaLeftTopWidthHeightCaptionData:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDateTimePickerdtpDataEntradaLeft5TopWidthhHeightCalAlignmentdtaLeftDate ��r�@Time ��r�@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder   TDateTimePickerdtpHoraEntradaLeft� TopWidthDHeightCalAlignmentdtaLeftDate ��r�@Time ��r�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkTime
ParseInput
ParentFontTabOrder   	TGroupBoxgrpSaidaLeftTop0WidthqHeight)Caption   SaídaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabellblHoraSaidaLeft� TopWidthHeightCaptionHora:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblDataSaidaLeftTopWidthHeightCaptionData:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDateTimePickerdtpDataSaidaLeft5TopWidthhHeightCalAlignmentdtaLeftDate ��r�@Time ��r�@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder   TDateTimePickerdtpHoraSaidaLeft� TopWidthDHeightCalAlignmentdtaLeftDate ��r�@Time ��r�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkTime
ParseInput
ParentFontTabOrder    	TComboBoxcboSalasLeftpTophWidthiHeight
ItemHeightSorted	TabOrderOnChangecboGenericoChangeOnClickcboGenericoClickOnEntercboGenericoEnter  TButtonbtnVisualGradeLeft� TophWidthiHeightCaptionVisualizar GradeTabOrderVisibleOnClickbtnGenericoClick  	TGroupBoxgrpNomesLeftTop� Width�Height� TabOrder TLabellblFalecidoNomeLeftTopWidthVHeightCaptionNome do FalecidoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblResponsavelLeftTop8WidthlHeightCaption   Nome do ResponsávelFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblTelefoneLeftTop`Width*HeightCaptionTelefoneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblFalecidoSobrenomeLeft� TopWidthpHeightCaptionSobrenome do FalecidoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedtFalecidoNomeLeftTop Width� Height	MaxLengthTabOrder OnChangeedtCodigoChangeOnEnteredtCodigoEnter  TEditedtResponsavelLeftTopHWidth�Height	MaxLengthdTabOrderOnChangeedtCodigoChangeOnEnteredtCodigoEnter  TEditedtTelefoneLeftToppWidth� Height	MaxLengthdTabOrderOnChangeedtCodigoChangeOnEnteredtCodigoEnter  TEditedtFalecidoSobrenomeLeft� Top Width� Height	MaxLengthTabOrderOnChangeedtCodigoChangeOnEnteredtCodigoEnter   TButtonbtnConfirmarLeftNTopWidthiHeightCaption	ConfirmarTabOrderOnClickbtnGenericoClick  TButtonbtnCancelarLeft� TopWidthiHeightCancel	CaptionCancelarDefault	TabOrderOnClickbtnGenericoClick    