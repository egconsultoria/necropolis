unit uniProcuraNome;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,
  uniConst, StdCtrls, OleCtrls, FPSpread_TLB;

const
  COL_NUM_PROP    = 1;
  COL_NOME_PROP   = 2;
  COL_SITUACAO    = 3;
  COL_TITULO      = 4;
  COL_LOCALIZACAO = 5;

  TIT_TITULAR       = '1';
  TIT_COTITULAR     = '2';
  TIT_BENEFICIAR    = '3';
  TIT_OCUPANTE      = '4';
  TIT_TITULARNIC    = '5';
  TIT_COTITULARNIC  = '6';
  TIT_OCUPANTENIC   = '7';

type
  TformProcuraNome = class(TForm)
    pnlFundo: TPanel;
    grpProcuraNome: TGroupBox;
    Label1: TLabel;
    edtNome: TEdit;
    Label2: TLabel;
    cboTipo: TComboBox;
    sprNomes: TvaSpread;
    Label40: TLabel;
    cboTipoTitulo: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure sprNomesDblClick(Sender: TObject; Col, Row: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formProcuraNome: TformProcuraNome;

implementation

{$R *.DFM}
uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos,
  uniProcessos, uniVisualizacaoGavetas;

procedure TformProcuraNome.FormShow(Sender: TObject);
var
  liConta:    Integer;
  liConexao:  Integer;
  lsSql:      string;
  liIndCol:   array[0..20] of Integer;
  lsTitulo:   string;
begin

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    Refresh;
    Enabled := False;

    { posiciona na primeira linha }
    sprNomes.MaxRows := 1;

    { limpar o spread}
    sprNomes.Row := 1;
    sprNomes.Col := 1;
    sprNomes.Row2 := sprNomes.MaxRows;
    sprNomes.Col2 := sprNomes.MaxCols;
    sprNomes.BlockMode := True;
    sprNomes.Action := SS_ACTION_CLEAR;
    sprNomes.BlockMode := False;

    { mostra o que foi selecionado }
    edtNome.Text := formProcessos.edtProcuraNome_Nome.Text;
    cboTipo.Text := formProcessos.cboProcuraNome_Tipo.Text;
    cboTipoTitulo.Text := formProcessos.cboProcuraNome_TipoTitulo.Text;

    { guarda o tipo do titulo }
    lsTitulo := untCombos.gf_Combo_SemDescricao(cboTipoTitulo);

    for liConta := 0 to High(liIndCol) do
      liIndCol[liConta] := IOPTR_NOPOINTER;

    liConta := 1;

    if (lsTitulo = TIT_TITULAR) or (lsTitulo = VAZIO) then
    begin
    { titulares }
    gaParm[0] := 'PROPONEN.NUM_PROP "NUM_PROP", NOME_PROP, QUADRA + '' '' + SETOR + '' '' + LOTE "LOCAL", PROPLOTE.BLOQUEADA "BLOQUEADA"';
    gaParm[1] := 'PROPONEN LEFT JOIN PROPGAVE ON PROPONEN.NUM_PROP = PROPGAVE.NUM_PROP' +
                 ' LEFT JOIN PROPLOTE ON PROPONEN.NUM_PROP = PROPLOTE.NUM_PROP';
    gaParm[2] := 'NOME_PROP';
    gaParm[3] := formProcessos.edtProcuraNome_Nome.Text;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '1' then
      gaParm[3] := gaParm[3] + CTEDB_LIKETODOSORCL;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '3' then
      gaParm[3] := CTEDB_LIKETODOSORCL + gaParm[3] + CTEDB_LIKETODOSORCL;
    lsSql := dmDados.SqlVersao('NEC_0004', gaParm);
    liConexao := dmDados.ExecutarSelect(lsSql);

    if liConexao = IOPTR_NOPOINTER then
      Exit;

    while not dmDados.Status(liConexao, IOSTATUS_EOF) do
    begin
      { se for spread checa linhas }
      if sprNomes.MaxRows < liConta then
        sprNomes.MaxRows := liConta;
      sprNomes.Row := liConta;

      sprNomes.Col := COL_NUM_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[1]);
      sprNomes.Col := COL_NOME_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NOME_PROP', liIndCol[2]);
      sprNomes.Col := COL_SITUACAO;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'BLOQUEADA', liIndCol[13]);
      sprNomes.Col := COL_TITULO;
      sprNomes.Text := 'TITULAR';
      sprNomes.Col := COL_LOCALIZACAO;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'LOCAL', liIndCol[9]);

      dmDados.Proximo(liConexao);
      liConta := liConta + 1;
    end;
    dmDados.FecharConexao(liConexao);
    end;

    if (lsTitulo = TIT_COTITULAR) or (lsTitulo = VAZIO) then
    begin
    { co-titulares }
    gaParm[0] := 'PROPOSEC.NUM_PROP "NUM_PROP", NOME_SEC, QUADRA + '' '' + SETOR + '' '' + LOTE "LOCAL", PROPLOTE.BLOQUEADA "BLOQUEADA"';
    gaParm[1] := 'PROPOSEC LEFT JOIN PROPGAVE ON PROPOSEC.NUM_PROP = PROPGAVE.NUM_PROP'+
                 ' LEFT JOIN PROPLOTE ON PROPOSEC.NUM_PROP = PROPLOTE.NUM_PROP';
    gaParm[2] := 'NOME_SEC';
    gaParm[3] := formProcessos.edtProcuraNome_Nome.Text;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '1' then
      gaParm[3] := gaParm[3] + CTEDB_LIKETODOSORCL;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '3' then
      gaParm[3] := CTEDB_LIKETODOSORCL + gaParm[3] + CTEDB_LIKETODOSORCL;
    lsSql := dmDados.SqlVersao('NEC_0004', gaParm);
    liConexao := dmDados.ExecutarSelect(lsSql);

    if liConexao = IOPTR_NOPOINTER then
      Exit;

    while not dmDados.Status(liConexao, IOSTATUS_EOF) do
    begin
      { se for spread checa linhas }
      if sprNomes.MaxRows < liConta then
        sprNomes.MaxRows := liConta;
      sprNomes.Row := liConta;

      sprNomes.Col := COL_NUM_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[3]);
      sprNomes.Col := COL_NOME_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NOME_SEC', liIndCol[4]);
      sprNomes.Col := COL_SITUACAO;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'BLOQUEADA', liIndCol[14]);
      sprNomes.Col := COL_TITULO;
      sprNomes.Text := 'CO-TITULAR';
      sprNomes.Col := COL_LOCALIZACAO;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'LOCAL', liIndCol[10]);

      dmDados.Proximo(liConexao);
      liConta := liConta + 1;
    end;
    dmDados.FecharConexao(liConexao);
    end;

    if (lsTitulo = TIT_BENEFICIAR) or (lsTitulo = VAZIO) then
    begin
    { Beneficiarios }
    gaParm[0] := 'BEN_PROP.NUM_PROP "NUM_PROP", BENEFICIAR, QUADRA + '' '' + SETOR + '' '' + LOTE "LOCAL", PROPLOTE.BLOQUEADA "BLOQUEADA"';
    gaParm[1] := 'BEN_PROP LEFT JOIN PROPGAVE ON BEN_PROP.NUM_PROP = PROPGAVE.NUM_PROP'+
                 ' LEFT JOIN PROPLOTE ON BEN_PROP.NUM_PROP = PROPLOTE.NUM_PROP';
    gaParm[2] := 'BENEFICIAR';
    gaParm[3] := formProcessos.edtProcuraNome_Nome.Text;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '1' then
      gaParm[3] := gaParm[3] + CTEDB_LIKETODOSORCL;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '3' then
      gaParm[3] := CTEDB_LIKETODOSORCL + gaParm[3] + CTEDB_LIKETODOSORCL;
    lsSql := dmDados.SqlVersao('NEC_0004', gaParm);
    liConexao := dmDados.ExecutarSelect(lsSql);

    if liConexao = IOPTR_NOPOINTER then
      Exit;

    while not dmDados.Status(liConexao, IOSTATUS_EOF) do
    begin
      { se for spread checa linhas }
      if sprNomes.MaxRows < liConta then
        sprNomes.MaxRows := liConta;
      sprNomes.Row := liConta;

      sprNomes.Col := COL_NUM_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[5]);
      sprNomes.Col := COL_NOME_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'BENEFICIAR', liIndCol[6]);
      sprNomes.Col := COL_SITUACAO;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'BLOQUEADA', liIndCol[15]);
      sprNomes.Col := COL_TITULO;
      sprNomes.Text := 'BENEFICIARIO';
      sprNomes.Col := COL_LOCALIZACAO;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'LOCAL', liIndCol[11]);

      dmDados.Proximo(liConexao);
      liConta := liConta + 1;
    end;
    dmDados.FecharConexao(liConexao);
    end;

    if (lsTitulo = TIT_OCUPANTE) or (lsTitulo = VAZIO) then
    begin
    { Ocupantes }
    gaParm[0] := 'NUM_PROP, NOME + '' '' + SOBRENOME "COMPLETO", TIPO_MOV, ' +
                 'PROPGAVE.QUADRA + '' '' + PROPGAVE.SETOR + '' '' + PROPGAVE.LOTE "LOCAL", ' +
                 'GAVEPESS.QUADRA + '' '' + GAVEPESS.SETOR + '' '' + GAVEPESS.LOTE "LOCAL_ESPE"';
    gaParm[1] := 'GAVEPESS LEFT JOIN PROPGAVE ON SUBSTRING(GAVEPESS.CODIGO,1,8) = PROPGAVE.NUM_PROP';
    gaParm[2] := 'NOME + '' '' + SOBRENOME';
    gaParm[3] := formProcessos.edtProcuraNome_Nome.Text;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '1' then
      gaParm[3] := gaParm[3] + CTEDB_LIKETODOSORCL;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '3' then
      gaParm[3] := CTEDB_LIKETODOSORCL + gaParm[3] + CTEDB_LIKETODOSORCL;
    lsSql := dmDados.SqlVersao('NEC_0004', gaParm);
    liConexao := dmDados.ExecutarSelect(lsSql);

    if liConexao = IOPTR_NOPOINTER then
      Exit;

    while not dmDados.Status(liConexao, IOSTATUS_EOF) do
    begin
      { se for spread checa linhas }
      if sprNomes.MaxRows < liConta then
        sprNomes.MaxRows := liConta;
      sprNomes.Row := liConta;

      sprNomes.Col := COL_NUM_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[7]);
      sprNomes.Col := COL_NOME_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'COMPLETO', liIndCol[8]);
      sprNomes.Col := COL_TITULO;
      if dmDados.ValorColuna(liConexao, 'TIPO_MOV', liIndCol[9]) = 'I' then
        sprNomes.Text := 'INUMADO';
      if dmDados.ValorColuna(liConexao, 'TIPO_MOV', liIndCol[9]) = 'R' then
        sprNomes.Text := 'REINUMADO';
      sprNomes.Col := COL_LOCALIZACAO;
      if dmDados.ValorColuna(liConexao, 'LOCAL_ESPE', liIndCol[13]) = VAZIO then
        sprNomes.Text := dmDados.ValorColuna(liConexao, 'LOCAL', liIndCol[12])
      else
        sprNomes.Text := dmDados.ValorColuna(liConexao, 'LOCAL', liIndCol[12]) + ' (' +
                         dmDados.ValorColuna(liConexao, 'LOCAL_ESPE', liIndCol[13]) + ')';

      dmDados.Proximo(liConexao);
      liConta := liConta + 1;
    end;
    dmDados.FecharConexao(liConexao);
    end;

    if (lsTitulo = TIT_TITULARNIC) or (lsTitulo = VAZIO) then
    begin
    { titulares }
    gaParm[0] := 'PRONICHO.NUM_PROP "NUM_PROP", NOME_PROP, QUADRA + '' '' + SETOR + '' '' + LOTE + '' '' + LOCULO "LOCAL"';
    gaParm[1] := 'PRONICHO LEFT JOIN NICHO ON PRONICHO.NUM_PROP = SUBSTRING(NICHO.CODIGO,1,8)';
    gaParm[2] := 'NOME_PROP';
    gaParm[3] := formProcessos.edtProcuraNome_Nome.Text;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '1' then
      gaParm[3] := gaParm[3] + CTEDB_LIKETODOSORCL;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '3' then
      gaParm[3] := CTEDB_LIKETODOSORCL + gaParm[3] + CTEDB_LIKETODOSORCL;
    lsSql := dmDados.SqlVersao('NEC_0004', gaParm);
    liConexao := dmDados.ExecutarSelect(lsSql);

    if liConexao = IOPTR_NOPOINTER then
      Exit;

    while not dmDados.Status(liConexao, IOSTATUS_EOF) do
    begin
      { se for spread checa linhas }
      if sprNomes.MaxRows < liConta then
        sprNomes.MaxRows := liConta;
      sprNomes.Row := liConta;

      sprNomes.Col := COL_NUM_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[1]);
      sprNomes.Col := COL_NOME_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NOME_PROP', liIndCol[2]);
      sprNomes.Col := COL_TITULO;
      sprNomes.Text := 'TITULAR';
      sprNomes.Col := COL_LOCALIZACAO;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'LOCAL', liIndCol[9]);

      dmDados.Proximo(liConexao);
      liConta := liConta + 1;
    end;
    dmDados.FecharConexao(liConexao);
    end;

    if (lsTitulo = TIT_COTITULARNIC) or (lsTitulo = VAZIO) then
    begin
    { co-titulares }
    gaParm[0] := 'NICHOSEC.NUM_PROP "NUM_PROP", NOME_SEC, QUADRA + '' '' + SETOR + '' '' + LOTE + '' '' + LOCULO "LOCAL"';
    gaParm[1] := 'NICHOSEC LEFT JOIN NICHO ON NICHOSEC.NUM_PROP = SUBSTRING(NICHO.CODIGO,1,8)';
    gaParm[2] := 'NOME_SEC';
    gaParm[3] := formProcessos.edtProcuraNome_Nome.Text;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '1' then
      gaParm[3] := gaParm[3] + CTEDB_LIKETODOSORCL;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '3' then
      gaParm[3] := CTEDB_LIKETODOSORCL + gaParm[3] + CTEDB_LIKETODOSORCL;
    lsSql := dmDados.SqlVersao('NEC_0004', gaParm);
    liConexao := dmDados.ExecutarSelect(lsSql);

    if liConexao = IOPTR_NOPOINTER then
      Exit;

    while not dmDados.Status(liConexao, IOSTATUS_EOF) do
    begin
      { se for spread checa linhas }
      if sprNomes.MaxRows < liConta then
        sprNomes.MaxRows := liConta;
      sprNomes.Row := liConta;

      sprNomes.Col := COL_NUM_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[3]);
      sprNomes.Col := COL_NOME_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NOME_SEC', liIndCol[4]);
      sprNomes.Col := COL_TITULO;
      sprNomes.Text := 'CO-TITULAR';
      sprNomes.Col := COL_LOCALIZACAO;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'LOCAL', liIndCol[10]);

      dmDados.Proximo(liConexao);
      liConta := liConta + 1;
    end;
    dmDados.FecharConexao(liConexao);
    end;

    if (lsTitulo = TIT_OCUPANTENIC) or (lsTitulo = VAZIO) then
    begin
    { Ocupantes }
    gaParm[0] := 'SUBSTRING(NICHOPES.CODIGO,1,8) "NUM_PROP", NOME + '' '' + SOBRENOME "COMPLETO", NICHO.QUADRA + '' '' + NICHO.SETOR + '' '' + NICHO.LOTE + '' '' + NICHO.LOCULO + '' '' + NICHO.NUM_NICHO "LOCAL"';
    gaParm[1] := 'NICHOPES LEFT JOIN NICHO ON NICHOPES.CODIGO = NICHO.CODIGO';
    gaParm[2] := 'NOME + '' '' + SOBRENOME';
    gaParm[3] := formProcessos.edtProcuraNome_Nome.Text;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '1' then
      gaParm[3] := gaParm[3] + CTEDB_LIKETODOSORCL;
    if Copy(formProcessos.cboProcuraNome_Tipo.Text, 1, 1) = '3' then
      gaParm[3] := CTEDB_LIKETODOSORCL + gaParm[3] + CTEDB_LIKETODOSORCL;
    lsSql := dmDados.SqlVersao('NEC_0004', gaParm);
    liConexao := dmDados.ExecutarSelect(lsSql);

    if liConexao = IOPTR_NOPOINTER then
      Exit;

    while not dmDados.Status(liConexao, IOSTATUS_EOF) do
    begin
      { se for spread checa linhas }
      if sprNomes.MaxRows < liConta then
        sprNomes.MaxRows := liConta;
      sprNomes.Row := liConta;

      sprNomes.Col := COL_NUM_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[7]);
      sprNomes.Col := COL_NOME_PROP;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'COMPLETO', liIndCol[8]);
      sprNomes.Col := COL_TITULO;
      sprNomes.Text := 'OCUPA��O';
      sprNomes.Col := COL_LOCALIZACAO;
      sprNomes.Text := dmDados.ValorColuna(liConexao, 'LOCAL', liIndCol[12]);

      dmDados.Proximo(liConexao);
      liConta := liConta + 1;
    end;
    dmDados.FecharConexao(liConexao);
    end;

    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniProcuraNome.pas');
  end;
end;

procedure TformProcuraNome.sprNomesDblClick(Sender: TObject; Col,
  Row: Integer);
begin

  sprNomes.Row := Row;
  sprNomes.Col := COL_NUM_PROP;
  { guarda o numero da proposta }
  gsNumProposta := sprNomes.Text;
  { chama a tela de visual gavetas }
  uniVisualizacaoGavetas.formVisualGaveta.Show;

end;


end.
