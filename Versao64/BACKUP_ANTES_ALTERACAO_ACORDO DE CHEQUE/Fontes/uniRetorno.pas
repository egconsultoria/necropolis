unit uniRetorno;
 
{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  StdCtrls, ComCtrls, Buttons, DB, DBTables, Crystal_TLB;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const

  CAMPO_REGISTRO        = 'Registro';
  CAMPO_NOSSONUMERO     = 'NNumero';
  CAMPO_NUMERODOCUMENTO = 'NDocumento';
  CAMPO_VALORRECEBIDO   = 'ValorRecebimento';
  CAMPO_DATAPAGO        = 'DataRecebimento';
  CAMPO_OCORRENCIA      = 'Ocorrencia';
  CAMPO_MOTIVOREJ       = 'Motivo';
  CAMPO_DATACREDITO     = 'DataCredito';
  CAMPO_BANCO           = 'Banco';

  TABELA_REC              = 'RECEB';
  TABELA_REC_SEGU         = 'REC_SEGU';
  TABELA_REC_SERV         = 'REC_SERV';

  CAMPO_REC_CODIGO        = 'CODIGO';
  CAMPO_REC_VLR_VENC      = 'VLR_PARC';
  CAMPO_REC_DATA_VENC     = 'D_VENCTO';
  CAMPO_REC_VLR_RECEB     = 'VLR_RECEB';
  CAMPO_REC_DATA_RECEB    = 'DATA_RECEB';
  CAMPO_REC_DATA_CREDITO  = 'DATA_CREDI';
  CAMPO_REC_PAGTO         = 'PAGTO';
  CAMPO_REC_CAIXA_BANC    = 'CAIXA_BANC';

  CAMPO_REC_TIPO_DOC      = 'TIPO_DOC';
  CAMPO_REC_TIPO_REC      = 'TIPO_REC';
  CAMPO_REC_TIPO_TAXA     = 'TIPO_TAXA';
  CAMPO_REC_VLR_VENC1     = 'VLR_PARC1';
  CAMPO_REC_VLR_VENC2     = 'VLR_PARC2';

  CAMPO_REC_REF_STAND     = 'REF_STAND';

  TAXA_UNICA = 'U';
  TAXA_SEMESTRAL = 'S';
  TAXA_BIMESTRAL = 'B';

 {*======================================================================
 *            RECORDS
 *======================================================================}
type

  TRetorno = record
    bEmUso : Boolean;
    sValor : Variant;
    sCampo : String;
    sTipo  : String;
  end;
{*======================================================================
 *            CLASSES
 *======================================================================}
  TdmRetorno = class(TDataModule)
  private
    { Private declarations }
    miIndCol: array[0..20] of Integer;
    mrRetorno: Array[0..20] of TRetorno;

    function AbrirArquivo(const sNomeArquivo: string): TTable;
    procedure FecharArquivo(var tbArquivo: TTable);

  public
    { Public declarations }
    gdbDados:  TDatabase;
    gtbTabelas: TTable;
    gtbTabRela: TTable;
    gtbCampos:  TTable;
    gtbIndices: TTable;

    function ImportarArquivo(const plCodigoLayout: Longint;
                             const psNomeArquivo: string): Boolean;
//    function ImportarArquivoAcc(const psNomeArquivo: string): Boolean;

  end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  dmRetorno: TdmRetorno;

  msSql: String;

implementation

uses uniDados, uniConst, uniImprimir, uniProcessos, uniGlobal;

{$R *.DFM}

{*----------------------------------------------------------------------
 * TdmImportar.AbrirArquivo    Abre um arquivo texto
 *
 *----------------------------------------------------------------------}
function TdmRetorno.AbrirArquivo(const sNomeArquivo: string): TTable;
var
  tbTempArquivo: TTable;
begin
  { Inicializa variavel }
  tbTempArquivo := nil;
  try
    { Cria um novo objeto para arquivo texto }
    tbTempArquivo := TTable.Create(Self);
//    tbTempArquivo.DatabaseName := ARQUIVOS_DIR;
    tbTempArquivo.TableName := sNomeArquivo;
    tbTempArquivo.TableType := ttASCII;
    tbTempArquivo.Open;
  except
    { se ocorreu erro libera o objeto da memoria }
    tbTempArquivo.Free;
    tbTempArquivo := nil;
    dmDados.ErroTratar('AbrirArquivo - UNIDADOS.PAS');
  end;
  Result := tbTempArquivo;
end;

{*----------------------------------------------------------------------
 * TdmImportar.ImportarArquivo    Importar os dados de um arquivo texto
 *                              para a tabela correspondente
 *----------------------------------------------------------------------}
function TdmRetorno.ImportarArquivo(const plCodigoLayout: Longint;
                                    const psNomeArquivo: string): Boolean;
var
  lwConta:        Word;
  iConta:         Integer;
  tbArquivo:      TTable;
  vCampo:         Variant;
  liConexao: Integer;
  liConexaoRetorno: Integer;
  liConexaoRetornoErro: Integer;
  liConexaoLayout: Integer;
  liConexaoRecebimento: Integer;
  liContaRet: Integer;
  lsCampo:        String;
  lsTipo:         String;
  lbFimLinha:     Boolean;
  liInicio:       Integer;
  liTamanho:      Integer;
  lsValorRecebido:Currency;
  lsDataPago:     TDate;
  lsOcorrencia:   String;
  lsCartorio:     String;
  lsOcorrenciaOK: String;
  lsRegistro:     String;
  lsMotivoRej:    String;
  lsNossoNumero:  String;
  lsNumeroDocumento: String;
  lsCodigoPag:    String;
  lbAchou:        Boolean;
  lbErro:         Boolean;
  lvValorVenc:    Variant;
  lvDataVenc:     Variant;
  lsMotivo:       String;
  lsTipoTaxa:     String;
  lsTipoRec:      String;
  lsTipoDoc:      String;
  lbPago: Boolean;
  lsCampoValorRecebido: String;
  lsCampoDataCredito: String;
  lsCampoDataPago: String;
  lsBanco: string;
  lsDataCredito: TDate;
  llParcQuitadas: LongInt;
  llParcRejeitadas: LongInt;
  lcValorQuitadas: Currency;
  lcValorRejeitadas: Currency;
  llPrazo: Longint;
//  lnMulta: Double;
  lnDesconto:  Double;
  lnDescontoU: Double;
  lnDescontoS: Double;
  lnDescontoB: Double;
  lbSeguro:    Boolean;
  lbServico:   Boolean;
  lnRefStand:  Double;
begin
  iConta := 0;
  Result := False;
  { Inicia uma transacao }

  try
    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    { zera contadores}
    llParcQuitadas    := 0;
    llParcRejeitadas  := 0;
    lcValorQuitadas   := 0;
    lcValorRejeitadas := 0;

    {Abre o Arquivo}
    tbArquivo := AbrirArquivo(psNomeArquivo);
    { se ano consegui abrir o arquivo}
    if tbArquivo = nil then
    begin
      { apresenta mensagem de altualizacao feita}
      dmDados.MensagemExibir('', 4600);
      Exit;
    end;

    { Exclui a Tabela de Retorno}
    gaParm[0] := 'TempRetorno';
    msSql := dmDados.SqlVersao('ARQ_0004', gaParm);
    dmDados.ExecutarSQL(msSql);

    { Exclui a Tabela de Retorno Erro}
(*    gaParm[0] := 'TempRetornoErro';
    msSql := dmDados.SqlVersao('ARQ_0004', gaParm);
    dmDados.ExecutarSQL(msSql);
*)
    { abre a conexao de Retorno}
    gaParm[0] := '* ';
    gaParm[1] := 'TempRetorno';
    msSql := dmDados.SqlVersao('ARQ_0005', gaParm);
    liConexaoRetorno := dmDados.ExecutarSelect(msSql);

    { Abre a conexao de Retorno Erro}
    gaParm[0] := '* ';
    gaParm[1] := 'TempRetornoErro';
    msSql := dmDados.SqlVersao('ARQ_0005', gaParm);
    liConexaoRetornoErro := dmDados.ExecutarSelect(msSql);

    { Abre a conexao do Layout}
    gaParm[0] := glCodigoLayout;
    msSql := dmDados.SqlVersao('ARQ_0002', gaParm);
    liConexaoLayout := dmDados.ExecutarSelect(msSql);

    { atualiza a barra de progresso }
    formProcessos.prbRetBanco.Min := 0;
    formProcessos.prbRetBanco.Max := tbArquivo.RecordCount;

    lsOcorrenciaOK := dmDados.ConfigValor('Ocorrencia_' + IntToStr(plCodigoLayout));
    lsCartorio := dmDados.ConfigValor('Cartorio_' + IntToStr(plCodigoLayout));
    llPrazo := StrToInt(dmDados.ConfigValor('Prazo_' + IntToStr(plCodigoLayout)));
    lsRegistro := dmDados.ConfigValor('Registro_' + IntToStr(plCodigoLayout));
//    lnDescontoU := StrToFloat(dmDados.ConfigValor('Desc_Manut_Ano'));
//    lnDescontoS := StrToFloat(dmDados.ConfigValor('Desc_Manut_Seme'));
//    lnDescontoB := StrToFloat(dmDados.ConfigValor('Desc_Manut_Bime'));

    { Posiciona no primeiro registro }
    tbArquivo.First;
    { se nao for final de arquivo}
    if not tbArquivo.EOF then
    begin
      { Posiciona na segunda linha}
      tbArquivo.Next;
      if not tbArquivo.EOF then
        { Pega o conteudo do campo }
        vCampo := tbArquivo.Fields[0].AsVariant;
    end;
    { Enquanto tiver registros }
    while not tbArquivo.EOF do
    begin
      { se esta na ultima linha}
      if Copy(VarToStr(vCampo), 1, 1) = '9' then
        Break;

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { Limpa o vetor}
      for lwConta := 0 to High(mrRetorno) do
      begin
        mrRetorno[lwConta].bEmUso := False;
        mrRetorno[lwConta].sValor := VAZIO;
        mrRetorno[lwConta].sCampo := VAZIO;
        mrRetorno[lwConta].sTipo := VAZIO;
      end;
      liContaRet := 0;

      { inicializa variaveis }
      lsNossoNumero := '0';
      lsCodigoPag := '0';
      lsNumeroDocumento := '0';
      lsValorRecebido := 0;
      lsDataCredito := 0;
      lsDataPago := 0;

      { Primeiro da Tabela Layout}
      dmDados.Primeiro(liConexaoLayout);
      { Enquanbto tiver registros}
      while not dmDados.Status(liConexaoLayout, IOSTATUS_EOF) do
      begin
        { le os dados da tabela Layout}
        lsCampo := dmDados.ValorColuna(liConexaoLayout, 'CONTEUDO', miIndCol[0]);
        lsTipo := dmDados.ValorColuna(liConexaoLayout, 'TIPO', miIndCol[1]);
        lbFimLinha := dmDados.ValorColuna(liConexaoLayout, 'FIMLINHA', miIndCol[2]);
        liInicio := dmDados.ValorColuna(liConexaoLayout, 'PosicaoInicial', miIndCol[3]);
        liTamanho := dmDados.ValorColuna(liConexaoLayout, 'Tamanho', miIndCol[4]);
        { se tiver campo para guardar}
        if lsCampo <> VAZIO then
        begin
          { guarda os Dados}
          mrRetorno[liContaRet].bEmUso := True;
          mrRetorno[liContaRet].sValor := Copy(VarToStr(vCampo), liInicio, liTamanho);
          mrRetorno[liContaRet].sCampo := lsCampo;
          mrRetorno[liContaRet].sTipo := lsTipo;

          { se for registro checa com o detalhe }
          if mrRetorno[liContaRet].sCampo = CAMPO_REGISTRO then
            { se nao eh detalhe sai do loop }
            if AnsiPos(mrRetorno[liContaRet].sValor, lsRegistro) = 0 then
              break;

          { se for nosso numero}
          if mrRetorno[liContaRet].sCampo = CAMPO_NOSSONUMERO then
          begin
            if Trim(mrRetorno[liContaRet].sValor) <> VAZIO then
            begin
              lsNossoNumero := mrRetorno[liContaRet].sValor;
              lsCodigoPag := Copy(mrRetorno[liContaRet].sValor, Length(mrRetorno[liContaRet].sValor) - 6, 6);
            end
            else
            begin
              lsNossoNumero := '0';
              lsCodigoPag := '0';
            end;
          end;
          { se for numero documento }
          if mrRetorno[liContaRet].sCampo = CAMPO_NUMERODOCUMENTO then
          begin
            if Trim(mrRetorno[liContaRet].sValor) <> VAZIO then
            begin
              lsNumeroDocumento := mrRetorno[liContaRet].sValor;
              lsCodigoPag := Copy(mrRetorno[liContaRet].sValor, Length(mrRetorno[liContaRet].sValor) - 6, 6);
            end
            else
            begin
              lsNumeroDocumento := '0';
              lsCodigoPag := '0';
            end;
          end;
          { se for Valor}
          if mrRetorno[liContaRet].sCampo = CAMPO_VALORRECEBIDO then
          begin
            if Trim(mrRetorno[liContaRet].sValor) <> VAZIO then
              mrRetorno[liContaRet].sValor := StrToInt(mrRetorno[liContaRet].sValor)/100
            else
              mrRetorno[liContaRet].sValor := 0;
            lsValorRecebido := mrRetorno[liContaRet].sValor;
            lsCampoValorRecebido := mrRetorno[liContaRet].sCampo;

          end;
          { se for DataCredito}
          if mrRetorno[liContaRet].sCampo = CAMPO_DATACREDITO then
          begin
            if Trim(mrRetorno[liContaRet].sValor) <> VAZIO then
            begin
//              if StrToInt(Copy(mrRetorno[liContaRet].sValor,5,2)) <= 50 then
//                mrRetorno[liContaRet].sValor := StrToDate(Copy(mrRetorno[liContaRet].sValor,1,2) + '/' + Copy(mrRetorno[liContaRet].sValor,3,2) + '/20' + Copy(mrRetorno[liContaRet].sValor,5,2))
//              else
//                mrRetorno[liContaRet].sValor := StrToDate(Copy(mrRetorno[liContaRet].sValor,1,2) + '/' + Copy(mrRetorno[liContaRet].sValor,3,2) + '/19' + Copy(mrRetorno[liContaRet].sValor,5,2));
              if (mrRetorno[liContaRet].sValor <> '000000') and
                 (mrRetorno[liContaRet].sValor <> '00000000') then
              begin
                mrRetorno[liContaRet].sValor := StrToDate(Copy(mrRetorno[liContaRet].sValor,1,2) + '/' + Copy(mrRetorno[liContaRet].sValor,3,2) + '/' + Copy(mrRetorno[liContaRet].sValor,5,4));
                lsDataCredito := mrRetorno[liContaRet].sValor;
              end
              else
              begin
                mrRetorno[liContaRet].sValor := VAZIO;
                lsDataCredito := 0;
              end;
            end
            else
            begin
              mrRetorno[liContaRet].sValor := VAZIO;
              lsDataCredito := 0;
            end;
            lsCampoDataCredito := mrRetorno[liContaRet].sCampo;
          end;
          { se for DataPagamento}
          if mrRetorno[liContaRet].sCampo = CAMPO_DATAPAGO then
          begin
            if Trim(mrRetorno[liContaRet].sValor) <> VAZIO then
            begin
//              if StrToInt(Copy(mrRetorno[liContaRet].sValor,5,2)) <= 50 then
//                mrRetorno[liContaRet].sValor := StrToDate(Copy(mrRetorno[liContaRet].sValor,1,2) + '/' + Copy(mrRetorno[liContaRet].sValor,3,2) + '/20' + Copy(mrRetorno[liContaRet].sValor,5,2))
//              else
//                mrRetorno[liContaRet].sValor := StrToDate(Copy(mrRetorno[liContaRet].sValor,1,2) + '/' + Copy(mrRetorno[liContaRet].sValor,3,2) + '/19' + Copy(mrRetorno[liContaRet].sValor,5,2));
                mrRetorno[liContaRet].sValor := StrToDate(Copy(mrRetorno[liContaRet].sValor,1,2) + '/' + Copy(mrRetorno[liContaRet].sValor,3,2) + '/' + Copy(mrRetorno[liContaRet].sValor,5,4));
              lsDataPago := mrRetorno[liContaRet].sValor;
            end
            else
            begin
              mrRetorno[liContaRet].sValor := VAZIO;
              lsDataPago := 0;
            end;
            lsCampoDataPago := mrRetorno[liContaRet].sCampo;
          end;
          { se for banco}
          if mrRetorno[liContaRet].sCampo = CAMPO_BANCO then
            if Trim(mrRetorno[liContaRet].sValor) <> VAZIO then
              lsBanco := mrRetorno[liContaRet].sValor;
          { se for Ocorrencia}
          if mrRetorno[liContaRet].sCampo = CAMPO_OCORRENCIA then
              lsOcorrencia := Trim(mrRetorno[liContaRet].sValor);
          { se for Motivo Rejeicao}
          if mrRetorno[liContaRet].sCampo = CAMPO_MOTIVOREJ then
              lsMotivoRej := Trim(mrRetorno[liContaRet].sValor);

          liContaRet := liContaRet + 1;

        end;

        { se for final de linha}
        if lbFimLinha then
        begin
          { Avanca um registro no arquivo texto }
          tbArquivo.Next;
          { se nao for final de arquivo}
          if not tbArquivo.EOF then
          begin
            { Pega o conteudo do campo }
            vCampo := tbArquivo.Fields[0].AsVariant;
            { atualza barra de progresso }
            iConta := iConta + 1;
            formProcessos.prbRetBanco.Position := iConta;
          end;
        end;

        if (dmDados.Proximo(liConexaoLayout) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
      end;

      { se for registro checa com o detalhe }
      if mrRetorno[liContaRet].sCampo = CAMPO_REGISTRO then
      begin
        { se nao eh detalhe vai para proximo registro }
        if AnsiPos(mrRetorno[liContaRet].sValor, lsRegistro) = 0 then
        begin
          { Avanca um registro no arquivo texto }
          tbArquivo.Next;
          { se nao for final de arquivo}
          if not tbArquivo.EOF then
          begin
            { Pega o conteudo do campo }
            vCampo := tbArquivo.Fields[0].AsVariant;
            { atualza barra de progresso }
            iConta := iConta + 1;
            formProcessos.prbRetBanco.Position := iConta;
          end;
          continue;
        end;
      end;

      lbAchou := False;
      lbSeguro := False;
      lbServico := False;
      lsTipoTaxa := VAZIO;
      lsTipoDoc := VAZIO;
      lnRefStand := 1;

      { procura p/ ver se acha a parcela}
//      gaParm[0] := StrToInt(lsNossoNumero);
//      gaParm[0] := StrToFloat(lsNossoNumero);
      gaParm[0] := lsNossoNumero;
      msSql := dmDados.SqlVersao('ARQ_0006', gaParm);
      liConexaoRecebimento := dmDados.ExecutarSelect(msSql);
      { se achou - Parcela }
      if not dmDados.Status(liConexaoRecebimento, IOSTATUS_EOF) then
      begin
        lbAchou := True;
        lsTipoDoc := dmDados.ValorColuna(liConexaoRecebimento, CAMPO_REC_TIPO_DOC, miIndCol[6]);
        if lsTipoDoc = 'T' then
        begin
          lsTipoTaxa := dmDados.ValorColuna(liConexaoRecebimento, CAMPO_REC_TIPO_TAXA, miIndCol[7]);
          if lsTipoTaxa = VAZIO then
            lsTipoTaxa := TAXA_UNICA;
        end;
      end
      else
      begin
        { Fecha a Conexao}
        dmDados.FecharConexao(liConexaoRecebimento);
        { abre novamente}
        msSql := dmDados.SqlVersao('ARQ_0012', gaParm);
        liConexaoRecebimento := dmDados.ExecutarSelect(msSql);
        { se Achou - Taxa Semestral }
        if not dmDados.Status(liConexaoRecebimento, IOSTATUS_EOF) then
        begin
          { Achou}
          lbAchou := True;
          lsTipoDoc := 'T';
          lsTipoTaxa := TAXA_SEMESTRAL;
        end
        else
        begin
          { Fecha a Conexao}
          dmDados.FecharConexao(liConexaoRecebimento);
          { abre novamente}
          msSql := dmDados.SqlVersao('ARQ_0013', gaParm);
          liConexaoRecebimento := dmDados.ExecutarSelect(msSql);
          { se Achou - Taxa Bimestral }
          if not dmDados.Status(liConexaoRecebimento, IOSTATUS_EOF) then
          begin
            { Achou}
            lbAchou := True;
            lsTipoDoc := 'T';
            lsTipoTaxa := TAXA_BIMESTRAL;
          end
          else
          begin
            { Fecha a Conexao}
            dmDados.FecharConexao(liConexaoRecebimento);
            { abre novamente}
            msSql := dmDados.SqlVersao('ARQ_0014', gaParm);
            liConexaoRecebimento := dmDados.ExecutarSelect(msSql);
            { se Achou - Servico }
            if not dmDados.Status(liConexaoRecebimento, IOSTATUS_EOF) then
            begin
              { Achou}
              lbAchou := True;
              lbServico := True;
            end
            else
            begin
              { Fecha a Conexao}
              dmDados.FecharConexao(liConexaoRecebimento);
              { abre novamente}
              msSql := dmDados.SqlVersao('ARQ_0008', gaParm);
              liConexaoRecebimento := dmDados.ExecutarSelect(msSql);
              { se Achou - Seguro }
              if not dmDados.Status(liConexaoRecebimento, IOSTATUS_EOF) then
              begin
                { Achou}
                lbAchou := True;
                lbSeguro := True;
              end
              else
              begin
                { Fecha a Conexao}
                dmDados.FecharConexao(liConexaoRecebimento);
              end;
            end;
          end;
        end;
      end;

      { se achou}
      if lbAchou and (AnsiPos(lsOcorrencia,lsOcorrenciaOK)<>0) then
      begin
        for lwConta := 0 to High(miIndCol) do
          miIndCol[lwConta] := IOPTR_NOPOINTER;

        { pega os Dados do recebimento}
        lsCodigoPag := dmDados.ValorColuna(liConexaoRecebimento, CAMPO_REC_CODIGO, miIndCol[0]);
        { se for taxa procura o desconto }
        if lsTipoDoc = 'T' then
        begin
          gaParm[0] := lsCodigoPag;
          msSql := dmDados.SqlVersao('NEC_0026', gaParm);
          liConexao := dmDados.ExecutarSelect(msSql);
          { se achou - Tipo Lote }
          if not dmDados.Status(liConexao, IOSTATUS_EOF) then
          begin
            lnDescontoU := dmDados.ValorColuna(liConexao, CAMPO_UNICA_DESCONTO, miIndCol[7]);
            lnDescontoS := dmDados.ValorColuna(liConexao, CAMPO_SEMESTRAL_DESCONTO, miIndCol[8]);
            lnDescontoB := dmDados.ValorColuna(liConexao, CAMPO_BIMESTRAL_DESCONTO, miIndCol[9]);
          end;
          { Fecha a Conexao}
          dmDados.FecharConexao(liConexao);
        end;
        if lsTipoTaxa = TAXA_UNICA then
        begin
          lvValorVenc := StrToFloat(dmDados.TirarSeparador(FormatFloat(MK_VALOR, dmDados.ValorColuna(liConexaoRecebimento, CAMPO_REC_VLR_VENC, miIndCol[1]))));
          lnDesconto := lnDescontoU;
        end
        else if lsTipoTaxa = TAXA_SEMESTRAL then
        begin
          lvValorVenc := StrToFloat(dmDados.TirarSeparador(FormatFloat(MK_VALOR, dmDados.ValorColuna(liConexaoRecebimento, CAMPO_REC_VLR_VENC1, miIndCol[4]))));
          lnDesconto := lnDescontoS;
        end
        else if lsTipoTaxa = TAXA_BIMESTRAL then
        begin
          lvValorVenc := StrToFloat(dmDados.TirarSeparador(FormatFloat(MK_VALOR, dmDados.ValorColuna(liConexaoRecebimento, CAMPO_REC_VLR_VENC2, miIndCol[5]))));
          lnDesconto := lnDescontoB;
        end
        else
        begin
          lvValorVenc := StrToFloat(dmDados.TirarSeparador(FormatFloat(MK_VALOR, dmDados.ValorColuna(liConexaoRecebimento, CAMPO_REC_VLR_VENC, miIndCol[1]))));
          lnDesconto := 0.01;
        end;
        { desconto nao pode ser zero }
        if lnDesconto = 0 then
          lnDesconto := 0.01;

        lvDataVenc := dmDados.ValorColuna(liConexaoRecebimento, CAMPO_REC_DATA_VENC, miIndCol[2]);
        lbPago := (dmDados.ValorColuna(liConexaoRecebimento, CAMPO_REC_PAGTO, miIndCol[3]) = 'S');
        lsNumeroDocumento := lsCodigoPag;

        { pega o tipo do lote }
        gaParm[0] := lsCodigoPag;
        msSql := dmDados.SqlVersao('NEC_0025', gaParm);
        liConexao := dmDados.ExecutarSelect(msSql);
        { se achou - Tipo Lote }
        if not dmDados.Status(liConexao, IOSTATUS_EOF) then
        begin
          lnRefStand := dmDados.ValorColuna(liConexao, CAMPO_REC_REF_STAND, miIndCol[6]);
        end;
        { Fecha a Conexao}
        dmDados.FecharConexao(liConexao);
        { atualiza desconto }
        lnDesconto := lnDesconto * lnRefStand;

        { Se foi pago}
        if lbPago then
        begin
          lsMotivo := 'Ja foi pago.';
          lbErro := True;
        end
        else
        begin
          { Se Data for menor}
          if ((lvDataVenc + llPrazo) < lsDataPago) and (lvDataVenc <> 0) then
          begin
            lsMotivo := 'Data Receb. > que Venc.';
            lbErro := True;
          end
          else
          begin
            { Se Valor for menor}
//            if lvValorVenc > (lsValorRecebido + 0.01) then
            if lvValorVenc > (lsValorRecebido + lnDesconto) then
            begin
              lsMotivo := 'Valor Receb. < que Venc.';
              lbErro := True;
            end
            else
              { Achou}
              lbErro := False;
          end;
        end;

        for lwConta := 0 to High(miIndCol) do
          miIndCol[lwConta] := IOPTR_NOPOINTER;

        { grava na tabela de Retorno}
        dmDados.gsRetorno := dmDados.AtivaModo(liConexaoRetorno, IOMODE_INSERT);
        liContaRet := 0;
        { Emaubnto tiver itens}
        while mrRetorno[liContaRet].bEmUso do
        begin
          { se nao for registro }
          if mrRetorno[liContaRet].sCampo <> CAMPO_REGISTRO then
          begin
            {faz a atualizacao}
            if mrRetorno[liContaRet].sCampo = CAMPO_NUMERODOCUMENTO then
              dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, mrRetorno[liContaRet].sCampo, miIndCol[liContaRet], lsNumeroDocumento)
            else
              dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, mrRetorno[liContaRet].sCampo, miIndCol[liContaRet], mrRetorno[liContaRet].sValor);
          end;

          liContaRet := liContaRet + 1;
        end;
        { faz a alteracao}
        dmDados.gsRetorno := dmDados.Incluir(liConexaoRetorno, 'TempRetorno');

        { se nao teve erro}
        if not lbErro then
        begin
          { atualiza pagamento}
          dmDados.gsRetorno := dmDados.AtivaModo(liConexaoRecebimento, IOMODE_EDIT);

          for lwConta := 0 to High(miIndCol) do
            miIndCol[lwConta] := IOPTR_NOPOINTER;

          { se nao tem data de credito usa a do recebimento }
          if lsDataCredito = 0 then
//            lsDataCredito := lsDataPago;
            lsDataCredito := lsDataPago + 1;

          {faz a atualizacao}
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRecebimento, CAMPO_REC_VLR_RECEB, miIndCol[0], lsValorRecebido);
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRecebimento, CAMPO_REC_DATA_CREDITO, miIndCol[1], lsDataCredito);
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRecebimento, CAMPO_REC_DATA_RECEB, miIndCol[2], lsDataPago);
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRecebimento, CAMPO_REC_PAGTO, miIndCol[3], 'S');
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRecebimento, CAMPO_REC_CAIXA_BANC, miIndCol[4], 'B');
          if lsTipoDoc = 'T' then
          begin
            dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRecebimento, CAMPO_REC_TIPO_TAXA, miIndCol[5], lsTipoTaxa);
            lsTipoRec := dmDados.ConfigValor('Receb_Manut_' + lsTipoTaxa);
          end
          else
          begin
            lsTipoRec := 'C';
          end;
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRecebimento, CAMPO_REC_TIPO_REC, miIndCol[6], lsTipoRec);
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRecebimento, 'USUARIO', miIndCol[7], gsNomeUsuario);

          { faz a alteracao}
          if lbSeguro then
            dmDados.gsRetorno := dmDados.Alterar(liConexaoRecebimento, TABELA_REC_SEGU, VAZIO)
          else if lbServico then
            dmDados.gsRetorno := dmDados.Alterar(liConexaoRecebimento, TABELA_REC_SERV, VAZIO)
          else
            dmDados.gsRetorno := dmDados.Alterar(liConexaoRecebimento, TABELA_REC, VAZIO);

          {incrementa contadores}
          llParcQuitadas := llParcQuitadas + 1;
          lcValorQuitadas := lcValorQuitadas + lsValorRecebido;

        end
        else
        begin
          for lwConta := 0 to High(miIndCol) do
            miIndCol[lwConta] := IOPTR_NOPOINTER;

          { grava na tabela de Retorno Erro}
          dmDados.gsRetorno := dmDados.AtivaModo(liConexaoRetornoErro, IOMODE_INSERT);
          liContaRet := 0;
          { Emaubnto tiver itens}
          while mrRetorno[liContaRet].bEmUso do
          begin
            { se nao for registro }
            if mrRetorno[liContaRet].sCampo <> CAMPO_REGISTRO then
            begin
              {faz a atualizacao}
              if mrRetorno[liContaRet].sCampo = CAMPO_NUMERODOCUMENTO then
                dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, mrRetorno[liContaRet].sCampo, miIndCol[liContaRet], lsNumeroDocumento)
              else
                dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, mrRetorno[liContaRet].sCampo, miIndCol[liContaRet], mrRetorno[liContaRet].sValor);
            end;

            liContaRet := liContaRet + 1;
          end;
          { Motivo}
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, 'MotivoErro', miIndCol[liContaRet], lsMotivo);
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, 'Arquivo', miIndCol[liContaRet + 1], ExtractFileName(psNomeArquivo));
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, 'DataBaixa', miIndCol[liContaRet + 2], Date);
          { faz a alteracao}
          dmDados.gsRetorno := dmDados.Incluir(liConexaoRetornoErro, 'TempRetornoErro');

          {incrementa contadores}
          llParcRejeitadas := llParcRejeitadas + 1;
          lcValorRejeitadas := lcValorRejeitadas + lsValorRecebido;

        end;
      end
      else
      begin

        for lwConta := 0 to High(miIndCol) do
          miIndCol[lwConta] := IOPTR_NOPOINTER;

        { grava na tabela de Retorno Erro}
        dmDados.gsRetorno := dmDados.AtivaModo(liConexaoRetornoErro, IOMODE_INSERT);
        liContaRet := 0;
        { Emaubnto tiver itens}
        while mrRetorno[liContaRet].bEmUso do
        begin
          { se nao for registro }
          if mrRetorno[liContaRet].sCampo <> CAMPO_REGISTRO then
          begin
            {faz a atualizacao}
            dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, mrRetorno[liContaRet].sCampo, miIndCol[liContaRet], mrRetorno[liContaRet].sValor);
          end;

          liContaRet := liContaRet + 1;
        end;
        { Motivo}
        if (AnsiPos(lsOcorrencia,lsOcorrenciaOK) <> 0) then
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, 'MotivoErro', miIndCol[liContaRet], 'Parcela nao encontrada')
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, 'MotivoErro', miIndCol[liContaRet], lsOcorrencia + ' - ' + lsMotivoRej);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, 'Arquivo', miIndCol[liContaRet + 1], ExtractFileName(psNomeArquivo));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetornoErro, 'DataBaixa', miIndCol[liContaRet + 2], Date);
        { faz a alteracao}
        dmDados.gsRetorno := dmDados.Incluir(liConexaoRetornoErro, 'TempRetornoErro');

        {incrementa contadores}
        llParcRejeitadas := llParcRejeitadas + 1;
        lcValorRejeitadas := lcValorRejeitadas + lsValorRecebido;

      end;
      { Fecha a Conexao}
      dmDados.FecharConexao(liConexaoRecebimento);

    end;
    formProcessos.prbRetBanco.Position := 0;
    {fecha o arquivo}
    dmRetorno.FecharArquivo(tbArquivo);
    dmDados.FecharConexao(liConexaoRetorno);
    dmDados.FecharConexao(liConexaoRetornoErro);
    dmDados.FecharConexao(liConexaoLayout);

    { salva na tabela de baixa}
    gaParm[0] := '* ';
    gaParm[1] := 'zRetornoBaixa';
    msSql := dmDados.SqlVersao('ARQ_0005', gaParm);
    liConexaoRetorno := dmDados.ExecutarSelect(msSql);
    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    { grava na tabela de Retorno Erro}
    dmDados.gsRetorno := dmDados.AtivaModo(liConexaoRetorno, IOMODE_INSERT);
    {faz a atualizacao}
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, 'Arquivo', miIndCol[0], ExtractFileName(psNomeArquivo));
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, 'Data', miIndCol[1], Date);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, 'ParcelasQuitadas', miIndCol[2], llParcQuitadas);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, 'ValorQuitadas', miIndCol[3], lcValorQuitadas);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, 'ParcelasRejeitadas', miIndCol[4], llParcRejeitadas);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, 'ValorRejeitadas', miIndCol[5], lcValorRejeitadas);
    { faz a Inclusao}
    dmDados.gsRetorno := dmDados.Incluir(liConexaoRetorno, 'zRetornoBaixa ');
    {fecha o arquivo}
    dmDados.FecharConexao(liConexaoRetorno);

     { imprime retorno}
//    formImprimir.rptRelatorio.report := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Retorno.rpt';
    formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Retorno.rpt';
//    formImprimir.rptRelatorio.Destination := crptToWindow;
//    formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
    formImprimir.rptRelatorio.Connect.ServerName := gsDbConnect;
    formImprimir.rptRelatorio.Connect.UserID := gsDbUserName;
    formImprimir.rptRelatorio.Connect.Password := gsDbPassWord;

//    formImprimir.rptRelatorio.Formulas[0] := 'NOMEARQUI='''+ ExtractFileName(psNomeArquivo) +'''';
    formImprimir.rptRelatorio.Formulas.ByName('NOMEARQUI').Formula.Text := ExtractFileName(psNomeArquivo);
//    formImprimir.rptRelatorio.Formulas[1] := 'BANCO='''+ lsBanco +'''';
    formImprimir.rptRelatorio.Formulas.ByName('BANCO').Formula.Text := lsBanco;
//    formImprimir.rptRelatorio.Formulas[2] := VAZIO;
    formImprimir.rptRelatorio.Selection.Formula.Text := VAZIO;
    { Assiona o Relatorio}
//    formImprimir.rptRelatorio.Action := 1;
    formImprimir.rptRelatorio.Show;
    { imprime retorno Erro}
    formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\RetornoErro.rpt';
//    formImprimir.rptRelatorio.Destination := crptToWindow;
//    formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
    formImprimir.rptRelatorio.Connect.ServerName := gsDbConnect;
    formImprimir.rptRelatorio.Connect.UserID := gsDbUserName;
    formImprimir.rptRelatorio.Connect.Password := gsDbPassWord;

//    formImprimir.rptRelatorio.Formulas[0] := 'NOMEARQUI='''+ ExtractFileName(psNomeArquivo) +'''';
//    formImprimir.rptRelatorio.Formulas[1] := 'BANCO='''+ lsBanco +'''';
    formImprimir.rptRelatorio.Formulas.ByName('NOMEARQUI').Formula.Text := ExtractFileName(psNomeArquivo);
    formImprimir.rptRelatorio.Formulas.ByName('BANCO').Formula.Text := lsBanco;
//    formImprimir.rptRelatorio.Formulas[2] := VAZIO;
    formImprimir.rptRelatorio.Selection.Formula.Text := VAZIO;

    { Assiona o Relatorio}
    formImprimir.rptRelatorio.Show;
    { imprime retorno}
    formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Retornop.rpt';
//    formImprimir.rptRelatorio.Destination := crptToWindow;
//    formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
//    formImprimir.rptRelatorio.Formulas[0] := 'NOMEARQUI='''+ ExtractFileName(psNomeArquivo) +'''';
//    formImprimir.rptRelatorio.Formulas[1] := 'BANCO='''+ lsBanco +'''';
    formImprimir.rptRelatorio.Formulas.ByName('NOMEARQUI').Formula.Text := ExtractFileName(psNomeArquivo);
    formImprimir.rptRelatorio.Formulas.ByName('BANCO').Formula.Text := lsBanco;
 //   formImprimir.rptRelatorio.Formulas[2] := VAZIO;
    formImprimir.rptRelatorio.Selection.Formula.Text := VAZIO;
    { Assiona o Relatorio}
    formImprimir.rptRelatorio.Show;
    { imprime retorno Erro}
(*    formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\RetornoErros.rpt';
    formImprimir.rptRelatorio.Destination := crptToWindow;
    formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
    formImprimir.rptRelatorio.Formulas[0] := 'NOMEARQUI='''+ ExtractFileName(psNomeArquivo) +'''';
    formImprimir.rptRelatorio.Formulas[1] := 'BANCO='''+ lsBanco +'''';
    formImprimir.rptRelatorio.Formulas[2] := VAZIO;
    { Assiona o Relatorio}
    formImprimir.rptRelatorio.Action := 1;
*)
    Result := True;
  except
    { Cancela a transacao }
    dmDados.ErroTratar('ImportarArquivo - uniDados.PAS');
    Result := False;
  end;
end;

{*----------------------------------------------------------------------
 * TdmImportar.FecharArquivo    Fecha um arquivo texto
 *
 *----------------------------------------------------------------------}
procedure TdmRetorno.FecharArquivo(var tbArquivo: TTable);
begin
  try
    { Fecha o arquivo texto }
    tbArquivo.Close;
    { Libera o objeto da memoria }
    tbArquivo.Free;
  except
    dmDados.ErroTratar('FecharArquivo - uniDados.PAS');
  end;
end;
(*
{*----------------------------------------------------------------------
 * TdmImportar.ImportarArquivoAcc    Importar os dados de um arquivo texto
 *                              para a tabela correspondente
 *----------------------------------------------------------------------}
function TdmRetorno.ImportarArquivoAcc(const psNomeArquivo: string): Boolean;
var
  lwConta:        Word;
  iConta:         Integer;
  tbArquivo:      TTable;
  vCampo:         Variant;
  liConexaoRetorno: Integer;
  liConexaoLayout: Integer;
  liContaRet: Integer;
  lsCampo:        String;
  lsTipo:         String;
  lbFimLinha:     Boolean;
  liInicio:       Integer;
  liTamanho:      Integer;
  lsConvId:       String;
  lsSerie:        String;
  lvItem:         Variant;
  lasServicos: Array[1..30] of string;
  liConexaoServico: Integer;
  liContaServ: Integer;
begin
  iConta := 0;
  Result := False;
  { Inicia uma transacao }

  try
    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    {Abre o Arquivo}
    tbArquivo := AbrirArquivo(psNomeArquivo);
    { se ano consegui abrir o arquivo}
    if tbArquivo = nil then
    begin
      { apresenta mensagem de altualizacao feita}
      dmDados.MensagemExibir('', 4600);
      Exit;
    end;

    { Exclui a Tabela do Acc4000}
    gaParm[0] := 'CsAcc4000 ';
    msSql := dmDados.SqlVersao('CS_0026', gaParm);
    dmDados.ExecutarSQL(msSql);

    { Exclui a Tabela do servicos}
    gaParm[0] := 'CsServicos ';
    msSql := dmDados.SqlVersao('CS_0026', gaParm);
    dmDados.ExecutarSQL(msSql);

    { abre a conexao do Acc4000}
    gaParm[0] := '* ';
    gaParm[1] := 'CsAcc4000 ';
    msSql := dmDados.SqlVersao('CS_0032', gaParm);
    liConexaoRetorno := dmDados.ExecutarSelect(msSql);

    { abre a conexao do Servicos}
    gaParm[0] := '* ';
    gaParm[1] := 'CsServicos ';
    msSql := dmDados.SqlVersao('CS_0032', gaParm);
    liConexaoServico := dmDados.ExecutarSelect(msSql);

    { Abre a conexao do Layout}
    gaParm[0] := glCodigoLayout;
    msSql := dmDados.SqlVersao('ARQ_0002', gaParm);
    liConexaoLayout := dmDados.ExecutarSelect(msSql);

    { atualiza a barra de progresso }
    formProcessos5.prbImpAcc.Min := 0;
    formProcessos5.prbImpAcc.Max := tbArquivo.RecordCount;

    { Posiciona no primeiro registro }
    tbArquivo.First;
    { se nao for final de arquivo}
    if not tbArquivo.EOF then
    begin
      if not tbArquivo.EOF then
        { Pega o conteudo do campo }
        vCampo := tbArquivo.Fields[0].AsVariant;
    end;
    { Enquanto tiver registros }
    while not tbArquivo.EOF do
    begin

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      for lwConta := 1 to High(lasServicos) do
        lasServicos[lwConta] := VAZIO;

      { Limpa o vetor}
      for lwConta := 0 to High(mrRetorno) do
      begin
        mrRetorno[lwConta].bEmUso := False;
        mrRetorno[lwConta].sValor := VAZIO;
        mrRetorno[lwConta].sCampo := VAZIO;
        mrRetorno[lwConta].sTipo := VAZIO;
      end;
      liContaRet := 0;
      liContaServ := 1;

      { Primeiro da Tabela Layout}
      dmDados.Primeiro(liConexaoLayout);
      { Enquanbto tiver registros}
      While not dmDados.Status(liConexaoLayout, IOSTATUS_EOF) do
      begin
        { le os dados da tabela Layout}
        lsCampo := dmDados.ValorColuna(liConexaoLayout, 'CONTEUDO', miIndCol[0]);
        lsTipo := dmDados.ValorColuna(liConexaoLayout, 'TIPO', miIndCol[1]);
        lbFimLinha := dmDados.ValorColuna(liConexaoLayout, 'FIMLINHA', miIndCol[2]);
        liInicio := dmDados.ValorColuna(liConexaoLayout, 'PosicaoInicial', miIndCol[3]);
        liTamanho := dmDados.ValorColuna(liConexaoLayout, 'Tamanho', miIndCol[4]);
        { se tiver campo para guardar}
        if Trim(lsCampo) <> VAZIO then
        begin
          if Trim(lsCampo) = CAMPO_SERVICO then
          begin
            lasServicos[liContaServ] := Trim(Copy(VarToStr(vCampo), liInicio, liTamanho));

            liContaServ := liContaServ + 1;
          end
          else
          begin
            { guarda os Dados}
            mrRetorno[liContaRet].bEmUso := True;
            mrRetorno[liContaRet].sValor := Trim(Copy(VarToStr(vCampo), liInicio, liTamanho));
            mrRetorno[liContaRet].sCampo := Trim(lsCampo);
            mrRetorno[liContaRet].sTipo := Trim(lsTipo);
            { se for Item}
            if mrRetorno[liContaRet].sCampo = CAMPO_ITEM then
            begin
              if Trim(mrRetorno[liContaRet].sValor) <> VAZIO then
                lvItem := mrRetorno[liContaRet].sValor
              else
                lvItem := VAZIO;
            end;
            { se for nosso numero}
            if mrRetorno[liContaRet].sCampo = CAMPO_CONVID then
            begin
              if Trim(mrRetorno[liContaRet].sValor) <> VAZIO then
                lsConvId := mrRetorno[liContaRet].sValor
              else
                lsConvId := '0';
            end;
            { se for Valor}
            if mrRetorno[liContaRet].sCampo = CAMPO_SERIAL then
            begin
              if Trim(mrRetorno[liContaRet].sValor) <> VAZIO then
                lsSerie := mrRetorno[liContaRet].sValor
              else
                lsSerie := VAZIO;
            end;
            liContaRet := liContaRet + 1;
          end;
        end;

        { se for fianl de linha}
        if lbFimLinha then
        begin
          { Avanca um registro no arquivo texto }
          tbArquivo.Next;
          { se nao for final de arquivo}
          if not tbArquivo.EOF then
          begin
            { Pega o conteudo do campo }
            vCampo := tbArquivo.Fields[0].AsVariant;
            { atualza barra de progresso }
            iConta := iConta + 1;
            formProcessos5.prbImpAcc.Position := iConta;
          end;
        end;

        if (dmDados.Proximo(liConexaoLayout) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
      end;

      try
        lvItem := StrToInt(lvItem);

        { grava na tabela de Retorno}
        dmDados.gsRetorno := dmDados.AtivaModo(liConexaoRetorno, IOMODE_INSERT);
        liContaRet := 0;
        { Emaubnto tiver itens}
        while mrRetorno[liContaRet].bEmUso do
        begin
          {faz a atualizacao}
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, mrRetorno[liContaRet].sCampo, miIndCol[liContaRet], mrRetorno[liContaRet].sValor);

          liContaRet := liContaRet + 1;
        end;
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoRetorno, 'Data', miIndCol[liContaRet], Date);
        { faz a alteracao}
        dmDados.gsRetorno := dmDados.Incluir(liConexaoRetorno, 'CsAcc4000 ');

        for lwConta := 0 to High(miIndCol) do
          miIndCol[lwConta] := IOPTR_NOPOINTER;

        liContaServ := 1;
        { Emquanto tiver itens}
        while Trim(lasServicos[liContaServ]) <> VAZIO do
        begin
          { grava na tabela de Servicos}
          dmDados.gsRetorno := dmDados.AtivaModo(liConexaoServico, IOMODE_INSERT);

          {faz a atualizacao}
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoServico, 'CodigoItem', miIndCol[0], lvItem);
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoServico, 'Codigo', miIndCol[1], liContaServ);
          dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoServico, 'Servico', miIndCol[2], lasServicos[liContaServ]);

          { faz a alteracao}
          dmDados.gsRetorno := dmDados.Incluir(liConexaoServico, 'CsServicos ');

          liContaServ := liContaServ + 1;
        end;

      except

      end;

    end;
    formProcessos5.prbImpAcc.Position := 0;
    {fecha o arquivo}
    dmRetorno.FecharArquivo(tbArquivo);
    dmDados.FecharConexao(liConexaoRetorno);
    dmDados.FecharConexao(liConexaoLayout);

    Result := True;
  except
    { Cancela a transacao }
    dmDados.ErroTratar('ImportarArquivoAcc - uniDados.PAS');
    Result := False;
  end;
end;
*)
end.
