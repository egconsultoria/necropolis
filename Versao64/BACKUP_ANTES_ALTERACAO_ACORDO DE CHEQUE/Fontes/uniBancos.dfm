�
 TFRMBANCO 0�  TPF0	TfrmBancofrmBancoLeft� Top� Width�Height� CaptionfrmBancoColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	Position	poDefaultVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidth�Height� TabOrder  	TGroupBoxgrbBancoLeftTopWidthyHeight� TabOrder  TLabel
lblAgenciaLeftPTop8Width'HeightCaption   Agência  TLabellblBancoLeftTop8WidthHeightCaptionBanco  TLabellblContaLeft� Top8WidthHeightCaptionConta  TLabellblNomeBancoLeft� Top8WidthMHeightCaptionNome do Banco  TLabel
lblNumero1LeftTop`Width.HeightCaption	   Número 1  TLabel
lblNumero2LeftPTop`Width.HeightCaption	   Número 2  TLabellblUltimoNumeroLeft� Top`WidthEHeightCaption   Último Número  TLabellblCobrancaLeft� Top`Width=HeightCaption   Cobrança de  TLabel	lblCodigoLeftTopWidth!HeightCaption   Código  TLabellblDescricaoLeftPTopWidth0HeightCaption   Descrição  	TPaintBoxPntComunicadorLeftTop WidthqHeight� OnPaintPntComunicadorPaint  	TMaskEdit
mskAgenciaLeftPTopHWidth1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskBancoLeftTopHWidth1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskNomeBancoLeft� TopHWidth� HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEdit
mskNumero1LeftToppWidth1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEdit
mskNumero2LeftPToppWidth1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskUltimoNumeroLeft� ToppWidthAHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboCobrancaLeft� ToppWidth� Height
ItemHeightSorted	TabOrder	OnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskContaLeft� TopHWidthAHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEdit	mskCodigoLeftTop Width1HeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtDescricaoLeftPTop WidthHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress     