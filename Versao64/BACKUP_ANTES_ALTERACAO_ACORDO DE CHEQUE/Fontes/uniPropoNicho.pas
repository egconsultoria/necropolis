unit uniPropoNicho;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  ExtCtrls, StdCtrls, uniConst, Mask, OleCtrls, ComCtrls,
  FPSpread_TLB;

const
  EDT_CODIGO: string = 'mskCodigo';

  COL_PARC = 1;
  COL_DATA = 2;
  COL_VALOR = 3;
  COL_VALORVENC = 4;

  VLR_MULTA = 0.02;
  VLR_JUROS = 0.00034;

type
  TfrmPropoNicho = class(TForm)
    pnlFundo: TPanel;
    grpClientes: TGroupBox;
    grpCadastro: TGroupBox;
    pagProponentes: TPageControl;
    tabProponentes: TTabSheet;
    tabOcupacao: TTabSheet;
    tabDebitos: TTabSheet;
    edtProponente_Nome: TEdit;
    Label1: TLabel;
    mskProponente_DataNascimento: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    edtProponente_Profissao: TEdit;
    Label7: TLabel;
    edtProponente_Endereco: TEdit;
    Label8: TLabel;
    mskProponente_CEP: TMaskEdit;
    edtProponente_Estado: TEdit;
    Label9: TLabel;
    edtProponente_Cidade: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    edtProponente_Bairro: TEdit;
    Label12: TLabel;
    edtProponente_DDD: TEdit;
    Label14: TLabel;
    mskProponente_Telefone: TMaskEdit;
    Label15: TLabel;
    edtProponente_Ramal: TEdit;
    Label16: TLabel;
    mskProponente_RG: TMaskEdit;
    Label17: TLabel;
    mskProponente_CPF: TMaskEdit;
    Label28: TLabel;
    edtProponente_Observacao: TEdit;
    sprProponentes: TvaSpread;
    grpContrato: TGroupBox;
    lblCodigo: TLabel;
    mskCodigo: TMaskEdit;
    grpGaveta: TGroupBox;
    Label5: TLabel;
    Label13: TLabel;
    Label6: TLabel;
    mskQuadra: TMaskEdit;
    mskSetor: TMaskEdit;
    mskLote: TMaskEdit;
    sprGavetaPessoas: TvaSpread;
    sprDebitoLote: TvaSpread;
    Label18: TLabel;
    Label21: TLabel;
    edtDebitoLote: TEdit;
    PntComunicador: TPaintBox;
    grpBloqueadaLote: TGroupBox;
    Label26: TLabel;
    Label27: TLabel;
    cboBloqueadaLote: TComboBox;
    mskDataBloqueadaLote: TMaskEdit;
    TabSheet1: TTabSheet;
    sprCheques: TvaSpread;
    Label31: TLabel;
    mskDataProposta: TMaskEdit;
    Label4: TLabel;
    mskLoculo: TMaskEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure cboComboExit(Sender: TObject);
    procedure cboComboKeyPress(Sender: TObject; var Key: Char);
    procedure edtTextoChange(Sender: TObject);
    procedure edtTextoEnter(Sender: TObject);
    procedure edtTextoExit(Sender: TObject);
    procedure sprGridChange(Sender: TObject; Col, Row: Integer);
    procedure sprGridEditChange(Sender: TObject; Col, Row: Integer);
    procedure chkCheckClick(Sender: TObject);
    procedure sprGavetaPessoasDblClick(Sender: TObject; Col, Row: Integer);
    procedure sprProponentesDblClick(Sender: TObject; Col, Row: Integer);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;

    procedure mpValor_Receber;
    procedure mpValor_Total;

  public
    { Public declarations }
  end;

var
  frmPropoNicho: TfrmPropoNicho;

implementation

uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos,
    uniVisualizacaoGavetas, uniEntradaManual, uniContratos, uniFuncoes;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.FormClose(Sender: TObject; var Action: TCloseAction);
var
  liConta:  Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - uniPropoNicho',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;
    gfrmTV := nil;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniPropostaLote');
  end;

  // destroi a janela
  Action := caFree;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, TForm(Sender));

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.FormResize(Sender: TObject);
begin
  if TForm(Sender).WindowState <> wsMinimized Then
  begin
    if (TForm(Sender).Width < pnlFundo.Width + 39) and (TForm(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TForm(Sender).Width := pnlFundo.Width + 39;

    TForm(Sender).Refresh;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
//    mrTelaVar.P.Titulo_Tela := 'Proposta de Lote';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
    untEdit.gp_Edit_EstufaCombos(mrTelaVar.P.Sigla_Tabela, TForm(Sender));

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);

    mpValor_Receber;
    mpValor_Total;

    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniPropostaLote');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 3);

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  {' muda o ponteiro do mouse para ampulheta}
  Screen.cursor := crHOURGLASS;

  lcboCombo := TComboBox(Sender);

  {' se nao for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
  begin
    if lcboCombo.Text <> VAZIO then
    begin
      {' altera��o efetuada}
      untEdit.gp_Edit_Alterado(True, mrTelaVar);
    end;
  end
  else
    If (Not mrTelaVar.Reposicionando) Then
      {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

  {' retorna o ponteiro do mouse para o default}
  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  liConta: Integer;
begin

  lcboCombo := TComboBox(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := lcboCombo.Text;
  giAreaDesfaz := -1;
  if lcboCombo.Items.Count > 0 then
    {'Executa um la�o nos itens comparando-os com o pConteudo}
    for liConta := 0 to lcboCombo.Items.Count - 1 do
      if lcboCombo.Items[liConta] = gsAreaDesfaz then
      begin
        giAreaDesfaz := liConta;
        break;
      end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.cboComboKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.cboComboKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.edtTextoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.edtTextoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = EDT_CODIGO) And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.edtTextoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.edtTextoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.sprGridChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.sprGridChange(Sender: TObject; Col,
  Row: Integer);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.sprGridEditChange       Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.sprGridEditChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
  lgrdGrid.Col := Col;
  if lgrdGrid.CellType = SS_CELL_TYPE_COMBOBOX then
    untCombos.gp_Combo_SpreadSeleciona(lgrdGrid, 0);

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.chkCheckClick       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.chkCheckClick(Sender: TObject);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TfrmProponente.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.PntComunicadorPaint(Sender: TObject);
var
  liComandoMDI: Integer;
begin

  liComandoMDI := giComando_MDI;

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, gfrmTV);

  if (liComandoMDI = TV_COMANDO_IR_PROXIMO) or
     (liComandoMDI = TV_COMANDO_IR_ANTERIOR) or
     (liComandoMDI = TV_COMANDO_IR_PRIMEIRO) or
     (liComandoMDI = TV_COMANDO_PROCURAR) or
     (liComandoMDI = TV_COMANDO_IR_ULTIMO) then
  begin
    mpValor_Receber;
    mpValor_Total;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.cboComboExit(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.edtTextoExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.edtTextoExit(Sender: TObject);
var
  ledtEdit: TMaskEdit;
begin

  ledtEdit := TMaskEdit(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprGavetaPessoasDblClick       Visualiza gavetas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.sprGavetaPessoasDblClick(Sender: TObject; Col,
  Row: Integer);
begin

  { guarda o numero da proposta }
  gsNumProposta := mskCodigo.Text;
  { chama a tela de visual gavetas }
  uniVisualizacaoGavetas.formVisualGaveta.Show;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpValor_Receber       Mostra o valor a receber
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.mpValor_Receber();
var
  liConta: Integer;
  lsParcela: string;
  lsDataVenc: string;
  lsValorVenc: string;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  { Lotes }
  sprDebitoLote.ReDraw := False;
  for liConta := 1 to sprDebitoLote.MaxRows - 1 do
  begin
    sprDebitoLote.Row := liConta;

    { parcela }
    sprDebitoLote.Col := COL_PARC;
    lsParcela := sprDebitoLote.Text;
    { data }
    sprDebitoLote.Col := COL_DATA;
    lsDataVenc := sprDebitoLote.Text;
    { valor }
    sprDebitoLote.Col := COL_VALORVENC;
    lsValorVenc := sprDebitoLote.Text;

    { se ainda nao foi pago }
    if lsParcela = VAZIO then
      break;
    { se nao tem vencimento nao calcula }
    if lsDataVenc = VAZIO then
    begin
      sprDebitoLote.Col := COL_VALOR;
      sprDebitoLote.Text := lsValorVenc;
    end
    else
    begin
      { tolerancia de cinco dias }
      if StrToDate(lsDataVenc) >= (Date - 5) then
      begin
        sprDebitoLote.Col := COL_VALOR;
//        sprDebitoLote.Text := lsValorVenc;
        sprDebitoLote.Text := FormatFloat(MK_VALOR,
                              StrToFloat(dmDados.TirarSeparador(lsValorVenc)) -
                              untFuncoes.gf_Funcoes_SaldoParc(mskCodigo.Text,
                                                            'L',
                                                            lsParcela));
      end
      else
      begin
        sprDebitoLote.Col := COL_VALOR;
        sprDebitoLote.Text := untFuncoes.gf_Funcoes_ValorAtual(mskCodigo.Text,
                                            'L',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela);
        if StrToFloat(dmDados.TirarSeparador(sprDebitoLote.Text)) = 0 then
          sprDebitoLote.Text := lsValorVenc
        else
          sprDebitoLote.Text := FormatFloat(MK_VALOR,
                                StrToFloat(dmDados.TirarSeparador(sprDebitoLote.Text)) -
                                untFuncoes.gf_Funcoes_SaldoParc(mskCodigo.Text,
                                                              'L',
                                                              lsParcela));
      end;
    end;
  end;
  sprDebitoLote.ReDraw := True;

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpValor_Receber       Mostra o valor a receber
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.mpValor_Total();
var
  liConta:      Integer;
  ldDebitoLote: Double;
  ldDebitoGave: Double;
  ldDebitoTaxa: Double;
begin

    { para nao ativar o salvar }
    mrTelaVar.Reposicionando := True;

    ldDebitoLote := 0;
    ldDebitoGave := 0;
    ldDebitoTaxa := 0;

    sprDebitoLote.Col := COL_VALOR;
    for liConta := 1 to sprDebitoLote.MaxRows do
    begin
      sprDebitoLote.Row := liConta;
      if sprDebitoLote.Text <> VAZIO then
        ldDebitoLote := ldDebitoLote + StrToFloat(dmDados.TirarSeparador(sprDebitoLote.Text));
    end;
    edtDebitoLote.Text := FormatFloat(MK_VALOR, ldDebitoLote);

    { para nao ativar o salvar }
    mrTelaVar.Reposicionando := False;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprProponentesDblClick       Visualiza documentos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropoNicho.sprProponentesDblClick(Sender: TObject; Col,
  Row: Integer);
begin

  { guarda o numero da proposta }
  gsNumProposta := mskCodigo.Text;
  { chama a tela de visual gavetas }
  uniContratos.frmImagens.Show;

end;






end.
