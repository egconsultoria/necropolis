unit uniEtiquetaEspecifica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ComCtrls, ExtCtrls, Variants,
  uniConst, OleCtrls, FPSpread_TLB;

const
  EDT_CODIGO: string = 'mskCodigo';

type
  TfrmEtiquetaEspecifica = class(TForm)
    pnlFundo: TPanel;
    grpEtiquetaEspecifica: TGroupBox;
    lblCodigo: TLabel;
    mskCodigo: TMaskEdit;
    PntComunicador: TPaintBox;
    lblProponente_Nome: TLabel;
    edtProponente_Nome: TEdit;
    lblProponente_Endereco: TLabel;
    edtProponente_Endereco: TEdit;
    lblProponente_CEP: TLabel;
    mskProponente_CEP: TMaskEdit;
    lblProponente_Estado: TLabel;
    edtProponente_Estado: TEdit;
    lblProponente_Cidade: TLabel;
    edtProponente_Cidade: TEdit;
    lblProponente_Bairro: TLabel;
    edtProponente_Bairro: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure cboComboExit(Sender: TObject);
    procedure cboComboKeyPress(Sender: TObject; var Key: Char);
    procedure edtTextoChange(Sender: TObject);
    procedure edtTextoEnter(Sender: TObject);
    procedure edtTextoExit(Sender: TObject);
    procedure sprGridChange(Sender: TObject; Col, Row: Integer);
    procedure sprGridEditChange(Sender: TObject; Col, Row: Integer);
    procedure chkCheckClick(Sender: TObject);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;

    procedure mpLimpa_Etiquetas;
    procedure mpMostra_Proponente;
  public
    { Public declarations }
  end;

var
  frmEtiquetaEspecifica: TfrmEtiquetaEspecifica;

implementation

uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos, uniFuncoes;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.FormClose(Sender: TObject; var Action: TCloseAction);
var
  liConta:  Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - uniEtiquetaEspecifica',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;
    gfrmTV := nil;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniEtiquetaEspecifica');
  end;

  // destroi a janela
  Action := caFree;
end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, TForm(Sender));

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.FormResize(Sender: TObject);
begin
  if TForm(Sender).WindowState <> wsMinimized Then
  begin
    if (TForm(Sender).Width < pnlFundo.Width + 39) and (TForm(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TForm(Sender).Width := pnlFundo.Width + 39;

    TForm(Sender).Refresh;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
//    mrTelaVar.P.Titulo_Tela := 'Proposta de Lote';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
    untEdit.gp_Edit_EstufaCombos(mrTelaVar.P.Sigla_Tabela, TForm(Sender));

    { limpa o arquivo de etiquetas }
    mpLimpa_Etiquetas;

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);

    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniEtiquetaEspecifica');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 3);

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
      if lcboCombo.Text <> VAZIO then
      begin
        {' altera��o efetuada}
        untEdit.gp_Edit_Alterado(True, mrTelaVar);
      end;
    end
    else
      If (Not mrTelaVar.Reposicionando) Then
        {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboComboClick - uniEtiquetaEspecifica');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  liConta: Integer;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' guarda o valor original do combo}
    gsAreaDesfaz := lcboCombo.Text;
    giAreaDesfaz := -1;
    if lcboCombo.Items.Count > 0 then
      {'Executa um la�o nos itens comparando-os com o pConteudo}
      for liConta := 0 to lcboCombo.Items.Count - 1 do
        if lcboCombo.Items[liConta] = gsAreaDesfaz then
        begin
          giAreaDesfaz := liConta;
          break;
        end;

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
    end;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboComboEnter - uniEtiquetaEspecifica');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.cboComboKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.cboComboKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.edtTextoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.edtTextoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = EDT_CODIGO) And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    {' atualiza o caption da janela}
    Caption := mrTelaVar.P.Titulo_Tela + ' ' + ledtEdit.Text + ' - Inclus�o';
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.edtTextoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.edtTextoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprGridChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.sprGridChange(Sender: TObject; Col,
  Row: Integer);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.sprGridEditChange       Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.sprGridEditChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
  lgrdGrid.Col := Col;
  if lgrdGrid.CellType = SS_CELL_TYPE_COMBOBOX then
    untCombos.gp_Combo_SpreadSeleciona(lgrdGrid, 0);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.chkCheckClick       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.chkCheckClick(Sender: TObject);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.PntComunicadorPaint(Sender: TObject);
var
  liComandoMDI: Integer;
begin

  liComandoMDI := giComando_MDI;

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, frmEtiquetaEspecifica);

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.edtTextoExit       quando perde o foco
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.edtTextoExit(Sender: TObject);
var
  ledtEdit: TMaskEdit;
  i, j:  Integer;
begin

  ledtEdit := TMaskEdit(Sender);

  if Sender is TMaskEdit then
  begin
    { procura pela entidade }
    i := untTelaVar.gf_TelaVar_Retorna_Tabela(mrTelaVar.P.Sigla_Tabela);
    { se nao achou }
    if i = 0 then
      Exit;

    j := 1;
    { percorre todos os campos da tabela principal }
    while gaLista_Campos[i][j].sSigla_Tabela = gaLista_Parametros[i].Sigla_Tabela do
    begin
      if gaLista_Campos[i][j].sNome_Controle = ledtEdit.Name then
      begin
        if gaLista_Campos[i][j].sAlinha = ALINHA_ZERO then
          ledtEdit.Text := untFuncoes.gf_Zero_Esquerda(ledtEdit.Text, gaLista_Campos[i][j].iTamanho);
        break;
      end;
      j := j + 1;
    end;
  end;

  if ledtEdit.Name = EDT_CODIGO then
    mpMostra_Proponente;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.cboComboExit(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.edtTextoExit       quando perde o foco
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.mpLimpa_Etiquetas();
var
  lsSql: string;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  lsSql := 'Truncate Table Etiqueta';

  dmDados.gsRetorno := dmDados.ExecutarSQL(lsSql);

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;

end;

{*-----------------------------------------------------------------------------
 *  TfrmEtiquetaEspecifica.edtTextoExit       quando perde o foco
 *
 *-----------------------------------------------------------------------------}
procedure TfrmEtiquetaEspecifica.mpMostra_Proponente();
var
  liConexao: Integer;
  liIndCol:  array[0..MAX_NUM_COLUNAS] of Integer;
  lsSql: string;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  { proponente }
  gaParm[0] := 'Nome_Prop, Endereco, Bairro, Cidade, Estado, CEP';
  gaParm[1] := 'Proponen';
  gaParm[2] := 'Num_Prop = ''' + mskCodigo.Text + '''';
  liConexao := 0;
  lsSQL := dmDados.SqlVersao('NEC_0000', gaParm);
  liConexao := dmDados.ExecutarSelect(lsSQL);
  if liConexao = IOPTR_NOPOINTER then
    Exit;

  if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
  begin
    dmDados.gsRetorno := dmDados.Primeiro(liConexao);

    liIndCol[0] := IOPTR_NOPOINTER;
    liIndCol[1] := IOPTR_NOPOINTER;
    liIndCol[2] := IOPTR_NOPOINTER;
    liIndCol[3] := IOPTR_NOPOINTER;
    liIndCol[4] := IOPTR_NOPOINTER;
    liIndCol[5] := IOPTR_NOPOINTER;

    edtProponente_Nome.Text := dmDados.ValorColuna(liConexao, 'NOME_PROP', liIndCol[0]);
    edtProponente_Endereco.Text := dmDados.ValorColuna(liConexao, 'ENDERECO', liIndCol[1]);
    edtProponente_Bairro.Text := dmDados.ValorColuna(liConexao, 'BAIRRO', liIndCol[2]);
    edtProponente_Cidade.Text := dmDados.ValorColuna(liConexao, 'CIDADE', liIndCol[3]);
    edtProponente_Estado.Text := dmDados.ValorColuna(liConexao, 'ESTADO', liIndCol[4]);
    mskProponente_CEP.Text := dmDados.ValorColuna(liConexao, 'CEP', liIndCol[5]);
  end
  else
  begin
    dmDados.FecharConexao(liConexao);
    { proponente }
    gaParm[0] := 'Nome_Prop, Endereco, Bairro, Cidade, Estado, CEP';
    gaParm[1] := 'Pronicho';
    gaParm[2] := 'Num_Prop = ''' + mskCodigo.Text + '''';
    liConexao := 0;
    lsSQL := dmDados.SqlVersao('NEC_0000', gaParm);
    liConexao := dmDados.ExecutarSelect(lsSQL);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      dmDados.gsRetorno := dmDados.Primeiro(liConexao);

      liIndCol[0] := IOPTR_NOPOINTER;
      liIndCol[1] := IOPTR_NOPOINTER;
      liIndCol[2] := IOPTR_NOPOINTER;
      liIndCol[3] := IOPTR_NOPOINTER;
      liIndCol[4] := IOPTR_NOPOINTER;
      liIndCol[5] := IOPTR_NOPOINTER;

      edtProponente_Nome.Text := dmDados.ValorColuna(liConexao, 'NOME_PROP', liIndCol[0]);
      edtProponente_Endereco.Text := dmDados.ValorColuna(liConexao, 'ENDERECO', liIndCol[1]);
      edtProponente_Bairro.Text := dmDados.ValorColuna(liConexao, 'BAIRRO', liIndCol[2]);
      edtProponente_Cidade.Text := dmDados.ValorColuna(liConexao, 'CIDADE', liIndCol[3]);
      edtProponente_Estado.Text := dmDados.ValorColuna(liConexao, 'ESTADO', liIndCol[4]);
      mskProponente_CEP.Text := dmDados.ValorColuna(liConexao, 'CEP', liIndCol[5]);
    end;
  end;
  dmDados.FecharConexao(liConexao);

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;

end;

end.
