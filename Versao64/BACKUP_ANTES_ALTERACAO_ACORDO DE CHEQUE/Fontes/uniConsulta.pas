unit uniConsulta;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OleCtrls, FPSpread_TLB;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const

  { colunas da Consulta Geral }
  COL_CONS_PROD_CHECK  = 1;
  COL_CONS_PROD_CODIGO = 2;

type
 {*======================================================================
 *            RECORDS
 *======================================================================}
  TConsulta = Record
     sCampo:   string;
     sTitulo:  string;
     dTamanho: Double;
     iTipo:    Integer;
     iCompr:   Integer;
     bEdita:   Boolean;
     sCombo:   string;
     bComboOrdem: Boolean;
  end;

  TCracha = Record
     sNome:   string;
     sFuncao: string;
     sSenha:  string;
  end;

 {*======================================================================
 *            CLASSES
 *======================================================================}
  TdlgConsulta = class(TForm)
    grpConsultaItens: TGroupBox;
    sprConsultaItens: TvaSpread;
    btnOK: TButton;
    btnCancel: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    maConexao: array[0..10] of Integer;
    miConexaoAtualiza: Integer;
    miConexaoHistorico: Integer;
    miIndCol: array[0..23] of Integer;
    msSql: String;
    mbDentro: Boolean;
    maConsulta: array[1..23] of TConsulta;
    miConsulta: Integer;
    mbAlterado: Boolean;

    { preenchimento dos controles}
    procedure lp_Prepara_Consulta;
    { fazer consistencias}
//    function lf_Consiste_Consulta: Boolean;
    { fazer as atualizacoes}
//    procedure lp_Atualiza_Consulta;
  end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  dlgConsulta: TdlgConsulta;

implementation

uses uniConst, uniDados, uniCombos, uniGlobal;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  lp_Prepara_Consulta      Pega dados da Tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgConsulta.lp_Prepara_Consulta;
var
  liConta: Integer;
  liLista: Integer;
  lwConta: Word;
  lvValor: Variant;
begin
  try

    { limpar o spread de consulta }
    sprConsultaItens.Row := 1;
    sprConsultaItens.Col := 1;
    sprConsultaItens.Row2 := sprConsultaItens.MaxRows;
    sprConsultaItens.Col2 := sprConsultaItens.MaxCols;
    sprConsultaItens.BlockMode := True;
    sprConsultaItens.Action := SS_ACTION_CLEAR;
    sprConsultaItens.Lock := False;
    sprConsultaItens.BlockMode := False;
    sprConsultaItens.MaxRows := 0;

    mbAlterado := False;

    { colunas do spread }
    maConsulta[1].sCampo := '';
    maConsulta[1].sTitulo := '';
    maConsulta[1].dTamanho := 1.88;
    maConsulta[1].iTipo := SS_CELL_TYPE_CHECKBOX;
    maConsulta[1].iCompr := 1;
    maConsulta[1].bEdita := False;
    { --- }
    maConsulta[2].sCampo := 'Codigo';
    maConsulta[2].sTitulo := 'Codigo';
    maConsulta[2].dTamanho := 15.00;
    maConsulta[2].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[2].iCompr := 13;
    maConsulta[2].bEdita := False;
    { --- }
    maConsulta[3].sCampo := 'Descricao';
    maConsulta[3].sTitulo := 'Descri��o';
    maConsulta[3].dTamanho := 25.00;
    maConsulta[3].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[3].iCompr := 50;
    maConsulta[3].bEdita := False;
    { --- }
    maConsulta[4].sCampo := 'Data_Lanc';
    maConsulta[4].sTitulo := 'Data Lanc';
    maConsulta[4].dTamanho := 8.00;
    maConsulta[4].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[4].iCompr := 10;
    maConsulta[4].bEdita := False;
    { --- }
    maConsulta[5].sCampo := 'Tipo_Rec';
    maConsulta[5].sTitulo := 'Tipo Rec';
    maConsulta[5].dTamanho := 10.00;
    maConsulta[5].iTipo := SS_CELL_TYPE_COMBOBOX;
    maConsulta[5].iCompr := 1;
    maConsulta[5].bEdita := False;
    maConsulta[5].sCombo := 'wTipoRecebimento';
    maConsulta[5].bComboOrdem := True;
    { --- }
    maConsulta[6].sCampo := 'Num_Prop';
    maConsulta[6].sTitulo := 'Num. Prop.';
    maConsulta[6].dTamanho := 8.00;
    maConsulta[6].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[6].iCompr := 8;
    maConsulta[6].bEdita := False;
    { --- }
    maConsulta[7].sCampo := 'Num_Parc';
    maConsulta[7].sTitulo := 'Num Parc';
    maConsulta[7].dTamanho := 8.00;
    maConsulta[7].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[7].iCompr := 5;
    maConsulta[7].bEdita := False;
    { --- }
    maConsulta[8].sCampo := 'Num_Doc';
    maConsulta[8].sTitulo := 'Num Doc';
    maConsulta[8].dTamanho := 10.00;
    maConsulta[8].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[8].iCompr := 12;
    maConsulta[8].bEdita := False;
    { --- }
    maConsulta[9].sCampo := 'Documento';
    maConsulta[9].sTitulo := 'Documento';
    maConsulta[9].dTamanho := 10.00;
    maConsulta[9].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[9].iCompr := 15;
    maConsulta[9].bEdita := False;
    { --- }
    maConsulta[10].sCampo := 'Vlr_Parc';
    maConsulta[10].sTitulo := 'Vlr Parc';
    maConsulta[10].dTamanho := 8.00;
    maConsulta[10].iTipo := SS_CELL_TYPE_FLOAT;
    maConsulta[10].iCompr := 2;
    maConsulta[10].bEdita := False;
    { --- }
    maConsulta[11].sCampo := 'D_Vencto';
    maConsulta[11].sTitulo := 'Data Venc';
    maConsulta[11].dTamanho := 8.00;
    maConsulta[11].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[11].iCompr := 10;
    maConsulta[11].bEdita := False;
    { --- }
    maConsulta[12].sCampo := 'P_Multa';
    maConsulta[12].sTitulo := 'Multa';
    maConsulta[12].dTamanho := 8.00;
    maConsulta[12].iTipo := SS_CELL_TYPE_FLOAT;
    maConsulta[12].iCompr := 2;
    maConsulta[12].bEdita := False;
    { --- }
    maConsulta[13].sCampo := 'P_Juros';
    maConsulta[13].sTitulo := 'Juros';
    maConsulta[13].dTamanho := 8.00;
    maConsulta[13].iTipo := SS_CELL_TYPE_FLOAT;
    maConsulta[13].iCompr := 2;
    maConsulta[13].bEdita := False;
    { --- }
    maConsulta[14].sCampo := 'Banco';
    maConsulta[14].sTitulo := 'Banco';
    maConsulta[14].dTamanho := 8.00;
    maConsulta[14].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[14].iCompr := 3;
    maConsulta[14].bEdita := False;
    { --- }
    maConsulta[15].sCampo := 'Historico';
    maConsulta[15].sTitulo := 'Historico';
    maConsulta[15].dTamanho := 20.00;
    maConsulta[15].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[15].iCompr := 45;
    maConsulta[15].bEdita := False;
    { --- }
    maConsulta[16].sCampo := 'Pagto';
    maConsulta[16].sTitulo := 'Pagto';
    maConsulta[16].dTamanho := 10.00;
    maConsulta[16].iTipo := SS_CELL_TYPE_COMBOBOX;
    maConsulta[16].iCompr := 1;
    maConsulta[16].bEdita := False;
    maConsulta[16].sCombo := 'wSimNao';
    maConsulta[16].bComboOrdem := True;
    { --- }
    maConsulta[17].sCampo := 'Tipo_Doc';
    maConsulta[17].sTitulo := 'Tipo Doc';
    maConsulta[17].dTamanho := 10.00;
    maConsulta[17].iTipo := SS_CELL_TYPE_COMBOBOX;
    maConsulta[17].iCompr := 1;
    maConsulta[17].bEdita := False;
    maConsulta[17].sCombo := 'wTipoDocumento';
    maConsulta[17].bComboOrdem := True;
    { --- }
    maConsulta[18].sCampo := 'Vlr_Receb';
    maConsulta[18].sTitulo := 'Vlr Receb';
    maConsulta[18].dTamanho := 10.00;
    maConsulta[18].iTipo := SS_CELL_TYPE_FLOAT;
    maConsulta[18].iCompr := 2;
    maConsulta[18].bEdita := False;
    { --- }
    maConsulta[19].sCampo := 'Data_Receb';
    maConsulta[19].sTitulo := 'Data Receb';
    maConsulta[19].dTamanho := 8.00;
    maConsulta[19].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[19].iCompr := 10;
    maConsulta[19].bEdita := False;
    { --- }
    maConsulta[20].sCampo := 'Emit_Carne';
    maConsulta[20].sTitulo := 'Emitido';
    maConsulta[20].dTamanho := 8.00;
    maConsulta[20].iTipo := SS_CELL_TYPE_INTEGER;
    maConsulta[20].iCompr := 1;
    maConsulta[20].bEdita := False;
    { --- }
    maConsulta[21].sCampo := 'Data_Credi';
    maConsulta[21].sTitulo := 'Data Cred';
    maConsulta[21].dTamanho := 8.00;
    maConsulta[21].iTipo := SS_CELL_TYPE_STATIC_TEXT;
    maConsulta[21].iCompr := 10;
    maConsulta[21].bEdita := False;
    { --- }
    maConsulta[22].sCampo := 'Caixa_Banc';
    maConsulta[22].sTitulo := 'Caixa/Banco';
    maConsulta[22].dTamanho := 10.00;
    maConsulta[22].iTipo := SS_CELL_TYPE_COMBOBOX;
    maConsulta[22].iCompr := 1;
    maConsulta[22].bEdita := False;
    maConsulta[22].sCombo := 'wTipoBaixa';
    maConsulta[22].bComboOrdem := True;
    { --- }

    miConsulta := 22;

    sprConsultaItens.ReDraw := False;

    { quantidade de colunas }
    sprConsultaItens.MaxCols := miConsulta;
    { define o  cabecalho }
    sprConsultaItens.Row := 0;
    for lwConta := 1 to miConsulta do
    begin
      sprConsultaItens.Col := lwConta;
      sprConsultaItens.Text := maConsulta[lwConta].sTitulo;
    end;
    { para pegar a coluna inteira }
    sprConsultaItens.Row := -1;
    for lwConta := 1 to miConsulta do
    begin
      sprConsultaItens.Col := lwConta;
      { Define o tipo da celula }
      sprConsultaItens.CellType := maConsulta[lwConta].iTipo;
      { se for edit }
      if maConsulta[lwConta].iTipo = SS_CELL_TYPE_EDIT then
      begin
        { limita o tamanho da digitacao }
        sprConsultaItens.TypeMaxEditLen := maConsulta[lwConta].iCompr;
        sprConsultaItens.Lock := not maConsulta[lwConta].bEdita;
      end;
      { se for float }
      if maConsulta[lwConta].iTipo = SS_CELL_TYPE_FLOAT then
      begin
        { Define o numero de decimais }
        sprConsultaItens.TypeFloatDecimalPlaces := maConsulta[lwConta].iCompr;
        sprConsultaItens.Lock := not maConsulta[lwConta].bEdita;
      end;
      { se for date }
      if maConsulta[lwConta].iTipo = SS_CELL_TYPE_DATE then
      begin
        { Define o limite das datas }
        sprConsultaItens.TypeDateMin := '01011800';
        sprConsultaItens.TypeDateMax := '12312100';
        sprConsultaItens.TypeDateCentury := True;
        sprConsultaItens.Lock := not maConsulta[lwConta].bEdita;
      end;
      { se for combo }
      if maConsulta[lwConta].iTipo = SS_CELL_TYPE_COMBOBOX then
      begin
        { preenche o combo }
        untCombos.gp_Combo_SpreadEstufa(sprConsultaItens, maConsulta[lwConta].sCombo, VAZIO);//, maConsulta[lwConta].bComboOrdem);
        sprConsultaItens.Lock := not maConsulta[lwConta].bEdita;
      end;
      sprConsultaItens.ColWidth[lwConta] := maConsulta[lwConta].dTamanho;
    end;
    sprConsultaItens.ReDraw := True;

    { limpar o spread de consulta }
    sprConsultaItens.Row := 1;
    sprConsultaItens.Col := 1;
    sprConsultaItens.Row2 := sprConsultaItens.MaxRows;
    sprConsultaItens.Col2 := sprConsultaItens.MaxCols;
    sprConsultaItens.BlockMode := True;
    sprConsultaItens.Action := SS_ACTION_CLEAR;
    sprConsultaItens.Lock := False;
    sprConsultaItens.BlockMode := False;
    sprConsultaItens.MaxRows := 1;

    { lista de produtos }
    gaParm[0] := 'TmpRemessa';
    gaParm[1] := 'Codigo';

    msSql := dmDados.SqlVersao('NEC_0001', gaParm);
    maConexao[0] := dmDados.ExecutarSelect(msSql);
    if maConexao[0] = IOPTR_NOPOINTER then
      Exit;

    sprConsultaItens.ReDraw := False;

    liConta := 1;
    while not dmDados.Status(maConexao[0], IOSTATUS_EOF) do
    begin
      if sprConsultaItens.MaxRows < liConta then
        sprConsultaItens.MaxRows := liConta;

      sprConsultaItens.Row := liConta;
      for liLista := 1 to miConsulta - 1 do
      begin
        sprConsultaItens.Col := liLista;
        { se tem campo }
        if maConsulta[liLista].sCampo <> VAZIO then
        begin
          { se for data }
          if maConsulta[liLista].iTipo = SS_CELL_TYPE_DATE then
            sprConsultaItens.Text := FormatDateTime(MK_DATATIMETELA, dmDados.ValorColuna(maConexao[0], maConsulta[liLista].sCampo, miIndCol[liLista-1]))
          else if maConsulta[liLista].iTipo = SS_CELL_TYPE_COMBOBOX then
            untCombos.gp_Combo_SpreadPosiciona(sprConsultaItens, dmDados.ValorColuna(maConexao[0], maConsulta[liLista].sCampo, miIndCol[liLista-1]))//, maConsulta[liLista].bComboOrdem)
          else
            sprConsultaItens.Text := dmDados.ValorColuna(maConexao[0], maConsulta[liLista].sCampo, miIndCol[liLista-1]);
        end;
      end;
      dmDados.gsRetorno := dmDados.Proximo(maConexao[0]);
      if dmDados.gsRetorno = IORET_FAIL then
        Break
      else
        Inc(liConta);
    end;
    sprConsultaItens.ReDraw := True;

  except
    dmDados.ErroTratar('lp_Prepara_Consulta - uniConsulta.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgConsulta.FormShow(Sender: TObject);
begin

  try
    { Pega os dados}
    lp_Prepara_Consulta;

    { foco no cancelar }
    if btnCancel.CanFocus then
      btnCancel.SetFocus;

  except
    dmDados.ErroTratar('FormShow - uniConsulta.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  FormClose      Fecha a Tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgConsulta.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  lwConta : Word;
begin
  try
    { fecha a conexao}
    for lwConta := 0 to High(maConexao) do
      dmDados.FecharConexao(maConexao[lwConta]);

    { se a conexao estiver aberta}
    if miConexaoAtualiza <> 0 then
      dmDados.FecharConexao(miConexaoAtualiza);
    miConexaoAtualiza := IOPTR_NOPOINTER;

    { se a conexao estiver aberta}
    if miConexaoHistorico <> 0 then
      dmDados.FecharConexao(miConexaoHistorico);
    miConexaoHistorico := IOPTR_NOPOINTER;

  except
    dmDados.ErroTratar('FormClose - uniConsulta.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  FormCloseQuery      Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgConsulta.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  i: Integer;
begin

    if (mbAlterado) then
    begin
      // Pede confirma��o ao usu�rio da altera��o
      dmDados.gaMsgParm[0] := 'Exclus�o de Boletos';
      i := dmDados.MensagemExibir('', 4050);
      // MsgSim
      if i = IDYES then
      begin
//        btnGenericoClick(btnConsultaGravar);
        mbAlterado := False;
        CanClose := True;
      end;
      // MsgNao
      if i = IDNO then
      begin
        mbAlterado := False;
        CanClose := True;
      end;
      if i = IDCANCEL then
      begin
        CanClose := False;
      end;
    end;

end;

{*-----------------------------------------------------------------------------
 *  btnCancelClick       Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgConsulta.btnCancelClick(Sender: TObject);
begin

  {fecha a tela}
  dlgConsulta.Close;

end;

{*-----------------------------------------------------------------------------
 *  btnOKClick      Confirma as atualizacoes
 *
 *-----------------------------------------------------------------------------}
procedure TdlgConsulta.btnOKClick(Sender: TObject);
var
  liConta: Integer;
begin

    {varre a lista procurando alguma item marcado}
    for liConta := 1 to sprConsultaItens.MaxRows do
    begin
      sprConsultaItens.Row := liConta;
      {posiciona na coluna do Check}
      sprConsultaItens.Col := COL_CONS_PROD_CHECK;
      {se o tiver marcado}
      if sprConsultaItens.Text = '1' then
      begin
        {posiciona na coluna do Codigo}
        sprConsultaItens.Col := COL_CONS_PROD_CODIGO;
        { exclui do tmp }
        msSql := 'DELETE FROM TmpRemessa WHERE Codigo = ''' + sprConsultaItens.Text + '''';
        dmDados.gsRetorno := dmDados.ExecutarSQL(msSql);
      end;
    end;

  {fecha a tela}
  dlgConsulta.Close;
    
end;

end.
