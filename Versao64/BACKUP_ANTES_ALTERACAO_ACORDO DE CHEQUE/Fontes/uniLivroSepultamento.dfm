�
 TFRMLIVROSEPULTAMENTO 0z   TPF0TfrmLivroSepultamentofrmLivroSepultamentoLeft TopkWidth�Height�CaptionLivro de SepultamentoColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	PositionpoScreenCenterVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidth�HeightaTabOrder  TLabel	lblCodigoLeftTopWidth!HeightCaption   Código  TLabellblDescricaoLeft� TopWidth0HeightCaption   Descrição  TLabellblDataLeft�TopWidthHeightCaptionData  	TPaintBoxPntComunicadorLeftTop Width�HeightaOnPaintPntComunicadorPaint  	TGroupBoxgrpLivroSepultamentoLeftTop0Width�Height)TabOrder TLabellblContratoLeftTopWidth(HeightCaptionContrato  TLabellblNumeroGavetaLeft[TopWidth2HeightCaption
   Nº Gaveta  TLabellblDataSepultadoLeft� TopWidth[HeightCaptionData Sepultamento  TLabellblLivroNumeroLeftTopWidth?HeightCaption   Número Livro  TLabellblFolhaNumeroLeftQTopWidthBHeightCaption   Número Folha  TLabellblTransladoLeft�TopWidth:HeightCaptionData Transl.  TLabellblLugarLeft�TopWidthHeightCaptionLugar  	TMaskEditmskContratoLeftTop Width>HeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskNumeroGavetaLeft[Top Width=HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataSepultadoLeft� Top WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskLivroNumeroLeftTop WidthEHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskFolhaNumeroLeftQTop Width@HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TGroupBoxgrpDadosGeraisLeftTop8Width�Height� CaptionDados GeraisTabOrder TLabellblEstadoCivilLeftJTopWidth7HeightCaptionEstado Civil  TLabellblIdadeLeftTop>WidthHeightCaptionIdade  TLabellblUFLeft� Top>WidthHeightCaptionUF  TLabel
lblNaturalLeft�TopWidth"HeightCaptionNatural  TLabellblSexoLeft� Top?WidthHeightCaptionSexo  TLabellblCorLeft	Top?WidthHeightCaptionCor  TLabellblNomeSepultadoLeft
TopWidth^HeightCaptionNome do Sepultado  TLabellblPaiLeft
TopfWidthHeightCaptionPai  TLabellblMaeLeftJTopfWidthHeightCaption   Mãe  TLabellblLocalFalecimentoLeftJTop>WidtheHeightCaptionLocal do Falecimento  TLabellblHoraFalecimentoLeft	Top� WidthSHeightCaptionHora Falecimento  TLabellblDiaFalecimentoLeftlTop� WidthSHeightCaptionData Falecimento  TLabellblCausaMorteLeftJTop� WidthKHeightCaptionCausa da Morte  TLabellblAtestadoLeft
Top� Width*HeightCaptionAtestado  TLabellblCertidaoObitoLeft� Top� WidthRHeightCaption   Certidão de Óbito  TLabellblDataCertidaoLeftLTop� WidthAHeightCaption   Data Certidão  TLabellblObservacaoLeft�Top� Width:HeightCaption   Observação  TEditedtUFLeft� TopLWidth'HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEdit
edtNaturalLeft�Top"Width� HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtNomeSepultadoLeft
Top"Width7HeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtPaiLeft
ToptWidth7HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtMaeLeftJToptWidthnHeightTabOrder	OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtLocalFalecimentoLeftJTopLWidthnHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskHoraFalecimentoLeft	Top� WidthXHeightEditMask!90:00:00>LL;1;_	MaxLength
TabOrder
Text
  :  :    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDiaFalecimentoLeftlTop� Width[HeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtCausaMorteLeftJTop� WidthoHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtAtestadoLeft
Top� Width� HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtCertidaoObitoLeft� Top� WidthrHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataCertidaoLeftKTop� Width[HeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboEstadoCivilLeftJTop"WidthwHeight
ItemHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboCorLeft	TopMWidthpHeight
ItemHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboSexoLeft� TopMWidthcHeight
ItemHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskIdadeLeftTopMWidth&HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtObservacaoLeft�Top� Width	HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TGroupBoxgprLocalizacaoLeftTopWidth� Height1TabOrder TLabel	lblQuadraLeftTopWidth#HeightCaptionQuadra  TLabellblSetorLeft@TopWidthHeightCaptionRua  TLabellblLoteLeftmTopWidthHeightCaptionJazigo  	TComboBox	cboQuadraLeftTopWidth9Height
ItemHeightSorted	TabOrder Text	cboQuadra
OnKeyPresscboComboKeyPress  	TMaskEditmskSetorLeft@TopWidth)HeightTabOrderTextmskSetorOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskLoteLeftpTopWidth9HeightTabOrderTextmskLoteOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TMaskEditmskTransladoLeft�TopWidthEHeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskLugarLeft�TopWidth&HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TMaskEdit	mskCodigoLeftTopWidth� HeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtDescricaoLeft� TopWidth>HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataLeft�TopWidthBHeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress    