�
 TFRMPLACAS 0B$  TPF0
TfrmPlacas	frmPlacasLeftKTop(Width�Height�CaptionPlacasColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	Position	poDefaultVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeft Top Width�Height�TabOrder  	TGroupBox	grpPlacasLeftTopWidth�Height�TabOrder  TLabel	lblCodigoLeftTop7WidthRHeightCaption   Número Proposta  TLabelLabel2LeftTopWidth!HeightCaption   Código  TLabelLabel3LeftxTopWidth0HeightCaption   Descrição  TLabelLabel7LeftwTop7WidthJHeightCaption   Número Recibo  TLabelLabel16Left� Top7WidthNHeightCaption   Data Solicitação  TLabelLabel17Left?Top7Width2HeightCaptionValor Sinal  TLabelLabel5Left'Top7WidthJHeightCaption1o. Vencimento  TLabelLabel11Left�Top7Width1HeightCaption
Valor Parc  TLabelLabel18Left�Top7Width'HeightCaptionNo Parc  	TPaintBoxPntComunicadorLeftTopWidth�Height�OnPaintPntComunicadorPaint  	TMaskEditmskPropostaLeftTopHWidthRHeightEditMask9999999-9;0;_	MaxLength	TabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TGroupBox
grpProdutoLeftTop� Width�Height� TabOrder
 TLabelLabel32LeftTopWidth%HeightCaptionProduto  TLabelLabel8LeftTop8WidthHHeightCaptionDizeres (Nome)  TLabelLabel9LeftTop`WidthdHeightCaptionDizeres (Nasc/Falec)  TLabelLabel19Left� TopWidth4HeightCaptionQtd. Letras  	TComboBox
cboProdutoLeftTop Width� Height
ItemHeightSorted	TabOrder OnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  TEdit
edtDizeresLeftTopGWidth�Height	MaxLength0TabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtNascFalecLeftTopoWidth�Height	MaxLength0TabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskQtdLetrasLeft� Top Width1HeightCharCaseecUpperCaseTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TMaskEdit	mskCodigoLeftTopWidthQHeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtDescricaoLeftxTopWidth	HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEdit	mskPedidoLeftwTopHWidthRHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataSolicitacaoLeft� TopHWidthZHeightEditMask!99/99/0000;1; 	MaxLength
TabOrderText
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorLeft?TopHWidthBHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TGroupBoxgrpPropostaLeftTophWidthzHeightITabOrder	 TLabelLabel4LeftTopWidth1HeightCaptionSolicitante  TLabelLabel13Left<TopWidth*HeightCaptionTelefone  TLabelLabel12Left�TopWidth#HeightCaptionQuadra  TLabelLabel14LeftTopWidthHeightCaptionRua  TLabelLabel15Left8TopWidthHeightCaptionJazigo  TEditedtSolicitanteLeftTopWidth'Height	MaxLength0TabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskTelefoneLeft<TopWidth� HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskSetorLeftTop Width)HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskLoteLeft8Top Width9HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBox	cboQuadraLeft�Top Width9Height
ItemHeightSorted	TabOrderText	cboQuadraOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress   	TMaskEditmskPrazoLeft'TopHWidthRHeightEditMask!99/99/0000;1; 	MaxLength
TabOrderText
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorParcLeft�TopHWidthBHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEdit
mskNumParcLeft�TopHWidth:HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TGroupBox
grpChequesLeftTopGWidthHeightiCaptionCheques/DepositosTabOrder TLabelLabel36LeftTopWidth*HeightCaptionChequesFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel37LeftTopWidth4HeightCaption   ReferênciaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TEdit
edtChequesLeftTop$Width	HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderText
edtChequesVisible  TEditedtReferenciaLeftTop$Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderTextedtReferenciaVisible  	TvaSpread
sprChequesLeftTopWidth	HeightQTabOrder ControlData
�     �5  _  @                        R������ � K�Q   �DB MS Sans Serif��� ���                          �  �  ��  �  e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif                          ����MbP?#      E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d                B          �����������         ��������              %      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����                @0        �����                @) &      ���� R e /  l  4  + -      ���� A   �����ח������חAR . ,        >
ףp=!@) &      ���� R e /  l  4         ����            �!@*       ����� <             ����            �'@*       ����� <      0    ����    �   8           Parcela 8           Vencimento 8           Valor 8           Recebimento 8           N� Banco 8           N� Cheque 8           Ag�ncia         ����                         �               	TGroupBoxgrpDatasLeft�Top� Width� Height� TabOrder TLabelLabel10LeftTop8WidthFHeightCaption   Data FundiçãoVisible  TLabellblDataPropostaLeftoTop7WidthYHeightCaptionData RecebimentoVisible  TLabelLabel1LeftTop_WidthMHeightCaption   Data ColocaçãoVisible  TLabelLabel6LeftpTop`Width$HeightCaption   UsuárioVisible  TLabelLabel125LeftoTopWidthBHeightCaptionValor DinheiroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel20LeftTopWidth3HeightCaptionValor TotalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TMaskEditmskDataFundicaoLeftTopGWidthZHeightEditMask!99/99/0000;1; 	MaxLength
TabOrderText
  /  /    VisibleOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataRecebimentoLeftoTopHWidthZHeightEditMask!99/99/0000;1; 	MaxLength
TabOrderText
  /  /    VisibleOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataColocacaoLeftToppWidthZHeightEditMask!99/99/0000;1; 	MaxLength
TabOrderText
  /  /    VisibleOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEdit
edtUsuarioLeftpTopoWidthYHeight	MaxLength0TabOrderVisibleOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorDinheiroLeftoTop WidthZHeightTabOrder  	TMaskEditmskValorTotalLeftTop WidthZHeightTabOrder       