unit uniProcessos;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Mask, ComCtrls, DB, DBTables, Series, TeEngine, Variants,
  OleCtrls, Crystal_TLB, Printers, FPSpread_TLB;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const
  { redimencionar a tela}
  MAIS_ALTURA = 100;
  MAIS_LARGURA = 8;

  LARGURA_BOTAO = 150;
  ALTURA_BOTAO = 25;
  { paineis das rotinas (tem q ter o mesmo nome que esta no banco)}
  MAX_PAINEL = 29;

  VISUAL_GAVETA = 'pnlVisualGaveta';
  PROCURA_NOME = 'pnlProcuraNome';

  INC_QUADRA = 'pnlIncluiQuadra';
  ALT_QUADRA = 'pnlAlteraQuadra';
  IMP_QUADRA = 'pnlImplantaQuadra';

  PLANO_AUTO = 'pnlPlanoAuto';
  COMIS_AUTO = 'pnlComissaoAuto';

  GERA_TAXA = 'pnlGeraTaxa';
  GERA_SEGURO = 'pnlGeraSeguro';
  GERA_SEGURADORA = 'pnlGeraSeguradora';
  GERA_PARCELA = 'pnlGeraParcelas';

  GRAPH_LOTETOTAL = 'pnlGraphLoteTotal';
  GRAPH_LOTECANCTOTAL = 'pnlGraphLoteCanTotal';
  GRAPH_LOTESLDTOTAL = 'pnlGraphLoteSldTotal';
  GRAPH_LOTEANO = 'pnlGraphLoteAno';
  GRAPH_LOTEMES = 'pnlGraphLoteMes';
  GRAPH_GAVETOTAL = 'pnlGraphGaveTotal';
  GRAPH_GAVEANO = 'pnlGraphGaveAno';
  GRAPH_GAVEMES = 'pnlGraphGaveMes';

  RETORNO_BANCO = 'pnlRetornoBanco';
  ENVIO_ARQUIVO = 'pnlEnvioArquivo';
  REENVIO_ARQUIVO = 'pnlReenvioArquivo';

  QUITAR_PARCELAS = 'pnlQuitarParcelas';
  ESTORNO_PARCELAS = 'pnlRecebimento';
  BAIXA_AUTO = 'pnlBaixaAutomatica';
  LANCAR_SERVICO = 'pnlLancarServico';

  INCLUIR_IMAGENS = 'pnlIncluirImagens';

  CARTA_COBRANCA = 'pnlCartaCobranca';

  REAJUSTE_PARCELAS = 'pnlReajusteParcelas';

  { controles dos paineis}
  BTN_TOTAL_QUITARPAG = 'btnTotalQuitarPag';
  BTN_RECIBO_QUITARPAG = 'btnReciboQuitarPag';
  BTN_NOTA_QUITARPAG = 'btnNFQuitarPag';
  BTN_INCLUIR_RETBANCO = 'btnIncluirRetBanco';
  BTN_EXCLUIR_RETBANCO = 'btnExcluirRetBanco';
  BTN_INCLUIR_IMAGENS = 'btnIncluirImagens';
  BTN_ALTERAR_VENC = 'btnAlterarVencimento';
  BTN_ALTERAR_VALOR = 'btnAlterarValor';
  BTN_TOTAL_LANCSERV = 'btnTotalLancarServico';
  BTN_RECIBO_LANCSERV = 'btnReciboLancarServico';
  BTN_NOTA_LANCSERV = 'btnNFLancarServico';
  BTN_LIMPAR_ENVIOARQUI = 'btnLimparEnvioArquivo';
  BTN_AGRUPAR_ENVIOARQUI = 'btnAgruparEnvioArquivo';
  BTN_LISTAR_ENVIOARQUI = 'btnListarEnvioArquivo';
  BTN_EXCLUIR_ENVIOARQUI = 'btnExcluirEnvioArquivo';
  BTN_GERAR_ENVIOARQUI = 'btnGerarEnvioArquivo';
  BTN_LIMPAR_REENVIOARQUI = 'btnLimparReenvioArquivo';
  BTN_AGRUPAR_REENVIOARQUI = 'btnAgruparReenvioArquivo';
  BTN_LISTAR_REENVIOARQUI = 'btnListarReenvioArquivo';
  BTN_EXCLUIR_REENVIOARQUI = 'btnExcluirReenvioArquivo';
  BTN_GERAR_REENVIOARQUI = 'btnGerarReenvioArquivo';

  { colunas do Quitar }
  COL_QUIT_CODIGO = 1;
  COL_QUIT_CHECK = 2;
  COL_QUIT_TIPODOC = 3;
  COL_QUIT_REFERENCIA = 4;
  COL_QUIT_PARCELA = 5;
  COL_QUIT_DATAVENC = 6;
  COL_QUIT_VALORVENC = 7;
  COL_QUIT_DATARECEB = 8;
  COL_QUIT_MULTA = 9;
  COL_QUIT_JUROS = 10;
  COL_QUIT_DESCONTO = 11;
  COL_QUIT_VALORRECEB = 12;
  COL_QUIT_HISTORICO = 13;
  COL_QUIT_TIPOBAIXA = 14;
  COL_QUIT_BANCO = 15;
  COL_QUIT_DOCUMENTO = 16;
  COL_QUIT_DATACREDITO = 17;
  COL_QUIT_TIPORECEB = 18;
  COL_QUIT_TIPOTAXA = 19;

  { colunas do Cheques }
  COL_CHQ_PARCELA = 1;
  COL_CHQ_DATAVENC = 2;
  COL_CHQ_VALORPARC = 3;
  COL_CHQ_DATARECEB = 4;
  COL_CHQ_BANCO = 5;
  COL_CHQ_NUMCHEQUE = 6;
  COL_CHQ_TIPOREC = 7;
  COL_CHQ_OBS = 8;
  COL_CHQ_CODIGO = 9;

  { colunas do Quitar }
  COL_SERV_TIPO = 1;
  COL_SERV_QTDE = 2;
  COL_SERV_VALOR = 3;
  COL_SERV_TOTAL = 4;
  COL_SERV_CODIGO = 5;

  PROCESSO_QUITAR = 'ENT00002';
  PROCESSO_PARCIAL = 'ENT00003';
  PROCESSO_SERVICO = 'RCS00002';
  PROCESSO_SEGURO = 'RCG00002';

  PROCESSO_ESTRECEB = 'ENT00004';
  PROCESSO_ESTSERVICO = 'RCS00003';
  PROCESSO_ESTSEGURO = 'RCG00003';

  VLR_MULTA = 0.02;
  VLR_JUROS = 0.00034;

  PARCELA_LOTE   = 'L';
  PARCELA_GAVETA = 'G';
  PARCELA_NICHO  = 'N';
  PARCELA_TAXA   = 'T';
  PARCELA_SEGURO = 'S';
  PARCELA_ICATU  = 'I';

  BAIXA_BANCO = 'B';
  BAIXA_CAIXA = 'C';

  COD_VELORIO = '003';

  TIPO_CHEQUE = ['B','F','G'];
  
  DIAS_ATRASO = 0;
  EXIGE_DESCONTO = 'N';

type
 {*======================================================================
 *            RECORDS
 *======================================================================}

 {*======================================================================
 *            CLASSES
 *======================================================================}
  TformProcessos = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    pnlVisualGaveta: TPanel;
    grpVisualGaveta: TGroupBox;
    lblProponente: TLabel;
    lblNumProposta: TLabel;
    edtProponente: TEdit;
    mskNumProposta: TMaskEdit;
    pnlProcuraNome: TPanel;
    grpProcuraNome: TGroupBox;
    Label1: TLabel;
    edtProcuraNome_Nome: TEdit;
    Label4: TLabel;
    cboProcuraNome_Tipo: TComboBox;
    pnlAlteraQuadra: TPanel;
    grpAlteraQuadra: TGroupBox;
    Label3: TLabel;
    cboAltTipoLote: TComboBox;
    Label5: TLabel;
    mskAltQuadra: TMaskEdit;
    Label13: TLabel;
    mskAltSetor: TMaskEdit;
    Label14: TLabel;
    Label2: TLabel;
    mskAltLoteFim: TMaskEdit;
    mskAltLoteIni: TMaskEdit;
    pnlImplantaQuadra: TPanel;
    grpImplantaQuadra: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    mskImpQuadra: TMaskEdit;
    mskImpSetor: TMaskEdit;
    mskImpLoteFim: TMaskEdit;
    mskImpLoteIni: TMaskEdit;
    Label11: TLabel;
    cboImplantado: TComboBox;
    pnlIncluiQuadra: TPanel;
    grpIncQuadra: TGroupBox;
    Label6: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    cboIncTipoLote: TComboBox;
    mskIncQuadra: TMaskEdit;
    mskIncSetor: TMaskEdit;
    mskIncLoteFim: TMaskEdit;
    mskIncLoteIni: TMaskEdit;
    pnlPlanoAuto: TPanel;
    grpPlanoAuto: TGroupBox;
    Label18: TLabel;
    cboPlanoTipoProposta: TComboBox;
    dtpPlanoDataIncl: TDateTimePicker;
    Label19: TLabel;
    pnlComissaoAuto: TPanel;
    grpComissaoAuto: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    cboComisTipoProposta: TComboBox;
    dtpComisDataIncl: TDateTimePicker;
    pnlGeraTaxa: TPanel;
    grpGeraTaxa: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    cboTaxaFixa: TComboBox;
    dtpTaxaDataVenc: TDateTimePicker;
    mskTaxaReferencia: TMaskEdit;
    Label24: TLabel;
    mskTaxaValor: TMaskEdit;
    Label25: TLabel;
    pnlGraphGaveTotal: TPanel;
    grpGraphGaveTotal: TGroupBox;
    Label26: TLabel;
    mskGraphGTData: TMaskEdit;
    pnlGraphLoteAno: TPanel;
    grpGraphLoteAno: TGroupBox;
    Label27: TLabel;
    mskGraphLAData: TMaskEdit;
    pnlGraphLoteMes: TPanel;
    grpGraphLoteMes: TGroupBox;
    Label29: TLabel;
    mskGraphLMData: TMaskEdit;
    pnlGraphGaveAno: TPanel;
    grpGraphGaveAno: TGroupBox;
    Label30: TLabel;
    mskGraphGAData: TMaskEdit;
    pnlGraphGaveMes: TPanel;
    grpGraphGaveMes: TGroupBox;
    Label31: TLabel;
    mskGraphGMData: TMaskEdit;
    pnlRetornoBanco: TPanel;
    lblArquivoImportantoRetBanco: TLabel;
    lblNomeArquivoRetBanco: TLabel;
    lblLayoutRetBanco: TLabel;
    grpArquivosRetBanco: TGroupBox;
    grdArquivosRetBanco: TvaSpread;
    btnIncluirRetBanco: TButton;
    btnExcluirRetBanco: TButton;
    prbRetBanco: TProgressBar;
    cboLayoutRetBanco: TComboBox;
    pnlEnvioArquivo: TPanel;
    grpEnvioArquivo: TGroupBox;
    lblDataFinalEnvioArqui: TLabel;
    lblDataInicialEnvioArqui: TLabel;
    lblCondominioEnvioArqui: TLabel;
    lblLayoutEnvioArqui: TLabel;
    lblNomeArquivoEnvioArqui: TLabel;
    lblTipoParcelaEnvioArqui: TLabel;
    dtpDataInicialEnvioArqui: TDateTimePicker;
    dtpDataFinalEnvioArqui: TDateTimePicker;
    cboLayoutEnvioArqui: TComboBox;
    edtNomeArquivoEnvioArqui: TEdit;
    cboTipoParcelaEnvioArqui: TComboBox;
    pnlReenvioArquivo: TPanel;
    grpReenvioArquivo: TGroupBox;
    lblDataFinalReenvioArqui: TLabel;
    lblDataInicialReenvioArqui: TLabel;
    lblLayoutReenvioArqui: TLabel;
    lblNomeArquivoReenvioArqui: TLabel;
    lblTipoParcelaReenvioArqui: TLabel;
    dtpDataInicialReenvioArqui: TDateTimePicker;
    dtpDataFinalReenvioArqui: TDateTimePicker;
    cboLayoutReenvioArqui: TComboBox;
    edtNomeArquivoReenvioArqui: TEdit;
    cboTipoParcelaReenvioArqui: TComboBox;
    pnlQuitarParcelas: TPanel;
    grpPagamentosQuitarPag: TGroupBox;
    grdParcelasQuitarPag: TvaSpread;
    grpCabecalhoQuitarPag: TGroupBox;
    Label32: TLabel;
    Label33: TLabel;
    edtNomePropQuitarPag: TEdit;
    edtCodigoPropQuitarPag: TEdit;
    btnTotalQuitarPag: TButton;
    edtTotalQuitarPag: TEdit;
    btnReciboQuitarPag: TButton;
    opdProcessos: TOpenDialog;
    pnlGraphLoteCanTotal: TPanel;
    grpGraphLoteCancTotal: TGroupBox;
    Label34: TLabel;
    mskGraphLCTData: TMaskEdit;
    pnlGraphLoteTotal: TPanel;
    grpGraphLoteTotal: TGroupBox;
    Label28: TLabel;
    mskGraphLTData: TMaskEdit;
    pnlGraphLoteSldTotal: TPanel;
    grpGraphLoteSldTotal: TGroupBox;
    Label35: TLabel;
    mskGraphLoteSldTotal: TMaskEdit;
    grpCheques: TGroupBox;
    Label36: TLabel;
    Label37: TLabel;
    edtCheques: TEdit;
    edtReferencia: TEdit;
    chkDinheiro: TCheckBox;
    mskPropostaInicialEnvioArqui: TMaskEdit;
    mskPropostaFinalEnvioArqui: TMaskEdit;
    Label38: TLabel;
    Label39: TLabel;
    mskNumTaxaEnvioArqui: TMaskEdit;
    prbEnvioBanco: TProgressBar;
    Label40: TLabel;
    cboProcuraNome_TipoTitulo: TComboBox;
    pnlRecebimento: TPanel;
    grpRecebimento: TGroupBox;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    mskValorDesconto: TMaskEdit;
    mskValorRecebido: TMaskEdit;
    mskDataRecebimento: TMaskEdit;
    mskDataCredito: TMaskEdit;
    cboRecebPagto: TComboBox;
    cboRecebCaixaBanco: TComboBox;
    grpRecebNome: TGroupBox;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    edtDocumentoReceb: TEdit;
    edtCodigoReceb: TEdit;
    edtParcelaReceb: TEdit;
    Label50: TLabel;
    mskNumTaxaReenvioArqui: TMaskEdit;
    Label51: TLabel;
    mskPropostaInicialReenvioArqui: TMaskEdit;
    Label52: TLabel;
    mskPropostaFinalReenvioArqui: TMaskEdit;
    prbReenvioBanco: TProgressBar;
    pnlGeraSeguro: TPanel;
    grpGeraSeguro: TGroupBox;
    Label53: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    mskSeguroReferencia: TMaskEdit;
    mskSeguroPropIni: TMaskEdit;
    mskSeguroPropFim: TMaskEdit;
    pnlGeraSeguradora: TPanel;
    Label54: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    mskSeguraReferencia: TMaskEdit;
    mskSeguraPropIni: TMaskEdit;
    mskSeguraPropFim: TMaskEdit;
    Label59: TLabel;
    dtpSeguraDataInicial: TDateTimePicker;
    Label60: TLabel;
    dtpSeguraDataFinal: TDateTimePicker;
    Label61: TLabel;
    cboTipoEnvioEnvioArqui: TComboBox;
    Label62: TLabel;
    cboTipoEnvioReenvioArqui: TComboBox;
    Label63: TLabel;
    dtpSeguroDataInicial: TDateTimePicker;
    Label64: TLabel;
    dtpSeguroDataFinal: TDateTimePicker;
    pnlIncluirImagens: TPanel;
    Label65: TLabel;
    lblIncluirImagens: TLabel;
    Label67: TLabel;
    grpIncluirImagens: TGroupBox;
    btnIncluirImagens: TButton;
    prbIncluirImagens: TProgressBar;
    cboTipoImagens: TComboBox;
    grdIncluirImagens: TvaSpread;
    Label66: TLabel;
    mskCEPInicialReenvioArqui: TMaskEdit;
    Label68: TLabel;
    mskCEPFinalReenvioArqui: TMaskEdit;
    Label69: TLabel;
    mskCEPInicialEnvioArqui: TMaskEdit;
    Label70: TLabel;
    mskCEPFinalEnvioArqui: TMaskEdit;
    pnlCartaCobranca: TPanel;
    grpCartaCobranca: TGroupBox;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    cboModeloCartaCobranca: TComboBox;
    cboTipoPropostaCartaCobranca: TComboBox;
    mskPropostaInicialCartaCobranca: TMaskEdit;
    mskPropostaFinalCartaCobranca: TMaskEdit;
    mskTaxaInicialCartaCobranca: TMaskEdit;
    cboOcupacaoCartaCobranca: TComboBox;
    mskCEPInicialCartaCobranca: TMaskEdit;
    mskCEPFinalCartaCobranca: TMaskEdit;
    prbCartaCobranca: TProgressBar;
    mskTaxaFinalCartaCobranca: TMaskEdit;
    mskAtrasoInicialCartaCobranca: TMaskEdit;
    mskAtrasoFinalCartaCobranca: TMaskEdit;
    Label82: TLabel;
    cboDebitosCartaCobranca: TComboBox;
    Label83: TLabel;
    cboValoresCartaCobranca: TComboBox;
    Label84: TLabel;
    cboAtualizadoCartaCobranca: TComboBox;
    Label85: TLabel;
    cboVersoCartaCobranca: TComboBox;
    btnNFQuitarPag: TButton;
    Label86: TLabel;
    mskDataVencimento: TMaskEdit;
    Label87: TLabel;
    cboBancoEnvioArqui: TComboBox;
    Label88: TLabel;
    cboBancoReenvioArqui: TComboBox;
    btnAlterarVencimento: TButton;
    Label89: TLabel;
    cboServicoEnvioArqui: TComboBox;
    Label90: TLabel;
    cboServicoReenvioArqui: TComboBox;
    Label91: TLabel;
    cboTipoTaxaEnvioArqui: TComboBox;
    Label92: TLabel;
    cboTipoTaxaReenvioArqui: TComboBox;
    Label93: TLabel;
    cboMsgReenvioArqui: TComboBox;
    Label94: TLabel;
    cboMsgEnvioArqui: TComboBox;
    pnlReajusteParcelas: TPanel;
    grpReajusteParcelas: TGroupBox;
    Label95: TLabel;
    Label96: TLabel;
    Label99: TLabel;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    dtpDataInicialReajuste: TDateTimePicker;
    dtpDataFinalReajuste: TDateTimePicker;
    cboTipoParcelaReajuste: TComboBox;
    mskNumTaxaReajuste: TMaskEdit;
    mskPropostaInicialReajuste: TMaskEdit;
    mskPropostaFinalReajuste: TMaskEdit;
    cboServicoReajuste: TComboBox;
    cboTipoTaxaReajuste: TComboBox;
    prbReajuste: TProgressBar;
    Label97: TLabel;
    mskValorOriginal: TMaskEdit;
    btnAlterarValor: TButton;
    pnlGeraParcelas: TPanel;
    grpGeraParcelas: TGroupBox;
    Label98: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    cboTipoParcelaGeraParcela: TComboBox;
    mskPropostaInicialGeraParcela: TMaskEdit;
    mskPropostaFinalGeraParcela: TMaskEdit;
    Label103: TLabel;
    mskPropostaInicialGeraTaxa: TMaskEdit;
    Label106: TLabel;
    mskPropostaFinalGeraTaxa: TMaskEdit;
    pnlBaixaAutomatica: TPanel;
    grpBaixaParcela: TGroupBox;
    grpBaixaProponente: TGroupBox;
    Label109: TLabel;
    Label110: TLabel;
    edtBaixaProponente: TEdit;
    edtBaixaProposta: TEdit;
    grpBaixaBarras: TGroupBox;
    edtBaixaBarras: TEdit;
    grpBaixaReceb: TGroupBox;
    Label111: TLabel;
    Label112: TLabel;
    Label113: TLabel;
    edtBaixaTipoDoc: TEdit;
    edtBaixaCodigo: TEdit;
    edtBaixaNumParc: TEdit;
    grpBaixaRecebParcela: TGroupBox;
    Label114: TLabel;
    Label115: TLabel;
    Label116: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    Label119: TLabel;
    Label120: TLabel;
    Label121: TLabel;
    mskBaixaValorDesconto: TMaskEdit;
    mskBaixaValorRecebido: TMaskEdit;
    mskBaixaDataReceb: TMaskEdit;
    mskBaixaDataCredito: TMaskEdit;
    cboBaixaPagto: TComboBox;
    cboBaixaCaixaBanco: TComboBox;
    mskBaixaDataVenc: TMaskEdit;
    mskBaixaValorOriginal: TMaskEdit;
    Label122: TLabel;
    cboBaixaTipoTaxa: TComboBox;
    Label123: TLabel;
    cboBaixaTipoReceb: TComboBox;
    Label124: TLabel;
    edtReciboQuitarPag: TEdit;
    mskValorDinheiroQuitarPag: TMaskEdit;
    Label125: TLabel;
    Label126: TLabel;
    mskNumParcQuitarPag: TMaskEdit;
    grdChequesQuitarPag: TvaSpread;
    pnlLancarServico: TPanel;
    Label127: TLabel;
    Label128: TLabel;
    Label129: TLabel;
    grpPagamentosLancarServico: TGroupBox;
    grdParcelasLancarServico: TvaSpread;
    grpCabecalhoLancarServico: TGroupBox;
    Label130: TLabel;
    Label131: TLabel;
    edtNomePropLancarServico: TEdit;
    btnTotalLancarServico: TButton;
    edtTotalLancarServico: TEdit;
    btnReciboLancarServico: TButton;
    btnNFLancarServico: TButton;
    edtReciboLancarServico: TEdit;
    mskValorDinheiroLancarServico: TMaskEdit;
    mskNumParcLancarServico: TMaskEdit;
    mskCodigoPropLancarServico: TMaskEdit;
    mskDataLancarServico: TMaskEdit;
    Label132: TLabel;
    grpChequesLancarServico: TGroupBox;
    Label133: TLabel;
    Label134: TLabel;
    edtChequesLancarServico: TEdit;
    edtReferenciaLancarServico: TEdit;
    chkDinheiroLancarServico: TCheckBox;
    grdChequesLancarServico: TvaSpread;
    btnAgruparEnvioArquivo: TButton;
    btnListarEnvioArquivo: TButton;
    btnGerarEnvioArquivo: TButton;
    btnLimparEnvioArquivo: TButton;
    btnExcluirEnvioArquivo: TButton;
    Label135: TLabel;
    mskDataInicialEnvioArqui: TMaskEdit;
    Label136: TLabel;
    mskDataFinalEnvioArqui: TMaskEdit;
    Label137: TLabel;
    mskDataVencimentoEnvioArqui: TMaskEdit;
    Label138: TLabel;
    mskDataInicialReenvioArqui: TMaskEdit;
    Label139: TLabel;
    mskDataFinalReenvioArqui: TMaskEdit;
    Label140: TLabel;
    mskDataVencimentoReenvioArqui: TMaskEdit;
    btnLimparReenvioArquivo: TButton;
    btnAgruparReenvioArquivo: TButton;
    btnListarReenvioArquivo: TButton;
    btnExcluirReenvioArquivo: TButton;
    btnGerarReenvioArquivo: TButton;
    Label141: TLabel;
    cboIncImplantado: TComboBox;
    cboImpQuadra: TComboBox;
    cboIncQuadra: TComboBox;
    cboAltQuadra: TComboBox;
    Label142: TLabel;
    mskIncDataAlvara: TMaskEdit;
    Label143: TLabel;
    mskIncDataConstrucao: TMaskEdit;
    Label144: TLabel;
    mskIncAlvara: TMaskEdit;
    Label145: TLabel;
    mskAltDataAlvara: TMaskEdit;
    Label146: TLabel;
    mskAltDataConstrucao: TMaskEdit;
    Label147: TLabel;
    cboAltImplantado: TComboBox;
    Label148: TLabel;
    mskAltAlvara: TMaskEdit;
    Label149: TLabel;
    cboIncLado: TComboBox;
    Label150: TLabel;
    cboAltLado: TComboBox;
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cboGenericoClick(Sender: TObject);
    procedure chkGenericoClick(Sender: TObject);
    procedure dtpGenericoCloseUp(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure edtGenericoExit(Sender: TObject);
    procedure mskGenericoExit(Sender: TObject);
    procedure btnGenericoClick(Sender: TObject);
    procedure grdGenericoEditChange(Sender: TObject; Col, Row: Integer);
    procedure grdGenericoButtonClicked(Sender: TObject; Col,
      Row: Integer; ButtonDown: Smallint);
    procedure grdGenericoLeaveCell(Sender: TObject; Col, Row,
      NewCol, NewRow: Integer; var Cancel: WordBool);
    procedure edtBaixaBarrasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mskNumPropostaExit(Sender: TObject);
  private
    { Private declarations }
    { p/ redimensionar}
    miAltura: Integer;
    miLargura: Integer;
    { p/ as conexoes genericas}
    maConexao: array[0..10] of Integer;
    miConexaoAtualiza: Integer;
    miConexaoHistorico: Integer;
    miIndCol: array[0..25] of Integer;
    msSql: String;
    mbDentro: Boolean;
    maPainel: array[1..MAX_PAINEL] of string;
    maReferencia: array[1..12] of string;
    msReferencia: String;
    maMensagem: array[1..26] of string;

    { gerais}
    procedure lp_Redimensionar;
    function lf_ConfirmaAtualizacoes: Boolean;
    { preenchimento dos controles}
    procedure lp_Prepara_Processo;
    { fazer consistencias}
    function lf_Consiste_Processo: Boolean;
    { gravar Historico}
    function lf_Grava_Processo: Boolean;
    { fazer as atualizacoes}
    procedure lp_Atualiza_Processo;

    function mf_Incluir_Imagem(psNumProp: string;
                               psTipo: string;
                               psCaminho: string): string;
    function mf_Pegar_Dizeres(psCodigo: string): string;
  public
    { Public declarations }
  end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  formProcessos: TformProcessos;

implementation

uses uniConst, uniDados, uniCombos, uniGlobal, uniTelaVar, uniErros, uniDigito,
     uniVisualizacaoGavetas, uniProcuraNome, uniGrafico, uniRetorno, uniGeraArquivo,
     uniEntradaManual, uniImprimir, uniRecebServico, uniRecebSeguro, uniFuncoes,
     uniConsulta;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TformProcessos.lp_Redimensionar       Redimensiona a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.lp_Redimensionar;
begin

  try
    { altera o tamanho da tela}
    formProcessos.Height := miAltura + MAIS_ALTURA;
    formProcessos.Width := miLargura + MAIS_LARGURA;
    btnOK.Top := formProcessos.Height - (3 * ALTURA_BOTAO);
    btnCancel.Top := formProcessos.Height - (3 * ALTURA_BOTAO);
    btnOK.Left := Round((formProcessos.Width - (2 * LARGURA_BOTAO)) / 3);
    btnCancel.Left := (LARGURA_BOTAO + (2 * Round((formProcessos.Width - (2 * LARGURA_BOTAO)) / 3)));

  except
    dmDados.ErroTratar('lp_Redimensionar - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.btnCancelClick       Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.btnCancelClick(Sender: TObject);
begin
  { Inicializa variavel de sucesso }
  gbProcessoOK := False;
  {fecha a tela}
  formProcessos.Close;
end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.FormShow(Sender: TObject);
var
  wContador: Word;
begin

  try
    { Inicializa variavel de sucesso }
    gbProcessoOK := False;

    { inicializa as variaveis}
    for wContador := 0 to High(maConexao) do
      maConexao[wContador] := 0;

    for wContador := 0 to High(miIndCol) do
      miIndCol[wContador] := IOPTR_NOPOINTER;

    maReferencia[1] := 'Janeiro';
    maReferencia[2] := 'Fevereiro';
    maReferencia[3] := 'Mar�o';
    maReferencia[4] := 'Abril';
    maReferencia[5] := 'Maio';
    maReferencia[6] := 'Junho';
    maReferencia[7] := 'Julho';
    maReferencia[8] := 'Agosto';
    maReferencia[9] := 'Setembro';
    maReferencia[10] := 'Outubro';
    maReferencia[11] := 'Novembro';
    maReferencia[12] := 'Dezembro';

    maPainel[1] := VISUAL_GAVETA;
    maPainel[2] := PROCURA_NOME;
    maPainel[3] := INC_QUADRA;
    maPainel[4] := ALT_QUADRA;
    maPainel[5] := IMP_QUADRA;
    maPainel[6] := PLANO_AUTO;
    maPainel[7] := COMIS_AUTO;
    maPainel[8] := GERA_TAXA;
    maPainel[9] := GRAPH_LOTETOTAL;
    maPainel[10] := GRAPH_LOTEANO;
    maPainel[11] := GRAPH_LOTEMES;
    maPainel[12] := GRAPH_GAVETOTAL;
    maPainel[13] := GRAPH_GAVEANO;
    maPainel[14] := GRAPH_GAVEMES;
    maPainel[15] := RETORNO_BANCO;
    maPainel[16] := ENVIO_ARQUIVO;
    maPainel[17] := REENVIO_ARQUIVO;
    maPainel[18] := QUITAR_PARCELAS;
    maPainel[19] := GRAPH_LOTECANCTOTAL;
    maPainel[20] := GRAPH_LOTESLDTOTAL;
    maPainel[21] := ESTORNO_PARCELAS;
    maPainel[22] := GERA_SEGURO;
    maPainel[23] := GERA_SEGURADORA;
    maPainel[24] := INCLUIR_IMAGENS;
    maPainel[25] := CARTA_COBRANCA;
    maPainel[26] := REAJUSTE_PARCELAS;
    maPainel[27] := GERA_PARCELA;
    maPainel[28] := BAIXA_AUTO;
    maPainel[29] := LANCAR_SERVICO;

    formProcessos.Caption := grProcessos.sDescricao;

    { pega a altura do painel}
    miAltura := TPanel(formProcessos.FindComponent(grProcessos.sNomePainel)).Height;
    miLargura := TPanel(formProcessos.FindComponent(grProcessos.sNomePainel)).Width;
    { posiciona o painel}
    TPanel(formProcessos.FindComponent(grProcessos.sNomePainel)).Left := 0;
    TPanel(formProcessos.FindComponent(grProcessos.sNomePainel)).Top := 0;

    for wContador := 1 to MAX_PAINEL do
      { habilita o painel certo}
      if maPainel[wContador] = grProcessos.sNomePainel then
        TPanel(formProcessos.FindComponent(maPainel[wContador])).Visible := True
      else
      { desabilita os outros }
        TPanel(formProcessos.FindComponent(maPainel[wContador])).Visible := False;
    { Pega os dados}
    lp_Prepara_Processo;

    { rerdimensiona a tela}
    lp_Redimensionar;

    { foco no cancelar }
    if btnCancel.CanFocus then
      btnCancel.SetFocus;

  except
    dmDados.ErroTratar('FormShow - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.lp_Prepara_Processo      Pega dados da Tela
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.lp_Prepara_Processo;
var
  lwConta: Word;
  lvValor: Variant;
  lsReferencia: string;
begin
  try
    { se for Visual Gaveta}
    if grProcessos.sNomePainel = VISUAL_GAVETA then
    begin
      { limpa os controles}
      mskNumProposta.Text := VAZIO;
      edtProponente.Text := VAZIO;
      { Pega os dados da tela ativa}
      { estufa o combo}
    end;
    { se for Procura Nome}
    if grProcessos.sNomePainel = PROCURA_NOME then
    begin
      { limpa os controles}
      edtProcuraNome_Nome.Text := VAZIO;
      { Pega os dados da tela ativa}
      { estufa o combo}
      cboProcuraNome_TipoTitulo.Text := VAZIO;
      cboProcuraNome_TipoTitulo.Items.Clear;
      { Pega os dados da tela ativa}
      { estufa o combo}
      untCombos.gp_Combo_Estufa(cboProcuraNome_TipoTitulo, 'wTipoTitulos', VAZIO);
    end;
    { se for Incluir Quadra}
    if grProcessos.sNomePainel = INC_QUADRA then
    begin
      { limpa os controles}
      mskIncQuadra.Text := VAZIO;
      mskIncSetor.Text := VAZIO;
      mskIncLoteIni.Text := VAZIO;
      mskIncLoteFim.Text := VAZIO;
      mskIncDataAlvara.Text := VAZIO;
      mskIncDataConstrucao.Text := VAZIO;
      mskIncAlvara.Text := VAZIO;
      cboIncQuadra.Text := VAZIO;
      cboIncQuadra.Items.Clear;
      cboIncTipoLote.Text := VAZIO;
      cboIncTipoLote.Items.Clear;
      cboIncImplantado.Text := VAZIO;
      cboIncImplantado.Items.Clear;
      cboIncLado.Text := VAZIO;
      cboIncLado.Items.Clear;
      { Pega os dados da tela ativa}
      { estufa o combo}
      untCombos.gp_Combo_Estufa(cboIncTipoLote, 'TAB_LOTE', VAZIO);
      untCombos.gp_Combo_Estufa(cboIncQuadra, 'wQuadras', VAZIO);
      untCombos.gp_Combo_Estufa(cboIncImplantado, 'wImplantado', VAZIO);
      untCombos.gp_Combo_Estufa(cboIncLado, 'wlado', VAZIO);
    end;
    { se for Alterar Quadra}
    if grProcessos.sNomePainel = ALT_QUADRA then
    begin
      { limpa os controles}
      mskAltQuadra.Text := VAZIO;
      mskAltSetor.Text := VAZIO;
      mskAltLoteIni.Text := VAZIO;
      mskAltLoteFim.Text := VAZIO;
      mskAltDataAlvara.Text := VAZIO;
      mskAltDataConstrucao.Text := VAZIO;
      mskAltAlvara.Text := VAZIO;
      cboAltQuadra.Text := VAZIO;
      cboAltQuadra.Items.Clear;
      cboAltTipoLote.Text := VAZIO;
      cboAltTipoLote.Items.Clear;
      cboAltImplantado.Text := VAZIO;
      cboAltImplantado.Items.Clear;
      cboAltLado.Text := VAZIO;
      cboAltLado.Items.Clear;
      { Pega os dados da tela ativa}
      { estufa o combo}
      untCombos.gp_Combo_Estufa(cboAltTipoLote, 'TAB_LOTE', VAZIO);
      untCombos.gp_Combo_Estufa(cboAltQuadra, 'wQuadras', VAZIO);
      untCombos.gp_Combo_Estufa(cboAltImplantado, 'wImplantado', VAZIO);
      untCombos.gp_Combo_Estufa(cboAltLado, 'wlado', VAZIO);
    end;
    { se for Implantar Quadra}
    if grProcessos.sNomePainel = IMP_QUADRA then
    begin
      { limpa os controles}
      mskImpQuadra.Text := VAZIO;
      mskImpSetor.Text := VAZIO;
      mskImpLoteIni.Text := VAZIO;
      mskImpLoteFim.Text := VAZIO;
      cboImpQuadra.Text := VAZIO;
      cboImpQuadra.Items.Clear;
      cboImplantado.Text := VAZIO;
      cboImplantado.Items.Clear;
      { Pega os dados da tela ativa}
      { estufa o combo}
      untCombos.gp_Combo_Estufa(cboImplantado, 'wImplantado', VAZIO);
      untCombos.gp_Combo_Estufa(cboImpQuadra, 'wQuadras', VAZIO);
    end;
    { se for Plano Automatico}
    if grProcessos.sNomePainel = PLANO_AUTO then
    begin
      { limpa os controles}
      dtpPlanoDataIncl.Date := Date;
      cboPlanoTipoProposta.Text := VAZIO;
      cboPlanoTipoProposta.Items.Clear;
      { Pega os dados da tela ativa}
      { estufa o combo}
      untCombos.gp_Combo_Estufa(cboPlanoTipoProposta, 'wTipoProposta', VAZIO);
    end;
    { se for Comissao Automatico}
    if grProcessos.sNomePainel = COMIS_AUTO then
    begin
      { limpa os controles}
      dtpComisDataIncl.Date := Date;
      cboComisTipoProposta.Text := VAZIO;
      cboComisTipoProposta.Items.Clear;
      { Pega os dados da tela ativa}
      { estufa o combo}
      untCombos.gp_Combo_Estufa(cboComisTipoProposta, 'wTipoProposta', VAZIO);
    end;
    { se for Gerar Taxas}
    if grProcessos.sNomePainel = GERA_TAXA then
    begin
      { limpa os controles}
      mskPropostaInicialGeraTaxa.Text := VAZIO;
      mskPropostaFinalGeraTaxa.Text := VAZIO;
      dtpTaxaDataVenc.Date := Date;
      mskTaxaReferencia.Text := VAZIO;
      mskTaxaValor.Text := VAZIO;
      cboTaxaFixa.Text := VAZIO;
      cboTaxaFixa.Items.Clear;
      { Pega os dados da tela ativa}
      { estufa o combo}
      untCombos.gp_Combo_Estufa(cboTaxaFixa, 'wTipoIndice', VAZIO);
    end;
    { se for Gerar Parcelas}
    if grProcessos.sNomePainel = GERA_PARCELA then
    begin
      { limpa os controles}
      mskPropostaInicialGeraParcela.Text := VAZIO;
      mskPropostaFinalGeraParcela.Text := VAZIO;
      cboTipoParcelaGeraParcela.Text := VAZIO;
      cboTipoParcelaGeraParcela.Items.Clear;
      { Pega os dados da tela ativa}
      { estufa o combo}
      untCombos.gp_Combo_Estufa(cboTipoParcelaGeraParcela, 'wTipoProposta', VAZIO);
    end;
    { se for Gerar Seguro}
    if grProcessos.sNomePainel = GERA_SEGURO then
    begin
      { limpa os controles}
      mskSeguroReferencia.Text := VAZIO;
      mskSeguroPropIni.Text := VAZIO;
      mskSeguroPropFim.Text := VAZIO;
      { Pega os dados da tela ativa}
      { estufa o combo}
    end;
    { se for Gerar Seguradora}
    if grProcessos.sNomePainel = GERA_SEGURADORA then
    begin
      { limpa os controles}
      dtpSeguraDataInicial.Date := Date();
      dtpSeguraDataFinal.Date := Date();
      mskSeguraReferencia.Text := VAZIO;
      mskSeguraPropIni.Text := VAZIO;
      mskSeguraPropFim.Text := VAZIO;
      { Pega os dados da tela ativa}
      { estufa o combo}
    end;
    { se for Retorno Banco}
    if grProcessos.sNomePainel = RETORNO_BANCO then
    begin
      { limpa os controles}
      lblNomeArquivoRetBanco.Caption := VAZIO;
      cboLayoutRetBanco.Text := VAZIO;
      cboLayoutRetBanco.Items.Clear;

      { Preenche o Combo}
      untCombos.gp_Combo_Estufa(cboLayoutRetBanco, TABELA_LAYOUT, 'TIPO=''T''');

      {' limpar o spread de Parcelas}
      grdArquivosRetBanco.Row := 1;
      grdArquivosRetBanco.Col := 1;
      grdArquivosRetBanco.Row2 := grdArquivosRetBanco.MaxRows;
      grdArquivosRetBanco.Col2 := grdArquivosRetBanco.MaxCols;
      grdArquivosRetBanco.BlockMode := True;
      grdArquivosRetBanco.Action := SS_ACTION_CLEAR;
      grdArquivosRetBanco.BlockMode := False;
      grdArquivosRetBanco.MaxRows := 0;

      { zera a barra de progresso }
      prbRetBanco.Position := 0;
    end;
    { se for Envio Banco}
    if grProcessos.sNomePainel = ENVIO_ARQUIVO then
    begin
      { limpa os controles}
      dtpDataInicialEnvioArqui.Date := Date();
      dtpDataFinalEnvioArqui.Date := Date();

      edtNomeArquivoEnvioArqui.Text := VAZIO;
      mskPropostaInicialEnvioArqui.Text := VAZIO;
      mskPropostaFinalEnvioArqui.Text := VAZIO;
      mskNumTaxaEnvioArqui.Text := VAZIO;
      mskDataInicialEnvioArqui.Text := VAZIO;
      mskDataFinalEnvioArqui.Text := VAZIO;
      mskDataVencimentoEnvioArqui.Text := VAZIO;
      cboLayoutEnvioArqui.Text := VAZIO;
      cboLayoutEnvioArqui.Items.Clear;
      cboMsgEnvioArqui.Text := VAZIO;
      cboMsgEnvioArqui.Items.Clear;
      cboTipoParcelaEnvioArqui.Text := VAZIO;
      cboTipoParcelaEnvioArqui.Items.Clear;
      cboTipoTaxaEnvioArqui.Text := VAZIO;
      cboTipoTaxaEnvioArqui.Items.Clear;
      cboTipoEnvioEnvioArqui.Text := VAZIO;
      cboTipoEnvioEnvioArqui.Items.Clear;
      cboBancoEnvioArqui.Text := VAZIO;
      cboBancoEnvioArqui.Items.Clear;
      cboServicoEnvioArqui.Text := VAZIO;
      cboServicoEnvioArqui.Items.Clear;

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { Preenche o Combo}
      untCombos.gp_Combo_Estufa(cboLayoutEnvioArqui, TABELA_LAYOUT, 'TIPO<>''T''');
      untCombos.gp_Combo_Estufa(cboMsgEnvioArqui, 'Mensagem', VAZIO);
      untCombos.gp_Combo_Estufa(cboTipoParcelaEnvioArqui, TABELA_TRECEB, VAZIO);
      untCombos.gp_Combo_Estufa(cboTipoTaxaEnvioArqui, 'wModeloTaxa', VAZIO);
      untCombos.gp_Combo_Estufa(cboTipoEnvioEnvioArqui, 'wTipoEnvio', VAZIO);
      untCombos.gp_Combo_Estufa(cboBancoEnvioArqui, 'C_BANCO', VAZIO);
      untCombos.gp_Combo_Estufa(cboServicoEnvioArqui, 'TAB_SERV', VAZIO);

      untCombos.gp_Combo_Posiciona(cboTipoEnvioEnvioArqui, '1');

      { zera a barra de progresso }
      prbEnvioBanco.Position := 0;
    end;
    { se for Reenvio Banco}
    if grProcessos.sNomePainel = REENVIO_ARQUIVO then
    begin
      { limpa os controles}
      dtpDataInicialReenvioArqui.Date := Date();
      dtpDataFinalReenvioArqui.Date := Date();

      edtNomeArquivoReenvioArqui.Text := VAZIO;
      mskPropostaInicialReenvioArqui.Text := VAZIO;
      mskPropostaFinalReenvioArqui.Text := VAZIO;
      mskNumTaxaReenvioArqui.Text := VAZIO;
      mskDataInicialReenvioArqui.Text := VAZIO;
      mskDataFinalReenvioArqui.Text := VAZIO;
      mskDataVencimentoReenvioArqui.Text := VAZIO;
      cboLayoutReenvioArqui.Text := VAZIO;
      cboLayoutReenvioArqui.Items.Clear;
      cboMsgReenvioArqui.Text := VAZIO;
      cboMsgReenvioArqui.Items.Clear;
      cboTipoParcelaReenvioArqui.Text := VAZIO;
      cboTipoParcelaReenvioArqui.Items.Clear;
      cboTipoTaxaReenvioArqui.Text := VAZIO;
      cboTipoTaxaReenvioArqui.Items.Clear;
      cboTipoEnvioReenvioArqui.Text := VAZIO;
      cboTipoEnvioReenvioArqui.Items.Clear;
      cboBancoReenvioArqui.Text := VAZIO;
      cboBancoReenvioArqui.Items.Clear;
      cboServicoReenvioArqui.Text := VAZIO;
      cboServicoReenvioArqui.Items.Clear;

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { Preenche o Combo}
      untCombos.gp_Combo_Estufa(cboLayoutReenvioArqui, TABELA_LAYOUT, 'TIPO<>''T''');
      untCombos.gp_Combo_Estufa(cboMsgReenvioArqui, 'Mensagem', VAZIO);
      untCombos.gp_Combo_Estufa(cboTipoParcelaReenvioArqui, TABELA_TRECEB, VAZIO);
      untCombos.gp_Combo_Estufa(cboTipoTaxaReenvioArqui, 'wModeloTaxa', VAZIO);
      untCombos.gp_Combo_Estufa(cboTipoEnvioReenvioArqui, 'wTipoEnvio', VAZIO);
      untCombos.gp_Combo_Estufa(cboBancoReenvioArqui, 'C_BANCO', VAZIO);
      untCombos.gp_Combo_Estufa(cboServicoReenvioArqui, 'TAB_SERV', VAZIO);

      untCombos.gp_Combo_Posiciona(cboTipoEnvioReenvioArqui, '1');

      { zera a barra de progresso }
      prbReenvioBanco.Position := 0;
    end;
    { se for Reajuste de Parcelas}
    if grProcessos.sNomePainel = REAJUSTE_PARCELAS then
    begin
      { limpa os controles}
      dtpDataInicialReajuste.Date := Date();
      dtpDataFinalReajuste.Date := Date();

      mskPropostaInicialReajuste.Text := VAZIO;
      mskPropostaFinalReajuste.Text := VAZIO;
      mskNumTaxaReajuste.Text := VAZIO;
      cboTipoParcelaReajuste.Text := VAZIO;
      cboTipoParcelaReajuste.Items.Clear;
      cboTipoTaxaReajuste.Text := VAZIO;
      cboTipoTaxaReajuste.Items.Clear;
      cboServicoReajuste.Text := VAZIO;
      cboServicoReajuste.Items.Clear;

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { Preenche o Combo}
      untCombos.gp_Combo_Estufa(cboTipoParcelaReajuste, TABELA_TRECEB, VAZIO);
      untCombos.gp_Combo_Estufa(cboTipoTaxaReajuste, 'wModeloTaxa', VAZIO);
      untCombos.gp_Combo_Estufa(cboServicoReajuste, 'TAB_SERV', VAZIO);

      { zera a barra de progresso }
      prbReajuste.Position := 0;
    end;
    { se for Estorno de Parcelas}
    if grProcessos.sNomePainel = ESTORNO_PARCELAS then
    begin
      { recebimento ou servico }
      if grProcessos.sCod_Processo = PROCESSO_ESTRECEB then
      begin
        { Pega os dados da tela ativa}
        edtCodigoReceb.Text := frmEntradaManual.mskCodigo.Text;
        edtDocumentoReceb.Text := frmEntradaManual.cboTipoDocumento.Text;
        edtParcelaReceb.Text := frmEntradaManual.mskNumeroParcela.Text;
      end
      else
      begin
        if grProcessos.sCod_Processo = PROCESSO_ESTSERVICO then
        begin
          { Pega os dados da tela ativa}
          edtCodigoReceb.Text := frmRecebServico.mskCodigo.Text;
          edtDocumentoReceb.Text := frmRecebServico.cboTipoDocumento.Text;
          edtParcelaReceb.Text := frmRecebServico.mskNumeroParcela.Text;
        end
        else
        begin
          { Pega os dados da tela ativa}
          edtCodigoReceb.Text := frmRecebSeguro.mskCodigo.Text;
          edtDocumentoReceb.Text := frmRecebSeguro.cboTipoDocumento.Text;
          edtParcelaReceb.Text := frmRecebSeguro.mskNumeroParcela.Text;
        end;
      end;

      cboRecebPagto.Text := VAZIO;
      cboRecebPagto.Items.Clear;
      cboRecebCaixaBanco.Text := VAZIO;
      cboRecebCaixaBanco.Items.Clear;

      { Preenche o Combo}
      untCombos.gp_Combo_Estufa(cboRecebPagto, 'wSimNao', VAZIO);
      untCombos.gp_Combo_Estufa(cboRecebCaixaBanco, 'wTipoBaixa', VAZIO);

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { recebimento ou servico }
      if grProcessos.sCod_Processo = PROCESSO_ESTRECEB then
        gaParm[0] := 'RECEB'
      else
        if grProcessos.sCod_Processo = PROCESSO_ESTSERVICO then
          gaParm[0] := 'REC_SERV'
        else
          gaParm[0] := 'REC_SEGU';
      gaParm[1] := 'CODIGO = ''' + edtCodigoReceb.Text + '''';
      gaParm[2] := 'CODIGO';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      if not dmDados.Status(maConexao[0], IOSTATUS_EOF) then
      begin
        lvValor := dmDados.ValorColuna(maConexao[0], 'DATA_RECEB', miIndCol[0]);
        if lvValor <> 0 then
          lvValor := FormatDateTime(MK_DATATELA, lvValor)
        else
          lvValor := VAZIO_DATA;
        mskDataRecebimento.Text := lvValor;
        lvValor := dmDados.ValorColuna(maConexao[0], 'DATA_CREDI', miIndCol[1]);
        if lvValor <> 0 then
          lvValor := FormatDateTime(MK_DATATELA, lvValor)
        else
          lvValor := VAZIO_DATA;
        mskDataCredito.Text := lvValor;
        lvValor := dmDados.ValorColuna(maConexao[0], 'D_VENCTO', miIndCol[6]);
        if lvValor <> 0 then
          lvValor := FormatDateTime(MK_DATATELA, lvValor)
        else
          lvValor := VAZIO_DATA;
        mskDataVencimento.Text := lvValor;
        untCombos.gp_Combo_Posiciona(cboRecebPagto, dmDados.ValorColuna(maConexao[0], 'PAGTO', miIndCol[2]));
        untCombos.gp_Combo_Posiciona(cboRecebCaixaBanco, dmDados.ValorColuna(maConexao[0], 'CAIXA_BANC', miIndCol[3]));
        mskValorDesconto.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'VLR_DESC', miIndCol[4]));
        mskValorRecebido.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'VLR_RECEB', miIndCol[5]));
        mskValorOriginal.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'VLR_PARC', miIndCol[7]));
        { se estiver em aberto }
        if dmDados.ValorColuna(maConexao[0], 'PAGTO', miIndCol[2]) = 'N' then
        begin
          mskDataVencimento.Enabled := True;
          mskValorOriginal.Enabled := True;
        end
        else
        begin
          mskDataVencimento.Enabled := False;
          mskValorOriginal.Enabled := False;
        end;

        if (mskDataRecebimento.Text <> VAZIO_DATA) and (mskDataRecebimento.Text <> VAZIO) then
        begin
          { mais de quarenta dias }
(*          if Date - StrToDate(mskDataRecebimento.Text) > 40 then
          begin
            mskDataRecebimento.Enabled := False;
            mskDataCredito.Enabled := False;
            cboRecebPagto.Enabled := False;
            cboRecebCaixaBanco.Enabled := False;
            mskValorDesconto.Enabled := False;
            mskValorRecebido.Enabled := False;
          end
          else
          begin
*)            mskDataRecebimento.Enabled := True;
            mskDataCredito.Enabled := True;
            cboRecebPagto.Enabled := True;
            cboRecebCaixaBanco.Enabled := True;
            mskValorDesconto.Enabled := True;
            mskValorRecebido.Enabled := True;
//          end;
          mskDataVencimento.Enabled := False;
          mskValorOriginal.Enabled := False;
        end
        else
        begin
          mskDataVencimento.Enabled := True;
          mskValorOriginal.Enabled := True;
        end;
      end
      else
      begin
        { limpa os controles}
        mskDataRecebimento.Text := VAZIO;
        mskDataCredito.Text := VAZIO;
        cboRecebPagto.Text := VAZIO;
        cboRecebCaixaBanco.Text := VAZIO;
        mskValorDesconto.Text := FormatFloat(MK_VALOR, 0);
        mskValorRecebido.Text := FormatFloat(MK_VALOR, 0);
        mskDataVencimento.Text := VAZIO;
        mskValorOriginal.Text := FormatFloat(MK_VALOR, 0);
      end;
    end;
    { se for Quitar Parcelas}
    if grProcessos.sNomePainel = QUITAR_PARCELAS then
    begin
      { limpa os controles}
      edtNomePropQuitarPag.Text := VAZIO;
      edtCodigoPropQuitarPag.Text := VAZIO;
      edtCheques.Text := VAZIO;
      edtReferencia.Text := VAZIO;
      chkDinheiro.Checked := False;
      edtTotalQuitarPag.Text := FormatFloat(MK_VALOR, 0);
      edtReciboQuitarPag.Text := VAZIO;
      mskValorDinheiroQuitarPag.Text := FormatFloat(MK_VALOR, 0);
      mskValorDinheiroQuitarPag.Enabled := False;
      mskNumParcQuitarPag.Text := VAZIO;
      mskNumParcQuitarPag.Enabled := False;

      {' limpar o spread de Parcelas}
      grdParcelasQuitarPag.Row := 1;
      grdParcelasQuitarPag.Col := 1;
      grdParcelasQuitarPag.Row2 := grdParcelasQuitarPag.MaxRows;
      grdParcelasQuitarPag.Col2 := grdParcelasQuitarPag.MaxCols;
      grdParcelasQuitarPag.BlockMode := True;
      grdParcelasQuitarPag.Action := SS_ACTION_CLEAR;
      grdParcelasQuitarPag.Lock := False;
      grdParcelasQuitarPag.BlockMode := False;
      grdParcelasQuitarPag.MaxRows := 1;

      {' limpar o spread de Cheques }
      grdChequesQuitarPag.MaxRows := 0;
      grdChequesQuitarPag.Enabled := False;

      { para pegar a coluna inteira }
      grdParcelasQuitarPag.Row := -1;
//      { para preencher todas as linhas}
//      for lwConta := 1 to grdParcelasQuitarPag.MaxRows do
//      begin
//        grdParcelasQuitarPag.Row := lwConta;

        {Preenche os combos}
        grdParcelasQuitarPag.Col := COL_QUIT_TIPOBAIXA;
        untCombos.gp_Combo_SpreadEstufa(grdParcelasQuitarPag, 'wTipoBaixa', VAZIO);
        grdParcelasQuitarPag.Col := COL_QUIT_TIPORECEB;
        untCombos.gp_Combo_SpreadEstufa(grdParcelasQuitarPag, 'wTipoRecebimento', VAZIO);
        grdParcelasQuitarPag.Col := COL_QUIT_TIPOTAXA;
        untCombos.gp_Combo_SpreadEstufa(grdParcelasQuitarPag, 'wModeloTaxa', VAZIO);
//      end;

      { quitacao ou recibo parcial }
      if (grProcessos.sCod_Processo = PROCESSO_QUITAR) or (grProcessos.sCod_Processo = PROCESSO_SERVICO) or (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
      begin
        grdParcelasQuitarPag.ColWidth[COL_QUIT_PARCELA] := 0;
        grdParcelasQuitarPag.ColWidth[COL_QUIT_HISTORICO] := 22.75;

        grdParcelasQuitarPag.ColWidth[COL_QUIT_MULTA] := 5.13;
        grdParcelasQuitarPag.ColWidth[COL_QUIT_JUROS] := 5.00;
        grdParcelasQuitarPag.ColWidth[COL_QUIT_DESCONTO] := 7.75;
        grdParcelasQuitarPag.ColWidth[COL_QUIT_DATACREDITO] := 10.63;
        { se for quitacao normal tem taxa }
        if (grProcessos.sCod_Processo = PROCESSO_QUITAR)  then
          grdParcelasQuitarPag.ColWidth[COL_QUIT_TIPOTAXA] := 11.00
        else
          grdParcelasQuitarPag.ColWidth[COL_QUIT_TIPOTAXA] := 0;
      end
      else
      begin
        {' limpar o spread de Parcelas}
        grdParcelasQuitarPag.Row := 1;
        grdParcelasQuitarPag.Col := 3;
        grdParcelasQuitarPag.Row2 := grdParcelasQuitarPag.MaxRows;
        grdParcelasQuitarPag.Col2 := grdParcelasQuitarPag.MaxCols;
        grdParcelasQuitarPag.BlockMode := True;
        grdParcelasQuitarPag.Lock := True;
        grdParcelasQuitarPag.BlockMode := False;

        grdParcelasQuitarPag.ColWidth[COL_QUIT_PARCELA] := 5.25;
        grdParcelasQuitarPag.ColWidth[COL_QUIT_HISTORICO] := 22.75;

        grdParcelasQuitarPag.ColWidth[COL_QUIT_MULTA] := 0;
        grdParcelasQuitarPag.ColWidth[COL_QUIT_JUROS] := 0;
        grdParcelasQuitarPag.ColWidth[COL_QUIT_DESCONTO] := 0;
        grdParcelasQuitarPag.ColWidth[COL_QUIT_DATACREDITO] := 0;
      end;

      if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
      begin
        { Pega os dados da tela ativa}
        edtCodigoPropQuitarPag.Text := frmRecebServico.mskNumeroProposta.Text;
        edtNomePropQuitarPag.Text := frmRecebServico.edtDescricao.Text;
        { habilita bota NF so em servico }
        btnNFQuitarPag.Visible := True;
      end
      else
      begin
        { desabilita bota NF se nao for servico }
        btnNFQuitarPag.Visible := False;
        if (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
        begin
          { Pega os dados da tela ativa}
          edtCodigoPropQuitarPag.Text := frmRecebSeguro.mskNumeroProposta.Text;
          edtNomePropQuitarPag.Text := frmRecebSeguro.edtDescricao.Text;
        end
        else
        begin
          { Pega os dados da tela ativa}
          edtCodigoPropQuitarPag.Text := frmEntradaManual.mskNumeroProposta.Text;
          edtNomePropQuitarPag.Text := frmEntradaManual.edtDescricao.Text;
        end;
      end;

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { quitacao ou recibo parcial }
      if (grProcessos.sCod_Processo = PROCESSO_QUITAR) or (grProcessos.sCod_Processo = PROCESSO_SERVICO) or (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
      begin
        if (grProcessos.sCod_Processo = PROCESSO_QUITAR) then
        begin
          gaParm[0] := 'RECEB';
          gaParm[1] := 'NUM_PROP = ''' + edtCodigoPropQuitarPag.Text + ''' AND ' +
                       '(PAGTO = ''N'' OR PAGTO IS NULL) AND ' +
                       '(PARCELAS = ''00'' OR PARCELAS IS NULL)';
        end
        else
        begin
          if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
          begin
            gaParm[0] := 'REC_SERV';
            gaParm[1] := 'NUM_PROP = ''' + edtCodigoPropQuitarPag.Text + ''' AND (PAGTO = ''N'' OR PAGTO IS NULL)';
          end
          else
          begin
            gaParm[0] := 'REC_SEGU';
            gaParm[1] := 'NUM_PROP = ''' + edtCodigoPropQuitarPag.Text + ''' AND (PAGTO = ''N'' OR PAGTO IS NULL)';
          end;
        end;
        gaParm[2] := 'CODIGO';
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;
      end
      else
      begin
//        gaParm[0] := 'RECEB.CODIGO AS CODIGO, REC_PARC.CODIGO AS PARCELA, RECEB.TIPO_DOC AS TIPO_DOC, ' +
//                     'RECEB.NUM_PARC AS NUM_PARC, RECEB.D_VENCTO AS D_VENCTO, RECEB.VLR_PARC AS VLR_PARC, ' +
//                     'REC_PARC.DATA_RECEB AS DATA_RECEB, REC_PARC.VLR_RECEB AS VLR_RECEB, ' +
//                     'REC_PARC.HISTORICO AS HISTORICO, RECEB.CAIXA_BANC AS CAIXA_BANC, ' +
//                     'RECEB.BANCO AS BANCO, RECEB.NUM_DOC AS NUM_DOC, REC_PARC.TIPO_REC AS TIPO_REC';
//        gaParm[1] := 'RECEB, REC_PARC';
//        gaParm[2] := 'RECEB.CODIGO = REC_PARC.CODIGORECEBER';
//        gaParm[2] := gaParm[2] + ' AND RECEB.NUM_PROP = ''' + edtCodigoPropQuitarPag.Text + '''';   // AND (PAGTO = ''N'' OR PAGTO IS NULL)';
//        gaParm[3] := 'RECEB.CODIGO, REC_PARC.CODIGO';
//        msSql := dmDados.SqlVersao('NEC_0017', gaParm);
        gaParm[0] := edtCodigoPropQuitarPag.Text;   // AND (PAGTO = ''N'' OR PAGTO IS NULL)';
        msSql := dmDados.SqlVersao('NEC_0028', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;
      end;

      if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
        gaParm[0] := 'TAB_SERV'
      else
//        if (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
//          gaParm[0] := 'TAB_SEGU'
//        else
          gaParm[0] := TABELA_TRECEB;
      gaParm[1] := 'CODIGO';
      msSql := dmDados.SqlVersao('NEC_0001', gaParm);
      maConexao[1] := dmDados.ExecutarSelect(msSql);
      if maConexao[1] = IOPTR_NOPOINTER then
        Exit;

      lwConta := 1;
      while not dmDados.Status(maConexao[0], IOSTATUS_EOF) do
      begin
        if grdParcelasQuitarPag.MaxRows < lwConta then
          grdParcelasQuitarPag.MaxRows := lwConta;

        grdParcelasQuitarPag.Row := lwConta;

        grdParcelasQuitarPag.Col := COL_QUIT_CODIGO;
        grdParcelasQuitarPag.Text := dmDados.ValorColuna(maConexao[0], 'CODIGO', miIndCol[4]);

        grdParcelasQuitarPag.Col := COL_QUIT_TIPODOC;
        if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
        begin
          lvValor := dmDados.ValorColuna(maConexao[0], 'COD_SERV', miIndCol[0]);
          grdParcelasQuitarPag.Text := untCombos.gf_ComboCadastro_Conteudo(maConexao[1], 'CODIGO', VarToStr(lvValor), 'DESCRICAO');
        end
        else
        begin
//          if (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
//          begin
//            lvValor := dmDados.ValorColuna(maConexao[0], 'COD_SERV', miIndCol[0]);
//            grdParcelasQuitarPag.Text := untCombos.gf_ComboCadastro_Conteudo(maConexao[1], 'CODIGO', VarToStr(lvValor), 'DESCRICAO');
//          end
//          else
//          begin
            lvValor := dmDados.ValorColuna(maConexao[0], 'TIPO_DOC', miIndCol[0]);
            grdParcelasQuitarPag.Text := untCombos.gf_ComboCadastro_Conteudo(maConexao[1], 'CODIGO', VarToStr(lvValor), 'DESCRICAO');
//          end;
        end;
        { guarda o tipo do documento }
        lsReferencia := lvValor;

        grdParcelasQuitarPag.Col := COL_QUIT_REFERENCIA;
        grdParcelasQuitarPag.Text := dmDados.ValorColuna(maConexao[0], 'NUM_PARC', miIndCol[1]);

        grdParcelasQuitarPag.Col := COL_QUIT_DATAVENC;
        lvValor := dmDados.ValorColuna(maConexao[0], 'D_VENCTO', miIndCol[2]);
        if VarType(lvValor) = varDate then
          grdParcelasQuitarPag.Text := FormatDateTime(MK_DATATELA, lvValor)
        else
          grdParcelasQuitarPag.Text := VAZIO;

        grdParcelasQuitarPag.Col := COL_QUIT_VALORVENC;
        grdParcelasQuitarPag.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'VLR_PARC', miIndCol[3]));

        { quitacao ou recibo parcial }
        if grProcessos.sCod_Processo = PROCESSO_QUITAR then
        begin
          grdParcelasQuitarPag.Col := COL_QUIT_MULTA;
          grdParcelasQuitarPag.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'P_MULTA', miIndCol[5]));

          grdParcelasQuitarPag.Col := COL_QUIT_JUROS;
          grdParcelasQuitarPag.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'P_JUROS', miIndCol[6]));
        end;
        { quitacao ou recibo parcial }
        if grProcessos.sCod_Processo = PROCESSO_PARCIAL then
        begin
          grdParcelasQuitarPag.Col := COL_QUIT_PARCELA;
          grdParcelasQuitarPag.Text := dmDados.ValorColuna(maConexao[0], 'PARCELA', miIndCol[9]);

          grdParcelasQuitarPag.Col := COL_QUIT_DATARECEB;
          lvValor := dmDados.ValorColuna(maConexao[0], 'DATA_RECEB', miIndCol[10]);
          if VarType(lvValor) = varDate then
            grdParcelasQuitarPag.Text := FormatDateTime(MK_DATATELA, lvValor)
          else
            grdParcelasQuitarPag.Text := VAZIO;

          grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
          grdParcelasQuitarPag.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'VLR_RECEB', miIndCol[11]));

          grdParcelasQuitarPag.Col := COL_QUIT_TIPOBAIXA;
          lvValor := dmDados.ValorColuna(maConexao[0], 'CAIXA_BANC', miIndCol[13]);
          untCombos.gp_Combo_SpreadPosiciona(grdParcelasQuitarPag, VarToStr(lvValor));
        end;

        grdParcelasQuitarPag.Col := COL_QUIT_HISTORICO;
        if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
          grdParcelasQuitarPag.Text := dmDados.ValorColuna(maConexao[0], 'OBSERVACAO', miIndCol[12])
        else
          grdParcelasQuitarPag.Text := dmDados.ValorColuna(maConexao[0], 'HISTORICO', miIndCol[12]);

        grdParcelasQuitarPag.Col := COL_QUIT_TIPORECEB;
        lvValor := dmDados.ValorColuna(maConexao[0], 'TIPO_REC', miIndCol[14]);
        untCombos.gp_Combo_SpreadPosiciona(grdParcelasQuitarPag, VarToStr(lvValor));

        { se for quitacao normal tem taxa }
        if (grProcessos.sCod_Processo = PROCESSO_QUITAR)  then
        begin
          grdParcelasQuitarPag.Col := COL_QUIT_TIPOTAXA;
          lvValor := dmDados.ValorColuna(maConexao[0], 'TIPO_TAXA', miIndCol[15]);
          untCombos.gp_Combo_SpreadPosiciona(grdParcelasQuitarPag, VarToStr(lvValor));
          { se nao for taxa }
          if (lsReferencia <> 'T') or (lvValor <> VAZIO) then
            grdParcelasQuitarPag.Lock := True
          else
            grdParcelasQuitarPag.Lock := False;
        end;

        grdParcelasQuitarPag.Col := COL_QUIT_BANCO;
        grdParcelasQuitarPag.Text := dmDados.ValorColuna(maConexao[0], 'BANCO', miIndCol[7]);

        grdParcelasQuitarPag.Col := COL_QUIT_DOCUMENTO;
        grdParcelasQuitarPag.Text := dmDados.ValorColuna(maConexao[0], 'NUM_DOC', miIndCol[8]);

        dmDados.gsRetorno := dmDados.Proximo(maConexao[0]);
        if dmDados.gsRetorno = IORET_FAIL then
          Break
        else
          Inc(lwConta);
      end;

      for lwConta := 0 to High(maConexao) do
      begin
        dmDados.FecharConexao(maConexao[lwConta]);
      end;
    end;
    { se for Lancar Servico}
    if grProcessos.sNomePainel = LANCAR_SERVICO then
    begin
      { limpa os controles}
      edtNomePropLancarServico.Text := VAZIO;
      mskCodigoPropLancarServico.Text := VAZIO;
      edtChequesLancarServico.Text := VAZIO;
      edtReferenciaLancarServico.Text := VAZIO;
      chkDinheiroLancarServico.Checked := False;
      edtTotalLancarServico.Text := FormatFloat(MK_VALOR, 0);
      edtReciboLancarServico.Text := VAZIO;
      mskValorDinheiroLancarServico.Text := FormatFloat(MK_VALOR, 0);
      mskValorDinheiroLancarServico.Enabled := True;
      mskNumParcLancarServico.Text := VAZIO;
      mskNumParcLancarServico.Enabled := False;

      { desabilita bota NF se nao for servico }
      btnNFLancarServico.Visible := False;

      {' limpar o spread de Parcelas}
      grdParcelasLancarServico.Row := 1;
      grdParcelasLancarServico.Col := 1;
      grdParcelasLancarServico.Row2 := grdParcelasLancarServico.MaxRows;
      grdParcelasLancarServico.Col2 := grdParcelasLancarServico.MaxCols;
      grdParcelasLancarServico.BlockMode := True;
      grdParcelasLancarServico.Action := SS_ACTION_CLEAR;
//      grdParcelasLancarServico.Lock := False;
      grdParcelasLancarServico.BlockMode := False;

      grdParcelasLancarServico.MaxRows := 20;

      {' limpar o spread de Cheques }
      grdChequesLancarServico.MaxRows := 0;
      grdChequesLancarServico.Enabled := False;

      { para pegar a coluna inteira }
      grdParcelasLancarServico.Row := -1;
      grdChequesLancarServico.Row := -1;
      {Preenche os combos}
      grdParcelasLancarServico.Col := COL_SERV_TIPO;
      untCombos.gp_Combo_SpreadEstufa(grdParcelasLancarServico, 'TAB_SERV', VAZIO);
      grdChequesLancarServico.Col := COL_CHQ_TIPOREC;
      untCombos.gp_Combo_SpreadEstufa(grdChequesLancarServico, 'wTipoRecebimento', VAZIO);

    end;
    { se for Baixa Automatica }
    if grProcessos.sNomePainel = BAIXA_AUTO then
    begin
      { limpa os controles}
      edtBaixaBarras.Text := VAZIO;
      edtBaixaProponente.Text := VAZIO;
      edtBaixaProposta.Text := VAZIO;
      edtBaixaCodigo.Text := VAZIO;
      edtBaixaTipoDoc.Text := VAZIO;
      edtBaixaNumParc.Text := VAZIO;

      mskBaixaValorOriginal.Text := FormatFloat(MK_VALOR, 0);
      mskBaixaValorDesconto.Text := FormatFloat(MK_VALOR, 0);
      mskBaixaValorRecebido.Text := FormatFloat(MK_VALOR, 0);
      mskBaixaDataVenc.Text := VAZIO_DATA;
      mskBaixaDataReceb.Text := VAZIO_DATA;
      mskBaixaDataCredito.Text := VAZIO_DATA;

      cboBaixaPagto.Text := VAZIO;
      cbobaixaPagto.Items.Clear;
      cbobaixaCaixaBanco.Text := VAZIO;
      cboBaixaCaixaBanco.Items.Clear;
      cbobaixaTipoTaxa.Text := VAZIO;
      cboBaixaTipoTaxa.Items.Clear;
      cbobaixaTipoReceb.Text := VAZIO;
      cboBaixaTipoReceb.Items.Clear;

      { Preenche o Combo}
      untCombos.gp_Combo_Estufa(cboBaixaPagto, 'wSimNao', VAZIO);
      untCombos.gp_Combo_Estufa(cboBaixaCaixaBanco, 'wTipoBaixa', VAZIO);
      untCombos.gp_Combo_Estufa(cboBaixaTipoTaxa, 'wModeloTaxa', VAZIO);
      untCombos.gp_Combo_Estufa(cboBaixaTipoReceb, 'wTipoDocumento', VAZIO);

      { coloca o foco no codigo de barras }
      if edtBaixaBarras.Enabled then
        edtBaixaBarras.SetFocus;
    end;
    { se for Incluir Imagens }
    if grProcessos.sNomePainel = INCLUIR_IMAGENS then
    begin
      { limpa os controles}
      lblIncluirImagens.Caption := VAZIO;
      cboTipoImagens.Text := VAZIO;
      cboTipoImagens.Items.Clear;

      { Preenche o Combo}
      untCombos.gp_Combo_Estufa(cboTipoImagens, 'wTipoImagem', VAZIO);

      {' limpar o spread de Parcelas}
      grdIncluirImagens.Row := 1;
      grdIncluirImagens.Col := 1;
      grdIncluirImagens.Row2 := grdIncluirImagens.MaxRows;
      grdIncluirImagens.Col2 := grdIncluirImagens.MaxCols;
      grdIncluirImagens.BlockMode := True;
      grdIncluirImagens.Action := SS_ACTION_CLEAR;
      grdIncluirImagens.BlockMode := False;
      grdIncluirImagens.MaxRows := 0;

      { zera a barra de progresso }
      prbIncluirImagens.Position := 0;
    end;
    { se for Carta Cobranca }
    if grProcessos.sNomePainel = CARTA_COBRANCA then
    begin
      { limpa os controles}
      mskPropostaInicialCartaCobranca.Text := VAZIO;
      mskPropostaFinalCartaCobranca.Text := VAZIO;
      mskCEPInicialCartaCobranca.Text := VAZIO;
      mskCEPFinalCartaCobranca.Text := VAZIO;
      mskAtrasoInicialCartaCobranca.Text := VAZIO;
      mskAtrasoFinalCartaCobranca.Text := VAZIO;
      mskTaxaInicialCartaCobranca.Text := VAZIO;
      mskTaxaFinalCartaCobranca.Text := VAZIO;

      cboModeloCartaCobranca.Text := VAZIO;
      cboModeloCartaCobranca.Items.Clear;
      cboTipoPropostaCartaCobranca.Text := VAZIO;
      cboTipoPropostaCartaCobranca.Items.Clear;
      cboOcupacaoCartaCobranca.Text := VAZIO;
      cboOcupacaoCartaCobranca.Items.Clear;
      cboDebitosCartaCobranca.Text := VAZIO;
      cboDebitosCartaCobranca.Items.Clear;
      cboValoresCartaCobranca.Text := VAZIO;
      cboValoresCartaCobranca.Items.Clear;
      cboAtualizadoCartaCobranca.Text := VAZIO;
      cboAtualizadoCartaCobranca.Items.Clear;
      cboVersoCartaCobranca.Text := VAZIO;
      cboVersoCartaCobranca.Items.Clear;

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { Preenche o Combo}
      untCombos.gp_Combo_Estufa(cboModeloCartaCobranca, 'CARTAS', VAZIO);
      untCombos.gp_Combo_Estufa(cboTipoPropostaCartaCobranca, 'wTipoDocumento', VAZIO);
      untCombos.gp_Combo_Estufa(cboOcupacaoCartaCobranca, 'wSimNao', VAZIO);
      untCombos.gp_Combo_Posiciona(cboOcupacaoCartaCobranca, 'N');
      untCombos.gp_Combo_Estufa(cboDebitosCartaCobranca, 'wSimNao', VAZIO);
      untCombos.gp_Combo_Posiciona(cboDebitosCartaCobranca, 'N');
      untCombos.gp_Combo_Estufa(cboValoresCartaCobranca, 'wSimNao', VAZIO);
      untCombos.gp_Combo_Posiciona(cboValoresCartaCobranca, 'N');
      untCombos.gp_Combo_Estufa(cboAtualizadoCartaCobranca, 'wSimNao', VAZIO);
      untCombos.gp_Combo_Posiciona(cboAtualizadoCartaCobranca, 'N');
      untCombos.gp_Combo_Estufa(cboVersoCartaCobranca, 'wSimNao', VAZIO);
      untCombos.gp_Combo_Posiciona(cboVersoCartaCobranca, 'S');

      { zera a barra de progresso }
      prbCartaCobranca.Position := 0;
    end;

  except
    dmDados.ErroTratar('lp_Prepara_Processo - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.FormClose      Fecha a Tela
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  lwConta : Word;
begin
  try
    { fecha a conexao}
    for lwConta := 0 to High(maConexao) do
      dmDados.FecharConexao(maConexao[lwConta]);

    { se a conexao estiver aberta}
    if miConexaoAtualiza <> 0 then
      dmDados.FecharConexao(miConexaoAtualiza);
    miConexaoAtualiza := IOPTR_NOPOINTER;

    { se a conexao estiver aberta}
    if miConexaoHistorico <> 0 then
      dmDados.FecharConexao(miConexaoHistorico);
    miConexaoHistorico := IOPTR_NOPOINTER;

  except
    dmDados.ErroTratar('FormClose - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.cboGenericoClick      Faz as devidas modificacoes
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.cboGenericoClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  try
    lcboCombo := TComboBox(Sender);

    if lcboCombo.Text <> VAZIO then
    begin
      { faz as devidas modificacoes de acordo com o combo}
    end;

  except
    dmDados.ErroTratar('cboGenericoClick - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.chkGenericoClick      Faz as devidas modificacoes
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.chkGenericoClick(Sender: TObject);
var
  lchkCheck: TCheckBox;
begin

  try

    { se ja esta dentro, sai fora}
    if mbDentro then
      Exit;

    { para evitar entradas }
    mbDentro := True;

    lchkCheck := TCheckBox(Sender);

    if lchkCheck.Name = 'chkDinheiro' then
    begin
      mskValorDinheiroQuitarPag.Text := FormatFloat(MK_VALOR, 0);
      mskValorDinheiroQuitarPag.Enabled := (chkDinheiro.Checked);
      mskNumParcQuitarPag.Text := VAZIO;
      mskNumParcQuitarPag.Enabled := (chkDinheiro.Checked);
      grdChequesQuitarPag.MaxRows := 0;
      grdChequesQuitarPag.Enabled := (chkDinheiro.Checked);
//      edtCheques.Text := VAZIO;
//      edtCheques.Enabled := not (chkDinheiro.Checked);
    end;
    if lchkCheck.Name = 'chkDinheiroLancarServico' then
    begin
      mskValorDinheiroLancarServico.Text := FormatFloat(MK_VALOR, 0);
//      mskValorDinheiroLancarServico.Enabled := (chkDinheiroLancarServico.Checked);
      mskNumParcLancarServico.Text := VAZIO;
      mskNumParcLancarServico.Enabled := (chkDinheiroLancarServico.Checked);
      grdChequesLancarServico.MaxRows := 0;
      grdChequesLancarServico.Enabled := (chkDinheiroLancarServico.Checked);
//      edtCheques.Text := VAZIO;
//      edtCheques.Enabled := not (chkDinheiro.Checked);
    end;

    mbDentro := False;

  except
    dmDados.ErroTratar('chkGenericoClick - uniProcessos.Pas');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.dtpGenericoCloseUp      Faz as devidas modificacoes
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.dtpGenericoCloseUp(Sender: TObject);
var
  ldtpData: TDateTimePicker;
begin

  try
    ldtpData := TDateTimePicker(Sender);

  except
    dmDados.ErroTratar('dtpGenericoCloseUp - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.btnOKClick      Confirma as atualizacoes
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.btnOKClick(Sender: TObject);
begin

  try

    { apresenta a ampulheta}
    Screen.Cursor := crHourGlass;

    { se for tdo ok}
    if lf_ConfirmaAtualizacoes then
    begin
      { apresenta mensagem de altualizacao feita}
      dmDados.MensagemExibir('', 4600);
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
      { apresenta a cursor default}
      Screen.Cursor := crDefault;
      Close;
    end;

    { apresenta a cursor default}
    Screen.Cursor := crDefault;

  except
    dmDados.ErroTratar('btnOKClick - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.edtGenericoExit      Faz as devidas modificacoes
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.edtGenericoExit(Sender: TObject);
var
  ledtTexto: TEdit;

  lbAchou: Boolean;
  lbSeguro: Boolean;
  lbServico: Boolean;
  lsTipoDoc: string;
  lsTipoTaxa: string;
  lvValor: Variant;
begin

  try
    ledtTexto := TEdit(Sender);

    if ledtTexto.Text <> VAZIO then
    begin
      { cartas de Cobranca - Atraso }
      if (ledtTexto.Name = 'mskAtrasoInicialCartaCobranca') or
         (ledtTexto.Name = 'mskAtrasoFinalCartaCobranca') then
      begin
        if (Trim(mskAtrasoInicialCartaCobranca.Text) = VAZIO) and
           (Trim(mskAtrasoFinalCartaCobranca.Text) = VAZIO) then
        begin
          mskTaxaInicialCartaCobranca.Enabled := True;
          mskTaxaFinalCartaCobranca.Enabled := True;
        end
        else
        begin
          mskTaxaInicialCartaCobranca.Enabled := False;
          mskTaxaFinalCartaCobranca.Enabled := False;
        end;
      end;
      { cartas de Cobranca - Parcelas }
      if (ledtTexto.Name = 'mskTaxaInicialCartaCobranca') or
         (ledtTexto.Name = 'mskTaxaFinalCartaCobranca') then
      begin
        if (Trim(mskTaxaInicialCartaCobranca.Text) = VAZIO) and
           (Trim(mskTaxaFinalCartaCobranca.Text) = VAZIO) then
        begin
          mskAtrasoInicialCartaCobranca.Enabled := True;
          mskAtrasoFinalCartaCobranca.Enabled := True;
        end
        else
        begin
          mskAtrasoInicialCartaCobranca.Enabled := False;
          mskAtrasoFinalCartaCobranca.Enabled := False;
        end;
      end;
      { Baixa Automatica - Codigo de Barras }
      if ledtTexto.Name = 'edtBaixaBarras' then
      begin
        Screen.Cursor := crHourGlass;

        lbAchou := False;
        gaParm[0] := StrToFloat(untDigito.gf_DAC_Registro(Copy(edtBaixaBarras.Text, 20, 10)));
        msSql := dmDados.SqlVersao('ARQ_0006', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        { se achou - Parcela }
        if not dmDados.Status(maConexao[0], IOSTATUS_EOF) then
        begin
          lbAchou := True;
          lsTipoDoc := dmDados.ValorColuna(maConexao[0], CAMPO_REC_TIPO_DOC, miIndCol[6]);
          if lsTipoDoc = 'T' then
          begin
            lsTipoTaxa := dmDados.ValorColuna(maConexao[0], CAMPO_REC_TIPO_TAXA, miIndCol[7]);
            if lsTipoTaxa = VAZIO then
              lsTipoTaxa := TAXA_UNICA;
          end;
        end
        else
        begin
          { Fecha a Conexao}
          dmDados.FecharConexao(maConexao[0]);
          { abre novamente}
          msSql := dmDados.SqlVersao('ARQ_0012', gaParm);
          maConexao[0] := dmDados.ExecutarSelect(msSql);
          { se Achou - Taxa Semestral }
          if not dmDados.Status(maConexao[0], IOSTATUS_EOF) then
          begin
            { Achou}
            lbAchou := True;
            lsTipoDoc := 'T';
            lsTipoTaxa := TAXA_SEMESTRAL;
          end
          else
          begin
            { Fecha a Conexao}
            dmDados.FecharConexao(maConexao[0]);
            { abre novamente}
            msSql := dmDados.SqlVersao('ARQ_0013', gaParm);
            maConexao[0] := dmDados.ExecutarSelect(msSql);
            { se Achou - Taxa Bimestral }
            if not dmDados.Status(maConexao[0], IOSTATUS_EOF) then
            begin
              { Achou}
              lbAchou := True;
              lsTipoDoc := 'T';
              lsTipoTaxa := TAXA_BIMESTRAL;
            end
            else
            begin
              { Fecha a Conexao}
              dmDados.FecharConexao(maConexao[0]);
              { abre novamente}
              msSql := dmDados.SqlVersao('ARQ_0014', gaParm);
              maConexao[0] := dmDados.ExecutarSelect(msSql);
              { se Achou - Servico }
              if not dmDados.Status(maConexao[0], IOSTATUS_EOF) then
              begin
                { Achou}
                lbAchou := True;
                lbServico := True;
              end
              else
              begin
                { Fecha a Conexao}
                dmDados.FecharConexao(maConexao[0]);
                { abre novamente}
                msSql := dmDados.SqlVersao('ARQ_0008', gaParm);
                maConexao[0] := dmDados.ExecutarSelect(msSql);
                { se Achou - Seguro }
                if not dmDados.Status(maConexao[0], IOSTATUS_EOF) then
                begin
                  { Achou}
                  lbAchou := True;
                  lbSeguro := True;
                end
                else
                begin
                  { Fecha a Conexao}
                  dmDados.FecharConexao(maConexao[0]);
                end;
              end;
            end;
          end;
        end;
        Screen.Cursor := crDefault;

        { se achou}
        if lbAchou then
        begin
          lvValor := dmDados.ValorColuna(maConexao[0], 'DATA_RECEB', miIndCol[0]);
          if lvValor <> 0 then
            lvValor := FormatDateTime(MK_DATATELA, lvValor)
          else
            lvValor := VAZIO_DATA;
          mskBaixaDataReceb.Text := lvValor;
          lvValor := dmDados.ValorColuna(maConexao[0], 'DATA_CREDI', miIndCol[1]);
          if lvValor <> 0 then
            lvValor := FormatDateTime(MK_DATATELA, lvValor)
          else
            lvValor := VAZIO_DATA;
          mskBaixaDataCredito.Text := lvValor;
          lvValor := dmDados.ValorColuna(maConexao[0], 'D_VENCTO', miIndCol[6]);
          if lvValor <> 0 then
            lvValor := FormatDateTime(MK_DATATELA, lvValor)
          else
            lvValor := VAZIO_DATA;
          mskBaixaDataVenc.Text := lvValor;
          untCombos.gp_Combo_Posiciona(cboBaixaPagto, dmDados.ValorColuna(maConexao[0], 'PAGTO', miIndCol[2]));
          untCombos.gp_Combo_Posiciona(cboBaixaCaixaBanco, dmDados.ValorColuna(maConexao[0], 'CAIXA_BANC', miIndCol[3]));
          untCombos.gp_Combo_Posiciona(cboBaixaTipoTaxa, lsTipoTaxa);
          if lbServico then
            untCombos.gp_Combo_Posiciona(cboBaixaTipoReceb, 'V')
          else if lbSeguro then
            untCombos.gp_Combo_Posiciona(cboBaixaTipoReceb, 'S')
          else
            untCombos.gp_Combo_Posiciona(cboBaixaTipoReceb, lsTipoDoc);
          mskBaixaValorDesconto.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'VLR_DESC', miIndCol[4]));
          mskBaixaValorRecebido.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'VLR_RECEB', miIndCol[5]));
          mskBaixaValorOriginal.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'VLR_PARC', miIndCol[7]));

          edtBaixaCodigo.Text := dmDados.ValorColuna(maConexao[0], 'CODIGO', miIndCol[8]);
          edtBaixaProponente.Text := dmDados.ValorColuna(maConexao[0], 'DESCRICAO', miIndCol[9]);
          edtBaixaProposta.Text := Copy(edtBaixaCodigo.Text, 1, 8);
          edtBaixaTipoDoc.Text := lsTipoDoc;
          edtBaixaNumParc.Text := dmDados.ValorColuna(maConexao[0], 'NUM_PARC', miIndCol[11]);

          { se estiver em aberto }
          if dmDados.ValorColuna(maConexao[0], 'PAGTO', miIndCol[2]) = 'N' then
          begin
            if (mskDataRecebimento.Text <> VAZIO_DATA) and (mskDataRecebimento.Text <> VAZIO) then
            begin
              ShowMessage('Parcela j� Paga [Nosso Numero: ' + Copy(edtBaixaBarras.Text, 20, 10) + ']');

              mskBaixaDataReceb.Enabled := False;
              mskBaixaDataCredito.Enabled := False;
              cboBaixaPagto.Enabled := False;
              cboBaixaCaixaBanco.Enabled := False;
              mskBaixaValorDesconto.Enabled := False;
              mskBaixaValorRecebido.Enabled := False;
              { coloca o foco no codigo de barras }
              if edtBaixaBarras.Enabled then
                edtBaixaBarras.SetFocus;
            end
            else
            begin
              mskBaixaDataReceb.Enabled := True;
              mskBaixaDataCredito.Enabled := True;
              cboBaixaPagto.Enabled := True;
              cboBaixaCaixaBanco.Enabled := True;
              mskBaixaValorDesconto.Enabled := True;
              mskBaixaValorRecebido.Enabled := True;

              mskBaixaDataReceb.Text := FormatDateTime(MK_DATATELA, Date);
              mskBaixaDataCredito.Text := FormatDateTime(MK_DATATELA, Date);
              untCombos.gp_Combo_Posiciona(cboBaixaPagto, 'S');
              untCombos.gp_Combo_Posiciona(cboBaixaCaixaBanco, 'C');
              mskBaixaValorDesconto.Text := FormatFloat(MK_VALOR, 0);
              mskBaixaValorRecebido.Text := untFuncoes.gf_Funcoes_ValorAtual(edtBaixaProposta.Text,
                                            edtBaixaTipoDoc.Text,
                                            mskBaixaValorOriginal.Text,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(mskBaixaValorOriginal.Text))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(mskBaixaValorOriginal.Text))),
                                            mskBaixaDataVenc.Text,
                                            edtBaixaNumParc.Text);
            end;
          end
          else
          begin
            ShowMessage('Parcela j� Paga [Nosso Numero: ' + Copy(edtBaixaBarras.Text, 20, 10) + ']');

            mskBaixaDataReceb.Enabled := False;
            mskBaixaDataCredito.Enabled := False;
            cboBaixaPagto.Enabled := False;
            cboBaixaCaixaBanco.Enabled := False;
            mskBaixaValorDesconto.Enabled := False;
            mskBaixaValorRecebido.Enabled := False;
            { coloca o foco no codigo de barras }
            if edtBaixaBarras.Enabled then
              edtBaixaBarras.SetFocus;
          end;
        end
        else
        begin
          ShowMessage('Parcela n�o Encontrada [Nosso Numero: ' + Copy(edtBaixaBarras.Text, 20, 10) + ']');

          { limpa os controles}
          edtBaixaProponente.Text := VAZIO;
          edtBaixaProposta.Text := VAZIO;
          edtBaixaCodigo.Text := VAZIO;
          edtBaixaTipoDoc.Text := VAZIO;
          edtBaixaNumParc.Text := VAZIO;
          mskBaixaDataReceb.Text := VAZIO;
          mskBaixaDataCredito.Text := VAZIO;
          cboBaixaPagto.Text := VAZIO;
          cboBaixaCaixaBanco.Text := VAZIO;
          mskBaixaValorDesconto.Text := FormatFloat(MK_VALOR, 0);
          mskBaixaValorRecebido.Text := FormatFloat(MK_VALOR, 0);
          mskBaixaDataVenc.Text := VAZIO;
          mskBaixaValorOriginal.Text := FormatFloat(MK_VALOR, 0);
          cboBaixaTipoTaxa.Text := VAZIO;
          cboBaixaTipoReceb.Text := VAZIO;
          { coloca o foco no codigo de barras }
          if edtBaixaBarras.Enabled then
            edtBaixaBarras.SetFocus;
        end;
        { Fecha a Conexao}
        dmDados.FecharConexao(maConexao[0]);
      end;
    end;

  except
    dmDados.ErroTratar('edtGenericoExit - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.mskGenericoExit      Faz as devidas modificacoes
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.mskGenericoExit(Sender: TObject);
var
  lmskTexto: TMaskEdit;
  lsTexto:  string;
begin

  try
    lmskTexto := TMaskEdit(Sender);

    if lmskTexto.Text <> VAZIO then
    begin
      { se for Visual Gaveta}
      If grProcessos.sNomePainel = VISUAL_GAVETA then
      begin
        { coloca zeros a esquerda }
        lsTexto := lmskTexto.Text;
        lmskTexto.Text := Copy('00000000' + lsTexto, Length(Trim(lsTexto)) + 1, 8);
      end;
      { se for Quitar Parcela }
      If grProcessos.sNomePainel = QUITAR_PARCELAS then
      begin
        if lmskTexto.Name = 'mskNumParcQuitarPag' then
        begin
          { coloca zeros a esquerda }
          lsTexto := lmskTexto.Text;
          lmskTexto.Text := Copy('00' + lsTexto, Length(Trim(lsTexto)) + 1, 2);
          { muda as linhas do spread }
          grdChequesQuitarPag.MaxRows := StrToInt(lmskTexto.Text);
        end;
      end;
      { se for Lancar Servico }
      If grProcessos.sNomePainel = LANCAR_SERVICO then
      begin
        if lmskTexto.Name = 'mskCodigoPropLancarServico' then
        begin
          mskCodigoPropLancarServico.Text := untFuncoes.gf_Zero_Esquerda(mskCodigoPropLancarServico.Text, 8);

          gaParm[0] := 'NOME_PROP';
          gaParm[1] := 'PROPONEN';
          gaParm[2] := 'NUM_PROP = ''' + mskCodigoPropLancarServico.Text + '''';
          msSql := dmDados.SqlVersao('NEC_0000', gaParm);
          maConexao[0] := dmDados.ExecutarSelect(msSql);
          if maConexao[0] = IOPTR_NOPOINTER then
            Exit;
          { existe }
          if not dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
          begin
            miIndCol[0] := IOPTR_NOPOINTER;
            edtNomePropLancarServico.Text := dmDados.ValorColuna(maConexao[0], 'NOME_PROP', miIndCol[0]);
          end;
          dmDados.FecharConexao(maConexao[0]);
        end;
        if lmskTexto.Name = 'mskNumParcLancarServico' then
        begin
          { coloca zeros a esquerda }
          lsTexto := lmskTexto.Text;
          lmskTexto.Text := Copy('00' + lsTexto, Length(Trim(lsTexto)) + 1, 2);
          { muda as linhas do spread }
          grdChequesLancarServico.MaxRows := StrToInt(lmskTexto.Text);
        end;
      end;
    end;

  except
    dmDados.ErroTratar('mskGenericoExit - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.lp_ConfirmaAtualizacoes      Faz as Consistencias e grava
 *
 *-----------------------------------------------------------------------------}
function TformProcessos.lf_ConfirmaAtualizacoes: Boolean;
begin

  try
    { a principio tem erros}
    Result := False;

    { Zera o acumulador de mensagens de erro}
    giNumErros := 0;
    gbErro := False;

    {  se nao tiver erro}
    if lf_Consiste_Processo then
    begin
      { se nao precisa gravar no historico}
      if not grProcessos.bGuardaHistorico then
        { faz a alteracao}
        lp_Atualiza_Processo
      else
        { se conseguiu gravar no historico}
        if lf_Grava_Processo then
          { faz a alteracao}
          lp_Atualiza_Processo;
    end
    else
    begin
      { apresenta mensagem de altualizacao nao feita}
      dmDados.MensagemExibir('', 4601);
      Exit;
    end;

    if gbProcessoOK then
      { tudo certo }
      Result := True;

  except
    dmDados.ErroTratar('lf_ConfirmaAtualizacoes - uniProcessos.Pas');
    Result := False;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.lf_Consiste_Processo      Faz as Consistencias
 *
 *-----------------------------------------------------------------------------}
function TformProcessos.lf_Consiste_Processo: Boolean;
var
  liConta:      Integer;
  lbTemMarcado: Boolean;
  ldTotal:      Double;
  ldTotalItem:  Double;
begin
  try
    { a principio tem erros}
    Result := False;

    gbErro := False;

    { se for Visual Gaveta}
    if grProcessos.sNomePainel = VISUAL_GAVETA then
    begin
      { se o Numero da Proposta estiver vazio}
      if mskNumProposta.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''N�mero da Proposta''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se o digito verificador Numero da Proposta estiver errado}
//      if not untDigito.gf_Digito_ChecarProposta(mskNumProposta.Text) then
//      begin
        { acumula menasgem de erro de grava��o}
//        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROCGCCPF, '''N�mero da Proposta''', mskNumProposta.Text, VAZIO);
//        gbErro := True;
//      end;
    end;
    { se for Procura Nome}
    if grProcessos.sNomePainel = PROCURA_NOME then
    begin
      { se o Nome estiver vazio}
      if edtProcuraNome_Nome.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Nome''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;
    { se for Incluir Quadra}
    if grProcessos.sNomePainel = INC_QUADRA then
    begin
    end;
    { se for Alterar Quadra}
    if grProcessos.sNomePainel = ALT_QUADRA then
    begin
    end;
    { se for Implantar Quadra}
    if grProcessos.sNomePainel = IMP_QUADRA then
    begin
    end;
    { se for Plano Automatico}
    if grProcessos.sNomePainel = PLANO_AUTO then
    begin
    end;
    { se for Comissao Automatico}
    if grProcessos.sNomePainel = COMIS_AUTO then
    begin
    end;
    { se for Gerar Taxas}
    if grProcessos.sNomePainel = GERA_TAXA then
    begin
      { se o Referencia Taxa estiver vazio}
      if mskTaxaReferencia.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Taxa de Refer�ncia''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se o Valor Taxa estiver vazio}
      if mskTaxaValor.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Valor da Taxa''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se o Tipo Taxa estiver vazio}
      if cboTaxaFixa.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo do Indice''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;
    { se for Gerar Parcelas}
    if grProcessos.sNomePainel = GERA_PARCELA then
    begin
      { se o Proposta Inicial estiver vazio}
      if mskPropostaInicialGeraParcela.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Proposta Inicial''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se o Proposta Final estiver vazio}
      if mskPropostaFinalGeraParcela.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Proposta Final''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se o Tipo Parcela estiver vazio}
      if cboTipoParcelaGeraParcela.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo da Parcela''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;
    { se for Gerar Seguro}
    if grProcessos.sNomePainel = GERA_SEGURO then
    begin
      { se o Referencia Seguro estiver vazio}
      if mskSeguroReferencia.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Seguro de Refer�ncia''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se a Proposta Inicial estiver vazio}
      if mskSeguroPropIni.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''No. Proposta Inicial''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se a Proposta Final estiver vazio}
      if mskSeguroPropFim.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''No. Proposta Final''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;
    { se for Gerar Seguradora}
    if grProcessos.sNomePainel = GERA_SEGURADORA then
    begin
      { se o Referencia Seguro estiver vazio}
      if mskSeguraReferencia.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Seguro de Refer�ncia''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se a Proposta Inicial estiver vazio}
      if mskSeguraPropIni.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''No. Proposta Inicial''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se a Proposta Final estiver vazio}
      if mskSeguraPropFim.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''No. Proposta Final''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;
    { se for Retorno Banco}
    if grProcessos.sNomePainel = RETORNO_BANCO then
    begin
      {posiciona na coluna do Arquivo}
      grdArquivosRetBanco.Col := 2;

      if grdArquivosRetBanco.MaxRows = 0 then
        gbErro := True;

      {varre a lista procurando alguma item marcado}
      for liConta := 1 to grdArquivosRetBanco.MaxRows do
      begin
        grdArquivosRetBanco.Row := liConta;
        {se o tiver Algo}
        if grdArquivosRetBanco.Text <> VAZIO then
        begin
          { nao tem erro e para}
          gbErro := False;
          Break;
        end
        else
          { pode continuar tendo erro e continua a procurar}
          gbErro := True;
      end;

      { se tem erros}
      if gbErro then
        untTelaVar.gp_TelaVar_Mensagem_Acumular(10505, '''Algum item tem que ser selecionado''', VAZIO, VAZIO);

      { se o layout estiver vazio}
      if cboLayoutRetBanco.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Layout''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;
    { se for Envio Banco}
    if grProcessos.sNomePainel = ENVIO_ARQUIVO then
    begin
      { se o banco estiver vazio}
      if cboBancoEnvioArqui.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Banco''', VAZIO, VAZIO);
        gbErro := True;
      end;

      { se for arquivo }
      if untCombos.gf_Combo_SemDescricao(cboTipoEnvioReenvioArqui) = '1' then
      begin
        { se o layout estiver vazio}
        if cboLayoutEnvioArqui.Text = VAZIO then
        begin
          { acumula menasgem de erro de grava��o}
          untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Layout''', VAZIO, VAZIO);
          gbErro := True;
        end;
        { se o nome arquiuvo estiver vazio}
        if edtNomeArquivoEnvioArqui.Text = VAZIO then
        begin
          { acumula menasgem de erro de grava��o}
          untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Nome do Arquivo''', VAZIO, VAZIO);
          gbErro := True;
        end;
      end;

      { se o nome arquiuvo estiver vazio}
      if cboTipoParcelaEnvioArqui.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo de Parcela''', VAZIO, VAZIO);
        gbErro := True;
      end;

      { se o nome arquiuvo estiver vazio}
      if cboTipoEnvioEnvioArqui.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo de Envio''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;
    { se for Reenvio Banco}
    if grProcessos.sNomePainel = REENVIO_ARQUIVO then
    begin
      { se o layout estiver vazio}
      if cboTipoParcelaReenvioArqui.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo Parcela''', VAZIO, VAZIO);
        gbErro := True;
      end;

      { se o layout estiver vazio}
      if cboTipoEnvioReenvioArqui.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo Envio''', VAZIO, VAZIO);
        gbErro := True;
      end;

      { se o banco estiver vazio}
      if cboBancoReenvioArqui.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Banco''', VAZIO, VAZIO);
        gbErro := True;
      end;

      { se for arquivo }
      if untCombos.gf_Combo_SemDescricao(cboTipoEnvioReenvioArqui) = '1' then
      begin
        { se o layout estiver vazio}
        if cboLayoutReenvioArqui.Text = VAZIO then
        begin
          { acumula menasgem de erro de grava��o}
          untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Layout''', VAZIO, VAZIO);
          gbErro := True;
        end;

        { se o nome arquiuvo estiver vazio}
        if edtNomeArquivoReenvioArqui.Text = VAZIO then
        begin
          { acumula menasgem de erro de grava��o}
          untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Nome do Arquivo''', VAZIO, VAZIO);
          gbErro := True;
        end;
      end;
    end;

    { se for Reajuste de Parcelas }
    if grProcessos.sNomePainel = REAJUSTE_PARCELAS then
    begin
      { se o tipo da parcela estiver vazio}
      if cboTipoParcelaReajuste.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo Parcela''', VAZIO, VAZIO);
        gbErro := True;
      end;

      { se a proposta inicial estiver vazio}
      if mskPropostaInicialReajuste.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Proposta Inicial''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se a proposta final estiver vazio}
      if mskPropostaFinalReajuste.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Proposta Final''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;

    { se for Estorno Parcelas}
    if grProcessos.sNomePainel = ESTORNO_PARCELAS then
    begin
      { se o tipo pagto estiver vazio}
      if cboRecebPagto.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Pagto''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;

    { se for Baixa Automatica }
    if grProcessos.sNomePainel = BAIXA_AUTO then
    begin
      { se o valor recebido estiver vazio}
      if mskBaixaValorRecebido.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Valor Recebido''', VAZIO, VAZIO);
        gbErro := True;
      end
      else
      begin
        { se o valor recebido estiver zerado}
        if StrToFloat(dmDados.TirarSeparador(mskBaixaValorRecebido.Text)) = 0 then
        begin
          { acumula menasgem de erro de grava��o}
          untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Valor Recebido''', VAZIO, VAZIO);
          gbErro := True;
        end;
      end;
      { se a data de recebimento estiver vazio}
      if mskBaixaDataReceb.Text = VAZIO_DATA then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Data de Recebimento''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se a data de credito estiver vazio}
      if mskBaixaDataCredito.Text = VAZIO_DATA then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Data de Cr�dito''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se o tipo pagto estiver vazio}
      if cboBaixaPagto.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Pagto''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se o tipo Caixa/Banco estiver vazio}
      if cboBaixaCaixaBanco.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Pagto''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;

    { se for Quitar Parcelas}
    if grProcessos.sNomePainel = QUITAR_PARCELAS then
    begin
      lbTemMarcado := False;

      {varre a lista procurando alguma item marcado}
      for liConta := 1 to grdParcelasQuitarPag.MaxRows do
      begin
        grdParcelasQuitarPag.Row := liConta;
        {posiciona na coluna do Check}
        grdParcelasQuitarPag.Col := COL_QUIT_CHECK;
        {se o tiver marcado}
        if grdParcelasQuitarPag.Text = '1' then
        begin
          { existe item marcado}
          lbTemMarcado := True;

          {posiciona na coluna do Data recebimento}
          grdParcelasQuitarPag.Col := COL_QUIT_DATARECEB;
          { se tiver vazio}
          if grdParcelasQuitarPag.Text = VAZIO then
          begin
            { acumula menasgem de erro de grava��o}
            untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Data de Recebimento (' + IntToStr(liConta) + ')''', VAZIO, VAZIO);
            gbErro := True;
          end;

          {posiciona na coluna do Valor de Recebimento}
          grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
          { se tiver vazio}
          if (grdParcelasQuitarPag.Text = '0.00') or (grdParcelasQuitarPag.Text = VAZIO) then
          begin
            { acumula menasgem de erro de grava��o}
            untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Valor do Recebimento (' + IntToStr(liConta) + ')''', VAZIO, VAZIO);
            gbErro := True;
          end;

          { quitacao ou recibo parcial }
          if (grProcessos.sCod_Processo = PROCESSO_QUITAR) or (grProcessos.sCod_Processo = PROCESSO_SERVICO) or (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
          begin
            {posiciona na coluna do Forma de Pagamento}
            grdParcelasQuitarPag.Col := COL_QUIT_TIPOBAIXA;
            untCombos.gp_Combo_SpreadPosiciona(grdParcelasQuitarPag, grdParcelasQuitarPag.Text);
            { se tiver vazio}
            if grdParcelasQuitarPag.Text = VAZIO then
            begin
              { acumula menasgem de erro de grava��o}
              untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo de Baixa (' + IntToStr(liConta) + ')''', VAZIO, VAZIO);
              gbErro := True;
            end
            else
            begin
              if untCombos.gf_Combo_SpreadSemDescricao(grdParcelasQuitarPag) = 'B' then
              begin
                untTelaVar.gp_TelaVar_Mensagem_Acumular(4000, '''Tipo Baixa''', '''B - Banco''', VAZIO);
                gbErro := True;
              end;
            end;
          end;
        end;
      end;

      { se nao tem nunhum item marcado da erro }
      if not lbTemMarcado then
      begin
        untTelaVar.gp_TelaVar_Mensagem_Acumular(10505, '''Algum item tem que ser selecionado''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;
    { se for Lancar Servico }
    if grProcessos.sNomePainel = LANCAR_SERVICO then
    begin
      { se a proposta estiver vazio}
      if mskCodigoPropLancarServico.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Proposta''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se a data estiver vazio}
      if mskDataLancarServico.Text = VAZIO_DATA then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Data''', VAZIO, VAZIO);
        gbErro := True;
      end;
      { se o numero do recibo estiver vazio}
      if edtReciboLancarServico.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Recibo''', VAZIO, VAZIO);
        gbErro := True;
      end;

      lbTemMarcado := False;

      {varre a lista procurando alguma item marcado}
//      for liConta := 1 to grdParcelasLancarServico.MaxRows do
      for liConta := 1 to grdParcelasLancarServico.DataRowCnt do
      begin
        grdParcelasLancarServico.Row := liConta;
        {posiciona na coluna do servico }
//        grdParcelasLancarServico.Col := COL_SERV_TIPO;
        { se nao tiver vazio}
//        if grdParcelasLancarServico.Text <> VAZIO then
//        begin
          { existe item marcado}
//          lbTemMarcado := True;
//          break;
//        end;
        {posiciona na coluna do valor }
        grdParcelasLancarServico.Col := COL_SERV_TOTAL;
        { se nao tiver vazio}
        if grdParcelasLancarServico.Text <> VAZIO then
          ldTotal := ldTotal + StrToFloat(dmDados.TirarSeparador(grdParcelasLancarServico.Text));
      end;

      { se nao tem nunhum item marcado da erro }
//      if not lbTemMarcado then
      if grdParcelasLancarServico.DataRowCnt = 0 then
      begin
        untTelaVar.gp_TelaVar_Mensagem_Acumular(10505, '''Algum Servi�o tem que ser selecionado''', VAZIO, VAZIO);
        gbErro := True;
      end;

      lbTemMarcado := False;
      ldTotalItem := 0;
      {varre a lista procurando alguma item marcado}
      for liConta := 1 to grdChequesLancarServico.DataRowCnt do
      begin
        grdChequesLancarServico.Row := liConta;
        {posiciona na coluna do servico }
        grdChequesLancarServico.Col := COL_CHQ_VALORPARC;
        { se nao tiver vazio}
        if grdChequesLancarServico.Text <> VAZIO then
          ldTotalItem := ldTotalItem + StrToFloat(dmDados.TirarSeparador(grdChequesLancarServico.Text));
      end;

      { se nao tem nunhum item marcado da erro }
      if chkDinheiroLancarServico.Checked then
      begin
//      if not lbTemMarcado then
        if grdChequesLancarServico.DataRowCnt = 0 then
        begin
          untTelaVar.gp_TelaVar_Mensagem_Acumular(10505, '''Alguma Parcela tem que ser selecionada''', VAZIO, VAZIO);
          gbErro := True;
        end;
      end;

      { se os totais nao coincidem }
     { if ldTotal > ldTotalItem then
      begin
        untTelaVar.gp_TelaVar_Mensagem_Acumular(4016, '''Total de Servi�os''', '''Total de Parcelas''', VAZIO);
        gbErro := True;
      end;}   
    end;
    { se for Incluir Imagens}
    if grProcessos.sNomePainel = INCLUIR_IMAGENS then
    begin
      {posiciona na coluna do Arquivo}
      grdIncluirImagens.Col := 2;

      if grdIncluirImagens.MaxRows = 0 then
        gbErro := True;

      { se tem erros}
      if gbErro then
        untTelaVar.gp_TelaVar_Mensagem_Acumular(10505, '''Algum item tem que ser selecionado''', VAZIO, VAZIO);

      { se o tipo imagem estiver vazio}
      if cboTipoImagens.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;
    { se for Carta Cobranca }
    if grProcessos.sNomePainel = CARTA_COBRANCA then
    begin
      { se o Modelo estiver vazio}
      if cboModeloCartaCobranca.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Modelo''', VAZIO, VAZIO);
        gbErro := True;
      end;

      { se o tipo proposta estiver vazio}
//      if cboTipoPropostaCartaCobranca.Text = VAZIO then
//      begin
        { acumula menasgem de erro de grava��o}
//        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Tipo de Proposta''', VAZIO, VAZIO);
//        gbErro := True;
//      end;

      { se a proposta inicial estiver vazio}
      if mskPropostaInicialCartaCobranca.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Proposta Inicial''', VAZIO, VAZIO);
        gbErro := True;
      end;

      { se a proposta final estiver vazio}
      if mskPropostaFinalCartaCobranca.Text = VAZIO then
      begin
        { acumula menasgem de erro de grava��o}
        untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROVAZIO, '''Proposta Final''', VAZIO, VAZIO);
        gbErro := True;
      end;
    end;

    { se tem erros}
    if gbErro then
    begin
      { S� para quando � a��o com grava��o}
      giErros_Modo := ERRO_MODO_PARAMETROS;
      untErros.gp_GridErros_Carregar;
      Exit;
    end;
    { Porque n�o houveram erros}
    giErros_Modo := ERRO_MODO_NORMAL;
    { tudo Certo}
    Result := True;

  except
    dmDados.ErroTratar('lf_Consiste_Processo - uniProcessos.Pas');
    Result := False;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.lf_Grava_Processo      grava no Historico
 *
 *-----------------------------------------------------------------------------}
function TformProcessos.lf_Grava_Processo: Boolean;
var
  lwConta: Word;
  lvCodigo: Variant;
begin
  try
    { a principio tem erros}
    Result := False;

    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    gaParm[0] := 'SEG_TabOperacao';
    gaParm[1] := 'Nome_Usuario = ''' + gsNomeUsuario + '''';
    gaParm[2] := 'Nome_Usuario';
    msSql := dmDados.SqlVersao('NEC_0002', gaParm);
    miConexaoHistorico := dmDados.ExecutarSelect(msSql);
    if miConexaoHistorico = IOPTR_NOPOINTER then
      Exit;

    dmDados.Ultimo(miConexaoHistorico);

    lvCodigo := dmDados.ValorColuna(miConexaoHistorico, 'Codigo', miIndCol[0]);
    { se nao tiver codigo}
    if VarAsType(lvCodigo, VarString) = IOVAL_NOVALUE then
      lvCodigo := 1
    else
      Inc(lvCodigo);

    dmDados.gsRetorno := dmDados.AtivaModo(miConexaoHistorico,IOMODE_INSERT);

    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'Codigo', miIndCol[1], lvCodigo);
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'Nome_Usuario', miIndCol[2], gsNomeUsuario);
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'Status_Processo', miIndCol[3], 'C');
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'Id_Processo', miIndCol[4], giId_Processo);
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'DtHora_Inicial', miIndCol[5], Date);
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'DtHora_Final', miIndCol[6], Date);
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'Opcao', miIndCol[7], grProcessos.sNomePainel);
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'Operacao', miIndCol[8], 'Consulta');
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'Cadastro', miIndCol[9], '');
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoHistorico, 'Chave', miIndCol[10], '');

    { faz a atualizacao}
    dmDados.gsRetorno := dmDados.Incluir(miConexaoHistorico, 'SEG_TabOperacao');

    if dmDados.gsRetorno = IORET_OK then
      Result := True;

    dmDados.FecharConexao(miConexaoHistorico);

  except
    dmDados.ErroTratar('lf_Grava_Processo - uniProcessos.Pas');
    Result := False;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.lp_Atualiza_Processo      grava
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.lp_Atualiza_Processo;
var
  lwConta: Word;
  liConta: Integer;
  laParams: array[0..10] of TSParams;
  lvCampo: Variant;
  lsRetorno: string;
  liRetorno: Integer;
  liTaxa: Integer;

  lsEnvio: string;
  lsBanco: string;
  lsNumBanco: string;
  lsAgencia: string;
  lsConta: string;

  lsDesconto: string;
  lsDescontoU: string;
  lsDescontoS: string;
  lsDescontoB: string;

  lsModelo: string;
  lsProposta: string;
  lsOcupacao: string;
  lsDebito: string;
  lsValores: string;
  lsAtualizado: string;
  lsVerso: string;
  liTotal: Integer;
  lbOK: Boolean;
  ldDataInicial: TDateTime;
  ldDataFinal: TDateTime;
  lsFiltro: string;

  lsNumProp: string;
  lsValorVenc: string;
  lsDataVenc: string;
  lsParcela: string;
  lsTipoProp: string;
  lsTipoTaxa: string;
  lsServico: string;

  lsSql: string;
  lsCodigo: string;
begin
  try

    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    { limpa a formula do relatorio}
//    for lwConta := 0 to 100 do
//      formImprimir.rptRelatorio.Formulas[liConta] := VAZIO;
//    formImprimir.rptRelatorio.SelectionFormula := VAZIO;

    { se for Visual Gaveta}
    if grProcessos.sNomePainel = VISUAL_GAVETA then
    begin
      { guarda o numero da proposta }
      gsNumProposta := mskNumProposta.Text;
      { chama a tela de visual gavetas }
      uniVisualizacaoGavetas.formVisualGaveta.Show;
    end;
    { se for Procura Nome}
    if grProcessos.sNomePainel = PROCURA_NOME then
    begin
      { chama a tela de visual gavetas }
      uniProcuraNome.formProcuraNome.Show;
    end;
    { se for Incluir Quadra}
    if grProcessos.sNomePainel = INC_QUADRA then
    begin
      laParams[0].sNome := '@Quadra';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftString;
      laParams[0].vValor := untCombos.gf_Combo_SemDescricao(cboIncQuadra);  //mskIncQuadra.Text;
      laParams[1].sNome := '@Setor';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftString;
      laParams[1].vValor := mskIncSetor.Text;
      laParams[2].sNome := '@cLoteIni';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := mskIncLoteIni.Text;
      laParams[3].sNome := '@cLoteFim';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftString;
      laParams[3].vValor := mskIncLoteFim.Text;
      laParams[4].sNome := '@Tipo';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftString;
      laParams[4].vValor := untCombos.gf_Combo_SemDescricao(cboIncTipoLote);
      laParams[5].sNome := '@TipoProp';
      laParams[5].ptTipo := ptInput;
      laParams[5].ftTipoDado := ftString;
      laParams[5].vValor := untCombos.gf_Combo_SemDescricao(cboIncImplantado);
      laParams[6].sNome := '@DataAlvara';
      laParams[6].ptTipo := ptInput;
      laParams[6].ftTipoDado := ftString;
      if mskIncDataAlvara.Text = VAZIO_DATA then
        laParams[6].vValor := VAZIO
      else
        laParams[6].vValor := mskIncDataAlvara.Text;
      laParams[7].sNome := '@DataConstr';
      laParams[7].ptTipo := ptInput;
      laParams[7].ftTipoDado := ftString;
      if mskIncDataConstrucao.Text = VAZIO_DATA then
        laParams[7].vValor := VAZIO
      else
        laParams[7].vValor := mskIncDataConstrucao.Text;
      laParams[8].sNome := '@Alvara';
      laParams[8].ptTipo := ptInput;
      laParams[8].ftTipoDado := ftString;
      laParams[8].vValor := mskIncAlvara.Text;
      laParams[9].sNome := '@Lado';
      laParams[9].ptTipo := ptInput;
      laParams[9].ftTipoDado := ftString;
      laParams[9].vValor := untCombos.gf_Combo_SemDescricao(cboIncLado);
      dmDados.CriarStored('Inc_Quadra', laParams, 9);
      { Inicializa variavel de sucesso }
//      gbProcessoOK := True;
    end;
    { se for Alterar Quadra}
    if grProcessos.sNomePainel = ALT_QUADRA then
    begin
      laParams[0].sNome := '@Quadra';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftString;
      laParams[0].vValor := untCombos.gf_Combo_SemDescricao(cboAltQuadra);  //mskAltQuadra.Text;
      laParams[1].sNome := '@Setor';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftString;
      laParams[1].vValor := mskAltSetor.Text;
      laParams[2].sNome := '@LoteIni';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := mskAltLoteIni.Text;
      laParams[3].sNome := '@LoteFim';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftString;
      laParams[3].vValor := mskAltLoteFim.Text;
      laParams[4].sNome := '@Tipo';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftString;
      laParams[4].vValor := untCombos.gf_Combo_SemDescricao(cboAltTipoLote);
      laParams[5].sNome := '@TipoProp';
      laParams[5].ptTipo := ptInput;
      laParams[5].ftTipoDado := ftString;
      laParams[5].vValor := untCombos.gf_Combo_SemDescricao(cboAltImplantado);
      laParams[6].sNome := '@DataAlvara';
      laParams[6].ptTipo := ptInput;
      laParams[6].ftTipoDado := ftString;
      if mskAltDataAlvara.Text = VAZIO_DATA then
        laParams[6].vValor := VAZIO
      else
        laParams[6].vValor := StrToDate(mskAltDataAlvara.Text);
      laParams[7].sNome := '@DataConstr';
      laParams[7].ptTipo := ptInput;
      laParams[7].ftTipoDado := ftString;
      if mskAltDataConstrucao.Text = VAZIO_DATA then
        laParams[7].vValor := VAZIO
      else
        laParams[7].vValor := StrToDate(mskAltDataConstrucao.Text);
      laParams[8].sNome := '@Alvara';
      laParams[8].ptTipo := ptInput;
      laParams[8].ftTipoDado := ftString;
      laParams[8].vValor := mskAltAlvara.Text;
      laParams[9].sNome := '@Lado';
      laParams[9].ptTipo := ptInput;
      laParams[9].ftTipoDado := ftString;
      laParams[9].vValor := untCombos.gf_Combo_SemDescricao(cboAltLado);
      dmDados.CriarStored('Alt_Quadra', laParams, 9);
      { Inicializa variavel de sucesso }
//      gbProcessoOK := True;
    end;
    { se for Implantar Quadra}
    if grProcessos.sNomePainel = IMP_QUADRA then
    begin
      laParams[0].sNome := '@Quadra';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftString;
      laParams[0].vValor := untCombos.gf_Combo_SemDescricao(cboImpQuadra);  //mskImpQuadra.Text;
      laParams[1].sNome := '@Setor';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftString;
      laParams[1].vValor := mskImpSetor.Text;
      laParams[2].sNome := '@LoteIni';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := mskImpLoteIni.Text;
      laParams[3].sNome := '@LoteFim';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftString;
      laParams[3].vValor := mskImpLoteFim.Text;
      laParams[4].sNome := '@TipoProp';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftString;
      laParams[4].vValor := untCombos.gf_Combo_SemDescricao(cboImplantado);
      dmDados.CriarStored('Imp_Quadra', laParams, 4);
      { Inicializa variavel de sucesso }
//      gbProcessoOK := True;
    end;
    { se for Plano Automatico}
    if grProcessos.sNomePainel = PLANO_AUTO then
    begin
      laParams[0].sNome := '@TipoProp';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftString;
      laParams[0].vValor := untCombos.gf_Combo_SemDescricao(cboPlanoTipoProposta);
      laParams[1].sNome := '@DataIni';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftDateTime;
      laParams[1].vValor := StrToDate(DateToStr(dtpPlanoDataIncl.Date));
      laParams[2].sNome := '@Erro';
      laParams[2].ptTipo := ptOutput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := VAZIO;
      lsRetorno := dmDados.CriarStored('Plano_Auto', laParams, 2);
      if lsRetorno <> '0' then
        liRetorno := MessageDlg(lsRetorno, mtError, [mbOK], 0);
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;
    { se for Comissao Automatico}
    if grProcessos.sNomePainel = COMIS_AUTO then
    begin
      laParams[0].sNome := '@TipoCom';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftString;
      laParams[0].vValor := untCombos.gf_Combo_SemDescricao(cboComisTipoProposta);
      laParams[1].sNome := '@DataIni';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftDateTime;
      laParams[1].vValor := StrToDate(DateToStr(dtpComisDataIncl.Date));
      laParams[2].sNome := '@Erro';
      laParams[2].ptTipo := ptOutput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := VAZIO;
      lsRetorno := dmDados.CriarStored('Comis_Auto', laParams, 2);
      if lsRetorno <> '0' then
        liRetorno := MessageDlg(lsRetorno, mtError, [mbOK], 0);
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;
    { se for Gerar Taxas}
    if grProcessos.sNomePainel = GERA_TAXA then
    begin
      laParams[0].sNome := '@DataIni';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftDateTime;
      laParams[0].vValor := StrToDate(DateToStr(dtpTaxaDataVenc.Date));
      laParams[1].sNome := '@ValorTaxa';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftCurrency;
      laParams[1].vValor := mskTaxaValor.Text;
      laParams[2].sNome := '@TaxaFixa';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftSmallint;
//      if untCombos.gf_Combo_SemDescricao(cboTaxaFixa) = 'S' then
//        laParams[2].vValor := 1
//      else
//        laParams[2].vValor := 0;
      laParams[2].vValor := StrToInt(untCombos.gf_Combo_SemDescricao(cboTaxaFixa));
      laParams[3].sNome := '@Data';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftString;
      laParams[3].vValor := mskTaxaReferencia.Text;
      laParams[4].sNome := '@NumIni';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftString;
      laParams[4].vValor := mskPropostaInicialGeraTaxa.Text;
      laParams[5].sNome := '@NumFim';
      laParams[5].ptTipo := ptInput;
      laParams[5].ftTipoDado := ftString;
      laParams[5].vValor := mskPropostaFinalGeraTaxa.Text;
      dmDados.CriarStored('Gera_Taxa', laParams, 5);
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;
    { se for Gerar Parcelas}
    if grProcessos.sNomePainel = GERA_PARCELA then
    begin
      for liConta := StrToInt(mskPropostaInicialGeraParcela.Text) to StrToInt(mskPropostaInicialGeraParcela.Text) do
      begin
        laParams[0].sNome := '@Proposta';
        laParams[0].ptTipo := ptInput;
        laParams[0].ftTipoDado := ftString;
        laParams[0].vValor := FormatFloat('00000000', liConta);
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaGeraParcela) = 'L' then
          dmDados.CriarStored('ArrumaParcelasLote', laParams, 0)
        else if untCombos.gf_Combo_SemDescricao(cboTipoParcelaGeraParcela) = 'G' then
          dmDados.CriarStored('ArrumaParcelasGaveta', laParams, 0);
      end;
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;
    { se for Gerar Seguro}
    if grProcessos.sNomePainel = GERA_SEGURO then
    begin
      laParams[0].sNome := '@NumIni';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftString;
      laParams[0].vValor := mskSeguroPropIni.Text;
      laParams[1].sNome := '@NumFim';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftString;
      laParams[1].vValor := mskSeguroPropFim.Text;
      laParams[2].sNome := '@Data';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := mskSeguroReferencia.Text;
      laParams[3].sNome := '@DataIni';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftDateTime;
      laParams[3].vValor := StrToDate(DateToStr(dtpSeguroDataInicial.Date));
      laParams[4].sNome := '@DataFim';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftDateTime;
      laParams[4].vValor := StrToDate(DateToStr(dtpSeguroDataFinal.Date));
      dmDados.CriarStored('Gera_Seguro', laParams, 4);
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;
    { se for Gerar Seguradora}
    if grProcessos.sNomePainel = GERA_SEGURADORA then
    begin
      laParams[0].sNome := '@DataIni';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftDateTime;
      laParams[0].vValor := StrToDate(DateToStr(dtpSeguraDataInicial.Date));
      laParams[1].sNome := '@DataFim';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftDateTime;
      laParams[1].vValor := StrToDate(DateToStr(dtpSeguraDataFinal.Date));
      laParams[2].sNome := '@NumIni';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := mskSeguraPropIni.Text;
      laParams[3].sNome := '@NumFim';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftString;
      laParams[3].vValor := mskSeguraPropFim.Text;
      laParams[4].sNome := '@Data';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftString;
      laParams[4].vValor := mskSeguraReferencia.Text;
      dmDados.CriarStored('Gera_Seguradora', laParams, 4);
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;
    { se for Grafico de Lote Total }
    if grProcessos.sNomePainel = GRAPH_LOTETOTAL then
    begin
      { pega a origem dos dados }
//      gaParm[0] := 'YEAR(PROPLOTE.DATAPROP) Ano, SUM(TAB_LOTE.REF_STAND) Venda';
//      gaParm[1] := 'PROPLOTE INNER JOIN TAB_LOTE ON PROPLOTE.TIPO_LOTE = TAB_LOTE.CODIGO';
//      gaParm[2] := '(PROPLOTE.VALOR_SINA) <> 0 OR (PROPLOTE.VALOR_PARC) <> 0';
//      gaParm[3] := 'YEAR(PROPLOTE.DATAPROP)';
//      gaParm[4] := 'YEAR(PROPLOTE.DATAPROP)';
//      msSql := dmDados.SqlVersao('NEC_0006', gaParm);
      msSql := dmDados.SqlVersao('NEC_0009', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      dmDados.Ultimo(maConexao[0]);

      lvCampo := dmDados.ValorColuna(maConexao[0], 'Ano', miIndCol[0]);

      { limpar as colunas }
      dlgGrafico.qrcsGrafico.XLabelsSource := '';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := '';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := '';
      { atribui a origem dos dados ao grafico }
      dlgGrafico.qrcsGrafico.DataSource := dmDados.gaSql[dmDados.gaConexao[maConexao[0]].iConexao];
      dlgGrafico.qrcsGrafico.XValues.ValueSource := 'Ano';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := 'Venda';

      { atribui o titulo }
      dlgGrafico.qrdbcGrafico.Title.Text.Clear;
      dlgGrafico.qrdbcGrafico.Title.Text.Add('VENDAS TOTAIS DE LOTES');
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      { zerar os eixos }
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 0;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 10000;
      { atribui os valores dos eixos }
      dlgGrafico.qrdbcGrafico.LeftAxis.Increment := 500;
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 1979;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := lvCampo;

      { tamanho da barra }
      dlgGrafico.qrcsGrafico.CustomBarWidth := 0;

      { chama a tela de grafico lote total }
      dlgGrafico.qrGrafico.Preview;

      { fechar a origem dos Dados }
      dmDados.FecharConexao(maConexao[0]);
    end;
    { se for Grafico de Cancelado Lote Total }
    if grProcessos.sNomePainel = GRAPH_LOTECANCTOTAL then
    begin
      { pega a origem dos dados }
//      gaParm[0] := 'YEAR(PROPLOTE.DATAPROP) Ano, SUM(TAB_LOTE.REF_STAND) Venda';
//      gaParm[1] := 'PROPLOTE INNER JOIN TAB_LOTE ON PROPLOTE.TIPO_LOTE = TAB_LOTE.CODIGO';
//      gaParm[2] := '(PROPLOTE.VALOR_SINA) <> 0 OR (PROPLOTE.VALOR_PARC) <> 0';
//      gaParm[3] := 'YEAR(PROPLOTE.DATAPROP)';
//      gaParm[4] := 'YEAR(PROPLOTE.DATAPROP)';
//      msSql := dmDados.SqlVersao('NEC_0006', gaParm);
      msSql := dmDados.SqlVersao('NEC_0015', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      dmDados.Ultimo(maConexao[0]);

      lvCampo := dmDados.ValorColuna(maConexao[0], 'Ano', miIndCol[0]);

      { limpar as colunas }
      dlgGrafico.qrcsGrafico.XLabelsSource := '';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := '';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := '';
      { atribui a origem dos dados ao grafico }
      dlgGrafico.qrcsGrafico.DataSource := dmDados.gaSql[dmDados.gaConexao[maConexao[0]].iConexao];
      dlgGrafico.qrcsGrafico.XValues.ValueSource := 'Ano';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := 'Venda';

      { atribui o titulo }
      dlgGrafico.qrdbcGrafico.Title.Text.Clear;
      dlgGrafico.qrdbcGrafico.Title.Text.Add('CANCELADOS TOTAIS DE LOTES');
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      { zerar os eixos }
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 0;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 10000;
      { atribui os valores dos eixos }
      dlgGrafico.qrdbcGrafico.LeftAxis.Increment := 500;
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 1979;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := lvCampo;

      { tamanho da barra }
      dlgGrafico.qrcsGrafico.CustomBarWidth := 0;

      { chama a tela de grafico lote total }
      dlgGrafico.qrGrafico.Preview;

      { fechar a origem dos Dados }
      dmDados.FecharConexao(maConexao[0]);
    end;
    { se for Grafico de Cancelado Lote Total }
    if grProcessos.sNomePainel = GRAPH_LOTESLDTOTAL then
    begin
      { pega a origem dos dados }
//      gaParm[0] := 'YEAR(PROPLOTE.DATAPROP) Ano, SUM(TAB_LOTE.REF_STAND) Venda';
//      gaParm[1] := 'PROPLOTE INNER JOIN TAB_LOTE ON PROPLOTE.TIPO_LOTE = TAB_LOTE.CODIGO';
//      gaParm[2] := '(PROPLOTE.VALOR_SINA) <> 0 OR (PROPLOTE.VALOR_PARC) <> 0';
//      gaParm[3] := 'YEAR(PROPLOTE.DATAPROP)';
//      gaParm[4] := 'YEAR(PROPLOTE.DATAPROP)';
//      msSql := dmDados.SqlVersao('NEC_0006', gaParm);
      msSql := dmDados.SqlVersao('NEC_0016', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      dmDados.Ultimo(maConexao[0]);

      lvCampo := dmDados.ValorColuna(maConexao[0], 'Ano', miIndCol[0]);

      { limpar as colunas }
      dlgGrafico.qrcsGrafico.XLabelsSource := '';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := '';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := '';
      { atribui a origem dos dados ao grafico }
      dlgGrafico.qrcsGrafico.DataSource := dmDados.gaSql[dmDados.gaConexao[maConexao[0]].iConexao];
      dlgGrafico.qrcsGrafico.XValues.ValueSource := 'Ano';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := 'Venda';

      { atribui o titulo }
      dlgGrafico.qrdbcGrafico.Title.Text.Clear;
      dlgGrafico.qrdbcGrafico.Title.Text.Add('SALDOS TOTAIS DE LOTES');
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      { zerar os eixos }
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 0;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 10000;
      { atribui os valores dos eixos }
      dlgGrafico.qrdbcGrafico.LeftAxis.Increment := 500;
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 1979;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := lvCampo;

      { tamanho da barra }
      dlgGrafico.qrcsGrafico.CustomBarWidth := 0;

      { chama a tela de grafico lote total }
      dlgGrafico.qrGrafico.Preview;

      { fechar a origem dos Dados }
      dmDados.FecharConexao(maConexao[0]);
    end;
    { se for Grafico de Gaveta Total }
    if grProcessos.sNomePainel = GRAPH_GAVETOTAL then
    begin
      { pega a origem dos dados }
//      gaParm[0] := 'YEAR(PROPGAVE.DATAPROP) Ano, SUM(TAB_LOTE.REF_STAND) Venda';
//      gaParm[1] := 'PROPGAVE INNER JOIN TAB_LOTE ON PROPGAVE.TIPO_GAVE = TAB_LOTE.CODIGO';
//      gaParm[2] := '(PROPGAVE.VALOR_SINA) <> 0 OR (PROPGAVE.VALOR_PARC) <> 0';
//      gaParm[3] := 'YEAR(PROPGAVE.DATAPROP)';
//      gaParm[4] := 'YEAR(PROPGAVE.DATAPROP)';
//      msSql := dmDados.SqlVersao('NEC_0006', gaParm);
      msSql := dmDados.SqlVersao('NEC_0010', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      dmDados.Ultimo(maConexao[0]);

      lvCampo := dmDados.ValorColuna(maConexao[0], 'Ano', miIndCol[0]);

      { limpar as colunas }
      dlgGrafico.qrcsGrafico.XLabelsSource := '';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := '';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := '';
      { atribui a origem dos dados ao grafico }
      dlgGrafico.qrcsGrafico.DataSource := dmDados.gaSql[dmDados.gaConexao[maConexao[0]].iConexao];
      dlgGrafico.qrcsGrafico.XValues.ValueSource := 'Ano';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := 'Venda';

      { atribui o titulo }
      dlgGrafico.qrdbcGrafico.Title.Text.Clear;
      dlgGrafico.qrdbcGrafico.Title.Text.Add('VENDAS TOTAIS DE GAVETAS');
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      { zerar os eixos }
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 0;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 10000;
      { atribui os valores dos eixos }
      dlgGrafico.qrdbcGrafico.LeftAxis.Increment := 500;
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 1982;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := lvCampo;

      { tamanho da barra }
      dlgGrafico.qrcsGrafico.CustomBarWidth := 0;

      { chama a tela de grafico lote total }
      dlgGrafico.qrGrafico.Preview;

      { fechar a origem dos Dados }
      dmDados.FecharConexao(maConexao[0]);
    end;
    { se for Grafico de Lote Ano }
    if grProcessos.sNomePainel = GRAPH_LOTEANO then
    begin
      { pega a origem dos dados }
//      gaParm[0] := 'MONTH(PROPLOTE.DATAPROP) Mes, SUM(TAB_LOTE.REF_STAND) Venda';
//      gaParm[1] := 'PROPLOTE INNER JOIN TAB_LOTE ON PROPLOTE.TIPO_LOTE = TAB_LOTE.CODIGO';
//      gaParm[2] := 'YEAR(PROPLOTE.DATAPROP) = ' + Copy(mskGraphLAData.Text, 4, 4) + ' AND ((PROPLOTE.VALOR_SINA) <> 0 OR (PROPLOTE.VALOR_PARC) <> 0)';
//      gaParm[3] := 'MONTH(PROPLOTE.DATAPROP)';
//      gaParm[4] := 'MONTH(PROPLOTE.DATAPROP)';
//      msSql := dmDados.SqlVersao('NEC_0006', gaParm);
      gaParm[0] := Copy(mskGraphLAData.Text, 4, 4);
      msSql := dmDados.SqlVersao('NEC_0007', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      { soma o total de vendas no ano }
      lvCampo := 0;
      while not dmDados.Status(maConexao[0], IOSTATUS_EOF) do
      begin
        lvCampo := lvCampo + Round(dmDados.ValorColuna(maConexao[0], 'Venda', miIndCol[0])+0.01);
        dmDados.Proximo(maConexao[0]);
      end;

      { limpar as colunas }
      dlgGrafico.qrcsGrafico.XLabelsSource := '';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := '';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := '';
      { atribui a origem dos dados ao grafico }
      dlgGrafico.qrcsGrafico.DataSource := dmDados.gaSql[dmDados.gaConexao[maConexao[0]].iConexao];
      dlgGrafico.qrcsGrafico.XValues.ValueSource := 'Mes';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := 'Venda';

      { atribui o titulo }
      dlgGrafico.qrdbcGrafico.Title.Text.Clear;
      dlgGrafico.qrdbcGrafico.Title.Text.Add('VENDAS DE LOTES - ' + Copy(mskGraphLAData.Text, 4, 4));
      dlgGrafico.qrdbcGrafico.Title.Text.Add('TOTAL - ' + IntToStr(lvCampo));
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      { zerar os eixos }
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 0;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 10000;
      { atribui os valores dos eixos }
      dlgGrafico.qrdbcGrafico.LeftAxis.Increment := 10;
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 1;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 12;
      { tamanho da barra }
      dlgGrafico.qrcsGrafico.CustomBarWidth := 50;

      { chama a tela de grafico lote total }
      dlgGrafico.qrGrafico.Preview;

      { fechar a origem dos Dados }
      dmDados.FecharConexao(maConexao[0]);
    end;
    { se for Grafico de Gaveta Ano }
    if grProcessos.sNomePainel = GRAPH_GAVEANO then
    begin
      { pega a origem dos dados }
//      gaParm[0] := 'MONTH(PROPGAVE.DATAPROP) Mes, SUM(TAB_LOTE.REF_STAND) Venda';
//      gaParm[1] := 'PROPGAVE INNER JOIN TAB_LOTE ON PROPGAVE.TIPO_GAVE = TAB_LOTE.CODIGO';
//      gaParm[2] := 'YEAR(PROPGAVE.DATAPROP) = ' + Copy(mskGraphGAData.Text, 4, 4) + ' AND ((PROPGAVE.VALOR_SINA) <> 0 OR (PROPGAVE.VALOR_PARC) <> 0)';
//      gaParm[3] := 'MONTH(PROPGAVE.DATAPROP)';
//      gaParm[4] := 'MONTH(PROPGAVE.DATAPROP)';
//      msSql := dmDados.SqlVersao('NEC_0006', gaParm);
      gaParm[0] := Copy(mskGraphGAData.Text, 4, 4);
      msSql := dmDados.SqlVersao('NEC_0008', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      { soma o total de vendas no ano }
      lvCampo := 0;
      while not dmDados.Status(maConexao[0], IOSTATUS_EOF) do
      begin
        lvCampo := lvCampo + Round(dmDados.ValorColuna(maConexao[0], 'Venda', miIndCol[0]));
        dmDados.Proximo(maConexao[0]);
      end;

      { limpar as colunas }
      dlgGrafico.qrcsGrafico.XLabelsSource := '';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := '';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := '';
      { atribui a origem dos dados ao grafico }
      dlgGrafico.qrcsGrafico.DataSource := dmDados.gaSql[dmDados.gaConexao[maConexao[0]].iConexao];
      dlgGrafico.qrcsGrafico.XValues.ValueSource := 'Mes';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := 'Venda';

      { atribui o titulo }
      dlgGrafico.qrdbcGrafico.Title.Text.Clear;
      dlgGrafico.qrdbcGrafico.Title.Text.Add('VENDAS DE GAVETAS - ' + Copy(mskGraphGAData.Text, 4, 4));
      dlgGrafico.qrdbcGrafico.Title.Text.Add('TOTAL - ' + IntToStr(lvCampo));
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      { zerar os eixos }
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 0;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 10000;
      { atribui os valores dos eixos }
      dlgGrafico.qrdbcGrafico.LeftAxis.Increment := 10;
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 1;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 12;
      { tamanho da barra }
      dlgGrafico.qrcsGrafico.CustomBarWidth := 50;

      { chama a tela de grafico lote total }
      dlgGrafico.qrGrafico.Preview;

      { fechar a origem dos Dados }
      dmDados.FecharConexao(maConexao[0]);
    end;
    { se for Grafico de Lote Mes }
    if grProcessos.sNomePainel = GRAPH_LOTEMES then
    begin
      { pega a origem dos dados }
//      gaParm[0] := 'Convert(Int, CORRETOR.CODIGO) Codigo, CORRETOR.DESCRICAO Vendedora, SUM(TAB_LOTE.REF_STAND) Venda';
//      gaParm[1] := 'PROPLOTE INNER JOIN TAB_LOTE ON PROPLOTE.TIPO_LOTE = TAB_LOTE.CODIGO INNER JOIN CORRETOR ON PROPLOTE.COD_COR = CORRETOR.CODIGO';
//      gaParm[2] := 'MONTH(PROPLOTE.DATAPROP) = '+ Copy(mskGraphLMData.Text, 1, 2) + ' AND YEAR(PROPLOTE.DATAPROP) = ' + Copy(mskGraphLMData.Text, 4, 4) + ' AND CORRETOR.T_PROPOSTA = ''L''' + ' AND ((PROPLOTE.VALOR_SINA) <> 0 OR (PROPLOTE.VALOR_PARC) <> 0)';
//      gaParm[3] := 'CORRETOR.CODIGO, CORRETOR.DESCRICAO';
//      gaParm[4] := 'CORRETOR.CODIGO';
//      msSql := dmDados.SqlVersao('NEC_0006', gaParm);
      gaParm[0] := Copy(mskGraphLMData.Text, 1, 2);
      gaParm[1] := Copy(mskGraphLMData.Text, 4, 4);
      msSql := dmDados.SqlVersao('NEC_0011', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      { limpar as colunas }
      dlgGrafico.qrcsGrafico.XLabelsSource := '';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := '';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := '';
      { atribui a origem dos dados ao grafico }
      dlgGrafico.qrcsGrafico.DataSource := dmDados.gaSql[dmDados.gaConexao[maConexao[0]].iConexao];
      dlgGrafico.qrcsGrafico.XLabelsSource := 'Vendedora';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := 'Codigo';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := 'Venda';
      { mostrar os valores em cima das barras }
      dlgGrafico.qrcsGrafico.Marks.Style := smsValue;

      { atribui o titulo }
      dlgGrafico.qrdbcGrafico.Title.Text.Clear;
      dlgGrafico.qrdbcGrafico.Title.Text.Add('VENDAS DE LOTES');
      dlgGrafico.qrdbcGrafico.Title.Text.Add(maReferencia[StrToInt(Copy(mskGraphLMData.Text, 1, 2))] + '/' + Copy(mskGraphLMData.Text, 4, 4));
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');
      { zerar os eixos }
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 0;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 10000;
      { atribui os valores dos eixos }
      dlgGrafico.qrdbcGrafico.LeftAxis.Increment := 5;
      lvCampo := dmDados.ValorColuna(maConexao[0], 'Codigo', miIndCol[0]);
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := lvCampo;
      dmDados.Ultimo(maConexao[0]);
      lvCampo := dmDados.ValorColuna(maConexao[0], 'Codigo', miIndCol[0]);
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := lvCampo;
      { tamanho da barra }
      dlgGrafico.qrcsGrafico.CustomBarWidth := 20;

      { chama a tela de grafico lote total }
      dlgGrafico.qrGrafico.Preview;

      { fechar a origem dos Dados }
      dmDados.FecharConexao(maConexao[0]);
    end;
    { se for Grafico de Gaveta Mes }
    if grProcessos.sNomePainel = GRAPH_GAVEMES then
    begin
      { pega a origem dos dados }
//      gaParm[0] := 'Convert(Int, CORRETOR.CODIGO) Codigo, CORRETOR.DESCRICAO Vendedora, SUM(TAB_LOTE.REF_STAND) Venda';
//      gaParm[1] := 'PROPGAVE INNER JOIN TAB_LOTE ON PROPGAVE.TIPO_GAVE = TAB_LOTE.CODIGO INNER JOIN CORRETOR ON PROPGAVE.COD_COR = CORRETOR.CODIGO';
//      gaParm[2] := 'MONTH(PROPGAVE.DATAPROP) = '+ Copy(mskGraphGMData.Text, 1, 2) + ' AND YEAR(PROPGAVE.DATAPROP) = ' + Copy(mskGraphGMData.Text, 4, 4) + ' AND CORRETOR.T_PROPOSTA = ''G''' + ' AND ((PROPGAVE.VALOR_SINA) <> 0 OR (PROPGAVE.VALOR_PARC) <> 0)';
//      gaParm[3] := 'CORRETOR.CODIGO, CORRETOR.DESCRICAO';
//      gaParm[4] := 'CORRETOR.CODIGO';
//      msSql := dmDados.SqlVersao('NEC_0006', gaParm);
      gaParm[0] := Copy(mskGraphGMData.Text, 1, 2);
      gaParm[1] := Copy(mskGraphGMData.Text, 4, 4);
      msSql := dmDados.SqlVersao('NEC_0012', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      { limpar as colunas }
      dlgGrafico.qrcsGrafico.XLabelsSource := '';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := '';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := '';
      { atribui a origem dos dados ao grafico }
      dlgGrafico.qrcsGrafico.DataSource := dmDados.gaSql[dmDados.gaConexao[maConexao[0]].iConexao];
      dlgGrafico.qrcsGrafico.XLabelsSource := 'Vendedora';
      dlgGrafico.qrcsGrafico.XValues.ValueSource := 'Codigo';
      dlgGrafico.qrcsGrafico.YValues.ValueSource := 'Venda';
      { mostrar os valores em cima das barras }
      dlgGrafico.qrcsGrafico.Marks.Style := smsValue;

      { atribui o titulo }
      dlgGrafico.qrdbcGrafico.Title.Text.Clear;
      dlgGrafico.qrdbcGrafico.Title.Text.Add('VENDAS DE GAVETAS');
      dlgGrafico.qrdbcGrafico.Title.Text.Add(maReferencia[StrToInt(Copy(mskGraphGMData.Text, 1, 2))] + '/' + Copy(mskGraphGMData.Text, 4, 4));
      dlgGrafico.qrdbcGrafico.Title.Text.Add('');

      { zerar os eixos }
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := 0;
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := 10000;
      { atribui os valores dos eixos }
      dlgGrafico.qrdbcGrafico.LeftAxis.Increment := 5;
      lvCampo := dmDados.ValorColuna(maConexao[0], 'Codigo', miIndCol[0]);
      dlgGrafico.qrdbcGrafico.BottomAxis.Minimum := lvCampo;
      dmDados.Ultimo(maConexao[0]);
      lvCampo := dmDados.ValorColuna(maConexao[0], 'Codigo', miIndCol[0]);
      dlgGrafico.qrdbcGrafico.BottomAxis.Maximum := lvCampo;
      { tamanho da barra }
      dlgGrafico.qrcsGrafico.CustomBarWidth := 20;

      { chama a tela de grafico lote total }
      dlgGrafico.qrGrafico.Preview;

      { fechar a origem dos Dados }
      dmDados.FecharConexao(maConexao[0]);
    end;
    { se for Retorno Banco }
    if grProcessos.sNomePainel = RETORNO_BANCO then
    begin
      glCodigoLayout := StrToInt(untCombos.gf_Combo_SemDescricao(cboLayoutRetBanco));

      {varre a lista procurando alguma item marcado}
      for lwConta := 1 to grdArquivosRetBanco.MaxRows do
      begin
        grdArquivosRetBanco.Row := lwConta;
        {posiciona na coluna do Arquivo}
        grdArquivosRetBanco.Col := 2;
        {se o tiver marcado}
        if grdArquivosRetBanco.Text <> VAZIO then
        begin
          { chama a funcao para Importar o Retorno}
          lblNomeArquivoRetBanco.Caption := grdArquivosRetBanco.Text;
          formProcessos.Refresh;
          dmRetorno.ImportarArquivo(glCodigoLayout, grdArquivosRetBanco.Text);
        end;
      end;
      lblNomeArquivoRetBanco.Caption := VAZIO;
      { Inicializa variavel de sucesso }
//      gbProcessoOK := True;
    end;
    { se for Envio Banco }
    if grProcessos.sNomePainel = ENVIO_ARQUIVO then
    begin
      lsEnvio := untCombos.gf_Combo_SemDescricao(cboTipoEnvioEnvioArqui);

      { se for arquivo }
      if lsEnvio = '1' then
        untGeraArquivo.gp_GeraArquivo_Inicializar;

      { se tiver layout }
      if cboLayoutEnvioArqui.Text <> VAZIO then
        glCodigoLayout := StrToInt(untCombos.gf_Combo_SemDescricao(cboLayoutEnvioArqui))
      else
        glCodigoLayout := 0;
//      lsBanco := untCombos.gf_Combo_SemDescricao(cboBancoEnvioArqui);

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { guarda as mensagens }
      gaParm[0] := 'C_BANCO';
      gaParm[1] := 'Codigo = ''' + untCombos.gf_Combo_SemDescricao(cboBancoEnvioArqui) + '''';
      gaParm[2] := 'Codigo';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[1] := dmDados.ExecutarSelect(msSql);
      if maConexao[1] = IOPTR_NOPOINTER then
        Exit;
      { enquento tiver pessoas p/ incluir}
      lsBanco := dmDados.ValorColuna(maConexao[1],'Tipo_Cobr',miIndCol[0]);
      lsNumBanco := dmDados.ValorColuna(maConexao[1],'Num_Banco',miIndCol[1]);
      lsAgencia := dmDados.ValorColuna(maConexao[1],'Agen_Banco',miIndCol[2]);
      lsConta := dmDados.ValorColuna(maConexao[1],'Conta_Banc',miIndCol[3]);
      dmDados.FecharConexao(maConexao[1]);

      laParams[0].sNome := '@DataIni';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftDateTime;
      laParams[0].vValor := StrToDate(DateToStr(dtpDataInicialEnvioArqui.Date));
      laParams[1].sNome := '@DataFim';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftDateTime;
      laParams[1].vValor := StrToDate(DateToStr(dtpDataFinalEnvioArqui.Date));
      laParams[2].sNome := '@TipoProp';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui);
      laParams[3].sNome := '@NumTaxa';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftString;
      { se for servico }
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
        laParams[3].vValor := untCombos.gf_Combo_SemDescricao(cboServicoEnvioArqui)
      else
        laParams[3].vValor := mskNumTaxaEnvioArqui.EditText;
      laParams[4].sNome := '@NumIni';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftString;
      laParams[4].vValor := mskPropostaInicialEnvioArqui.Text;
      laParams[5].sNome := '@NumFim';
      laParams[5].ptTipo := ptInput;
      laParams[5].ftTipoDado := ftString;
      laParams[5].vValor := mskPropostaFinalEnvioArqui.Text;
//      dmDados.CriarStored('Gera_NossoNumero_' + IntToStr(glCodigoLayout), laParams, 5);
      dmDados.CriarStored('GeraNossoNumero' + lsBanco, laParams, 5);

      laParams[6].sNome := '@Emite';
      laParams[6].ptTipo := ptInput;
      laParams[6].ftTipoDado := ftInteger;
      laParams[6].vValor := 0;
      laParams[7].sNome := '@DataVenc';
      laParams[7].ptTipo := ptInput;
      laParams[7].ftTipoDado := ftString;
      if mskDataVencimentoEnvioArqui.Text <> VAZIO_DATA then
        laParams[7].vValor := (mskDataVencimentoEnvioArqui.Text)
      else
        laParams[7].vValor := VAZIO;
      laParams[8].sNome := '@DataInicial';
      laParams[8].ptTipo := ptInput;
      laParams[8].ftTipoDado := ftString;
      if mskDataInicialEnvioArqui.Text <> VAZIO_DATA then
        laParams[8].vValor := (mskDataInicialEnvioArqui.Text)
      else
        laParams[8].vValor := VAZIO;
      laParams[9].sNome := '@DataFinal';
      laParams[9].ptTipo := ptInput;
      laParams[9].ftTipoDado := ftString;
      if mskDataFinalEnvioArqui.Text <> VAZIO_DATA then
        laParams[9].vValor := (mskDataFinalEnvioArqui.Text)
      else
        laParams[9].vValor := VAZIO;
      { guarda em arquivo temporario }
      dmDados.CriarStored('GravaRemessa', laParams, 9);

      { chama a janela com os registros selecionados }
      dlgConsulta.ShowModal;
      
      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { se for impressora }
      if lsEnvio = '2' then
      begin
        { se tiver mensagem }
        if cboMsgEnvioArqui.Text <> VAZIO then
        begin
          { guarda as mensagens }
          gaParm[0] := 'Mensagem';
//          gaParm[1] := 'Codigo = ''' + IntToStr(glCodigoLayout) + '''';
          gaParm[1] := 'Codigo = ''' + untCombos.gf_Combo_SemDescricao(cboMsgEnvioArqui) + '''';
          gaParm[2] := 'Codigo';
          msSql := dmDados.SqlVersao('NEC_0002', gaParm);
          maConexao[1] := dmDados.ExecutarSelect(msSql);
          if maConexao[1] = IOPTR_NOPOINTER then
            Exit;

          { enquento tiver pessoas p/ incluir}
          for lwConta := 1 to High(maMensagem) do
            maMensagem[lwConta] := dmDados.ValorColuna(maConexao[1],'Mensagem'+ IntToStr(lwConta),miIndCol[lwConta - 1]);

          dmDados.FecharConexao(maConexao[1]);
        end
        else
        begin
          for lwConta := 1 to High(maMensagem) do
            maMensagem[lwConta] := VAZIO;
        end;
      end;

      { se for taxa}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'T' then
      begin
        for lwConta := 0 to High(miIndCol) do
          miIndCol[lwConta] := IOPTR_NOPOINTER;

        { valor e desconto da taxa }
        gaParm[0] := 'TAB_TAXA';
        gaParm[1] := 'NUM_TAXA1 = ''' + mskNumTaxaEnvioArqui.Text + '''';
        gaParm[2] := 'CODIGO';
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        maConexao[1] := dmDados.ExecutarSelect(msSql);
        { se achou - Num da Taxa }
        if not dmDados.Status(maConexao[1], IOSTATUS_EOF) then
        begin
          lsDescontoU := dmDados.ValorColuna(maConexao[1], CAMPO_UNICA_DESCONTO, miIndCol[0]);
          lsDescontoS := dmDados.ValorColuna(maConexao[1], CAMPO_SEMESTRAL_DESCONTO, miIndCol[1]);
          lsDescontoB := dmDados.ValorColuna(maConexao[1], CAMPO_BIMESTRAL_DESCONTO, miIndCol[2]);
        end;
        { Fecha a Conexao}
        dmDados.FecharConexao(maConexao[1]);
      end;

      gaParm[0] := CTEDB_DATE_INI + FormatDateTime(MK_DATA, dtpDataInicialEnvioArqui.Date) + CTEDB_DATE_FIM;
      gaParm[1] := CTEDB_DATE_INI + FormatDateTime(MK_DATA, dtpDataFinalEnvioArqui.Date) + CTEDB_DATE_FIM;
      gaParm[2] := untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui);
      { se for taxa}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'T' then
      begin
        { guarda o tipo da taxa }
        lsTipoTaxa := untCombos.gf_Combo_SemDescricao(cboTipoTaxaEnvioArqui);
        if mskPropostaInicialEnvioArqui.Text <> VAZIO then
        begin
          if Trim(mskCEPInicialEnvioArqui.Text) <> '-' then
          begin
            gaParm[3] := mskNumTaxaEnvioArqui.EditText;
            gaParm[4] := '0';
            gaParm[5] := mskPropostaInicialEnvioArqui.Text;
            gaParm[6] := mskPropostaFinalEnvioArqui.Text;
            gaParm[7] := mskCEPInicialEnvioArqui.Text;
            gaParm[8] := mskCEPFinalEnvioArqui.Text;
            if lsTipoTaxa = VAZIO then
              gaParm[9] := '(Tipo_Taxa Is Null)'
            else
              gaParm[9] := '(Tipo_Taxa = ''' + lsTipoTaxa + ''')';
            msSql := dmDados.SqlVersao('ARQ_0011', gaParm);
          end
          else
          begin
            gaParm[3] := mskNumTaxaEnvioArqui.EditText;
            gaParm[4] := '0';
            gaParm[5] := mskPropostaInicialEnvioArqui.Text;
            gaParm[6] := mskPropostaFinalEnvioArqui.Text;
            if lsTipoTaxa = VAZIO then
              gaParm[7] := '(Tipo_Taxa Is Null)'
            else
              gaParm[7] := '(Tipo_Taxa = ''' + lsTipoTaxa + ''')';
            msSql := dmDados.SqlVersao('ARQ_0010', gaParm);
          end;
        end
        else
        begin
          gaParm[3] := mskNumTaxaEnvioArqui.EditText;
          gaParm[4] := '0';
          if lsTipoTaxa = VAZIO then
            gaParm[5] := '(Tipo_Taxa Is Null)'
          else
            gaParm[5] := '(Tipo_Taxa = ''' + lsTipoTaxa + ''')';
          msSql := dmDados.SqlVersao('ARQ_0007', gaParm);
        end;
      end
      else
      begin
        gaParm[3] := mskPropostaInicialEnvioArqui.Text;
        gaParm[4] := mskPropostaFinalEnvioArqui.Text;
        gaParm[5] := '0';
        { se for seguro}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'S' then
          gaParm[6] := mskNumTaxaEnvioArqui.EditText;
        { se for servico}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
          gaParm[2] := untCombos.gf_Combo_SemDescricao(cboServicoEnvioArqui);
        msSql := dmDados.SqlVersao('ARQ_008' + untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui), gaParm);
      end;

      miConexaoAtualiza := dmDados.ExecutarSelect(msSql);
      if miConexaoAtualiza = IOPTR_NOPOINTER then
        Exit;

      { atualiza a barra de progresso }
      liConta := 0;
      prbEnvioBanco.Min := 0;
      prbEnvioBanco.Max := dmDados.gaSql[miConexaoAtualiza].RecordCount;

      { se for arquivo }
      if lsEnvio = '1' then
        { grava a primeira linha do arquivo}
        untGeraArquivo.EmiteLinha1(dmDados.ConfigValor('DirRem_' + IntToStr(glCodigoLayout)) + edtNomeArquivoEnvioArqui.Text);

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { se for seguro}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'S' then
        lsRetorno := 'REC_SEGU'
      else
        { se for seguradora}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'I' then
          lsRetorno := 'SEGURO_PARC'
        else
          { se for servico}
          if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
            lsRetorno := 'REC_SERV'
          else
            lsRetorno := 'RECEB';

      { enquento tiver pessoas p/ incluir}
      While not dmDados.Status(miConexaoAtualiza, IOSTATUS_EOF) do
      begin

        { se for arquivo }
        if lsEnvio = '1' then
          { Grava no Arquivo texto}
          untGeraArquivo.EmiteLinhaX(dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]))
        else
        begin
          if (untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'L') or
             (untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'G') or
             (untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'N') or
             (untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'T') then
          begin
            { ha debito vencido }
            gaParm[0] := dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]);
            gaParm[1] := untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui);
            msSql := dmDados.SqlVersao('NEC_0022', gaParm);
            maConexao[0] := dmDados.ExecutarSelect(msSql);
            if maConexao[0] = IOPTR_NOPOINTER then
              Exit;
            if not dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
              lsDebito := dmDados.ValorColuna(maConexao[0],'Valor',miIndCol[3])
            else
              lsDebito := VAZIO;
            dmDados.FecharConexao(maConexao[0]);
          end;
 (*
         { se for taxa }
          lsDesconto := VAZIO;
          if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'T' then
          begin
            lsTipoTaxa := dmDados.ValorColuna(miConexaoAtualiza,'Tipo_Taxa',miIndCol[2]);
            { se nao escolheu a taxa }
            if lsTipoTaxa = VAZIO then
            begin
              for liTaxa :=  1 to 3 do
              begin
                { taxa Unica }
                if liTaxa = 1 then
                begin
                  formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'U.rpt';
//                  lsDesconto := dmDados.ConfigValor('Desc_Manut_Ano');
                  lsDesconto := lsDescontoU;
                end
                { taxa Semestral }
                else if liTaxa = 2 then
                begin
                  formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'S.rpt';
//                  lsDesconto := dmDados.ConfigValor('Desc_Manut_Seme');
                  lsDesconto := lsDescontoS;
                end
                { taxa Bimestral }
                else if liTaxa = 3 then
                begin
                  formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'B.rpt';
//                  lsDesconto := dmDados.ConfigValor('Desc_Manut_Bime');
                  lsDesconto := lsDescontoB;
                end;
                formImprimir.rptRelatorio.Destination := crptToPrinter;
                formImprimir.rptRelatorio.Connect := gsDbConnect;    //DB_CONNECT;
                { A formula}
                for lwConta := 1 to High(maMensagem) do
                  formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'LINHA' + IntToStr(lwConta) + '= ''' + maMensagem[lwConta] + '''';
                formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'INSTRUCAO1= ''' + lsDebito + '''';
                formImprimir.rptRelatorio.Formulas[lwConta] := 'AGENCIA= ''' + lsAgencia + '''';
                formImprimir.rptRelatorio.Formulas[lwConta + 1] := 'CONTA= ''' + lsConta + '''';
                formImprimir.rptRelatorio.Formulas[lwConta + 2] := 'DESCONTO= ''' + lsDesconto + '''';;
                formImprimir.rptRelatorio.Formulas[lwConta + 3] := VAZIO;
                formImprimir.rptRelatorio.SelectionFormula := '{RECEB.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
                { Assiona o Relatorio}
                formImprimir.rptRelatorio.Action := 1;
              end;
            end
            else
            begin
              { taxa Unica }
              if lsTipoTaxa = 'U' then
              begin
//                lsDesconto := dmDados.ConfigValor('Desc_Manut_Ano');
                lsDesconto := lsDescontoU;
              end
              { taxa Semestral }
              else if lsTipoTaxa = 'S' then
              begin
//                lsDesconto := dmDados.ConfigValor('Desc_Manut_Seme');
                lsDesconto := lsDescontoS;
              end
              { taxa Bimestral }
              else if lsTipoTaxa = 'B' then
              begin
//                lsDesconto := dmDados.ConfigValor('Desc_Manut_Bime');
                lsDesconto := lsDescontoB;
              end;
              formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + '.rpt';
              formImprimir.rptRelatorio.Destination := crptToPrinter;
              formImprimir.rptRelatorio.Connect := gsDbConnect;    //DB_CONNECT;
              { A formula}
              for lwConta := 1 to High(maMensagem) do
                formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'LINHA' + IntToStr(lwConta) + '= ''' + maMensagem[lwConta] + '''';
              formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'INSTRUCAO1= ''' + lsDebito + '''';
              formImprimir.rptRelatorio.Formulas[lwConta] := 'AGENCIA= ''' + lsAgencia + '''';
              formImprimir.rptRelatorio.Formulas[lwConta + 1] := 'CONTA= ''' + lsConta + '''';
              formImprimir.rptRelatorio.Formulas[lwConta + 2] := 'DESCONTO= ''' + lsDesconto + '''';;
              formImprimir.rptRelatorio.Formulas[lwConta + 3] := VAZIO;
              formImprimir.rptRelatorio.SelectionFormula := '{RECEB.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
              { Assiona o Relatorio}
              formImprimir.rptRelatorio.Action := 1;
            end;
          end
          else
          begin
          *)
            { se for nicho }
            if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'N' then
              formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'N.rpt'
            { se for seguro }
            else if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'S' then
              formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'S.rpt'
            { se for servico }
            else if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
              formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'V.rpt'
            else
              formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + '.rpt';
   //         formImprimir.rptRelatorio.Destination := crptToPrinter;
   //         formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
              formImprimir.rptRelatorio.Connect.ServerName := gsDbConnect;
              formImprimir.rptRelatorio.Connect.UserID := gsDbUserName;
              formImprimir.rptRelatorio.Connect.Password := gsDbPassWord;
            { A formula}
            for lwConta := 1 to High(maMensagem) do
     //         formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'LINHA' + IntToStr(lwConta) + '= ''' + maMensagem[lwConta] + '''';
     //       formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'INSTRUCAO1= ''' + lsDebito + '''';
              formImprimir.rptRelatorio.Formulas.ByName('LINHA' + IntToStr(lwConta)).Formula.Text := maMensagem[lwConta];
              formImprimir.rptRelatorio.Formulas.ByName('INSTRUCAO1').Formula.Text := lsDebito;

//            formImprimir.rptRelatorio.Formulas[lwConta] := VAZIO;
        //    formImprimir.rptRelatorio.Formulas[lwConta] := 'AGENCIA= ''' + lsAgencia + '''';
        //    formImprimir.rptRelatorio.Formulas[lwConta + 1] := 'CONTA= ''' + lsConta + '''';
              formImprimir.rptRelatorio.Formulas.ByName('AGENCIA').Formula.Text := lsAgencia;
              formImprimir.rptRelatorio.Formulas.ByName('CONTA').Formula.Text := lsConta;
 //           formImprimir.rptRelatorio.Formulas[lwConta + 2] := VAZIO;
            { se for seguro }
            if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'S' then
               formImprimir.rptRelatorio.Selection.Formula.Text := '{REC_SEGU.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + ''''
            { se for servico }
            else if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
                formImprimir.rptRelatorio.Selection.Formula.Text := '{REC_SERV.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + ''''
            else
//              formImprimir.rptRelatorio.SelectionFormula := '{RECEB.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
            formImprimir.rptRelatorio.Selection.Formula.Text := '{DbRecebimento.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
            { Assiona o Relatorio}
            formImprimir.rptRelatorio.Print;
//          end;
        end;

        { atualza barra de progresso }
        Inc(liConta);
        prbEnvioBanco.Position := liConta;

        gaParm[0] := lsRetorno;
        { se for seguradora}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'I' then
        begin
          gaParm[1] := 'CodigoSeg+Num_Parc = ''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
          gaParm[2] := 'Codigo';
        end
        else
        begin
          gaParm[1] := 'Codigo = ''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
          gaParm[2] := 'Codigo';
        end;
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;

        dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_EDIT);

        dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Emit_Carne', miIndCol[0], 1);
        dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Data_Envio', miIndCol[1], Date);
        dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Banco', miIndCol[2], lsNumBanco);

        { faz a atualizacao}
        dmDados.gsRetorno := dmDados.Alterar(maConexao[0], lsRetorno, VAZIO);

        dmDados.FecharConexao(maConexao[0]);

        { proxima pessoa}
        if (dmDados.Proximo(miConexaoAtualiza) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
      end;

      { se for arquivo }
      if lsEnvio = '1' then
        { emite a ultima linha do arquivo}
        untGeraArquivo.EmiteLinha9;

      { se for arquivo }
      if lsEnvio = '1' then
        untGeraArquivo.gp_GeraArquivo_Finalizar;

      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;
    { se for Reenvio Banco }
    if grProcessos.sNomePainel = REENVIO_ARQUIVO then
    begin
      lsEnvio := untCombos.gf_Combo_SemDescricao(cboTipoEnvioReenvioArqui);

      { se for arquivo }
      if lsEnvio = '1' then
        untGeraArquivo.gp_GeraArquivo_Inicializar;

      { se tiver layout }
      if cboLayoutReenvioArqui.Text <> VAZIO then
        glCodigoLayout := StrToInt(untCombos.gf_Combo_SemDescricao(cboLayoutReenvioArqui))
      else
        glCodigoLayout := 0;
//      lsBanco := untCombos.gf_Combo_SemDescricao(cboBancoReenvioArqui);

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { guarda as mensagens }
      gaParm[0] := 'C_BANCO';
      gaParm[1] := 'Codigo = ''' + untCombos.gf_Combo_SemDescricao(cboBancoReenvioArqui) + '''';
      gaParm[2] := 'Codigo';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[1] := dmDados.ExecutarSelect(msSql);
      if maConexao[1] = IOPTR_NOPOINTER then
        Exit;
      { enquento tiver pessoas p/ incluir}
      lsBanco := dmDados.ValorColuna(maConexao[1],'Tipo_Cobr',miIndCol[0]);
      lsNumBanco := dmDados.ValorColuna(maConexao[1],'Num_Banco',miIndCol[1]);
      lsAgencia := dmDados.ValorColuna(maConexao[1],'Agen_Banco',miIndCol[2]);
      lsConta := dmDados.ValorColuna(maConexao[1],'Conta_Banc',miIndCol[3]);
      dmDados.FecharConexao(maConexao[1]);

      laParams[0].sNome := '@DataIni';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftDateTime;
      laParams[0].vValor := StrToDate(DateToStr(dtpDataInicialReenvioArqui.Date));
      laParams[1].sNome := '@DataFim';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftDateTime;
      laParams[1].vValor := StrToDate(DateToStr(dtpDataFinalReenvioArqui.Date));
      laParams[2].sNome := '@TipoProp';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui);
      laParams[3].sNome := '@NumTaxa';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftString;
      { se for servico }
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
        laParams[3].vValor := untCombos.gf_Combo_SemDescricao(cboServicoReenvioArqui)
      else
        laParams[3].vValor := mskNumTaxaReenvioArqui.EditText;
      laParams[4].sNome := '@NumIni';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftString;
      laParams[4].vValor := mskPropostaInicialReenvioArqui.Text;
      laParams[5].sNome := '@NumFim';
      laParams[5].ptTipo := ptInput;
      laParams[5].ftTipoDado := ftString;
      laParams[5].vValor := mskPropostaFinalReenvioArqui.Text;
      dmDados.CriarStored('GeraNossoNumero' + lsBanco, laParams, 5);

      laParams[6].sNome := '@Emite';
      laParams[6].ptTipo := ptInput;
      laParams[6].ftTipoDado := ftInteger;
      laParams[6].vValor := 1;
      laParams[7].sNome := '@DataVenc';
      laParams[7].ptTipo := ptInput;
      laParams[7].ftTipoDado := ftString;
      if mskDataVencimentoReenvioArqui.Text <> VAZIO_DATA then
        laParams[7].vValor := (mskDataVencimentoReenvioArqui.Text)
      else
        laParams[7].vValor := VAZIO;
      laParams[8].sNome := '@DataInicial';
      laParams[8].ptTipo := ptInput;
      laParams[8].ftTipoDado := ftString;
      if mskDataInicialReenvioArqui.Text <> VAZIO_DATA then
        laParams[8].vValor := (mskDataInicialReenvioArqui.Text)
      else
        laParams[8].vValor := VAZIO;
      laParams[9].sNome := '@DataFinal';
      laParams[9].ptTipo := ptInput;
      laParams[9].ftTipoDado := ftString;
      if mskDataFinalReenvioArqui.Text <> VAZIO_DATA then
        laParams[9].vValor := (mskDataFinalReenvioArqui.Text)
      else
        laParams[9].vValor := VAZIO;
      { guarda em arquivo temporario }
      dmDados.CriarStored('GravaRemessa', laParams, 9);

      { chama a janela com os registros selecionados }
      dlgConsulta.ShowModal;

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { se for impressora }
      if lsEnvio = '2' then
      begin
        { se tiver mensagem }
        if cboMsgReenvioArqui.Text <> VAZIO then
        begin
          { guarda as mensagens }
          gaParm[0] := 'Mensagem';
//          gaParm[1] := 'Codigo = ''' + IntToStr(glCodigoLayout) + '''';
          gaParm[1] := 'Codigo = ''' + untCombos.gf_Combo_SemDescricao(cboMsgReenvioArqui) + '''';
          gaParm[2] := 'Codigo';
          msSql := dmDados.SqlVersao('NEC_0002', gaParm);
          maConexao[1] := dmDados.ExecutarSelect(msSql);
          if maConexao[1] = IOPTR_NOPOINTER then
            Exit;

          { enquento tiver pessoas p/ incluir}
          for lwConta := 1 to High(maMensagem) do
            maMensagem[lwConta] := dmDados.ValorColuna(maConexao[1],'Mensagem'+ IntToStr(lwConta),miIndCol[lwConta - 1]);

          dmDados.FecharConexao(maConexao[1]);
        end
        else
        begin
          for lwConta := 1 to High(maMensagem) do
            maMensagem[lwConta] := VAZIO;
        end;
      end;

      { se for taxa}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'T' then
      begin
        for lwConta := 0 to High(miIndCol) do
          miIndCol[lwConta] := IOPTR_NOPOINTER;

        { valor e desconto da taxa }
        gaParm[0] := 'TAB_TAXA';
        gaParm[1] := 'NUM_TAXA1 = ''' + mskNumTaxaReenvioArqui.Text + '''';
        gaParm[2] := 'CODIGO';
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        maConexao[1] := dmDados.ExecutarSelect(msSql);
        { se achou - Num da Taxa }
        if not dmDados.Status(maConexao[1], IOSTATUS_EOF) then
        begin
          lsDescontoU := dmDados.ValorColuna(maConexao[1], CAMPO_UNICA_DESCONTO, miIndCol[0]);
          lsDescontoS := dmDados.ValorColuna(maConexao[1], CAMPO_SEMESTRAL_DESCONTO, miIndCol[1]);
          lsDescontoB := dmDados.ValorColuna(maConexao[1], CAMPO_BIMESTRAL_DESCONTO, miIndCol[2]);
        end;
        { Fecha a Conexao}
        dmDados.FecharConexao(maConexao[1]);
      end;

      gaParm[0] := CTEDB_DATE_INI + FormatDateTime(MK_DATA, dtpDataInicialReenvioArqui.Date) + CTEDB_DATE_FIM;
      gaParm[1] := CTEDB_DATE_INI + FormatDateTime(MK_DATA, dtpDataFinalReenvioArqui.Date) + CTEDB_DATE_FIM;
      gaParm[2] := untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui);
      { se for taxa}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'T' then
      begin
        { guarda o tipo da taxa }
        lsTipoTaxa := untCombos.gf_Combo_SemDescricao(cboTipoTaxaReenvioArqui);
        if mskPropostaInicialReenvioArqui.Text <> VAZIO then
        begin
          if Trim(mskCEPInicialReenvioArqui.Text) <> '-' then
          begin
            gaParm[3] := mskNumTaxaReenvioArqui.EditText;
            gaParm[4] := '1';
            gaParm[5] := mskPropostaInicialReenvioArqui.Text;
            gaParm[6] := mskPropostaFinalReenvioArqui.Text;
            gaParm[7] := mskCEPInicialReenvioArqui.Text;
            gaParm[8] := mskCEPFinalReenvioArqui.Text;
            if lsTipoTaxa = VAZIO then
              gaParm[9] := '(Tipo_Taxa Is Null)'
            else
              gaParm[9] := '(Tipo_Taxa = ''' + lsTipoTaxa + ''')';
            msSql := dmDados.SqlVersao('ARQ_0011', gaParm);
          end
          else
          begin
            gaParm[3] := mskNumTaxaReenvioArqui.EditText;
            gaParm[4] := '1';
            gaParm[5] := mskPropostaInicialReenvioArqui.Text;
            gaParm[6] := mskPropostaFinalReenvioArqui.Text;
            if lsTipoTaxa = VAZIO then
              gaParm[7] := '(Tipo_Taxa Is Null)'
            else
              gaParm[7] := '(Tipo_Taxa = ''' + lsTipoTaxa + ''')';
            msSql := dmDados.SqlVersao('ARQ_0010', gaParm);
          end;
        end
        else
        begin
          gaParm[3] := mskNumTaxaReenvioArqui.EditText;
          gaParm[4] := '1';
          if lsTipoTaxa = VAZIO then
            gaParm[5] := '(Tipo_Taxa Is Null)'
          else
            gaParm[5] := '(Tipo_Taxa = ''' + lsTipoTaxa + ''')';
          msSql := dmDados.SqlVersao('ARQ_0007', gaParm);
        end;
      end
      else
      begin
        gaParm[3] := mskPropostaInicialReenvioArqui.Text;
        gaParm[4] := mskPropostaFinalReenvioArqui.Text;
        gaParm[5] := '1';
        { se for seguro}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'S' then
          gaParm[6] := mskNumTaxaReenvioArqui.EditText;
        { se for servico}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
          gaParm[2] := untCombos.gf_Combo_SemDescricao(cboServicoReenvioArqui);
        msSql := dmDados.SqlVersao('ARQ_008' + untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui), gaParm);
      end;

      miConexaoAtualiza := dmDados.ExecutarSelect(msSql);
      if miConexaoAtualiza = IOPTR_NOPOINTER then
        Exit;

      { atualiza a barra de progresso }
      liConta := 0;
      prbReenvioBanco.Min := 0;
      prbReenvioBanco.Max := dmDados.gaSql[miConexaoAtualiza].RecordCount;

      { se for arquivo }
      if lsEnvio = '1' then
        { grava a primeira linha do arquivo}
        untGeraArquivo.EmiteLinha1(dmDados.ConfigValor('DirRem_' + IntToStr(glCodigoLayout)) + edtNomeArquivoReenvioArqui.Text);

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { se for seguro}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'S' then
        lsRetorno := 'REC_SEGU'
      else
        { se for seguradora}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'I' then
          lsRetorno := 'SEGURO_PARC'
        else
          { se for servico}
          if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
            lsRetorno := 'REC_SERV'
          else
            lsRetorno := 'RECEB';

      { enquento tiver pessoas p/ incluir}
      While not dmDados.Status(miConexaoAtualiza, IOSTATUS_EOF) do
      begin

        { se for arquivo }
        if lsEnvio = '1' then
          { Grava no Arquivo texto}
          untGeraArquivo.EmiteLinhaX(dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]))
        else
        begin
          if (untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'L') or
             (untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'G') or
             (untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'N') or
             (untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'T') then
          begin
            { ha debito vencido }
            gaParm[0] := dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]);
            gaParm[1] := untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui);
            msSql := dmDados.SqlVersao('NEC_0022', gaParm);
            maConexao[0] := dmDados.ExecutarSelect(msSql);
            if maConexao[0] = IOPTR_NOPOINTER then
              Exit;
            if not dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
              lsDebito := dmDados.ValorColuna(maConexao[0],'Valor',miIndCol[3])
            else
              lsDebito := VAZIO;
            dmDados.FecharConexao(maConexao[0]);
          end;
 (*
          { se for taxa }
          lsDesconto := VAZIO;
          if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'T' then
          begin
            lsTipoTaxa := dmDados.ValorColuna(miConexaoAtualiza,'Tipo_Taxa',miIndCol[2]);
            { se nao escolheu a taxa }
            if lsTipoTaxa = VAZIO then
            begin
              for liTaxa :=  1 to 3 do
              begin
                { taxa Unica }
                if liTaxa = 1 then
                begin
                  formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'U.rpt';
//                  lsDesconto := dmDados.ConfigValor('Desc_Manut_Ano');
                  lsDesconto := lsDescontoU;
                end
                { taxa Semestral }
                else if liTaxa = 2 then
                begin
                  formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'S.rpt';
//                  lsDesconto := dmDados.ConfigValor('Desc_Manut_Seme');
                  lsDesconto := lsDescontoS;
                end
                { taxa Bimestral }
                else if liTaxa = 3 then
                begin
                  formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'B.rpt';
//                  lsDesconto := dmDados.ConfigValor('Desc_Manut_Bime');
                  lsDesconto := lsDescontoB;
                end;
                formImprimir.rptRelatorio.Destination := crptToPrinter;
                formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
                { A formula}
                for lwConta := 1 to High(maMensagem) do
                  formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'LINHA' + IntToStr(lwConta) + '= ''' + maMensagem[lwConta] + '''';
                formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'INSTRUCAO1= ''' + lsDebito + '''';
                formImprimir.rptRelatorio.Formulas[lwConta] := 'AGENCIA= ''' + lsAgencia + '''';
                formImprimir.rptRelatorio.Formulas[lwConta + 1] := 'CONTA= ''' + lsConta + '''';
                formImprimir.rptRelatorio.Formulas[lwConta + 2] := 'DESCONTO= ''' + lsDesconto + '''';;
                formImprimir.rptRelatorio.Formulas[lwConta + 3] := VAZIO;
                formImprimir.rptRelatorio.SelectionFormula := '{RECEB.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
                { Assiona o Relatorio}
                formImprimir.rptRelatorio.Action := 1;
              end;
            end
            else
            begin
              { taxa Unica }
              if lsTipoTaxa = 'U' then
              begin
//               lsDesconto := dmDados.ConfigValor('Desc_Manut_Ano');
                lsDesconto := lsDescontoU;
              end
              { taxa Semestral }
              else if lsTipoTaxa = 'S' then
              begin
//                lsDesconto := dmDados.ConfigValor('Desc_Manut_Seme');
                lsDesconto := lsDescontoS;
              end
              { taxa Bimestral }
              else if lsTipoTaxa = 'B' then
              begin
//                lsDesconto := dmDados.ConfigValor('Desc_Manut_Bime');
                lsDesconto := lsDescontoB;
              end;

              formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + '.rpt';
              formImprimir.rptRelatorio.Destination := crptToPrinter;
              formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
              { A formula}
              for lwConta := 1 to High(maMensagem) do
                formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'LINHA' + IntToStr(lwConta) + '= ''' + maMensagem[lwConta] + '''';
              formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'INSTRUCAO1= ''' + lsDebito + '''';
              formImprimir.rptRelatorio.Formulas[lwConta] := 'AGENCIA= ''' + lsAgencia + '''';
              formImprimir.rptRelatorio.Formulas[lwConta + 1] := 'CONTA= ''' + lsConta + '''';
              formImprimir.rptRelatorio.Formulas[lwConta + 2] := 'DESCONTO= ''' + lsDesconto + '''';;
              formImprimir.rptRelatorio.Formulas[lwConta + 3] := VAZIO;
              formImprimir.rptRelatorio.SelectionFormula := '{RECEB.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
              { Assiona o Relatorio}
              formImprimir.rptRelatorio.Action := 1;
            end;
          end
          else
          begin
          *)
            { se for nicho }
            if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'N' then
              formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'N.rpt'
            { se for seguro }
            else if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'S' then
              formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'S.rpt'
            { se for servico }
            else if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
              formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + 'V.rpt'
            else
              formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Boleto' + lsBanco + '.rpt';
    //        formImprimir.rptRelatorio.Destination := crptToPrinter;
    //        formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
              formImprimir.rptRelatorio.Connect.ServerName := gsDbConnect;
              formImprimir.rptRelatorio.Connect.UserID := gsDbUserName;
              formImprimir.rptRelatorio.Connect.Password := gsDbPassWord;
            { A formula}
            for lwConta := 1 to High(maMensagem) do
//            formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'LINHA' + IntToStr(lwConta) + '= ''' + maMensagem[lwConta] + '''';
//            formImprimir.rptRelatorio.Formulas[lwConta - 1] := 'INSTRUCAO1= ''' + lsDebito + '''';
              formImprimir.rptRelatorio.Formulas.ByName('LINHA' + IntToStr(lwConta)).Formula.Text := maMensagem[lwConta];
              formImprimir.rptRelatorio.Formulas.ByName('INSTRUCAO1').Formula.Text := lsDebito;

//            formImprimir.rptRelatorio.Formulas[lwConta] := VAZIO;
 //           formImprimir.rptRelatorio.Formulas[lwConta] := 'AGENCIA= ''' + lsAgencia + '''';
 //           formImprimir.rptRelatorio.Formulas[lwConta + 1] := 'CONTA= ''' + lsConta + '''';
            formImprimir.rptRelatorio.Formulas.ByName('AGENCIA').Formula.Text := lsAgencia;
            formImprimir.rptRelatorio.Formulas.ByName('CONTA').Formula.Text := lsConta;

    //        formImprimir.rptRelatorio.Formulas[lwConta + 2] := VAZIO;
//            formImprimir.rptRelatorio.Formulas[lwConta + 2] := 'DESCONTO= ''' + lsDesconto + '''';
//            formImprimir.rptRelatorio.Formulas[lwConta + 3] := VAZIO;
            { se for seguro }
            if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'S' then
              formImprimir.rptRelatorio.Selection.Formula.Text := '{REC_SEGU.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + ''''
            { se for servico }
            else if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
              formImprimir.rptRelatorio.Selection.Formula.Text := '{REC_SERV.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + ''''
            else
              formImprimir.rptRelatorio.Selection.Formula.Text := '{DbRecebimento.CODIGO}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
            { Assiona o Relatorio}
            formImprimir.rptRelatorio.Print;
//          end;
        end;

        { atualza barra de progresso }
        Inc(liConta);
        prbReenvioBanco.Position := liConta;

        gaParm[0] := lsRetorno;
        { se for seguradora}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'I' then
        begin
          gaParm[1] := 'CodigoSeg+Num_Parc = ''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
          gaParm[2] := 'Codigo';
        end
        else
        begin
          gaParm[1] := 'Codigo = ''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
          gaParm[2] := 'Codigo';
        end;
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;

        dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_EDIT);

        dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Data_Envio', miIndCol[1], Date);

        { faz a atualizacao}
        dmDados.gsRetorno := dmDados.Alterar(maConexao[0], lsRetorno, VAZIO);

        dmDados.FecharConexao(maConexao[0]);

        { proxima pessoa}
        if (dmDados.Proximo(miConexaoAtualiza) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
      end;

      { se for arquivo }
      if lsEnvio = '1' then
        { emite a ultima linha do arquivo}
        untGeraArquivo.EmiteLinha9;

      { se for arquivo }
      if lsEnvio = '1' then
        untGeraArquivo.gp_GeraArquivo_Finalizar;

      { Inicializa variavel de sucesso }
//      gbProcessoOK := True;
    end;

    { se for Reajuste de Parcelas }
    if grProcessos.sNomePainel = REAJUSTE_PARCELAS then
    begin
      laParams[0].sNome := '@DataIni';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftDateTime;
      laParams[0].vValor := StrToDate(DateToStr(dtpDataInicialReajuste.Date));
      laParams[1].sNome := '@DataFim';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftDateTime;
      laParams[1].vValor := StrToDate(DateToStr(dtpDataFinalReajuste.Date));
      laParams[2].sNome := '@TipoProp';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := untCombos.gf_Combo_SemDescricao(cboTipoParcelaReajuste);
      laParams[3].sNome := '@NumIni';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftString;
      laParams[3].vValor := mskPropostaInicialReajuste.Text;
      laParams[4].sNome := '@NumFim';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftString;
      laParams[4].vValor := mskPropostaFinalReajuste.Text;
      laParams[5].sNome := '@Data';
      laParams[5].ptTipo := ptInput;
      laParams[5].ftTipoDado := ftString;
      laParams[5].vValor := mskNumTaxaReajuste.EditText;
      dmDados.CriarStored('Reajusta_Parcelas', laParams, 5);

      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;

    { se for Estorno de Parcelas }
    if grProcessos.sNomePainel = ESTORNO_PARCELAS then
    begin
      { recebimento ou servico }
      if grProcessos.sCod_Processo = PROCESSO_ESTRECEB then
        gaParm[0] := 'RECEB'
      else
        if grProcessos.sCod_Processo = PROCESSO_ESTSERVICO then
          gaParm[0] := 'REC_SERV'
        else
          gaParm[0] := 'REC_SEGU';
      gaParm[1] := 'CODIGO = ''' + edtCodigoReceb.Text + '''';
      gaParm[2] := 'CODIGO';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      miConexaoAtualiza := dmDados.ExecutarSelect(msSql);
      if miConexaoAtualiza = IOPTR_NOPOINTER then
        Exit;

      if not dmDados.Status(miConexaoAtualiza, IOSTATUS_NOROWS) then
      begin
        dmDados.gsRetorno := dmDados.AtivaModo(miConexaoAtualiza,IOMODE_EDIT);

        if (Trim(mskDataRecebimento.Text) <> VAZIO) and (mskDataRecebimento.Text <> VAZIO_DATA) then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_RECEB', miIndCol[2], StrToDateTime(mskDataRecebimento.Text))
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_RECEB', miIndCol[2], VAZIO);

        if Trim(mskValorRecebido.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_RECEB', miIndCol[3], StrToFloat(dmDados.TirarSeparador(mskValorRecebido.Text)))
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_RECEB', miIndCol[3], 0);

        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'CAIXA_BANC', miIndCol[4], untCombos.gf_Combo_SemDescricao(cboRecebCaixaBanco));

        if Trim(mskValorDesconto.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_DESC', miIndCol[5], StrToFloat(dmDados.TirarSeparador(mskValorDesconto.Text)))
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_DESC', miIndCol[5], 0);

        if (Trim(mskDataCredito.Text) <> VAZIO) and (mskDataCredito.Text <> VAZIO_DATA) then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_CREDI', miIndCol[8], StrToDateTime(mskDataCredito.Text))
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_CREDI', miIndCol[8], VAZIO);

        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'PAGTO', miIndCol[9], untCombos.gf_Combo_SemDescricao(cboRecebPagto));

        if (Trim(mskDataVencimento.Text) <> VAZIO) and (mskDataVencimento.Text <> VAZIO_DATA) then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'D_VENCTO', miIndCol[10], StrToDateTime(mskDataVencimento.Text))
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'D_VENCTO', miIndCol[10], VAZIO);

        if Trim(mskValorOriginal.Text) <> VAZIO then
        begin
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_PARC', miIndCol[11], StrToFloat(dmDados.TirarSeparador(mskValorOriginal.Text)));
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'P_MULTA', miIndCol[12], StrToFloat(dmDados.TirarSeparador(mskValorOriginal.Text)) * StrToFloat(dmDados.TirarSeparador(dmDados.ConfigValor('Multa'))) / 100);
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'P_JUROS', miIndCol[13], StrToFloat(dmDados.TirarSeparador(mskValorOriginal.Text)) * StrToFloat(dmDados.TirarSeparador(dmDados.ConfigValor('Juros_Dias'))) / 100);
        end;

        { faz a atualizacao}
        { recebimento ou servico }
        if grProcessos.sCod_Processo = PROCESSO_ESTRECEB then
          dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'RECEB', VAZIO)
        else
          if grProcessos.sCod_Processo = PROCESSO_ESTSERVICO then
            dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'REC_SERV', VAZIO)
          else
            dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'REC_SEGU', VAZIO);
      end;
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;

    { se for Quitar Parcelas }
    if grProcessos.sNomePainel = QUITAR_PARCELAS then
    begin
      { quitacao ou recibo parcial }
      if (grProcessos.sCod_Processo = PROCESSO_QUITAR) or (grProcessos.sCod_Processo = PROCESSO_SERVICO) or (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
      begin
        if (grProcessos.sCod_Processo = PROCESSO_QUITAR) then
        begin
          gaParm[0] := 'RECEB';
          gaParm[1] := 'NUM_PROP = ''' + edtCodigoPropQuitarPag.Text + ''' AND ' +
                       '(PAGTO = ''N'' OR PAGTO IS NULL) AND ' +
                       '(PARCELAS = ''00'' OR PARCELAS IS NULL)';
        end
        else
        begin
          if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
          begin
            gaParm[0] := 'REC_SERV';
            gaParm[1] := 'NUM_PROP = ''' + edtCodigoPropQuitarPag.Text + ''' AND (PAGTO = ''N'' OR PAGTO IS NULL)';
          end
          else
          begin
            gaParm[0] := 'REC_SEGU';
            gaParm[1] := 'NUM_PROP = ''' + edtCodigoPropQuitarPag.Text + ''' AND (PAGTO = ''N'' OR PAGTO IS NULL)';
          end;
        end;
        gaParm[2] := 'CODIGO';
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        miConexaoAtualiza := dmDados.ExecutarSelect(msSql);
        if miConexaoAtualiza = IOPTR_NOPOINTER then
          Exit;

        {varre a lista procurando alguma item marcado}
        for lwConta := 1 to grdParcelasQuitarPag.MaxRows do
        begin
          grdParcelasQuitarPag.Row := lwConta;
          {posiciona na coluna do Check}
          grdParcelasQuitarPag.Col := COL_QUIT_CHECK;
          {se o tiver marcado}
          if grdParcelasQuitarPag.Text = '1' then
          begin
            {posiciona na coluna dA Codigo}
            grdParcelasQuitarPag.Col := COL_QUIT_CODIGO;
            { Posiciona no item certo}
            dmDados.gsRetorno := dmDados.AtivaFiltro(miConexaoAtualiza, 'CODIGO = ''' + grdParcelasQuitarPag.Text + '''', IOFLAG_FIRST, IOFLAG_GET);
            if not dmDados.Status(miConexaoAtualiza, IOSTATUS_NOROWS) then
            begin
              dmDados.gsRetorno := dmDados.AtivaModo(miConexaoAtualiza,IOMODE_EDIT);

              grdParcelasQuitarPag.Col := COL_QUIT_DATARECEB;
              if (Trim(grdParcelasQuitarPag.Text) <> VAZIO) and (grdParcelasQuitarPag.Text <> VAZIO_DATA) then
                dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_RECEB', miIndCol[2], StrToDateTime(grdParcelasQuitarPag.Text))
              else
                dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_RECEB', miIndCol[2], Null);

              grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
              if Trim(grdParcelasQuitarPag.Text) <> VAZIO then
                dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_RECEB', miIndCol[3], StrToFloat(dmDados.TirarSeparador(grdParcelasQuitarPag.Text)));

              grdParcelasQuitarPag.Col := COL_QUIT_TIPOBAIXA;
              if Trim(grdParcelasQuitarPag.Text) <> VAZIO then
                dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'CAIXA_BANC', miIndCol[4], untCombos.gf_Combo_SpreadSemDescricao(grdParcelasQuitarPag));

              grdParcelasQuitarPag.Col := COL_QUIT_DESCONTO;
              if Trim(grdParcelasQuitarPag.Text) <> VAZIO then
                dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_DESC', miIndCol[5], StrToFloat(dmDados.TirarSeparador(grdParcelasQuitarPag.Text)));

              grdParcelasQuitarPag.Col := COL_QUIT_DATACREDITO;
              if (Trim(grdParcelasQuitarPag.Text) <> VAZIO) and (grdParcelasQuitarPag.Text <> VAZIO_DATA) then
                dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_CREDI', miIndCol[8], StrToDateTime(grdParcelasQuitarPag.Text))
              else
                dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_CREDI', miIndCol[8], Null);

              dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'PAGTO', miIndCol[9], 'S');

              grdParcelasQuitarPag.Col := COL_QUIT_TIPORECEB;
              if Trim(grdParcelasQuitarPag.Text) <> VAZIO then
                dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'TIPO_REC', miIndCol[10], untCombos.gf_Combo_SpreadSemDescricao(grdParcelasQuitarPag));

              grdParcelasQuitarPag.Col := COL_QUIT_HISTORICO;
              if Trim(grdParcelasQuitarPag.Text) <> VAZIO then
                if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
                  dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'OBSERVACAO', miIndCol[12], grdParcelasQuitarPag.Text)
                else
                  dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'HISTORICO', miIndCol[12], grdParcelasQuitarPag.Text);

              { se for quitacao normal tem taxa }
              if (grProcessos.sCod_Processo = PROCESSO_QUITAR)  then
              begin
                grdParcelasQuitarPag.Col := COL_QUIT_TIPOTAXA;
                if Trim(grdParcelasQuitarPag.Text) <> VAZIO then
                  dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'TIPO_TAXA', miIndCol[11], untCombos.gf_Combo_SpreadSemDescricao(grdParcelasQuitarPag));
              end;
              if Trim(edtReciboQuitarPag.Text) <> VAZIO then
                dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'RECIBO', miIndCol[13], edtReciboQuitarPag.Text);
              dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'USUARIO', miIndCol[14], gsNomeUsuario);

              { faz a atualizacao}
              if (grProcessos.sCod_Processo = PROCESSO_QUITAR) then
                dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'RECEB', VAZIO)
              else
                if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
                  dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'REC_SERV', VAZIO)
                else
                  dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'REC_SEGU', VAZIO);
            end;
          end;
        end;
        { se for cheque }
        if (chkDinheiro.Checked) and (mskNumParcQuitarPag.Text <> VAZIO) then
        begin
          { pegar o proximo codigo do cheque pre }
          for liConta := 0 to High(miIndCol) do
            miIndCol[liConta] := IOPTR_NOPOINTER;
          lsSql := 'SELECT MAX(Codigo) AS Codigo_Int FROM Chq_Pre';
          maConexao[0] := dmDados.ExecutarSelect(lsSql);
          if maConexao[0] = IOPTR_NOPOINTER then
            Exit;
          { se n�o encontrou a op��o }
          if not dmDados.Status(maConexao[0], IOSTATUS_NOROWS) Then
          begin
            lsCodigo := dmDados.ValorColuna(maConexao[0], 'Codigo_Int', miIndCol[0]);
            if lsCodigo <> VAZIO then
              lsCodigo := FormatFloat('00000000', StrToInt(lsCodigo) + 1)
            else
              lsCodigo := '00000001';
          end
          else
          begin
            lsCodigo := '00000001';
          end;
          dmDados.FecharConexao(maConexao[0]);

          { gravar o cabecalho do cheque pre }
          gaParm[0] := 'CHQ_PRE';
          gaParm[1] := 'CODIGO = ''' + lsCodigo + '''';
          gaParm[2] := 'CODIGO';
          msSql := dmDados.SqlVersao('NEC_0002', gaParm);
          maConexao[0] := dmDados.ExecutarSelect(msSql);
          if maConexao[0] = IOPTR_NOPOINTER then
            Exit;
          { nao existe }
          if dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
          begin
            dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_INSERT);

            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGO', miIndCol[0], lsCodigo);
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'DESCRICAO', miIndCol[1], edtCodigoPropQuitarPag.Text + ' - ' + FormatDateTime(MK_DATATELA, Date));
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'NUM_PROP', miIndCol[2], edtCodigoPropQuitarPag.Text);
            if mskNumParcQuitarPag.Text <> VAZIO then
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'NUM_PARC', miIndCol[3], StrToInt(mskNumParcQuitarPag.Text));
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'DATA', miIndCol[4], Date);
            if edtTotalQuitarPag.Text <> VAZIO then
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'VALOR_TOTAL', miIndCol[5], edtTotalQuitarPag.Text);
            if mskValorDinheiroQuitarPag.Text <> VAZIO then
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'VALOR_DIN', miIndCol[6], mskValorDinheiroQuitarPag.Text);
          if Trim(edtReciboQuitarPag.Text) <> VAZIO then
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'RECIBO', miIndCol[7], edtReciboQuitarPag.Text);

            dmDados.gsRetorno := dmDados.Incluir(maConexao[0], 'CHQ_PRE');
          end;
          dmDados.FecharConexao(maConexao[0]);

          { gravar os cheques }
          gaParm[0] := 'CHQ_PRE_PARC';
          gaParm[1] := 'CODIGOCHEQUE = ''' + lsCodigo + '''';
          gaParm[2] := 'CODIGO';
          msSql := dmDados.SqlVersao('NEC_0002', gaParm);
          maConexao[0] := dmDados.ExecutarSelect(msSql);
          if maConexao[0] = IOPTR_NOPOINTER then
            Exit;
          { nao existe }
          if dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
          begin
            for lwConta := 1 to grdChequesQuitarPag.MaxRows do
            begin
              grdChequesQuitarPag.Row := lwConta;

              dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_INSERT);

              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGOCHEQUE', miIndCol[0], lsCodigo);
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGO', miIndCol[1], lwConta);

              grdChequesQuitarPag.Col := COL_CHQ_BANCO;
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'BANCO', miIndCol[2], grdChequesQuitarPag.Text);

              grdChequesQuitarPag.Col := COL_CHQ_DATAVENC;
              if (Trim(grdChequesQuitarPag.Text) <> VAZIO) and (grdChequesQuitarPag.Text <> VAZIO_DATA) then
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'D_VENCTO', miIndCol[4], StrToDateTime(grdChequesQuitarPag.Text))
              else
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'D_VENCTO', miIndCol[4], Null);

              grdChequesQuitarPag.Col := COL_CHQ_VALORPARC;
              if Trim(grdChequesQuitarPag.Text) <> VAZIO then
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'VALOR', miIndCol[3], StrToFloat(dmDados.TirarSeparador(grdChequesQuitarPag.Text)));

              grdChequesQuitarPag.Col := COL_CHQ_NUMCHEQUE;
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'NUM_CHEQUE', miIndCol[5], grdChequesQuitarPag.Text);

              grdChequesQuitarPag.Col := COL_CHQ_DATARECEB;
              if (Trim(grdChequesQuitarPag.Text) <> VAZIO) and (grdChequesQuitarPag.Text <> VAZIO_DATA) then
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'DATA_RECEB', miIndCol[6], StrToDateTime(grdChequesQuitarPag.Text))
              else
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'DATA_RECEB', miIndCol[6], Null);

              dmDados.gsRetorno := dmDados.Incluir(maConexao[0], 'CHQ_PRE_PARC');
            end;
          end;
          dmDados.FecharConexao(maConexao[0]);

          { para identificar o tipo de documento }
          if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
            gaParm[0] := 'TAB_SERV'
          else
            gaParm[0] := TABELA_TRECEB;
          gaParm[1] := 'CODIGO';
          msSql := dmDados.SqlVersao('NEC_0001', gaParm);
          maConexao[1] := dmDados.ExecutarSelect(msSql);
          if maConexao[1] = IOPTR_NOPOINTER then
            Exit;

          { gravar as parcelas pagas }
          gaParm[0] := 'CHQ_PRE_RECEB';
          gaParm[1] := 'CODIGOCHEQUE = ''' + lsCodigo + '''';
          gaParm[2] := 'CODIGO';
          msSql := dmDados.SqlVersao('NEC_0002', gaParm);
          maConexao[0] := dmDados.ExecutarSelect(msSql);
          if maConexao[0] = IOPTR_NOPOINTER then
            Exit;
          { nao existe }
          if dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
          begin
            for lwConta := 1 to grdParcelasQuitarPag.MaxRows do
            begin
              grdParcelasQuitarPag.Row := lwConta;
              {posiciona na coluna do Check}
              grdParcelasQuitarPag.Col := COL_QUIT_CHECK;
              {se o tiver marcado}
              if grdParcelasQuitarPag.Text = '1' then
              begin
                dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_INSERT);

                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGOCHEQUE', miIndCol[0], lsCodigo);
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGO', miIndCol[1], lwConta);

                {posiciona na coluna do Codigo}
                grdParcelasQuitarPag.Col := COL_QUIT_CODIGO;
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGORECEB', miIndCol[2], grdParcelasQuitarPag.Text);

                grdParcelasQuitarPag.Col := COL_QUIT_REFERENCIA;
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'NUM_PARC', miIndCol[3], grdParcelasQuitarPag.Text);

                grdParcelasQuitarPag.Col := COL_QUIT_TIPODOC;
//                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'TIPO_DOC', miIndCol[4], grdParcelasQuitarPag.Text);
                if (grProcessos.sCod_Processo = PROCESSO_SERVICO) then
                begin
                  dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'TIPO_DOC', miIndCol[4], 'V');
                  dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'COD_SERV', miIndCol[7], untCombos.gf_ComboCadastro_Conteudo(maConexao[1], 'DESCRICAO', grdParcelasQuitarPag.Text, 'CODIGO'));
                end
                else
                  dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'TIPO_DOC', miIndCol[4], untCombos.gf_ComboCadastro_Conteudo(maConexao[1], 'DESCRICAO', grdParcelasQuitarPag.Text, 'CODIGO'));

                grdParcelasQuitarPag.Col := COL_QUIT_DATAVENC;
                if (Trim(grdParcelasQuitarPag.Text) <> VAZIO) and (grdParcelasQuitarPag.Text <> VAZIO_DATA) then
                  dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'D_VENCTO', miIndCol[5], StrToDateTime(grdParcelasQuitarPag.Text))
                else
                  dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'D_VENCTO', miIndCol[5], Null);

                grdParcelasQuitarPag.Col := COL_QUIT_VALORVENC;
                if Trim(grdParcelasQuitarPag.Text) <> VAZIO then
                  dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'VLR_PARC', miIndCol[6], StrToFloat(dmDados.TirarSeparador(grdParcelasQuitarPag.Text)));

                dmDados.gsRetorno := dmDados.Incluir(maConexao[0], 'CHQ_PRE_RECEB');
              end;
            end;
          end;
          dmDados.FecharConexao(maConexao[0]);
          dmDados.FecharConexao(maConexao[1]);
        end;
      end;
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;
    { se for Lancar Servico }
    if grProcessos.sNomePainel = LANCAR_SERVICO then
    begin
      gaParm[0] := 'REC_SERV';
      gaParm[1] := 'NUM_PROP = ''' + mskCodigoPropLancarServico.Text + '''';
      gaParm[2] := 'CODIGO';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      miConexaoAtualiza := dmDados.ExecutarSelect(msSql);
      if miConexaoAtualiza = IOPTR_NOPOINTER then
        Exit;

      grdParcelasLancarServico.Row := 1;
      {posiciona na coluna do Servico }
      grdParcelasLancarServico.Col := COL_SERV_TIPO;
      if grdParcelasLancarServico.Text <> VAZIO then
        lsServico := untCombos.gf_Combo_SpreadSemDescricao(grdParcelasLancarServico);
      { Posiciona no mesmo servico }
      dmDados.gsRetorno := dmDados.AtivaFiltro(miConexaoAtualiza, 'COD_SERV = ''' + lsServico + '''', IOFLAG_FIRST, IOFLAG_GET);
      { nao achou }
      if dmDados.Status(miConexaoAtualiza, IOSTATUS_NOROWS) then
        lsParcela := '0001'
      else
      begin
        dmDados.gsRetorno := dmDados.Ultimo(maConexao[0]);
        lsParcela := FormatFloat('0000', StrToInt(dmDados.ValorColuna(miConexaoAtualiza, 'NUM_PARC', miIndCol[0])) + 1);
      end;

      {varre a lista procurando algum item marcado}
      for lwConta := 1 to grdChequesLancarServico.DataRowCnt do
      begin
        grdChequesLancarServico.Row := lwConta;

        dmDados.gsRetorno := dmDados.AtivaModo(miConexaoAtualiza,IOMODE_INSERT);

        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'CODIGO', miIndCol[1], mskCodigoPropLancarServico.Text + lsServico + lsParcela);
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DESCRICAO', miIndCol[2], edtNomePropLancarServico.Text);
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'COD_SERV', miIndCol[3], lsServico);
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'NUM_PROP', miIndCol[4], mskCodigoPropLancarServico.Text);
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_LANC', miIndCol[5], Date);
        { valor da parcela }
        grdChequesLancarServico.Col := COL_CHQ_VALORPARC;
        if Trim(grdChequesLancarServico.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_PARC', miIndCol[6], StrToFloat(dmDados.TirarSeparador(grdChequesLancarServico.Text)));
        { data do vencimento }
        grdChequesLancarServico.Col := COL_CHQ_DATAVENC;
        if (Trim(grdChequesLancarServico.Text) <> VAZIO) and (grdChequesLancarServico.Text <> VAZIO_DATA) then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'D_VENCTO', miIndCol[7], StrToDateTime(grdChequesLancarServico.Text))
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'D_VENCTO', miIndCol[7], Null);
        { data de recebimento }
        grdChequesLancarServico.Col := COL_CHQ_DATARECEB;
        if (Trim(grdChequesLancarServico.Text) <> VAZIO) and (grdChequesLancarServico.Text <> VAZIO_DATA) then
        begin
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_RECEB', miIndCol[9], StrToDateTime(grdChequesLancarServico.Text));
          { data de credito }
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_CREDI', miIndCol[10], StrToDateTime(grdChequesLancarServico.Text));
          { valor recebido }
          grdChequesLancarServico.Col := COL_CHQ_VALORPARC;
          if Trim(grdChequesLancarServico.Text) <> VAZIO then
            dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_RECEB', miIndCol[8], StrToFloat(dmDados.TirarSeparador(grdChequesLancarServico.Text)));
          { Caixa ou banco }
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'CAIXA_BANC', miIndCol[13], 'C');
          { pago }
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'PAGTO', miIndCol[15], 'S');
        end
        else
        begin
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_RECEB', miIndCol[9], Null);
          { data de credito }
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_CREDI', miIndCol[10], Null);
          { Caixa ou banco }
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'CAIXA_BANC', miIndCol[13], VAZIO);
          { pago }
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'PAGTO', miIndCol[15], 'N');
        end;
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'NUM_PARC', miIndCol[11], lsParcela);
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_DESC', miIndCol[12], 0);
        grdChequesLancarServico.Col := COL_CHQ_TIPOREC;
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'TIPO_REC', miIndCol[14], untCombos.gf_Combo_SpreadSemDescricao(grdChequesLancarServico));
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'TIPO_COBR', miIndCol[16], '1');
        grdChequesLancarServico.Col := COL_CHQ_VALORPARC;
        if Trim(grdChequesLancarServico.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'P_MULTA', miIndCol[17], StrToFloat(dmDados.TirarSeparador(grdChequesLancarServico.Text)) * VLR_MULTA);
        if Trim(grdChequesLancarServico.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'P_JUROS', miIndCol[17], StrToFloat(dmDados.TirarSeparador(grdChequesLancarServico.Text)) * VLR_JUROS);
        { banco }
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'BANCO', miIndCol[18], '1');
        { numero do cheque }
        grdChequesLancarServico.Col := COL_CHQ_NUMCHEQUE;
        if Trim(grdChequesLancarServico.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'NUM_CHEQUE', miIndCol[19], grdChequesLancarServico.Text);
        { obs }
        grdChequesLancarServico.Col := COL_CHQ_OBS;
        if Trim(grdChequesLancarServico.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'OBSERVACAO', miIndCol[20], grdChequesLancarServico.Text);
        { recibo }
        if Trim(edtReciboLancarServico.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'RECIBO', miIndCol[21], edtReciboLancarServico.Text);
        { documento }
        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DOCUMENTO', miIndCol[22], mskCodigoPropLancarServico.Text + lsServico + lsParcela);

        dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'REC_SERV', VAZIO);

        grdChequesLancarServico.Col := COL_CHQ_CODIGO;
        grdChequesLancarServico.Text := mskCodigoPropLancarServico.Text + lsServico + lsParcela;

        { proxima parcela }
        lsParcela := FormatFloat('0000', StrToInt(lsParcela) + 1);
      end;

      { guarda os servicos }
      gaParm[0] := 'SERVICO';
      gaParm[1] := 'NUM_PROP = ''' + mskCodigoPropLancarServico.Text + '''';
      gaParm[2] := 'CODIGO';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      { nao achou }
      if dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
        lsParcela := '0001'
      else
      begin
        dmDados.gsRetorno := dmDados.Ultimo(maConexao[0]);
        lsParcela := FormatFloat('0000', dmDados.ValorColuna(maConexao[0], 'CODIGO', miIndCol[0]) + 1);
      end;

      { varre a lista procurando algum item marcado}
      for lwConta := 1 to grdParcelasLancarServico.DataRowCnt do
      begin
        grdParcelasLancarServico.Row := lwConta;

        dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_INSERT);

        dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'NUM_PROP', miIndCol[1], mskCodigoPropLancarServico.Text);
        dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGO', miIndCol[2], StrToInt(lsParcela));
        dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'DATA_LANC', miIndCol[3], Date);
        { posiciona na coluna do Servico }
        grdParcelasLancarServico.Col := COL_SERV_TIPO;
        if grdParcelasLancarServico.Text <> VAZIO then
        dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'COD_SERV', miIndCol[4], untCombos.gf_Combo_SpreadSemDescricao(grdParcelasLancarServico));
        { quantidade }
        grdParcelasLancarServico.Col := COL_SERV_QTDE;
        if Trim(grdParcelasLancarServico.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'QUANTIDADE', miIndCol[5], StrToInt(grdParcelasLancarServico.Text));
        { valor Total }
        grdParcelasLancarServico.Col := COL_SERV_TOTAL;
        if Trim(grdParcelasLancarServico.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'VALOR', miIndCol[6], StrToFloat(dmDados.TirarSeparador(grdParcelasLancarServico.Text)));
        { recibo }
        if Trim(edtReciboLancarServico.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'RECIBO', miIndCol[7], edtReciboLancarServico.Text);

        dmDados.gsRetorno := dmDados.Alterar(maConexao[0], 'SERVICO', VAZIO);

        { proxima parcela }
        lsParcela := FormatFloat('0000', StrToInt(lsParcela) + 1);
      end;
      dmDados.FecharConexao(maConexao[0]);

      {varre a lista procurando algum item marcado}
      lbOK := False;
      for lwConta := 1 to grdChequesLancarServico.DataRowCnt do
      begin
        grdChequesLancarServico.Row := lwConta;
        grdChequesLancarServico.Col := COL_CHQ_TIPOREC;
        lsServico := untCombos.gf_Combo_SpreadSemDescricao(grdChequesLancarServico);
        if lsServico[1] in TIPO_CHEQUE then
        begin
          lbOK := True;
          break;
        end;
      end;

      { se for cheque }
      if lbOK then
      begin
        { pegar o proximo codigo do cheque pre }
        for liConta := 0 to High(miIndCol) do
          miIndCol[liConta] := IOPTR_NOPOINTER;
        lsSql := 'SELECT MAX(Codigo) AS Codigo_Int FROM Chq_Pre';
        maConexao[0] := dmDados.ExecutarSelect(lsSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;
        { se n�o encontrou a op��o }
        if not dmDados.Status(maConexao[0], IOSTATUS_NOROWS) Then
        begin
          lsCodigo := dmDados.ValorColuna(maConexao[0], 'Codigo_Int', miIndCol[0]);
          if lsCodigo <> VAZIO then
            lsCodigo := FormatFloat('00000000', StrToInt(lsCodigo) + 1)
          else
            lsCodigo := '00000001';
        end
        else
        begin
          lsCodigo := '00000001';
        end;
        dmDados.FecharConexao(maConexao[0]);

        { gravar o cabecalho do cheque pre }
        gaParm[0] := 'CHQ_PRE';
        gaParm[1] := 'CODIGO = ''' + lsCodigo + '''';
        gaParm[2] := 'CODIGO';
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;
        { nao existe }
        if dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
        begin
          dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_INSERT);

          dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGO', miIndCol[0], lsCodigo);
          dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'DESCRICAO', miIndCol[1], mskCodigoPropLancarServico.Text + ' - ' + FormatDateTime(MK_DATATELA, Date));
          dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'NUM_PROP', miIndCol[2], mskCodigoPropLancarServico.Text);
          if mskNumParcLancarServico.Text <> VAZIO then
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'NUM_PARC', miIndCol[3], StrToInt(mskNumParcLancarServico.Text));
          dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'DATA', miIndCol[4], Date);
          if edtTotalLancarServico.Text <> VAZIO then
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'VALOR_TOTAL', miIndCol[5], edtTotalLancarServico.Text);
          if mskValorDinheiroLancarServico.Text <> VAZIO then
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'VALOR_DIN', miIndCol[6], mskValorDinheiroLancarServico.Text);
          if Trim(edtReciboLancarServico.Text) <> VAZIO then
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'RECIBO', miIndCol[7], edtReciboLancarServico.Text);

          dmDados.gsRetorno := dmDados.Incluir(maConexao[0], 'CHQ_PRE');
        end;
        dmDados.FecharConexao(maConexao[0]);

        { gravar os cheques }
        gaParm[0] := 'CHQ_PRE_PARC';
        gaParm[1] := 'CODIGOCHEQUE = ''' + lsCodigo + '''';
        gaParm[2] := 'CODIGO';
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;
        { nao existe }
        if dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
        begin
          for lwConta := 1 to grdChequesLancarServico.MaxRows do
          begin
            grdChequesLancarServico.Row := lwConta;

            { se for cheque }
            grdChequesLancarServico.Col := COL_CHQ_TIPOREC;
            lsServico := untCombos.gf_Combo_SpreadSemDescricao(grdChequesLancarServico);
            if lsServico[1] in TIPO_CHEQUE then
            begin
              dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_INSERT);

              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGOCHEQUE', miIndCol[0], lsCodigo);
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGO', miIndCol[1], lwConta);

              grdChequesLancarServico.Col := COL_CHQ_BANCO;
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'BANCO', miIndCol[2], grdChequesLancarServico.Text);

              grdChequesLancarServico.Col := COL_CHQ_DATAVENC;
              if (Trim(grdChequesLancarServico.Text) <> VAZIO) and (grdChequesLancarServico.Text <> VAZIO_DATA) then
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'D_VENCTO', miIndCol[4], StrToDateTime(grdChequesLancarServico.Text))
              else
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'D_VENCTO', miIndCol[4], Null);

              grdChequesLancarServico.Col := COL_CHQ_VALORPARC;
              if Trim(grdChequesLancarServico.Text) <> VAZIO then
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'VALOR', miIndCol[3], StrToFloat(dmDados.TirarSeparador(grdChequesLancarServico.Text)));

              grdChequesLancarServico.Col := COL_CHQ_NUMCHEQUE;
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'NUM_CHEQUE', miIndCol[5], grdChequesLancarServico.Text);

              grdChequesLancarServico.Col := COL_CHQ_DATARECEB;
              if (Trim(grdChequesLancarServico.Text) <> VAZIO) and (grdChequesLancarServico.Text <> VAZIO_DATA) then
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'DATA_RECEB', miIndCol[6], StrToDateTime(grdChequesLancarServico.Text))
              else
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'DATA_RECEB', miIndCol[6], Null);

              dmDados.gsRetorno := dmDados.Incluir(maConexao[0], 'CHQ_PRE_PARC');
            end;
          end;
        end;
        dmDados.FecharConexao(maConexao[0]);

        { gravar as parcelas pagas }
        gaParm[0] := 'CHQ_PRE_RECEB';
        gaParm[1] := 'CODIGOCHEQUE = ''' + lsCodigo + '''';
        gaParm[2] := 'CODIGO';
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;
        { nao existe }
        if dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
        begin
          for lwConta := 1 to grdParcelasLancarServico.DataRowCnt do
          begin
            grdParcelasLancarServico.Row := lwConta;
            {posiciona na coluna do Check}
            grdParcelasLancarServico.Col := COL_SERV_TIPO;
            {se o tiver marcado}
            if grdParcelasLancarServico.Text <> VAZIO then
            begin
              dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_INSERT);

              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGOCHEQUE', miIndCol[0], lsCodigo);
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGO', miIndCol[1], lwConta);

              {posiciona na coluna do Codigo}
              grdParcelasLancarServico.Col := COL_SERV_CODIGO;
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'CODIGORECEB', miIndCol[2], grdParcelasLancarServico.Text);
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'NUM_PARC', miIndCol[3], FormatFloat('00',lwConta));
              grdParcelasLancarServico.Col := COL_SERV_TIPO;
              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'COD_SERV', miIndCol[4], untCombos.gf_Combo_SpreadSemDescricao(grdParcelasLancarServico));

              dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'TIPO_DOC', miIndCol[5], 'V');

              if (Trim(mskDataLancarServico.Text) <> VAZIO) and (mskDataLancarServico.Text <> VAZIO_DATA) then
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'D_VENCTO', miIndCol[6], StrToDateTime(mskDataLancarServico.Text))
              else
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'D_VENCTO', miIndCol[6], Null);

              grdParcelasLancarServico.Col := COL_SERV_TOTAL;
              if Trim(grdParcelasLancarServico.Text) <> VAZIO then
                dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'VLR_PARC', miIndCol[7], StrToFloat(dmDados.TirarSeparador(grdParcelasLancarServico.Text)));

              dmDados.gsRetorno := dmDados.Incluir(maConexao[0], 'CHQ_PRE_RECEB');
            end;
          end;
          dmDados.FecharConexao(maConexao[0]);
        end;
      end;
      { Inicializa variavel de sucesso }
      gbProcessoOK := True;
    end;
    { se for Baixa Automatica }
    if grProcessos.sNomePainel = BAIXA_AUTO then
    begin
      if untCombos.gf_Combo_SemDescricao(cboBaixaTipoReceb)[1] In ['L', 'G', 'N', 'T'] then
      begin
        gaParm[0] := 'RECEB';
        gaParm[1] := 'CODIGO = ''' + edtBaixaCodigo.Text + ''' AND ' +
                     '(PAGTO = ''N'' OR PAGTO IS NULL) AND ' +
                     '(PARCELAS = ''00'' OR PARCELAS IS NULL)';
      end
      else if untCombos.gf_Combo_SemDescricao(cboBaixaTipoReceb) = 'V' then
      begin
        gaParm[0] := 'REC_SERV';
        gaParm[1] := 'CODIGO = ''' + edtBaixaCodigo.Text + ''' AND (PAGTO = ''N'' OR PAGTO IS NULL)';
      end
      else if untCombos.gf_Combo_SemDescricao(cboBaixaTipoReceb) = 'S' then
      begin
        gaParm[0] := 'REC_SEGU';
        gaParm[1] := 'CODIGO = ''' + edtBaixaCodigo.Text + ''' AND (PAGTO = ''N'' OR PAGTO IS NULL)';
      end;
      gaParm[2] := 'CODIGO';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      miConexaoAtualiza := dmDados.ExecutarSelect(msSql);
      if miConexaoAtualiza = IOPTR_NOPOINTER then
        Exit;

      { Posiciona no item certo}
//      dmDados.gsRetorno := dmDados.AtivaFiltro(miConexaoAtualiza, 'CODIGO = ''' + edtBaixaCodigo.Text + '''', IOFLAG_FIRST, IOFLAG_GET);
      if not dmDados.Status(miConexaoAtualiza, IOSTATUS_NOROWS) then
      begin
        dmDados.gsRetorno := dmDados.AtivaModo(miConexaoAtualiza,IOMODE_EDIT);

        if (Trim(mskBaixaDataReceb.Text) <> VAZIO) and (mskBaixaDataReceb.Text <> VAZIO_DATA) then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_RECEB', miIndCol[2], StrToDateTime(mskBaixaDataReceb.Text))
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_RECEB', miIndCol[2], Null);

        if Trim(mskBaixaValorRecebido.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_RECEB', miIndCol[3], StrToFloat(dmDados.TirarSeparador(mskBaixaValorRecebido.Text)));

        if Trim(cboBaixaCaixaBanco.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'CAIXA_BANC', miIndCol[4], untCombos.gf_Combo_SemDescricao(cboBaixaCaixaBanco));

        if Trim(mskBaixaValorDesconto.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'VLR_DESC', miIndCol[5], StrToFloat(dmDados.TirarSeparador(mskBaixaValorDesconto.Text)));

        if (Trim(mskBaixaDataCredito.Text) <> VAZIO) and (mskBaixaDataReceb.Text <> VAZIO_DATA) then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_CREDI', miIndCol[8], StrToDateTime(mskBaixaDataReceb.Text))
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'DATA_CREDI', miIndCol[8], Null);

        dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'PAGTO', miIndCol[9], 'S');

        if (Trim(cboBaixaTipoTaxa.Text) = VAZIO) or
           (untCombos.gf_Combo_SemDescricao(cboBaixaTipoTaxa) = 'U') or
           (untCombos.gf_Combo_SemDescricao(cboBaixaTipoTaxa) = 'S') then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'TIPO_REC', miIndCol[10], 'C')
        else
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'TIPO_REC', miIndCol[10], 'C');

        if Trim(cboBaixaTipoTaxa.Text) <> VAZIO then
          dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAtualiza, 'TIPO_TAXA', miIndCol[11], untCombos.gf_Combo_SemDescricao(cboBaixaTipoTaxa));

        { faz a atualizacao}
        if untCombos.gf_Combo_SemDescricao(cboBaixaTipoReceb)[1] In ['L', 'G', 'N', 'T'] then
          dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'RECEB', VAZIO)
        else if untCombos.gf_Combo_SemDescricao(cboBaixaTipoReceb) = 'V' then
          dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'REC_SERV', VAZIO)
        else if untCombos.gf_Combo_SemDescricao(cboBaixaTipoReceb) = 'S' then
          dmDados.gsRetorno := dmDados.Alterar(miConexaoAtualiza, 'REC_SEGU', VAZIO);

        { escolhe a impressora }
        for liConta := 0 to Printer.Printers.Count - 1 do
          if Printer.Printers[liConta] = dmDados.ConfigValor('Imp_Autentica') then  // 'Generic / Text Only' then
            break;
        { nao achou impressora }
        if liConta >= Printer.Printers.Count then
          Printer.PrinterIndex := -1
        else
          Printer.PrinterIndex := liConta;

        { imprimir 1a Via }
        ShowMessage('Imprimir 1� VIA do Boleto !');
        Screen.Cursor := crHourGlass;
        try
          Printer.BeginDoc;
          try
            Printer.Canvas.TextOut(0, 0, 'DOrione   ' + edtBaixaCodigo.Text + ' ' + mskBaixaDataReceb.Text + '    ' + mskBaixaValorRecebido.Text + 'C');
            Printer.EndDoc;
          except
            { se ocorrer um erro... }
            on Exception do
            begin
              Printer.Abort;
              Printer.EndDoc;
              { repassa o erro !!! }
              raise;
            end;
          end;
        finally
          Screen.Cursor:=crDefault;
        end;

        ShowMessage('Imprimir 2� VIA do Boleto !');
        Screen.Cursor := crHourGlass;
        try
          Printer.BeginDoc;
          try
            Printer.Canvas.TextOut(0, 0, 'DOrione   ' + edtBaixaCodigo.Text + ' ' + mskBaixaDataReceb.Text + '    ' + mskBaixaValorRecebido.Text + 'C' );
            Printer.EndDoc;
          except
            { se ocorrer um erro... }
            on Exception do
            begin
              Printer.Abort;
              Printer.EndDoc;
              { repassa o erro !!! }
              raise;
            end;
          end;
        finally
          Screen.Cursor:=crDefault;
        end;
      end;
      { posiciona no codigo de barras }
      if edtBaixaBarras.Enabled then
        edtBaixaBarras.SetFocus;
      { Inicializa variavel de sucesso }
//      gbProcessoOK := True;
    end;
    { se for Incluir Imagens }
    if grProcessos.sNomePainel = INCLUIR_IMAGENS then
    begin
      lsEnvio := untCombos.gf_Combo_SemDescricao(cboTipoImagens);

      prbIncluirImagens.Min := 0;
      prbIncluirImagens.Max := grdIncluirImagens.MaxRows;
      prbIncluirImagens.Position := 0;

      {varre a lista procurando alguma item marcado}
      for lwConta := 1 to grdIncluirImagens.MaxRows do
      begin
        grdIncluirImagens.Row := lwConta;
        {posiciona na coluna da proposta}
        grdIncluirImagens.Col := 1;
        lsRetorno := grdIncluirImagens.Text;
        {posiciona na coluna do Arquivo}
        grdIncluirImagens.Col := 2;
        {se o tiver marcado}
        if grdIncluirImagens.Text <> VAZIO then
        begin
          { chama a funcao para Incluir a imagem }
          lblIncluirImagens.Caption := grdIncluirImagens.Text;
          formProcessos.Refresh;
          dmDados.gsRetorno := mf_Incluir_Imagem(lsRetorno, lsEnvio, grdIncluirImagens.Text);
          {posiciona na coluna da mensagem}
          grdIncluirImagens.Col := 3;
          grdIncluirImagens.Text := dmDados.gsRetorno;
        end;
        prbIncluirImagens.Position := lwConta;
      end;
      lblIncluirImagens.Caption := VAZIO;
      { Inicializa variavel de sucesso }
//      gbProcessoOK := True;
    end;
    { se for Carta Cobranca }
    if grProcessos.sNomePainel = CARTA_COBRANCA then
    begin
      lsModelo := untCombos.gf_Combo_SemDescricao(cboModeloCartaCobranca);
      lsProposta := untCombos.gf_Combo_SemDescricao(cboTipoPropostaCartaCobranca);
      lsOcupacao := untCombos.gf_Combo_SemDescricao(cboOcupacaoCartaCobranca);
      lsDebito := untCombos.gf_Combo_SemDescricao(cboDebitosCartaCobranca);
      lsValores := untCombos.gf_Combo_SemDescricao(cboValoresCartaCobranca);
      lsAtualizado := untCombos.gf_Combo_SemDescricao(cboAtualizadoCartaCobranca);
      lsVerso := untCombos.gf_Combo_SemDescricao(cboVersoCartaCobranca);

      { guarda a data inicial de pesquisa }
      if Trim(mskAtrasoInicialCartaCobranca.Text) <> VAZIO then
        ldDataInicial := StrToDate(DateToStr(Date-StrToInt(Trim(mskAtrasoFinalCartaCobranca.Text))))
      else
        ldDataInicial := StrToDate('01/01/1978');
      { guarda a data final de pesquisa }
      if Trim(mskAtrasoFinalCartaCobranca.Text) <> VAZIO then
        ldDataFinal := StrToDate(DateToStr(Date-StrToInt(Trim(mskAtrasoInicialCartaCobranca.Text))))
      else
        ldDataFinal := StrToDate(DateToStr(Date-1));

      { acha as parcelas em atraso }
      laParams[0].sNome := '@Tipo';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftString;
      laParams[0].vValor := lsProposta;
      laParams[1].sNome := '@NumIni';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftString;
      laParams[1].vValor := mskPropostaInicialCartaCobranca.Text;
      laParams[2].sNome := '@NumFim';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftString;
      laParams[2].vValor := mskPropostaFinalCartaCobranca.Text;
      laParams[3].sNome := '@DataIni';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftDateTime;
      laParams[3].vValor := ldDataInicial;
      laParams[4].sNome := '@DataFim';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftDateTime;
      laParams[4].vValor := ldDataFinal;
      laParams[5].sNome := '@TipoProp';
      laParams[5].ptTipo := ptInput;
      laParams[5].ftTipoDado := ftString;
      laParams[5].vValor := 'A';
      laParams[6].sNome := '@RelEtq';
      laParams[6].ptTipo := ptInput;
      laParams[6].ftTipoDado := ftString;
      laParams[6].vValor := 'R';
      laParams[7].sNome := '@Modelo';
      laParams[7].ptTipo := ptInput;
      laParams[7].ftTipoDado := ftString;
      laParams[7].vValor := lsModelo;
      laParams[8].sNome := '@Debito';
      laParams[8].ptTipo := ptInput;
      laParams[8].ftTipoDado := ftString;
      laParams[8].vValor := lsVerso;
      dmDados.CriarStored('Prop_Cobranca', laParams, 8);

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { guarda as mensagens }
      gaParm[0] := 'Cartas';
      gaParm[1] := 'Codigo = ''' + lsModelo + '''';
      gaParm[2] := 'Codigo';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[1] := dmDados.ExecutarSelect(msSql);
      if maConexao[1] = IOPTR_NOPOINTER then
        Exit;

      { enquento tiver linhas p/ incluir}
      for lwConta := 1 to High(maMensagem) do
        maMensagem[lwConta] := dmDados.ValorColuna(maConexao[1],'Linha'+ IntToStr(lwConta),miIndCol[lwConta - 1]);

      dmDados.FecharConexao(maConexao[1]);

      { propostas a imprimir }
      lsFiltro := VAZIO;
      if Trim(mskCEPInicialCartaCobranca.Text) <> '-' then
      begin
        lsFiltro := 'PROPONEN.CEP >= ''' + mskCEPInicialCartaCobranca.Text + '''';
      end;
      if Trim(mskCEPFinalCartaCobranca.Text) <> '-' then
      begin
        if lsFiltro <> VAZIO then
          lsFiltro := lsFiltro + ' And ';
        lsFiltro := lsFiltro + 'PROPONEN.CEP <= ''' + mskCEPFinalCartaCobranca.Text + '''';
      end;
      if lsOcupacao <> VAZIO then
      begin
        if lsFiltro <> VAZIO then
          lsFiltro := lsFiltro + ' And ';
        if lsOcupacao = 'N' then
          lsFiltro := lsFiltro + ' Not ';
        lsFiltro := lsFiltro + 'VGAVEPESS.NUM_PROP IS NULL';
      end;
      if lsFiltro = VAZIO then
        lsFiltro := '1=1';

      { atualizar os valores }
      if (lsValores = 'S') and (lsAtualizado = 'S') then
      begin
        for lwConta := 0 to High(miIndCol) do
          miIndCol[lwConta] := IOPTR_NOPOINTER;

//        gaParm[0] := 'TmpAtras';
        gaParm[0] := 'TmpCobranca';
        gaParm[1] := 'Num_Prop';
        msSql := dmDados.SqlVersao('NEC_0001', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;

        While not dmDados.Status(maConexao[0], IOSTATUS_EOF) do
        begin
          dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_EDIT);

          lsNumProp := dmDados.ValorColuna(maConexao[0], 'Num_Prop', miIndCol[0]);
          lsTipoProp := dmDados.ValorColuna(maConexao[0], 'Tipo_Prop', miIndCol[1]);
//          if (lsProposta = 'T') or (lsProposta = VAZIO) then
//          begin
            lsValorVenc := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'Valor', miIndCol[2]));
            lsDataVenc := FormatDateTime(MK_DATATELA, dmDados.ValorColuna(maConexao[0], 'Vencimento', miIndCol[3]));
            lsParcela := dmDados.ValorColuna(maConexao[0], 'Parcela', miIndCol[4]);
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Valor', miIndCol[2],
                                            untFuncoes.gf_Funcoes_ValorAtual(lsNumProp,
                                            lsTipoProp,
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela));
            { faz a atualizacao}
//            dmDados.gsRetorno := dmDados.Alterar(maConexao[0], 'TmpAtras', 'Num_Prop=''' + lsNumProp + ''' and Parc_Taxa=''' + lsParcela + '''');
            dmDados.gsRetorno := dmDados.Alterar(maConexao[0], 'TmpCobranca', 'Num_Prop=''' + lsNumProp + ''' and Tipo_Prop=''' + lsTipoProp + ''' And Parcela=''' + lsParcela + '''');
(*          end
          else if (lsProposta = 'L') or (lsProposta = VAZIO) then
          begin
            lsValorVenc := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'Valor', miIndCol[4]));
            lsDataVenc := FormatDateTime(MK_DATATELA, dmDados.ValorColuna(maConexao[0], 'Vencimento', miIndCol[5]));
            lsParcela := dmDados.ValorColuna(maConexao[0], 'Parcela', miIndCol[6]);
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Valor', miIndCol[4],
                                            untFuncoes.gf_Funcoes_ValorAtual(lsNumProp,
                                            'L',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela));
            { faz a atualizacao}
            dmDados.gsRetorno := dmDados.Alterar(maConexao[0], 'TmpCobranca', 'Num_Prop=''' + lsNumProp + ''' and Parcela=''' + lsParcela + '''');
          end
          else if (lsProposta = 'G') or (lsProposta = VAZIO) then
          begin
            lsValorVenc := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'Valor', miIndCol[7]));
            lsDataVenc := FormatDateTime(MK_DATATELA, dmDados.ValorColuna(maConexao[0], 'Vencimento', miIndCol[8]));
            lsParcela := dmDados.ValorColuna(maConexao[0], 'Parcela', miIndCol[9]);
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Valor', miIndCol[7],
                                            untFuncoes.gf_Funcoes_ValorAtual(lsNumProp,
                                            'G',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela));
            { faz a atualizacao}
            dmDados.gsRetorno := dmDados.Alterar(maConexao[0], 'TmpCobranca', 'Num_Prop=''' + lsNumProp + ''' and Parcela=''' + lsParcela + '''');
          end
          else if lsProposta = 'N' then
          begin
            lsValorVenc := FormatFloat(MK_VALOR, dmDados.ValorColuna(maConexao[0], 'Valor', miIndCol[10]));
            lsDataVenc := FormatDateTime(MK_DATATELA, dmDados.ValorColuna(maConexao[0], 'Vencimento', miIndCol[11]));
            lsParcela := dmDados.ValorColuna(maConexao[0], 'Parcela', miIndCol[12]);
            dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Valor', miIndCol[10],
                                            untFuncoes.gf_Funcoes_ValorAtual(lsNumProp,
                                            'N',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela));
            { faz a atualizacao}
            dmDados.gsRetorno := dmDados.Alterar(maConexao[0], 'TmpCobranca', 'Num_Prop=''' + lsNumProp + ''' and Parcela=''' + lsParcela + '''');
          end;
*)
          { proxima parcela }
          if (dmDados.Proximo(maConexao[0]) = IORET_EOF) or
             (dmDados.giStatus <> STATUS_OK) then
              break;
        end;
        dmDados.FecharConexao(maConexao[0]);
      end;

      gaParm[0] := lsFiltro;
      msSql := dmDados.SqlVersao('NEC_0021', gaParm);

      miConexaoAtualiza := dmDados.ExecutarSelect(msSql);
      if miConexaoAtualiza = IOPTR_NOPOINTER then
        Exit;

      { atualiza a barra de progresso }
      liConta := 0;
      prbCartaCobranca.Min := 0;
      prbCartaCobranca.Max := dmDados.gaSql[miConexaoAtualiza].RecordCount;

      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      lsNumProp := VAZIO;
      { enquento tiver contratos }
      While not dmDados.Status(miConexaoAtualiza, IOSTATUS_EOF) do
      begin

        lbOK := True;

        { se for o mesmo contrato pula }
        if lsNumProp <> dmDados.ValorColuna(miConexaoAtualiza,'Num_Prop',miIndCol[2]) then
        begin
          lsNumProp := dmDados.ValorColuna(miConexaoAtualiza,'Num_Prop',miIndCol[2]);

          { se for Parcelas }
          if (Trim(mskTaxaInicialCartaCobranca.Text) <> VAZIO) and (Trim(mskTaxaFinalCartaCobranca.Text) <> VAZIO) then
          begin
            gaParm[0] := 'Count(Num_Prop) Total, Num_Prop';
            gaParm[1] := 'TmpCobranca';
            gaParm[2] := 'Num_Prop = ''' + lsNumProp + '''';
            gaParm[3] := 'Num_Prop';
            gaParm[4] := 'Num_Prop';
            msSql := dmDados.SqlVersao('NEC_0006', gaParm);
            maConexao[0] := dmDados.ExecutarSelect(msSql);
            if maConexao[0] = IOPTR_NOPOINTER then
              Exit;

            liTotal := dmDados.ValorColuna(maConexao[0], 'Total', miIndCol[1]);

            dmDados.FecharConexao(maConexao[0]);

            if (liTotal >= StrToInt(Trim(mskTaxaInicialCartaCobranca.Text))) and
               (liTotal <= StrToInt(Trim(mskTaxaFinalCartaCobranca.Text))) then
              lbOK := True
            else
              lbOK := False;
(*          { se informou parcelas inicial }
          if Trim(mskTaxaInicialCartaCobranca.Text) <> VAZIO then
          begin
            if liTotal >= StrToInt(mskTaxaInicialCartaCobranca.Text) then
              lbOK := True
            else
              lbOK := False;
          end;
          { se informou parcelas final }
          if Trim(mskTaxaFinalCartaCobranca.Text) <> VAZIO then
          begin
            if liTotal <= StrToInt(mskTaxaFinalCartaCobranca.Text) then
              lbOK := True
            else
              lbOK := False;
          end;
*)
          end;

          if lbOK then
          begin
//          formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Carta' + lsModelo + '.rpt';
//            if lsVerso = 'S' then
//              formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Carta01.rpt'
//            else
              formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Carta02.rpt';

            //       formImprimir.rptRelatorio.Destination := crptToPrinter;
//            formImprimir.rptRelatorio.Destination := crptToWindow;
     //       formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;

            formImprimir.rptRelatorio.Connect.ServerName := gsDbConnect;
            formImprimir.rptRelatorio.Connect.UserID := gsDbUserName;
            formImprimir.rptRelatorio.Connect.Password := gsDbPassWord;
            { A formula}
           for lwConta := 1 to High(maMensagem) do
              formImprimir.rptRelatorio.Formulas.ByName('LINHA' + IntToStr(lwConta)).Formula.Text :=maMensagem[lwConta];
            formImprimir.rptRelatorio.Formulas.ByName('F_DEBITO').Formula.Text := lsDebito;
            formImprimir.rptRelatorio.Formulas.ByName('F_VALORES').Formula.Text := lsValores;
            formImprimir.rptRelatorio.Formulas.ByName('F_ATUALIZADO').Formula.Text := lsAtualizado;
//            formImprimir.rptRelatorio.Formulas[lwConta + 2] := VAZIO;
            formImprimir.rptRelatorio.Selection.Formula.Text := '{TmpCobranca.Num_Prop}=''' + dmDados.ValorColuna(miConexaoAtualiza,'Num_Prop',miIndCol[2]) + '''';
            { Assiona o Relatorio}
            formImprimir.rptRelatorio.Print;
          end;
        end;

        { grava a carta impressa }
        lsTipoProp := dmDados.ValorColuna(miConexaoAtualiza, 'Tipo_Prop', miIndCol[3]);
        lsParcela := dmDados.ValorColuna(miConexaoAtualiza, 'Parcela', miIndCol[4]);
//          aSqlParms[0] := psValor;
//          aSqlParms[1] := psChave;
//          lsSql := SqlVersao('GL_0001', aSqlParms);
        lsSql := 'UPDATE Receb SET Num_Carta = ''' + lsModelo + ''' WHERE Num_Prop = ''' + lsNumProp +
                                                                  ''' AND Tipo_Doc = ''' + lsTipoProp +
                                                                  ''' AND Num_Parc = ''' + lsParcela + '''';
        dmDados.gsRetorno := dmDados.ExecutarSQL(lsSql);

        { atualza barra de progresso }
        Inc(liConta);
        prbCartaCobranca.Position := liConta;

        { proxima pessoa}
        if (dmDados.Proximo(miConexaoAtualiza) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
      end;

      { Inicializa variavel de sucesso }
//      gbProcessoOK := True;
    end;

  except
    dmDados.ErroTratar('lp_Atualiza_Processo - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.btnGenericoClick
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.btnGenericoClick(Sender: TObject);
var
  lbtnButton: TButton;
  liConta: Integer;
  lwConta: Word;
  lcValor: Currency;
  lsValor: String;
  lbAcabou: Boolean;
  lsTipoDoc: string;
  lsTipo: string;
  lsVencimento: string;
  lsUsuario: string;
  lsDiretorio: string;
  lsExtensao: string;

  lsEnvio: string;
  lsBanco: string;
  lsNumBanco: string;
  lsAgencia: string;
  lsConta: string;
  lsDescontoU: string;
  lsDescontoS: string;
  lsDescontoB: string;
  lsTipoTaxa: string;
  lsRetorno: string;

  laParams: array[0..10] of TSParams;
  laParcelas: array[1..3] of Integer;
  laDataIni: array[1..3] of TDateTime;
  laDataFim: array[1..3] of TDateTime;
  laValores: array[1..3] of Currency;
begin

  lbtnButton := TButton(Sender);

 { limpa a formula do relatorio}
 for lwConta := 0 to 100 do
 //formImprimir.rptRelatorio.Formulas[lwConta] := VAZIO;
   formImprimir.rptRelatorio.Formulas.Clear;

  { botao recibo do quitar Parcelas}
  if lbtnButton.Name = BTN_RECIBO_QUITARPAG then
  begin
    btnGenericoClick(btnTotalQuitarPag);
    { Prepara para imprimir}
    if untDigito.gfC_Extenso(StrToFloat(dmDados.TirarSeparador(edtTotalQuitarPag.Text)), 'RE-AL;RE-AIS', 'CEN-TA-VO;CEN-TA-VOS', '100;100', lsValor) then
    begin
      gaParm[0] := 'TAB_USER';
      gaParm[1] := 'CODIGO = ''' + gsNomeUsuario + '''';
      gaParm[2] := 'CODIGO';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;
      miIndCol[0] := IOPTR_NOPOINTER;
      { se encontrou a op��o }
      if not dmDados.Status(maConexao[0], IOSTATUS_NOROWS) Then
        lsUsuario := dmDados.ValorColuna(maConexao[0], 'Descricao', miIndCol[0])
      else
        lsUsuario := VAZIO;

      dmDados.FecharConexao(maConexao[0]);
(*
    for liConta := 1 to grdParcelasQuitarPag.MaxRows do
    begin
      grdParcelasQuitarPag.Row := liConta;
      { ve se o parametro esta marcado}
      grdParcelasQuitarPag.Col := COL_QUIT_CHECK;
      if grdParcelasQuitarPag.Text = '1' then
      begin
        { coluna do valor}
        grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
        if grdParcelasQuitarPag.Text <> VAZIO then
        begin
          { soma o valor}
          lcValor := lcValor + StrToFloat(dmDados.TirarSeparador(grdParcelasQuitarPag.Text));
          { pega tipo documento }
          grdParcelasQuitarPag.Col := COL_QUIT_TIPODOC;
          if (grProcessos.sCod_Processo <> PROCESSO_SERVICO) then
          begin
            if grdParcelasQuitarPag.Text <> VAZIO then
              lsTipoDoc := Copy(grdParcelasQuitarPag.Text,1,1);
            if lsTipoDoc = 'L' then
              lsTipo := 'Lote: ';
            if lsTipoDoc = 'G' then
              lsTipo := 'Gaveta: ';
            if lsTipoDoc = 'N' then
              lsTipo := 'Nicho: ';
            if lsTipoDoc = 'R' then
              lsTipo := 'Remido: ';
            if lsTipoDoc = 'T' then
              lsTipo := 'Taxa: ';
            if lsTipoDoc = 'S' then
              lsTipo := 'Seguro: ';
          end
          else
            lsTipo := grdParcelasQuitarPag.Text;

          { pega data vencimento }
          grdParcelasQuitarPag.Col := COL_QUIT_DATAVENC;
          if grdParcelasQuitarPag.Text <> VAZIO then
            lsVencimento := grdParcelasQuitarPag.Text;
          if (grProcessos.sCod_Processo <> PROCESSO_SERVICO) then
            { coluna do Referencia}
            grdParcelasQuitarPag.Col := COL_QUIT_REFERENCIA
          else
            { coluna do valor}
            grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
          { guarda a referencia }
          if msReferencia = VAZIO then
            if (grProcessos.sCod_Processo <> PROCESSO_SERVICO) then
              msReferencia := lsTipo + Trim(grdParcelasQuitarPag.Text) + ' - ' + lsVencimento
            else
              msReferencia := lsTipo + ': ' + Trim(grdParcelasQuitarPag.Text)
          else
            if (grProcessos.sCod_Processo <> PROCESSO_SERVICO) then
              msReferencia := msReferencia + ', ' + lsTipo + Trim(grdParcelasQuitarPag.Text) + ' - ' + lsVencimento
            else
              msReferencia := msReferencia + ', ' + lsTipo + ': ' + Trim(grdParcelasQuitarPag.Text);
          { guarda os dizeres }
          if (grProcessos.sCod_Processo = PROCESSO_SERVICO) and
             (lsTipo = 'PLACA') then
            if frmRecebServico.edtNossoNumero.Text <> VAZIO then
              msReferencia := msReferencia + ' - ' + mf_Pegar_Dizeres(frmRecebServico.edtNossoNumero.Text);
        end;
      end;
    end;
  COL_QUIT_TIPODOC = 3;
  COL_QUIT_REFERENCIA = 4;
  COL_QUIT_PARCELA = 5;
  COL_QUIT_DATAVENC = 6;
  COL_QUIT_VALORVENC = 7;
  COL_QUIT_DATARECEB = 8;
  COL_QUIT_MULTA = 9;
  COL_QUIT_JUROS = 10;
  COL_QUIT_DESCONTO = 11;
  COL_QUIT_VALORRECEB = 12;
*)
      { gravar o recibo }
      laParams[0].sNome := '@Descricao';
      laParams[0].ptTipo := ptInput;
      laParams[0].ftTipoDado := ftString;
      laParams[0].vValor := dmDados.EstufaString(edtNomePropQuitarPag.Text, '''');
      laParams[1].sNome := '@NumProp';
      laParams[1].ptTipo := ptInput;
      laParams[1].ftTipoDado := ftString;
      laParams[1].vValor := edtCodigoPropQuitarPag.Text;
      laParams[2].sNome := '@VlrParc';
      laParams[2].ptTipo := ptInput;
      laParams[2].ftTipoDado := ftCurrency;
      laParams[2].vValor := StrToFloat(dmDados.TirarSeparador(edtTotalQuitarPag.Text));
      laParams[3].sNome := '@DataRecib';
      laParams[3].ptTipo := ptInput;
      laParams[3].ftTipoDado := ftDateTime;
      laParams[3].vValor := Date();
      laParams[4].sNome := '@Dinheiro';
      laParams[4].ptTipo := ptInput;
      laParams[4].ftTipoDado := ftString;
      if chkDinheiro.Checked then
        laParams[4].vValor := '1'
      else
        laParams[4].vValor := '0';
      laParams[5].sNome := '@NomeBanco';
      laParams[5].ptTipo := ptInput;
      laParams[5].ftTipoDado := ftString;
      laParams[5].vValor := edtCheques.Text;
      laParams[6].sNome := '@Motivo';
      laParams[6].ptTipo := ptInput;
      laParams[6].ftTipoDado := ftString;
      laParams[6].vValor := msReferencia;
      laParams[7].sNome := '@Usuario';
      laParams[7].ptTipo := ptInput;
      laParams[7].ftTipoDado := ftString;
      laParams[7].vValor := lsUsuario;
      laParams[8].sNome := '@NumRecibo';
      laParams[8].ptTipo := ptInput;
      laParams[8].ftTipoDado := ftString;
      laParams[8].vValor := edtReciboQuitarPag.Text;
      dmDados.CriarStored('GravaRecibo', laParams, 8);

      {  se for nicho }
      if (Copy(edtCodigoPropQuitarPag.Text,1,7) > '0100000') and
         (Copy(edtCodigoPropQuitarPag.Text,1,7) < '9999999') then
        formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Recibnic.rpt'
      else
        formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\Recibo.rpt';
//      formImprimir.rptRelatorio.Destination := crptToWindow;
//      formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
        formImprimir.rptRelatorio.Connect.ServerName := gsDbConnect;
        formImprimir.rptRelatorio.Connect.UserID := gsDbUserName;
        formImprimir.rptRelatorio.Connect.Password := gsDbPassWord;
//        formImprimir.rptRelatorio.Connect.DatabaseName := gsDbNome;
      { A formula}
      formImprimir.rptRelatorio.Formulas.ByName('VALOR').Formula.Text := edtTotalQuitarPag.Text;
      formImprimir.rptRelatorio.Formulas.ByName('EXTENSO').Formula.Text := lsValor;
      formImprimir.rptRelatorio.Formulas.ByName('REFERENCIA').Formula.Text := Copy(msReferencia,1,100);
      if Length(msReferencia) > 100 then
        formImprimir.rptRelatorio.Formulas.ByName('REFERENCIA1').Formula.Text := Copy(msReferencia,101,100)
      else
        formImprimir.rptRelatorio.Formulas.ByName('REFERENCIA1').Formula.Text := '';
      if chkDinheiro.Checked then
        formImprimir.rptRelatorio.Formulas.ByName('DINHEIRO').Formula.Text := 'X'
      else
        formImprimir.rptRelatorio.Formulas.ByName('DINHEIRO').Formula.Text := '';
      formImprimir.rptRelatorio.Formulas.ByName('CHEQUES').Formula.Text := Copy(edtCheques.Text,1,90);
      if Length(edtCheques.Text) > 90 then
        formImprimir.rptRelatorio.Formulas.ByName('CHEQUES1').Formula.Text := Copy(edtCheques.Text,91,90)
      else
        formImprimir.rptRelatorio.Formulas.ByName('CHEQUES1').Formula.Text := '';
      formImprimir.rptRelatorio.Formulas.ByName('USUARIO').Formula.Text := lsUsuario;
      formImprimir.rptRelatorio.Formulas.ByName('NOME').Formula.Text := dmDados.EstufaString(edtNomePropQuitarPag.Text, '''');
      formImprimir.rptRelatorio.Formulas.ByName('NUM_PROP').Formula.Text := edtCodigoPropQuitarPag.Text;
//      formImprimir.rptRelatorio.Formulas[10] := VAZIO;
      {  se for nicho }
      if (Copy(edtCodigoPropQuitarPag.Text,1,7) > '0100000') and
         (Copy(edtCodigoPropQuitarPag.Text,1,7) < '9999999') then
        formImprimir.rptRelatorio.Selection.Formula.Text := '{PRONICHO.NUM_PROP} = ''' + edtCodigoPropQuitarPag.Text + ''''
      else
        formImprimir.rptRelatorio.Selection.Formula.Text := '{PROPONEN.NUM_PROP} = ''' + edtCodigoPropQuitarPag.Text + '''';
      { Assiona o Relatorio}
      formImprimir.rptRelatorio.Show;
    end;
  end;


  { botao NF do quitar Parcelas}
  if lbtnButton.Name = BTN_NOTA_QUITARPAG then
  begin
    msReferencia := VAZIO;
    { calcula o total do valor recebido}
    for liConta := 1 to grdParcelasQuitarPag.MaxRows do
    begin
      grdParcelasQuitarPag.Row := liConta;
      { ve se o parametro esta marcado}
      grdParcelasQuitarPag.Col := COL_QUIT_CHECK;
      if grdParcelasQuitarPag.Text = '1' then
      begin
        { coluna do valor}
        grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
        if grdParcelasQuitarPag.Text <> VAZIO then
        begin
          { soma o valor}
          lcValor := lcValor + StrToFloat(dmDados.TirarSeparador(grdParcelasQuitarPag.Text));
          { pega tipo documento }
          grdParcelasQuitarPag.Col := COL_QUIT_TIPODOC;
          lsTipo := grdParcelasQuitarPag.Text;

          if lsTipo = 'VEL�RIO' then
          begin
            { coluna do valor}
            grdParcelasQuitarPag.Col := COL_QUIT_REFERENCIA;
            { guarda a referencia }
            msReferencia := edtCodigoPropQuitarPag.Text + COD_VELORIO + Trim(grdParcelasQuitarPag.Text);
            break;
          end;
        end;
      end;
    end;
    { coloca no edit}
    edtTotalQuitarPag.Text := FormatFloat(MK_VALOR, lcValor);

    if msReferencia <> VAZIO then
    begin
      formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\NotaFisc.rpt';
   //   formImprimir.rptRelatorio.Destination := crptToWindow;
  //    formImprimir.rptRelatorio.Connect := gsDbConnect;   //DB_CONNECT;
      formImprimir.rptRelatorio.Connect.ServerName := gsDbConnect;
      formImprimir.rptRelatorio.Connect.UserID := gsDbUserName;
      formImprimir.rptRelatorio.Connect.Password := gsDbPassWord;
      { A formula}
//      formImprimir.rptRelatorio.SelectionFormula := '{REC_SERV.CODIGO} = ''' + msReferencia + '''';
     formImprimir.rptRelatorio.Selection.Formula.Text := '{REC_SERV.CODIGO} = ''' + msReferencia + '''';
      { Assiona o Relatorio}
      formImprimir.rptRelatorio.Show;
    end;
  end;

  { botao Total do Quitar Parcelas}
  if lbtnButton.Name = BTN_TOTAL_QUITARPAG then
  begin
    lcValor := 0;
    msReferencia := VAZIO;

    { calcula o total do valor recebido}
    for liConta := 1 to grdParcelasQuitarPag.MaxRows do
    begin
      grdParcelasQuitarPag.Row := liConta;
      { ve se o parametro esta marcado}
      grdParcelasQuitarPag.Col := COL_QUIT_CHECK;
      if grdParcelasQuitarPag.Text = '1' then
      begin
        { coluna do valor}
        grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
        if grdParcelasQuitarPag.Text <> VAZIO then
        begin
          { soma o valor}
          lcValor := lcValor + StrToFloat(dmDados.TirarSeparador(grdParcelasQuitarPag.Text));
          { pega tipo documento }
          grdParcelasQuitarPag.Col := COL_QUIT_TIPODOC;
          if (grProcessos.sCod_Processo <> PROCESSO_SERVICO) then
          begin
            if grdParcelasQuitarPag.Text <> VAZIO then
              lsTipoDoc := Copy(grdParcelasQuitarPag.Text,1,1);
            if lsTipoDoc = 'L' then
              lsTipo := 'Lote: ';
            if lsTipoDoc = 'G' then
              lsTipo := 'Gaveta: ';
            if lsTipoDoc = 'N' then
              lsTipo := 'Nicho: ';
            if lsTipoDoc = 'R' then
              lsTipo := 'Remido: ';
            if lsTipoDoc = 'T' then
              lsTipo := 'Taxa: ';
            if lsTipoDoc = 'S' then
              lsTipo := 'Seguro: ';
          end
          else
            lsTipo := grdParcelasQuitarPag.Text;

          { pega data vencimento }
          grdParcelasQuitarPag.Col := COL_QUIT_DATAVENC;
          if grdParcelasQuitarPag.Text <> VAZIO then
            lsVencimento := grdParcelasQuitarPag.Text;
          if (grProcessos.sCod_Processo <> PROCESSO_SERVICO) then
            { coluna do Referencia}
            grdParcelasQuitarPag.Col := COL_QUIT_REFERENCIA
          else
            { coluna do valor}
            grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
          { guarda a referencia }
          if msReferencia = VAZIO then
            if (grProcessos.sCod_Processo <> PROCESSO_SERVICO) then
              msReferencia := lsTipo + Trim(grdParcelasQuitarPag.Text) + ' - ' + lsVencimento
            else
              msReferencia := lsTipo + ': ' + Trim(grdParcelasQuitarPag.Text)
          else
            if (grProcessos.sCod_Processo <> PROCESSO_SERVICO) then
              msReferencia := msReferencia + ', ' + lsTipo + Trim(grdParcelasQuitarPag.Text) + ' - ' + lsVencimento
            else
              msReferencia := msReferencia + ', ' + lsTipo + ': ' + Trim(grdParcelasQuitarPag.Text);
          { guarda os dizeres }
          if (grProcessos.sCod_Processo = PROCESSO_SERVICO) and
             (lsTipo = 'PLACA') then
            if frmRecebServico.edtNossoNumero.Text <> VAZIO then
              msReferencia := msReferencia + ' - ' + mf_Pegar_Dizeres(frmRecebServico.edtNossoNumero.Text);
        end;
      end;
    end;
    { coloca no edit}
    if Trim(edtReferencia.Text) = VAZIO then
      edtReferencia.Text := msReferencia
    else
      msReferencia := edtReferencia.Text;
    edtTotalQuitarPag.Text := FormatFloat(MK_VALOR, lcValor);
  end;

  { botao Total do Lancar Servico }
  if lbtnButton.Name = BTN_TOTAL_LANCSERV then
  begin
    lcValor := 0;
    msReferencia := VAZIO;

    { calcula o total do valor recebido}
    for liConta := 1 to grdParcelasLancarServico.MaxRows do
    begin
      grdParcelasLancarServico.Row := liConta;
      { ve se o parametro esta marcado}
      grdParcelasLancarServico.Col := COL_SERV_TIPO;
      if grdParcelasLancarServico.Text <> VAZIO then
      begin
        { coluna do valor}
        grdParcelasLancarServico.Col := COL_SERV_TOTAL;
        if grdParcelasLancarServico.Text <> VAZIO then
        begin
          { soma o valor}
          lcValor := lcValor + StrToFloat(dmDados.TirarSeparador(grdParcelasLancarServico.Text));
          { pega tipo documento }
          grdParcelasLancarServico.Col := COL_SERV_TIPO;
          lsTipoDoc := untCombos.gf_Combo_SpreadSemCodigo(grdParcelasLancarServico);
          lsTipo := lsTipoDoc + ': ';

          { pega data vencimento }
          grdParcelasLancarServico.Col := COL_QUIT_DATAVENC;
          if (mskDataLancarServico.Text <> VAZIO) and (mskDataLancarServico.Text <> VAZIO_DATA) then
            lsVencimento := mskDataLancarServico.Text;
          { coluna do valor}
          grdParcelasLancarServico.Col := COL_SERV_TOTAL;
          { guarda a referencia }
          if msReferencia = VAZIO then
            msReferencia := lsTipo + Trim(grdParcelasLancarServico.Text)
          else
            msReferencia := msReferencia + ', ' + lsTipo + Trim(grdParcelasLancarServico.Text);
        end;
      end;
    end;
    { coloca no edit}
    if Trim(edtReferenciaLancarServico.Text) = VAZIO then
      edtReferenciaLancarServico.Text := msReferencia
    else
      msReferencia := edtReferenciaLancarServico.Text;
    edtTotalLancarServico.Text := FormatFloat(MK_VALOR, lcValor);
  end;

  { botao Incluir Retorno do Banco}
  if lbtnButton.Name = BTN_INCLUIR_RETBANCO then
  begin
    lsDiretorio := dmDados.ConfigValor('DirRet_' + (untCombos.gf_Combo_SemDescricao(cboLayoutRetBanco)));
    lsExtensao := dmDados.ConfigValor('ExtRet_' + (untCombos.gf_Combo_SemDescricao(cboLayoutRetBanco)));
    {Prepara a caixa de dialogo}
    opdProcessos.Title := 'Localizar de Arquivos de Retorno do Banco';
    opdProcessos.Filter := 'Arquivos Retorno ('+lsExtensao+')|*'+lsExtensao;   //'Arquivos Retorno (.RET)|*.RET';
    opdProcessos.DefaultExt := lsExtensao;   //'.RET';
    opdProcessos.InitialDir := lsDiretorio;
    { se escolheu arquivo}
    if opdProcessos.Execute then
    begin
      {guarda na lista}
      for liConta := 0 to opdProcessos.Files.Count - 1 do
      begin
        { Ultima Linha}
        grdArquivosRetBanco.MaxRows := grdArquivosRetBanco.MaxRows + 1;
        grdArquivosRetBanco.Row := grdArquivosRetBanco.MaxRows;
        { Coluna do Arquivo}
        grdArquivosRetBanco.Col := 2;
        grdArquivosRetBanco.Text := opdProcessos.Files.Strings[liConta];
      end;
    end;
  end;

  { botao Excluir Retorno do Banco}
  if lbtnButton.Name = BTN_EXCLUIR_RETBANCO then
  begin
    { Coluna do Check}
    grdArquivosRetBanco.Col := 1;
    {seta como se nao chegou no final}
    lbAcabou := False;
    { Ate percorrer tosdas as linhas}
    while not lbAcabou do
    begin
      {seta como se chegou no final}
      lbAcabou := True;
      { Varre a lista procurando linhas marcadas}
      for liConta := 1 to grdArquivosRetBanco.MaxRows do
      begin
        {seta como se chegou no final}
        lbAcabou := True;
        { posiciona na linha Linha}
        grdArquivosRetBanco.Row := liConta;
        { se tiver marcado}
        if grdArquivosRetBanco.Text = '1' then
        begin
          grdArquivosRetBanco.Row := liConta;
          grdArquivosRetBanco.Col := 1;
          grdArquivosRetBanco.Action := SS_ACTION_DELETE_ROW;
          grdArquivosRetBanco.MaxRows := grdArquivosRetBanco.MaxRows - 1;
          {seta como se nao chegou no final}
          lbAcabou := False;
          Break;
        end;
      end;
    end;
  end;

  { botao Incluir Imagem}
  if lbtnButton.Name = BTN_INCLUIR_IMAGENS then
  begin
    lsDiretorio := dmDados.ConfigValor('DirImg_' + (untCombos.gf_Combo_SemDescricao(cboTipoImagens)));
    lsExtensao := dmDados.ConfigValor('ExtImg_' + (untCombos.gf_Combo_SemDescricao(cboTipoImagens)));
    {Prepara a caixa de dialogo}
    opdProcessos.Title := 'Localizar de Arquivos de Documentos';
    opdProcessos.Filter := 'Arquivos Documentos ('+lsExtensao+')|*'+lsExtensao;   //'Arquivos Retorno (.RET)|*.RET';
    opdProcessos.DefaultExt := lsExtensao;   //'.RET';
    opdProcessos.InitialDir := lsDiretorio;
    opdProcessos.Options := [ofAllowMultiSelect];
    { se escolheu arquivo}
    if opdProcessos.Execute then
    begin
      {guarda na lista}
      for liConta := 0 to opdProcessos.Files.Count - 1 do
      begin
        { Ultima Linha}
        grdIncluirImagens.MaxRows := grdIncluirImagens.MaxRows + 1;
        grdIncluirImagens.Row := grdIncluirImagens.MaxRows;
        { Coluna do Arquivo}
        grdIncluirImagens.Col := 2;
        grdIncluirImagens.Text := opdProcessos.Files.Strings[liConta];
        { pegar o numero da proposta }
        grdIncluirImagens.Col := 1;
        grdIncluirImagens.Text := Copy(ExtractFileName(opdProcessos.Files.Strings[liConta]),3,8);
      end;
      grdIncluirImagens.SortKey[1] := 2;
      grdIncluirImagens.SortKeyOrder[1] := SS_SORT_ORDER_ASCENDING;

      grdIncluirImagens.Row := 1;
      grdIncluirImagens.Col := 1;
      grdIncluirImagens.Row2 := grdIncluirImagens.MaxRows;
      grdIncluirImagens.Col2 := grdIncluirImagens.MaxCols;
      grdIncluirImagens.BlockMode := True;
      grdIncluirImagens.Action := SS_ACTION_SORT;
      grdIncluirImagens.BlockMode := False;
    end;
  end;

  { botao Alterar Vencimento }
  if lbtnButton.Name = BTN_ALTERAR_VENC then
  begin
    laParams[0].sNome := '@NumProp';
    laParams[0].ptTipo := ptInput;
    laParams[0].ftTipoDado := ftString;
    laParams[0].vValor := edtCodigoReceb.Text;
    laParams[1].sNome := '@TipoDoc';
    laParams[1].ptTipo := ptInput;
    laParams[1].ftTipoDado := ftString;
    laParams[1].vValor := edtDocumentoReceb.Text;
    laParams[2].sNome := '@NumParc';
    laParams[2].ptTipo := ptInput;
    laParams[2].ftTipoDado := ftString;
    laParams[2].vValor := edtParcelaReceb.Text + Copy(edtCodigoReceb.Text,10,2);
    laParams[3].sNome := '@DataVenc';
    laParams[3].ptTipo := ptInput;
    laParams[3].ftTipoDado := ftDateTime;
    laParams[3].vValor := StrToDate(mskDataVencimento.Text);
    dmDados.CriarStored('Altera_Vencimento', laParams, 3);
    { Inicializa variavel de sucesso }
    gbProcessoOK := True;
    {fecha a tela}
    formProcessos.Close;
  end;

  { botao Alterar Valor }
  if lbtnButton.Name = BTN_ALTERAR_VALOR then
  begin
    laParams[0].sNome := '@NumProp';
    laParams[0].ptTipo := ptInput;
    laParams[0].ftTipoDado := ftString;
    laParams[0].vValor := edtCodigoReceb.Text;
    laParams[1].sNome := '@TipoDoc';
    laParams[1].ptTipo := ptInput;
    laParams[1].ftTipoDado := ftString;
    laParams[1].vValor := edtDocumentoReceb.Text;
    laParams[2].sNome := '@NumParc';
    laParams[2].ptTipo := ptInput;
    laParams[2].ftTipoDado := ftString;
    laParams[2].vValor := edtParcelaReceb.Text + Copy(edtCodigoReceb.Text,10,2);
    laParams[3].sNome := '@ValorVenc';
    laParams[3].ptTipo := ptInput;
    laParams[3].ftTipoDado := ftCurrency;
    laParams[3].vValor := mskValorOriginal.Text;
    dmDados.CriarStored('Altera_Valor', laParams, 3);
    { Inicializa variavel de sucesso }
    gbProcessoOK := True;
    {fecha a tela}
    formProcessos.Close;
  end;

  { botao Limpar Envio de Boleto }
  if lbtnButton.Name = BTN_LIMPAR_ENVIOARQUI then
  begin
    dmDados.CriarStored('EnvioBanco_Limpar', laParams, -1);
  end;

  { botao Limpar Reenvio de Boleto }
  if lbtnButton.Name = BTN_LIMPAR_REENVIOARQUI then
  begin
    dmDados.CriarStored('ReenvioBanco_Limpar', laParams, -1);
  end;

  { botao Agrupar Envio de Boleto }
  if lbtnButton.Name = BTN_AGRUPAR_ENVIOARQUI then
  begin
    laParams[0].sNome := '@DataIni';
    laParams[0].ptTipo := ptInput;
    laParams[0].ftTipoDado := ftDateTime;
    laParams[0].vValor := StrToDate(DateToStr(dtpDataInicialEnvioArqui.Date));
    laParams[1].sNome := '@DataFim';
    laParams[1].ptTipo := ptInput;
    laParams[1].ftTipoDado := ftDateTime;
    laParams[1].vValor := StrToDate(DateToStr(dtpDataFinalEnvioArqui.Date));
    laParams[2].sNome := '@TipoProp';
    laParams[2].ptTipo := ptInput;
    laParams[2].ftTipoDado := ftString;
    laParams[2].vValor := untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui);
    laParams[3].sNome := '@NumTaxa';
    laParams[3].ptTipo := ptInput;
    laParams[3].ftTipoDado := ftString;
    { se for servico }
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
      laParams[3].vValor := untCombos.gf_Combo_SemDescricao(cboServicoEnvioArqui)
    else
      laParams[3].vValor := mskNumTaxaEnvioArqui.EditText;
    laParams[4].sNome := '@NumIni';
    laParams[4].ptTipo := ptInput;
    laParams[4].ftTipoDado := ftString;
    laParams[4].vValor := mskPropostaInicialEnvioArqui.Text;
    laParams[5].sNome := '@NumFim';
    laParams[5].ptTipo := ptInput;
    laParams[5].ftTipoDado := ftString;
    laParams[5].vValor := mskPropostaFinalEnvioArqui.Text;
    dmDados.CriarStored('EnvioBanco_Agrupar', laParams, 5);
  end;

  { botao Agrupar Reenvio de Boleto }
  if lbtnButton.Name = BTN_AGRUPAR_REENVIOARQUI then
  begin
    laParams[0].sNome := '@DataIni';
    laParams[0].ptTipo := ptInput;
    laParams[0].ftTipoDado := ftDateTime;
    laParams[0].vValor := StrToDate(DateToStr(dtpDataInicialReenvioArqui.Date));
    laParams[1].sNome := '@DataFim';
    laParams[1].ptTipo := ptInput;
    laParams[1].ftTipoDado := ftDateTime;
    laParams[1].vValor := StrToDate(DateToStr(dtpDataFinalReenvioArqui.Date));
    laParams[2].sNome := '@TipoProp';
    laParams[2].ptTipo := ptInput;
    laParams[2].ftTipoDado := ftString;
    laParams[2].vValor := untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui);
    laParams[3].sNome := '@NumTaxa';
    laParams[3].ptTipo := ptInput;
    laParams[3].ftTipoDado := ftString;
    { se for servico }
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
      laParams[3].vValor := untCombos.gf_Combo_SemDescricao(cboServicoReenvioArqui)
    else
      laParams[3].vValor := mskNumTaxaReenvioArqui.EditText;
    laParams[4].sNome := '@NumIni';
    laParams[4].ptTipo := ptInput;
    laParams[4].ftTipoDado := ftString;
    laParams[4].vValor := mskPropostaInicialReenvioArqui.Text;
    laParams[5].sNome := '@NumFim';
    laParams[5].ptTipo := ptInput;
    laParams[5].ftTipoDado := ftString;
    laParams[5].vValor := mskPropostaFinalReenvioArqui.Text;
    dmDados.CriarStored('ReenvioBanco_Agrupar', laParams, 5);
  end;

  { botao Listar Envio de Boleto }
  if lbtnButton.Name = BTN_LISTAR_ENVIOARQUI then
  begin
//    formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\prcAgrup.rpt';
    formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\prcAgrup.rpt';
//    formImprimir.rptRelatorio.Destination := crptToWindow;
//    formImprimir.rptRelatorio.Connect := gsDbConnect;
    formImprimir.rptRelatorio.Connect.ServerName := gsDbConnect;
    formImprimir.rptRelatorio.Connect.UserID := gsDbUserName;
    formImprimir.rptRelatorio.Connect.Password := gsDbPassWord;

    { A formula}
    //formImprimir.rptRelatorio.Formulas[0] := VAZIO;
    formImprimir.rptRelatorio.Formulas.Clear;
    formImprimir.rptRelatorio.Selection.Formula.Text := VAZIO;
    { Assiona o Relatorio}
//    formImprimir.rptRelatorio.Action := 1;
     formImprimir.rptRelatorio.Print;
  end;

  { botao Listar Reenvio de Boleto }
  if lbtnButton.Name = BTN_LISTAR_REENVIOARQUI then
  begin
   // formImprimir.rptRelatorio.ReportFileName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\prcAgrup.rpt';
    formImprimir.rptRelatorio.ReportName := ExtractFileDir(Application.ExeName) + '\RELATORIOS\prcAgrup.rpt';
   // formImprimir.rptRelatorio.Destination := crptToWindow;
   // formImprimir.rptRelatorio.Connect := gsDbConnect;
    formImprimir.rptRelatorio.Connect.ServerName := gsDbConnect;
    formImprimir.rptRelatorio.Connect.UserID := gsDbUserName;
    formImprimir.rptRelatorio.Connect.Password := gsDbPassWord;

    { A formula}
    //formImprimir.rptRelatorio.Formulas[0] := VAZIO;
    formImprimir.rptRelatorio.Formulas.Clear;
    formImprimir.rptRelatorio.Selection.Formula.Text := VAZIO;
    { Assiona o Relatorio}
//    formImprimir.rptRelatorio.Action := 1;
     formImprimir.rptRelatorio.Print;
  end;

  { botao Excluir Envio de Boleto }
  if lbtnButton.Name = BTN_EXCLUIR_ENVIOARQUI then
  begin
    laParams[0].sNome := '@DataIni';
    laParams[0].ptTipo := ptInput;
    laParams[0].ftTipoDado := ftDateTime;
    laParams[0].vValor := StrToDate(DateToStr(dtpDataInicialEnvioArqui.Date));
    laParams[1].sNome := '@DataFim';
    laParams[1].ptTipo := ptInput;
    laParams[1].ftTipoDado := ftDateTime;
    laParams[1].vValor := StrToDate(DateToStr(dtpDataFinalEnvioArqui.Date));
    laParams[2].sNome := '@TipoProp';
    laParams[2].ptTipo := ptInput;
    laParams[2].ftTipoDado := ftString;
    laParams[2].vValor := untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui);
    laParams[3].sNome := '@NumTaxa';
    laParams[3].ptTipo := ptInput;
    laParams[3].ftTipoDado := ftString;
    { se for servico }
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
      laParams[3].vValor := untCombos.gf_Combo_SemDescricao(cboServicoEnvioArqui)
    else
      laParams[3].vValor := mskNumTaxaEnvioArqui.EditText;
    laParams[4].sNome := '@NumIni';
    laParams[4].ptTipo := ptInput;
    laParams[4].ftTipoDado := ftString;
    laParams[4].vValor := mskPropostaInicialEnvioArqui.Text;
    laParams[5].sNome := '@NumFim';
    laParams[5].ptTipo := ptInput;
    laParams[5].ftTipoDado := ftString;
    laParams[5].vValor := mskPropostaFinalEnvioArqui.Text;
    dmDados.CriarStored('EnvioBanco_Excluir', laParams, 5);
  end;

  { botao Excluir Reenvio de Boleto }
  if lbtnButton.Name = BTN_EXCLUIR_REENVIOARQUI then
  begin
    laParams[0].sNome := '@DataIni';
    laParams[0].ptTipo := ptInput;
    laParams[0].ftTipoDado := ftDateTime;
    laParams[0].vValor := StrToDate(DateToStr(dtpDataInicialReenvioArqui.Date));
    laParams[1].sNome := '@DataFim';
    laParams[1].ptTipo := ptInput;
    laParams[1].ftTipoDado := ftDateTime;
    laParams[1].vValor := StrToDate(DateToStr(dtpDataFinalReenvioArqui.Date));
    laParams[2].sNome := '@TipoProp';
    laParams[2].ptTipo := ptInput;
    laParams[2].ftTipoDado := ftString;
    laParams[2].vValor := untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui);
    laParams[3].sNome := '@NumTaxa';
    laParams[3].ptTipo := ptInput;
    laParams[3].ftTipoDado := ftString;
    { se for servico }
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
      laParams[3].vValor := untCombos.gf_Combo_SemDescricao(cboServicoReenvioArqui)
    else
      laParams[3].vValor := mskNumTaxaReenvioArqui.EditText;
    laParams[4].sNome := '@NumIni';
    laParams[4].ptTipo := ptInput;
    laParams[4].ftTipoDado := ftString;
    laParams[4].vValor := mskPropostaInicialReenvioArqui.Text;
    laParams[5].sNome := '@NumFim';
    laParams[5].ptTipo := ptInput;
    laParams[5].ftTipoDado := ftString;
    laParams[5].vValor := mskPropostaFinalReenvioArqui.Text;
    dmDados.CriarStored('ReenvioBanco_Excluir', laParams, 5);
  end;

  { botao Gerar Envio de Boleto }
  if lbtnButton.Name = BTN_GERAR_ENVIOARQUI then
  begin
    lsEnvio := untCombos.gf_Combo_SemDescricao(cboTipoEnvioEnvioArqui);

    untGeraArquivo.gp_GeraArquivo_Inicializar;

    { se tiver layout }
    if cboLayoutEnvioArqui.Text <> VAZIO then
      glCodigoLayout := StrToInt(untCombos.gf_Combo_SemDescricao(cboLayoutEnvioArqui))
    else
      glCodigoLayout := 0;

    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    { guarda as mensagens }
    gaParm[0] := 'C_BANCO';
    gaParm[1] := 'Codigo = ''' + untCombos.gf_Combo_SemDescricao(cboBancoEnvioArqui) + '''';
    gaParm[2] := 'Codigo';
    msSql := dmDados.SqlVersao('NEC_0002', gaParm);
    maConexao[1] := dmDados.ExecutarSelect(msSql);
    if maConexao[1] = IOPTR_NOPOINTER then
      Exit;
    { enquento tiver pessoas p/ incluir}
    lsBanco := dmDados.ValorColuna(maConexao[1],'Tipo_Cobr',miIndCol[0]);
    lsNumBanco := dmDados.ValorColuna(maConexao[1],'Num_Banco',miIndCol[1]);
    lsAgencia := dmDados.ValorColuna(maConexao[1],'Agen_Banco',miIndCol[2]);
    lsConta := dmDados.ValorColuna(maConexao[1],'Conta_Banc',miIndCol[3]);
    dmDados.FecharConexao(maConexao[1]);

    laParams[0].sNome := '@DataIni';
    laParams[0].ptTipo := ptInput;
    laParams[0].ftTipoDado := ftDateTime;
    laParams[0].vValor := StrToDate(DateToStr(dtpDataInicialEnvioArqui.Date));
    laParams[1].sNome := '@DataFim';
    laParams[1].ptTipo := ptInput;
    laParams[1].ftTipoDado := ftDateTime;
    laParams[1].vValor := StrToDate(DateToStr(dtpDataFinalEnvioArqui.Date));
    laParams[2].sNome := '@TipoProp';
    laParams[2].ptTipo := ptInput;
    laParams[2].ftTipoDado := ftString;
    laParams[2].vValor := untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui);
    laParams[3].sNome := '@NumTaxa';
    laParams[3].ptTipo := ptInput;
    laParams[3].ftTipoDado := ftString;
    { se for servico }
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
      laParams[3].vValor := untCombos.gf_Combo_SemDescricao(cboServicoEnvioArqui)
    else
      laParams[3].vValor := mskNumTaxaEnvioArqui.EditText;
    laParams[4].sNome := '@NumIni';
    laParams[4].ptTipo := ptInput;
    laParams[4].ftTipoDado := ftString;
    laParams[4].vValor := mskPropostaInicialEnvioArqui.Text;
    laParams[5].sNome := '@NumFim';
    laParams[5].ptTipo := ptInput;
    laParams[5].ftTipoDado := ftString;
    laParams[5].vValor := mskPropostaFinalEnvioArqui.Text;
    dmDados.CriarStored('GeraNossoNumeroA' + lsBanco, laParams, 5);

    laParams[6].sNome := '@Emite';
    laParams[6].ptTipo := ptInput;
    laParams[6].ftTipoDado := ftInteger;
    laParams[6].vValor := 0;
    laParams[7].sNome := '@DataVenc';
    laParams[7].ptTipo := ptInput;
    laParams[7].ftTipoDado := ftString;
    if mskDataVencimentoEnvioArqui.Text <> VAZIO_DATA then
      laParams[7].vValor := (mskDataVencimentoEnvioArqui.Text)
    else
      laParams[7].vValor := VAZIO;
    laParams[8].sNome := '@DataInicial';
    laParams[8].ptTipo := ptInput;
    laParams[8].ftTipoDado := ftString;
    if mskDataInicialEnvioArqui.Text <> VAZIO_DATA then
      laParams[8].vValor := (mskDataInicialEnvioArqui.Text)
    else
      laParams[8].vValor := VAZIO;
    laParams[9].sNome := '@DataFinal';
    laParams[9].ptTipo := ptInput;
    laParams[9].ftTipoDado := ftString;
    if mskDataFinalEnvioArqui.Text <> VAZIO_DATA then
      laParams[9].vValor := (mskDataFinalEnvioArqui.Text)
    else
      laParams[9].vValor := VAZIO;
    { guarda em arquivo temporario }
    dmDados.CriarStored('GravaRemessaA', laParams, 9);

    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    { se for taxa}
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'T' then
    begin
      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { valor e desconto da taxa }
      gaParm[0] := 'TAB_TAXA';
      gaParm[1] := 'NUM_TAXA1 = ''' + mskNumTaxaEnvioArqui.Text + '''';
      gaParm[2] := 'CODIGO';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[1] := dmDados.ExecutarSelect(msSql);
      { se achou - Num da Taxa }
      if not dmDados.Status(maConexao[1], IOSTATUS_EOF) then
      begin
        lsDescontoU := dmDados.ValorColuna(maConexao[1], CAMPO_UNICA_DESCONTO, miIndCol[0]);
        lsDescontoS := dmDados.ValorColuna(maConexao[1], CAMPO_SEMESTRAL_DESCONTO, miIndCol[1]);
        lsDescontoB := dmDados.ValorColuna(maConexao[1], CAMPO_BIMESTRAL_DESCONTO, miIndCol[2]);
      end;
      { Fecha a Conexao}
      dmDados.FecharConexao(maConexao[1]);
    end;

    gaParm[0] := CTEDB_DATE_INI + FormatDateTime(MK_DATA, dtpDataInicialEnvioArqui.Date) + CTEDB_DATE_FIM;
    gaParm[1] := CTEDB_DATE_INI + FormatDateTime(MK_DATA, dtpDataFinalEnvioArqui.Date) + CTEDB_DATE_FIM;
    gaParm[2] := untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui);
(*    { se for taxa}
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'T' then
    begin
      { guarda o tipo da taxa }
      lsTipoTaxa := untCombos.gf_Combo_SemDescricao(cboTipoTaxaEnvioArqui);
      if mskPropostaInicialEnvioArqui.Text <> VAZIO then
      begin
        if Trim(mskCEPInicialEnvioArqui.Text) <> '-' then
        begin
          gaParm[3] := mskNumTaxaEnvioArqui.EditText;
          gaParm[4] := '0';
          gaParm[5] := mskPropostaInicialEnvioArqui.Text;
          gaParm[6] := mskPropostaFinalEnvioArqui.Text;
          gaParm[7] := mskCEPInicialEnvioArqui.Text;
          gaParm[8] := mskCEPFinalEnvioArqui.Text;
          if lsTipoTaxa = VAZIO then
            gaParm[9] := '(Tipo_Taxa Is Null)'
          else
            gaParm[9] := '(Tipo_Taxa = ''' + lsTipoTaxa + ''')';
          msSql := dmDados.SqlVersao('ARQ_0011A', gaParm);
        end
        else
        begin
          gaParm[3] := mskNumTaxaEnvioArqui.EditText;
          gaParm[4] := '0';
          gaParm[5] := mskPropostaInicialEnvioArqui.Text;
          gaParm[6] := mskPropostaFinalEnvioArqui.Text;
          if lsTipoTaxa = VAZIO then
            gaParm[7] := '(Tipo_Taxa Is Null)'
          else
            gaParm[7] := '(Tipo_Taxa = ''' + lsTipoTaxa + ''')';
          msSql := dmDados.SqlVersao('ARQ_0010A', gaParm);
        end;
      end
      else
      begin
        gaParm[3] := mskNumTaxaEnvioArqui.EditText;
        gaParm[4] := '0';
        if lsTipoTaxa = VAZIO then
          gaParm[5] := '(Tipo_Taxa Is Null)'
        else
          gaParm[5] := '(Tipo_Taxa = ''' + lsTipoTaxa + ''')';
        msSql := dmDados.SqlVersao('ARQ_0007A', gaParm);
      end;
    end
    else
    begin
*)
      gaParm[3] := mskPropostaInicialEnvioArqui.Text;
      gaParm[4] := mskPropostaFinalEnvioArqui.Text;
      gaParm[5] := '0';
      { se for seguro}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'S' then
        gaParm[6] := mskNumTaxaEnvioArqui.EditText;
      { se for servico}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
        gaParm[2] := untCombos.gf_Combo_SemDescricao(cboServicoEnvioArqui);
//      msSql := dmDados.SqlVersao('ARQ_008A' + untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui), gaParm);
      msSql := dmDados.SqlVersao('ARQ_008A', gaParm);
//    end;

    miConexaoAtualiza := dmDados.ExecutarSelect(msSql);
    if miConexaoAtualiza = IOPTR_NOPOINTER then
      Exit;

    { atualiza a barra de progresso }
    liConta := 0;
    prbEnvioBanco.Min := 0;
    prbEnvioBanco.Max := dmDados.gaSql[miConexaoAtualiza].RecordCount;

    { grava a primeira linha do arquivo}
    untGeraArquivo.EmiteLinha1(dmDados.ConfigValor('DirRem_' + IntToStr(glCodigoLayout)) + edtNomeArquivoEnvioArqui.Text);

    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    { se for seguro}
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'S' then
      lsRetorno := 'REC_SEGU'
    else
      { se for seguradora}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'I' then
        lsRetorno := 'SEGURO_PARC'
      else
        { se for servico}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaEnvioArqui) = 'V' then
          lsRetorno := 'REC_SERV'
        else
          lsRetorno := 'RECEB';

    { enquento tiver pessoas p/ incluir}
    While not dmDados.Status(miConexaoAtualiza, IOSTATUS_EOF) do
    begin
      { Grava no Arquivo texto}
      untGeraArquivo.EmiteLinhaX(dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[4]));

      { atualza barra de progresso }
      Inc(liConta);
      prbEnvioBanco.Position := liConta;

      { se for seguro}
      if dmDados.ValorColuna(miConexaoAtualiza, 'Tipo_Doc', miIndCol[3]) = 'S' then
        lsRetorno := 'REC_SEGU'
      else
        { se for seguradora}
        if dmDados.ValorColuna(miConexaoAtualiza, 'Tipo_Doc', miIndCol[3]) = 'I' then
          lsRetorno := 'SEGURO_PARC'
        else
          { se for servico}
          if dmDados.ValorColuna(miConexaoAtualiza, 'Tipo_Doc', miIndCol[3]) = 'V' then
            lsRetorno := 'REC_SERV'
          else
            lsRetorno := 'RECEB';

      gaParm[0] := lsRetorno;
      { se for seguradora}
      if dmDados.ValorColuna(miConexaoAtualiza, 'Tipo_Doc', miIndCol[3]) = 'I' then
      begin
        gaParm[1] := 'CodigoSeg+Num_Parc = ''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[4]) + '''';
        gaParm[2] := 'Codigo';
      end
      else
      begin
        gaParm[1] := 'Codigo = ''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[4]) + '''';
        gaParm[2] := 'Codigo';
      end;
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_EDIT);

      dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Emit_Carne', miIndCol[0], 1);
      dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Data_Envio', miIndCol[1], Date);
      dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Banco', miIndCol[2], lsNumBanco);

      { faz a atualizacao}
      dmDados.gsRetorno := dmDados.Alterar(maConexao[0], lsRetorno, VAZIO);

      dmDados.FecharConexao(maConexao[0]);

      { proxima pessoa}
      if (dmDados.Proximo(miConexaoAtualiza) = IORET_EOF) or
         (dmDados.giStatus <> STATUS_OK) then
          break;
    end;

    { emite a ultima linha do arquivo}
    untGeraArquivo.EmiteLinha9;

    untGeraArquivo.gp_GeraArquivo_Finalizar;

    edtNomeArquivoEnvioArqui.Text := VAZIO;
  end;

  { botao Gerar Reenvio de Boleto }
  if lbtnButton.Name = BTN_GERAR_REENVIOARQUI then
  begin
    lsEnvio := untCombos.gf_Combo_SemDescricao(cboTipoEnvioReenvioArqui);

    untGeraArquivo.gp_GeraArquivo_Inicializar;

    { se tiver layout }
    if cboLayoutReenvioArqui.Text <> VAZIO then
      glCodigoLayout := StrToInt(untCombos.gf_Combo_SemDescricao(cboLayoutReenvioArqui))
    else
      glCodigoLayout := 0;

    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    { guarda as mensagens }
    gaParm[0] := 'C_BANCO';
    gaParm[1] := 'Codigo = ''' + untCombos.gf_Combo_SemDescricao(cboBancoReenvioArqui) + '''';
    gaParm[2] := 'Codigo';
    msSql := dmDados.SqlVersao('NEC_0002', gaParm);
    maConexao[1] := dmDados.ExecutarSelect(msSql);
    if maConexao[1] = IOPTR_NOPOINTER then
      Exit;
    { enquento tiver pessoas p/ incluir}
    lsBanco := dmDados.ValorColuna(maConexao[1],'Tipo_Cobr',miIndCol[0]);
    lsNumBanco := dmDados.ValorColuna(maConexao[1],'Num_Banco',miIndCol[1]);
    lsAgencia := dmDados.ValorColuna(maConexao[1],'Agen_Banco',miIndCol[2]);
    lsConta := dmDados.ValorColuna(maConexao[1],'Conta_Banc',miIndCol[3]);
    dmDados.FecharConexao(maConexao[1]);

    laParams[0].sNome := '@DataIni';
    laParams[0].ptTipo := ptInput;
    laParams[0].ftTipoDado := ftDateTime;
    laParams[0].vValor := StrToDate(DateToStr(dtpDataInicialReenvioArqui.Date));
    laParams[1].sNome := '@DataFim';
    laParams[1].ptTipo := ptInput;
    laParams[1].ftTipoDado := ftDateTime;
    laParams[1].vValor := StrToDate(DateToStr(dtpDataFinalReenvioArqui.Date));
    laParams[2].sNome := '@TipoProp';
    laParams[2].ptTipo := ptInput;
    laParams[2].ftTipoDado := ftString;
    laParams[2].vValor := untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui);
    laParams[3].sNome := '@NumTaxa';
    laParams[3].ptTipo := ptInput;
    laParams[3].ftTipoDado := ftString;
    { se for servico }
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
      laParams[3].vValor := untCombos.gf_Combo_SemDescricao(cboServicoReenvioArqui)
    else
      laParams[3].vValor := mskNumTaxaReenvioArqui.EditText;
    laParams[4].sNome := '@NumIni';
    laParams[4].ptTipo := ptInput;
    laParams[4].ftTipoDado := ftString;
    laParams[4].vValor := mskPropostaInicialReenvioArqui.Text;
    laParams[5].sNome := '@NumFim';
    laParams[5].ptTipo := ptInput;
    laParams[5].ftTipoDado := ftString;
    laParams[5].vValor := mskPropostaFinalReenvioArqui.Text;
//    dmDados.CriarStored('GeraNossoNumeroA' + lsBanco, laParams, 5);

    laParams[6].sNome := '@Emite';
    laParams[6].ptTipo := ptInput;
    laParams[6].ftTipoDado := ftInteger;
    laParams[6].vValor := 0;
    laParams[7].sNome := '@DataVenc';
    laParams[7].ptTipo := ptInput;
    laParams[7].ftTipoDado := ftString;
    if mskDataVencimentoReenvioArqui.Text <> VAZIO_DATA then
      laParams[7].vValor := (mskDataVencimentoReenvioArqui.Text)
    else
      laParams[7].vValor := VAZIO;
    laParams[8].sNome := '@DataInicial';
    laParams[8].ptTipo := ptInput;
    laParams[8].ftTipoDado := ftString;
    if mskDataInicialReenvioArqui.Text <> VAZIO_DATA then
      laParams[8].vValor := (mskDataInicialReenvioArqui.Text)
    else
      laParams[8].vValor := VAZIO;
    laParams[9].sNome := '@DataFinal';
    laParams[9].ptTipo := ptInput;
    laParams[9].ftTipoDado := ftString;
    if mskDataFinalReenvioArqui.Text <> VAZIO_DATA then
      laParams[9].vValor := (mskDataFinalReenvioArqui.Text)
    else
      laParams[9].vValor := VAZIO;
    { guarda em arquivo temporario }
    dmDados.CriarStored('GravaRemessaA', laParams, 9);

    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    { se for taxa}
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'T' then
    begin
      for lwConta := 0 to High(miIndCol) do
        miIndCol[lwConta] := IOPTR_NOPOINTER;

      { valor e desconto da taxa }
      gaParm[0] := 'TAB_TAXA';
      gaParm[1] := 'NUM_TAXA1 = ''' + mskNumTaxaReenvioArqui.Text + '''';
      gaParm[2] := 'CODIGO';
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[1] := dmDados.ExecutarSelect(msSql);
      { se achou - Num da Taxa }
      if not dmDados.Status(maConexao[1], IOSTATUS_EOF) then
      begin
        lsDescontoU := dmDados.ValorColuna(maConexao[1], CAMPO_UNICA_DESCONTO, miIndCol[0]);
        lsDescontoS := dmDados.ValorColuna(maConexao[1], CAMPO_SEMESTRAL_DESCONTO, miIndCol[1]);
        lsDescontoB := dmDados.ValorColuna(maConexao[1], CAMPO_BIMESTRAL_DESCONTO, miIndCol[2]);
      end;
      { Fecha a Conexao}
      dmDados.FecharConexao(maConexao[1]);
    end;

    gaParm[0] := CTEDB_DATE_INI + FormatDateTime(MK_DATA, dtpDataInicialReenvioArqui.Date) + CTEDB_DATE_FIM;
    gaParm[1] := CTEDB_DATE_INI + FormatDateTime(MK_DATA, dtpDataFinalReenvioArqui.Date) + CTEDB_DATE_FIM;
    gaParm[2] := untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui);
    gaParm[3] := mskPropostaInicialReenvioArqui.Text;
    gaParm[4] := mskPropostaFinalReenvioArqui.Text;
    gaParm[5] := '0';
    { se for seguro}
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'S' then
      gaParm[6] := mskNumTaxaReenvioArqui.EditText;
    { se for servico}
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
      gaParm[2] := untCombos.gf_Combo_SemDescricao(cboServicoReenvioArqui);
    msSql := dmDados.SqlVersao('ARQ_008A', gaParm);

    miConexaoAtualiza := dmDados.ExecutarSelect(msSql);
    if miConexaoAtualiza = IOPTR_NOPOINTER then
      Exit;

    { atualiza a barra de progresso }
    liConta := 0;
    prbReenvioBanco.Min := 0;
    prbReenvioBanco.Max := dmDados.gaSql[miConexaoAtualiza].RecordCount;

    { grava a primeira linha do arquivo}
    untGeraArquivo.EmiteLinha1(dmDados.ConfigValor('DirRem_' + IntToStr(glCodigoLayout)) + edtNomeArquivoReenvioArqui.Text);

    for lwConta := 0 to High(miIndCol) do
      miIndCol[lwConta] := IOPTR_NOPOINTER;

    { se for seguro}
    if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'S' then
      lsRetorno := 'REC_SEGU'
    else
      { se for seguradora}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'I' then
        lsRetorno := 'SEGURO_PARC'
      else
        { se for servico}
        if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'V' then
          lsRetorno := 'REC_SERV'
        else
          lsRetorno := 'RECEB';

    { enquento tiver pessoas p/ incluir}
    While not dmDados.Status(miConexaoAtualiza, IOSTATUS_EOF) do
    begin
      { Grava no Arquivo texto}
      untGeraArquivo.EmiteLinhaX(dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]));

      { atualza barra de progresso }
      Inc(liConta);
      prbReenvioBanco.Position := liConta;

      gaParm[0] := lsRetorno;
      { se for seguradora}
      if untCombos.gf_Combo_SemDescricao(cboTipoParcelaReenvioArqui) = 'I' then
      begin
        gaParm[1] := 'CodigoSeg+Num_Parc = ''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
        gaParm[2] := 'Codigo';
      end
      else
      begin
        gaParm[1] := 'Codigo = ''' + dmDados.ValorColuna(miConexaoAtualiza,'Codigo',miIndCol[2]) + '''';
        gaParm[2] := 'Codigo';
      end;
      msSql := dmDados.SqlVersao('NEC_0002', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;

      dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_EDIT);

      dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Emit_Carne', miIndCol[0], 1);
      dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Data_Envio', miIndCol[1], Date);
      dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Banco', miIndCol[2], lsNumBanco);

      { faz a atualizacao}
      dmDados.gsRetorno := dmDados.Alterar(maConexao[0], lsRetorno, VAZIO);

      dmDados.FecharConexao(maConexao[0]);

      { proxima pessoa}
      if (dmDados.Proximo(miConexaoAtualiza) = IORET_EOF) or
         (dmDados.giStatus <> STATUS_OK) then
          break;
    end;

    { emite a ultima linha do arquivo}
    untGeraArquivo.EmiteLinha9;

    untGeraArquivo.gp_GeraArquivo_Finalizar;

    edtNomeArquivoReenvioArqui.Text := VAZIO;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.grdGenericoEditChange       Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.grdGenericoEditChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
  lsCodigo: string;
  lsTipoTaxa: string;
  liConta: Integer;
  lvValor: Variant;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
  lgrdGrid.Col := Col;
  if lgrdGrid.CellType = SS_CELL_TYPE_COMBOBOX then
    untCombos.gp_Combo_SpreadSeleciona(lgrdGrid, 0);

  if lgrdGrid.Name = 'grdParcelasQuitarPag' then
  begin
    if grdParcelasQuitarPag.Col = COL_QUIT_TIPOTAXA then
    begin
      grdParcelasQuitarPag.Col := COL_QUIT_TIPOTAXA;
      lsTipoTaxa := untCombos.gf_Combo_SpreadSemDescricao(grdParcelasQuitarPag);

      { se nao for a anual }
      if lsTipoTaxa <> 'U' then
      begin
        grdParcelasQuitarPag.Col := COL_QUIT_CODIGO;
        lsCodigo := grdParcelasQuitarPag.Text;

        { checar se num proposta existe }
        for liConta := 0 to High(miIndCol) do
          miIndCol[liConta] := IOPTR_NOPOINTER;

        gaParm[0] := 'Receb';
        gaParm[1] := 'Codigo = ''' + lsCodigo + '''';
        gaParm[2] := 'Codigo';
        msSql := dmDados.SqlVersao('NEC_0002', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;
        if not dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
        begin
          grdParcelasQuitarPag.Col := COL_QUIT_VALORVENC;
          if lsTipoTaxa = 'S' then
            lvValor := dmDados.ValorColuna(maConexao[0], 'Vlr_Parc1', miIndCol[0])
          else
            lvValor := dmDados.ValorColuna(maConexao[0], 'Vlr_Parc2', miIndCol[0]);
          grdParcelasQuitarPag.Text := FormatFloat(MK_VALOR, lvValor);
          grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
          if grdParcelasQuitarPag.Text <> VAZIO then
            grdParcelasQuitarPag.Text := FormatFloat(MK_VALOR, lvValor);
        end;
        dmDados.FecharConexao(maConexao[0]);
      end;
    end;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.grdParcelasQuitarPagButtonClicked       Selecionou parcela
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.grdGenericoButtonClicked(Sender: TObject;
  Col, Row: Integer; ButtonDown: Smallint);
var
  lgrdGrid: TvaSpread;

  lsValorReceb: string;
  lsTipoDoc: string;
  lsValorVenc: string;
  lsMulta: string;
  lsJuros: string;
  lsVencimento: string;
  lsNumeroParcela: string;
  ldValorParc: Double;
begin

  lgrdGrid := TvaSpread(Sender);

  if lgrdGrid.Name = 'grdParcelasQuitarPag' then
  begin
    grdParcelasQuitarPag.Row := Row;

    grdParcelasQuitarPag.Col := COL_QUIT_CHECK;
    if grdParcelasQuitarPag.Text = '1' then
    begin
      { quitacao ou recibo parcial }
      if (grProcessos.sCod_Processo = PROCESSO_QUITAR) or (grProcessos.sCod_Processo = PROCESSO_SERVICO) or (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
      begin
        grdParcelasQuitarPag.Col := COL_QUIT_DATARECEB;
        grdParcelasQuitarPag.Text := FormatDateTime(MK_DATATELA, Date);
        grdParcelasQuitarPag.Col := COL_QUIT_DATACREDITO;
        grdParcelasQuitarPag.Text := FormatDateTime(MK_DATATELA, Date);

        { pega tipo documento }
        grdParcelasQuitarPag.Col := COL_QUIT_TIPODOC;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsTipoDoc := Copy(grdParcelasQuitarPag.Text,1,1);
        { pega valor vencimento }
        grdParcelasQuitarPag.Col := COL_QUIT_VALORVENC;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsValorVenc := grdParcelasQuitarPag.Text;
        { pega valor multa }
        grdParcelasQuitarPag.Col := COL_QUIT_MULTA;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsMulta := grdParcelasQuitarPag.Text;
        { pega valor juros }
        grdParcelasQuitarPag.Col := COL_QUIT_JUROS;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsJuros := grdParcelasQuitarPag.Text;
        { pega data vencimento }
        grdParcelasQuitarPag.Col := COL_QUIT_DATAVENC;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsVencimento := grdParcelasQuitarPag.Text;
        { pega no parcela }
        grdParcelasQuitarPag.Col := COL_QUIT_REFERENCIA;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsNumeroParcela := grdParcelasQuitarPag.Text;

        if (grProcessos.sCod_Processo = PROCESSO_QUITAR) then
        begin
          { valor parcial }
          ldValorParc := untFuncoes.gf_Funcoes_SaldoParc(edtCodigoPropQuitarPag.Text,
                                                     lsTipoDoc,
                                                     lsNumeroParcela);
//        lsValorReceb := grdParcelasQuitarPag.Text;
          { tolerancia de cinco dias }
          if StrToDate(lsVencimento) < (Date - DIAS_ATRASO) then
            lsValorReceb := FormatFloat(MK_VALOR,
                          StrToFloat(dmDados.TirarSeparador(untFuncoes.gf_Funcoes_ValorAtual(edtCodigoPropQuitarPag.Text,
                                     lsTipoDoc,
                                     lsValorVenc,
                                     lsMulta,
                                     lsJuros,
                                     lsVencimento,
                                     lsNumeroParcela))))  // - ldValorParc);
          else
            lsValorReceb := lsValorVenc;
        end
        else
          lsValorReceb := lsValorVenc;

        grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
        grdParcelasQuitarPag.Text := lsValorReceb;

        grdParcelasQuitarPag.Col := COL_QUIT_TIPOBAIXA;
        untCombos.gp_Combo_SpreadPosiciona(grdParcelasQuitarPag, 'C');

        grdParcelasQuitarPag.Col := COL_QUIT_TIPORECEB;
        untCombos.gp_Combo_SpreadPosiciona(grdParcelasQuitarPag, '1');
      end
      else
      begin
        grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
        lsValorReceb := grdParcelasQuitarPag.Text;
      end;
      edtTotalQuitarPag.Text := FormatFloat(MK_VALOR,StrToFloat(dmDados.TirarSeparador(edtTotalQuitarPag.Text)) + StrToFloat(dmDados.TirarSeparador(lsValorReceb)));
    end
    else
    begin
      { quitacao ou recibo parcial }
      if (grProcessos.sCod_Processo = PROCESSO_QUITAR) or (grProcessos.sCod_Processo = PROCESSO_SERVICO) or (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
      begin
        grdParcelasQuitarPag.Col := COL_QUIT_DATARECEB;
        grdParcelasQuitarPag.Text := VAZIO;
        grdParcelasQuitarPag.Col := COL_QUIT_DATACREDITO;
        grdParcelasQuitarPag.Text := VAZIO;

        grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
        lsValorReceb := grdParcelasQuitarPag.Text;
        grdParcelasQuitarPag.Text := VAZIO;

        grdParcelasQuitarPag.Col := COL_QUIT_TIPOBAIXA;
        grdParcelasQuitarPag.Text := VAZIO;

        grdParcelasQuitarPag.Col := COL_QUIT_TIPORECEB;
        grdParcelasQuitarPag.Text := VAZIO;
      end
      else
      begin
        grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
        lsValorReceb := grdParcelasQuitarPag.Text;
      end;
      edtTotalQuitarPag.Text := FormatFloat(MK_VALOR,StrToFloat(dmDados.TirarSeparador(edtTotalQuitarPag.Text)) - StrToFloat(dmDados.TirarSeparador(lsValorReceb)));
    end;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.grdParcelasQuitarPagLeaveCell       Valor a ser cobrado
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.grdGenericoLeaveCell(Sender: TObject;
  Col, Row, NewCol, NewRow: Integer; var Cancel: WordBool);
var
  lgrdGrid: TvaSpread;

  liConta: Integer;
  lsTipoDoc: string;
  lsValorVenc: string;
  ldDesconto: Double;
  ldValorReceb: Double;
  ldValorParc: Double;
  lsMulta: string;
  lsJuros: string;
  lsVencimento: string;
  lsNumeroParcela: string;
begin

  lgrdGrid := TvaSpread(Sender);

  if lgrdGrid.Name = 'grdParcelasQuitarPag' then
  begin
    { valor baixado }
    if (Col = COL_QUIT_VALORRECEB) or (Col = COL_QUIT_DESCONTO) then
    begin
      { quitacao ou recibo parcial }
      if (grProcessos.sCod_Processo = PROCESSO_QUITAR) or (grProcessos.sCod_Processo = PROCESSO_SERVICO) or (grProcessos.sCod_Processo = PROCESSO_SEGURO) then
      begin
        grdParcelasQuitarPag.Row := Row;
        { pega valor vencimento }
        grdParcelasQuitarPag.Col := COL_QUIT_VALORVENC;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsValorVenc := grdParcelasQuitarPag.Text;
        { pega valor do desconto }
        grdParcelasQuitarPag.Col := COL_QUIT_DESCONTO;
        if grdParcelasQuitarPag.Text <> VAZIO then
          ldDesconto := StrToFloat(dmDados.TirarSeparador(grdParcelasQuitarPag.Text))
        else
          ldDesconto := 0;
        { pega data vencimento }
        grdParcelasQuitarPag.Col := COL_QUIT_DATAVENC;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsVencimento := grdParcelasQuitarPag.Text;
        { quitacao }
        if (grProcessos.sCod_Processo = PROCESSO_QUITAR) then
        begin
          { pega valor multa }
          grdParcelasQuitarPag.Col := COL_QUIT_MULTA;
          if grdParcelasQuitarPag.Text <> VAZIO then
            lsMulta := grdParcelasQuitarPag.Text;
          { pega valor juros }
          grdParcelasQuitarPag.Col := COL_QUIT_JUROS;
          if grdParcelasQuitarPag.Text <> VAZIO then
            lsJuros := grdParcelasQuitarPag.Text;
        end;
        { pega tipo documento }
        grdParcelasQuitarPag.Col := COL_QUIT_TIPODOC;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsTipoDoc := Copy(grdParcelasQuitarPag.Text,1,1);
        { pega no parcela }
        grdParcelasQuitarPag.Col := COL_QUIT_REFERENCIA;
        if grdParcelasQuitarPag.Text <> VAZIO then
          lsNumeroParcela := grdParcelasQuitarPag.Text;
        { pega valor recebido }
        grdParcelasQuitarPag.Col := COL_QUIT_VALORRECEB;
        if grdParcelasQuitarPag.Text <> VAZIO then
          ldValorReceb := StrToFloat(dmDados.TirarSeparador(grdParcelasQuitarPag.Text))
        else
          ldValorReceb := 0;

        { quitacao }
        if (grProcessos.sCod_Processo = PROCESSO_QUITAR) then
        begin
          { valor parcial }
          ldValorParc := untFuncoes.gf_Funcoes_SaldoParc(edtCodigoPropQuitarPag.Text,
                                                     lsTipoDoc,
                                                     lsNumeroParcela);
          if EXIGE_DESCONTO = 'S' then
          begin
            if grdParcelasQuitarPag.Text <> VAZIO then
            begin
              { tolerancia de cinco dias }
              if StrToDate(lsVencimento) < (Date - DIAS_ATRASO) then
              begin
                if StrToFloat(dmDados.TirarSeparador(untFuncoes.gf_Funcoes_ValorAtual(edtCodigoPropQuitarPag.Text,
                              lsTipoDoc,
                              lsValorVenc,
                              lsMulta,
                              lsJuros,
                              lsVencimento,
                              lsNumeroParcela))) - ldDesconto (*- ldValorParc*) - ldValorReceb > 0.009 then
                begin
                  MessageDlg('Valor do Recebimento Incorreto !!!',
                             mtWarning, [mbOK], 0);
                  Cancel := True;
                end;
              end
              else
              begin
                if StrToFloat(dmDados.TirarSeparador(lsValorVenc)) - ldDesconto (*- ldValorParc*) - ldValorReceb > 0.009 then
                begin
                  MessageDlg('Valor do Recebimento Incorreto !!!',
                             mtWarning, [mbOK], 0);
                    Cancel := True;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
  if lgrdGrid.Name = 'grdParcelasLancarServico' then
  begin
    { tipo do servico }
    if (Col = COL_SERV_TIPO) then
    begin
      grdParcelasLancarServico.Row := Row;
      { pega o tipo }
      grdParcelasLancarServico.Col := COL_SERV_TIPO;
      if grdParcelasLancarServico.Text <> VAZIO then
        lsTipoDoc := untCombos.gf_Combo_SpreadSemDescricao(grdParcelasLancarServico);

      { pegar o valor do servico }
      for liConta := 0 to High(miIndCol) do
        miIndCol[liConta] := IOPTR_NOPOINTER;

      if lsTipoDoc <> VAZIO then
      begin
        gaParm[0] := 'VALOR_NORMAL';
        gaParm[1] := 'TAB_SERV_VALOR';
        gaParm[2] := 'CODIGOSERV = ''' + lsTipoDoc + ''' AND CODIGO = (SELECT MAX(CODIGO) FROM TAB_SERV_VALOR WHERE CODIGOSERV = ''' + lsTipoDoc + ''')';
        msSql := dmDados.SqlVersao('NEC_0000', gaParm);
        maConexao[0] := dmDados.ExecutarSelect(msSql);
        if maConexao[0] = IOPTR_NOPOINTER then
          Exit;
        { achou }
        if not dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
        begin
          ldValorReceb := dmDados.ValorColuna(maConexao[0],'VALOR_NORMAL',miIndCol[0]);
        end;
        dmDados.FecharConexao(maConexao[0]);
        { define quantidade }
        grdParcelasLancarServico.Col := COL_SERV_QTDE;
        if grdParcelasLancarServico.Text = VAZIO then
          grdParcelasLancarServico.Text := '1';
        { define valor do servico }
        grdParcelasLancarServico.Col := COL_SERV_VALOR;
        if grdParcelasLancarServico.Text = VAZIO then
          grdParcelasLancarServico.Text := FormatFloat(MK_VALOR, ldValorReceb);
      end;
    end;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.mp_Incluir_Imagem       Incluir imagens no cadastro
 *
 *-----------------------------------------------------------------------------}
function TformProcessos.mf_Incluir_Imagem(psNumProp: string;
                                          psTipo: string;
                                          psCaminho: string): string;
var
  liConta: Integer;
  lvCodigo: Variant;
begin
  Result := VAZIO;
  try
    { checar se num proposta esta correto }
//    if not untDigito.gf_Digito_ChecarProposta(psNumProp) then
//    begin
//      Result := 'D�gito Inv�lido!';
//      Exit;
//    end;

    { checar se num proposta existe }
    for liConta := 0 to High(miIndCol) do
      miIndCol[liConta] := IOPTR_NOPOINTER;

    gaParm[0] := 'PropLote';
    gaParm[1] := 'Num_Prop = ''' + psNumProp + '''';
    gaParm[2] := 'Num_Prop';
    msSql := dmDados.SqlVersao('NEC_0002', gaParm);
    maConexao[0] := dmDados.ExecutarSelect(msSql);
    if maConexao[0] = IOPTR_NOPOINTER then
      Exit;
    if dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
    begin
      dmDados.FecharConexao(maConexao[0]);
      Result := 'Proposta Inexistente!';
      Exit;
    end;
    dmDados.FecharConexao(maConexao[0]);

    for liConta := 0 to High(miIndCol) do
      miIndCol[liConta] := IOPTR_NOPOINTER;

    gaParm[0] := 'Contratos';
    gaParm[1] := 'Num_Prop = ''' + psNumProp + '''';
    gaParm[2] := 'Num_Prop, Sequencia';
    msSql := dmDados.SqlVersao('NEC_0002', gaParm);
    maConexao[0] := dmDados.ExecutarSelect(msSql);
    if maConexao[0] = IOPTR_NOPOINTER then
      Exit;

    { checar se a imagem ja nao esta incluida }
    if dmDados.Posiciona(maConexao[0], 'Arquivo', [ExtractFileName(psCaminho)]) = IORET_OK then
    begin
      dmDados.FecharConexao(maConexao[0]);
      Result := 'Documento j� Cadastrado!';
      Exit;
    end;

    dmDados.Ultimo(maConexao[0]);

    lvCodigo := dmDados.ValorColuna(maConexao[0], 'Sequencia', miIndCol[0]);
    { se nao tiver codigo}
    if VarAsType(lvCodigo, VarString) = IOVAL_NOVALUE then
      lvCodigo := 1
    else
      Inc(lvCodigo);

    dmDados.gsRetorno := dmDados.AtivaModo(maConexao[0],IOMODE_INSERT);

    dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Sequencia', miIndCol[0], lvCodigo);
    dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Num_Prop', miIndCol[1], psNumProp);
    dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Arquivo', miIndCol[2], ExtractFileName(psCaminho));
    dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Caminho', miIndCol[3], psCaminho);
    dmDados.gsRetorno := dmDados.AlterarColuna(maConexao[0], 'Tipo', miIndCol[4], psTipo);

    { faz a atualizacao}
    dmDados.gsRetorno := dmDados.Incluir(maConexao[0], 'Contratos');

    dmDados.FecharConexao(maConexao[0]);

  except
    dmDados.ErroTratar('mp_Incluir_Imagem - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.mf_Pegar_Dizeres       Pegar dizeres das placas
 *
 *-----------------------------------------------------------------------------}
function TformProcessos.mf_Pegar_Dizeres(psCodigo: string): string;
var
  liConta: Integer;
  lvCodigo: Variant;
begin
  Result := VAZIO;
  try
    { checar se num proposta existe }
    for liConta := 0 to High(miIndCol) do
      miIndCol[liConta] := IOPTR_NOPOINTER;

    gaParm[0] := 'Placas';
    gaParm[1] := 'Codigo = ''' + psCodigo + '''';
    gaParm[2] := 'Codigo';
    msSql := dmDados.SqlVersao('NEC_0002', gaParm);
    maConexao[0] := dmDados.ExecutarSelect(msSql);
    if maConexao[0] = IOPTR_NOPOINTER then
      Exit;
    if not dmDados.Status(maConexao[0], IOSTATUS_NOROWS) then
    begin
      Result := dmDados.ValorColuna(maConexao[0], 'Dizeres', miIndCol[0]) + ' ' + dmDados.ValorColuna(maConexao[0], 'Nasc_Falec', miIndCol[1]);
    end;
    dmDados.FecharConexao(maConexao[0]);

  except
    dmDados.ErroTratar('mf_Pegar_Dizeres - uniProcessos.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.edtBaixaBarrasKeyDown       Simula o TAB no ENTER
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.edtBaixaBarrasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  if Key = VK_RETURN then
  begin
    Key := 0;
    Perform(WM_NEXTDLGCTL, 0, 0);
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.mskNumPropostaExit       preenche com zeros
 *
 *-----------------------------------------------------------------------------}
procedure TformProcessos.mskNumPropostaExit(Sender: TObject);
var
  lmskEdit:   TMaskEdit;
begin

  lmskEdit := TMaskEdit(Sender);

  if Sender is TMaskEdit then
  begin
    lmskEdit.Text := untFuncoes.gf_Zero_Esquerda(lmskEdit.Text, 8);
  end;

end;















end.
