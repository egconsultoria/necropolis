unit uniPlacas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ComCtrls, ExtCtrls, Variants,
  uniConst, OleCtrls, FPSpread_TLB;

const
  EDT_CODIGO: string = 'mskCodigo';

type
  TfrmPlacas = class(TForm)
    pnlFundo: TPanel;
    grpPlacas: TGroupBox;
    lblCodigo: TLabel;
    mskProposta: TMaskEdit;
    grpProposta: TGroupBox;
    grpProduto: TGroupBox;
    Label32: TLabel;
    cboProduto: TComboBox;
    PntComunicador: TPaintBox;
    Label4: TLabel;
    edtSolicitante: TEdit;
    Label13: TLabel;
    mskTelefone: TMaskEdit;
    Label2: TLabel;
    mskCodigo: TMaskEdit;
    Label3: TLabel;
    edtDescricao: TEdit;
    Label7: TLabel;
    mskPedido: TMaskEdit;
    Label8: TLabel;
    edtDizeres: TEdit;
    Label9: TLabel;
    edtNascFalec: TEdit;
    Label12: TLabel;
    Label14: TLabel;
    mskSetor: TMaskEdit;
    Label15: TLabel;
    mskLote: TMaskEdit;
    Label16: TLabel;
    mskDataSolicitacao: TMaskEdit;
    Label17: TLabel;
    mskValor: TMaskEdit;
    Label5: TLabel;
    mskPrazo: TMaskEdit;
    Label11: TLabel;
    mskValorParc: TMaskEdit;
    Label18: TLabel;
    mskNumParc: TMaskEdit;
    mskQtdLetras: TMaskEdit;
    Label19: TLabel;
    grpCheques: TGroupBox;
    Label36: TLabel;
    Label37: TLabel;
    edtCheques: TEdit;
    edtReferencia: TEdit;
    sprCheques: TvaSpread;
    grpDatas: TGroupBox;
    Label10: TLabel;
    lblDataProposta: TLabel;
    Label1: TLabel;
    Label6: TLabel;
    mskDataFundicao: TMaskEdit;
    mskDataRecebimento: TMaskEdit;
    mskDataColocacao: TMaskEdit;
    edtUsuario: TEdit;
    mskValorDinheiro: TMaskEdit;
    Label125: TLabel;
    Label20: TLabel;
    mskValorTotal: TMaskEdit;
    cboQuadra: TComboBox;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure cboComboExit(Sender: TObject);
    procedure cboComboKeyPress(Sender: TObject; var Key: Char);
    procedure edtTextoChange(Sender: TObject);
    procedure edtTextoEnter(Sender: TObject);
    procedure edtTextoExit(Sender: TObject);
    procedure sprGridChange(Sender: TObject; Col, Row: Integer);
    procedure sprGridEditChange(Sender: TObject; Col, Row: Integer);
    procedure chkCheckClick(Sender: TObject);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;

    procedure mpMostra_Valor;
  public
    { Public declarations }
  end;

var
  frmPlacas: TfrmPlacas;

implementation

uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos, uniFuncoes,
     uniDigito;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.FormClose(Sender: TObject; var Action: TCloseAction);
var
  liConta:  Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - uniPlacas',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;
    gfrmTV := nil;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniPlacas');
  end;

  // destroi a janela
  Action := caFree;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, TForm(Sender));

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.FormResize(Sender: TObject);
begin
  if TForm(Sender).WindowState <> wsMinimized Then
  begin
    if (TForm(Sender).Width < pnlFundo.Width + 39) and (TForm(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TForm(Sender).Width := pnlFundo.Width + 39;

    TForm(Sender).Refresh;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
//    mrTelaVar.P.Titulo_Tela := 'Proposta de Lote';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
    untEdit.gp_Edit_EstufaCombos(mrTelaVar.P.Sigla_Tabela, TForm(Sender));

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);

//    mpMostra_Valor;

    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniPlacas');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 3);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
      if lcboCombo.Text <> VAZIO then
      begin
        {' altera��o efetuada}
        untEdit.gp_Edit_Alterado(True, mrTelaVar);
      end;
    end
    else
      If (Not mrTelaVar.Reposicionando) Then
        {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboComboClick - uniPlacas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  lwMes:     Word;
  lwAno:     Word;
  lwDia:     Word;
  liConexao: Integer;
  liIndCol:  array[0..MAX_NUM_COLUNAS] of Integer;
  lsCampo:   string;
  lsSql:     string;
  liConta: Integer;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' guarda o valor original do combo}
    gsAreaDesfaz := lcboCombo.Text;
    giAreaDesfaz := -1;
    if lcboCombo.Items.Count > 0 then
      {'Executa um la�o nos itens comparando-os com o pConteudo}
      for liConta := 0 to lcboCombo.Items.Count - 1 do
        if lcboCombo.Items[liConta] = gsAreaDesfaz then
        begin
          giAreaDesfaz := liConta;
          break;
        end;

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
    end;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboComboEnter - uniPlacas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.cboComboKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.cboComboKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.edtTextoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.edtTextoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = EDT_CODIGO) And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    {' atualiza o caption da janela}
    Caption := mrTelaVar.P.Titulo_Tela + ' ' + ledtEdit.Text + ' - Inclus�o';
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.edtTextoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.edtTextoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprGridChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.sprGridChange(Sender: TObject; Col,
  Row: Integer);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.sprGridEditChange       Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.sprGridEditChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
  lgrdGrid.Col := Col;
  if lgrdGrid.CellType = SS_CELL_TYPE_COMBOBOX then
    untCombos.gp_Combo_SpreadSeleciona(lgrdGrid, 0);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.chkCheckClick       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.chkCheckClick(Sender: TObject);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.PntComunicadorPaint(Sender: TObject);
var
  liComandoMDI: Integer;
begin

  liComandoMDI := giComando_MDI;

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, gfrmTV);

  if (liComandoMDI = TV_COMANDO_IR_PROXIMO) or
     (liComandoMDI = TV_COMANDO_IR_ANTERIOR) or
     (liComandoMDI = TV_COMANDO_IR_PRIMEIRO) or
     (liComandoMDI = TV_COMANDO_PROCURAR) or
     (liComandoMDI = TV_COMANDO_IR_ULTIMO) then
  begin
//    mpMostra_Gaveta;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.edtTextoExit       quando perde o foco
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.edtTextoExit(Sender: TObject);
var
  ledtEdit: TMaskEdit;
  i, j:  Integer;
  liConta:    Integer;
  liConexao:  Integer;
  lsSql:      string;
  liIndCol:   array[0..4] of Integer;
  lsCodCompl: string;
  lcVlrCompl: Currency;
begin

  ledtEdit := TMaskEdit(Sender);

  if Sender is TMaskEdit then
  begin
    { procura pela entidade }
    i := untTelaVar.gf_TelaVar_Retorna_Tabela(mrTelaVar.P.Sigla_Tabela);
    { se nao achou }
    if i = 0 then
      Exit;

    j := 1;
    { percorre todos os campos da tabela principal }
    while gaLista_Campos[i][j].sSigla_Tabela = gaLista_Parametros[i].Sigla_Tabela do
    begin
      if gaLista_Campos[i][j].sNome_Controle = ledtEdit.Name then
      begin
        if gaLista_Campos[i][j].sAlinha = ALINHA_ZERO then
          ledtEdit.Text := untFuncoes.gf_Zero_Esquerda(ledtEdit.Text, gaLista_Campos[i][j].iTamanho);
        break;
      end;
      j := j + 1;
    end;
  end;

  if ledtEdit.Name = 'mskProposta' then
  begin
    { se nao estiver vazio }
//    if Trim(mskProposta.Text) <> VAZIO then
//    begin
      { se o digito verificador Numero da Proposta estiver errado}
//      if not untDigito.gf_Digito_ChecarProposta(mskProposta.Text) then
//      begin
//        MessageDlg('D�gito incorreto no No. da Proposta [' + Copy(mskProposta.Text,1,7) + '-' + Copy(mskProposta.Text,8,1) + '] !!!',
//                   mtWarning, [mbOK], 0);
//        Exit;
//      end;
//    end;
    { ler os registros da tabela proponentes }
    gaParm[0] := 'PROPONEN LEFT JOIN PROPGAVE ON PROPONEN.NUM_PROP = PROPGAVE.NUM_PROP';
    gaParm[1] := 'PROPONEN.NUM_PROP = ''' + mskProposta.Text + '''';
    gaParm[2] := 'PROPONEN.NUM_PROP';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;
    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    dmDados.gsRetorno := dmDados.Primeiro(liConexao);
    if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
    begin
      for liConta := 0 to High(liIndCol) do
        liIndCol[liConta] := IOPTR_NOPOINTER;
      { pega o conteudo do campo }
      edtSolicitante.Text := dmDados.ValorColuna(liConexao, 'NOME_PROP', liIndCol[0]);
      edtDescricao.Text := edtSolicitante.Text;
      mskTelefone.Text := dmDados.ValorColuna(liConexao, 'FONE', liIndCol[1]);
//      mskQuadra.Text := dmDados.ValorColuna(liConexao, 'QUADRA', liIndCol[2]);
      untCombos.gp_Combo_Posiciona(cboQuadra, dmDados.ValorColuna(liConexao, 'QUADRA', liIndCol[2]));
      mskSetor.Text := dmDados.ValorColuna(liConexao, 'SETOR', liIndCol[3]);
      mskLote.Text := dmDados.ValorColuna(liConexao, 'LOTE', liIndCol[4]);
    end;
    dmDados.FecharConexao(liConexao);
    mpMostra_Valor;
  end;
  if ledtEdit.Name = 'mskNumParc' then
  begin
    if mskNumParc.Text <> VAZIO then
      sprCheques.MaxRows := StrToInt(mskNumParc.Text);
  end;
  if ledtEdit.Name = 'mskQtdLetras' then
  begin
    if mskQtdLetras.Text <> VAZIO then
    begin
      { codigo do complemento }
      lsCodCompl := dmDados.ConfigValor('Cod_Compl');
      { valor por letra }
      gaParm[0] := 'VALOR_NORMAL';
      gaParm[1] := 'TAB_SERV_VALOR';
      gaParm[2] := 'CODIGOSERV = ''' + lsCodCompl + ''' AND DATA = (SELECT MAX(DATA) FROM TAB_SERV_VALOR WHERE CODIGOSERV = ''' + lsCodCompl + ''')';
      gaParm[3] := 'PROPONEN.NUM_PROP';
      lsSql := dmDados.SqlVersao('NEC_0000', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;
      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      { se achou }
      if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
      begin
        liIndCol[0] := IOPTR_NOPOINTER;
        lcVlrCompl := dmDados.ValorColuna(liConexao, 'VALOR_NORMAL', liIndCol[0]);
        mskValorTotal.Text := FormatFloat(MK_VALOR, StrToInt(mskQtdLetras.Text) * lcVlrCompl);
      end;
      dmDados.FecharConexao(liConexao);
    end;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.cboComboExit(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpMostra_Gaveta       Mostra se tem gaveta
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPlacas.mpMostra_Valor();
var
  lsSql: string;
  liConexao: Integer;
  liIndCol:   array[0..1] of Integer;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  { ler os registros da tabela servicos }
  gaParm[0] := 'TAB_SERV_VALOR';
  gaParm[1] := 'CODIGOSERV = ''' + untCombos.gf_Combo_SemDescricao(cboProduto) + '''';
  gaParm[2] := 'CODIGO DESC';
  lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
  if dmDados.giStatus <> STATUS_OK then
    Exit;
  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;

  dmDados.gsRetorno := dmDados.Primeiro(liConexao);
  if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
  begin
    liIndCol[0] := IOPTR_NOPOINTER;
    { pega o conteudo do campo }
    mskValor.Text := FormatFloat(MK_VALOR, dmDados.ValorColuna(liConexao, 'VALOR_NORMAL', liIndCol[0]));
  end;
  dmDados.FecharConexao(liConexao);

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;

end;


end.
