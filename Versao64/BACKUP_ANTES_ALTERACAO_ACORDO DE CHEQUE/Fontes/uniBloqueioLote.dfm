�
 TFRMBLOQUEIOLOTE 0V  TPF0TfrmBloqueioLotefrmBloqueioLoteLeft� Top� Width�Height� CaptionBloqueio de LoteColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	Position	poDefaultVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidth�HeightyTabOrder  	TGroupBoxgrpTipoMotivoLeftTopWidth�HeightqTabOrder  TLabellblBloqueioLeftxTopWidthHeightCaptionTipo  TLabellblDataBloqLeft� TopWidthRHeightCaptionData do Bloqueio  TLabel	lblCodigoLeftTopWidthRHeightCaption   Número Proposta  TLabellblReativacaoLeftxTop@Width7HeightCaption   Reativação  TLabellblDataReatLeft� Top@Width`HeightCaption   Data da Reativação  	TPaintBoxPntComunicadorLeftTopWidthqHeightaOnPaintPntComunicadorPaint  	TComboBoxcboBloqueioLeftxTop WidthdHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataBloqLeft� Top WidthZHeightEditMask!99/99/0000;1; 	MaxLength
TabOrderText
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEdit	mskCodigoLeftTop WidthRHeightEditMask9999999-9;0;_	MaxLength	TabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboReativacaoLeftxTopPWidthdHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataReatLeft� TopPWidthZHeightEditMask!99/99/0000;1; 	MaxLength
TabOrderText
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress     