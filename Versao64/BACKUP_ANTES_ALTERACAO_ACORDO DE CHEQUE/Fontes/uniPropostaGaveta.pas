unit uniPropostaGaveta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Mask, Variants,
  uniConst, ExtCtrls, OleCtrls, FPSpread_TLB;

const
  EDT_CODIGO: string = 'mskCodigo';

  COL_PARC = 1;
  COL_DATA = 2;
  COL_VALOR = 3;
  COL_VALORVENC = 4;

  VLR_MULTA = 0.02;
  VLR_JUROS = 0.00034;

  DIAS_ATRASO = 0;

type
  TfrmPropostaGaveta = class(TForm)
    pnlFundo: TPanel;
    grbPropostaGaveta: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    mskCodigo: TMaskEdit;
    mskDataProposta: TMaskEdit;
    grbGaveta: TGroupBox;
    Label14: TLabel;
    Label13: TLabel;
    Label5: TLabel;
    grbPagamento: TGroupBox;
    Label11: TLabel;
    Label17: TLabel;
    Label12: TLabel;
    Label10: TLabel;
    edtValorSinal: TEdit;
    edtNumeroParcelas: TEdit;
    edtValorParcela: TEdit;
    mskVcto1Parcela: TMaskEdit;
    grbPlano: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    cboCodigoPlano: TComboBox;
    cboCodigoCorretor: TComboBox;
    pagProponentes: TPageControl;
    tabProponentes: TTabSheet;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label9: TLabel;
    Label22: TLabel;
    Label25: TLabel;
    edtEndereco: TEdit;
    mskCEP: TMaskEdit;
    edtCidade: TEdit;
    edtBairro: TEdit;
    mskDataNascimento: TMaskEdit;
    mskRG: TMaskEdit;
    mskCPF: TMaskEdit;
    mskTelefone: TMaskEdit;
    edtDDD: TEdit;
    edtPropNome: TEdit;
    edtRamal: TEdit;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    edtProfissao: TEdit;
    Label26: TLabel;
    mskObservacao: TEdit;
    Label27: TLabel;
    chkEmitirBoleto: TCheckBox;
    sprGavetaConjugada: TvaSpread;
    sprGavetaTransformada: TvaSpread;
    PntComunicador: TPaintBox;
    edtProponente_Estado: TEdit;
    mskSetor: TMaskEdit;
    mskLote: TMaskEdit;
    mskDataPlano: TMaskEdit;
    Label6: TLabel;
    cboTipoGaveta: TComboBox;
    Label3: TLabel;
    sprProponentes: TvaSpread;
    Label28: TLabel;
    edtBloqueada: TEdit;
    Label29: TLabel;
    edtTemOcupacao: TEdit;
    TabSheet3: TTabSheet;
    sprBeneficiarios: TvaSpread;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Label30: TLabel;
    sprDebitoLote: TvaSpread;
    Label31: TLabel;
    edtDebitoLote: TEdit;
    Label32: TLabel;
    sprDebitoGaveta: TvaSpread;
    Label33: TLabel;
    edtDebitoGaveta: TEdit;
    Label34: TLabel;
    sprDebitoTaxa: TvaSpread;
    Label35: TLabel;
    edtdebitoTaxa: TEdit;
    TabSheet6: TTabSheet;
    sprCheques: TvaSpread;
    TabSheet7: TTabSheet;
    Label36: TLabel;
    sprAcompanhamento: TvaSpread;
    sprExumacoes: TvaSpread;
    sprGavetaPessoas: TvaSpread;
    Label37: TLabel;
    edtLocEspecial: TEdit;
    edtNumeroGaveta: TEdit;
    Label38: TLabel;
    cboQuadra: TComboBox;
    grbNumGaveta: TGroupBox;
    mskGav2: TMaskEdit;
    mskGav3: TMaskEdit;
    mskGav1: TMaskEdit;
    mskGav4: TMaskEdit;
    mskGav5: TMaskEdit;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    mskGav6: TMaskEdit;
    GroupBox1: TGroupBox;
    chkGaveta1: TCheckBox;
    chkGaveta2: TCheckBox;
    chkGaveta3: TCheckBox;
    chkGaveta4: TCheckBox;
    chkGaveta5: TCheckBox;
    chkGaveta6: TCheckBox;
    chkGaveta7: TCheckBox;
    chkGaveta8: TCheckBox;
    chkGaveta9: TCheckBox;
    chkGaveta10: TCheckBox;
    mskLote2: TMaskEdit;
    Label45: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure cboComboExit(Sender: TObject);
    procedure cboComboKeyPress(Sender: TObject; var Key: Char);
    procedure edtTextoChange(Sender: TObject);
    procedure edtTextoEnter(Sender: TObject);
    procedure edtTextoExit(Sender: TObject);
    procedure sprGridChange(Sender: TObject; Col, Row: Integer);
    procedure sprGridEditChange(Sender: TObject; Col, Row: Integer);
    procedure chkCheckClick(Sender: TObject);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;
    mbDentro:   Boolean; {' Para evitar recurs�o: Inicia com False}

    procedure mpMostra_Ocupacao;
    procedure mpValor_Receber;
    procedure mpValor_Total;
    procedure mpValida_Localizacao;

  public
    { Public declarations }
  end;

var
  frmPropostaGaveta: TfrmPropostaGaveta;

implementation

{$R *.DFM}
uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos, uniFuncoes,
  uniEntradaManual;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.FormClose(Sender: TObject; var Action: TCloseAction);
var
  liConta:  Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - uniPropostaGaveta',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;
    gfrmTV := nil;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniPropostaGaveta');
  end;

  // destroi a janela
  Action := caFree;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, frmPropostaGaveta);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.FormResize(Sender: TObject);
begin
  if TfrmPropostaGaveta(Sender).WindowState <> wsMinimized Then
  begin
    if (TfrmPropostaGaveta(Sender).Width < pnlFundo.Width + 39) and (TfrmPropostaGaveta(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TfrmPropostaGaveta(Sender).Width := pnlFundo.Width + 39;

    TfrmPropostaGaveta(Sender).Refresh;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
//    mrTelaVar.P.Titulo_Tela := 'Proposta de Gaveta';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
    untEdit.gp_Edit_EstufaCombos(mrTelaVar.P.Sigla_Tabela, TForm(Sender));

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);

    mpMostra_Ocupacao;
    mpValor_Receber;
    mpValor_Total;
    mpValida_Localizacao;

    { Para evitar recurs�o: Inicia com False}
    mbDentro := False;

    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniPropostaGaveta');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 3);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
      if lcboCombo.Text <> VAZIO then
      begin
        { se foi combo de planos }
        if lcboCombo.Name = CBO_PLANO_CODIGO_PGV then
        begin
          mskDataPlano.Text := Copy(lcboCombo.Text,34,10);
          edtValorSinal.Text := FormatFloat(MK_VALOR, StrToFloat(Copy(lcboCombo.Text,7,8)));
          edtValorParcela.Text := FormatFloat(MK_VALOR, StrToFloat(Copy(lcboCombo.Text,18,8)));
          edtNumeroParcelas.Text := Copy(lcboCombo.Text,29,2);
        end;

        {' altera��o efetuada}
        untEdit.gp_Edit_Alterado(True, mrTelaVar);
      end;
    end
    else
      If (Not mrTelaVar.Reposicionando) Then
        {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboCOmboClick - uniPropostaGaveta');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  lwMes:     Word;
  lwAno:     Word;
  lwDia:     Word;
  liConexao: Integer;
  liIndCol:  array[0..MAX_NUM_COLUNAS] of Integer;
  lsCampo:   string;
  lsSql:     string;
  liConta: Integer;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' guarda o valor original do combo}
    gsAreaDesfaz := lcboCombo.Text;
  giAreaDesfaz := -1;
  if lcboCombo.Items.Count > 0 then
    {'Executa um la�o nos itens comparando-os com o pConteudo}
    for liConta := 0 to lcboCombo.Items.Count - 1 do
      if lcboCombo.Items[liConta] = gsAreaDesfaz then
      begin
        giAreaDesfaz := liConta;
        break;
      end;

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
      { se foi combo de Planos }
      if lcboCombo.Name = CBO_PLANO_CODIGO_PGV then
      begin
        DecodeDate(Date(), lwAno, lwMes, lwDia);
        if lwDia <= 15 then
          lwMes := lwMes - 1;
        if lwMes <= 0 then
        begin
          lwMes := lwMes + 12;
          lwAno := lwAno - 1;
        end;
        { planos }
        gaParm[0] := 'G';
        gaParm[1] := Copy(cboTipoGaveta.Text,1,2);
        gaParm[2] := IntToStr(lwMes) + '/' + '01' + '/' + IntToStr(lwAno);
        liConexao := 0;
        lsSQL := dmDados.SqlVersao('NEC_0003', gaParm);
        liConexao := dmDados.ExecutarSelect(lsSQL);
        if liConexao = IOPTR_NOPOINTER then
          Exit;

        if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
        begin
          dmDados.gsRetorno := dmDados.Primeiro(liConexao);

          liIndCol[0] := IOPTR_NOPOINTER;
          liIndCol[1] := IOPTR_NOPOINTER;
          liIndCol[2] := IOPTR_NOPOINTER;
          liIndCol[3] := IOPTR_NOPOINTER;
          liIndCol[4] := IOPTR_NOPOINTER;

          cboCodigoPlano.Clear;
          while not (dmDados.gsRetorno = IORET_EOF) do
          begin
            lsCampo := dmDados.ValorColuna(liConexao, 'COD_PLANO', liIndCol[0]);
            lsCampo := lsCampo + '   ' + FormatFloat('00000.00', dmDados.ValorColuna(liConexao, 'VLR_SINAL', liIndCol[1]));
            lsCampo := lsCampo + '   ' + FormatFloat('00000.00', dmDados.ValorColuna(liConexao, 'V_PARCELAS', liIndCol[2]));
            lsCampo := lsCampo + '   ' + FormatFloat('00', dmDados.ValorColuna(liConexao, 'Q_PARCELAS', liIndCol[3]));
            lsCampo := lsCampo + '   ' + FormatDateTime('dd/mm/yyyy', dmDados.ValorColuna(liConexao, 'DATA_INCL', liIndCol[4]));
            cboCodigoPlano.Items.Add(lsCampo);
            dmDados.gsRetorno := dmDados.Proximo(liConexao);
          end;
        end;
        dmDados.FecharConexao(liConexao);
      end;
    end;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboComboEnter - uniPropostaGaveta');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.cboComboKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.cboComboKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.edtTextoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.edtTextoChange(Sender: TObject);
var
  ledtEdit: TEdit;
  lcboCombo: TComboBox;
  lsSql: string;
  liConexao: Integer;
begin

  try
    {'se j� estou nesta rotina nada feito}
    if mbDentro then
      Exit;

    {' Estou dentro n�o executar de novo}
    mbDentro := True;

    ledtEdit := TEdit(Sender);
    lcboCombo := TComboBox(Sender);

    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
      {' altera��o efetuada}
      untEdit.gp_Edit_Alterado(True, mrTelaVar);

    {' se for o campo Codigo no modo de Inclus�o}
    If (ledtEdit.Name = EDT_CODIGO) And (mrTelaVar.Comando_MDI = ED_NOVO) Then
    begin
      {' atualiza o caption da janela}
      Caption := mrTelaVar.P.Titulo_Tela + ' ' + ledtEdit.Text + ' - Inclus�o';
      mrTelaVar.Cod_Valor := ledtEdit.Text;
    End;

    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
      if (lcboCombo.Name = 'cboQuadra') or (ledtEdit.Name = 'mskSetor') or (ledtEdit.Name = 'mskLote') then
      begin
        { ler os registros da tabela ocupacao }
//        gaParm[0] := 'GAVEPESS';
//        gaParm[1] := 'Substring(CODIGO,1,8) = ''' + mskCodigo.Text + '''';
//        gaParm[2] := 'CODIGO';
//        lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
        gaParm[0] := 'GAVEPESS';
        gaParm[1] := mskCodigo.Text;
        lsSql := dmDados.SqlVersao('NEC_0024', gaParm);
        if dmDados.giStatus <> STATUS_OK then
        begin
          {'Ok estou saindo, n�o h� mais risco de recurs�o}
          mbDentro := False;
          Exit;
        end;
        liConexao := dmDados.ExecutarSelect(lsSql);
        if liConexao = IOPTR_NOPOINTER then
        begin
          {'Ok estou saindo, n�o h� mais risco de recurs�o}
          mbDentro := False;
          Exit;
        end;

        if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
        begin
          MessageDlg('Proposta com Ocupa��o !!!',
                     mtWarning, [mbOK], 0);
          ledtEdit.Text := gsAreaDesfaz;
        end;
        dmDados.FecharConexao(liConexao);
      end;
    end;

  except
    dmDados.ErroTratar('edtTextoChange - uniPropostaGaveta.Pas');
  end;

  {'Ok estou saindo, n�o h� mais risco de recurs�o}
  mbDentro := False;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.edtTextoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.edtTextoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprGridChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.sprGridChange(Sender: TObject; Col,
  Row: Integer);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.sprGridEditChange       Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.sprGridEditChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
  lgrdGrid.Col := Col;
  if lgrdGrid.CellType = SS_CELL_TYPE_COMBOBOX then
    untCombos.gp_Combo_SpreadSeleciona(lgrdGrid, 0);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.chkCheckClick       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.chkCheckClick(Sender: TObject);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.PntComunicadorPaint(Sender: TObject);
var
  liComandoMDI: Integer;
begin

  liComandoMDI := giComando_MDI;

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, frmPropostaGaveta);

  if (liComandoMDI = TV_COMANDO_IR_PROXIMO) or
     (liComandoMDI = TV_COMANDO_IR_ANTERIOR) or
     (liComandoMDI = TV_COMANDO_IR_PRIMEIRO) or
     (liComandoMDI = TV_COMANDO_PROCURAR) or
     (liComandoMDI = TV_COMANDO_IR_ULTIMO) then
  begin
    mpMostra_Ocupacao;
    mpValor_Receber;
    mpValor_Total;
    mpValida_Localizacao;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaGaveta.edtTextoExit       Procura pelo Proponente
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.edtTextoExit(Sender: TObject);
var
  liConexao:  Integer;
  liLinha:    Integer;
  liConta:    Integer;
  lsSql:      string;
  lsBloqueada: string;
  lmskEdit:   TMaskEdit;
  liIndCol:   array[0..20] of Integer;
  i, j:  Integer;
begin

  try
    lmskEdit := TMaskEdit(Sender);

    if Sender is TMaskEdit then
    begin
      { procura pela entidade }
      i := untTelaVar.gf_TelaVar_Retorna_Tabela(mrTelaVar.P.Sigla_Tabela);
      { se nao achou }
      if i = 0 then
        Exit;

      j := 1;
      { percorre todos os campos da tabela principal }
      while gaLista_Campos[i][j].sSigla_Tabela = gaLista_Parametros[i].Sigla_Tabela do
      begin
        if gaLista_Campos[i][j].sNome_Controle = lmskEdit.Name then
        begin
          if gaLista_Campos[i][j].sAlinha = ALINHA_ZERO then
            lmskEdit.Text := untFuncoes.gf_Zero_Esquerda(lmskEdit.Text, gaLista_Campos[i][j].iTamanho);
          break;
        end;
        j := j + 1;
      end;
    end;
    
    if lmskEdit.Name = 'mskCodigo' then
    begin
      { ler os registros da tabela proplote }
      gaParm[0] := 'PROPLOTE';
      gaParm[1] := 'NUM_PROP = ''' + lmskEdit.Text + '''';
      gaParm[2] := 'NUM_PROP';
      lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;
      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      for liConta := 0 to 20 do
        liIndCol[liConta] := IOPTR_NOPOINTER;

      dmDados.gsRetorno := dmDados.Primeiro(liConexao);
      if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
      begin
        { verifica se nao esta cancelada }
        lsBloqueada := dmDados.ValorColuna(liConexao, 'BLOQUEADA', liIndCol[16]);
        if lsBloqueada <> VAZIO then
        begin
          dmDados.gaMsgParm[0] := 'N�mero da Proposta [' + lmskEdit.Text + ']';
          dmDados.gaMsgParm[1] := ' Proposta Bloqueada (' + lsBloqueada + ') ';
          dmDados.MensagemExibir('', 4100);
          mrTelavar.Reposicionando := True;
          mskCodigo.Text := VAZIO;
          mrTelavar.Reposicionando := False;
          Exit;
        end;
        { pega o conteudo do campo }
        cboTipoGaveta.Text := dmDados.ValorColuna(liConexao, 'TIPO_LOTE', liIndCol[15]);
        untCombos.gp_Combo_Seleciona(cboTipoGaveta, 2);
      end
      else
      begin
        dmDados.gaMsgParm[0] := 'N�mero da Proposta';
        dmDados.gaMsgParm[1] := '[' + lmskEdit.Text + ']';
        dmDados.MensagemExibir('', 4003);
      end;
      dmDados.FecharConexao(liConexao);

      { ler os registros da tabela proponentes }
      gaParm[0] := 'PROPONEN';
      gaParm[1] := 'NUM_PROP = ''' + lmskEdit.Text + '''';
      gaParm[2] := 'NUM_PROP';
      lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;
      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      dmDados.gsRetorno := dmDados.Primeiro(liConexao);
      if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
      begin
        { pega o conteudo do campo }
        edtPropNome.Text := dmDados.ValorColuna(liConexao, 'NOME_PROP', liIndCol[1]);
        if dmDados.ValorColuna(liConexao, 'DATA_NASC', liIndCol[2]) <> 0 then
          mskDataNascimento.Text := FormatDateTime(MK_DATATELA, dmDados.ValorColuna(liConexao, 'DATA_NASC', liIndCol[2]));
        edtProfissao.Text := dmDados.ValorColuna(liConexao, 'PROFISSAO', liIndCol[3]);
        edtEndereco.Text := dmDados.ValorColuna(liConexao, 'ENDERECO', liIndCol[4]);
        mskCEP.Text := dmDados.ValorColuna(liConexao, 'CEP', liIndCol[5]);
        edtProponente_Estado.Text := dmDados.ValorColuna(liConexao, 'ESTADO', liIndCol[6]);
        edtCidade.Text := dmDados.ValorColuna(liConexao, 'CIDADE', liIndCol[7]);
        edtBairro.Text := dmDados.ValorColuna(liConexao, 'BAIRRO', liIndCol[8]);
        edtDDD.Text := dmDados.ValorColuna(liConexao, 'DDD', liIndCol[9]);
        mskTelefone.Text := dmDados.ValorColuna(liConexao, 'FONE', liIndCol[10]);
        edtRamal.Text := dmDados.ValorColuna(liConexao, 'RAMAL', liIndCol[11]);
        mskRG.Text := dmDados.ValorColuna(liConexao, 'RG', liIndCol[12]);
        mskCPF.Text := dmDados.ValorColuna(liConexao, 'CPF', liIndCol[13]);
        mskObservacao.Text := dmDados.ValorColuna(liConexao, 'OBSERVACAO', liIndCol[14]);
        edtRamal.Text := dmDados.ValorColuna(liConexao, 'CELULAR', liIndCol[4]);
      end;
      dmDados.FecharConexao(liConexao);

      { ler os registros da tabela proponentes }
      gaParm[0] := 'PROPOSEC';
      gaParm[1] := 'NUM_PROP = ''' + lmskEdit.Text + '''';
      gaParm[2] := 'NUM_PROP';
      lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;
      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      { posiciona na coluna nome }
      sprProponentes.Col := 1;
      liLinha := 1;

      dmDados.gsRetorno := dmDados.Primeiro(liConexao);
      while (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) do
      begin
        { posiciona na linha }
        sprProponentes.Row := liLinha;
        { pega o conteudo do campo }
        sprProponentes.Text := dmDados.ValorColuna(liConexao, 'NOME_SEC', liIndCol[16]);
        liLinha := liLinha + 1;
        dmDados.gsRetorno := dmDados.Proximo(liConexao);
      end;
      dmDados.FecharConexao(liConexao);
    end;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('mskCodigoExit - uniPropostaGaveta');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.cboComboExit(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpMostra_Ocupacao       Mostra se tem ocupacao
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.mpMostra_Ocupacao();
var
  lsSql: string;
  liConexao: Integer;
  aSqlParms:  array[0..2] of Variant;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

//  aSqlParms[0] := 'Codigo';
//  aSqlParms[1] := 'GavePess';
//  aSqlParms[2] := 'Substring(Codigo,1,8) = ''' + mskCodigo.Text + '''';
//  lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);
  aSqlParms[0] := 'GavePess';
  aSqlParms[1] := mskCodigo.Text;
  lsSql := dmDados.SqlVersao('NEC_0024', aSqlParms);

  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;
  { se encontrou a op��o }
  if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
    edtTemOcupacao.Text := 'S'
  else
    edtTemOcupacao.Text := 'N';
  dmDados.FecharConexao(liConexao);

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpValor_Receber       Mostra o valor a receber
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.mpValor_Receber();
var
  liConta: Integer;
  lsParcela: string;
  lsDataVenc: string;
  lsValorVenc: string;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  { Lotes }
  sprDebitoLote.ReDraw := False;
  for liConta := 1 to sprDebitoLote.MaxRows - 1 do
  begin
    sprDebitoLote.Row := liConta;

    { parcela }
    sprDebitoLote.Col := COL_PARC;
    lsParcela := sprDebitoLote.Text;
    { data }
    sprDebitoLote.Col := COL_DATA;
    lsDataVenc := sprDebitoLote.Text;
    { valor }
    sprDebitoLote.Col := COL_VALORVENC;
    lsValorVenc := sprDebitoLote.Text;

    { se ainda nao foi pago }
    if lsParcela = VAZIO then
      break;
    { se nao tem vencimento nao calcula }
    if lsDataVenc = VAZIO then
    begin
      sprDebitoLote.Col := COL_VALOR;
      sprDebitoLote.Text := lsValorVenc;
    end
    else
    begin
      { tolerancia de cinco dias }
      if StrToDate(lsDataVenc) >= (Date - DIAS_ATRASO) then
      begin
        sprDebitoLote.Col := COL_VALOR;
        sprDebitoLote.Text := lsValorVenc;
      end
      else
      begin
        sprDebitoLote.Col := COL_VALOR;
//        sprDebitoLote.Text := frmEntradaManual.gpValor_Atual(mskCodigo.Text,
        sprDebitoLote.Text := untFuncoes.gf_Funcoes_ValorAtual(mskCodigo.Text,
                                            'L',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela);
        if StrToFloat(dmDados.TirarSeparador(sprDebitoLote.Text)) = 0 then
          sprDebitoLote.Text := lsValorVenc;
      end;
    end;
  end;
  sprDebitoLote.ReDraw := True;

  { Gavetas }
  sprDebitoGaveta.ReDraw := False;
  for liConta := 1 to sprDebitoGaveta.MaxRows - 1 do
  begin
    sprDebitoGaveta.Row := liConta;

    { parcela }
    sprDebitoGaveta.Col := COL_PARC;
    lsParcela := sprDebitoGaveta.Text;
    { data }
    sprDebitoGaveta.Col := COL_DATA;
    lsDataVenc := sprDebitoGaveta.Text;
    { valor }
    sprDebitoGaveta.Col := COL_VALORVENC;
    lsValorVenc := sprDebitoGaveta.Text;

    { se ainda nao foi pago }
    if lsParcela = VAZIO then
      break;
    { se nao tem vencimento nao calcula }
    if lsDataVenc = VAZIO then
    begin
      sprDebitoGaveta.Col := COL_VALOR;
      sprDebitoGaveta.Text := lsValorVenc;
    end
    else
    begin
      { tolerancia de cinco dias }
      if StrToDate(lsDataVenc) >= (Date - DIAS_ATRASO) then
      begin
        sprDebitoGaveta.Col := COL_VALOR;
        sprDebitoGaveta.Text := lsValorVenc;
      end
      else
      begin
        sprDebitoGaveta.Col := COL_VALOR;
//        sprDebitoGaveta.Text := frmEntradaManual.gpValor_Atual(mskCodigo.Text,
        sprDebitoGaveta.Text := untFuncoes.gf_Funcoes_ValorAtual(mskCodigo.Text,
                                            'G',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela);
        if StrToFloat(dmDados.TirarSeparador(sprDebitoGaveta.Text)) = 0 then
          sprDebitoGaveta.Text := lsValorVenc;
      end;
    end;
  end;
  sprDebitoGaveta.ReDraw := True;

  { Taxas }
  sprDebitoTaxa.ReDraw := False;
  for liConta := 1 to sprDebitoTaxa.MaxRows - 1 do
  begin
    sprDebitoTaxa.Row := liConta;

    { parcela }
    sprDebitoTaxa.Col := COL_PARC;
    lsParcela := sprDebitoTaxa.Text;
    { data }
    sprDebitoTaxa.Col := COL_DATA;
    lsDataVenc := sprDebitoTaxa.Text;
    { valor }
    sprDebitoTaxa.Col := COL_VALORVENC;
    lsValorVenc := sprDebitoTaxa.Text;

    { se ainda nao foi pago }
    if lsParcela = VAZIO then
      break;
    { se nao tem vencimento nao calcula }
    if lsDataVenc = VAZIO then
    begin
      sprDebitoTaxa.Col := COL_VALOR;
      sprDebitoTaxa.Text := lsValorVenc;
    end
    else
    begin
      { tolerancia de cinco dias }
      if StrToDate(lsDataVenc) >= (Date - DIAS_ATRASO) then
      begin
        sprDebitoTaxa.Col := COL_VALOR;
        sprDebitoTaxa.Text := lsValorVenc;
      end
      else
      begin
        sprDebitoTaxa.Col := COL_VALOR;
        sprDebitoTaxa.Text := untFuncoes.gf_Funcoes_ValorAtual(mskCodigo.Text,
                                            'T',
                                            lsValorVenc,
                                            FormatFloat(MK_VALOR, VLR_MULTA * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            FormatFloat(MK_VALOR, VLR_JUROS * StrToFloat(dmDados.TirarSeparador(lsValorVenc))),
                                            lsDataVenc,
                                            lsParcela);
        if StrToFloat(dmDados.TirarSeparador(sprDebitoTaxa.Text)) = 0 then
          sprDebitoTaxa.Text := lsValorVenc
        else
          sprDebitoTaxa.Text := FormatFloat(MK_VALOR,
                                StrToFloat(dmDados.TirarSeparador(sprDebitoTaxa.Text)) -
                                untFuncoes.gf_Funcoes_SaldoParc(mskCodigo.Text,
                                                              'T',
                                                              lsParcela));
      end;
    end;
  end;
  sprDebitoTaxa.ReDraw := True;

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpValor_Total       Mostra o valor a receber
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.mpValor_Total();
var
  liConta:      Integer;
  ldDebitoLote: Double;
  ldDebitoGave: Double;
  ldDebitoTaxa: Double;
begin

    { para nao ativar o salvar }
    mrTelaVar.Reposicionando := True;

    ldDebitoLote := 0;
    ldDebitoGave := 0;
    ldDebitoTaxa := 0;

    sprDebitoLote.Col := COL_VALOR;
    for liConta := 1 to sprDebitoLote.MaxRows do
    begin
      sprDebitoLote.Row := liConta;
      if sprDebitoLote.Text <> VAZIO then
        ldDebitoLote := ldDebitoLote + StrToFloat(dmDados.TirarSeparador(sprDebitoLote.Text));
    end;
    sprDebitoGaveta.Col := COL_VALOR;
    for liConta := 1 to sprDebitoGaveta.MaxRows do
    begin
      sprDebitoGaveta.Row := liConta;
      if sprDebitoGaveta.Text <> VAZIO then
        ldDebitoGave := ldDebitoGave + StrToFloat(dmDados.TirarSeparador(sprDebitoGaveta.Text));
    end;
    sprDebitoTaxa.Col := COL_VALOR;
    for liConta := 1 to sprDebitoTaxa.MaxRows do
    begin
      sprDebitoTaxa.Row := liConta;
      if sprDebitoTaxa.Text <> VAZIO then
        ldDebitoTaxa := ldDebitoTaxa + StrToFloat(dmDados.TirarSeparador(sprDebitoTaxa.Text));
    end;

    edtDebitoLote.Text := FormatFloat(MK_VALOR, ldDebitoLote);
    edtDebitoGaveta.Text := FormatFloat(MK_VALOR, ldDebitoGave);
    edtDebitoTaxa.Text := FormatFloat(MK_VALOR, ldDebitoTaxa);

    { para nao ativar o salvar }
    mrTelaVar.Reposicionando := False;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpValida_Localizacao       Habilita/desabilita localizacao
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaGaveta.mpValida_Localizacao();
var
  lsSql: string;
  liConexao: Integer;
  aSqlParms:  array[0..2] of Variant;
begin
  try
    { para nao ativar o salvar }
    mrTelaVar.Reposicionando := True;

    { se for inclusao pode mudar }
    if mrTelaVar.Comando_MDI = ED_NOVO then
    begin
      cboQuadra.Enabled := True;
      mskSetor.Enabled := True;
      mskLote.Enabled := True;
    end
    else
    begin
      { se estiver bloqueada pode mudar }
      if edtBloqueada.Text <> VAZIO then
      begin
        cboQuadra.Enabled := True;
        mskSetor.Enabled := True;
        mskLote.Enabled := True;
      end
      else
      begin
        { se nao tiver ocupacao pode mudar }
        aSqlParms[0] := 'Num_Prop';
        aSqlParms[1] := 'vGavePess';
        aSqlParms[2] := 'Num_Prop = ''' + mskCodigo.Text + '''';
        lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);

        liConexao := dmDados.ExecutarSelect(lsSql);
        if liConexao = IOPTR_NOPOINTER then
          Exit;
        { se encontrou a op��o }
        if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
        begin
          cboQuadra.Enabled := False;
          mskSetor.Enabled := False;
          mskLote.Enabled := False;
        end
        else
        begin
          cboQuadra.Enabled := True;
          mskSetor.Enabled := True;
          mskLote.Enabled := True;
        end;
        dmDados.FecharConexao(liConexao);
      end;
    end;

    { para nao ativar o salvar }
    mrTelaVar.Reposicionando := False;
  except
    dmDados.ErroTratar('mpValida_Localizacao - uniPropostaGaveta.pas');
  end;

end;





























end.
