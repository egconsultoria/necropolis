unit uniFuncoes;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, Buttons, DB, DBTables, Mask, OleCtrls, FPSpreadADO_TLB,
  uniConst, uniGlobal, uniTelaVar;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const

  DIAS_IGPM = 0;

 {*======================================================================
 *            RECORDS
 *======================================================================}
type

{*======================================================================
 *            CLASSES
 *======================================================================}
  TuntFuncoes = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function gf_Funcoes_Validar(const psNomeFuncao: string;
                                const paParametros: array of Variant): Boolean;
    function gf_Funcoes_Executar(const psNomeFuncao: string;
                                 const paParametros: array of Variant): Variant;
    function gf_Zero_Esquerda(const psTexto: string;
                              const liTamanho: Integer): string;
    function gf_Funcoes_Bloqueio(const psNumProp: string;
                                 const psTipoProp: string): Boolean;
    function gf_Funcoes_ValidaBloq(const psNumProp: string;
                                   const psTipoBloq: string;
                                   const psTipoProp: string): Boolean;
    function gf_Funcoes_ProcuraLote(const psQuadra: string;
                                    const psSetor: string;
                                    const psLote: string;
                                    const psTipo: string;
                                    const psNumProp: string): Boolean;
    function gf_Funcoes_Posiciona(const psTabela: string;
                                  const psCampo: string;
                                  const psValor: string;
                                  const psTipo: string): Boolean;
    function gf_Funcoes_Duplicado(const psTabela: string;
                                  const psCampo1: string;
                                  const psValor1: string;
                                  const psCampo2: string;
                                  const psValor2: string): Boolean;
    function gf_Funcoes_ValorAtual(psNumeroProposta: string;
                                   psTipoDoc: string;
                                   psValorOriginal: string;
                                   psMulta: string;
                                   psJuros: string;
                                   psVencimento: string;
                                   psNumeroParcela: string): string;
    function gf_Funcoes_SaldoParc(psNumeroProposta: string;
                                  psTipoDoc: string;
                                  psNumeroParcela: string): Double;
    function gf_Funcoes_CalculaIndice(const pnValor: Double;
                                      const pdDate: TDateTime;
                                      const psIndice: string): Double;

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untFuncoes: TuntFuncoes;

implementation

uses uniDados, uniDigito;

{*----------------------------------------------------------------------
 * TuntFuncoes.gf_Funcoes_Validar
 *----------------------------------------------------------------------}
function TuntFuncoes.gf_Funcoes_Validar(const psNomeFuncao: string;
                                         const paParametros: array of Variant): Boolean;
begin
  Result := False;
  try
    dmDados.giStatus := STATUS_OK;

    { verifica qual a funcao a executar }
    if UpperCase(psNomeFuncao) = 'MAIORIGUAL' then
      Result := (paParametros[1] >= paParametros[0]);
//      Result := mf_Funcao_(paParametros);
    if UpperCase(psNomeFuncao) = 'MAIORMENOR' then
      Result := (paParametros[2] >= paParametros[0]) and (paParametros[2] <= paParametros[1]);
    if UpperCase(psNomeFuncao) = 'CHECADIG' then
      Result := untDigito.gf_Digito_ChecarProposta(paParametros[0]);
    if UpperCase(psNomeFuncao) = 'BLOQUEIO' then
      Result := not(gf_Funcoes_Bloqueio(paParametros[0],paParametros[1]));
    if UpperCase(psNomeFuncao) = 'VALIDABLOQ' then
      Result := not(gf_Funcoes_ValidaBloq(paParametros[0],paParametros[1],paParametros[2]));
    if UpperCase(psNomeFuncao) = 'PROCURALOTE' then
      Result := gf_Funcoes_ProcuraLote(paParametros[0],paParametros[1],paParametros[2],paParametros[3],paParametros[4]);
    if UpperCase(psNomeFuncao) = 'POSICIONA' then
      Result := gf_Funcoes_Posiciona(paParametros[0],paParametros[1],paParametros[2],paParametros[3]);
    if UpperCase(psNomeFuncao) = 'DUPLICADO' then
      Result := gf_Funcoes_Duplicado(paParametros[0],paParametros[1],paParametros[2],paParametros[3],paParametros[4]);

  except
    Screen.Cursor := crDefault;
    dmDados.ErroTratar('gf_Funcoes_Validar - uniFuncoes.PAS');
  end;

end;

{*----------------------------------------------------------------------
 * TuntFuncoes.gf_Funcoes_Executar
 *----------------------------------------------------------------------}
function TuntFuncoes.gf_Funcoes_Executar(const psNomeFuncao: string;
                                         const paParametros: array of Variant): Variant;
begin
  Result := False;
  try
    dmDados.giStatus := STATUS_OK;

    { verifica qual a funcao a executar }
    if UpperCase(psNomeFuncao) = 'DATA' then
      Result := FormatDateTime(MK_DATATELA, Date);
    if UpperCase(psNomeFuncao) = 'NOMEUSUARIO' then
      Result := gsNomeUsuario;

  except
    Screen.Cursor := crDefault;
    dmDados.ErroTratar('gf_Funcoes_Executar - uniFuncoes.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.gpValor_Atual       Calcula o valor atualizado
 *
 *-----------------------------------------------------------------------------}
function TuntFuncoes.gf_Funcoes_ValorAtual(psNumeroProposta: string;
                                           psTipoDoc: string;
                                           psValorOriginal: string;
                                           psMulta: string;
                                           psJuros: string;
                                           psVencimento: string;
                                           psNumeroParcela: string): string;
var
  liConta: Integer;
  lsSql: string;
  liConexao: Integer;
  lsTabela: string;
  lsTipo: string;
  liIndCol:   array[0..7] of Integer;
  aSqlParms:  array[0..2] of Variant;
  lnRefStand: Double;
  lnVlrAtual: Double;
  lnPercTaxa: Double;
  lsCodigo: string;
  lbTaxaFixa: Boolean;
  lsTaxaRec: string;
  lsNumTaxa1: string;
  lsNumTaxa2: string;
  lsTaxaAtr1: string;
  lsTaxaAtr2: string;
  lnVlrTaxaFixa1: Double;
  lnVlrTaxaFixa2: Double;
  lsNumTaxa3: string;
  lsNumTaxa4: string;
  lnVlrTaxaVar1: Double;
  lnVlrTaxaVar2: Double;
begin

  { se nao for Taxa }
  if (psTipoDoc = 'L') or (psTipoDoc = 'G') or (psTipoDoc = 'N') or (psTipoDoc = 'R') then
  begin
(*    if psTipoDoc = 'L' then
    begin
      lsTabela := 'PropLote';
      lsTipo := 'Tipo_Lote';
    end;
    if psTipoDoc = 'G' then
    begin
      lsTabela := 'PropGave';
      lsTipo := 'Tipo_Gave';
    end;
    if psTipoDoc = 'N' then
    begin
      lsTabela := 'PropNich';
      lsTipo := '';
    end;
    if psTipoDoc = 'R' then
    begin
      lsTabela := 'PropRemi';
      lsTipo := 'Tipo_Lote';
    end;

    aSqlParms[0] := lsTabela + '.Cod_Plano, ';
    if lsTipo <> VAZIO then
      aSqlParms[0] := aSqlParms[0] + lsTabela + '.' + lsTipo + ', ' ;
    aSqlParms[0] := aSqlParms[0] + lsTabela + '.Data_Plano, PlanoVlr.Tipo_Plano';
    aSqlParms[1] := lsTabela + ', PlanoVlr';
    aSqlParms[2] := lsTabela + '.Cod_Plano = PlanoVlr.Cod_Plano And ';
    aSqlParms[2] := aSqlParms[2] + lsTabela + '.Data_Plano = PlanoVlr.Data_Incl And ';
    aSqlParms[2] := aSqlParms[2] + '(PlanoVlr.Tipo_Plano = ''U'' Or PlanoVlr.Tipo_Plano = ''R'' Or PlanoVlr.Tipo_Plano IS Null) And ';
    aSqlParms[2] := aSqlParms[2] + 'Num_Prop = ''' + psNumeroProposta + '''';
    lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;
    { se encontrou a op��o }
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
    begin
*)
      if Date > StrToDate(psVencimento) then
        Result := FormatFloat(MK_VALOR,
                  StrToFloat(dmDados.TirarSeparador(psValorOriginal)) +
                  gf_Funcoes_CalculaIndice(StrToFloat(dmDados.TirarSeparador(psValorOriginal)), StrToDate(psVencimento), '2') +
                  StrToFloat(dmDados.TirarSeparador(psJuros)) * (Date - StrToDate(psVencimento)) +
                  StrToFloat(dmDados.TirarSeparador(psMulta)))
      else
        Result := psValorOriginal;
(*    end
    else
    begin
      Result := psValorOriginal;
    end;
    dmDados.FecharConexao(liConexao);
*)
  end
  else
  begin
    aSqlParms[0] := 'Indice_Taxa';
    aSqlParms[1] := 'Proplote';
    aSqlParms[2] := 'Num_Prop = ''' + psNumeroProposta + '''';
    lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;
    { se encontrou a op��o }
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
      lsTipo := dmDados.ValorColuna(liConexao, 'Indice_Taxa', liIndCol[0])
    else
      lsTipo := '2';
    dmDados.FecharConexao(liConexao);

    if Date > StrToDate(psVencimento) then
      Result := FormatFloat(MK_VALOR,
                StrToFloat(dmDados.TirarSeparador(psValorOriginal)) +
                gf_Funcoes_CalculaIndice(StrToFloat(dmDados.TirarSeparador(psValorOriginal)), StrToDate(psVencimento), lsTipo) +
                StrToFloat(dmDados.TirarSeparador(psJuros)) * (Date - StrToDate(psVencimento)) +
                StrToFloat(dmDados.TirarSeparador(psMulta)))
    else
      Result := psValorOriginal;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.gpValor_Atual       Calcula o valor atualizado
 *
 *-----------------------------------------------------------------------------}
function TuntFuncoes.gf_Funcoes_SaldoParc(psNumeroProposta: string;
                                          psTipoDoc: string;
                                          psNumeroParcela: string): Double;
var
  liConta: Integer;
  lsSql: string;
  liConexao: Integer;
  liIndCol:   array[0..0] of Integer;
  aSqlParms:  array[0..2] of Variant;
  lsNumeroParcela: string;
begin

  Result := 0;

  if psTipoDoc = 'T' then
  begin
    if (Copy(psNumeroParcela,4,2) <= '29') and (Copy(psNumeroParcela,4,2) >= '10') then
      lsNumeroParcela := '00'
    else
      if (Copy(psNumeroParcela,4,2) >= '00') and (Copy(psNumeroParcela,4,2) <= '30') then
        lsNumeroParcela := '20'
      else
        lsNumeroParcela := '19';
    lsNumeroParcela := lsNumeroParcela + Copy(psNumeroParcela,4,2) + Copy(psNumeroParcela,1,2);
  end
  else if psTipoDoc = 'V' then
  begin
    lsNumeroParcela := dmDados.TirarMascara(psNumeroParcela);
  end
  else
  begin
//    lsNumeroParcela := '00' + Copy(psNumeroParcela,1,2);
    if Length(psNumeroParcela) < 6 then
      lsNumeroParcela := '00' + dmDados.TirarMascara(psNumeroParcela)
    else
      lsNumeroParcela := dmDados.TirarMascara(psNumeroParcela);
  end;

  aSqlParms[0] := 'Sum(Vlr_Receb) As Soma';
  if psTipoDoc = 'V' then
  begin
    aSqlParms[1] := 'Rec_Serv_Parc';
    aSqlParms[2] := 'CodigoReceber = ''' + psNumeroProposta + lsNumeroParcela + '''';
  end
  else
  begin
    aSqlParms[1] := 'Rec_Parc';
    aSqlParms[2] := 'CodigoReceber = ''' + psNumeroProposta + psTipoDoc + lsNumeroParcela + '''';
  end;
  lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);

  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;
  { se encontrou a op��o }
  if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
  begin
    liIndCol[0] := IOPTR_NOPOINTER;
    Result := dmDados.ValorColuna(liConexao, 'Soma', liIndCol[0]);
  end;
  dmDados.FecharConexao(liConexao);

end;

{*----------------------------------------------------------------------
 * TuntFuncoes.gf_Funcoes_CalculaIGPM
 *----------------------------------------------------------------------}
function TuntFuncoes.gf_Funcoes_CalculaIndice(const pnValor: Double;
                                              const pdDate: TDateTime;
                                              const psIndice: string): Double;
var
  liConexao: Integer;
  lsSql: string;
  ldDateInicial: TDateTime;
  ldDateFinal: TDateTime;
  liConta: Longint;
  lnPrevious: Double;
  lwDia, lwMes, lwAno: Word;
  liIndCol: array[0..0] of Integer;
begin

  try
    DecodeDate(pdDate, lwAno, lwMes, lwDia);
    ldDateInicial := pdDate - (lwDia - 1) + 14;
    DecodeDate(Date(), lwAno, lwMes, lwDia);
    ldDateFinal := Date() - (lwDia - 1) + 14;

    { se passou de 30 dias do vencimento calcula }
    if (Date() - pdDate) > DIAS_IGPM then
    begin
      lnPrevious := pnValor;
    end
    else
    begin
      Result := 0;
      Exit;
    end;

    if ldDateFinal <= ldDateInicial then
    begin
      Result := 0;
      Exit;
    end;

    gaParm[0] := 'IND_IGP';
    gaParm[1] := 'DATA_IGP >= ' + CTEDB_DATE_INI + FormatDateTime(MK_DATA, ldDateInicial) + CTEDB_DATE_FIM +
                 ' And TIPO = ''' + psIndice + '''';
    gaParm[2] := 'DATA_IGP';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    liIndCol[0] := IOPTR_NOPOINTER;
    while not dmDados.Status(liConexao, IOSTATUS_EOF) do
    begin
      lnPrevious := lnPrevious + ((dmDados.ValorColuna(liConexao,'Valor_IGP',liIndCol[0]) / 100) * lnPrevious);
      dmDados.Proximo(liConexao);
    end;
    dmDados.FecharConexao(liConexao);

    Result := lnPrevious - pnValor;

  except
    dmDados.ErroTratar('gf_Funcoes_CalculaIndice - uniFuncoes.pas');
  end;
end;

{*----------------------------------------------------------------------
 * TuntFuncoes.gf_Zero_Esquerda
 *----------------------------------------------------------------------}
function TuntFuncoes.gf_Zero_Esquerda(const psTexto: string;
                                      const liTamanho: Integer): string;
begin
  { coloca zeros a esquerda }
  Result := Copy(StringOfChar('0', liTamanho) + psTexto, Length(Trim(psTexto)) + 1, liTamanho);
end;

{*----------------------------------------------------------------------
 * TuntFuncoes.gf_Funcoes_Bloqueio
 *----------------------------------------------------------------------}
function TuntFuncoes.gf_Funcoes_Bloqueio(const psNumProp: string;
                                         const psTipoProp: string): Boolean;
var
  liConexao:  Integer;
  lsSql:      string;
  liIndCol:   array[0..2] of Integer;
  lsBloqueada: string;
begin

  Result := False;

  try
    gaParm[0] := 'PROPGAVE';
    gaParm[1] := 'NUM_PROP = ''' + psNumProp + '''';
    gaParm[2] := 'NUM_PROP';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    liIndCol[0] := IOPTR_NOPOINTER;
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
      lsBloqueada := dmDados.ValorColuna(liConexao, 'BLOQUEADA', liIndCol[0]);
    dmDados.FecharConexao(liConexao);

    if (lsBloqueada <> VAZIO) and (lsBloqueada <> 'B') then
    begin
      MessageDlg('Proposta Bloqueada [' + lsBloqueada + '] !',
               mtWarning, [mbOK], 0);
      Result := True;
    end;

  except
    dmDados.ErroTratar('gf_Funcoes_Bloqueio - uniFuncoes.pas');
  end;

end;

{*----------------------------------------------------------------------
 * TuntFuncoes.gf_Funcoes_ValidaBloq
 *----------------------------------------------------------------------}
function TuntFuncoes.gf_Funcoes_ValidaBloq(const psNumProp: string;
                                           const psTipoBloq: string;
                                           const psTipoProp: string): Boolean;
var
  liConexao:  Integer;
  lsSql:      string;
  liIndCol:   array[0..2] of Integer;
  lsBloqueada: string;
begin

  Result := False;

  try
    if (psTipoProp = 'L') or (psTipoProp = 'G') then
    begin
      if psTipoBloq <> VAZIO then
      begin
        {  com Ocupacao }
        if psTipoBloq[1] in ['C', 'R', 'S', 'T', 'D'] then
        begin
//          gaParm[0] := 'GAVEPESS';
//          gaParm[1] := 'Substring(CODIGO,1,8) = ''' + psNumProp + '''';
//          gaParm[2] := 'CODIGO';
//          lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
          gaParm[0] := 'GAVEPESS';
          gaParm[1] := psNumProp;
          lsSql := dmDados.SqlVersao('NEC_0024', gaParm);
          if dmDados.giStatus <> STATUS_OK then
            Exit;

          liConexao := dmDados.ExecutarSelect(lsSql);
          if liConexao = IOPTR_NOPOINTER then
            Exit;

          { achou }
          if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
          begin
            MessageDlg('Proposta [' + psNumProp + '] com Ocupa��o !',
                     mtWarning, [mbOK], 0);
            Result := True;
          end;
          dmDados.FecharConexao(liConexao);
        end;

        { sem Contrato Transformado }
        if psTipoBloq[1] in ['S', 'T'] then
        begin
          gaParm[0] := 'PROPTRNF';
          gaParm[1] := 'NUM_PROP = ''' + psNumProp + '''';
          gaParm[2] := 'NUM_PROP';
          lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
          if dmDados.giStatus <> STATUS_OK then
            Exit;

          liConexao := dmDados.ExecutarSelect(lsSql);
          if liConexao = IOPTR_NOPOINTER then
            Exit;

          { nao achou }
          if dmDados.Status(liConexao, IOSTATUS_NOROWS) then
          begin
            MessageDlg('Proposta [' + psNumProp + '] n�o possui Novo Contrato !',
                     mtWarning, [mbOK], 0);
            Result := True;
          end;
          dmDados.FecharConexao(liConexao);
        end;
      end;

      { com Saldo Devedor }
      if psTipoBloq = 'S' then
      begin
        gaParm[0] := 'RECEB';
        gaParm[1] := 'NUM_PROP = ''' + psNumProp + ''' AND (PAGTO = ''N'' OR PAGTO IS NULL)';
        gaParm[2] := 'CODIGO';
        lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
        if dmDados.giStatus <> STATUS_OK then
          Exit;

        liConexao := dmDados.ExecutarSelect(lsSql);
        if liConexao = IOPTR_NOPOINTER then
          Exit;

        { achou }
        if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
        begin
          MessageDlg('Proposta [' + psNumProp + '] com Saldo Devedor !',
                   mtWarning, [mbOK], 0);
          Result := True;
        end;
        dmDados.FecharConexao(liConexao);
      end;

      { com Reativacao de Gaveta sem Raetivar Lote }
      if ((psTipoBloq = VAZIO) or (psTipoBloq = 'B')) and (psTipoProp = 'G') then
      begin
        gaParm[0] := 'PROPGAVE';
        gaParm[1] := 'NUM_PROP = ''' + psNumProp + '''';
        gaParm[2] := 'NUM_PROP';
        lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
        if dmDados.giStatus <> STATUS_OK then
          Exit;

        liConexao := dmDados.ExecutarSelect(lsSql);
        if liConexao = IOPTR_NOPOINTER then
          Exit;

        { achou }
        liIndCol[0] := IOPTR_NOPOINTER;
        if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
          lsBloqueada := dmDados.ValorColuna(liConexao, 'BLOQUEADA', liIndCol[0]);

        if lsBloqueada <> VAZIO then
        begin
          if lsBloqueada[1] in ['C','D','R','T','S'] then
          begin
            gaParm[0] := 'PROPLOTE';
            gaParm[1] := 'NUM_PROP = ''' + psNumProp + '''';
            gaParm[2] := 'NUM_PROP';
            lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
            if dmDados.giStatus <> STATUS_OK then
              Exit;

            liConexao := dmDados.ExecutarSelect(lsSql);
            if liConexao = IOPTR_NOPOINTER then
              Exit;

            { achou }
            liIndCol[1] := IOPTR_NOPOINTER;
            if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
              lsBloqueada := dmDados.ValorColuna(liConexao, 'BLOQUEADA', liIndCol[1]);

            if lsBloqueada <> VAZIO then
            begin
              if lsBloqueada[1] in ['C','D','R','T','S'] then
              begin
                MessageDlg('N�o pode Reativar Gaveta [' + psNumProp + '] com Lote Bloqueado [' + lsBloqueada + '] !',
                         mtWarning, [mbOK], 0);
                Result := True;
              end;
            end;
            dmDados.FecharConexao(liConexao);
          end;
        end;
      end;
    end
    else
    begin
      if psTipoBloq <> VAZIO then
      begin
        {  com Ocupacao }
        if psTipoBloq[1] in ['C', 'R', 'S', 'T', 'D'] then
        begin
//          gaParm[0] := 'NICHOPES';
//          gaParm[1] := 'Substring(CODIGO,1,8) = ''' + psNumProp + '''';
//          gaParm[2] := 'CODIGO';
//          lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
          gaParm[0] := 'NICHOPES';
          gaParm[1] := psNumProp;
          lsSql := dmDados.SqlVersao('NEC_0024', gaParm);
          if dmDados.giStatus <> STATUS_OK then
            Exit;

          liConexao := dmDados.ExecutarSelect(lsSql);
          if liConexao = IOPTR_NOPOINTER then
            Exit;

          { achou }
          if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
          begin
            MessageDlg('Proposta [' + psNumProp + '] com Ocupa��o !',
                     mtWarning, [mbOK], 0);
            Result := True;
          end;
          dmDados.FecharConexao(liConexao);
        end;
      end;
    end;

  except
    dmDados.ErroTratar('gf_Funcoes_ValidaBloq - uniFuncoes.pas');
  end;

end;

{*----------------------------------------------------------------------
 * TuntFuncoes.gf_Funcoes_ProcuraLote
 *----------------------------------------------------------------------}
//
// Funcao que Verifica se O LOTE, QUADRA e SETOR cadastrados na
// proposta de Gavetas, se estao implantados e nao estao sendo
// utilizados por outra proposta.
//
function TuntFuncoes.gf_Funcoes_ProcuraLote(const psQuadra: string;
                                            const psSetor: string;
                                            const psLote: string;
                                            const psTipo: string;
                                            const psNumProp: string): Boolean;
var
  liConexao:  Integer;
  lsSql:      string;
  liIndCol:   array[0..2] of Integer;
  liGaveta:   Integer;
begin

  Result := True;

  gaParm[0] := 'TAB_GAVE';
  gaParm[1] := 'CODIGO = ''' + psTipo + '''';
  gaParm[2] := 'CODIGO';
  lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
  if dmDados.giStatus <> STATUS_OK then
    Exit;

  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;

  { achou }
  if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    liGaveta := dmDados.ValorColuna(liConexao, 'Gaveta', liIndCol[0])
  else
    liGaveta := 0;
  dmDados.FecharConexao(liConexao);

//  if (psTipo <> 'N1') and (psTipo <> 'N2') then
  if Trim(psLote) <> VAZIO then
  begin
    gaParm[0] := 'LOTE';
    gaParm[1] := 'CODIGO = ''' + Trim(psQuadra) + Trim(psSetor) + Trim(psLote) + '''';
    gaParm[2] := 'CODIGO';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    { nao achou }
    if dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      MessageDlg('Localizacao '+psQuadra+' - '+psSetor+' - '+psLote+' n�o Encontrada !!!',
                mtWarning, [mbOK], 0);
      Result := False;
      Exit;
    end
    else
    begin
      { pode ter mais de um tipo }
      if liGaveta = 0 then
      begin
        if dmDados.ValorColuna(liConexao, 'Tipo', liIndCol[0]) <> psTipo then
        begin
          MessageDlg('Tipo da Localizacao '+psTipo+' diferente do Contorno !!!',
                    mtWarning, [mbOK], 0);
          Result := False;
          Exit;
        end;
      end;
    end;
    dmDados.FecharConexao(liConexao);

    { pode ter mais de uma localizacao }
    if liGaveta = 0 then
    begin
      gaParm[0] := 'PROPGAVE';
      gaParm[1] := 'QUADRA = ''' + psQuadra + ''' and SETOR = ''' + psSetor +
                   ''' and LOTE = ''' + psLote + ''' and BLOQUEADA IS NULL'
                   + ' and NUM_PROP <> ''' + psNumProp +'''';
      gaParm[2] := 'NUM_PROP';
      lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;

      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      { achou }
      if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
      begin
        MessageDlg('Localizacao '+psQuadra+' - '+psSetor+' - '+psLote+' n�o Dispon�vel !!!',
                  mtWarning, [mbOK], 0);
        Result := False;
        Exit;
      end;
      dmDados.FecharConexao(liConexao);
    end;
  end;
end;

{*----------------------------------------------------------------------
 * TuntFuncoes.gf_Funcoes_Posiciona   Verifica se existe
 *----------------------------------------------------------------------}
function TuntFuncoes.gf_Funcoes_Posiciona(const psTabela: string;
                                          const psCampo: string;
                                          const psValor: string;
                                          const psTipo: string): Boolean;
var
  liConexao:  Integer;
  lsSql:      string;
  liIndCol:   array[0..2] of Integer;
begin

  Result := False;

  try
    gaParm[0] := '*';
    gaParm[1] := psTabela;
    case psTipo[1] of
      'T':
        gaParm[2] := psCampo + ' = ''' + psValor + '''';
      'N':
        gaParm[2] := psCampo + ' = ' + psValor;
    end;
    lsSql := dmDados.SqlVersao('NEC_0000', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    { nao achou }
    if dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      Result := True;
    end;
    dmDados.FecharConexao(liConexao);
  except
    dmDados.ErroTratar('gf_Funcoes_Posiciona - uniFuncoes.pas');
  end;
end;

{*----------------------------------------------------------------------
 * TuntFuncoes.gf_Funcoes_Duplicado   Verifica se esta duplicado
 *----------------------------------------------------------------------}
function TuntFuncoes.gf_Funcoes_Duplicado(const psTabela: string;
                                          const psCampo1: string;
                                          const psValor1: string;
                                          const psCampo2: string;
                                          const psValor2: string): Boolean;
var
  liConexao:  Integer;
  lsSql:      string;
  liIndCol:   array[0..2] of Integer;
begin

  Result := True;

  try
    gaParm[0] := '*';
    gaParm[1] := psTabela;
    if psValor1 <> VAZIO then
      gaParm[2] := psCampo1 + ' = ''' + psValor1 + '''';
    if psValor2 <> VAZIO then
    begin
      if gaParm[2] <> VAZIO then
        gaParm[2] := gaParm[2] + ' AND ';
      gaParm[2] := gaParm[2] + psCampo2 + ' = ''' + psValor2 + '''';
    end;

    { se tiver o que procurar }
    if psValor2 <> VAZIO then
    begin
      lsSql := dmDados.SqlVersao('NEC_0000', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;

      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      { se achou }
      if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
      begin
        Result := False;
      end;
      dmDados.FecharConexao(liConexao);
    end;
  except
    dmDados.ErroTratar('gf_Funcoes_Posiciona - uniFuncoes.pas');
  end;
end;

end.
