�
 TFRMGUIASEPULTAMENTO 0�  TPF0TfrmGuiaSepultamentofrmGuiaSepultamentoLeft`Top� Width�Height7CaptionGuias de SepultamentoColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	PositionpoScreenCenterVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidth�HeightTabOrder  TLabel	lblCodigoLeftTopWidth!HeightCaption   Código  TLabellblDescricaoLeft� TopWidth0HeightCaption   Descrição  TLabellblNumeroPropostaLeft8TopWidthFHeightCaptionNum. Proposta  TLabellblGuiaLeft�TopWidthMHeightCaption   Número da Guia  	TPaintBoxPntComunicadorLeftTop Width�Height	OnPaintPntComunicadorPaint  	TGroupBoxgrpGuiaLeftTop1Width�Height� TabOrder 	TGroupBoxgrpFalecidoLeftTopWidth�Height� CaptionFalecidoTabOrder  TLabellblSobrenomeLeft� TopWidth6HeightCaption	Sobrenome  TLabellblNomeLeftTopWidthHeightCaptionNome  TLabel
lblDataMovLeft8TopWidth;HeightCaptionData Sepult.  TLabellblIdadeLeft�TopWidthHeightCaptionIdade  TLabellblIdadeUnidadeLeft�TopWidth7HeightCaptionUnid. Idade  TLabellblSexoLeft TopWidthHeightCaptionSexo  TLabellblDataObitoLeft Top@WidthBHeightCaption   Data do Óbito  TLabellblEnderecoLeftTop@Width.HeightCaption	   Endereço  TLabellblNumeroObitoLeft�Top@WidthPHeightCaption   Número do Óbito  TLabellblCausaMortisLeftToppWidth=HeightCaptionCausa Mortis  TEditedtNomeLeftTop Width� HeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtSobrenomeLeft� Top WidthiHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEdit
mskDataMovLeft8Top WidthPHeightEditMask!99/99/0000;1; 	MaxLength
TabOrderText
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskIdadeLeft�Top Width1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtIdadeUnidadeLeft�Top Width9HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboSexoLeft Top WidthYHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataObitoLeft TopPWidthQHeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtEnderecoLeftTopPWidthyHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtNumeroObitoLeft�TopPWidthyHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtCausaMortisLeftTop� WidthaHeightTabOrder	OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress    	TMaskEdit	mskCodigoLeftTopWidthiHeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtDescricaoLeft� TopWidth1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskNumeroPropostaLeft8TopWidthPHeightEditMask9999999-9;1;_	MaxLength	TabOrderText	       - OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskGuiaLeft�TopWidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress    