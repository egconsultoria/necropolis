unit uniTABValorTaxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Variants,
  uniConst, StdCtrls, Mask;

const
  EDT_CODIGO: string = 'edtCodigo';

type
  TfrmTABValorTaxa = class(TForm)
    pnlFundo: TPanel;
    grpValorTaxa: TGroupBox;
    mskDataVigencia: TMaskEdit;
    grpTaxaVariavel: TGroupBox;
    mskVariavel_Numero1: TMaskEdit;
    lblVariavel_Numero1: TLabel;
    lblVariavel_Valor1: TLabel;
    mskVariavel_Valor1: TMaskEdit;
    mskVariavel_Numero2: TMaskEdit;
    lblVariavel_Valor2: TLabel;
    lblVariavel_Numero2: TLabel;
    mskVariavel_Valor2: TMaskEdit;
    grpTaxaFixas: TGroupBox;
    mskFixa_Numero1: TMaskEdit;
    lblFixa_Numero1: TLabel;
    mskFixa_Valor1: TMaskEdit;
    lblFixa_Valor1: TLabel;
    mskFixa_Numero2: TMaskEdit;
    lblFixa_Numero2: TLabel;
    lblFixa_Valor2: TLabel;
    mskFixa_Valor2: TMaskEdit;
    lblDataVigencia: TLabel;
    PntComunicador: TPaintBox;
    Label7: TLabel;
    edtCodigo: TEdit;
    Label21: TLabel;
    edtDescricao: TEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    mskVariavel_Numero3: TMaskEdit;
    mskVariavel_Valor3: TMaskEdit;
    mskVariavel_Numero4: TMaskEdit;
    mskVariavel_Valor4: TMaskEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure cboComboExit(Sender: TObject);
    procedure cboComboKeyPress(Sender: TObject; var Key: Char);
    procedure edtTextoChange(Sender: TObject);
    procedure edtTextoEnter(Sender: TObject);
    procedure edtTextoExit(Sender: TObject);
    procedure sprGridChange(Sender: TObject; Col, Row: Integer);
    procedure chkCheckClick(Sender: TObject);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;

  public
    { Public declarations }
  end;

var
  frmTABValorTaxa: TfrmTABValorTaxa;

implementation

{$R *.DFM}
uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos, uniFuncoes;

{*-----------------------------------------------------------------------------
 *  TfrmPadrao.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.FormClose(Sender: TObject; var Action: TCloseAction);

var
  liConta:  Integer;
begin
  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - FORMENDERECO.FRM',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;
    gfrmTV := nil;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uni.PAS');
  end;

  // destroi a janela
  Action := caFree;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPadrao.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, frmTABValorTaxa);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPadrao.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPadrao.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.FormResize(Sender: TObject);
begin
  if TfrmTABValorTaxa(Sender).WindowState <> wsMinimized Then
  begin
    if (TfrmTABValorTaxa(Sender).Width < (pnlFundo.Width + 39)) and (frmTABValorTaxa.WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TfrmTABValorTaxa(Sender).Width := pnlFundo.Width + 39;

    TfrmTABValorTaxa(Sender).Refresh;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    { redimensiona a tela}
//    Width  := 527;
//    Height := 276;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
//    mrTelaVar.P.Titulo_Tela := 'Tab. de Taxas';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
    untEdit.gp_Edit_EstufaCombos(mrTelaVar.P.Sigla_Tabela, TForm(Sender));

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);
    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uni');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 3);


end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  {' muda o ponteiro do mouse para ampulheta}
  Screen.cursor := crHOURGLASS;

  lcboCombo := TComboBox(Sender);

  {' se nao for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
  begin
    if lcboCombo.Text <> VAZIO then
    begin
      {' altera��o efetuada}
      untEdit.gp_Edit_Alterado(True, mrTelaVar);
    end;
  end
  else
    If (Not mrTelaVar.Reposicionando) Then
      {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

  {' retorna o ponteiro do mouse para o default}
  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TformAssinante.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  liConta: Integer;
begin

  lcboCombo := TComboBox(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := lcboCombo.Text;
  giAreaDesfaz := -1;
  if lcboCombo.Items.Count > 0 then
    {'Executa um la�o nos itens comparando-os com o pConteudo}
    for liConta := 0 to lcboCombo.Items.Count - 1 do
      if lcboCombo.Items[liConta] = gsAreaDesfaz then
      begin
        giAreaDesfaz := liConta;
        break;
      end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.cboComboKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.edtTextoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.edtTextoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = EDT_CODIGO) And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    {' atualiza o caption da janela}
    Caption := mrTelaVar.P.Titulo_Tela + ' ' + ledtEdit.Text + ' - Inclus�o';
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.edtTextoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.edtTextoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprGridChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.sprGridChange(Sender: TObject; Col,
  Row: Integer);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.chkCheckClick       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.chkCheckClick(Sender: TObject);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.PntComunicadorPaint(Sender: TObject);
begin

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, frmTABValorTaxa);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.cboComboExit(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPadrao.edtTextoExit       Procura pelo Proponente
 *
 *-----------------------------------------------------------------------------}
procedure TfrmTABValorTaxa.edtTextoExit(Sender: TObject);
var
  ledtEdit: TMaskEdit;
  i, j:  Integer;
begin

  ledtEdit := TMaskEdit(Sender);

  if Sender is TMaskEdit then
  begin
    { procura pela entidade }
    i := untTelaVar.gf_TelaVar_Retorna_Tabela(mrTelaVar.P.Sigla_Tabela);
    { se nao achou }
    if i = 0 then
      Exit;

    j := 1;
    { percorre todos os campos da tabela principal }
    while gaLista_Campos[i][j].sSigla_Tabela = gaLista_Parametros[i].Sigla_Tabela do
    begin
      if gaLista_Campos[i][j].sNome_Controle = ledtEdit.Name then
      begin
        if gaLista_Campos[i][j].sAlinha = ALINHA_ZERO then
          ledtEdit.Text := untFuncoes.gf_Zero_Esquerda(ledtEdit.Text, gaLista_Campos[i][j].iTamanho);
        break;
      end;
      j := j + 1;
    end;
  end;

  if mrTelaVar.Comando_MDI = ED_NOVO then
  begin
    if ledtEdit.Name = 'mskDataVigencia' then
    begin
      edtCodigo.Text := Copy(mskDataVigencia.Text,7,4) + '/' + Copy(mskDataVigencia.Text,4,2) + '/' + Copy(mskDataVigencia.Text,1,2);
      edtDescricao.Text := mskDataVigencia.Text;
    end;
  end;

end;


end.
