unit uniGeraArquivo;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls, Variants,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const

  NOSSO_NUMERO = 'NOSSONUMERO';
  MAX_TRANSACAO = 200;

type
 {*======================================================================
 *            RECORDS
 *======================================================================}
{*----------------------------------------------------------------------
 * Estrutura da Transacao
 *}
  TTransacao = Record
     lLayout:     LongInt;
     lCodigo:     LongInt;
     lPosicaoIni: LongInt;
     lPosicaoFim: LongInt;
     sTitulo:     string;
     iTamanho:    Integer;
     vConteudo:   Variant;
     sTipo:       string;
     bCompletar:  Boolean;
     bFimLinha:   Boolean;
     sSql:        AnsiString;
  end;

 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntGeraArquivo = class(TObject)
  private
    mtfArquivoTexto: TextFile;
    mlRegistro: LongInt;
    msSql: String;
    miConexaoLayout: Integer;
    miIndCol: Array[0..20] of Integer;
    miConexaoAux: Integer;
    maTransacao: Array[0..MAX_TRANSACAO] of TTransacao;

    procedure Envia(var ptfArquivoTexto: TextFile;
                        psConteudo: String;
                        piTamanho: Integer;
                        pbFimLinha: Boolean);
    procedure Replicate(var ptfArquivoTexto: TextFile;
                            psConteudo: String;
                            piTamanho: Integer;
                            pbFimLinha: Boolean);
    procedure lp_GravaNossoNumero(psCodigoRecebimento: String;
                                 pvConteudo: Variant);

    { Private declarations }
  public
    { Public declarations }
    function ZeraNumero(plNumero: LongInt;
                        piTamanho: Integer): String;
    procedure gp_GeraArquivo_Inicializar;
    procedure gp_GeraArquivo_Finalizar;
    procedure EmiteLinha1(psArquivo: String);
    procedure EmiteLinha9;
    procedure EmiteLinhaX(psCodigoRecebimento: String);

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untGeraArquivo: TuntGeraArquivo;

implementation

uses uniDados;

{*-----------------------------------------------------------------------------
 *  TuniGeraArquivo.gp_GeraArquivo_Inicializa
 *        Cria o objeto e inicializa variaveis
 *-----------------------------------------------------------------------------}
procedure TuntGeraArquivo.gp_GeraArquivo_Inicializar;
begin
  { cria o objeto}
  untGeraArquivo := TuntGeraArquivo.Create;
  glCodigoLayout := 1;
end;

{*-----------------------------------------------------------------------------
 *  TuntParam.gp_GeraArquivo_Finaliza
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
procedure TuntGeraArquivo.gp_GeraArquivo_Finalizar;
begin
  { destroi objeto}
  untGeraArquivo.Free;
end;

{*-----------------------------------------------------------------------------
 *  TuntGeraArquivo.EmiteLinha1       emite a primeira linha do arquivo
 *
 *-----------------------------------------------------------------------------}
procedure TuntGeraArquivo.EmiteLinha1(psArquivo : string);
var
  liTamanho: Integer;
  lbCompletar: Boolean;
  lvConteudo: Variant;
  lsTipo: String;
  lbFimLinha: Boolean;
  wContador: Word;
  liConta: Integer;
begin

    { Cria o Arquivocom o caminho definido em psArquivo}
//    AssignFile(mtfArquivoTexto, ExtractFilePath(Application.EXEName) + psArquivo);
    AssignFile(mtfArquivoTexto, psArquivo);
    { deixa o arquivo no modo escrita}
    Rewrite(mtfArquivoTexto);
    { inicializa o marcador de registro do arquivo texto}
    mlRegistro := 1;

    for wContador := 0 to High(miIndCol) do
      miIndCol[wContador] := IOPTR_NOPOINTER;

    { apenas para atualizar o contador}
    dmDados.SequenciaProxVal('Arquivo');

    {Zera o contador de registros do arquivo texto}
    gaParm[0] := 'Registro';
    msSql := dmDados.SqlVersao('SEQ_0000', gaParm);
    miConexaoAux := dmDados.ExecutarSelect(msSql);
    dmDados.gsRetorno := dmDados.AlterarColuna(miConexaoAux, 'Sequencia', miIndCol[8], 1);
    { faz a atualizacao}
    dmDados.gsRetorno := dmDados.Alterar(miConexaoAux, 'zSequencia', VAZIO);
    { fecha a conexao}
    dmDados.FecharConexao(miConexaoAux);
    miConexaoAux := 0;

    for wContador := 0 to High(miIndCol) do
      miIndCol[wContador] := IOPTR_NOPOINTER;

    gaParm[0] := glCodigoLayout;
    msSql := dmDados.SqlVersao('ARQ_0001', gaParm);
    miConexaoLayout := dmDados.ExecutarSelect(msSql);

    { enquanto nao for final de arquivo}
    while not dmDados.Status(miConexaoLayout, IOSTATUS_EOF) do
    begin

      { pega os dados da tabela de layout}
      liTamanho := dmDados.ValorColuna(miConexaoLayout, 'TAMANHO', miIndCol[0]);
      lbCompletar := dmDados.ValorColuna(miConexaoLayout, 'COMPLETAR', miIndCol[1]);
      msSql := dmDados.ValorColuna(miConexaoLayout, 'SQL', miIndCol[2]);
      lvConteudo := dmDados.ValorColuna(miConexaoLayout, 'CONTEUDO', miIndCol[4]);
      lsTipo := dmDados.ValorColuna(miConexaoLayout, 'TIPO', miIndCol[5]);
      lbFimLinha := dmDados.ValorColuna(miConexaoLayout, 'FIMLINHA', miIndCol[9]);

      { se for para completar}
      if lbCompletar then
      begin
        { abre a conexao para pegar o dado certo}
        miConexaoAux := dmDados.ExecutarSelect(msSql);
        { se achou}
        if not dmDados.Status(miConexaoAux, IOSTATUS_NOROWS) then
          lvConteudo := dmDados.ValorColuna(miConexaoAux, 'VALOR', miIndCol[6]);
        { fecha a conexao}
        dmDados.FecharConexao(miConexaoAux);
        miConexaoAux := 0;
      end;

      if lsTipo = 'N' then
        { grava a informacao no Arquivo texto}
        Envia(mtfArquivoTexto, ZeraNumero(StrToInt(VarToStr(lvConteudo)),liTamanho), liTamanho, lbFimLinha)
      else
        { grava a informacao no Arquivo texto}
        Envia(mtfArquivoTexto, VarToStr(lvConteudo), liTamanho, lbFimLinha);

      { proximo item do layout}
      dmDados.Proximo(miConexaoLayout);
    end;

    { apenas para atualizar o contador}
    dmDados.SequenciaProxVal('NNumeroC');

    { fecha a conexao}
    dmDados.FecharConexao(miConexaoLayout);
    miConexaoLayout := 0;

    { guarda as transacoes no vetor }
    for wContador := 0 to High(miIndCol) do
      miIndCol[wContador] := IOPTR_NOPOINTER;

    gaParm[0] := glCodigoLayout;
    msSql := dmDados.SqlVersao('ARQ_0002', gaParm);
    miConexaoLayout := dmDados.ExecutarSelect(msSql);

    liConta := 0;
    { enquanto nao for final de arquivo}
    while not dmDados.Status(miConexaoLayout, IOSTATUS_EOF) do
    begin

      { pega os dados da tabela de layout}
      maTransacao[liConta].lLayout := dmDados.ValorColuna(miConexaoLayout, 'CODIGOLAYOUT', miIndCol[0]);
      maTransacao[liConta].lCodigo := dmDados.ValorColuna(miConexaoLayout, 'CODIGO', miIndCol[1]);
      maTransacao[liConta].lPosicaoIni := dmDados.ValorColuna(miConexaoLayout, 'POSICAOINICIAL', miIndCol[2]);
      maTransacao[liConta].lPosicaoFim := dmDados.ValorColuna(miConexaoLayout, 'POSICAOFINAL', miIndCol[3]);
      maTransacao[liConta].sTitulo := dmDados.ValorColuna(miConexaoLayout, 'TITULO', miIndCol[4]);
      maTransacao[liConta].iTamanho := dmDados.ValorColuna(miConexaoLayout, 'TAMANHO', miIndCol[5]);
      maTransacao[liConta].bCompletar := dmDados.ValorColuna(miConexaoLayout, 'COMPLETAR', miIndCol[6]);
      maTransacao[liConta].sSql := dmDados.ValorColuna(miConexaoLayout, 'SQL', miIndCol[7]);
      maTransacao[liConta].vConteudo := dmDados.ValorColuna(miConexaoLayout, 'CONTEUDO', miIndCol[8]);
      maTransacao[liConta].sTipo := dmDados.ValorColuna(miConexaoLayout, 'TIPO', miIndCol[9]);
      maTransacao[liConta].bFimLinha := dmDados.ValorColuna(miConexaoLayout, 'FIMLINHA', miIndCol[10]);

      { proximo item do layout}
      dmDados.Proximo(miConexaoLayout);
      Inc(liConta);
    end;

    { fecha a conexao}
    dmDados.FecharConexao(miConexaoLayout);
    miConexaoLayout := 0;

end;

{*-----------------------------------------------------------------------------
 *  TuntGeraArquivo.EmiteLinhaX       emite as linhas com dados para arquivo
 *
 *-----------------------------------------------------------------------------}
procedure TuntGeraArquivo.EmiteLinhaX(psCodigoRecebimento: String);
var
  liTamanho: Integer;
  lbCompletar: Boolean;
  lvConteudo: Variant;
  lsTipo: String;
  lsTitulo: String;
  lbFimLinha: Boolean;
//  wContador: Word;
  liConta: Integer;
  liNumero: LongInt;
begin
(*
    for wContador := 0 to High(miIndCol) do
      miIndCol[wContador] := IOPTR_NOPOINTER;

    gaParm[0] := glCodigoLayout;
    msSql := dmDados.SqlVersao('ARQ_0002', gaParm);
    miConexaoLayout := dmDados.ExecutarSelect(msSql);

    { enquanto nao for final de arquivo}
    while not dmDados.Status(miConexaoLayout, IOSTATUS_EOF) do
    begin

      { pega os dados da tabela de layout}
      liTamanho := dmDados.ValorColuna(miConexaoLayout, 'TAMANHO', miIndCol[0]);
      lbCompletar := dmDados.ValorColuna(miConexaoLayout, 'COMPLETAR', miIndCol[1]);
      msSql := dmDados.ValorColuna(miConexaoLayout, 'SQL', miIndCol[2]);
      lvConteudo := dmDados.ValorColuna(miConexaoLayout, 'CONTEUDO', miIndCol[3]);
      lsTipo := dmDados.ValorColuna(miConexaoLayout, 'TIPO', miIndCol[4]);
      lbFimLinha := dmDados.ValorColuna(miConexaoLayout, 'FIMLINHA', miIndCol[5]);
      lsTitulo := dmDados.ValorColuna(miConexaoLayout, 'TITULO', miIndCol[6]);

      { se for para completar}
      if lbCompletar then
      begin
        { abre a conexao para pegar o dado certo}
        gaParm[0] := psCodigoRecebimento;
        msSql := dmDados.LiteralSubstituir(msSql,gaParm);
        miConexaoAux := dmDados.ExecutarSelect(msSql);
        { se achou}
        if not dmDados.Status(miConexaoAux, IOSTATUS_NOROWS) then
          lvConteudo := dmDados.ValorColuna(miConexaoAux, 'VALOR', miIndCol[7]);
        { fecha a conexao}
        dmDados.FecharConexao(miConexaoAux);
        miConexaoAux := 0;
      end;

      { se for nosso numero}
      if lsTitulo = NOSSO_NUMERO then
      begin
        lp_GravaNossoNumero(psCodigoRecebimento,VarToStr(lvConteudo));
        { apenas para atualizar o contador}
        lvConteudo := dmDados.SequenciaProxVal('NOSSO_NUM');
      end;

      if lsTipo = 'N' then
      begin
        if VarType(lvConteudo) = varString then
        begin
          if Trim(VarToStr(lvConteudo)) <> VAZIO then
            { grava a informacao no Arquivo texto}
            Envia(mtfArquivoTexto, ZeraNumero(StrToInt(VarToStr(lvConteudo)),liTamanho), liTamanho,lbFimLinha)
          else
            { grava a informacao no Arquivo texto}
            Envia(mtfArquivoTexto, ZeraNumero(0,liTamanho), liTamanho,lbFimLinha)
        end
        else
          { grava a informacao no Arquivo texto}
          Envia(mtfArquivoTexto, ZeraNumero(StrToInt(VarToStr(lvConteudo)),liTamanho), liTamanho,lbFimLinha);
      end
      else
        { grava a informacao no Arquivo texto}
        Envia(mtfArquivoTexto, VarToStr(lvConteudo), liTamanho, lbFimLinha);

      { proximo item do layout}
      dmDados.Proximo(miConexaoLayout);
    end;

    { fecha a conexao}
    dmDados.FecharConexao(miConexaoLayout);
    miConexaoLayout := 0;
*)
    for liConta := 0 to High(maTransacao) do
    begin
      { se nao tem mais item }
      if maTransacao[liConta].sTitulo = VAZIO then
        break;

      { pega os dados da tabela de layout}
      liTamanho := maTransacao[liConta].iTamanho;
      lbCompletar := maTransacao[liConta].bCompletar;
      msSql := maTransacao[liConta].sSql;
      lvConteudo := maTransacao[liConta].vConteudo;
      lsTipo := maTransacao[liConta].sTipo;
      lbFimLinha := maTransacao[liConta].bFimLinha;
      lsTitulo := maTransacao[liConta].sTitulo;

      { se for para completar}
      if lbCompletar then
      begin
        { abre a conexao para pegar o dado certo}
        gaParm[0] := psCodigoRecebimento;
        msSql := dmDados.LiteralSubstituir(msSql,gaParm);
        miConexaoAux := dmDados.ExecutarSelect(msSql);
        { se achou}
        miIndCol[0] := IOPTR_NOPOINTER;
        if not dmDados.Status(miConexaoAux, IOSTATUS_NOROWS) then
          lvConteudo := dmDados.ValorColuna(miConexaoAux, 'VALOR', miIndCol[0]);
        { fecha a conexao}
        dmDados.FecharConexao(miConexaoAux);
        miConexaoAux := 0;
      end;

      { se for nosso numero}
      if lsTitulo = NOSSO_NUMERO then
      begin
        lp_GravaNossoNumero(psCodigoRecebimento,VarToStr(lvConteudo));
        { apenas para atualizar o contador}
        lvConteudo := dmDados.SequenciaProxVal('NOSSO_NUM');
      end;

      if lsTipo = 'N' then
      begin
        if VarType(lvConteudo) = varString then
        begin
          if Trim(VarToStr(lvConteudo)) <> VAZIO then
          begin
            liNumero := LongInt(lvConteudo);
            { grava a informacao no Arquivo texto}
//            Envia(mtfArquivoTexto, ZeraNumero(StrToFloat(VarToStr(lvConteudo)),liTamanho), liTamanho, lbFimLinha)
            Envia(mtfArquivoTexto, ZeraNumero(liNumero,liTamanho), liTamanho, lbFimLinha);
          end
          else
          begin
            { grava a informacao no Arquivo texto}
            Envia(mtfArquivoTexto, ZeraNumero(0,liTamanho), liTamanho,lbFimLinha);
          end
        end
        else
        begin
          liNumero := LongInt(lvConteudo);
          { grava a informacao no Arquivo texto}
//          Envia(mtfArquivoTexto, ZeraNumero(StrToFloat(VarToStr(lvConteudo)),liTamanho), liTamanho, lbFimLinha);
          Envia(mtfArquivoTexto, ZeraNumero(liNumero,liTamanho), liTamanho, lbFimLinha);
        end
      end
      else
        { grava a informacao no Arquivo texto}
        Envia(mtfArquivoTexto, VarToStr(lvConteudo), liTamanho, lbFimLinha);

    end;

end;

{*-----------------------------------------------------------------------------
 *  TuntGeraArquivo.EmiteLinha9       emite a ultima linha para arquivo
 *
 *-----------------------------------------------------------------------------}
procedure TuntGeraArquivo.EmiteLinha9;
var
  liTamanho: Integer;
  lbCompletar: Boolean;
  lvConteudo: Variant;
  lsTipo: String;
  lbFimLinha: Boolean;
  wContador: Word;
begin

    for wContador := 0 to High(miIndCol) do
      miIndCol[wContador] := IOPTR_NOPOINTER;

    gaParm[0] := glCodigoLayout;
    msSql := dmDados.SqlVersao('ARQ_0003', gaParm);
    miConexaoLayout := dmDados.ExecutarSelect(msSql);

    { enquanto nao for final de arquivo}
    while not dmDados.Status(miConexaoLayout, IOSTATUS_EOF) do
    begin

      { pega os dados da tabela de layout}
      liTamanho := dmDados.ValorColuna(miConexaoLayout, 'TAMANHO', miIndCol[0]);
      lbCompletar := dmDados.ValorColuna(miConexaoLayout, 'COMPLETAR', miIndCol[1]);
      msSql := dmDados.ValorColuna(miConexaoLayout, 'SQL', miIndCol[2]);
      lvConteudo := dmDados.ValorColuna(miConexaoLayout, 'CONTEUDO', miIndCol[4]);
      lsTipo := dmDados.ValorColuna(miConexaoLayout, 'TIPO', miIndCol[5]);
      lbFimLinha := dmDados.ValorColuna(miConexaoLayout, 'FIMLINHA', miIndCol[9]);

      { se for para completar}
      if lbCompletar then
      begin
        { abre a conexao para pegar o dado certo}
        miConexaoAux := dmDados.ExecutarSelect(msSql);
        { se achou}
        if not dmDados.Status(miConexaoAux, IOSTATUS_NOROWS) then
          lvConteudo := dmDados.ValorColuna(miConexaoAux, 'VALOR', miIndCol[6]);
        { fecha a conexao}
        dmDados.FecharConexao(miConexaoAux);
        miConexaoAux := 0;
      end;

      if lsTipo = 'N' then
        { grava a informacao no Arquivo texto}
        Envia(mtfArquivoTexto, ZeraNumero(StrToInt(VarToStr(lvConteudo)),liTamanho), liTamanho, lbFimLinha)
      else
        { grava a informacao no Arquivo texto}
        Envia(mtfArquivoTexto, VarToStr(lvConteudo), liTamanho, lbFimLinha);

      { proximo item do layout}
      dmDados.Proximo(miConexaoLayout);
    end;

    { fecha a conexao}
    dmDados.FecharConexao(miConexaoLayout);
    miConexaoLayout := 0;

    { fecha o arquivo}
    CloseFile(mtfArquivoTexto);

end;

{*-----------------------------------------------------------------------------
 *  TuntGeraArquivo.Envia       Preenche o campo com o tamanho certo
 *
 *-----------------------------------------------------------------------------}
procedure TuntGeraArquivo.Envia(var ptfArquivoTexto: TextFile;
                                psConteudo: String;
                                piTamanho: Integer;
                                pbFimLinha: Boolean);
var
  liTamanhoConteudo: integer;
begin
  { pega o tamanho do Conteudo}
  liTamanhoConteudo := Length(psConteudo);
  { se o tamanho do conteudo for menor do q o espaco no arquivo}
  if liTamanhoConteudo < piTamanho then
  begin
    { grava a informacao}
    Write(ptfArquivoTexto, psConteudo);
    { preenche o resto com espaco}
    Replicate(ptfArquivoTexto, ' ', piTamanho - liTamanhoConteudo, pbFimLinha);
  end
  else
  begin
    { se o tamanho for exato}
    if liTamanhoConteudo = piTamanho then
    begin
      { se for final de linha}
      if pbFimLinha then
      begin
        { grava vazio e se posiciona na proxima linha}
        WriteLn(ptfArquivoTexto, psConteudo);
        Inc(mlRegistro);
        { apenas para atualizar o contador}
        dmDados.SequenciaProxVal('Registro');
      end
      else
        { grava a informacao}
        Write(ptfArquivoTexto, psConteudo);
    end
    else
    begin
      { se for final de linha}
      if pbFimLinha then
      begin
        { trunca a informacao passa e grava no arquivo}
        WriteLn(ptfArquivoTexto, Copy(psConteudo, 1, piTamanho));
        Inc(mlRegistro);
        { apenas para atualizar o contador}
        dmDados.SequenciaProxVal('Registro');
      end
      else
        { trunca a informacao passa e grava no arquivo}
        Write(ptfArquivoTexto, Copy(psConteudo, 1, piTamanho));
    end;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TuntGeraArquivo.Replicate       Replica a informacao passada
 *
 *-----------------------------------------------------------------------------}
procedure TuntGeraArquivo.Replicate(var ptfArquivoTexto: TextFile;
                                    psConteudo: String;
                                    piTamanho: Integer;
                                    pbFimLinha: Boolean);
var
  liConta: Integer;
begin
  { Ate atingir o tamanho}
  for liConta := 1 to piTamanho do
  begin
    { se for no ultimo loop e for final de linha}
    if (liConta = piTamanho) and pbFimLinha then
    begin
      { grava a informacao no arquivo e vai p/ a proxima linha}
      WriteLn(ptfArquivoTexto, psConteudo);
      Inc(mlRegistro);
      { apenas para atualizar o contador}
      dmDados.SequenciaProxVal('Registro');
    end
    else
      { grava a informacao no arquivo}
      Write(ptfArquivoTexto, psConteudo);
  end;
end;

{*-----------------------------------------------------------------------------
 *  TuntGeraArquivo.ZeraNumero       Acrescenta zero ate chegar no tamanho
 *
 *-----------------------------------------------------------------------------}
function TuntGeraArquivo.ZeraNumero(plNumero: LongInt; piTamanho: Integer): String;
var
  lsNumero: String;
  liConta: Integer;
begin
  { transforma o numero em texto com o tamanho desejado}
  lsNumero := Format('%'+IntToStr(piTamanho)+'d', [plNumero]);
  { varre toda a string}
  for liConta := 1 to Length(lsNumero) do
    { se tiver vazio}
    if lsNumero[liConta] = ' ' then
      { muda por zero}
      lsNumero[liConta] := '0';
  { retorna a string com zeros}
  Result := lsNumero;

end;

{*-----------------------------------------------------------------------------
 *  TuntGeraArquivo.lp_GravaNossoNumero     Grava o NossoNumero na Tabela recebimento
 *
 *-----------------------------------------------------------------------------}
procedure TuntGeraArquivo.lp_GravaNossoNumero(psCodigoRecebimento: String;
                                              pvConteudo: Variant);
var
  liIndCol: Integer;
  liConexao: Integer;
begin

//    gaParm[0] := 'Codigo, Num_Doc';
//    gaParm[1] := 'Receb ';
//    gaParm[2] := 'Codigo = ' + psCodigoRecebimento;
    gaParm[0] := psCodigoRecebimento;
    msSql := dmDados.SqlVersao('ARQ_0009', gaParm);
    liConexao := dmDados.ExecutarSelect(msSql);
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin

      { altera as colunas}
      dmDados.gsRetorno := dmDados.AtivaModo(liConexao,IOMODE_EDIT);

      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'Num_Doc', liIndCol, pvConteudo);

      { faz a atualizacao}
      dmDados.gsRetorno := dmDados.Alterar(liConexao, 'Receb', VAZIO);
    end;
    {fecha a conexao}
    dmDados.FecharConexao(liConexao);
end;

end.
