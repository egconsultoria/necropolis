�
 TFRMCHEQUEDEVOLVIDO 0�  TPF0TfrmChequeDevolvidofrmChequeDevolvidoLeft[Top� Width�Height+CaptionCheques DevolvidosColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	PositionpoScreenCenterVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidthqHeightTabOrder  TLabel	lblCodigoLeftTopWidth!HeightCaption   Código  TLabellblDescricaoLeft� TopWidth0HeightCaption   Descrição  	TPaintBoxPntComunicadorLeftTop WidthiHeightOnPaintPntComunicadorPaint  	TGroupBoxgrpPropostaLeftTop1WidthdHeight� TabOrder TLabellblDataLancamentoLeftTopWidthUHeightCaption   Data Lançamento  TLabellblNumeroPropostaLeftpTopWidthFHeightCaptionNum. Proposta  TLabellblSituacaoLeft8TopWidth*HeightCaption
   Situação  TLabellblDataBaixaLeft�TopWidthCHeightCaptionData da Baixa  TLabellblTipoProdutoLeft� TopWidthLHeightCaptionTipo do Produto  	TMaskEditmskDataLancamentoLeftTop WidthPHeightEditMask!99/99/0000;1; 	MaxLength
TabOrder Text
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TGroupBoxgrpValorLeft(Top@Width1HeightyTabOrder TLabellblValorChequeLeft� TopWidthOHeightCaptionValor do Cheque  TLabellblDataChequeLeftTopWidthNHeightCaptionData do Cheque  TLabel	lblAlineaLeft� TopWidthHeightCaptionAlinea  TLabellblTipoCobrancaLeftTop@WidthFHeightCaption   Tipo Cobrança  	TMaskEditmskDataChequeLeftTop WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrder Text  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorChequeLeft� Top WidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEdit	mskAlineaLeft� Top Width1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboTipoCobrancaLeftTopPWidthYHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress   	TGroupBox	grpChequeLeftTop@Width	HeightyCaptionChequeTabOrder TLabellblBancoLeftTopWidthHeightCaptionBanco  TLabellblEmitenteLeftTopHWidth)HeightCaptionEmitente  TLabel
lblAgenciaLeftXTopWidth'HeightCaption   Agência  TLabellblNumeroChequeLeft� TopWidthAHeightCaptionNum. Cheque  	TMaskEditmskBancoLeftTop(Width1HeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtEmitenteLeftTopXWidth� HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEdit
mskAgenciaLeftXTop(WidthAHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskNumeroChequeLeft� Top(WidthIHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TMaskEditmskNumeroPropostaLeftpTop WidthPHeightEditMask9999999-9;1;_	MaxLength	TabOrderText	       - OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboSituacaoLeft8Top WidthiHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TComboBoxcboTipoProdutoLeft� Top WidthYHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataBaixaLeft�Top WidthPHeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TMaskEdit	mskCodigoLeftTopWidth� HeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtDescricaoLeft� TopWidth1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress    