unit uniGrafico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, TeEngine, Series, ExtCtrls, TeeProcs, Chart, DBChart,
  QrTee, quickrpt;

type
  TdlgGrafico = class(TForm)
    qrGrafico: TQuickRep;
    qrcGrafico: TQRChart;
    qrdbcGrafico: TQRDBChart;
    qrcsGrafico: TBarSeries;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgGrafico: TdlgGrafico;

implementation

uses uniConst, uniDados, uniGlobal;

{$R *.DFM}

end.
