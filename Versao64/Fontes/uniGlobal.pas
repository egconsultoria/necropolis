unit uniGlobal;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, DB, DBTables,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const

  { banco}
  DATABASE_NAME = 'Necropolis';  { Alias do Banco de dados }
  ARQUIVOS_DIR  = 'C:\SISTEMAS\NECROPOLIS\'; { Diretorios para arquivo textos}
  NOME_DRIVER = '';
  TEXTO_USER  = 'UID';
  USER_NAME   = '';
  TEXTO_PASS  = 'PWD';
  PASSWORD    = '';
  DB_PATH = 'C:\SISTEMAS\NECROPOLIS\';
  DB_CONNECT = 'DSN=Necropolis;UID=sa;Pwd=;DataBase=Necropolis';
  GR_DRV = 'C:\SISTEMAS\EPICA\GENDRV.EXE';
  INI_NOME = 'Necropolis.INI';
  DB_TIPO_ACCESS = 'MS_ACCESS';
  DB_TIPO_SQLSERVER = 'MS_SQL';
  DB_TIPO_ORACLE = 'OR_SERVER';

  { Tela do About}
  MDI_CAPTION = 'Necropolis';
  MDI_SUB_CAPTION = 'Sistema de Gest�o de Cemit�rios';
  LBL_VERSAO = 'Vers�o: 2.00';
  LBL_EMPRESA = 'Don Orione Empreendimentos Ltda';   //'<Empresa Cliente>';
  NOME_SISTEMA = 'Necropolis';

  {'Constantes utilizadas para as Tabelas de Op��es do Sistema}
  TAB_PREFIXO = '';
  SIGLA_TABELAS = 'EpTab';
  SIGLA_SEGURANCA = 'SEG';
  FLDCMB_CODIGO     = 'Codigo';
  FLDCMB_DESCRICAO  = 'Descricao';

  { constante de tamanho dos vetores }
  MAX_NUM_TEMREG = 20;

  { indice das telas para selecao}
  IND_PROPOSTA_LOTE     = 1;
  IND_PROPOSTA_GAVETA   = 2;
  IND_PROPOSTA_NICHO    = 3;
  IND_PROPOSTA_REMIDO   = 4;
  IND_CORRETOR          = 5;
  IND_BANCO             = 6;
  IND_PROPONENTES       = 29;
  IND_RCB_ENTRADAMANUAL = 8;
  IND_CONTORNO          = 9;
  IND_OCUPGAVETA        =21;
  IND_OCUPNICHO         =22;

  IND_TAB_PLANOS        =26;

  IND_LOTEESPE          =40;
  IND_GUIASEULPT        =41;
  IND_CHEQUEDEVOL       =42;
  IND_METACOMISSAO      =43;
  IND_AGENDA            =45;
  IND_REC_SERV          =46;
  IND_REC_SEGU          =48;
  IND_PLACAS            =53;
  IND_PROPONICHO        =54;
  IND_SERVICO           =68;

  IND_REPLANEJAMENTO    =62;

  IND_PAGAR             =70;
  IND_TALAO             =71;

  IND_LIVROSEP          =73;

  IND_CHEQUEPRE         =76;

  IND_TAB_EMPRESA       = 400;
  IND_TAB_VENDEDOR      = 401;
  IND_TAB_TIPOLOTE      = 402;
  IND_TAB_TIPOGAVETA    = 403;
  IND_TAB_MOTIVO        = 404;
  IND_TAB_TAXA          = 405;
  IND_TAB_OPERADOR      = 406;
  IND_TAB_COMGAVETA     = 409;

  IND_TAB_IGP           = 410;
  IND_TAB_URV           = 147;

  IND_BLOQ_LOTE         = 411;
  IND_REAT_LOTE         = 412;
  IND_BLOQ_GAVE         = 413;
  IND_REAT_GAVE         = 414;
  IND_BLOQ_NICH         = 415;
  IND_REAT_NICH         = 416;

  IND_ETQ_ESPEC         = 417;
  IND_TAB_SERV          = 418;
  IND_TAB_MENS          = 420;
  IND_TAB_CARTA         = 421;

  IND_TAB_DESP          = 430;
  IND_TAB_FORN          = 431;

  IND_Z_CONFIG          = 901;
  IND_Z_LOG             = 902;

  { constantes com codigos de erro}
  COD_ERROINVALIDO  = 4000;
  COD_ERROVAZIO     = 4001;
  COD_ERRODUPLI     = 4009;
  COD_ERROCGCCPF    = 4005;
  COD_ERROTOTAL     = 4016;
  COD_ERROITEM      = 4017;
  COD_ERRODATAINVALIDA = 19;
  COD_ERROINFINVALIDA = 4002;

  { constantes p/ a tela de Situacao financeira
  (estao definido na tabela de tipoRecebimento)}
  RECEBIMEMTO_ADESAO      = '1';
  RECEBIMEMTO_MENSALIDADE = '2';
  RECEBIMEMTO_MATERIAL    = '3';
  RECEBIMEMTO_REVISTA     = '4';
  MSG_FINAN_EMDIA         = 'Em Dia ';
  MSG_FINAN_ABRE_EMATRASO = 'Em Atraso [ ';
  MSG_FINAN_FECHA_EMATRASO = ' ]';
  MSG_FINAN_ADESAO        = 'Ade.: ';
  MSG_FINAN_MENSALIDADE   = 'Mens.: ';
  MSG_FINAN_MATERIAL      = 'Mat.: ';
  MSG_FINAN_REVISTA       = 'Rev.: ';

  {rotinas}
  CAD_ROT_CONTORNO = 'U';
  CAD_INC_QUADRA = 'V';

{*======================================================================
 *            NOMES DAS TABELAS
 *======================================================================}
 TABELA_TIPOPROPOSTA = 'TipoProposta';
 TABELA_LAYOUT = 'zLayout';
 TABELA_TRECEB = 'wTipoDocumento';

{*======================================================================
 *            NOMES DOS CONTROLES
 *======================================================================}
{*----------------------------------------------------------------------
 * Constantes de Proposta de Lote
 *}
 CBO_PLANO_LOTE_PLT        = 'cboPlano_Lote';
 CBO_PLANO_CODIGO_PLT      = 'cboPlano_Codigo';
{*----------------------------------------------------------------------
 * Constantes de Proposta de Gaveta
 *}
 CBO_PLANO_GAVE_PGV        = 'cboTipoGaveta';
 CBO_PLANO_CODIGO_PGV      = 'cboCodigoPlano';

{*----------------------------------------------------------------------
 * Constantes de Proposta de Nicho
 *}
 CBO_PLANO_CODIGO_PNI      = 'cboCodigoPlano';

{*----------------------------------------------------------------------
 * Constantes de Proposta de Remido
 *}
 CBO_PLANO_LOTE_PRE        = 'cboTipoLote';
 CBO_PLANO_CODIGO_PRE      = 'cboCodigoPlano';

{*----------------------------------------------------------------------
 * Constantes de Projeto Contorno Cemit�rio
 *}
  CBO_CEMITERIO_PCC        = 'cboCemiterio';
  CBO_QUADRA_PCC           = 'cboQuadra';
  CBO_SETOR_PCC            = 'cboSetor';
  CBO_TIPOLOTE_PCC         = 'cboTipoLote';
  CBO_NUMEROLOTE_PCC       = 'cboNumeroLote';

{*----------------------------------------------------------------------
 * Constantes de Valores das Taxas
 *}
  MSK_DATAVIGENCIA_VLT = 'mskDataVigencia';
  MSK_UNICA_NUMERO1_VLT = 'mskVariavel_Numero1';
  MSK_UNICA_VALOR_VLT = 'mskVariavel_Valor1';
  MSK_UNICA_NUMERO2_VLT = 'mskVariavel_Numero2';
  MSK_UNICA_DESCONTO_VLT = 'mskVariavel_Valor2';
  MSK_SEMESTRAL_NUMERO1_VLT = 'mskFixa_Numero1';
  MSK_SEMESTRAL_VALOR_VLT = 'mskFixa_Valor1';
  MSK_SEMESTRAL_NUMERO2_VLT = 'mskFixa_Numero2';
  MSK_SEMESTRAL_DESCONTO_VLT = 'mskFixa_Valor2';
  MSK_BIMESTRAL_NUMERO1_VLT = 'mskVariavel_Numero3';
  MSK_BIMESTRAL_VALOR_VLT = 'mskVariavel_Valor3';
  MSK_BIMESTRAL_NUMERO2_VLT = 'mskVariavel_Numero4';
  MSK_BIMESTRAL_DESCONTO_VLT = 'mskVariavel_Valor4';

  CAMPO_DATAVIGENCIA = 'Data_Taxa';
  CAMPO_UNICA_NUMERO1 = 'Num_Taxa1';
  CAMPO_UNICA_VALOR = 'Vlr_TaxVar';
  CAMPO_UNICA_NUMERO2 = 'Num_Taxa2';
  CAMPO_UNICA_DESCONTO = 'Vlr_TaxVr2';
  CAMPO_SEMESTRAL_NUMERO1 = 'Num_Taxa3';
  CAMPO_SEMESTRAL_VALOR = 'Vlr_TaxFx1';
  CAMPO_SEMESTRAL_NUMERO2 = 'Num_Taxa4';
  CAMPO_SEMESTRAL_DESCONTO = 'Vlr_TaxFx2';
  CAMPO_BIMESTRAL_NUMERO1 = 'Num_Taxa5';
  CAMPO_BIMESTRAL_VALOR = 'Vlr_TaxVr3';
  CAMPO_BIMESTRAL_NUMERO2 = 'Num_Taxa6';
  CAMPO_BIMESTRAL_DESCONTO = 'Vlr_TaxVr4';

{*----------------------------------------------------------------------
 * Constantes de Tipos de Lote
 *}
  EDT_CODIGO_TLT           = 'edtCodigo';
  EDT_DESCRICAO_TLT        = 'edtDescricao';
  MSK_REFERENCIA_STD_TLT   = 'mskTaxaManutencao';
  MSK_TAXAMANUTENCAO_TLT   = 'mskReferencia_STD';

{*----------------------------------------------------------------------
 * Constantes de Tipos de Gaveta
 *}
  EDT_CODIGO_TGV           = 'edtCodigo';
  EDT_DESCRICAO_TGV        = 'edtDescricao';
  MSK_REFERENCIA_STD_TGV   = 'mskTaxaManutencao';
  MSK_TAXAMANUTENCAO_TGV   = 'mskReferencia_STD';

{*----------------------------------------------------------------------
 * Constantes de Empresas
 *}
  EDT_CODIGO_EMP           = 'edtCodigo';
  EDT_DESCRICAO_EMP        = 'edtDescricao';

{*----------------------------------------------------------------------
 * Constantes de Corretores
 *}
  EDT_NOME_COR             = 'edtNome';
  EDT_CODIGO               = 'edtCodigo';
  CBO_TIPOPROPOSTA         = 'cboTipoProposta';
  CBO_ATENDIMENTO          = 'cboAtendimento';
  CHK_COMISSAO             = 'chkComissao';
  EDT_EMPRESA_NUMPESSOAS   = 'edtEmpresa_NumPessoas';
  CBO_EMPRESA_NOME         = 'cboEmpresa_Nome';
  SPR_CORRASSOC            = 'sprAssociados';

{*----------------------------------------------------------------------
 * Constantes de Vendedores
 *}
  EDT_CODIGO_VEN           = 'edtCodigo';
  EDT_DESCRICAO_VEN        = 'edtNome';

{*----------------------------------------------------------------------
 * Constantes de Operadores
 *}
  EDT_CODIGO_OPE           = 'edtCodigo';
  EDT_DESCRICAO_OPE        = 'edtNome';

{*----------------------------------------------------------------------
 * Constantes de Comiss�o de Gaveta
 *}
  MSK_DATAREFERENCIA_CGV   = 'mskDataReferencia';
  MSK_PERCENTUAL_CGV       = 'mskPercentual';
  SPR_COMISSAO_CGV         = 'sprComissao';

  COL_PONTOS               = 1;
  COL_TIPOGAVETA           = 1;
  COL_VALOR                = 1;

{*----------------------------------------------------------------------
 * Constantes de Motivo
 *}
  EDT_CODIGO_MOT           = 'edtCodigo';
  EDT_DESCRICAO_MOT        = 'edtMotivo';
  CBO_TIPOMOTIVO_MOT       = 'cboTipoMotivo';

{*----------------------------------------------------------------------
 * Constantes de Tabelas que usam Codigo - Descricao
 *}
  EDT_CODIGO_TCD           = 'edtCodigo';
  EDT_DESCRICAO_TCD        = 'edtDescricao';

{*----------------------------------------------------------------------
 * Constantes de Tabelas que usam Codigo - Descricao - Abreviatura
 *}
  EDT_CODIGO_TCDA           = 'edtCodigo';
  EDT_DESCRICAO_TCDA        = 'edtDescricao';
  EDT_ABREVIATURA_TCDA      = 'edtAbreviatura';

{*----------------------------------------------------------------------
 * Constantes de Tabelas que usam Codigo - Descricao - Valor
 *}
  EDT_CODIGO_TCDV           = 'edtCodigo';
  EDT_DESCRICAO_TCDV        = 'edtDescricao';
  EDT_VALOR_TCDV            = 'edtValor';

{*======================================================================
 *            RECORDS
 *======================================================================}

{*======================================================================
 *            CLASSES
 *======================================================================}

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  { variaveis para edi��o }
  gbAlterado: Boolean;
  gbErro:     Boolean;
  gsAreaDesfaz: String;
  giAreaDesfaz: Integer;
  gsNumProposta: String;

implementation

end.
