�
 TFRMRECEBSERVICO 0�&  TPF0TfrmRecebServicofrmRecebServicoLeft
TopWidthHeight�CaptionRecebimento de Servi�osFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildPositionpoScreenCenterVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeft Top Width�HeightyTabOrder  TLabelLabel20LeftTopWidth!HeightCaptionC�digo  TLabelLabel21Left� TopWidth0HeightCaption	Descri��o  	TPaintBoxPntComunicadorLeftTopWidth�HeightqOnPaintPntComunicadorPaint  TLabel	lblReciboLeftHTopWidth"HeightCaptionRecibo  	TGroupBox
grpServicoLeftTop0Width�HeightBTabOrder TLabelLabel1LeftTopWidthUHeightCaptionData Lan�amento  TLabelLabel4LeftpTopWidthFHeightCaptionNum. Proposta  TLabelLabel3Left8TopWidthOHeightCaptionTipo DocumentoVisible  TLabelLabel5Left�TopWidth@HeightCaptionNum. Parcela  TLabelLabel2Left� TopWidth$HeightCaptionServi�o  TLabelLabel27LeftTopWidthWHeightCaptionTipo Recebimento  	TMaskEditmskDataLancamentoLeftTop WidthPHeightEditMask!99/99/0000;1; 	MaxLength
TabOrder Text
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TGroupBoxgrpRecebimentoLeftTop� Width9HeightyTabOrder TLabelLabel14LeftTopWidthYHeightCaptionData Recebimento  TLabelLabel15LeftoTopWidthJHeightCaptionData de Credito  TLabelLabel16Left� TopWidthMHeightCaptionValor a Receber  TLabelLabel17Left	TopHWidthXHeightCaptionValor de Desconto  TLabelLabel18LeftpTopHWidthIHeightCaptionValor Recebido  TLabelLabel19Left� TopHWidthGHeightCaptionSaldo Devedor  	TMaskEditmskValorReceberLeft� Top(WidthQHeightEnabledTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorDescontoLeftTopXWidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorRecebidoLeftpTopXWidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskSaldoDevedorLeft� TopXWidthQHeightEnabledTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataRecebimentoLeftTop(WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrder Text  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataCreditoLeftpTop(WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TGroupBoxgrpRecebParcialLeftXTop� Width�HeightyTabOrder	 	TvaSpreadsprRecebParcialLeftTopWidthyHeightaTabOrder 
OnKeyPresscboComboKeyPressOnChangesprGridChangeOnEditChangesprGridEditChangeControlData
0     �&  
  @                         R������ � K�Q   �DB MS Sans Serif��� ���    
                      �  �  �c  c         
   B          �����������             ? 2    ����        �             MS Sans Serif     ��������        ����MbP?%      �  �  �  �*    ���������   <      E    q   r   s   F    ����������������G     Q =    �    �  �9          �            MS Sans Serif I      J    P    K                       @0        �����          ����K               {�G�z@,       ���� A     ���    K        + -      ���� A           �����חAR . , K        ) &      ���� R e /    4  K        + -      ���� A           �����חAR . , K                      @) &      ���� R e /  l  4  K               ��Q��3@K        M       ����    �   K        M       ����   ����  L         0    ����    �   8           Parcela 8           Valor Venc 8           Data Venc 8           Valor Rec. 8           Data Rec. 8           Hist�rico 8           Tipo Cobr. 8           Tipo Rec. L        "             %@L        L        L        L        L        L        L        L    	    L    
            ����                         �           	TGroupBoxgrpValorLeftXTop@Width�HeightyTabOrder TLabelLabel7LeftxTopWidth>HeightCaptionValor Original  TLabelLabel8LeftTopWidthaHeightCaptionData de Vencimento  TLabelLabel10LeftTop@WidthHeightCaptionJuros  TLabelLabel9LeftxTop@WidthHeightCaptionMulta  TLabelLabel13Left� Top@Width:HeightCaptionRec. Parcial  TLabelLabel22Left@TopWidthHeightCaptionCx/Bc  TLabelLabel23LefthTopWidthHeightCaptionPagto  TLabelLabel26Left� TopWidthFHeightCaptionTipo Cobran�a  	TMaskEditmskVencimentoLeftTop WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrder Text  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorOriginalLeftxTop WidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskJurosLeftTopPWidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskMultaLeftxTopPWidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskRecebParcialLeft� TopPWidth9HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtCaixaBancoLeft@Top Width!HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtPagtoLefthTop Width!HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboTipoCobrancaLeft� Top WidthYHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress   	TGroupBoxgrpDocumentoLeftTop@Width9HeightyCaption Documento TabOrder TLabelLabel6LeftTopWidth7HeightCaption	Documento  TLabelLabel11LeftTopHWidth:HeightCaption
Observa��o  TLabelLabel12Left� TopWidthFHeightCaptionNosso N�mero  	TMaskEditmskDocumentoLeftTop(WidthyHeightEditMask9999999-9\/999.9999;1;_	MaxLengthTabOrder Text       - /   .    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtHistoricoLeftTopXWidthHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtNossoNumeroLeft� Top(WidthiHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TMaskEditmskNumeroPropostaLeftpTop WidthPHeightEditMask9999999-9;0;_	MaxLength	TabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboTipoDocumentoLeft8Top WidthiHeight
ItemHeightSorted	TabOrderVisibleOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TComboBox
cboServicoLeft� Top Width� Height
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskNumeroParcelaLeft�Top WidthPHeightEditMask99;1;_	MaxLengthTabOrderText  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TGroupBoxgrpBloqueadaLeftpTopWidthyHeightACaption	BloqueadaTabOrder
Visible TLabelLabel24LeftTopWidthHeightCaptionTipo:  TLabelLabel25LeftTop(WidthHeightCaptionData:  	TComboBoxcboBloqueadaLeft)TopWidthHHeightEnabled
ItemHeightSorted	TabOrder OnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataBloqueadaLeft(Top(WidthIHeightEnabledEditMask99/99/9999;1; 	MaxLength
TabOrderText
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TComboBoxcboTipoRecebimentoLeftTop WidthYHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress   	TMaskEdit	mskCodigoLeftTopWidth� HeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtDescricaoLeft� TopWidth1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TCheckBoxchkNaoClienteLeft�TopWidthPHeightCaptionN�o ClienteTabOrderOnClickchkNaoClienteClick
OnKeyPresscboComboKeyPress  TEdit	edtReciboLeftHTopWidthIHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress    