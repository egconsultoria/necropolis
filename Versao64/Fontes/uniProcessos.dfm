�
 TFORMPROCESSOS 0�4 TPF0TformProcessosformProcessosLeftToprBorderIcons BorderStylebsSingleCaption   Rotinas de AtualizaçãoClientHeight:ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlQuitarParcelasLeftTopXWidth�Height�CaptionpnlQuitarParcelasTabOrder TLabelLabel124Left� TopPWidth3HeightCaption	No ReciboFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel125Left� TopPWidth6HeightCaptionVlr DinheiroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel126Left@TopPWidth-HeightCaption	Qtd Parc.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TGroupBoxgrpPagamentosQuitarPagLeftTop9Width}HeightCaption   Parcelas à QuitarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TvaSpreadgrdParcelasQuitarPagLeftTopWidthqHeightTabOrder OnButtonClickedgrdGenericoButtonClickedOnLeaveCellgrdGenericoLeaveCellOnEditChangegrdGenericoEditChangeControlData

     �@  �  @                        R������ � K�Q   �DB MS Sans Serif��� ���                          �  �  �9  9  e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif                          ����MbP?E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d                B          �����������         ��������              %      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����                @0        �����                         �Q����?' L      ����                                                                   &@0       ����                @0       ����                @0       ����                @0       ����          >
ףp=!@) &      ���� R e /  �  �      	   ����    	   {�G�z@0    	   ����       
   ����    
         @0    
   ����          {�G�z@+ -      ���� A   �����ח������חAR . ,              !@+ -      ���� A   �����ח������חAR . ,        ��Q��=@       >
ףp�$@M       ����    �   ^       ����       ����       {�G�z@0       ����          ����       >
ףp=%@0       ����          >
ףp=%@) &      ���� R e /  �  �               &@M       ����    �   ^       ����             &@M       ����    �   ^       ����0    ����    �   8           Tipo Documento 8           Refer�ncia 8           Parcela 8           Vencimento 8           Valor 8           Recebimento 8    	       Multa 8    
       Juros 8           Desconto 8           Valor Receb. 8           Hist�rico 8           Tipo Baixa 8           Banco 8           N� Documento 8           Data Cr�dito 8           Tipo Receb 8           Tipo Taxa         ����                         �               	TGroupBoxgrpCabecalhoQuitarPagLeftTopWidth}Height8TabOrder TLabelLabel32LeftTopWidtheHeightCaptionNome do ProponenteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel33Left�TopWidth9HeightCaption   Nº PropostaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedtNomePropQuitarPagLeftTopWidth�HeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TEditedtCodigoPropQuitarPagLeft�TopWidthEHeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TButtonbtnTotalQuitarPagLeftTopXWidthKHeightCaption&TotalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TEditedtTotalQuitarPagLeftXTop`WidthIHeightEnabledTabOrder  TButtonbtnReciboQuitarPagLeft�TopXWidthKHeightCaption&ReciboFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder	OnClickbtnGenericoClick  	TGroupBox
grpChequesLeftTopxWidth}HeightiTabOrder TLabelLabel36LeftTopWidth*HeightCaptionChequesFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel37LeftTopWidth4HeightCaption   ReferênciaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TEdit
edtChequesLeftTop$Width	HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderVisible  TEditedtReferenciaLeftTop$Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderVisible  	TCheckBoxchkDinheiroLeftTop WidthqHeightCaptionCheques/DepositosTabOrderOnClickchkGenericoClick  	TvaSpreadgrdChequesQuitarPagLeftTopWidth�HeightQTabOrder OnButtonClickedgrdGenericoButtonClickedOnLeaveCellgrdGenericoLeaveCellOnEditChangegrdGenericoEditChangeControlData
�     14  _  @                        R������ � K�Q   �DB MS Sans Serif��� ���                          �  �  ��  �  e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif                          ����MbP?#      E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d                B          �����������         ��������              %      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����                @0        �����                @) &      ���� R e /  l  4  + -      ���� A   �����ח������חAR . ,        >
ףp=!@) &      ���� R e /  l  4         ����            �!@*       ����� <             ����            �'@*       ����� <      0    ����    �   8           Parcela 8           Vencimento 8           Valor 8           Recebimento 8           N� Banco 8           N� Cheque         ����                         �               TButtonbtnNFQuitarPagLeftpTopXWidthKHeightCaption&Nota FiscalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TEditedtReciboQuitarPagLeft� Top`WidthIHeightTabOrder  	TMaskEditmskValorDinheiroQuitarPagLeft� Top`WidthAHeightTabOrder  	TMaskEditmskNumParcQuitarPagLeft@Top`Width)HeightTabOrderOnExitmskGenericoExit   TButtonbtnOKLeft4Top�Width� HeightCaption&OKFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClick
btnOKClick  TButton	btnCancelLeft Top�Width� HeightCancel	Caption&CancelFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnCancelClick  TPanelpnlVisualGavetaLeftTop Width�HeightfFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  	TGroupBoxgrpVisualGavetaLeftTopWidth�Height`TabOrder  TLabellblProponenteLeftTop4WidtheHeightCaptionNome do ProponenteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblNumPropostaLeft
TopWidthMHeightCaptionNo. da PropostaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedtProponenteLeftTopDWidth1HeightTabStopColorclInactiveCaptionEnabledReadOnly	TabOrder  	TMaskEditmskNumPropostaLeftTopWidthYHeightEditMask!99999999;0;	MaxLengthTabOrder OnExitmskNumPropostaExit    TPanelpnlComissaoAutoLeft TopWidth�HeightfCaptionpnlComissaoAutoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpComissaoAutoLeftTopWidth�Height`TabOrder  TLabelLabel20LeftTop/WidthQHeightCaptionTipo da PropostaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel21LeftTopWidthQHeightCaptionData da InclusaoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxcboComisTipoPropostaLeftTop@Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  TDateTimePickerdtpComisDataInclLeftTopWidthaHeightCalAlignmentdtaLeftDate Hֹ�|�@Time Hֹ�|�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder     TPanelpnlGraphGaveTotalLeft Top Width�HeightfCaptionpnlGraphLoteTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpGraphGaveTotalLeftTopWidth�Height`TabOrder  TLabelLabel26LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TMaskEditmskGraphGTDataLeftTop(WidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /        TPanelpnlGraphLoteMesLeftTopWidth�HeightfCaptionpnlGraphLoteTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder
 	TGroupBoxgrpGraphLoteMesLeftTopWidth�Height`TabOrder  TLabelLabel29LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TMaskEditmskGraphLMDataLeftTop(WidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /        TPanelpnlGraphGaveAnoLeftTopWidth�HeightfCaptionpnlGraphLoteTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpGraphGaveAnoLeftTopWidth�Height`TabOrder  TLabelLabel30LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TMaskEditmskGraphGADataLeftTop(WidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /        TPanelpnlGraphGaveMesLeft Top Width�HeightfCaptionpnlGraphLoteTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpGraphGaveMesLeftTopWidth�Height`TabOrder  TLabelLabel31LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TMaskEditmskGraphGMDataLeftTop(WidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /        TPanelpnlGraphLoteAnoLeftTopWidth�HeightiCaptionpnlGraphLoteTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder	 	TGroupBoxgrpGraphLoteAnoLeftTopWidth�Height`TabOrder  TLabelLabel27LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TMaskEditmskGraphLADataLeftTop(WidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /        TPanelpnlPlanoAutoLeftTop�Width�HeightfCaptionpnlPlanoAutoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpPlanoAutoLeftTopWidth�Height`TabOrder  TLabelLabel18LeftTop/WidthQHeightCaptionTipo da PropostaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel19LeftTopWidthQHeightCaptionData da InclusaoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxcboPlanoTipoPropostaLeftTop@Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  TDateTimePickerdtpPlanoDataInclLeftTopWidthaHeightCalAlignmentdtaLeftDate      |�@Time      |�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder     TPanelpnlGraphLoteTotalLeft0TopWidth�HeightfCaptionpnlGraphLoteTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpGraphLoteTotalLeftTopWidth�Height`TabOrder  TLabelLabel28LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  	TMaskEditmskGraphLTDataLeftTop(WidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /    Visible    TPanelpnlGraphLoteCanTotalLeftxTopnWidth�HeightfCaptionpnlGraphLoteTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpGraphLoteCancTotalLeftTopWidth�Height`TabOrder  TLabelLabel34LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  	TMaskEditmskGraphLCTDataLeftTop(WidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /    Visible    TPanelpnlGraphLoteSldTotalLeft� TopvWidth�HeightfCaptionpnlGraphLoteTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpGraphLoteSldTotalLeftTopWidth�Height`TabOrder  TLabelLabel35LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  	TMaskEditmskGraphLoteSldTotalLeftTop(WidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /    Visible    TPanelpnlProcuraNomeLeftTophWidth�HeightqCaptionpnlProcuraNomeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpProcuraNomeLeftTopWidth�HeighthTabOrder  TLabelLabel1LeftTopWidtheHeightCaptionNome do ProponenteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel4LeftTop8WidthLHeightCaptionTipo de ProcuraFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel40Left� Top8WidthHeightCaption   TítuloFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedtProcuraNome_NomeLeftTopWidth9HeightTabStopFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TComboBoxcboProcuraNome_TipoLeftTopHWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrderItems.Strings   1 - Início do Nome2 - Nome Inteiro3 - Qualquer parte do Nome   	TComboBoxcboProcuraNome_TipoTituloLeft� TopHWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrderItems.Strings   1 - Início do Nome2 - Nome Inteiro3 - Qualquer parte do Nome     TPanelpnlGeraSeguroLeft� Top� Width�HeightfCaptionpnlGeraSeguroFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpGeraSeguroLeftTopWidth�Height`TabOrder  TLabelLabel53Left_Top/WidthHHeightCaptionProposta InicialFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel55LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel56Left� Top/WidthCHeightCaptionProposta FinalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel63Left`TopWidth5HeightCaptionData InicialFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel64Left� TopWidth0HeightCaption
Data FinalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TMaskEditmskSeguroReferenciaLeftTopWidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /      	TMaskEditmskSeguroPropIniLeft`Top@WidthaHeightEditMask99999999;0;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderOnExitmskNumPropostaExit  	TMaskEditmskSeguroPropFimLeft� Top@WidthaHeightEditMask99999999;0;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderOnExitmskNumPropostaExit  TDateTimePickerdtpSeguroDataInicialLeft`TopWidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder  TDateTimePickerdtpSeguroDataFinalLeft� TopWidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder    TPanelpnlRetornoBancoLeft Top0WidthHeightyCaptionpnlQuitarParcelasTabOrder TLabellblArquivoImportantoRetBancoLeftTophWidtheHeightCaptionImportanto o Arquivo:  TLabellblNomeArquivoRetBancoLeftxTophWidth�HeightAutoSizeCaptionkjhkjhkjhkhjkjhkjhFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabellblLayoutRetBancoLeftTop6Width#HeightCaptionLayout:  	TGroupBoxgrpArquivosRetBancoLeftTopWidthHeight(CaptionArquivos de RetornoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TvaSpreadgrdArquivosRetBancoLeftTopWidth�HeightTabOrder ControlData
�     ^3  7  @                          R������ � K�Q   �DB MS Sans Serif��� ���                           �  �  ��  �  e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif               ����MbP?#      E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d                 B          �����������         ��������              %      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����                @0        �����          �Q����?' L      ����                                                                  �K@0       ����   0    ����    �   8           Arquivo         ����                         �               TButtonbtnIncluirRetBancoLeft�Top0Width;HeightCaption&IncluiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnExcluirRetBancoLeft�Top0Width;HeightCaption&ExcluiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TProgressBarprbRetBancoLeftTopPWidthHeightMin MaxdTabOrder  	TComboBoxcboLayoutRetBancoLeft0Top2WidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder   TPanelpnlIncluirImagensLeftHTop@WidthHeightyCaptionpnlQuitarParcelasTabOrder TLabelLabel65LeftTophWidth^HeightCaptionIncluindo o Arquivo:  TLabellblIncluirImagensLeftxTophWidth�HeightAutoSizeCaptionkjhkjhkjhkhjkjhkjhFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel67LeftTop6WidthHeightCaptionTipo:  	TGroupBoxgrpIncluirImagensLeftTopWidthHeight(CaptionImagens dos DocumentosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TvaSpreadgrdIncluirImagensLeftTopWidth�HeightTabOrder ControlData
�     ^3  7  @                          R������ � K�Q   �DB MS Sans Serif��� ���                           �  �  ��  �  e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif               ����MbP?E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d                 B          �����������     3         ��������              "    ����      %@%      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����                @0        �����          >
ףp� @0       ����          ���(\OH@0       ����          ��Q�^2@0    ����    �   8           Proposta 8           Caminho 8           Mensagem         ����                         �               TButtonbtnIncluirImagensLeft�Top0Width� HeightCaption&Inclui DocumentosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TProgressBarprbIncluirImagensLeftTopPWidthHeightMin MaxdTabOrder  	TComboBoxcboTipoImagensLeft0Top2WidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrderTextcboTipoImagens   TPanelpnlGeraSeguradoraLeft� Top Width�HeightfCaptionpnlGeraSeguradoraFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBox LeftTopWidth�Height`TabOrder  TLabelLabel54Left_Top/WidthHHeightCaptionProposta InicialFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel57LeftTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel58Left� Top/WidthCHeightCaptionProposta FinalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel59Left`TopWidth5HeightCaptionData InicialFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel60Left� TopWidth0HeightCaption
Data FinalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TMaskEditmskSeguraReferenciaLeftTopWidthAHeightEditMask99/9999	MaxLengthTabOrder Text  /      	TMaskEditmskSeguraPropIniLeft`Top@WidthaHeightEditMask99999999;0;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderOnExitmskNumPropostaExit  	TMaskEditmskSeguraPropFimLeft� Top@WidthaHeightEditMask99999999;0;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderOnExitmskNumPropostaExit  TDateTimePickerdtpSeguraDataInicialLeft`TopWidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder  TDateTimePickerdtpSeguraDataFinalLeft� TopWidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder    TPanelpnlReajusteParcelasLeft� Top� WidthHeight� TabOrder 	TGroupBoxgrpReajusteParcelasLeftTopWidthuHeight� CaptionReajuste de ParcelasFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelLabel95Left� TopWidth0HeightCaption
Data Final  TLabelLabel96LeftTopWidth5HeightCaptionData Inicial  TLabelLabel99LeftTop`WidthKHeightCaptionTipo de Parcela  TLabelLabel100Left� TopWidtheHeightCaption   Vigência do Reajuste  TLabelLabel101LeftTop8WidthHHeightCaptionProposta Inicial  TLabelLabel102Left� Top8WidthCHeightCaptionProposta Final  TLabelLabel107Left� Top8Width$HeightCaption   Serviço  TLabelLabel108Left� Top`Width?HeightCaptionTipo da Taxa  TDateTimePickerdtpDataInicialReajusteLeftTop WidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder   TDateTimePickerdtpDataFinalReajusteLeft� Top WidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder  	TComboBoxcboTipoParcelaReajusteLeftToppWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TMaskEditmskNumTaxaReajusteLeft� Top WidthyHeightEditMask
99/9999;1;	MaxLengthTabOrderText  /      	TMaskEditmskPropostaInicialReajusteLeftTopHWidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit  	TMaskEditmskPropostaFinalReajusteLeft� TopHWidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit  	TComboBoxcboServicoReajusteLeft� TopHWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboTipoTaxaReajusteLeft� ToppWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder   TProgressBarprbReajusteLeftTop� WidthuHeightMin MaxdTabOrder   TPanelpnlGeraParcelasLeft`Top� Width�HeightfCaptionpnlGeraTaxaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpGeraParcelasLeftTopWidth�Height`TabOrder  TLabelLabel98LeftTopWidthQHeightCaptionTipo da PropostaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel104LeftwTop/WidthCHeightCaptionProposta FinalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel105LeftTop/WidthHHeightCaptionProposta InicialFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxcboTipoParcelaGeraParcelaLeftTopWidthbHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder   	TMaskEditmskPropostaInicialGeraParcelaLeftTop@WidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit  	TMaskEditmskPropostaFinalGeraParcelaLeftxTop@WidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit    TPanelpnlGeraTaxaLeft0Top`Width�HeightfCaptionpnlGeraTaxaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpGeraTaxaLeftTopWidth�Height`TabOrder  TLabelLabel22LeftTop/WidthHeightCaptionIndiceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel23LeftTopWidthaHeightCaptionData de VencimentoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel24LeftwTopWidth4HeightCaption
ReferenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel25LeftwTop/WidthBHeightCaptionValor da TaxaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel103Left� Top0WidthHHeightCaptionProposta InicialFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel106Left0Top0WidthCHeightCaptionProposta FinalFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxcboTaxaFixaLeftTop@WidthbHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  TDateTimePickerdtpTaxaDataVencLeftTopWidthaHeightCalAlignmentdtaLeftDate Hֹ�|�@Time Hֹ�|�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder   	TMaskEditmskTaxaReferenciaLeftxTopWidthAHeightEditMask99/9999	MaxLengthTabOrderText  /      	TMaskEditmskTaxaValorLeftxTop@WidthAHeightTabOrder  	TMaskEditmskPropostaInicialGeraTaxaLeft� Top@WidthaHeightEditMask99999999;0;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderOnExitmskNumPropostaExit  	TMaskEditmskPropostaFinalGeraTaxaLeft0Top@WidthaHeightEditMask99999999;0;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderOnExitmskNumPropostaExit    TPanelpnlRecebimentoLeft0Top@WidthIHeightCaptionpnlRecebimentoTabOrder 	TGroupBoxgrpRecebimentoLeftTop9Width9Height� TabOrder TLabelLabel41LeftTophWidthYHeightCaptionData Recebimento  TLabelLabel42LeftoTophWidthJHeightCaptionData de Credito  TLabelLabel43Left� TophWidthHeightCaptionPagto  TLabelLabel44Left	Top� WidthXHeightCaptionValor de Desconto  TLabelLabel45LeftpTop� WidthIHeightCaptionValor Recebido  TLabelLabel46Left� Top� Width>HeightCaptionCaixa/Banco  TLabelLabel86LeftTop8WidthRHeightCaptionData Vencimento  TLabelLabel97Left	TopWidthMHeightCaptionValor de Original  	TMaskEditmskValorDescontoLeftTop� WidthYHeightTabOrder  	TMaskEditmskValorRecebidoLeftpTop� WidthYHeightTabOrder  	TMaskEditmskDataRecebimentoLeftTopxWidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      	TMaskEditmskDataCreditoLeftpTopxWidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      	TComboBoxcboRecebPagtoLeft� TopxWidthYHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboRecebCaixaBancoLeft� Top� WidthYHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder	  	TMaskEditmskDataVencimentoLeftTopHWidthYHeightEnabledEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      TButtonbtnAlterarVencimentoLeftvTopGWidth� HeightCaption&Alterar Todos VencimentosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  	TMaskEditmskValorOriginalLeftTop WidthYHeightTabOrder   TButtonbtnAlterarValorLeftvTopWidth� HeightCaptionAlterar Todos &ValoresFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick   	TGroupBoxgrpRecebNomeLeftTopWidth9Height8TabOrder  TLabelLabel47LeftxTopWidthOHeightCaptionTipo DocumentoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel48LeftTopWidth!HeightCaption   CódigoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel49Left� TopWidthAHeightCaptionParcela/TaxaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedtDocumentoRecebLeftxTopWidthQHeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TEditedtCodigoRecebLeftTopWidthaHeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TEditedtParcelaRecebLeft� TopWidthQHeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder    TPanelpnlBaixaAutomaticaLefthTop� WidthHeightaCaptionpnlQuitarParcelasTabOrder 	TGroupBoxgrpBaixaParcelaLeftTopiWidthHeight� Caption   Parcela à QuitarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel122LeftITop� Width0HeightCaption	Tipo Taxa  TLabelLabel123LeftITop� Width;HeightCaptionTipo Receb.  	TGroupBoxgrpBaixaRecebLeftTopWidth9Height8TabOrder  TLabelLabel111Left� TopWidth/HeightCaption	Tipo Doc.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel112LeftTopWidth!HeightCaption   CódigoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel113Left� TopWidthAHeightCaptionParcela/TaxaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedtBaixaTipoDocLeft� TopWidth1HeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TEditedtBaixaCodigoLeftTopWidth� HeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TEditedtBaixaNumParcLeft� TopWidthQHeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   	TGroupBoxgrpBaixaRecebParcelaLeftTopHWidth9Height� TabOrder TLabelLabel114LeftTop8WidthYHeightCaptionData Recebimento  TLabelLabel115LeftoTop8WidthJHeightCaptionData de Credito  TLabelLabel116Left� Top8WidthHeightCaptionPagto  TLabelLabel117Left	TophWidthXHeightCaptionValor de Desconto  TLabelLabel118LeftpTophWidthIHeightCaptionValor Recebido  TLabelLabel119Left� TophWidth>HeightCaptionCaixa/Banco  TLabelLabel120LeftpTopWidthRHeightCaptionData Vencimento  TLabelLabel121Left	TopWidthMHeightCaptionValor de Original  	TMaskEditmskBaixaValorDescontoLeftTopxWidthYHeightTabOrder  	TMaskEditmskBaixaValorRecebidoLeftpTopxWidthYHeightTabOrder  	TMaskEditmskBaixaDataRecebLeftTopHWidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      	TMaskEditmskBaixaDataCreditoLeftpTopHWidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      	TComboBoxcboBaixaPagtoLeft� TopHWidthYHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboBaixaCaixaBancoLeft� TopxWidthYHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  	TMaskEditmskBaixaDataVencLeftpTop WidthYHeightEnabledEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      	TMaskEditmskBaixaValorOriginalLeftTop WidthYHeightEnabledTabOrder    	TComboBoxcboBaixaTipoTaxaLeftHTop� WidthYHeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboBaixaTipoRecebLeftHTop� WidthYHeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder   	TGroupBoxgrpBaixaProponenteLeftTop1WidthHeight8TabOrder TLabelLabel109LeftTopWidtheHeightCaptionNome do ProponenteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel110Left�TopWidth9HeightCaption   Nº PropostaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedtBaixaProponenteLeftTopWidth�HeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TEditedtBaixaPropostaLeft�TopWidthEHeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   	TGroupBoxgrpBaixaBarrasLeftTop WidthHeight1Caption   Código de BarrasTabOrder  TEditedtBaixaBarrasLeftTopWidth�HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder OnExitedtGenericoExit	OnKeyDownedtBaixaBarrasKeyDown    TPanelpnlReenvioArquivoLeftTop`WidthHeight�CaptionpnlReemissaoParcelasTabOrder 	TGroupBoxgrpReenvioArquivoLeftTopWidthuHeightxCaptionReenvio de Arquivo para o BancoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabellblDataFinalReenvioArquiLeft� TopWidth0HeightCaption
Data Final  TLabellblDataInicialReenvioArquiLeftTopWidth5HeightCaptionData Inicial  TLabellblLayoutReenvioArquiLeftTop� Width HeightCaptionLayout  TLabellblNomeArquivoReenvioArquiLeftTop� WidthRHeightCaptionNome do Arquivo  TLabellblTipoParcelaReenvioArquiLeftTop� WidthKHeightCaptionTipo de Parcela  TLabelLabel50Left� Top� Width;HeightCaptionNo. da Taxa  TLabelLabel51LeftTop8WidthHHeightCaptionProposta Inicial  TLabelLabel52Left� Top8WidthCHeightCaptionProposta Final  TLabelLabel62Left� Top� WidthBHeightCaptionTipo do Envio  TLabelLabel66LeftTop`Width3HeightCaptionCEP Inicial  TLabelLabel68Left� Top`Width.HeightCaption	CEP Final  TLabelLabel88Left� TopWidthHeightCaptionBanco  TLabelLabel90Left� Top8Width$HeightCaption   Serviço  TLabelLabel92Left� Top`Width?HeightCaptionTipo da Taxa  TLabelLabel93LeftTop Width7HeightCaption	Mensagens  TLabelLabel138LeftTop,Width5HeightCaptionData InicialFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel139LeftxTop,Width0HeightCaption
Data FinalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel140Left� Top,WidthRHeightCaptionData VencimentoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDateTimePickerdtpDataInicialReenvioArquiLeftTop WidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder   TDateTimePickerdtpDataFinalReenvioArquiLeft� Top WidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder  	TComboBoxcboLayoutReenvioArquiLeftTop� WidthYHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  TEditedtNomeArquivoReenvioArquiLeftTop� Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  	TComboBoxcboTipoParcelaReenvioArquiLeftTop� Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder	  	TMaskEditmskNumTaxaReenvioArquiLeft� Top� WidthyHeightEditMask99/99;1;	MaxLengthTabOrder
Text  /    	TMaskEditmskPropostaInicialReenvioArquiLeftTopHWidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit  	TMaskEditmskPropostaFinalReenvioArquiLeft� TopHWidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit  	TComboBoxcboTipoEnvioReenvioArquiLeft� Top� WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TMaskEditmskCEPInicialReenvioArquiLeftToppWidthaHeightEditMask99999-999;1;	MaxLength	TabOrderText	     -     	TMaskEditmskCEPFinalReenvioArquiLeft� ToppWidthaHeightEditMask99999-999;1;	MaxLength	TabOrderText	     -     	TComboBoxcboBancoReenvioArquiLeft� Top WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboServicoReenvioArquiLeft� TopHWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboTipoTaxaReenvioArquiLeft� ToppWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboMsgReenvioArquiLeftTopWidthYHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TMaskEditmskDataInicialReenvioArquiLeftTop;WidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      	TMaskEditmskDataFinalReenvioArquiLeftxTop;WidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      	TMaskEditmskDataVencimentoReenvioArquiLeft� Top;WidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      TButtonbtnLimparReenvioArquivoLeftTopWWidthAHeightCaption&LimparFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnAgruparReenvioArquivoLeftPTopWWidthAHeightCaption&AgruparFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnListarReenvioArquivoLeft� TopWWidthAHeightCaptionLis&tarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnExcluirReenvioArquivoLeft� TopWWidthAHeightCaption&ExcluirFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnGerarReenvioArquivoLeft(TopWWidthAHeightCaption&GerarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick   TProgressBarprbReenvioBancoLeftTop�WidthuHeightMin MaxdTabOrder   TPanelpnlIncluiQuadraLeftTop�Width�HeightfCaptionpnlIncluiQuadraFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpIncQuadraLeftTopWidth�Height`TabOrder  TLabelLabel6LeftTop/Width<HeightCaptionTipo do LoteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel12LeftTopWidth#HeightCaptionQuadraFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel15LeftPTopWidthHeightCaptionRuaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel16Left� TopWidth/HeightCaptionJazigo Ini.Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel17Left� TopWidth1HeightCaption
Jazigo FimFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel141Left� Top0Width=HeightCaptionImplantado ?Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel142Left
TopWidth8HeightCaption   Data AlvaráFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel143LeftRTopWidth;HeightCaptionData Constr.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel144LeftTop0Width2HeightCaption   No. AlvaráFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel149LeftPTop0WidthHeightCaptionLadoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxcboIncTipoLoteLeftTop@WidthzHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  	TMaskEditmskIncQuadraLeftTopWidth9HeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TMaskEditmskIncSetorLeftPTopWidth1HeightEditMaska9a;1;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderText     	TMaskEditmskIncLoteFimLeft� TopWidth9HeightEditMask	00000l;1;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderText        	TMaskEditmskIncLoteIniLeft� TopWidth9HeightEditMask	00000l;1;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderText        	TComboBoxcboIncImplantadoLeft� Top@WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboIncQuadraLeftTopWidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder   	TMaskEditmskIncDataAlvaraLeftTopWidthAHeightEditMask!99/99/9999;1;_Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength

ParentFontTabOrderText
  /  /      	TMaskEditmskIncDataConstrucaoLeftPTopWidthAHeightEditMask!99/99/9999;1;_Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength

ParentFontTabOrderText
  /  /      	TMaskEditmskIncAlvaraLeftTop@WidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrder	  	TComboBox
cboIncLadoLeftPTop@WidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder
    TPanelpnlImplantaQuadraLeftTop8Width�HeightfCaptionpnlImplantaQuadraFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpImplantaQuadraLeftTopWidth�Height`TabOrder  TLabelLabel7LeftTopWidth#HeightCaptionQuadraFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel8LeftPTopWidthHeightCaptionRuaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel9Left� TopWidth/HeightCaptionJazigo Ini.Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel10Left� TopWidth1HeightCaption
Jazigo FimFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel11LeftTop0Width=HeightCaptionImplantado ?Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TMaskEditmskImpQuadraLeftTopWidth9HeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TMaskEditmskImpSetorLeftPTopWidth1HeightEditMaska9a;1;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderText     	TMaskEditmskImpLoteFimLeft� TopWidth9HeightEditMask	00000l;1;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderText        	TMaskEditmskImpLoteIniLeft� TopWidth9HeightEditMask	00000l;1;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderText        	TComboBoxcboImplantadoLeftTopCWidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboImpQuadraLeftTopWidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder     TPanelpnlAlteraQuadraLeftTop� Width�HeightfCaptionpnlAlteraQuadraFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder 	TGroupBoxgrpAlteraQuadraLeftTopWidth�Height`TabOrder  TLabelLabel3LeftTop/Width<HeightCaptionTipo do LoteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftTopWidth#HeightCaptionQuadraFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel13LeftPTopWidthHeightCaptionRuaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel14Left� TopWidth/HeightCaptionJazigo Ini.Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2Left� TopWidth1HeightCaption
Jazigo FimFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel145Left
TopWidth8HeightCaption   Data AlvaráFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel146LeftRTopWidth;HeightCaptionData Constr.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel147Left� Top0Width=HeightCaptionImplantado ?Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel148LeftTop0Width2HeightCaption   No. AlvaráFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel150LeftPTop0WidthHeightCaptionLadoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  	TComboBoxcboAltTipoLoteLeftTop@WidthzHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  	TMaskEditmskAltQuadraLeftTopWidth9HeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TMaskEditmskAltSetorLeftPTopWidth1HeightEditMaska9a;1;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderText     	TMaskEditmskAltLoteFimLeft� TopWidth9HeightEditMask	00000l;1;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderText        	TMaskEditmskAltLoteIniLeft� TopWidth9HeightEditMask	00000l;1;Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrderText        	TComboBoxcboAltQuadraLeftTopWidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder   	TMaskEditmskAltDataAlvaraLeftTopWidthAHeightEditMask!99/99/9999;1;_Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength

ParentFontTabOrderText
  /  /      	TMaskEditmskAltDataConstrucaoLeftPTopWidthAHeightEditMask!99/99/9999;1;_Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength

ParentFontTabOrderText
  /  /      	TComboBoxcboAltImplantadoLeft� Top@WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder  	TMaskEditmskAltAlvaraLeftTop@WidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength
ParentFontTabOrder	  	TComboBox
cboAltLadoLeftPTop@WidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontSorted	TabOrder
Visible    TPanelpnlCartaCobrancaLeft� Top� WidthHeight)TabOrder 	TGroupBoxgrpCartaCobrancaLeftTopWidthuHeightCaption   Emissão de Carta de CobrançaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelLabel71Left� Top� WidthOHeightCaptionDias Atraso Final  TLabelLabel72LeftTop� WidthTHeightCaptionDias Atraso Inicial  TLabelLabel73LeftTop`WidthHHeightCaptionProposta Inicial  TLabelLabel74LeftTopWidth#HeightCaptionModelo  TLabelLabel75Left� Top� Width`HeightCaptionNo. Parc/Taxa Final  TLabelLabel76LeftTop8WidthQHeightCaptionTipo da Proposta  TLabelLabel77Left� Top`WidthCHeightCaptionProposta Final  TLabelLabel78LeftTop� WidtheHeightCaptionNo. Parc/Taxa Inicial  TLabelLabel79Left� Top8WidthJHeightCaption   Com Ocupação  TLabelLabel80LeftTop� Width3HeightCaptionCEP Inicial  TLabelLabel81Left� Top� Width.HeightCaption	CEP Final  TLabelLabel82Left� Top`WidthWHeightCaption   Relaciona Débitos  TLabelLabel83Left� Top� WidthaHeightCaption   Débitos com Valores  TLabelLabel84Left� Top� Width]HeightCaption   Débitos Atualizados  TLabelLabel85Left� Top� Width7HeightCaption   Com Débito  	TComboBoxcboModeloCartaCobrancaLeftTop WidthAHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder   	TComboBoxcboTipoPropostaCartaCobrancaLeftTopHWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TMaskEditmskPropostaInicialCartaCobrancaLeftToppWidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit  	TMaskEditmskPropostaFinalCartaCobrancaLeft� ToppWidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit  	TMaskEditmskTaxaInicialCartaCobrancaLeftTop� WidthaHeightEditMask999;1;	MaxLengthTabOrderText   OnExitedtGenericoExit  	TComboBoxcboOcupacaoCartaCobrancaLeft� TopHWidthaHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder
  	TMaskEditmskCEPInicialCartaCobrancaLeftTop� WidthaHeightEditMask99999-999;1;	MaxLength	TabOrderText	     -     	TMaskEditmskCEPFinalCartaCobrancaLeft� Top� WidthaHeightEditMask99999-999;1;	MaxLength	TabOrderText	     -     	TMaskEditmskTaxaFinalCartaCobrancaLeft� Top� WidthaHeightEditMask999;1;	MaxLengthTabOrder	Text   OnExitedtGenericoExit  	TMaskEditmskAtrasoInicialCartaCobrancaLeftTop� WidthaHeightEditMask999;1;	MaxLengthTabOrderText   OnExitedtGenericoExit  	TMaskEditmskAtrasoFinalCartaCobrancaLeft� Top� WidthaHeightEditMask999;1;	MaxLengthTabOrderText   OnExitedtGenericoExit  	TComboBoxcboDebitosCartaCobrancaLeft� ToppWidthaHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboValoresCartaCobrancaLeft� Top� WidthaHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboAtualizadoCartaCobrancaLeft� Top� WidthaHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboVersoCartaCobrancaLeft� Top� WidthaHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder   TProgressBarprbCartaCobrancaLeftTopWidthuHeightMin MaxdTabOrder   TPanelpnlEnvioArquivoLeft� TopPWidthHeight�CaptionpnlReemissaoParcelasTabOrder 	TGroupBoxgrpEnvioArquivoLeftTopWidthuHeightxCaptionEnvio de Arquivo para o BancoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabellblDataFinalEnvioArquiLeft� TopWidth0HeightCaption
Data Final  TLabellblDataInicialEnvioArquiLeftTopWidth5HeightCaptionData Inicial  TLabellblCondominioEnvioArquiLeftTop8WidthHHeightCaptionProposta Inicial  TLabellblLayoutEnvioArquiLeftTop� Width HeightCaptionLayout  TLabellblNomeArquivoEnvioArquiLeftTop� WidthRHeightCaptionNome do Arquivo  TLabellblTipoParcelaEnvioArquiLeftTop� WidthKHeightCaptionTipo de Parcela  TLabelLabel38Left� Top8WidthCHeightCaptionProposta Final  TLabelLabel39Left� Top� Width;HeightCaptionNo. da Taxa  TLabelLabel61Left� Top� WidthBHeightCaptionTipo do Envio  TLabelLabel69LeftTop`Width3HeightCaptionCEP Inicial  TLabelLabel70Left� Top`Width.HeightCaption	CEP Final  TLabelLabel87Left� TopWidthHeightCaptionBanco  TLabelLabel89Left� Top8Width$HeightCaption   Serviço  TLabelLabel91Left� Top`Width?HeightCaptionTipo da Taxa  TLabelLabel94LeftTop Width7HeightCaption	Mensagens  TLabelLabel135LeftTop,Width5HeightCaptionData InicialFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel136LeftxTop,Width0HeightCaption
Data FinalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel137Left� Top,WidthRHeightCaptionData VencimentoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDateTimePickerdtpDataInicialEnvioArquiLeftTop WidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder   TDateTimePickerdtpDataFinalEnvioArquiLeft� Top WidthaHeightCalAlignmentdtaLeftDate xX�`Ed�@Time xX�`Ed�@
DateFormatdfShortDateMode
dmComboBoxFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style KinddtkDate
ParseInput
ParentFontTabOrder  	TComboBoxcboLayoutEnvioArquiLeftTop� WidthYHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  TEditedtNomeArquivoEnvioArquiLeftTop� Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  	TComboBoxcboTipoParcelaEnvioArquiLeftTop� Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder	  	TMaskEditmskPropostaInicialEnvioArquiLeftTopHWidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit  	TMaskEditmskPropostaFinalEnvioArquiLeft� TopHWidthaHeightEditMask99999999;0;	MaxLengthTabOrderOnExitmskNumPropostaExit  	TMaskEditmskNumTaxaEnvioArquiLeft� Top� WidthyHeightEditMask99/99;1;	MaxLengthTabOrder
Text  /    	TComboBoxcboTipoEnvioEnvioArquiLeft� Top� WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TMaskEditmskCEPInicialEnvioArquiLeftToppWidthaHeightEditMask99999-999;1;	MaxLength	TabOrderText	     -     	TMaskEditmskCEPFinalEnvioArquiLeft� ToppWidthaHeightEditMask99999-999;1;	MaxLength	TabOrderText	     -     	TComboBoxcboBancoEnvioArquiLeft� Top WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboServicoEnvioArquiLeft� TopHWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboTipoTaxaEnvioArquiLeft� ToppWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TComboBoxcboMsgEnvioArquiLeftTopWidthYHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  TButtonbtnAgruparEnvioArquivoLeftPTopWWidthAHeightCaption&AgruparFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnListarEnvioArquivoLeft� TopWWidthAHeightCaptionLis&tarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnGerarEnvioArquivoLeft(TopWWidthAHeightCaption&GerarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnLimparEnvioArquivoLeftTopWWidthAHeightCaption&LimparFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnExcluirEnvioArquivoLeft� TopWWidthAHeightCaption&ExcluirFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  	TMaskEditmskDataInicialEnvioArquiLeftTop;WidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      	TMaskEditmskDataFinalEnvioArquiLeftxTop;WidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /      	TMaskEditmskDataVencimentoEnvioArquiLeft� Top;WidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /       TProgressBarprbEnvioBancoLeftTop�WidthuHeightMin MaxdTabOrder   TPanelpnlLancarServicoLeftTopWidth�Height�TabOrder TLabelLabel127Left� TopPWidth3HeightCaption	No ReciboFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel128Left� TopPWidth6HeightCaptionVlr DinheiroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel129Left@TopPWidth-HeightCaption	Qtd Parc.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TGroupBoxgrpPagamentosLancarServicoLeftTop9Width}HeightCaption	   ServiçosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder 	TvaSpreadgrdParcelasLancarServicoLeftTopWidthqHeightTabOrder OnButtonClickedgrdGenericoButtonClickedOnLeaveCellgrdGenericoLeaveCellOnEditChangegrdGenericoEditChangeControlData
�     �@  �  @                        R������ � K�Q   �DB MS Sans Serif��� ���                          �  �  ��  �  e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif                          ����MbP?E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d                B          �����������         ��������              %      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����                @0        �����                .@M       ����    �   ^       ����             @,       ���� A 
����    + -      ���� A   �����ח������חAR . ,        ����             !@+ -      ���� A   �����ח������חAR . ,                0    ����    �   8           Tipo de Servi�o 8           Qtde 8           Pre�o 8           Total 8           Codigo  ,      ����            �       �	          ����                         �               	TGroupBoxgrpCabecalhoLancarServicoLeftTopWidth}Height8TabOrder  TLabelLabel130LeftXTopWidtheHeightCaptionNome do ProponenteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel131LeftTopWidth9HeightCaption   Nº PropostaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel132LeftTopWidthHeightCaptionDataFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditedtNomePropLancarServicoLeftXTopWidth�HeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  	TMaskEditmskCodigoPropLancarServicoLeftTopWidthAHeightTabOrder OnExitmskGenericoExit  	TMaskEditmskDataLancarServicoLeftTopWidthYHeightEditMask!99/99/9999;1;_	MaxLength
TabOrderText
  /  /       TButtonbtnTotalLancarServicoLeftTopXWidthKHeightCaption&TotalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TEditedtTotalLancarServicoLeftXTop`WidthIHeightEnabledTabOrder  TButtonbtnReciboLancarServicoLeft�TopXWidthKHeightCaption&ReciboFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TButtonbtnNFLancarServicoLeftpTopXWidthKHeightCaption&Nota FiscalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnGenericoClick  TEditedtReciboLancarServicoLeft� Top`WidthIHeightTabOrder  	TMaskEditmskValorDinheiroLancarServicoLeft� Top`WidthAHeightTabOrder  	TMaskEditmskNumParcLancarServicoLeft@Top`Width)HeightTabOrderOnExitmskGenericoExit  	TGroupBoxgrpChequesLancarServicoLeftTopxWidth}HeightiTabOrder	 TLabelLabel133LeftTopWidth*HeightCaptionChequesFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel134LeftTopWidth4HeightCaption   ReferênciaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TEditedtChequesLancarServicoLeftTop$Width	HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderVisible  TEditedtReferenciaLancarServicoLeftTop$Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderVisible  	TCheckBoxchkDinheiroLancarServicoLeftTop WidthqHeightCaptionCheques/DepositosTabOrderOnClickchkGenericoClick  	TvaSpreadgrdChequesLancarServicoLeftTopWidthqHeightQTabOrder OnButtonClickedgrdGenericoButtonClickedOnLeaveCellgrdGenericoLeaveCellOnEditChangegrdGenericoEditChangeControlData
I     �@  _  @                        R������ � K�Q   �DB MS Sans Serif��� ��� 	                         �  �  �x  x  e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif                          ����MbP?#      E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d          	      B          �����������         ��������              %      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����                @0        �����                @) &      ���� R e /  l  4  + -      ���� A   �����ח������חAR . ,        >
ףp=!@) &      ���� R e /  l  4         ����            �!@*       ����� <             ����            �'@*       ����� <                   (@M       ����    �   ^       ����             4@    	           0    ����    �   8           Parcela 8           Vencimento 8           Valor 8           Recebimento 8           N� Banco 8           N� Cheque 8           Tipo Receb. 8           Observa��o 8    	       Codigo         ����                         �                TOpenDialogopdProcessos
DefaultExt.exeFilterAplicativos (.exe)|*.EXETitle*   Localizar Aplicativo de Emissão de CarnêLeftTop�   