unit uniAgendaVelorio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ComCtrls, ExtCtrls, Grids, DBCtrls, OleCtrls, FPSpread_TLB,
  uniConst, uniMDI, Variants;

type
  TfrmAgendaVelorio = class(TForm)
    pnlFundo: TPanel;
    pnlAgendaVelorio: TPanel;
    cboSalas: TComboBox;
    lblSalas: TLabel;
    btnVisualGrade: TButton;
    PntComunicador: TPaintBox;
    grpNomes: TGroupBox;
    lblFalecidoNome: TLabel;
    lblResponsavel: TLabel;
    edtFalecidoNome: TEdit;
    edtResponsavel: TEdit;
    lblTelefone: TLabel;
    edtTelefone: TEdit;
    btnConfirmar: TButton;
    btnCancelar: TButton;
    grpEntrada: TGroupBox;
    dtpDataEntrada: TDateTimePicker;
    lblHoraEntrada: TLabel;
    dtpHoraEntrada: TDateTimePicker;
    lblDataEntrada: TLabel;
    grpSaida: TGroupBox;
    dtpDataSaida: TDateTimePicker;
    lblHoraSaida: TLabel;
    dtpHoraSaida: TDateTimePicker;
    lblDataSaida: TLabel;
    edtFalecidoSobrenome: TEdit;
    lblFalecidoSobrenome: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure cboGenericoChange(Sender: TObject);
    procedure cboGenericoClick(Sender: TObject);
    procedure cboGenericoEnter(Sender: TObject);
    procedure cboGenericoKeyPress(Sender: TObject; var Key: Char);
    procedure edtCodigoChange(Sender: TObject);
    procedure edtCodigoEnter(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);
    procedure grdGenericoChange(Sender: TObject; Col, Row: Integer);
    procedure chkGenericoClick(Sender: TObject);
    procedure cboNumOSExit(Sender: TObject);
    procedure grdGenericoLeaveCell(Sender: TObject; Col, Row, NewCol,
      NewRow: Integer; var Cancel: WordBool);
    procedure btnGenericoClick(Sender: TObject);
  private
    { Private declarations }

    msCodigo: String;
    msSql: String;
    miConexao: Integer;
    miIndCol: Integer;

    {' Vari�veis de controle da inst�ncia do Form}
    mrTelaVar: TumaTelaVar;            {' vari�vel que guarda os dados da tela local}

    procedure lp_EstufaCombos;
    procedure mp_MostrarAgenda;

  public
    { Public declarations }
    function gf_ConflitoHorario(pdtDataEntrada: TDateTime;
                                pdtDataSaida: TDateTime;
                                pdtHoraEntrada: TDateTime;
                                pdtHoraSaida: TDateTime;
                                psSala: string;
                                psCodigo: string): Boolean;
  end;

var
  frmAgendaVelorio: TfrmAgendaVelorio;

implementation

uses uniDados, (*uniMDI,*) uniTelaVar, uniLock, uniCombos, uniEdit, uniGlobal,
      uniAgendamento;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.lp_EstufaCombos       Preenche os combos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.lp_EstufaCombos;
begin
  {' estufa os combos}

  untCombos.gp_Combo_Estufa(cboSalas, 'wSalas', VAZIO);

end;

{'-------------------------------------------------------------------------------------------------
' TfrmAgendaVelorio.FormShow
'     Prepara a tela
'-------------------------------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.FormShow(Sender: TObject);
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
    mrTelaVar.P.Titulo_Tela := 'Cadastro de Agenda de Vel�rio';

    { redimensiona a tela}
    Width  := 713;
    Height := 415;

    if formMDI.ActiveMDIChild = nil then
    begin
      Top := 0;
      Left := 0;
    end;

    {' ajusta a identifica��o da tela}
    if mrTelaVar.Comando_MDI = ED_CONSULTA then
      Caption := Caption + ' - Consulta'
    else
      If mrTelaVar.Comando_MDI = ED_NOVO then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    { para desabilitar os eventos change dos controles}
    Enabled := False;

    {' estufar os combos}
    lp_EstufaCombos;

//    if frmAgendamento.gbIncluir then
    if frmAgendamento.giModo = 0 then
    begin
      { posiciona no primeiro tab}
      dtpDataEntrada.Date := frmAgendamento.dtpData.Date;
      dtpHoraEntrada.Time := StrToTime(FormatFloat('00', frmAgendamento.grdAgendamento.SelBlockCol-1) + ':00');
      dtpDataSaida.Date := frmAgendamento.dtpData.Date;
      dtpHoraSaida.Time := StrToTime(FormatFloat('00', frmAgendamento.grdAgendamento.SelBlockCol2-1) + ':00');
      untCombos.gp_Combo_Posiciona(cboSalas,Chr(frmAgendamento.grdAgendamento.SelBlockRow + 64));
      msCodigo := VAZIO;
    end
    else
      mp_MostrarAgenda;

    { abre todas as tabelas secundarias}
//    untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    {' se for inclus�o}
//    If mrTelaVar.Comando_MDI = ED_NOVO Then
//      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender));
//    Else
//      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);
    {' Habilta a tela}
    Enabled := True;
    { deixa maximizado}
//    WindowState := wsMaximized;
    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
    { For�a atualiza��o da tela }
    FormActivate(Sender);

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniAgendaVelorio.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.FormActivate(Sender: TObject);
begin

  gfrmTV := TForm(Sender);

  { se nao existe a tela sai fora}
  If gfrmTV = Nil then
    Exit;

  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.FormDeactivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.FormDeactivate(Sender: TObject);
begin

  If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin

  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
  begin
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, frmAgendaVelorio);
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.FormResize(Sender: TObject);
begin

  if TfrmAgendaVelorio(Sender).WindowState <> wsMinimized Then
  begin
    if (TfrmAgendaVelorio(Sender).Width < pnlFundo.Width + 39) and (TfrmAgendaVelorio(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TfrmAgendaVelorio(Sender).Width := pnlFundo.Width + 39;

    TfrmAgendaVelorio(Sender).Refresh;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  liConta: Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - uniAgendaVelorio.FRM',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;

    gfrmTV := nil;

    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniAgendaVelorio.PAS');
  end;

  // destroi a janela
  Action := caFree;
  frmAgendaVelorio := Nil;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.cboGenericoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.cboGenericoChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
//    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 1);

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.cboGenericoClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.cboGenericoClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  { cursor ampulheta}
  Screen.Cursor := crHOURGLASS;

  lcboCombo := TComboBox(Sender);

  {' se nao for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
  begin
    { se tiver algo}
    if lcboCombo.Text <> VAZIO then
    begin
    end;

  end
  Else
    If (Not mrTelaVar.Reposicionando) Then
    begin
      {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.Text := gsAreaDesfaz;
    end;
  { cursor normal}
  Screen.Cursor := crDEFAULT;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.cboGenericoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.cboGenericoEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  liConta: Integer;
begin

  lcboCombo := TComboBox(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := lcboCombo.Text;
  giAreaDesfaz := -1;
  if lcboCombo.Items.Count > 0 then
    {'Executa um la�o nos itens comparando-os com o pConteudo}
    for liConta := 0 to lcboCombo.Items.Count - 1 do
      if lcboCombo.Items[liConta] = gsAreaDesfaz then
      begin
        giAreaDesfaz := liConta;
        break;
      end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.cboGenericoKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.cboGenericoKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.edtCodigoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.edtCodigoChange(Sender: TObject);
begin

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
//    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.edtCodigoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.edtCodigoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.PntComunicadorPaint(Sender: TObject);
begin

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, frmAgendaVelorio);

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.grdGenericoChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.grdGenericoChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
begin

  lgrdGrid := TvaSpread(Sender);

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
  begin
    {' altera��o efetuada}
//    untEdit.gp_Edit_Alterado(True, mrTelaVar);
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.chkGenericoClick       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.chkGenericoClick(Sender: TObject);
var
  lchkCheck: TCheckBox;
begin

  lchkCheck := TCheckBox(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);
end;

{*-----------------------------------------------------------------------------
 *  TformAssTecnica.cboNumOSExit       Para saber se � novo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.cboNumOSExit(Sender: TObject);
var
  lsValor: String;
begin

  Try

    {' se for consulta}
    If Not((Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado)) Then
      Exit;

    { cursor ampulheta}
    Screen.Cursor := crHOURGLASS;

    {' altera��o nao efetuada}
    untEdit.gp_Edit_Alterado(False, mrTelaVar);
    { cursor normal}
    Screen.Cursor := crDEFAULT;
  except
    { cursor normal}
    Screen.Cursor := crDEFAULT;
    dmDados.ErroTratar('cboNumOSExit - uniAgendaVelorio.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.grdGenericoClick       atualiza as colunas certas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.grdGenericoLeaveCell(Sender: TObject; Col, Row,
  NewCol, NewRow: Integer; var Cancel: WordBool);
var
  lgrdGrid: TvaSpread;
  lcValor: Currency;
  liQuantidade: Integer;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.btnGenericoClick       aciona o evento do botao
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.btnGenericoClick(Sender: TObject);
var
  lbtnBotao: TButton;
  liConexao:  Integer;
  liConta:    Integer;
  lsSql:      string;
  liIndCol:   array[0..20] of Integer;
  ldData: TDateTime;
  ldDataEntrada:  TDateTime;
  ldDataSaida:    TDateTime;
  lsHoraEntrada:  string;
  lsHoraSaida:    string;
  lsSala: string;
begin

  dmDados.giStatus := STATUS_OK;
  try
    lbtnBotao := TButton(Sender);

    if lbtnBotao.Name = 'btnConfirmar' then
    begin
      { sem sala nao pode gravar }
      if untCombos.gf_Combo_SemDescricao(cboSalas) =  VAZIO then
      begin
        MessageDlg('Informar a Sala !!!',
                   mtWarning, [mbOK], 0);
        Exit;
      end;

      { se for inclusao ou alteracao }
//      if frmAgendamento.gbIncluir then
      if (frmAgendamento.giModo = 0) or (frmAgendamento.giModo = 1) then
      begin
        { checa pelo conflito de horario }
        if not gf_ConflitoHorario(dtpDataEntrada.Date,
                                  dtpDataSaida.Date,
                                  dtpHoraEntrada.Time,
                                  dtpHoraSaida.Time,
                                  untCombos.gf_Combo_SemDescricao(cboSalas),
                                  msCodigo) then
          Exit;

        { ler os registros da tabela proplote }
        gaParm[0] := 'AGENDA';
        if frmAgendamento.giModo = 0 then
          gaParm[1] := 'DATA_ENTRADA = ''' + FormatDateTime(MK_DATATELA, dtpDataEntrada.Date) + ''' AND DATA_SAIDA = ''' + FormatDateTime(MK_DATATELA, dtpDataSaida.Date) + ''''
        else
          gaParm[1] := 'CODIGO = ''' + msCodigo + '''';
        gaParm[2] := 'CODIGO';
        lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
        if dmDados.giStatus <> STATUS_OK then
          Exit;

        liConexao := dmDados.ExecutarSelect(lsSql);
        if liConexao = IOPTR_NOPOINTER then
          Exit;

        for liConta := 0 to High(liIndCol) do
          liIndCol[liConta] := IOPTR_NOPOINTER;

        {'Atualiza��o no DB.}
        if frmAgendamento.giModo = 0 then
          dmDados.gsRetorno := dmDados.AtivaModo(liConexao,IOMODE_INSERT)
        else
          dmDados.gsRetorno := dmDados.AtivaModo(liConexao,IOMODE_EDIT);

        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'CODIGO',liIndCol[0],FormatDateTime('yyyy/mm/dd', dtpDataEntrada.Date)+untCombos.gf_Combo_SemDescricao(cboSalas)+FormatDateTime('hh:nn', dtpHoraEntrada.Time));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'DESCRICAO',liIndCol[1],Trim(edtFalecidoNome.Text)+' '+Trim(edtFalecidoSobrenome.Text));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'DATA_ENTRADA',liIndCol[2],StrToDate(DateToStr(dtpDataEntrada.Date)));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'DATA_SAIDA',liIndCol[3],StrToDate(DateToStr(dtpDataSaida.Date)));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'HORA_ENTRADA',liIndCol[4],TimeToStr(dtpHoraEntrada.Time));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'HORA_SAIDA',liIndCol[5],TimeToStr(dtpHoraSaida.Time));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'SALA',liIndCol[6],untCombos.gf_Combo_SemDescricao(cboSalas));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'NOME_FALECIDO',liIndCol[7],Trim(edtFalecidoNome.Text));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'SOBRENOME_FALECIDO',liIndCol[8],Trim(edtFalecidoSobrenome.Text));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'RESPONSAVEL',liIndCol[9],Trim(edtResponsavel.Text));
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'TELEFONE',liIndCol[10],Trim(edtTelefone.Text));

        if frmAgendamento.giModo = 0 then
          dmDados.gsRetorno := dmDados.Incluir(liConexao,'AGENDA')
        else
          dmDados.gsRetorno := dmDados.Alterar(liConexao,'AGENDA',VAZIO);
      end
      else
      begin
        { ler os registros da tabela proplote }
        gaParm[0] := 'AGENDA';
        gaParm[1] := 'CODIGO = ''' + msCodigo + '''';
        gaParm[2] := 'CODIGO';
        lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
        if dmDados.giStatus <> STATUS_OK then
          Exit;

        liConexao := dmDados.ExecutarSelect(lsSql);
        if liConexao = IOPTR_NOPOINTER then
          Exit;

        dmDados.gsRetorno := dmDados.Excluir(liConexao);
      end;
      dmDados.FecharConexao(liConexao);

      { pega as novas informacoes}
      frmAgendamento.dtpDataClick(Sender);

      frmAgendaVelorio.Close;
    end;

    if lbtnBotao.Name = 'btnCancelar' then
    begin
      frmAgendaVelorio.Close;
    end;

    if lbtnBotao.Name = 'btnVisualGrade' then
    begin
    end;

  except
    dmDados.ErroTratar('btnGenericoClick - uniAgendVelorio.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.mp_MostrarAgenda       mostrar os dados da agenda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmAgendaVelorio.mp_MostrarAgenda();
var
  liConexao:  Integer;
  liConta:    Integer;
  lsSql:      string;
  liIndCol:   array[0..20] of Integer;
  lsHora:  string;
  lsSala: string;
begin

  dmDados.giStatus := STATUS_OK;
  try
    lsHora := FormatFloat('00', frmAgendamento.grdAgendamento.SelBlockCol-1) + ':00';
    lsSala := Chr(frmAgendamento.grdAgendamento.SelBlockRow + 64);

    { ler os registros da tabela proplote }
    gaParm[0] := 'AGENDA';
//    gaParm[1] := '(DATA_ENTRADA <= ''' + FormatDateTime(MK_DATA, frmAgendamento.dtpData.Date) + ''' AND DATA_SAIDA >= ''' + FormatDateTime(MK_DATA, frmAgendamento.dtpData.Date) + ''')' +
//                 ' AND SUBSTRING(HORA_ENTRADA,1,2) <= ''' + Copy(lsHora,1,2) + ''' AND SUBSTRING(HORA_SAIDA,1,2) >= ''' + Copy(lsHora,1,2) + '''' +
//                 ' AND SALA = ''' + lsSala + '''';
    gaParm[1] := 'DATA_ENTRADA + Convert(datetime, HORA_ENTRADA) <= ' +
                 'Convert(datetime, ''' + FormatDateTime(MK_DATA, frmAgendamento.dtpData.Date) + ''')' +
                 ' + Convert(datetime, '''+ lsHora + ''')' +
                 ' AND DATA_SAIDA + Convert(datetime, HORA_SAIDA) >= ' +
                 'Convert(datetime, ''' + FormatDateTime(MK_DATA, frmAgendamento.dtpData.Date) + ''')' +
                 ' + Convert(datetime, '''+ lsHora + ''')' +
                 ' AND SALA = ''' + lsSala + '''';
    gaParm[2] := 'CODIGO';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    for liConta := 0 to High(liIndCol) do
      liIndCol[liConta] := IOPTR_NOPOINTER;

    dmDados.gsRetorno := dmDados.Primeiro(liConexao);
    if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
    begin
      msCodigo := dmDados.ValorColuna(liConexao, 'CODIGO', liIndCol[0]);
      dtpDataEntrada.Date := dmDados.ValorColuna(liConexao, 'DATA_ENTRADA', liIndCol[1]);
      dtpDataSaida.Date := dmDados.ValorColuna(liConexao, 'DATA_SAIDA', liIndCol[2]);
      dtpHoraEntrada.Time := StrToTime(dmDados.ValorColuna(liConexao, 'HORA_ENTRADA', liIndCol[3]));
      dtpHoraSaida.Time := StrToTime(dmDados.ValorColuna(liConexao, 'HORA_SAIDA', liIndCol[4]));
      untCombos.gp_Combo_Posiciona(cboSalas, dmDados.ValorColuna(liConexao, 'SALA', liIndCol[5]));
      edtFalecidoNome.Text := dmDados.ValorColuna(liConexao, 'NOME_FALECIDO', liIndCol[6]);
      edtFalecidoSobrenome.Text := dmDados.ValorColuna(liConexao, 'SOBRENOME_FALECIDO', liIndCol[7]);
      edtResponsavel.Text := dmDados.ValorColuna(liConexao, 'RESPONSAVEL', liIndCol[8]);
      edtTelefone.Text := dmDados.ValorColuna(liConexao, 'TELEFONE', liIndCol[9]);

      if frmAgendamento.giModo = 2 then
      begin
        pnlAgendaVelorio.Enabled := False;
        cboSalas.Enabled := False;
        grpNomes.Enabled := False;
      end;
    end;
    dmDados.FecharConexao(liConexao);

  except
    dmDados.ErroTratar('mpMostrarAgenda - uniAgendVelorio.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmAgendaVelorio.gf_ConflitoHorario       checa se nao tem conflito de horario
 *
 *-----------------------------------------------------------------------------}
function TfrmAgendaVelorio.gf_ConflitoHorario(pdtDataEntrada: TDateTime;
                                              pdtDataSaida: TDateTime;
                                              pdtHoraEntrada: TDateTime;
                                              pdtHoraSaida: TDateTime;
                                              psSala: string;
                                              psCodigo: string): Boolean;
var
  liConexao:  Integer;
  lsSql:      string;
begin

  dmDados.giStatus := STATUS_OK;
  Result := True;
  try
   { checa por data entrada maior que data saida }
   if (pdtDataEntrada + pdtHoraEntrada) > (pdtDataSaida + pdtHoraSaida) then
   begin
      MessageDlg('Entrada maior que a Sa�da !!!',
                 mtWarning, [mbOK], 0);
      Result := False;
      Exit;
   end;

    { ler os registros da tabela proplote }
    gaParm[0] := FormatDateTime(MK_DATA, pdtDataEntrada);
    gaParm[1] := TimeToStr(pdtHoraEntrada);
    gaParm[2] := FormatDateTime(MK_DATA, pdtDataSaida);
    gaParm[3] := TimeToStr(pdtHoraSaida);
    gaParm[4] := psSala;
    gaParm[5] := psCodigo;
    lsSql := dmDados.SqlVersao('NEC_0019', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;
    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      MessageDlg('Conflito de Hor�rios !!!',
                 mtWarning, [mbOK], 0);
      Result := False;
    end;
    dmDados.FecharConexao(liConexao);

  except
    dmDados.ErroTratar('gf_ConflitoHorario - uniAgendVelorio.PAS');
  end;

end;

end.
