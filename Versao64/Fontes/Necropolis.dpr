program Necropolis;

uses
  Forms,
  uniAlterarSenha in '..\Eg_Delphi\uniAlterarSenha.pas' {dlgAlterarSenha},
  uniCombos in '..\Eg_Delphi\uniCombos.pas',
  uniDados in '..\Eg_Delphi\uniDados.pas' {dmDados: TDataModule},
  uniDigito in '..\Eg_Delphi\uniDigito.pas',
  uniErros in '..\Eg_Delphi\uniErros.pas',
  uniExibeMsg in '..\Eg_Delphi\uniExibeMsg.pas' {dlgExibeMsg},
  uniGrupo in '..\Eg_Delphi\uniGrupo.pas' {dlgGrupo},
  uniImprimir in '..\Eg_Delphi\uniImprimir.pas' {formImprimir},
  uniLocalizar in '..\Eg_Delphi\uniLocalizar.pas' {dlgLocalizar},
  uniLock in '..\Eg_Delphi\uniLock.pas',
  uniMostraErros in '..\Eg_Delphi\uniMostraErros.pas' {dlgMostraErros},
  uniNaveg in '..\Eg_Delphi\uniNaveg.pas',
  uniParam in '..\Eg_Delphi\uniParam.pas',
  uniPergunta in '..\Eg_Delphi\uniPergunta.pas' {dlgPergunta},
  uniSeleciona in '..\Eg_Delphi\uniSeleciona.pas',
  uniSelPar in '..\Eg_Delphi\uniSelPar.pas',
  uniTelaVar in '..\Eg_Delphi\uniTelaVar.pas',
  uniUsuario in '..\Eg_Delphi\uniUsuario.pas' {dlgUsuario},
  uniUsuarioSenha in '..\Eg_Delphi\uniUsuarioSenha.pas' {dlgUsuarioSenha},
  uniGlobal in 'uniGlobal.pas',
  uniConst in '..\Eg_Delphi\uniConst.pas',
  uniEdit in 'uniEdit.pas',
  uniAbout in 'uniAbout.pas' {formAbout},
  uniPropostaGaveta in 'uniPropostaGaveta.pas' {frmPropostaGaveta},
  uniTABEmpresas in 'uniTABEmpresas.pas' {frmTABEmpresa},
  uniCorretores in 'uniCorretores.pas' {frmCorretor},
  uniPropostaNicho in 'uniPropostaNicho.pas' {frmPropostaNicho},
  uniPropostaRemido in 'uniPropostaRemido.pas' {frmPropostaRemido},
  uniTABPlanos in 'uniTABPlanos.pas' {frmTABPlanos},
  uniBloqueioLote in 'uniBloqueioLote.pas' {frmBloqueioLote},
  uniTABVendedores in 'uniTABVendedores.pas' {frmTABVendedor},
  uniTABServico in 'uniTABServico.pas' {frmTABServico},
  uniTABTipoGaveta in 'uniTABTipoGaveta.pas' {frmTABTipoGaveta},
  uniGuiaSepultamento in 'uniGuiaSepultamento.pas' {frmGuiaSepultamento},
  uniMDI in '..\Eg_Delphi\uniMDI.pas' {formMDI},
  uniRotinas in '..\Eg_Delphi\uniRotinas.pas' {formRotinas},
  uniCadSeleciona in '..\Eg_Delphi\uniCadSeleciona.pas' {dlgCadSelecionar},
  uniProcessos in 'uniProcessos.pas' {formProcessos},
  uniProponente in 'uniProponente.pas' {frmProponente},
  uniLoteEspecial in 'uniLoteEspecial.pas' {frmLoteEspecial},
  uniTABValorTaxa in 'uniTABValorTaxa.pas' {frmTABValorTaxa},
  uniMetaComissao in 'uniMetaComissao.pas' {frmMetaComissao},
  uniTABOperadores in 'uniTABOperadores.pas' {frmTABOperador},
  uniBancos in 'uniBancos.pas' {frmBanco},
  uniOcupacaoNicho in 'uniOcupacaoNicho.pas' {frmOcupacaoNicho},
  uniOcupacaoGaveta in 'uniOcupacaoGaveta.pas' {frmOcupacaoGaveta},
  uniVisualizacaoGavetas in 'uniVisualizacaoGavetas.pas' {formVisualGaveta},
  uniEtiquetaEspecifica in 'uniEtiquetaEspecifica.pas' {frmEtiquetaEspecifica},
  uniProcuraNome in 'uniProcuraNome.pas' {formProcuraNome},
  uniFuncoes in 'uniFuncoes.pas',
  uniGrafico in 'uniGrafico.pas' {dlgGrafico},
  uniRetorno in 'uniRetorno.pas' {dmRetorno: TDataModule},
  uniGeraArquivo in 'uniGeraArquivo.pas',
  uniIndiceIGP in 'uniIndiceIGP.pas' {frmINDIGP},
  uniAgendamento in 'uniAgendamento.pas' {frmAgendamento},
  uniAgendaVelorio in 'uniAgendaVelorio.pas' {frmAgendaVelorio},
  uniSalas in 'uniSalas.pas' {frmSalas},
  uniContorno in 'uniContorno.pas' {frmContorno},
  uniTalaoCheque in 'uniTalaoCheque.pas' {frmTalaoCheque},
  uniDatas in '..\Eg_Delphi\uniDatas.pas',
  uniChequePre in 'uniChequePre.pas' {frmChequePre},
  uniServico in 'uniServico.pas' {frmServico},
  uniTABCartas in 'uniTABCartas.pas' {frmTABCartas},
  uniPropostaLote in 'uniPropostaLote.pas' {frmPropostaLote},
  uniPagar in 'uniPagar.pas' {frmPagar},
  unITABTipoLote in 'uniTABTipoLote.pas' {frmTABTipoLote},
  uniEntradaManual in 'uniEntradaManual.pas' {frmEntradaManual},
  uniTABMotivo in 'uniTABMotivo.pas' {frmTABMotivo},
  uniContratos in 'uniContratos.pas' {frmImagens},
  uniTABMensagem in 'uniTABMensagem.pas' {frmTABMensagem},
  uniPlacas in 'uniPlacas.pas' {frmPlacas},
  uniConfig in 'uniConfig.pas' {frmConfig},
  uniLogOperacao in 'uniLogOperacao.pas' {frmLogOperacao},
  uniCodDescr in '..\Eg_Delphi\uniCodDescr.pas' {formCodDescr},
  uniTABComissaoGaveta in 'uniTABComissaoGaveta.pas' {frmTABComissaoGaveta},
  uniRecebSeguro in 'uniRecebSeguro.pas' {frmRecebSeguro},
  uniLivroSepultamento in 'uniLivroSepultamento.pas' {frmLivroSepultamento},
  uniChequeDevolvido in 'uniChequeDevolvido.pas' {frmChequeDevolvido},
  uniReplanejamento in 'uniReplanejamento.pas' {frmReplanejamento},
  uniConsulta in 'uniConsulta.pas' {dlgConsulta},
  uniRecebServico in 'uniRecebServico.pas' {frmRecebServico};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TformMDI, formMDI);
  Application.CreateForm(TdlgAlterarSenha, dlgAlterarSenha);
  Application.CreateForm(TdmDados, dmDados);
  Application.CreateForm(TdlgExibeMsg, dlgExibeMsg);
  Application.CreateForm(TdlgGrupo, dlgGrupo);
  Application.CreateForm(TformAbout, formAbout);
  Application.CreateForm(TdlgLocalizar, dlgLocalizar);
  Application.CreateForm(TdlgMostraErros, dlgMostraErros);
  Application.CreateForm(TdlgPergunta, dlgPergunta);
  Application.CreateForm(TdlgUsuario, dlgUsuario);
  Application.CreateForm(TdlgUsuarioSenha, dlgUsuarioSenha);
  Application.CreateForm(TformImprimir, formImprimir);
  Application.CreateForm(TformRotinas, formRotinas);
  Application.CreateForm(TformProcessos, formProcessos);
  Application.CreateForm(TdlgCadSelecionar, dlgCadSelecionar);
  Application.CreateForm(TformVisualGaveta, formVisualGaveta);
  Application.CreateForm(TformProcuraNome, formProcuraNome);
  Application.CreateForm(TdlgGrafico, dlgGrafico);
  Application.CreateForm(TdmRetorno, dmRetorno);
  Application.CreateForm(TfrmImagens, frmImagens);
  Application.CreateForm(TdlgConsulta, dlgConsulta);
  Application.Run;
end.
