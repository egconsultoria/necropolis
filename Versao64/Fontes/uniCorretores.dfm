�
 TFRMCORRETOR 0�  TPF0TfrmCorretorfrmCorretorLeft7Top� Width�HeightiCaption
CorretoresColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	Position	poDefaultVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeft Top Width�HeightITabOrder  	TGroupBoxgrpCorretorLeftTopWidth�Height@TabOrder  TLabellblNomeLeftpTop8WidthHeightCaptionNome  TLabellblCodigoCorretorLeftTop8WidthIHeightCaption   &Código Corretor  TLabellblTipoPropostaLeftTop`WidthQHeightCaption&Tipo da Proposta  TLabellblAtendimentoLeftpTop`Width;HeightCaptionAtendimento  TLabel	lblCodigoLeftTopWidth!HeightCaption   Código  TLabellblDescricaoLeftpTopWidth0HeightCaption   Descrição  TLabellblMetaLeftTop`WidthMHeightCaptionMeta em Pontos  	TPaintBoxPntComunicadorLeftTopWidth�Height9OnPaintPntComunicadorPaint  TEditedtNomeLeftpTopHWidth� HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtCodigoCorretorLeftTopHWidthPHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboTipoPropostaLeftToppWidthPHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TGroupBox
grpEmpresaLeftTop� WidthyHeightACaption	 Empresa TabOrder TLabellblEmpresa_NumPessoasLeftTopWidthDHeightCaptionNum. Pessoas  TLabellblEmpresa_NomeLeftpTopWidth)HeightCaptionEmpresa  	TComboBoxcboEmpresa_NomeLeftpTop$Width� Height
ItemHeightTabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  TEditedtEmpresa_NumPessoasLeftTop$WidthPHeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TComboBoxcboAtendimentoLeftpToppWidthPHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TCheckBoxchkComissaoLeft� TopsWidthAHeightCaption
   &ComissãoTabOrderOnClickchkCheckClick
OnKeyPresscboComboKeyPress  	TGroupBoxgrpAssociadosLeftTop� WidthyHeightYCaptionCorretores AssociadosTabOrder 	TvaSpreadsprAssociadosLeftTopWidthYHeightATabOrder 
OnKeyPresscboComboKeyPressOnChangesprGridChangeOnEditChangesprGridEditChangeControlData
+     �#  �  @                         R������ � K�Q   �DB MS Sans Serif��� ���    
                      �  �  �Z  Z  e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif               ����MbP?#      E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d             
   B          �����������         �������� %      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����  0        �����          ����            �<@M       ����    �   ^       ����0    ����    �   8           Nome         ����                         �               TEdit	edtCodigoLeftTop WidthPHeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtDescricaoLeftpTop Width� HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskMetaLeftToppWidthYHeightTabOrder	OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress     