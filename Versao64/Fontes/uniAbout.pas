unit uniAbout;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls,
  uniConst;

type
  TformAbout = class(TForm)
    pnlAbout: TPanel;
    pnlLogo: TPanel;
    pnlAguarde: TPanel;
    btnOK: TButton;
    lblAplicacao: TLabel;
    lblVersao: TLabel;
    lblCopyright: TLabel;
    lblCopyright2: TLabel;
    imgLogo: TImage;
    lblEmpresa: TLabel;
    lblSite: TLabel;
    lblEMail: TLabel;
    lblDireitos: TLabel;
    lblContTitulo: TLabel;
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  formAbout: TformAbout;

implementation

uses uniDados, uniGlobal;
{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TformLogin.btnOKClick                Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformAbout.btnOKClick(Sender: TObject);
begin
  { fecha a tela }
  formAbout.Close;
end;

{*-----------------------------------------------------------------------------
 *  TformLogin.FormShow                Mostra a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformAbout.FormShow(Sender: TObject);
begin

  { Se o About est� sendo chamado no in�cio da aplica��o }
  If Screen.Cursor  = crHourGlass Then
  begin
      formAbout.cursor := crHourGlass;
      formAbout.Caption := VAZIO;
      {n�o mostra o bot�o OK}
      btnOk.Visible := False;
      pnlAguarde.Visible := True;
  end
  Else
  begin
      formAbout.Caption := 'Sobre ' + gsMDICaption;   //MDI_CAPTION;
      {mostra o bot�o OK}
      btnOk.Visible := True;
      pnlAguarde.Visible := False;
      formAbout.Cursor := crDefault;
  End;

  { Conforme aumenta o tamanho do cabe�alho da MDI,
   diminui-se o tamanho do fonte do t�tulo}
  If Length (gsMDICaption(*MDI_CAPTION*)) > 25 Then
    lblAplicacao.Font.Size := 18;

  lblAplicacao.Caption := gsMDICaption;   //MDI_CAPTION;

  { Mostra o Titulo e a Versao }
  lblVersao.Caption := gsLblVersao;   //LBL_VERSAO;
  lblEmpresa.Caption := gsLblEmpresa;   //LBL_EMPRESA;

end;






end.
