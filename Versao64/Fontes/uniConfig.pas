unit uniConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Variants,
  uniConst, Mask, OleCtrls, FPSpread_TLB;

const
  COL_CHAVE = 1;
  COL_DESCR = 2;
  COL_VALOR = 3;

type
  TfrmConfig = class(TForm)
    pnlFundo: TPanel;
    PntComunicador: TPaintBox;
    lblCodigo: TLabel;
    lblConfig: TLabel;
    edtConfig: TEdit;
    mskCodigo: TMaskEdit;
    cboConfig: TComboBox;
    mskConfig: TMaskEdit;
    edtDescricao: TEdit;
    lblDescricao: TLabel;
    sprConfig: TvaSpread;
    edtTamanho: TEdit;
    edtComboTabela: TEdit;
    edtComboFiltro: TEdit;
    edtTipoControle: TEdit;
    lblTamanho: TLabel;
    lblTipoControle: TLabel;
    lblComboTabela: TLabel;
    lblComboFiltro: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure edtCodigoChange(Sender: TObject);
    procedure edtCodigoEnter(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);
    procedure edtCodigoKeyPress(Sender: TObject; var Key: Char);
    procedure mskCodigoExit(Sender: TObject);
  private
    { Private declarations }

    {' Vari�veis de controle da inst�ncia do Form}
    mrTelaVar: TumaTelaVar;            {' vari�vel que guarda os dados da tela local}

    procedure mpMostra_Tela;
    procedure mpGuarda_Tela;

  public
    { Public declarations }
  end;

var
  frmConfig: TfrmConfig;

implementation

uses uniDados, uniMDI, uniTelaVar, uniLock, uniCombos, uniEdit, uniGlobal, uniFuncoes;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TformCodDescr.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.FormShow(Sender: TObject);
begin

  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    { redimensiona a tela}
    Width  := 336;
    Height := 139;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
    mrTelaVar.P.Titulo_Tela := 'Cadastro de ' + mrTelaVar.P.Titulo_Tela;
    Caption := mrTelaVar.P.Titulo_Tela;

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    { para desabilitar os eventos change dos controles}
    Enabled := False;

    { abre todas as tabelas secundarias}
    untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);
    mpMostra_Tela();

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender));
//    Else
//      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);
    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;
    { Repinta a tela}
    Repaint;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniConfig.PAS');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TformCodDescr.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.FormActivate(Sender: TObject);
begin

  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescr.FormDeactivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.FormDeactivate(Sender: TObject);
begin

  If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescr.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin

  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, frmConfig);

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescr.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.FormResize(Sender: TObject);
begin

  if WindowState <> wsMinimized Then
  begin
    if (Width < pnlFundo.Width + 39) and (WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      Width := pnlFundo.Width + 39;

    Refresh;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescr.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  liConta: Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin             

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - frmConfig.FRM',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
      gfrmTV := nil;
    End;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
//    InvalidateRect(formMDI.cobMenu.Handle, nil, False);
    formMDI.gp_AtualizaBarraStatus;
//    InvalidateRect(formMDI.stbMDI.Handle, nil, False);

  except
    dmDados.ErroTratar('FormClose - uniConfig.PAS');
  end;

  // destroi a janela
  Action := caFree;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 0);


end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  {' muda o ponteiro do mouse para ampulheta}
  Screen.cursor := crHOURGLASS;

  lcboCombo := TComboBox(Sender);

  {' se nao for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
  begin
    if lcboCombo.Text <> VAZIO then
    begin
      {' altera��o efetuada}
      untEdit.gp_Edit_Alterado(True, mrTelaVar);
    end;
  end
  else
    If (Not mrTelaVar.Reposicionando) Then
      {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

  {' retorna o ponteiro do mouse para o default}
  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TformAssinante.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  liConta: Integer;
begin

  lcboCombo := TComboBox(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := lcboCombo.Text;
  giAreaDesfaz := -1;
  if lcboCombo.Items.Count > 0 then
    {'Executa um la�o nos itens comparando-os com o pConteudo}
    for liConta := 0 to lcboCombo.Items.Count - 1 do
      if lcboCombo.Items[liConta] = gsAreaDesfaz then
      begin
        giAreaDesfaz := liConta;
        break;
      end;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescr.edtCodigoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.edtCodigoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = 'mskCodigo') And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;
 
end;

{*-----------------------------------------------------------------------------
 *  TformCodDescr.edtCodigoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.edtCodigoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescr.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.PntComunicadorPaint(Sender: TObject);
var
  liComando_MDI: Integer;
begin

  liComando_MDI := giComando_MDI;

  if (liComando_MDI = TV_COMANDO_GRAVAR) then
    mpGuarda_Tela;

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, frmConfig);

  if (liComando_MDI = TV_COMANDO_IR_PRIMEIRO) or
     (liComando_MDI = TV_COMANDO_IR_PROXIMO) or
     (liComando_MDI = TV_COMANDO_IR_ANTERIOR) or
     (liComando_MDI = TV_COMANDO_PROCURAR) or
     (liComando_MDI = TV_COMANDO_IR_ULTIMO) then
  begin
    mpMostra_Tela;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescr.edtCodigoKeyPress       Se for consulta nao edita
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.edtCodigoKeyPress(Sender: TObject; var Key: Char);
begin
  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.edtTextoExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.mskCodigoExit(Sender: TObject);
var
  lmskEdit: TMaskEdit;
  i, j:  Integer;
begin

  lmskEdit := TMaskEdit(Sender);

  if Sender is TMaskEdit then
  begin
    { procura pela entidade }
    i := untTelaVar.gf_TelaVar_Retorna_Tabela(mrTelaVar.P.Sigla_Tabela);
    { se nao achou }
    if i = 0 then
      Exit;

    j := 1;
    { percorre todos os campos da tabela principal }
    while gaLista_Campos[i][j].sSigla_Tabela = gaLista_Parametros[i].Sigla_Tabela do
    begin
      if gaLista_Campos[i][j].sNome_Controle = lmskEdit.Name then
      begin
        if gaLista_Campos[i][j].sAlinha = ALINHA_ZERO then
          lmskEdit.Text := untFuncoes.gf_Zero_Esquerda(lmskEdit.Text, gaLista_Campos[i][j].iTamanho);
        break;
      end;
      j := j + 1;
    end;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmConfig.mpMostra_Tela       habilitar o controle correto
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.mpMostra_Tela();
begin

  dmDados.giStatus := STATUS_OK;
  mrTelaVar.Reposicionando := True;
  try
    case edtTipoControle.Text[1] of
      'T':
      begin
        mskConfig.Visible := False;
        edtConfig.Visible := True;
        cboConfig.Visible := False;
        edtConfig.MaxLength := StrToInt(edtTamanho.Text);
      end;
      'N':
      begin
        mskConfig.Visible := True;
        edtConfig.Visible := False;
        cboConfig.Visible := False;
        mskConfig.Text := edtConfig.Text;
        mskConfig.MaxLength := StrToInt(edtTamanho.Text);
      end;
      'C':
      begin
        mskConfig.Visible := False;
        edtConfig.Visible := False;
        cboConfig.Visible := True;
        cboConfig.Text := VAZIO;
        cboConfig.Clear;
        untCombos.gp_Combo_Estufa(cboConfig, edtComboTabela.Text, edtComboFiltro.Text);
        untCombos.gp_Combo_Posiciona(cboConfig, edtConfig.Text);
      end;
    end;

  except
    dmDados.ErroTratar('mpMostra_Tela - uniConfig.PAS');
  end;
  mrTelaVar.Reposicionando := False;

end;

{*-----------------------------------------------------------------------------
 *  TfrmConfig.mpGuarda_Tela       guardar o controle correto
 *
 *-----------------------------------------------------------------------------}
procedure TfrmConfig.mpGuarda_Tela();
begin

  dmDados.giStatus := STATUS_OK;
  try
    case edtTipoControle.Text[1] of
      'N':
      begin
        edtConfig.Text := mskConfig.Text;
      end;
      'C':
      begin
        edtConfig.Text := untCombos.gf_Combo_SemDescricao(cboConfig);
      end;
    end;

  except
    dmDados.ErroTratar('mpGuarda_Tela - uniConfig.PAS');
  end;

end;

end.
