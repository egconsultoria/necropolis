unit uniSalas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, OleCtrls, FPSpread_TLB, ComCtrls, ExtCtrls, Mask, Variants,
  uniConst, MaskUtils;

const
  COL_SALA = 1;
  COL_FALECIDO = 2;
  COL_RESPONSAVEL = 3;
  COL_DATA_ENTRADA = 4;
  COL_ENTRADA = 5;
  COL_DATA_SAIDA = 6;
  COL_SAIDA = 7;
  COL_CONFIRMA = 8;
  COL_SEPULT = 9;
  COL_ENCERRA = 10;
  COL_NUMPROP = 11;
  COL_QUADRA = 12;
  COL_SETOR = 13;
  COL_LOTE = 14;
  COL_GAVETA = 15;
  COL_GAVESPE = 16;
  COL_ORTN = 17;

  COD_VELORIO = '010';
  COD_SEPULT = '001';

  SERV_VELORIO = 1;
  SERV_SEPULT  = 2;

  NAO_CLIENTE = '99999999';

type
  TfrmSalas = class(TForm)
    pnlFundo: TPanel;
    pnlInformacao: TPanel;
    PntComunicador: TPaintBox;
    dtpData: TDateTimePicker;
    btnDataFrente: TButton;
    btnDataTras: TButton;
    pnlSalas: TPanel;
    grdSalas: TvaSpread;
    lblSalas: TLabel;
    cboSalas: TComboBox;
    btnLocalizar: TButton;
    btnCancelar: TButton;
    btnConfirmar: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dtpDataClick(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);
    procedure btnDataFrenteClick(Sender: TObject);
    procedure btnDataTrasClick(Sender: TObject);
    procedure btnGenericoClick(Sender: TObject);
    procedure grdSalasLeaveCell(Sender: TObject; Col, Row, NewCol,
      NewRow: Integer; var Cancel: WordBool);
  private
    { Private declarations }

    {' Vari�veis de controle da inst�ncia do Form}
    mrTelaVar: TumaTelaVar;            {' vari�vel que guarda os dados da tela local}

    procedure mp_Ressincronizar(pdData: TDateTime);
    procedure mpLanca_Servico(psNumProp: string;
                              psQuadra: string;
                              psSetor: string;
                              psLote:string;
                              psGaveta: string;
                              piTipo: Integer);
    procedure mpLanca_Ocupacao(psNumProp: string;
                               psQuadra: string;
                               psSetor: string;
                               psLote:string;
                               psGaveta: string;
                               psNome: string;
                               psSobrenome: string;
                               pdData: TDateTime);

  public
    { Public declarations }
  end;

var
  frmSalas: TfrmSalas;

implementation

uses uniDados, uniMDI, uniTelaVar, uniLock, uniCombos, uniEdit, uniGlobal,
  uniAgendamento, uniDigito;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.FormShow(Sender: TObject);
begin
  try

    dmDados.giStatus := STATUS_OK;

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    { redimensiona a tela}
    Width  := 780;
    Height := 369;

    { para nao entrar nos eventos}
    Enabled := False;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    { default data do dia}
    dtpData.Date := frmAgendamento.dtpData.Date;

    {' Monto Titulo da Janela}
    mrTelaVar.P.Titulo_Tela := 'Visualiza��o de Salas';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    { para desabilitar os eventos change dos controles}
    Enabled := False;

    { abre todas as tabelas secundarias}
    untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);
    mp_Ressincronizar(dtpData.Date);

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender));
//    Else
//      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, CAD_TRAPDROP, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);
    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    { deixa maximizado}
    WindowState := wsMaximized;
    Screen.Cursor := crDefault;
    { For�a atualiza��o da tela }
    FormActivate(Sender);

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniSalsa.PAS');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.dtpDataClick       Houve mudancas, atualiza o grid
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.dtpDataClick(Sender: TObject);
begin
  try
    { muda cursor}
    Screen.cursor := crHOURGLASS;

    { muda a data p/ reposicionar}
//    gsDataTrapDrop := FormatDateTime(MK_DATATELA, dtpData.Date);
    {Preeenche a tela}
    mp_Ressincronizar(dtpData.Date);
//    untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, frmSalas, True, 0);
//    untEdit.gp_Edit_Alterado(False, mrTelaVar);

    { muda cursor}
    Screen.cursor := crDefault;
  except
    { muda cursor}
    Screen.cursor := crDefault;
    dmDados.ErroTratar('dtpDataClick - uniSalsa.PAS');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.FormActivate(Sender: TObject);
begin

  gfrmTV := TForm(Sender);

  { se nao existe a tela sai fora}
  If gfrmTV = Nil then
    Exit;

  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;

end;

{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.FormDeactivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.FormDeactivate(Sender: TObject);
begin

  If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;

end;

{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin

  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, frmSalas);

end;

{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.FormResize(Sender: TObject);
begin

  if TfrmSalas(Sender).WindowState <> wsMinimized Then
  begin
    if (TfrmSalas(Sender).Width < pnlFundo.Width + 39) and (TfrmSalas(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TfrmSalas(Sender).Width := pnlFundo.Width + 39;

    TfrmSalas(Sender).Refresh;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  liConta:  Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - formSalas.FRM',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;

    gfrmTV := nil;

    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniSalsa.PAS');
  end;

  { destroi a janela}
  Action := caFree;
  frmSalas := Nil;

end;

{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.PntComunicadorPaint(Sender: TObject);
begin

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, frmSalas);

end;


{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.btnDataFrenteClick       Avanca um dia na data
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.btnDataFrenteClick(Sender: TObject);
begin
  { incrementa 1 na data}
  dtpData.Date := dtpData.Date + 1;
  { pega as novas informacoes}
  dtpDataClick(Sender);
end;

{*-----------------------------------------------------------------------------
 *  TfromTRAPDROP.btnDataFrenteClick       Retrocede um dia na data
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.btnDataTrasClick(Sender: TObject);
begin
  { decrementa 1 na data}
  dtpData.Date := dtpData.Date - 1;
  { pega as novas informacoes}
  dtpDataClick(Sender);
end;

{*-----------------------------------------------------------------------------
 *  TfrmSalas.mp_Ressincronizar       remontar a lista de agenda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.mp_Ressincronizar(pdData: TDateTime);
var
  liConexao:  Integer;
  liConta:    Integer;
  lsSql:      string;
  liIndCol:   array[0..20] of Integer;
  ldData: TDateTime;
  ldDataEntrada:  TDateTime;
  ldDataSaida:    TDateTime;
  lsHoraEntrada:  string;
  lsHoraSaida:    string;
  lsSala: string;
  lsConfirmado: string;
begin

  dmDados.giStatus := STATUS_OK;
  try
    { ler os registros da tabela proplote }
    gaParm[0] := 'AGENDA';
    gaParm[1] := 'DATA_ENTRADA = ''' + FormatDateTime(MK_DATA, pdData) + ''' OR DATA_SAIDA = ''' + FormatDateTime(MK_DATA, pdData) + '''';
    gaParm[2] := 'SALA, DATA_ENTRADA, HORA_ENTRADA';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    for liConta := 0 to High(liIndCol) do
      liIndCol[liConta] := IOPTR_NOPOINTER;

    ldData := StrToDate(DateToStr(pdData));

    grdSalas.ReDraw := False;
    { limpar o spread}
    grdSalas.Row := 1;
    grdSalas.Col := 1;
    grdSalas.Row2 := grdSalas.MaxRows;
    grdSalas.Col2 := grdSalas.MaxCols;
    grdSalas.BlockMode := True;
    grdSalas.Action := SS_ACTION_CLEAR;
//    grdSalas.Lock := True;
    grdSalas.BlockMode := False;

    liConta := 1;

    dmDados.gsRetorno := dmDados.Primeiro(liConexao);
    while (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) do
    begin
      if liConta > grdSalas.MaxRows then
        grdSalas.MaxRows := liConta;
      { posiciona na linha }
      grdSalas.Row := liConta;

      grdSalas.Col := COL_SALA;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'SALA', liIndCol[0]);
      grdSalas.Col := COL_FALECIDO;
      grdSalas.Text := Trim(dmDados.ValorColuna(liConexao, 'NOME_FALECIDO', liIndCol[1])) + ' ' + dmDados.ValorColuna(liConexao, 'SOBRENOME_FALECIDO', liIndCol[2]);
      grdSalas.Col := COL_RESPONSAVEL;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'RESPONSAVEL', liIndCol[3]);
      grdSalas.Col := COL_DATA_ENTRADA;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'DATA_ENTRADA', liIndCol[4]);
      grdSalas.Col := COL_ENTRADA;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'HORA_ENTRADA', liIndCol[5]);
      grdSalas.Col := COL_DATA_SAIDA;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'DATA_SAIDA', liIndCol[6]);
      grdSalas.Col := COL_SAIDA;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'HORA_SAIDA', liIndCol[7]);
      grdSalas.Col := COL_CONFIRMA;
      if dmDados.ValorColuna(liConexao, 'CONFIRMADO', liIndCol[8]) = 'S' then
      begin
        grdSalas.Text := '1';
        grdSalas.Lock := True;
      end
      else
        grdSalas.Text := VAZIO;
      grdSalas.Col := COL_SEPULT;
      if dmDados.ValorColuna(liConexao, 'SEPULTADO', liIndCol[9]) = 'S' then
      begin
        grdSalas.Text := '1';
        grdSalas.Lock := True;
      end
      else
        grdSalas.Text := VAZIO;
      grdSalas.Col := COL_ENCERRA;
      if dmDados.ValorColuna(liConexao, 'ENCERRADO', liIndCol[10]) = 'S' then
      begin
        grdSalas.Text := '1';
        grdSalas.Lock := True;
      end
      else
        grdSalas.Text := VAZIO;
      grdSalas.Col := COL_NUMPROP;
      grdSalas.Text := FormatMaskText('0000000-0;0;',dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[11]));
      grdSalas.Col := COL_QUADRA;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'QUADRA', liIndCol[12]);
      grdSalas.Col := COL_SETOR;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'SETOR', liIndCol[13]);
      grdSalas.Col := COL_LOTE;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'LOTE', liIndCol[14]);
      grdSalas.Col := COL_GAVETA;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'NUM_GAVETA', liIndCol[15]);
      grdSalas.Col := COL_GAVESPE;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'GAVETA_ESPECIAL', liIndCol[16]);
      grdSalas.Col := COL_ORTN;
      grdSalas.Text := dmDados.ValorColuna(liConexao, 'ORTN', liIndCol[17]);

      dmDados.gsRetorno := dmDados.Proximo(liConexao);
      liConta := liConta + 1;
    end;
    dmDados.FecharConexao(liConexao);

    grdSalas.ReDraw := True;

  except
    dmDados.ErroTratar('mp_Ressincronizar - uniSalsa.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmSalas.btnGenericoClick       aciona o evento do botao
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.btnGenericoClick(Sender: TObject);
var
  lbtnBotao: TButton;
  liConexao:  Integer;
  liConexaoCheca: Integer;
  liConta:    Integer;
  lsSql:      string;
  liIndCol:   array[0..20] of Integer;
  ldDataEntrada:  TDateTime;
  lsHoraEntrada:  string;
  ldDataSaida:    TDateTime;
  lsSala: string;
  lsCodigo: string;

  lsConfirmado: string;
  lsSepultado:  string;
  lsEncerrado:  string;
  lsConfirAnt:  string;
  lsSepultAnt:  string;
  lsEncerrAnt:  string;

  lsGaveEspe:  string;
  lsORTN:  string;

  lsNumProp:    string;
  lsQuadra:     string;
  lsSetor:      string;
  lsLote:       string;
  lsGaveta:     string;
  lsNome:       string;
  lsSobrenome:  string;
begin

  dmDados.giStatus := STATUS_OK;
  try
    lbtnBotao := TButton(Sender);

    if lbtnBotao.Name = 'btnConfirmar' then
    begin
      for liConta := 0 to High(liIndCol) do
        liIndCol[liConta] := IOPTR_NOPOINTER;

      { ler os registros da tabela agenda }
      gaParm[0] := 'AGENDA';
      gaParm[1] := 'DATA_ENTRADA = ''' + FormatDateTime(MK_DATA, dtpData.Date) + ''' OR DATA_SAIDA = ''' + FormatDateTime(MK_DATA, dtpData.Date) + '''';
//        gaParm[1] := 'CODIGO = ''' + lsCodigo;
      gaParm[2] := 'CODIGO';
      lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;

      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      for liConta := 1 to grdSalas.MaxRows do
      begin
        grdSalas.Row := liConta;

        grdSalas.Col := COL_SALA;
        lsSala := grdSalas.Text;
        if lsSala = VAZIO then
          break;

        grdSalas.Col := COL_DATA_ENTRADA;
        ldDataEntrada := StrToDate(grdSalas.Text);
        grdSalas.Col := COL_ENTRADA;
        lsHoraEntrada := grdSalas.Text;
        grdSalas.Col := COL_DATA_SAIDA;
        ldDataSaida := StrToDate(grdSalas.Text);

        lsCodigo := FormatDateTime('yyyy/mm/dd', ldDataEntrada) +
                    lsSala + lsHoraEntrada;

        grdSalas.Col := COL_CONFIRMA;
        if grdSalas.Text = '1' then
          lsConfirmado := 'S'
        else
          lsConfirmado := VAZIO;
        grdSalas.Col := COL_SEPULT;
        if grdSalas.Text = '1' then
          lsSepultado := 'S'
        else
          lsSepultado := VAZIO;
        grdSalas.Col := COL_ENCERRA;
        if grdSalas.Text = '1' then
          lsEncerrado := 'S'
        else
          lsEncerrado := VAZIO;
        grdSalas.Col := COL_NUMPROP;
        lsNumProp := dmDados.TirarMascara(grdSalas.Text);
        grdSalas.Col := COL_QUADRA;
        lsQuadra := grdSalas.Text;
        grdSalas.Col := COL_SETOR;
        lsSetor := grdSalas.Text;
        grdSalas.Col := COL_LOTE;
        lsLote := grdSalas.Text;
        grdSalas.Col := COL_GAVETA;
        lsGaveta := grdSalas.Text;
        grdSalas.Col := COL_GAVESPE;
        if grdSalas.Text = '1' then
          lsGaveEspe := 'S'
        else
          lsGaveEspe := VAZIO;
        grdSalas.Col := COL_ORTN;
        if grdSalas.Text = '1' then
          lsORTN := 'S'
        else
          lsORTN := VAZIO;

        { se nao estiver vazio }
//        if Trim(lsNumProp) <> VAZIO then
//        begin
          { se o digito verificador Numero da Proposta estiver errado}
//          if not untDigito.gf_Digito_ChecarProposta(lsNumProp) then
//          begin
//            MessageDlg('D�gito incorreto no No. da Proposta [' + Copy(lsNumProp,1,7) + '-' + Copy(lsNumProp,8,1) + '] !!!',
//                       mtWarning, [mbOK], 0);
//            dmDados.FecharConexao(liConexao);
//            Exit;
//          end;
//        end;

        { informar o No da Proposta }
        if lsConfirmado = 'S' then
          if (Trim(lsNumProp) = VAZIO) then
          begin
            MessageDlg('Informar No. da Proposta !!!',
                       mtWarning, [mbOK], 0);
            dmDados.FecharConexao(liConexao);
            Exit;
          end;

        { informar a Localizacao }
        if lsSepultado = 'S' then
          if (lsQuadra = VAZIO) or (lsSetor = VAZIO) or (lsLote = VAZIO) then
          begin
            MessageDlg('Informar Localiza��o no No. da Proposta [' + Copy(lsNumProp,1,7) + '-' + Copy(lsNumProp,8,1) + '] !!!',
                       mtWarning, [mbOK], 0);
            dmDados.FecharConexao(liConexao);
            Exit;
          end;

        { informar a No. da gaveta }
        if (lsSepultado = 'S') and (lsEncerrado = 'S') then
        begin
          if (lsGaveta = VAZIO) then
          begin
            MessageDlg('Informar No. da Gaveta no No. da Proposta [' + Copy(lsNumProp,1,7) + '-' + Copy(lsNumProp,8,1) + '] !!!',
                       mtWarning, [mbOK], 0);
            dmDados.FecharConexao(liConexao);
            Exit;
          end;
          { localizacao incorreta }
          if lsGaveEspe = 'S' then
          begin
            gaParm[0] := 'LOTEESPE';
            gaParm[1] := 'CODIGO = ''' + lsQuadra + lsSetor + lsLote + '''';
            gaParm[2] := 'CODIGO';
            lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
            if dmDados.giStatus <> STATUS_OK then
              Exit;

            liConexaoCheca := dmDados.ExecutarSelect(lsSql);
            if liConexaoCheca = IOPTR_NOPOINTER then
              Exit;
            { nao achou }
            if dmDados.Status(liConexaoCheca, IOSTATUS_NOROWS) then
            begin
              MessageDlg('Localiza��o da Gaveta incorreta no No. da Proposta [' + Copy(lsNumProp,1,7) + '-' + Copy(lsNumProp,8,1) + '] !!!',
                         mtWarning, [mbOK], 0);
              dmDados.FecharConexao(liConexaoCheca);
              dmDados.FecharConexao(liConexao);
              Exit;
            end;
            dmDados.FecharConexao(liConexaoCheca);
          end
          else
          begin
            { localizacao incorreta }
            gaParm[0] := 'PROPGAVE';
            gaParm[1] := 'NUM_PROP = ''' + lsNumProp + ''' AND QUADRA = ''' + lsQuadra + ''' AND SETOR = ''' + lsSetor + ''' AND LOTE = ''' + lsLote + '''';
            gaParm[2] := 'NUM_PROP';
            lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
            if dmDados.giStatus <> STATUS_OK then
              Exit;
            liConexaoCheca := dmDados.ExecutarSelect(lsSql);
            if liConexaoCheca = IOPTR_NOPOINTER then
              Exit;
            { nao achou }
            if dmDados.Status(liConexaoCheca, IOSTATUS_NOROWS) then
            begin
              MessageDlg('Localiza��o da Gaveta incorreta no No. da Proposta [' + Copy(lsNumProp,1,7) + '-' + Copy(lsNumProp,8,1) + '] !!!',
                         mtWarning, [mbOK], 0);
              dmDados.FecharConexao(liConexaoCheca);
              dmDados.FecharConexao(liConexao);
              Exit;
            end;
            dmDados.FecharConexao(liConexaoCheca);
          end;
          { gaveta ocupada }
          gaParm[0] := 'AGENDA';
          gaParm[1] := 'CODIGO = ''' + lsCodigo + '''';
          gaParm[2] := 'CODIGO';
          lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
          if dmDados.giStatus <> STATUS_OK then
            Exit;
          liConexaoCheca := dmDados.ExecutarSelect(lsSql);
          if liConexaoCheca = IOPTR_NOPOINTER then
            Exit;
          lsEncerrAnt := VAZIO;
          { achou }
          if not dmDados.Status(liConexaoCheca, IOSTATUS_NOROWS) then
          begin
            lsEncerrAnt := dmDados.ValorColuna(liConexaoCheca, 'ENCERRADO', liIndCol[2]);
          end;
          dmDados.FecharConexao(liConexaoCheca);
          { se for a primeira vez }
          if (lsEncerrado = 'S') and (lsEncerrAnt = VAZIO) then
          begin
            { gaveta ocupada }
            gaParm[0] := 'GAVEPESS';
            gaParm[1] := 'CODIGO = ''' + lsNumProp + lsGaveta + ''' AND TIPO_MOV = ''I''';
            gaParm[2] := 'CODIGO';
            lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
            if dmDados.giStatus <> STATUS_OK then
              Exit;
            liConexaoCheca := dmDados.ExecutarSelect(lsSql);
            if liConexaoCheca = IOPTR_NOPOINTER then
              Exit;
            { achou }
            if not dmDados.Status(liConexaoCheca, IOSTATUS_NOROWS) then
            begin
              MessageDlg('Gaveta Ocupada no No. da Proposta [' + Copy(lsNumProp,1,7) + '-' + Copy(lsNumProp,8,1) + '] !!!',
                         mtWarning, [mbOK], 0);
              dmDados.FecharConexao(liConexaoCheca);
              dmDados.FecharConexao(liConexao);
              Exit;
            end;
            dmDados.FecharConexao(liConexaoCheca);
          end;
        end;
(*
        { situacao anterior }
        lsConfirAnt := dmDados.ValorColuna(liConexao, 'CONFIRMADO', liIndCol[0]);
        lsSepultAnt := dmDados.ValorColuna(liConexao, 'SEPULTADO', liIndCol[1]);
        lsEncerrAnt := dmDados.ValorColuna(liConexao, 'ENCERRADO', liIndCol[2]);
*)
        {'Atualiza��o no DB.}
        dmDados.gsRetorno := dmDados.AtivaModo(liConexao,IOMODE_EDIT);

        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'CONFIRMADO',liIndCol[0],lsConfirmado);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'SEPULTADO',liIndCol[1],lsSepultado);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'ENCERRADO',liIndCol[2],lsEncerrado);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'QUADRA',liIndCol[3],lsQuadra);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'SETOR',liIndCol[4],lsSetor);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'LOTE',liIndCol[5],lsLote);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'NUM_GAVETA',liIndCol[6],lsGaveta);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'NUM_PROP',liIndCol[9],lsNumProp);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'GAVETA_ESPECIAL',liIndCol[10],lsGaveEspe);
        dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'ORTN',liIndCol[11],lsORTN);

        dmDados.gsRetorno := dmDados.Alterar(liConexao,'AGENDA','CODIGO = ''' + lsCodigo + '''');
(*
        { lanca velorio }
        if (lsConfirmado = 'S') and (lsConfirAnt = VAZIO) then
        begin
          mpLanca_Servico(lsNumProp, lsQuadra, lsSetor, lsLote, lsGaveta, SERV_VELORIO);
        end;
        { lanca sepulta }
        if (lsSepultado = 'S') and (lsSepultAnt = VAZIO) then
        begin
          mpLanca_Servico(lsNumProp, lsQuadra, lsSetor, lsLote, lsGaveta, SERV_SEPULT);
        end;
        { lanca ocupacao }
        if (lsEncerrado = 'S') and (lsEncerrAnt = VAZIO) then
        begin
          lsNome := dmDados.ValorColuna(liConexao, 'NOME', liIndCol[7]);
          lsSobrenome := dmDados.ValorColuna(liConexao, 'SOBRENOME', liIndCol[8]);
          mpLanca_Ocupacao(lsNumProp, lsQuadra, lsSetor, lsLote, lsGaveta, lsNome, lsSobrenome, ldDataSaida);
        end;
*)
      end;

      dmDados.FecharConexao(liConexao);
//      frmSalas.Close;

      { pega as novas informacoes}
      frmAgendamento.dtpDataClick(Sender);
    end;

    if lbtnBotao.Name = 'btnCancelar' then
    begin
      frmSalas.Close;
    end;

    if lbtnBotao.Name = 'btnVisualGrade' then
    begin
    end;

  except
    dmDados.ErroTratar('btnGenericoClick - uniSalas.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmSalas.mpLanca_Servico       lanca taxa de velorio e sepultamento
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.mpLanca_Servico(psNumProp: string;
                                    psQuadra: string;
                                    psSetor: string;
                                    psLote:string;
                                    psGaveta: string;
                                    piTipo: Integer);
var
  liConexao:  Integer;
  liConta:    Integer;
  lsSql:      string;
  liIndCol:   array[0..20] of Integer;
  lsNomeProp: string;
  lsNumParc:  string;
  lnVlrParc:  Double;
  lsCodServ:  string;
  lsNomeFalec: string;
  lsData:     string;
  lsHora:     string;
begin

  dmDados.giStatus := STATUS_OK;
  try
    if piTipo = SERV_VELORIO then
      lsCodServ := COD_VELORIO;
    if piTipo = SERV_SEPULT then
      lsCodServ := COD_SEPULT;

    if psNumProp <> NAO_CLIENTE then
    begin
      { ler os registros da tabela propgave }
      gaParm[0] := 'PROPGAVE';
//      gaParm[1] := 'QUADRA = ''' + psQuadra + ''' AND SETOR = ''' + psSetor + ''' AND LOTE = ''' + psLote + ''' AND ' +
      gaParm[1] := 'NUM_PROP = ''' + psNumProp + ''' AND ' +
                   'BLOQUEADA IS NULL';
      gaParm[2] := 'NUM_PROP';
      lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;

      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      liIndCol[0] := IOPTR_NOPOINTER;
      liIndCol[1] := IOPTR_NOPOINTER;
      if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
      begin
//        lsNumProp := dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[0]);
        lsNomeProp := dmDados.ValorColuna(liConexao, 'DESCRICAO', liIndCol[1]);
      end;
      dmDados.FecharConexao(liConexao);
    end
    else
      lsNomeProp := 'NAO CLIENTE';

    { ler os registros da tabela tab_serv }
    gaParm[0] := 'TAB_SERV_VALOR';
    gaParm[1] := 'CODIGOSERV = ''' + lsCodServ + '''' +
                 ' AND DATA = (SELECT MAX(DATA) FROM TAB_SERV_VALOR WHERE CODIGOSERV = ''' + lsCodServ + ''')';
    gaParm[2] := 'CODIGO';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    liIndCol[0] := IOPTR_NOPOINTER;
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
      lnVlrParc := dmDados.ValorColuna(liConexao, 'VALOR_NORMAL', liIndCol[0]);
    dmDados.FecharConexao(liConexao);

    { ler os registros da tabela rec_serv}
    gaParm[0] := 'REC_SERV';
    gaParm[1] := 'SUBSTRING(CODIGO,1,11) = ''' + psNumProp + lsCodServ + '''';
    gaParm[2] := 'CODIGO';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    liIndCol[0] := IOPTR_NOPOINTER;
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      dmDados.Ultimo(liConexao);
      lsNumParc := dmDados.ValorColuna(liConexao, 'NUM_PARC', liIndCol[0]);
    end
    else
      lsNumParc := '00';
    lsNumParc := FormatFloat('00', StrToInt(lsNumParc) + 1);

    { dados do falecido }
    grdSalas.Row := grdSalas.SelBlockRow;
    grdSalas.Col := COL_FALECIDO;
    lsNomeFalec := grdSalas.Text;
    grdSalas.Col := COL_DATA_ENTRADA;
    lsData := grdSalas.Text;
    grdSalas.Col := COL_ENTRADA;
    lsHora := grdSalas.Text;

    for liConta := 0 to High(liIndCol) do
      liIndCol[liConta] := IOPTR_NOPOINTER;

    {'Atualiza��o no DB.}
    dmDados.gsRetorno := dmDados.AtivaModo(liConexao,IOMODE_INSERT);

    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'CODIGO',liIndCol[0],psNumProp + lsCodServ + lsNumParc);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'DESCRICAO',liIndCol[1],lsNomeProp);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'COD_SERV',liIndCol[2],lsCodServ);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'NUM_PROP',liIndCol[3],psNumProp);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'DATA_LANC',liIndCol[4],StrToDate(DateToStr(Date)));
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'VLR_PARC',liIndCol[5],lnVlrParc);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'D_VENCTO',liIndCol[6],StrToDate(DateToStr(Date)));
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'NUM_PARC',liIndCol[7],lsNumParc);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'PAGTO',liIndCol[8],'N');
    if piTipo = SERV_VELORIO then
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'OBSERVACAO',liIndCol[9],'Vel�rio de ' + Trim(lsNomeFalec) +
                                                                                    ' em ' + lsData + ' as ' + lsHora)
    else
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'OBSERVACAO',liIndCol[9],'Sepultamento de ' + Trim(lsNomeFalec) +
                                                                                    ' em ' + lsData + ' as ' + lsHora);

    dmDados.gsRetorno := dmDados.Incluir(liConexao,'REC_SERV');

    dmDados.FecharConexao(liConexao);

  except
    dmDados.ErroTratar('mpLanca_Servico - uniSalas.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmSalas.mpLanca_Ocupacao       lanca ocupacao
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.mpLanca_Ocupacao(psNumProp: string;
                                     psQuadra: string;
                                     psSetor: string;
                                     psLote:string;
                                     psGaveta: string;
                                     psNome: string;
                                     psSobrenome: string;
                                     pdData: TDateTime);
var
  liConexao:  Integer;
  liConta:    Integer;
  lsSql:      string;
  liIndCol:   array[0..20] of Integer;
//  lsNumProp:  string;
  lsNomeProp: string;
  lsLugar:    string;
begin

  dmDados.giStatus := STATUS_OK;
  try
    { ler os registros da tabela propgave }
    gaParm[0] := 'PROPGAVE';
//    gaParm[1] := 'QUADRA = ''' + psQuadra + ''' AND SETOR = ''' + psSetor + ''' AND LOTE = ''' + psLote + ''' AND ' +
    gaParm[1] := 'NUM_PROP = ''' + psNumProp + ''' AND ' +
                 'BLOQUEADA IS NULL';
    gaParm[2] := 'NUM_PROP';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    liIndCol[0] := IOPTR_NOPOINTER;
    liIndCol[1] := IOPTR_NOPOINTER;
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
//      lsNumProp := dmDados.ValorColuna(liConexao, 'NUM_PROP', liIndCol[0]);
      lsNomeProp := dmDados.ValorColuna(liConexao, 'DESCRICAO', liIndCol[1]);
    end;
    dmDados.FecharConexao(liConexao);

    { inicia transacao }
    dmDados.TransBegin('Lanca_Ocupacao ' + psNumProp + psGaveta);

    { ler os registros da tabela gaveta}
    gaParm[0] := 'GAVETA';
    gaParm[1] := 'CODIGO = ''' + psNumProp + psGaveta + '''';
    gaParm[2] := 'CODIGO';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    if dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      for liConta := 0 to High(liIndCol) do
        liIndCol[liConta] := IOPTR_NOPOINTER;

      {'Atualiza��o no DB.}
      dmDados.gsRetorno := dmDados.AtivaModo(liConexao,IOMODE_INSERT);

      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'CODIGO',liIndCol[0],psNumProp + psGaveta);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'DESCRICAO',liIndCol[1],lsNomeProp);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'CONTRATO',liIndCol[2],psNumProp);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'QUADRA',liIndCol[3],psQuadra);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'SETOR',liIndCol[4],psSetor);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'LOTE',liIndCol[5],psLote);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'NUM_GAVETA',liIndCol[6],psGaveta);

      dmDados.gsRetorno := dmDados.Incluir(liConexao,'GAVETA');
    end;
    dmDados.FecharConexao(liConexao);

    { ler os registros da tabela gavepess}
    gaParm[0] := 'GAVEPESS';
    gaParm[1] := 'CODIGO = ''' + psNumProp + psGaveta + '''';
    gaParm[2] := 'CODIGO';
    lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    liConexao := dmDados.ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
      Exit;

    liIndCol[0] := IOPTR_NOPOINTER;
    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      dmDados.Ultimo(liConexao);
      lsLugar := dmDados.ValorColuna(liConexao, 'LUGAR', liIndCol[0]);
    end
    else
      lsLugar := '00';
    lsLugar := FormatFloat('00', StrToInt(lsLugar) + 1);

    for liConta := 0 to High(liIndCol) do
      liIndCol[liConta] := IOPTR_NOPOINTER;

    {'Atualiza��o no DB.}
    dmDados.gsRetorno := dmDados.AtivaModo(liConexao,IOMODE_INSERT);

    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'CODIGO',liIndCol[0],psNumProp + psGaveta);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'LUGAR',liIndCol[1],lsLugar);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'NOME',liIndCol[2],psNome);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'SOBRENOME',liIndCol[3],psSobrenome);
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'TIPO_MOV',liIndCol[4],'I');
    dmDados.gsRetorno := dmDados.AlterarColuna(liConexao,'DATA_MOV',liIndCol[5],pdData);

    dmDados.gsRetorno := dmDados.Incluir(liConexao,'GAVEPESS');

    dmDados.FecharConexao(liConexao);

    { confirma transacao }
    dmDados.TransCommit('Lanca_Ocupacao ' + psNumProp + psGaveta);

  except
    { cancela transacao }
    dmDados.TransRollback('Lanca_Ocupacao ' + psNumProp + psGaveta);
    dmDados.ErroTratar('mpLanca_Ocupacao - uniSalas.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmSalas.grdSalasLeaveCell       acha a localizacao
 *
 *-----------------------------------------------------------------------------}
procedure TfrmSalas.grdSalasLeaveCell(Sender: TObject; Col, Row, NewCol,
  NewRow: Integer; var Cancel: WordBool);
var
  lvasSpread: TvaSpread;
  liConexao:  Integer;
  liConta:    Integer;
  lsSql:      string;
  liIndCol:   array[0..2] of Integer;
  lsNumProp:  string;
begin

  dmDados.giStatus := STATUS_OK;
  try

    lvasSpread := TvaSpread(Sender);

    if lvasSpread.Name = 'grdSalas' then
    begin
      if (Col = COL_NUMPROP) then
      begin
        grdSalas.Row := Row;
        grdSalas.Col := COL_NUMPROP;
        if grdSalas.Text <> VAZIO then
        begin
          lsNumProp := dmDados.TirarMascara(grdSalas.Text);
          { ler os registros da tabela propgave }
          gaParm[0] := 'PROPGAVE';
          gaParm[1] := 'NUM_PROP = ''' + lsNumProp + '''';
          gaParm[2] := 'NUM_PROP';
          lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
          if dmDados.giStatus <> STATUS_OK then
            Exit;

          liConexao := dmDados.ExecutarSelect(lsSql);
          if liConexao = IOPTR_NOPOINTER then
            Exit;

          for liConta := 0 to High(liIndCol) do
            liIndCol[liConta] := IOPTR_NOPOINTER;

          grdSalas.Col := COL_QUADRA;
          if grdSalas.Text = VAZIO then
            grdSalas.Text := dmDados.ValorColuna(liConexao, 'QUADRA', liIndCol[0]);
          grdSalas.Col := COL_SETOR;
          if grdSalas.Text = VAZIO then
            grdSalas.Text := dmDados.ValorColuna(liConexao, 'SETOR', liIndCol[1]);
          grdSalas.Col := COL_LOTE;
          if grdSalas.Text = VAZIO then
            grdSalas.Text := dmDados.ValorColuna(liConexao, 'LOTE', liIndCol[2]);

          dmDados.FecharConexao(liConexao);
        end;
      end;
    end;

  except
    dmDados.ErroTratar('grdSalasLeaveCell - uniSalas.PAS');
  end;
end;

end.
