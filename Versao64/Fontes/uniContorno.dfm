�
 TFRMCONTORNO 0  TPF0TfrmContornofrmContornoLeft� Top� Width�HeightCaption   Contorno Projeto CemitérioColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	Position	poDefaultVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidth�Height� TabOrder  	TGroupBoxgrpContornoLeftTopWidth�Height� TabOrder  TLabel	lblQuadraLeftTop0Width#HeightCaptionQuadra  TLabellblSetorLeftXTop0WidthHeightCaptionRua  TLabellblLoteLeft� Top0WidthHeightCaptionJazigo  TLabellblTipoLoteLeft� Top0Width<HeightCaptionTipo do Lote  TLabellblCemiterioLeft� TopXWidth+HeightCaption
   Cemitério  TLabellblImplantadoLeftTopXWidth=HeightCaptionImplantado ?  TLabel	lblCodigoLeftTopWidth!HeightCaption   Código  TLabellblDescricaoLefthTopWidth0HeightCaption   Descrição  TLabellblLoteAlvaraLeftTop� Width6HeightCaption   Lote Alvará  TLabellblNoAlvaraLeftPTop� Width2HeightCaption   No. Alvará  TLabellblDataAlvaraLeft� Top� WidthGHeightCaption   Data do Alvará  TLabellblDataConstrucaoLeftTop� WidthPHeightCaption   Data Construção  TLabellblDataInclusaoLeftTop� WidthBHeightCaption   Data Inclusão  	TPaintBoxPntComunicadorLeftTopWidth�Height� OnPaintPntComunicadorPaint  	TComboBoxcboTipoLoteLeft� Top@Width� Height
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TComboBoxcboImplantadoLeftTophWidthqHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  TEditedtCemiterioLeft� TophWidth� HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEdit	edtCodigoLeftTopWidthIHeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtDescricaoLefthTopWidthHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtLoteAlvaraLeftTop� Width1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtNoAlvaraLeftPTop� WidthIHeightTabOrder	OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataAlvaraLeft� Top� WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrder
Text  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataConstrucaoLeftTop� WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBox	cboQuadraLeftTop@WidthAHeight
ItemHeightSorted	TabOrderText	cboQuadraOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskSetorLeftXTop@Width9HeightTabOrderTextmskSetorOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskLoteLeft� Top@WidthIHeightTabOrderTextmskLoteOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataInclusaoLeftTop� WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress     