unit uniCombos;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls, FPSpread_TLB;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
//const
//  FLDCMB_CODIGO     = 'Opcao_Codigo';
//  FLDCMB_DESCRICAO  = 'Opcao_Descricao';

{*======================================================================
 *            RECORDS
 *======================================================================}

type
 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntCombos = class(TObject)
  private
    mbDentro:   Boolean; {' Para evitar recurs�o: Inicia com False}
    miSsCombo:  Integer;

    { Private declarations }
    function mf_Variavel_Traduz(psNome: String): String;

  public
    { Public declarations }

    procedure gp_Combo_Posiciona(pcCombo: TComboBox;
                                 psConteudo: String);
    procedure gp_Combo_SpreadPosiciona(pSpread: TvaSpread;
                                      psConteudo: String);
    function  gf_Combo_SemDescricao(pcCombo: TComboBox): String;
    function  gf_Combo_SemCodigo(pcCombo: TComboBox): String;
    function  gf_Combo_SpreadSemDescricao(pSpread: TvaSpread): String;
    procedure gp_Combo_Seleciona(pcCombo: TComboBox;
                                 piTamanho: Integer);
    procedure gp_Combo_SpreadSeleciona(pSpread: TvaSpread;
                                 piTamanho: Integer);
    procedure gp_Combo_Estufa(pcCombo: TComboBox;
                              psChave: String;
                              psFiltro: string);
    function gf_Combo_Consistir(const psCodigoOpcao: String;
                                const piTamanho: Integer;
                                const psTabelaOpcoes: String): Boolean;
    procedure gp_Combo_SpreadEstufa(pSpread: TvaSpread;
                                    psChave: String;
                                    psFiltro: string);
    procedure gp_Combo_Inicializar;
    procedure gp_Combo_Finalizar;
    function  gf_ComboCadastro_Consistir(piConexao: Integer;
                                         psConteudo: String;
                                         psNomeCampo: String;
                                         psCodigo: String;
                                         psMensagem: String): String;
    function gf_ComboCadastro_Estufa(pcCombo: TComboBox;
                                     psCodigoSql: String;
                                     psCampo: String;
                                     piConexao: Integer;
                                     paParm: array of variant): Integer;
    Function gf_ComboCadastro_Conteudo(piConexao: Integer;
                                        psNomeCampo: String;
                                        psCodigo: String;
                                        psCampo: String): String;
    Function gf_ComboCadastro_SpreadEstufa(pSpread: TvaSpread;
                                            psCodigoSql: String;
                                            psCampo: String;
                                            paParm: array of variant): Integer;

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untCombos: TuntCombos;

implementation

uses uniDados, uniConst, uniTelaVar, uniGlobal;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_Inicializa
 *        Cria o objeto e inicializa variaveis
 *-----------------------------------------------------------------------------}
procedure TuntCombos.gp_Combo_Inicializar;
begin
  // cria o objeto
  untCombos := TuntCombos.Create;

  { Para evitar recurs�o: Inicia com False}
  untCombos.mbDentro := False;
  untCombos.miSsCombo := 0;
end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_Finaliza
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
procedure TuntCombos.gp_Combo_Finalizar;
begin
  // destroi objeto
  untCombos.Free;
end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_Posiciona       Posiciona no item passado
 *
 *-----------------------------------------------------------------------------}
function TuntCombos.gf_Combo_Consistir(const psCodigoOpcao: String;
                                       const piTamanho: Integer;
                                       const psTabelaOpcoes: String): Boolean;
var
  lsSQl:      String;  { monta lista de op��es do combo }
  liConexao:  Integer;
  lsCriterio: String;
  aSqlParms:  array[0..1] of Variant;
begin

  dmDados.giStatus := STATUS_OK;
  try

    { nome da tab.de op��es do combo }
    aSqlParms[0] := TAB_PREFIXO + psTabelaOpcoes;
    { Codigo espec�fico }
    aSqlParms[1] := FLDCMB_CODIGO + '=''' + Copy(Trim(psCodigoOpcao), 1, piTamanho) + '''';
    lsSQl := dmDados.SqlVersao('CB_1002', aSqlParms);

    liConexao := dmDados.ExecutarSelect(lsSQl);
    { se n�o encontrou a op��o }
    if dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
      Result := False
    else
      Result := True;

    dmDados.FecharConexao(liConexao);

  except
    dmDados.ErroTratar('gf_Combo_Consistir - uniCOMBOS.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_Posiciona       Posiciona no item passado
 *
 *-----------------------------------------------------------------------------}
procedure TuntCombos.gp_Combo_Posiciona(pcCombo: TComboBox;
                                        psConteudo: String);
var
  liContador: Integer;
  lsSiglaCbo: String;
  liTamanho: Integer;
begin

  dmDados.giStatus := STATUS_OK;
  try

    psConteudo := Trim(psConteudo);
    if AnsiPos(' ',psConteudo) > 0 then
      {' se vier a express�o com codigo e descri��o ficar s� com o c�digo}
      psConteudo := Copy(psConteudo,1,AnsiPos(' ',psConteudo) - 1);

    liTamanho := Length(psConteudo);
    if liTamanho = 0 then
    begin
      pcCombo.ItemIndex := -1;
      Exit;
    end;

    if pcCombo.Items.Count > 0 then
    begin
      {'Executa um la�o nos itens comparando-os com o pConteudo}
      for liContador := 0 to pcCombo.Items.Count - 1 do
      begin
        lsSiglaCbo := Trim(Copy(pcCombo.Items[liContador],1, liTamanho));
        if lsSiglaCbo = psConteudo then
        begin
          {'J� obteve o efeito necess�rio}
          pcCombo.ItemIndex := liContador;
          Break;
        end
        else
          {'Se chegou aqui n�o achou nada}
          pcCombo.ItemIndex := -1;
      end;
    end;

  except
    dmDados.ErroTratar('gp_Combo_Posiciona - uniCOMBOS.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_SpreadPosiciona       Posiciona no item passado
 *
 *-----------------------------------------------------------------------------}
procedure TuntCombos.gp_Combo_SpreadPosiciona(pSpread: TvaSpread;
                                              psConteudo: String);
var
  liContador: Integer;
  lsSiglaCbo: String;
  liTamanho: Integer;
begin

  dmDados.giStatus := STATUS_OK;
  try

    psConteudo := Trim(psConteudo);
    if AnsiPos(' ',psConteudo) > 0 then
      {' se vier a express�o com codigo e descri��o ficar s� com o c�digo}
      psConteudo := Copy(psConteudo,1,AnsiPos(' ',psConteudo) - 1);

    liTamanho := Length(psConteudo);
    if liTamanho = 0 then
    begin
      pSpread.Text := VAZIO;
      Exit;
    end;

    if pSpread.TypeComboBoxCount > 0 then
    begin
      {'Executa um la�o nos itens comparando-os com o pConteudo}
      for liContador := 0 to pSpread.TypeComboBoxCount - 1 do
      begin
        { posiciona no item}
        pSpread.TypeComboBoxCurSel := liContador;
        lsSiglaCbo := Trim(Copy(pSpread.Text,1, liTamanho));
        if lsSiglaCbo = psConteudo then
        begin
          {'J� obteve o efeito necess�rio}
          pSpread.TypeComboBoxCurSel := liContador;
          Break;
        end
        else
          {'Se chegou aqui n�o achou nada}
          pSpread.Text := VAZIO;
      end;
    end;

  except
    dmDados.ErroTratar('gp_Combo_SpreadPosiciona - uniCOMBOS.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_SemDescricao       Retorna so o codigo
 *
 *-----------------------------------------------------------------------------}
function TuntCombos.gf_Combo_SemDescricao(pcCombo: TComboBox): String;
var
  liInicio: Integer;
begin

  Result := VAZIO;

  dmDados.giStatus := STATUS_OK;
  try
    if Trim(pcCombo.Text) <> VAZIO then
    begin
      liInicio := AnsiPos(' ',pcCombo.Text);
      {'se o texto do combo n�o estiver formatado "cod - Descricao"}
      if liInicio <> 0 then
      begin
        Result := Copy(pcCombo.Text,1,liInicio - 1);
        Exit;
      end
      else
      begin
//        Result := VAZIO;
        Result := '0';
        Exit;
      end;
    end;
  except
    dmDados.ErroTratar('gf_Combo_SemDescricao - uniCOMBOS.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_SemCodigo       Retorna so a descricao
 *
 *-----------------------------------------------------------------------------}
function TuntCombos.gf_Combo_SemCodigo(pcCombo: TComboBox): String;
var
  liInicio: Integer;
begin

  dmDados.giStatus := STATUS_OK;
  try
    if Trim(pcCombo.Text) <> VAZIO then
    begin
      liInicio := AnsiPos(' ',pcCombo.Text);
      {'se o texto do combo n�o estiver formatado "cod - Descricao"}
      if liInicio <> 0 then
      begin
        Result := Copy(pcCombo.Text,liInicio + 3,Length(pcCombo.Text));
        Exit;
      end
      else
      begin
//        Result := VAZIO;
        Result := '0';
        Exit;
      end;
    end;
  except
    dmDados.ErroTratar('gf_Combo_SemCodigo - uniCOMBOS.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_Seleciona       Posiciona no item digitado
 *
 *-----------------------------------------------------------------------------}
procedure TuntCombos.gp_Combo_Seleciona(pcCombo: TComboBox;
                                        piTamanho: Integer);
var
  i: Integer;
  lsTextoDigitado: String;
  lsItemCombo: String;

begin

  {'se j� estou nesta rotina nada feito}
  if mbDentro then
    Exit;

  {' Estou dentro n�o executar de novo}
  mbDentro := True;

  {' Se n�o tem tamanho de chave ent�o busca a op��o semi-preenchida}
  if piTamanho = 0 then
  begin
    lsTextoDigitado := mf_Variavel_Traduz(TrimLeft(UpperCase(pcCombo.Text)));
    for i := 0 to (pcCombo.Items.Count - 1) do
    begin
      lsItemCombo := mf_Variavel_Traduz(UpperCase(pcCombo.Items[i]));
      {' Para chaves com o semi-coincidentes}
      if Copy(lsTextoDigitado,Length(lsTextoDigitado)-1, 1) = ' ' then
      begin
        if lsItemCombo + ' ' = lsTextoDigitado then
        begin
          pcCombo.ItemIndex := i;
          {'Ok estou saindo, n�o h� mais risco de recurs�o}
          mbDentro := False;
          {' Sai diretamente da rotina}
          Exit;
        end;
      end;
      if Copy(lsItemCombo, 1, Length(lsTextoDigitado)) = lsTextoDigitado then
      begin
        if i < (pcCombo.Items.Count - 2) then
        begin
          if Copy(UpperCase(pcCombo.items[i + 1]), 1, Length(lsTextoDigitado)) = lsTextoDigitado then
          begin
            pcCombo.ItemIndex := -1;
            {'Ok estou saindo, n�o h� mais risco de recurs�o}
            mbDentro := False;
            {' Sai diretamente da rotina}
            Exit;
          end;
        end;
        pcCombo.ItemIndex := i;
        {'Ok estou saindo, n�o h� mais risco de recurs�o}
        mbDentro := False;
        {' Sai diretamente da rotina}
        Exit;
      end;
    end;
    {'se saiu do FOR n�o achou, portanto
    'Caso nao exista, limpa a area de digitacao do combo}
    pcCombo.ItemIndex := -1; {' Aqui existe risco de recurs�o}
    pcCombo.Text := VAZIO;   {' Aqui existe risco de recurs�o}
  end
  else
  begin
    {'Verifica se o tamanho do texto digitado no combo, satisfaz a condi��o de
    'busca dos elementos do mesmo.}
    if Length(Trim(pcCombo.Text)) >= piTamanho then
    begin
      lsTextoDigitado := mf_Variavel_Traduz(Copy(UpperCase(pcCombo.Text), 1, piTamanho));

      {'Efetua um la�o sobre todos os itens do combo}
      for i := 0 to pcCombo.Items.Count do
      begin
        lsItemCombo := mf_Variavel_Traduz(Copy(UpperCase(pcCombo.items[i]), 1, piTamanho));

        {'Verifica se a string digitada limitada pelo tamanho requerido,
        'existe nos elementos do combo.}
        if lsTextoDigitado = lsItemCombo then
        begin
          {' Aqui existe risco de recurs�o}
          pcCombo.ItemIndex := i;
          {'Ok estou saindo, n�o h� mais risco de recurs�o}
          mbDentro := False;
          {' Sai diretamente da rotina}
          Exit;
        end
        else
        begin
          {'Caso a busca em ordem alfabetica identifique que nao achou
          'nenhum elemento o la�o � quebrado}
          if lsTextoDigitado < lsItemCombo then
          begin
            {'Ok estou saindo, n�o h� mais risco de recurs�o}
            mbDentro := False;
            {' sai do for para zerar o resultado}
            break;
          end;
        end;
      end;
      {'se saiu do FOR n�o achou, portanto
      'Caso nao exista, limpa a area de digitacao do combo}
      pcCombo.ItemIndex := -1; {' Aqui existe risco de recurs�o}
      pcCombo.Text := VAZIO;   {' Aqui existe risco de recurs�o}
    end;
  end;
  {'Ok estou saindo, n�o h� mais risco de recurs�o}
  mbDentro := False;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_SpreadSeleciona       Posiciona no item digitado
 *
 *-----------------------------------------------------------------------------}
procedure TuntCombos.gp_Combo_SpreadSeleciona(pSpread: TvaSpread;
                                 piTamanho: Integer);
var
  i: Integer;
  lsTextoDigitado: String;
  lsItemCombo: String;

begin

  {'se j� estou nesta rotina nada feito}
  if mbDentro then
    Exit;

  {' Estou dentro n�o executar de novo}
  mbDentro := True;

  {' Se n�o tem tamanho de chave ent�o busca a op��o semi-preenchida}
  if piTamanho = 0 then
  begin
    lsTextoDigitado := mf_Variavel_Traduz(TrimLeft(UpperCase(pSpread.Text)));
    for i := 0 to (pSpread.TypeComboBoxCount - 1) do
    begin
      pSpread.TypeComboBoxIndex := i;
      lsItemCombo := mf_Variavel_Traduz(UpperCase(pSpread.TypeComboBoxString));
      {' Para chaves com o semi-coincidentes}
      if Copy(lsTextoDigitado,Length(lsTextoDigitado)-1, 1) = ' ' then
      begin
        if lsItemCombo + ' ' = lsTextoDigitado then
        begin
          pSpread.TypeComboBoxIndex := i;
          pSpread.TypeComboBoxCurSel := i;
          {'Ok estou saindo, n�o h� mais risco de recurs�o}
          mbDentro := False;
          {' Sai diretamente da rotina}
          Exit;
        end;
      end;
      if Copy(lsItemCombo, 1, Length(lsTextoDigitado)) = lsTextoDigitado then
      begin
        if i < (pSpread.TypeComboBoxCount - 1) then
        begin
          pSpread.TypeComboBoxIndex := i + 1;
          if Copy(UpperCase(pSpread.TypeComboBoxString), 1, Length(lsTextoDigitado)) = lsTextoDigitado then
          begin
            pSpread.TypeComboBoxIndex := -1;
            pSpread.TypeComboBoxCurSel := 0;
            {'Ok estou saindo, n�o h� mais risco de recurs�o}
            mbDentro := False;
            {' Sai diretamente da rotina}
            Exit;
          end;
        end;
        pSpread.TypeComboBoxIndex := i;
        pSpread.TypeComboBoxCurSel := i;
        {'Ok estou saindo, n�o h� mais risco de recurs�o}
        mbDentro := False;
        {' Sai diretamente da rotina}
        Exit;
      end;
    end;
    {'se saiu do FOR n�o achou, portanto
    'Caso nao exista, limpa a area de digitacao do combo}
    pSpread.TypeComboBoxIndex := -1; {' Aqui existe risco de recurs�o}
    pSpread.TypeComboBoxCurSel := 0;
    pSpread.Text := VAZIO;   {' Aqui existe risco de recurs�o}
  end
  else
  begin
    {'Verifica se o tamanho do texto digitado no combo, satisfaz a condi��o de
    'busca dos elementos do mesmo.}
    if Length(Trim(pSpread.TypeComboBoxString)) >= piTamanho then
    begin
      lsTextoDigitado := mf_Variavel_Traduz(Copy(UpperCase(pSpread.Text), 1, piTamanho));

      {'Efetua um la�o sobre todos os itens do combo}
      for i := 0 to pSpread.TypeComboBoxCount do
      begin
        pSpread.TypeComboBoxIndex := i;
        lsItemCombo := mf_Variavel_Traduz(Copy(UpperCase(pSpread.TypeComboBoxString), 1, piTamanho));

        {'Verifica se a string digitada limitada pelo tamanho requerido,
        'existe nos elementos do combo.}
        if lsTextoDigitado = lsItemCombo then
        begin
          {' Aqui existe risco de recurs�o}
          pSpread.TypeComboBoxIndex := i;
          pSpread.TypeComboBoxCurSel := i;
          {'Ok estou saindo, n�o h� mais risco de recurs�o}
          mbDentro := False;
          {' Sai diretamente da rotina}
          Exit;
        end
        else
        begin
          {'Caso a busca em ordem alfabetica identifique que nao achou
          'nenhum elemento o la�o � quebrado}
          if lsTextoDigitado < lsItemCombo then
          begin
            {'Ok estou saindo, n�o h� mais risco de recurs�o}
            mbDentro := False;
            {' sai do for para zerar o resultado}
            break;
          end;
        end;
      end;
      {'se saiu do FOR n�o achou, portanto
      'Caso nao exista, limpa a area de digitacao do combo}
      if Length(lsTextoDigitado) >= piTamanho then
      begin
        pSpread.TypeComboBoxIndex := -1; {' Aqui existe risco de recurs�o}
        pSpread.TypeComboBoxCurSel := 0;
        pSpread.Text := VAZIO;   {' Aqui existe risco de recurs�o}
      end;
    end;
  end;
  {'Ok estou saindo, n�o h� mais risco de recurs�o}
  mbDentro := False;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.lf_Variavel_Traduz       Posiciona no item digitado
 *
 *-----------------------------------------------------------------------------}
function TuntCombos.mf_Variavel_Traduz(psNome: String): String;
var
  p, pBeg: PChar;

begin

  p := PChar(psNome);
  pBeg := p;

  while (p^ <> '') do
  begin
    Case p^ of
      '�', '�', '�', '�', '�' :
        p^ := 'A';
      '�', '�', '�', '�':
        p^ := 'E';
      '�', '�', '�', '�':
        p^ := 'I';
      '�', '�', '�', '�', '�':
        p^ := 'O';
      '�', '�', '�', '�':
        p^ := 'U';
      '�':
        p^ := 'C';
      '�':
        p^ := 'N';
      '�', '�', '�', '�', '�':
        p^ := 'a';
      '�', '�', '�', '�':
        p^ := 'e';
      '�', '�', '�', '�':
        p^ := 'i';
      '�', '�', '�', '�', '�':
        p^ := 'o';
      '�', '�', '�', '�':
        p^ := 'u';
      '�':
        p^ := 'c';
      '�':
        p^ := 'n';
    end;
    inc(p);
  end;
  Result := pBeg;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_Estufa       Preenche o combo
 *
 *-----------------------------------------------------------------------------}
procedure TuntCombos.gp_Combo_Estufa(pcCombo: TComboBox;
                                     psChave: String;
                                     psFiltro: string);
var
  lsSQL: String;
  liColOpcao_Codigo: Integer;
  liColOpcao_Descricao: Integer;
  lvOpcao_Descricao: Variant;

Begin

  {'Abre o Arquivo Texto para Leitura dos combos}
  gaParm[0] := TAB_PREFIXO + psChave;
  if psFiltro <> VAZIO then
  begin
    gaParm[1] := psFiltro;
    lsSQL := dmDados.SqlVersao('CB_1002', gaParm);
  end
  else
    lsSQL := dmDados.SqlVersao('CB_1001', gaParm);
  miSsCombo := dmDados.ExecutarSelect(lsSQL);

  if miSsCombo = IOPTR_NOPOINTER then
    Exit;

  {'Efetua La�o de leitura das linhas do Arquivo}
  liColOpcao_Codigo := IOPTR_NOPOINTER;
  liColOpcao_Descricao := IOPTR_NOPOINTER;

  while not dmDados.Status(miSsCombo, IOSTATUS_EOF) do
  begin
    lvOpcao_Descricao := dmDados.ValorColuna(miSsCombo, FLDCMB_DESCRICAO, liColOpcao_Descricao);

    {se nao for nulo}
    if VarIsNull(lvOpcao_Descricao) then
      pcCombo.Items.Add(VarToStr(dmDados.ValorColuna(miSsCombo, FLDCMB_CODIGO, liColOpcao_Codigo)))
    else
      pcCombo.Items.Add(VarToStr(dmDados.ValorColuna(miSsCombo, FLDCMB_CODIGO, liColOpcao_Codigo)) +
             ' - ' + dmDados.ValorColuna(miSsCombo, FLDCMB_DESCRICAO, liColOpcao_Descricao));

    if (dmDados.Proximo(miSsCombo) = IORET_EOF) or
       (dmDados.giStatus <> STATUS_OK) then
        break;
  end;

  dmDados.FecharConexao(miSsCombo);

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gf_ComboCadastro_Consistir
 *        Verifica se o valor existe no cadastro
 *-----------------------------------------------------------------------------}
function TuntCombos.gf_ComboCadastro_Consistir(piConexao: Integer;
                                               psConteudo: String;
                                               psNomeCampo: String;
                                               psCodigo: String;
                                               psMensagem: String): String;
var
  lsRetorno: String;
  liIndCol: Integer;
begin
  try
    dmDados.giStatus := STATUS_OK;
    liIndCOl := IOPTR_NOPOINTER;

    // procura pelo Campo
//    dmDados.gsRetorno := dmDados.AtivaFiltro(piConexao, psNomeCampo + '= ''' + psConteudo + '''', IOFLAG_FIRST, IOFLAG_GET);
    dmDados.gsRetorno := dmDados.Posiciona(piConexao, psNomeCampo, [psConteudo]);
    // se n�o achou
    if dmDados.Status(piConexao, IOSTATUS_NOROWS) then
    begin
      // acumula menasgem de erro de grava��o
      untTelaVar.gp_TelaVar_Mensagem_Acumular(COD_ERROINVALIDO, psMensagem, psConteudo, VAZIO);
      gbErro := True;
      lsRetorno := VAZIO;
    end
    else
      lsRetorno := dmDados.ValorColuna(piConexao, psCodigo, liIndCOl);

//    dmDados.gsRetorno := dmDados.AtivaFiltro(piConexao, VAZIO, IOFLAG_FIRST, IOFLAG_GET);

    Result := lsRetorno;

  except
    dmDados.ErroTratar('gf_ComboCadastro_Consistir  - uniCOMBOS.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gf_ComboCadastro_Estufa
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
function TuntCombos.gf_ComboCadastro_Estufa(pcCombo: TComboBox;
                                            psCodigoSql: String;
                                            psCampo: String;
                                            piConexao: Integer;
                                            paParm: array of variant): Integer;
var
  lsSQL:          String;
  liConexao:      Integer;
  miIndColCampo: Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    liConexao := piConexao;
    {' se ainda nao estiver aberto}
    if liConexao = 0 then
    begin
      lsSQL := dmDados.SqlVersao(psCodigoSql, paParm);
      if dmDados.giStatus <> STATUS_OK then
      begin
        Result := 0;
        dmDados.ErroTratar('gf_ComboCadastro_Estufa - uniCOMBOS.PAS');
        Exit;
      end;
      liConexao := dmDados.ExecutarSelect(lsSQL);
      if liConexao = IOPTR_NOPOINTER then
      begin
        Result := 0;
        Exit;
      end;
    end;

    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      dmDados.gsRetorno := dmDados.Primeiro(liConexao);

      miIndColCampo := IOPTR_NOPOINTER;

      while not (dmDados.gsRetorno = IORET_EOF) do
      begin
        pcCombo.Items.Add(dmDados.ValorColuna(liConexao, psCampo, miIndColCampo));
        dmDados.gsRetorno := dmDados.Proximo(liConexao);
      end;
    end;

    Result := liConexao;

  except
    Result := 0;
    dmDados.ErroTratar('gf_ComboCadastro_Estufa - uniCOMBOS.PAS');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gf_ComboCadastro_Conteudo
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
Function TuntCombos.gf_ComboCadastro_Conteudo(piConexao: Integer;
                                               psNomeCampo: String;
                                               psCodigo: String;
                                               psCampo: String): String;
var
  lsConteudo: String;
  liIndCol: Integer;

begin

  dmDados.giStatus := STATUS_OK;

  try

    {' inicializa com vazio}
    lsConteudo := VAZIO;

    if piConexao <> 0 then
    begin
      liIndCOl := IOPTR_NOPOINTER;

      {' procura pelo Campo}
//      dmDados.gsRetorno := dmDados.AtivaFiltro(piConexao, psNomeCampo + '= ''' + psCodigo + '''', IOFLAG_FIRST, IOFLAG_GET);
      dmDados.gsRetorno := dmDados.Posiciona(piConexao, psNomeCampo, [psCodigo]);
      {' se achou}
      If Not dmDados.Status(piConexao, IOSTATUS_NOROWS) Then
        {' guarda o Conteudo}
        lsConteudo := dmDados.ValorColuna(piConexao, psCampo, liIndCOl);

//      dmDados.gsRetorno := dmDados.AtivaFiltro(piConexao, VAZIO, IOFLAG_FIRST, IOFLAG_GET);
    end;

    Result := lsConteudo;

  except
    Result := VAZIO;
    dmDados.ErroTratar('gf_ComboCadastro_Conteudo - uniCOMBOS.PAS');
  end;

End;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gf_ComboCadastro_Conteudo
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
Function TuntCombos.gf_ComboCadastro_SpreadEstufa(pSpread: TvaSpread;
                                                  psCodigoSql: String;
                                                  psCampo: String;
                                                  paParm: array of variant): Integer;
var
  lsSQL       : String;
  liConexao   : Integer;
  lsCombo     : String;
  lsColOpcao  : Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try

    lsSQL := dmDados.SqlVersao(psCodigoSql, paParm);
    If dmDados.giStatus <> STATUS_OK Then
    begin
      Result := 0;
      dmDados.ErroTratar('gf_ComboCadastro_SpreadEstufa - uniCOMBOS.PAS');
      Exit;
    End;

    liConexao := dmDados.ExecutarSelect(lsSQL);

    If Not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
    begin
      lsColOpcao := IOPTR_NOPOINTER;

      lsCombo := dmDados.ValorColuna(liConexao, psCampo, lsColOpcao);
      dmDados.gsRetorno := dmDados.Proximo(liConexao);
      {'Efetua La�o de leitura das linhas do Arquivo}
      While not dmDados.Status(liConexao, IOSTATUS_EOF) do
      begin
        lsCombo := lsCombo + Chr(9) + VarToStr(dmDados.ValorColuna(liConexao, psCampo, lsColOpcao));
        {'Le Proxima linha do Arquivo}
        dmDados.gsRetorno := dmDados.Proximo(liConexao);
      end;
    End;

    {'Define cells as type COMBOBOX}
    pSpread.CellType := SS_CELL_TYPE_COMBOBOX;
    pSpread.TypeComboBoxList := lsCombo;
    pSpread.TypeComboBoxEditable := True;

    Result := liConexao
  except
    Result := 0;
    dmDados.ErroTratar('gf_ComboCadastro_SpreadEstufa - uniCOMBOS.PAS');
  end;
End;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gp_Combo_SpreadEstufa       Preenche o combo
 *
 *-----------------------------------------------------------------------------}
procedure TuntCombos.gp_Combo_SpreadEstufa(pSpread: TvaSpread;
                                           psChave: String;
                                           psFiltro: string);
var
  lsSQL: String;
  liColOpcao_Codigo: Integer;
  liColOpcao_Descricao: Integer;
  lvOpcao_Descricao: Variant;
  lsCombo     : String;
Begin

  try
    {'Abre o Arquivo Texto para Leitura dos combos}
    gaParm[0] := TAB_PREFIXO + psChave;
    if psFiltro <> VAZIO then
    begin
      gaParm[1] := psFiltro;
      lsSQL := dmDados.SqlVersao('CB_1002', gaParm);
    end
    else
      lsSQL := dmDados.SqlVersao('CB_1001', gaParm);
    miSsCombo := dmDados.ExecutarSelect(lsSQL);

    if miSsCombo = IOPTR_NOPOINTER then
      Exit;

    {'Efetua La�o de leitura das linhas do Arquivo}
    liColOpcao_Codigo := IOPTR_NOPOINTER;
    liColOpcao_Descricao := IOPTR_NOPOINTER;

    if not dmDados.Status(miSsCombo, IOSTATUS_EOF) then
    begin
      lvOpcao_Descricao := dmDados.ValorColuna(miSsCombo, FLDCMB_DESCRICAO, liColOpcao_Descricao);

      {se nao for nulo}
      if VarIsNull(lvOpcao_Descricao) then
        lsCombo := (VarToStr(dmDados.ValorColuna(miSsCombo, FLDCMB_CODIGO, liColOpcao_Codigo)))
      else
        lsCombo := (VarToStr(dmDados.ValorColuna(miSsCombo, FLDCMB_CODIGO, liColOpcao_Codigo)) +
               ' - ' + dmDados.ValorColuna(miSsCombo, FLDCMB_DESCRICAO, liColOpcao_Descricao));

      dmDados.gsRetorno := dmDados.Proximo(miSsCombo);
      {'Efetua La�o de leitura das linhas do Arquivo}
      While not dmDados.Status(miSsCombo, IOSTATUS_EOF) do
      begin
        lvOpcao_Descricao := dmDados.ValorColuna(miSsCombo, FLDCMB_DESCRICAO, liColOpcao_Descricao);

        {se nao for nulo}
        if VarIsNull(lvOpcao_Descricao) then
          lsCombo := lsCombo + Chr(9) + (VarToStr(dmDados.ValorColuna(miSsCombo, FLDCMB_CODIGO, liColOpcao_Codigo)))
        else
          lsCombo := lsCombo + Chr(9) + (VarToStr(dmDados.ValorColuna(miSsCombo, FLDCMB_CODIGO, liColOpcao_Codigo)) +
                 ' - ' + dmDados.ValorColuna(miSsCombo, FLDCMB_DESCRICAO, liColOpcao_Descricao));
        { se o proximo for final de arquivo, para}
        if (dmDados.Proximo(miSsCombo) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
      end;
    End;

    {'Define cells as type COMBOBOX}
    pSpread.CellType := SS_CELL_TYPE_COMBOBOX;
    pSpread.TypeComboBoxList := lsCombo;
    pSpread.TypeComboBoxEditable := True;

    dmDados.FecharConexao(miSsCombo);

  except
    dmDados.ErroTratar('gp_Combo_SpreadEstufa - uniCOMBOS.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntCombos.gf_Combo_SpreadSemDescricao       Tira o codigo
 *
 *-----------------------------------------------------------------------------}
function TuntCombos.gf_Combo_SpreadSemDescricao(pSpread: TvaSpread): String;
var
  liInicio: Integer;
begin

  dmDados.giStatus := STATUS_OK;
  try
    if Trim(pSpread.Text) <> VAZIO then
    begin
      liInicio := AnsiPos(' ',pSpread.Text);
      {'se o texto do combo n�o estiver formatado "cod - Descricao"}
      if liInicio <> 0 then
      begin
        Result := Copy(pSpread.Text,1,liInicio - 1);
        Exit;
      end
      else
      begin
        Result := '0';
        Exit;
      end;
    end;
  except
    dmDados.ErroTratar('gf_Combo_SpreadSemDescricao - uniCOMBOS.PAS');
  end;

end;

end.
