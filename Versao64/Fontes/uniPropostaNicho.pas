unit uniPropostaNicho;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Mask, StdCtrls, ComCtrls, Variants,
  uniConst, OleCtrls, FPSpread_TLB;

const
  EDT_CODIGO: string = 'mskCodigo';

type
  TfrmPropostaNicho = class(TForm)
    pnlFundo: TPanel;
    grbPropostaNicho: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    mskDataProposta: TMaskEdit;
    chkEmitirBoleto: TCheckBox;
    pagProponentes: TPageControl;
    tabProponentes: TTabSheet;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label13: TLabel;
    Label22: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    edtEndereco: TEdit;
    mskCEP: TMaskEdit;
    edtCidade: TEdit;
    edtBairro: TEdit;
    mskDataNasc: TMaskEdit;
    mskRG: TMaskEdit;
    mskCPF: TMaskEdit;
    mskTelefone: TMaskEdit;
    edtDDD: TEdit;
    edtPropNome: TEdit;
    edtRamal: TEdit;
    edtProfissao: TEdit;
    edtObservacao: TEdit;
    grbPlano: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    cboCodigoPlano: TComboBox;
    cboCodigoCorretor: TComboBox;
    grbPagamento: TGroupBox;
    Label3: TLabel;
    Label17: TLabel;
    Label12: TLabel;
    Label4: TLabel;
    edtValorSinal: TEdit;
    edtNumeroParcelas: TEdit;
    edtValorParcela: TEdit;
    mskVcto1Parcela: TMaskEdit;
    PntComunicador: TPaintBox;
    edtProponente_Estado: TEdit;
    mskDataPlano: TMaskEdit;
    Label6: TLabel;
    sprProponentes: TvaSpread;
    Label5: TLabel;
    edtBloqueada: TEdit;
    Label7: TLabel;
    edtTemOcupacao: TEdit;
    mskCodigo: TMaskEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure cboComboExit(Sender: TObject);
    procedure cboComboKeyPress(Sender: TObject; var Key: Char);
    procedure edtTextoChange(Sender: TObject);
    procedure edtTextoEnter(Sender: TObject);
    procedure edtTextoExit(Sender: TObject);
    procedure sprGridChange(Sender: TObject; Col, Row: Integer);
    procedure sprGridEditChange(Sender: TObject; Col, Row: Integer);
    procedure chkCheckClick(Sender: TObject);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;

    procedure mpMostra_Ocupacao;
  public
    { Public declarations }
  end;

var
  frmPropostaNicho: TfrmPropostaNicho;

implementation

{$R *.DFM}
uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos, uniFuncoes;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.FormClose(Sender: TObject; var Action: TCloseAction);
var
  liConta:  Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - uniPropostaNicho',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;
    gfrmTV := nil;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniPropostaNicho');
  end;

  // destroi a janela
  Action := caFree;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, frmPropostaNicho);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.FormResize(Sender: TObject);
begin
  if TfrmPropostaNicho(Sender).WindowState <> wsMinimized Then
  begin
    if (TfrmPropostaNicho(Sender).Width < pnlFundo.Width + 39) and (TfrmPropostaNicho(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TfrmPropostaNicho(Sender).Width := pnlFundo.Width + 39;

    TfrmPropostaNicho(Sender).Refresh;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
//    mrTelaVar.P.Titulo_Tela := 'Proposta de Nicho';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
    untEdit.gp_Edit_EstufaCombos(mrTelaVar.P.Sigla_Tabela, TForm(Sender));

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);

    mpMostra_Ocupacao;

    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniPropostaNicho');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 3);


end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
      if lcboCombo.Text <> VAZIO then
      begin
        { se foi combo de planos }
        if lcboCombo.Name = CBO_PLANO_CODIGO_PNI then
        begin
          mskDataPlano.Text := Copy(lcboCombo.Text,32,10);
          edtValorSinal.Text := FormatFloat(MK_VALOR, StrToFloat(Copy(lcboCombo.Text,7,7)));
          edtValorParcela.Text := FormatFloat(MK_VALOR, StrToFloat(Copy(lcboCombo.Text,17,7)));
          edtNumeroParcelas.Text := Copy(lcboCombo.Text,27,2);
        end;

        {' altera��o efetuada}
        untEdit.gp_Edit_Alterado(True, mrTelaVar);
      end;
    end
    else
      If (Not mrTelaVar.Reposicionando) Then
        {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboComboClick - uniPropostaNicho');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  lwMes:     Word;
  lwAno:     Word;
  lwDia:     Word;
  liConexao: Integer;
  liIndCol:  array[0..MAX_NUM_COLUNAS] of Integer;
  lsCampo:   string;
  lsSql:     string;
  liConta: Integer;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' guarda o valor original do combo}
    gsAreaDesfaz := lcboCombo.Text;
  giAreaDesfaz := -1;
  if lcboCombo.Items.Count > 0 then
    {'Executa um la�o nos itens comparando-os com o pConteudo}
    for liConta := 0 to lcboCombo.Items.Count - 1 do
      if lcboCombo.Items[liConta] = gsAreaDesfaz then
      begin
        giAreaDesfaz := liConta;
        break;
      end;

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
      { se foi combo de plano }
      if lcboCombo.Name = CBO_PLANO_CODIGO_PNI then
      begin
        DecodeDate(Date(), lwAno, lwMes, lwDia);
        if lwDia <= 15 then
          lwMes := lwMes - 1;
        if lwMes <= 0 then
        begin
          lwMes := lwMes + 12;
          lwAno := lwAno - 1;
        end;
        { planos }
        gaParm[0] := 'N';
        gaParm[1] := IntToStr(lwMes) + '/' + '01' + '/' + IntToStr(lwAno);
        liConexao := 0;
        lsSQL := dmDados.SqlVersao('NEC_0014', gaParm);
        liConexao := dmDados.ExecutarSelect(lsSQL);
        if liConexao = IOPTR_NOPOINTER then
          Exit;

        if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
        begin
          dmDados.gsRetorno := dmDados.Primeiro(liConexao);

          liIndCol[0] := IOPTR_NOPOINTER;
          liIndCol[1] := IOPTR_NOPOINTER;
          liIndCol[2] := IOPTR_NOPOINTER;
          liIndCol[3] := IOPTR_NOPOINTER;
          liIndCol[4] := IOPTR_NOPOINTER;

          lcboCombo.Clear;
          while not (dmDados.gsRetorno = IORET_EOF) do
          begin
            lsCampo := dmDados.ValorColuna(liConexao, 'COD_PLANO', liIndCol[0]);
            lsCampo := lsCampo + '   ' + FormatFloat('0000.00', dmDados.ValorColuna(liConexao, 'VLR_SINAL', liIndCol[1]));
            lsCampo := lsCampo + '   ' + FormatFloat('0000.00', dmDados.ValorColuna(liConexao, 'V_PARCELAS', liIndCol[2]));
            lsCampo := lsCampo + '   ' + FormatFloat('00', dmDados.ValorColuna(liConexao, 'Q_PARCELAS', liIndCol[3]));
            lsCampo := lsCampo + '   ' + FormatDateTime('dd/mm/yyyy', dmDados.ValorColuna(liConexao, 'DATA_INCL', liIndCol[4]));
            lcboCombo.Items.Add(lsCampo);
            dmDados.gsRetorno := dmDados.Proximo(liConexao);
          end;
        end;
        dmDados.FecharConexao(liConexao);
      end;
    end;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboComboEnter - uniPropostaNicho');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.cboComboKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.cboComboKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.edtTextoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.edtTextoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = EDT_CODIGO) And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    {' atualiza o caption da janela}
    Caption := mrTelaVar.P.Titulo_Tela + ' ' + ledtEdit.Text + ' - Inclus�o';
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.edtTextoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.edtTextoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprGridChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.sprGridChange(Sender: TObject; Col,
  Row: Integer);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.sprGridEditChange       Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.sprGridEditChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
  lgrdGrid.Col := Col;
  if lgrdGrid.CellType = SS_CELL_TYPE_COMBOBOX then
    untCombos.gp_Combo_SpreadSeleciona(lgrdGrid, 0);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.chkCheckClick       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.chkCheckClick(Sender: TObject);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.PntComunicadorPaint(Sender: TObject);
var
  liComandoMDI: Integer;
begin

  liComandoMDI := giComando_MDI;

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, frmPropostaNicho);

  if (liComandoMDI = TV_COMANDO_IR_PROXIMO) or
     (liComandoMDI = TV_COMANDO_IR_ANTERIOR) or
     (liComandoMDI = TV_COMANDO_IR_PRIMEIRO) or
     (liComandoMDI = TV_COMANDO_PROCURAR) or
     (liComandoMDI = TV_COMANDO_IR_ULTIMO) then
  begin
    mpMostra_Ocupacao;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaNicho.edtTextoExit       quando perde o foco
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.edtTextoExit(Sender: TObject);
var
  ledtEdit: TMaskEdit;
  i, j:  Integer;
begin

  ledtEdit := TMaskEdit(Sender);

  if Sender is TMaskEdit then
  begin
    { procura pela entidade }
    i := untTelaVar.gf_TelaVar_Retorna_Tabela(mrTelaVar.P.Sigla_Tabela);
    { se nao achou }
    if i = 0 then
      Exit;

    j := 1;
    { percorre todos os campos da tabela principal }
    while gaLista_Campos[i][j].sSigla_Tabela = gaLista_Parametros[i].Sigla_Tabela do
    begin
      if gaLista_Campos[i][j].sNome_Controle = ledtEdit.Name then
      begin
        if gaLista_Campos[i][j].sAlinha = ALINHA_ZERO then
          ledtEdit.Text := untFuncoes.gf_Zero_Esquerda(ledtEdit.Text, gaLista_Campos[i][j].iTamanho);
        break;
      end;
      j := j + 1;
    end;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.cboComboExit(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpMostra_Ocupacao       Mostra se tem ocupacao
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaNicho.mpMostra_Ocupacao();
var
  lsSql: string;
  liConexao: Integer;
  aSqlParms:  array[0..2] of Variant;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

//  aSqlParms[0] := 'Codigo';
//  aSqlParms[1] := 'NichoPes';
//  aSqlParms[2] := 'Substring(Codigo,1,8) = ''' + mskCodigo.Text + '''';
//  lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);
  aSqlParms[0] := 'NichoPes';
  aSqlParms[1] := mskCodigo.Text;
  lsSql := dmDados.SqlVersao('NEC_0024', aSqlParms);

  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;
  { se encontrou a op��o }
  if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
    edtTemOcupacao.Text := 'S'
  else
    edtTemOcupacao.Text := 'N';
  dmDados.FecharConexao(liConexao);

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;

end;

end.
