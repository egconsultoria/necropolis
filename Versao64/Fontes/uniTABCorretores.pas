unit uniTABCorretores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls,
  uniConst, StdCtrls;

type
  TfrmTABCorretores = class(TForm)
    pnlFundo: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    Edit2: TEdit;
    Edit1: TEdit;
    ComboBox1: TComboBox;
    CheckBox1: TCheckBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label4: TLabel;
    ComboBox2: TComboBox;
    Edit3: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;
  public
    { Public declarations }
  end;

var
  frmTABCorretores: TfrmTABCorretores;

implementation

{$R *.DFM}
uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit;

procedure TfrmTABCorretores.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

procedure TfrmTABCorretores.FormClose(Sender: TObject; var Action: TCloseAction);

var
  liConta:  Integer;
begin
  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - FORMENDERECO.FRM',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      grParam_TelaVar.DynaTempID[1] := 0;
      grParam_TelaVar.DynaTempID[2] := 0;
      grParam_TelaVar.DynaTempID[3] := 0;
      grParam_TelaVar.DynaTempID[4] := 0;
      grParam_TelaVar.DynaTempID[5] := 0;
      grParam_TelaVar.DynaTempID[6] := 0;
      grParam_TelaVar.DynaTempID[7] := 0;
      grParam_TelaVar.DynaTempID[8] := 0;
      grParam_TelaVar.DynaTempID[9]:= 0;
      grParam_TelaVar.DynaTempID[10] := 0;
      grParam_TelaVar.DynaTempID[11] := 0;
      grParam_TelaVar.DynaTempID[12] := 0;
      gfrmTV := nil;
    End;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[1]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[2]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[3]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[4]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[5]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[6]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[7]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[8]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[9]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[10]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[11]);
    dmDados.FecharConexao(mrTelaVar.DynaTempID[12]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uni.PAS');
  end;

  // destroi a janela
  Action := caFree;
end;
procedure TfrmTABCorretores.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, CAD_TAB_CORRETOR, frmTABCorretores);

end;

procedure TfrmTABCorretores.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

procedure TfrmTABCorretores.FormResize(Sender: TObject);
begin
  if TfrmTABCorretores(Sender).WindowState <> wsMinimized Then
  begin
    if (TfrmTABCorretores(Sender).Width < pnlFundo.Width + 39) and (TfrmTABCorretores(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TfrmTABCorretores(Sender).Width := pnlFundo.Width + 39;

    TfrmTABCorretores(Sender).Refresh;
  end;
end;

procedure TfrmTABCorretores.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    { redimensiona a tela
    Width  := 527;
    Height := 276;}

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
    mrTelaVar.P.Titulo_Tela := 'Tabela de Corretores';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
{    lp_End_EstufaCombos;}

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, CAD_TAB_CORRETOR, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, CAD_TAB_CORRETOR, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);
    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uni');
  end;
end;













end.
