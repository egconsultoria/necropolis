�
 TFRMENTRADAMANUAL 0�(  TPF0TfrmEntradaManualfrmEntradaManualLeftTopCWidthHeight�CaptionEntrada ManualColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	FormStyle
fsMDIChildOldCreateOrder	PositionpoScreenCenterVisible	WindowStatewsMaximized
OnActivateFormActivateOnClose	FormCloseOnCloseQueryFormCloseQueryOnDeactivateFormDeactivateOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlFundoLeftTopWidth�HeightyTabOrder  TLabel	lblCodigoLeftTopWidth!HeightCaption   Código  TLabellblDescricaoLeft� TopWidth0HeightCaption   Descrição  	TPaintBoxPntComunicadorLeftTop Width�HeightyOnPaintPntComunicadorPaint  	TGroupBoxgrpParcelasLeftTop1Width�HeightATabOrder TLabellblDataLancamentoLeftTopWidthUHeightCaption   Data Lançamento  TLabellblNumeroPropostaLeftpTopWidthFHeightCaptionNum. Proposta  TLabellblTipoDocumentoLeft8TopWidthOHeightCaptionTipo Documento  TLabellblNumeroParcelaLeft�TopWidth]HeightCaptionNum. Parcela/Taxa  TLabellblTipoRecebimentoLeft� TopWidthWHeightCaptionTipo Recebimento  TLabel	lblReciboLeftTopWidth"HeightCaptionRecibo  	TMaskEditmskDataLancamentoLeftTop WidthPHeightEditMask!99/99/0000;1; 	MaxLength
TabOrder Text
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TGroupBoxgrpRecebimentoLeftTop� Width9HeightyTabOrder TLabellblDataRecebimentoLeftTopWidthYHeightCaptionData Recebimento  TLabellblDataCreditoLeftoTopWidthJHeightCaptionData de Credito  TLabellblValorReceberLeft� TopWidthMHeightCaptionValor a Receber  TLabellblValorDescontoLeft	TopHWidthXHeightCaptionValor de Desconto  TLabellblValorRecebidoLeftpTopHWidthIHeightCaptionValor Recebido  TLabellblSaldoDevedorLeft� TopHWidthGHeightCaptionSaldo Devedor  	TMaskEditmskValorReceberLeft� Top(WidthQHeightEnabledTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorDescontoLeftTopXWidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorRecebidoLeftpTopXWidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskSaldoDevedorLeft� TopXWidthQHeightEnabledTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataRecebimentoLeftTop(WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrder Text  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataCreditoLeftpTop(WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrderText  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TGroupBoxgrpRecebParcialLeftXTop� Width�HeightyCaptionRecebimentos ParciaisTabOrder	 	TvaSpreadsprRecebParcialLeftTopWidthyHeightaTabOrder 
OnKeyPresscboComboKeyPressOnChangesprGridChangeOnEditChangesprGridEditChangeControlData
�     �&  
  @                         R������ � K�Q   �DB MS Sans Serif��� ���    
                      �  �  �    e     ? \    9          �            MS Sans Serif ����        �             MS Sans Serif               ����MbP?E    q   r   s   G     Q =    �    �  �9          �            MS Sans Serif J    P    S      T      W 
       X                h      333333�?��d             
   B          �����������         �������� %      �  �  �  �*    ���������   <      F    ����������������I      U      V    ����                @0        �����          ����       {�G�z@,       ���� A     ���    + -      ���� A           �����חAR . , ) &      ���� R e /    4  + -      ���� A           �����חAR . , ) &      ���� R e /  l  4         ��Q��3@M       ����    �   ^       ����M       ����    �   ^       ����0    ����    �   8           Parcela 8           Valor Venc 8           Data Venc 8           Valor Rec. 8           Data Rec. 8           Hist�rico 8           Tipo Cobr 8           Tipo Rec.         ����                         �               	TGroupBoxgrpValorLeftXTop@Width�HeightyTabOrder TLabellblValorOriginalLeftxTopWidth>HeightCaptionValor Original  TLabellblVencimentoLeftTopWidthaHeightCaptionData de Vencimento  TLabellblJurosLeftTop@WidthHeightCaptionJuros  TLabellblMultaLeftxTop@WidthHeightCaptionMulta  TLabellblRecebParcialLeft@Top@Width:HeightCaptionRec. Parcial  TLabellblCaixaBancoLeft@TopWidthHeightCaptionCx/Bc  TLabellblPagtoLeft`TopWidthHeightCaptionPagto  TLabellblTipoCobrancaLeft� TopWidthFHeightCaption   Tipo Cobrança  TLabellblTipoTaxaLeft� Top@Width0HeightCaption	Tipo Taxa  	TMaskEditmskVencimentoLeftTop WidthYHeightEditMask!99/99/00;1;_	MaxLengthTabOrder Text  /  /  OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskValorOriginalLeftxTop WidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskJurosLeftTopPWidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskMultaLeftxTopPWidthYHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TMaskEditmskRecebParcialLeft@TopPWidth9HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtCaixaBancoLeft@Top WidthHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtPagtoLeft`Top WidthHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboTipoCobrancaLeft� Top WidthYHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TComboBoxcboTipoTaxaLeft� TopPWidthYHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress   	TGroupBoxgrpDocumentoLeftTop@Width9HeightyCaption Documento TabOrder TLabellblDocumentoLeftTopWidth7HeightCaption	Documento  TLabellblHistoricoLeftTopHWidth)HeightCaption
   Histórico  TLabellblNossoNumeroLeft� TopWidthAHeightCaption   N Número - U  TLabellblNossoNumeroSLeft� TopWidth@HeightCaption   N Número - S  TLabellblNossoNumeroBLeft� TopHWidth@HeightCaption   N Número - B  	TMaskEditmskDocumentoLeftTop(Width� HeightEditMask9999999-9\/999.9999;1;_	MaxLengthTabOrder Text       - /   .    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtHistoricoLeftTopXWidth� HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtNossoNumeroLeft� Top(WidthIHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtNossoNumeroSLeft� Top(WidthIHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtNossoNumeroBLeft� TopXWidthIHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TMaskEditmskNumeroPropostaLeftpTop WidthPHeightEditMask9999999-9;1;_	MaxLength	TabOrderText	       - OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TComboBoxcboTipoDocumentoLeft8Top WidthiHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TComboBoxcboTipoRecebimentoLeft� Top WidthYHeight
ItemHeightSorted	TabOrderOnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskNumeroParcelaLeft�Top WidthPHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  	TGroupBoxgrpBloqueadaLefthTop WidthyHeightACaption	BloqueadaTabOrder
 TLabellblBloqueadaLeftTopWidthHeightCaptionTipo:  TLabellblDataBloqueadaLeftTop(WidthHeightCaptionData:  	TComboBoxcboBloqueadaLeft)TopWidthHHeightEnabled
ItemHeightSorted	TabOrder OnChangecboComboChangeOnClickcboComboClickOnEntercboComboEnterOnExitcboComboExit
OnKeyPresscboComboKeyPress  	TMaskEditmskDataBloqueadaLeft(Top(WidthIHeightEnabledEditMask99/99/9999;1; 	MaxLength
TabOrderText
  /  /    OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   TEdit	edtReciboLeftTop WidthIHeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress   	TMaskEdit	mskCodigoLeftTopWidth� HeightTabOrder OnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress  TEditedtDescricaoLeft� TopWidth1HeightTabOrderOnChangeedtTextoChangeOnEnteredtTextoEnterOnExitedtTextoExit
OnKeyPresscboComboKeyPress    