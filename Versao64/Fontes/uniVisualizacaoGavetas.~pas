unit uniVisualizacaoGavetas;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, jpeg, Animate, GIFCtrl, RXGif, OleCtrls, FPSpreadADO_TLB;

{*======================================================================
 *            CONSTANTES
 *======================================================================}

 {*======================================================================
 *            RECORDS
 *======================================================================}
type
{ *======================================================================}
  // Records - OCUPACAO
{ *======================================================================}
 {*----------------------------------------------------------------------
 * Estrutura de dados da Ocupacao
 *}
  TumaOcupacao = Record
    iLugar:  Integer;
    sNome:  string;
    sSobrenome: string;
    sTipoMov: string;
    dData: tDateTime;
  end;

{ *======================================================================}
  // Records - GAVETAS
{ *======================================================================}
 {*----------------------------------------------------------------------
 * Estrutura das Gavetas
 *}
  TumaGaveta = Record
    iNumGaveta:  Integer;
    iTotOcupacao:  Integer;
    arOcupacao: array[1..15] of TumaOcupacao;
  end;

{*======================================================================
 *            CLASSES
 *======================================================================}
  TformVisualGaveta = class(TForm)
    pnlFundo: TPanel;
    Panel1: TPanel;
    pnlAmbiente: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    lblQuadra: TLabel;
    Label3: TLabel;
    lblSetor: TLabel;
    Label5: TLabel;
    lblLote: TLabel;
    lblTipoGaveta: TLabel;
    Label8: TLabel;
    Label14: TLabel;
    lblOcupacoes: TLabel;
    lblGavetaLivre: TLabel;
    GroupBox4: TGroupBox;
    Label13: TLabel;
    Panel6: TPanel;
    GroupBox1: TGroupBox;
    lblOcupado: TLabel;
    lblLivre: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    lblNumProp: TLabel;
    lblNomeProp: TLabel;
    Label19: TLabel;
    sprNomes: TvaSpread;
    ptbAndar4: TPaintBox;
    ptbAndar9: TPaintBox;
    ptbAndar14: TPaintBox;
    ptbAndar3: TPaintBox;
    ptbAndar8: TPaintBox;
    ptbAndar13: TPaintBox;
    ptbAndar2: TPaintBox;
    ptbAndar7: TPaintBox;
    ptbAndar12: TPaintBox;
    ptbAndar1: TPaintBox;
    ptbAndar6: TPaintBox;
    ptbAndar11: TPaintBox;
    ptbTeste: TPaintBox;
    btnImprimir: TButton;
    ptbAndar5: TPaintBox;
    ptbAndar10: TPaintBox;
    ptbAndar15: TPaintBox;
    procedure FormShow(Sender: TObject);
    procedure ptbAndar4Paint(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
    maOcupacoes: array[1..99] of TumaGaveta;
    maCondominio: array[1..3] of string;
    mbCondominio: Boolean;
    msTipoGaveta: string;
    miTotGaveta: Integer;
    mnTotLote: Double;
    miTotAndar: Integer;
    mbPrimeiraVez : Boolean;
    msNumProp: string;
  public
    { Public declarations }
  end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  formVisualGaveta: TformVisualGaveta;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const
  ANDAR_ALTURA = 70;
  ANDAR_LARGURA = 120;
  ANDAR_PROFUNDIDADE = 21;
  ANDAR_LATERAL = 34;

//  DIRETORIO_FIGURAS = 'Z:\windows\gavetas\';

  COR_TOPO = 39;
  COR_ESQUERDA = 5;
  COR_DIREITA = 38;
  COR_FUNDO = 6;

  FONTE_ESQUERDA = 6;
  FONTE_TOPO = 26;
  FONTE_LARGURA = 15;
  FONTE_ALTURA = 11;

  COR_DEGRADE = $32;
  COR_DEGRADE_AZUL = $100;

  TIPO_STANDARD = 'ST';
  TIPO_NOBRENORMAL = 'NN';
  TIPO_NOBREPREF = 'NP';
  TIPO_TRADICIONAL = 'TR';

  TIPO_STANDARD_4 = 'S4';
  TIPO_STANDARD_5 = 'S5';
  TIPO_STANDARD_6 = 'S6';

  TIPO_NOBREANTIGO = 'NA';
  TIPO_NOBREPREF1 = 'N1';
  TIPO_NOBREPREF2 = 'N2';
  TIPO_NOBREPREF3 = 'N3';

  TAMANHO_NOBREPREF = 25;

  ANOS_OCUPACAO = '3';

  TOTAL_GAVETAS = 99;

implementation

uses uniConst, uniDados, uniGlobal;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TformVisualGaveta.FormShow
 *        Mostra o form
 *-----------------------------------------------------------------------------}
procedure TformVisualGaveta.FormShow(Sender: TObject);
var
  liConta     : Integer;
  liOcupante  : Integer;
  liLinha     : Integer;
  liDegrade   : Integer;
  wAno1, wAno2, wmes1, wMes2, wDia1, wDia2: Word;
  lCor        : TColor;
  liConexao   : Integer;
  liIndCol    : array[0..20] of Integer;
  lsSQL       : string;
  liTotGaveta : Integer;
  liNumGaveta : Integer;
  liGavetaAnterior : Integer;

begin

  { para desenhar as gavetas quando entra }
  mbPrimeiraVez := True;

  msNumProp := gsNumProposta;

  mbCondominio := False;

  { mostra o numero da proposta }
//  lblNumProp.Caption := Copy(msNumProp,1,7) + '-' + Copy(msNumProp,8,1);
  lblNumProp.Caption := msNumProp;
  lblOcupado.Caption := '0 - ' + ANOS_OCUPACAO;
  lblLivre.Caption := '+ ' + ANOS_OCUPACAO;

  for liConta := 0 to High(liIndCol) do
    liIndCol[liConta] := IOPTR_NOPOINTER;

  { ler os registros da proposta de gaveta }
  gaParm[0] := 'PROPGAVE';
  gaParm[1] := 'NUM_PROP = ''' + msNumProp + '''';
  gaParm[2] := 'NUM_PROP';
  lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
  if dmDados.giStatus <> STATUS_OK then
    Exit;
  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;

  dmDados.gsRetorno := dmDados.Primeiro(liConexao);
  if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
  begin
    { pega o conteudo do campo }
    lblQuadra.Caption := dmDados.ValorColuna(liConexao, 'QUADRA', liIndCol[1]);
    lblSetor.Caption := dmDados.ValorColuna(liConexao, 'SETOR', liIndCol[2]);
    lblLote.Caption := dmDados.ValorColuna(liConexao, 'LOTE', liIndCol[3]);
    msTipoGaveta := dmDados.ValorColuna(liConexao, 'TIPO_GAVE', liIndCol[4]);
    if (msTipoGaveta = TIPO_NOBREPREF1) or
       (msTipoGaveta = TIPO_NOBREPREF2) or
       (msTipoGaveta = TIPO_NOBREPREF3) then
    begin
//      msTipoGaveta := TIPO_NOBREPREF;
      mbCondominio := True;
      dmDados.FecharConexao(liConexao);
      { ler os registros do condominio }
      gaParm[0] := 'PROPANDR';
      gaParm[1] := 'NUM_PROP = ''' + msNumProp + '''';
      gaParm[2] := 'NUM_PROP';
      lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
      if dmDados.giStatus <> STATUS_OK then
        Exit;
      liConexao := dmDados.ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;

      dmDados.gsRetorno := dmDados.Primeiro(liConexao);
      if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
      begin
        maCondominio[1] := dmDados.ValorColuna(liConexao, 'ANDAR1', liIndCol[14]);
        maCondominio[2] := dmDados.ValorColuna(liConexao, 'ANDAR2', liIndCol[15]);
        maCondominio[3] := dmDados.ValorColuna(liConexao, 'ANDAR3', liIndCol[16]);
      end
      else
      begin
        maCondominio[1] := VAZIO;
        maCondominio[2] := VAZIO;
        maCondominio[3] := VAZIO;
      end;
    end;
  end
  else
  begin
    { limpa os campos }
    lblQuadra.Caption := VAZIO;
    lblSetor.Caption := VAZIO;
    lblLote.Caption := VAZIO;
    msTipoGaveta := VAZIO;
  end;
  dmDados.FecharConexao(liConexao);

  { ler os registros do nome do proponente }
  gaParm[0] := 'PROPONEN';
  gaParm[1] := 'NUM_PROP = ''' + msNumProp + '''';
  gaParm[2] := 'NUM_PROP';
  lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
  if dmDados.giStatus <> STATUS_OK then
    Exit;
  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;

  dmDados.gsRetorno := dmDados.Primeiro(liConexao);
  if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
  begin
    { pega o conteudo do campo }
    lblNomeProp.Caption := dmDados.ValorColuna(liConexao, 'NOME_PROP', liIndCol[5]);
  end
  else
  begin
    { limpa os campo }
    lblNomeProp.Caption := VAZIO;
  end;
  dmDados.FecharConexao(liConexao);

  { ler os registros do tipo de gaveta }
  gaParm[0] := 'TAB_GAVE';
  gaParm[1] := 'CODIGO = ''' + msTipoGaveta + '''';
  gaParm[2] := 'CODIGO';
  lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
  if dmDados.giStatus <> STATUS_OK then
    Exit;
  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;

  dmDados.gsRetorno := dmDados.Primeiro(liConexao);
  if (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) then
  begin
    { pega o conteudo do campo }
    lblTipoGaveta.Caption := dmDados.ValorColuna(liConexao, 'DESCRICAO', liIndCol[6]);
    liTotGaveta := dmDados.ValorColuna(liConexao, 'NUM_GAVE', liIndCol[7]);
    miTotGaveta := liTotGaveta;
    mnTotLote := dmDados.ValorColuna(liConexao, 'REF_STAND', liIndCol[17]);
    miTotAndar := dmDados.ValorColuna(liConexao, 'ANDAR', liIndCol[18]);
  end
  else
  begin
    { limpa os campos }
    lblTipoGaveta.Caption := VAZIO;
    liTotGaveta := 0;
  end;
  dmDados.FecharConexao(liConexao);

  { ler os registros dos ocupantes das gavetas }
//  gaParm[0] := 'GAVEPESS';
//  gaParm[1] := 'CONTRATO = ''' + msNumProp + '''';
//  gaParm[2] := 'CONTRATO, NUM_GAVETA, LUGAR';
  gaParm[0] := 'GAVETA, GAVEPESS';
  gaParm[1] := 'GAVETA.CONTRATO = ''' + msNumProp + '''';
  gaParm[1] := gaParm[1] + ' and GAVETA.CODIGO = GAVEPESS.CODIGO';
  gaParm[1] := gaParm[1] + ' and GAVEPESS.TIPO_MOV <> ''E''';
  gaParm[2] := 'GAVEPESS.CODIGO, LUGAR';
  lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
  if dmDados.giStatus <> STATUS_OK then
    Exit;
  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;

  { limpa o vetor de ocupacao }
  for liConta := 1 to High(maOcupacoes) do
  begin
    maOcupacoes[liConta].iTotOcupacao := 0;
  end;
  liGavetaAnterior := 0;
  liNumGaveta := 0;
  liConta := 1;
  dmDados.gsRetorno := dmDados.Primeiro(liConexao);
  while (dmDados.gsRetorno <> IORET_EOF) and (dmDados.gsRetorno <> IORET_NOROWS) do
  begin
    { pega o conteudo do campo }
    liNumGaveta := StrToInt(dmDados.ValorColuna(liConexao, 'NUM_GAVETA', liIndCol[8]));
    { se for menor que o permitido }
    if liNumGaveta <= TOTAL_GAVETAS then
    begin
      { se mudou de gaveta }
      if (liNumGaveta <> liGavetaAnterior) and (liGavetaAnterior > 0) then
      begin
        maOcupacoes[liGavetaAnterior].iNumGaveta := liGavetaAnterior;
        maOcupacoes[liGavetaAnterior].iTotOcupacao := liConta - 1;
        liConta := 1;
      end;
      maOcupacoes[liNumGaveta].arOcupacao[liConta].iLugar := StrToInt(dmDados.ValorColuna(liConexao, 'LUGAR', liIndCol[9]));
      maOcupacoes[liNumGaveta].arOcupacao[liConta].sNome := dmDados.ValorColuna(liConexao, 'NOME', liIndCol[10]);
      maOcupacoes[liNumGaveta].arOcupacao[liConta].sSobrenome := dmDados.ValorColuna(liConexao, 'SOBRENOME', liIndCol[11]);
      maOcupacoes[liNumGaveta].arOcupacao[liConta].sTipoMov := dmDados.ValorColuna(liConexao, 'TIPO_MOV', liIndCol[12]);
      maOcupacoes[liNumGaveta].arOcupacao[liConta].dData := dmDados.ValorColuna(liConexao, 'DATA_MOV', liIndCol[13]);
    end;
    liGavetaAnterior := liNumGaveta;
    liConta := liConta + 1;
    dmDados.gsRetorno := dmDados.Proximo(liConexao);
  end;
  {  se tem gavetas ocupadas }
  if liNumGaveta > 0 then
  begin
    maOcupacoes[liNumGaveta].iNumGaveta := liNumGaveta;
    maOcupacoes[liNumGaveta].iTotOcupacao := liConta - 1;
  end;
  dmDados.FecharConexao(liConexao);

  { guarda as ocupacoes }
  liOcupante := 0;
  liNumGaveta := 0;
  for liConta := 1 to 8 do
  begin
    { se a gaveta nao esta vazia conta }
    if maOcupacoes[liConta].iTotOcupacao <> 0 then
      liNumGaveta := liNumGaveta + 1;
    liOcupante := liOcupante + maOcupacoes[liConta].iTotOcupacao;
  end;
  lblOcupacoes.Caption := IntToStr(liOcupante);
  lblGavetaLivre.Caption := IntToStr(liTotGaveta - liNumGaveta);

  { aciona o paint do paint box desenha os andares }
  ptbAndar4.Invalidate;

  { guarda a data atual }
  DecodeDate(Date, wAno1, wMes1, wDia1);

  { limpa o spread }
  TvaSpread(FindComponent('sprNomes')).Row := 1;
  TvaSpread(FindComponent('sprNomes')).Col := 1;
  TvaSpread(FindComponent('sprNomes')).Row2 := TvaSpread(FindComponent('sprNomes')).MaxRows;
  TvaSpread(FindComponent('sprNomes')).Col2 := TvaSpread(FindComponent('sprNomes')).MaxCols;
  TvaSpread(FindComponent('sprNomes')).BlockMode := True;
  TvaSpread(FindComponent('sprNomes')).Action := SS_ACTION_CLEAR;
  TvaSpread(FindComponent('sprNomes')).BlockMode := False;

  liLinha := 1;
  { percorre todas as gavetas possiveis }
  for liConta := 1 to 8 do
  begin
    { limpa o degrade }
    liDegrade := $0;
    { percorre todas as ocupacoes }
    for liOcupante := 1 to maOcupacoes[liConta].iTotOcupacao do
    begin
      { se ultrapassou o tamanho do spread }
      if liLinha > TvaSpread(FindComponent('sprNomes')).MaxRows then
        TvaSpread(FindComponent('sprNomes')).MaxRows := liLinha;

      { limpa a cor }
      lCor := clWhite;
      { se for reinumacao }
      if maOcupacoes[liConta].arOcupacao[liOcupante].sTipoMov = 'R' then
      begin
        lCor := clBlue + (liDegrade * COR_DEGRADE_AZUL) + liDegrade;
        liDegrade := liDegrade + COR_DEGRADE;
      end
      else
      begin
        DecodeDate(maOcupacoes[liConta].arOcupacao[liOcupante].dData, wAno2, wMes2, wDia2);
        { se nao passou tres anos }
        if (wAno1 - wAno2 < StrToInt(ANOS_OCUPACAO)) or ((wAno1 - wAno2 = StrToInt(ANOS_OCUPACAO)) and (wMes1 <= wMes2)) then
          lCor := clRed
        else
          lCor := clGreen;
      end;

      TvaSpread(FindComponent('sprNomes')).Row := liLinha;
      TvaSpread(FindComponent('sprNomes')).Col := 1;
      TvaSpread(FindComponent('sprNomes')).Text := '0' + IntToStr(maOcupacoes[liConta].iNumGaveta);
      TvaSpread(FindComponent('sprNomes')).Col := 2;
      TvaSpread(FindComponent('sprNomes')).BackColor := lCor;
      TvaSpread(FindComponent('sprNomes')).Col := 3;
      TvaSpread(FindComponent('sprNomes')).Text := Trim(maOcupacoes[liConta].arOcupacao[liOcupante].sNome) + ' ' + Trim(maOcupacoes[liConta].arOcupacao[liOcupante].sSobrenome);
      TvaSpread(FindComponent('sprNomes')).Col := 4;
      TvaSpread(FindComponent('sprNomes')).Text := FormatDateTime('dd/mm/yyyy', maOcupacoes[liConta].arOcupacao[liOcupante].dData);

      liLinha := liLinha + 1;
    end;
  end;

  { lista o ossario se existir }
  if maOcupacoes[TOTAL_GAVETAS].iTotOcupacao <> 0 then
  begin
    liLinha := TvaSpread(FindComponent('sprNomes')).DataRowCnt + 1;
    { limpa o degrade }
    liDegrade := $0;
    { percorre todas as ocupacoes }
    for liOcupante := 1 to maOcupacoes[TOTAL_GAVETAS].iTotOcupacao do
    begin
      { se ultrapassou o tamanho do spread }
      if liLinha > TvaSpread(FindComponent('sprNomes')).MaxRows then
        TvaSpread(FindComponent('sprNomes')).MaxRows := liLinha;

      { limpa a cor }
      lCor := clWhite;
      { se for reinumacao }
      if maOcupacoes[TOTAL_GAVETAS].arOcupacao[liOcupante].sTipoMov = 'R' then
      begin
        lCor := clBlue + (liDegrade * COR_DEGRADE_AZUL) + liDegrade;
        liDegrade := liDegrade + COR_DEGRADE;
      end
      else
      begin
        DecodeDate(maOcupacoes[TOTAL_GAVETAS].arOcupacao[liOcupante].dData, wAno2, wMes2, wDia2);
        { se nao passou tres anos }
        if (wAno1 - wAno2 < StrToInt(ANOS_OCUPACAO)) or ((wAno1 - wAno2 = StrToInt(ANOS_OCUPACAO)) and (wMes1 <= wMes2)) then
          lCor := clRed
        else
          lCor := clGreen;
      end;

      TvaSpread(FindComponent('sprNomes')).Row := liLinha;
      TvaSpread(FindComponent('sprNomes')).Col := 1;
      TvaSpread(FindComponent('sprNomes')).Text := IntToStr(maOcupacoes[TOTAL_GAVETAS].iNumGaveta);
      TvaSpread(FindComponent('sprNomes')).Col := 2;
      TvaSpread(FindComponent('sprNomes')).BackColor := lCor;
      TvaSpread(FindComponent('sprNomes')).Col := 3;
      TvaSpread(FindComponent('sprNomes')).Text := Trim(maOcupacoes[TOTAL_GAVETAS].arOcupacao[liOcupante].sNome) + ' ' + Trim(maOcupacoes[TOTAL_GAVETAS].arOcupacao[liOcupante].sSobrenome);
      TvaSpread(FindComponent('sprNomes')).Col := 4;
      TvaSpread(FindComponent('sprNomes')).Text := FormatDateTime('dd/mm/yyyy', maOcupacoes[TOTAL_GAVETAS].arOcupacao[liOcupante].dData);

      liLinha := liLinha + 1;
    end;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformVisualGaveta.ptbAndar1Paint
 *        Desenha os andares da gaveta
 *-----------------------------------------------------------------------------}
procedure TformVisualGaveta.ptbAndar4Paint(Sender: TObject);
var
  liConta : Integer;
  liOcupante : Integer;
  lfAndar: array[1..7] of TGifImage;
  wAno1, wAno2, wmes1, wMes2, wDia1, wDia2: Word;
  liDegrade: Integer;
  liEsquerda: Integer;
  liDireita: Integer;
  lsDirFiguras: string;

  lFrame: array[1..7] of TGIFFrame;
  lDrawRect: TRect;
  lDrawFont: TRect;
  lCor: TColor;
  laAndar: array[1..10] of Integer;
begin

  {  se nao tem contrato de gaveta }
  if msTipoGaveta = VAZIO then
    Exit;

  { se nao eh a aprimeira vez sai }
  if not mbPrimeiraVez then
    Exit;

  { para evitar ficar redesenhando as gavetas }
  mbPrimeiraVez := False;

  { guarda os numeros dos andares }
  if (msTipoGaveta = TIPO_STANDARD) or (msTipoGaveta = TIPO_NOBRENORMAL) or (msTipoGaveta = TIPO_TRADICIONAL) then
  begin
    laAndar[1] := 3;
    laAndar[2] := 2;
    laAndar[3] := 1;
  end;
  { guarda os numeros dos andares }
  if (msTipoGaveta = TIPO_NOBRENORMAL) or (msTipoGaveta = TIPO_TRADICIONAL) then
  begin
    laAndar[4] := 6;
    laAndar[5] := 5;
    laAndar[6] := 4;
  end;
  { guarda os numeros dos andares }
  if (msTipoGaveta = TIPO_TRADICIONAL) then
  begin
    laAndar[7] := 9;
    laAndar[8] := 8;
    laAndar[9] := 7;
  end;
  { guarda os numeros dos andares }
  if (msTipoGaveta = TIPO_STANDARD_4) or (msTipoGaveta = TIPO_STANDARD_5) or (msTipoGaveta = TIPO_STANDARD_6) then
  begin
    laAndar[1] := 4;
    laAndar[2] := 3;
    laAndar[3] := 2;
    laAndar[4] := 1;
  end;
  for liConta :=  1 to 10 do
    laAndar[liConta] := liConta;

  { cria as imagens GIF }
  for liConta := 1 to 7 do
    lfAndar[liConta] := TGIFImage.Create;

  lsDirFiguras := dmDados.ConfigValor('DirFiguras');

  { carrega os arquivos do disco }
  lfAndar[1].LoadFromFile(lsDirFiguras + 'andar.gif');


  lfAndar[2].LoadFromFile(lsDirFiguras + 'livre_fundo2.gif');
  lfAndar[3].LoadFromFile(lsDirFiguras + 'livre2.gif');
  lfAndar[4].LoadFromFile(lsDirFiguras + 'livre_topo2.gif');

  lfAndar[5].LoadFromFile(lsDirFiguras + 'livre_fundo4.gif');
  lfAndar[6].LoadFromFile(lsDirFiguras + 'livre4.gif');
  lfAndar[7].LoadFromFile(lsDirFiguras + 'livre_topo4.gif');

  { guarda os frames das imagens }
  for liConta := 1 to 7 do
    lFrame[liConta] := lfAndar[liConta].Frames[0];

  { limpa a cor }
  lCor := clWhite;

  { guarda a data atual }
  DecodeDate(Date, wAno1, wMes1, wDia1);

  { desenha as quatro primeiras gavetas }
  for liConta := 1 to 5 do
  begin
    { se nao for 3 andares ou se ja desenhou tres andares }
    if (liConta <= miTotAndar) then
    begin
      lDrawRect := TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).ClientRect;
      lFrame[1].Draw(TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas, lDrawRect, True);
      lCor := clWhite;
      { se for condominio }
      if mbCondominio then
      begin
        lCor := clGray;
        if maCondominio[1] <> VAZIO then
          if StrToInt(maCondominio[1]) = liConta then
            lCor := clWhite;
        if maCondominio[2] <> VAZIO then
          if StrToInt(maCondominio[2]) = liConta then
            lCor := clWhite;
        if maCondominio[3] <> VAZIO then
          if StrToInt(maCondominio[3]) = liConta then
            lCor := clWhite;
        lDrawRect.Left := lDrawRect.Left + COR_ESQUERDA;
        lDrawRect.Top := lDrawRect.Top + COR_TOPO;
        lDrawRect.Right := lDrawRect.Right - COR_DIREITA;
        lDrawRect.Bottom := lDrawRect.Bottom - COR_FUNDO;
        TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Brush.Color := lCor;
        TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.FillRect(lDrawRect);
      end;
      { desenha o numero da gaveta }
      TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Brush.Color := clWhite;
      TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Font.Style := [fsBold];
      lDrawFont := Bounds(FONTE_ESQUERDA, FONTE_TOPO, FONTE_LARGURA, FONTE_ALTURA);
//      TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.TextRect(lDrawFont,6,26,Trim(IntToStr(liConta)));
      TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.TextRect(lDrawFont,6,26,Trim(IntToStr(laAndar[liConta])));
      { se tiver ocupante pinta }
      if maOcupacoes[laAndar[liConta]].iTotOcupacao > 0 then
      begin
        lDrawRect := TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).ClientRect;
        lDrawRect.Left := lDrawRect.Left + COR_ESQUERDA;
        lDrawRect.Top := lDrawRect.Top + COR_TOPO;
        lDrawRect.Right := lDrawRect.Right - COR_DIREITA;
        lDrawRect.Bottom := lDrawRect.Bottom - COR_FUNDO;

        liEsquerda := lDrawRect.Left;
        liDireita := lDrawRect.Right;
        liDegrade := $0;
        { para todos os ocupantes }
        for liOcupante := 1 to maOcupacoes[laAndar[liConta]].iTotOcupacao do
        begin
          { se for reinumacao }
          if maOcupacoes[laAndar[liConta]].arOcupacao[liOcupante].sTipoMov = 'R' then
          begin
            lCor := clBlue + (liDegrade * COR_DEGRADE_AZUL) + liDegrade;
            liDegrade := liDegrade + COR_DEGRADE;
          end
          else
          begin
            DecodeDate(maOcupacoes[laAndar[liConta]].arOcupacao[liOcupante].dData, wAno2, wMes2, wDia2);
            { se nao passou tres anos }
            if (wAno1 - wAno2 < StrToInt(ANOS_OCUPACAO)) or ((wAno1 - wAno2 = StrToInt(ANOS_OCUPACAO)) and (wMes1 <= wMes2)) then
              lCor := clRed
            else
              lCor := clGreen;
          end;
          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Brush.Color := lCor;
          lDrawRect.Left := liEsquerda + (liOcupante - 1) * Round((liDireita - liEsquerda) / maOcupacoes[laAndar[liConta]].iTotOcupacao);
          { se for o ultimo pedaco }
          if liOcupante = maOcupacoes[laAndar[liConta]].iTotOcupacao then
            lDrawRect.Right := liDireita
          else
            lDrawRect.Right := liEsquerda + (liOcupante) * Round((liDireita - liEsquerda) / maOcupacoes[laAndar[liConta]].iTotOcupacao);
          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.FillRect(lDrawRect);
        end;
      end;
    end;
  end;
  { se nao for 1 terreno }
  if mnTotLote <> 1 then
  begin
    { se for NP - nobre preferencial desloca para a esquerda - MUDAR }
    { se for terreno de 1 e meio desloca para a esquerda - MUDAR }
    if (mnTotLote = 1.5) then
      liOcupante := 3
    else
      liOcupante := 0;

    { se tiver area livre }
    if (miTotGaveta / miTotAndar) <> mnTotLote then
    begin
      { desenha o fundo do espaco do meio }
      lDrawRect := TPaintBox(FindComponent('ptbAndar6')).ClientRect;
      lFrame[2+liOcupante].Draw(TPaintBox(FindComponent('ptbAndar6')).Canvas, lDrawRect, True);
      { desenha o espaco do meio }
//      for liConta := 6 to 7 do
      for liConta := 7 to 10 do
      begin
        if liConta - 5 <= miTotAndar then
        begin
(*
          { se for 3 andares mostra o topo }
          if (liConta = 8) and (miTotAndar = 3) then
          begin
            if mnTotLote = 2 then
                liOcupante := 1
            else
            begin
              if (mnTotLote = 1.5) then
                liOcupante := 4
              else
                liOcupante := 1;
            end;
          end;
*)
          lDrawRect := TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).ClientRect;
          lFrame[3+liOcupante].Draw(TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas, lDrawRect, True);
          { se for 3 andares nao mostra 4 andares }
//          if miTotAndar <> 3 then
          if miTotAndar = liConta - 5 then
          begin
            { desenha o topo do espaco do meio }
            lDrawRect := TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).ClientRect;
            lFrame[4+liOcupante].Draw(TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas, lDrawRect, True);
          end;
        end;
      end;
    end
    else
    begin
//      for liConta := 5 to 8 do
      for liConta := 6 to 10 do
      begin
        { se nao for 3 andares ou se ja desenhou 3 andares }
//        if (miTotAndar <> 3) or (liConta < 8) then
        if miTotAndar >= liConta - 5 then
        begin
          { desenha o fundo do espaco do meio }
          lDrawRect := TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).ClientRect;
          lFrame[1].Draw(TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas, lDrawRect, True);
          { desenha o numero da gaveta }
          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Brush.Color := clWhite;
          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Font.Style := [fsBold];
          lDrawFont := Bounds(FONTE_ESQUERDA, FONTE_TOPO, FONTE_LARGURA, FONTE_ALTURA);
//          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.TextRect(lDrawFont,6,26,Trim(IntToStr(laAndar[liConta-(4-miTotAndar)])));
          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.TextRect(lDrawFont,6,26,Trim(IntToStr(laAndar[liConta-(5-miTotAndar)])));
          { se tiver ocupante pinta }
          if maOcupacoes[laAndar[liConta-(4-miTotAndar)]].iTotOcupacao > 0 then
          begin
            lDrawRect.Left := lDrawRect.Left + COR_ESQUERDA;
            lDrawRect.Top := lDrawRect.Top + COR_TOPO;
            lDrawRect.Right := lDrawRect.Right - COR_DIREITA;
            lDrawRect.Bottom := lDrawRect.Bottom - COR_FUNDO;

            liEsquerda := lDrawRect.Left;
            liDireita := lDrawRect.Right;
            liDegrade := $0;
            { para todos os ocupantes }
            for liOcupante := 1 to maOcupacoes[laAndar[liConta-(4-miTotAndar)]].iTotOcupacao do
            begin
              { se for reinumacao }
              if maOcupacoes[laAndar[liConta-(4-miTotAndar)]].arOcupacao[liOcupante].sTipoMov = 'R' then
              begin
                lCor := clBlue + (liDegrade * COR_DEGRADE_AZUL) + liDegrade;
                liDegrade := liDegrade + COR_DEGRADE;
              end
              else
              begin
                DecodeDate(maOcupacoes[laAndar[liConta-(4-miTotAndar)]].arOcupacao[liOcupante].dData, wAno2, wMes2, wDia2);
                { se nao passou tres anos }
                if (wAno1 - wAno2 < StrToInt(ANOS_OCUPACAO)) or ((wAno1 - wAno2 = StrToInt(ANOS_OCUPACAO)) and (wMes1 <= wMes2)) then
                  lCor := clRed
                else
                  lCor := clGreen;
              end;
              TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Brush.Color := lCor;
              lDrawRect.Left := liEsquerda + (liOcupante - 1) * Trunc((liDireita - liEsquerda) / maOcupacoes[laAndar[liConta-(4-miTotAndar)]].iTotOcupacao);
              { se for o ultimo pedaco }
              if liOcupante = maOcupacoes[laAndar[liConta]].iTotOcupacao then
                lDrawRect.Right := liDireita
              else
                lDrawRect.Right := liEsquerda + (liOcupante) * Trunc((liDireita - liEsquerda) / maOcupacoes[laAndar[liConta-(4-miTotAndar)]].iTotOcupacao);
              TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.FillRect(lDrawRect);
            end;
          end;
        end;
      end;
    end;
    { limpa a cor }
    lCor := clWhite;
    { se for 3 terrenos }
    if mnTotLote = 3 then
    begin
      { desenha as quatro ultimas gavetas }
      for liConta := 11 to 15 do
      begin
        { se nao for 3 andares ou se ja desenhou 3 andares }
//        if (miTotAndar <> 3) or (liConta < 12) then
        if miTotAndar >= liConta - 10 then
        begin
          lDrawRect := TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).ClientRect;
          lFrame[1].Draw(TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas, lDrawRect, True);
          { desenha o numero da gaveta }
          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Brush.Color := clWhite;
          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Font.Style := [fsBold];
          lDrawFont := Bounds(FONTE_ESQUERDA, FONTE_TOPO, FONTE_LARGURA, FONTE_ALTURA);
//          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.TextRect(lDrawFont,6,26,Trim(IntToStr(liConta-4)));
//          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.TextRect(lDrawFont,6,26,Trim(IntToStr(laAndar[liConta-(4+(4-miTotAndar))])));
          TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.TextRect(lDrawFont,6,26,Trim(IntToStr(laAndar[liConta-(5+(5-miTotAndar))])));
          { se tiver ocupante pinta }
          if maOcupacoes[laAndar[liConta-4]].iTotOcupacao > 0 then
          begin
            lDrawRect.Left := lDrawRect.Left + COR_ESQUERDA;
            lDrawRect.Top := lDrawRect.Top + COR_TOPO;
            lDrawRect.Right := lDrawRect.Right - COR_DIREITA;
            lDrawRect.Bottom := lDrawRect.Bottom - COR_FUNDO;

            liEsquerda := lDrawRect.Left;
            liDireita := lDrawRect.Right;
            liDegrade := $0;
            { para todos os ocupantes }
            for liOcupante := 1 to maOcupacoes[laAndar[liConta-4]].iTotOcupacao do
            begin
              { se for reinumacao }
              if maOcupacoes[laAndar[liConta-4]].arOcupacao[liOcupante].sTipoMov = 'R' then
              begin
                lCor := clBlue + (liDegrade * COR_DEGRADE_AZUL) + liDegrade;
                liDegrade := liDegrade + COR_DEGRADE;
              end
              else
              begin
                DecodeDate(maOcupacoes[laAndar[liConta-4]].arOcupacao[liOcupante].dData, wAno2, wMes2, wDia2);
                { se nao passou tres anos }
                if (wAno1 - wAno2 < StrToInt(ANOS_OCUPACAO)) or ((wAno1 - wAno2 = StrToInt(ANOS_OCUPACAO)) and (wMes1 <= wMes2)) then
                  lCor := clRed
                else
                  lCor := clGreen;
              end;
              TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.Brush.Color := lCor;
              lDrawRect.Left := liEsquerda + (liOcupante - 1) * Trunc((liDireita - liEsquerda) / maOcupacoes[laAndar[liConta-4]].iTotOcupacao);
              { se for o ultimo pedaco }
              if liOcupante = maOcupacoes[laAndar[liConta]].iTotOcupacao then
                lDrawRect.Right := liDireita
              else
                lDrawRect.Right := liEsquerda + (liOcupante) * Trunc((liDireita - liEsquerda) / maOcupacoes[laAndar[liConta-4]].iTotOcupacao);
              TPaintBox(FindComponent('ptbAndar' + Trim(IntToStr(liConta)))).Canvas.FillRect(lDrawRect);
            end;
          end;
        end;
      end;
    end;
  end;

end;

procedure TformVisualGaveta.FormPaint(Sender: TObject);
begin

  { para redesenhar as gavetas }
  mbPrimeiraVez := True;

end;

procedure TformVisualGaveta.btnImprimirClick(Sender: TObject);
var
  liConta     : Integer;
  liConexao   : Integer;
  liIndCol    : array[0..20] of Integer;
  lsSQL       : string;

  ptbFigura: TPaintBox;
  btmFigura: TBitMap;
  recDestino: TRect;
  recOrigem: TRect;
  x,y : integer;
  P : PByteArray;
  vFigura: Variant;
  sFigura: AnsiString;
begin

  { guarda a figura no banco de dados }
//  ptbFigura := TPaintBox.Create(pnlAmbiente);
  btmFigura := TBitMap.Create;
//  ptbFigura.Parent := pnlAmbiente;
//  ptbFigura.Height := 217;
//  ptbFigura.Width := 290;
  recOrigem := Bounds(0, 0, 290, 217);
  recDestino := Bounds(0, 0, 290, 217);
  ptbTeste.Canvas.CopyRect(recDestino,ptbAndar4.Canvas,recOrigem);
  btmFigura.Height := 217;
  btmFigura.Width := 290;
  btmFigura.Canvas.CopyRect(recDestino,ptbAndar4.Canvas,recOrigem);
  for y := 0 to btmFigura.Height -1 do
  begin
    P := btmFigura.ScanLine[y];
    if y = 0 then
      sFigura := IntToHex(P[0],2)
    else
      sFigura := sFigura + IntToHex(P[0],2);
    for x := 1 to btmFigura.Width -1 do
      sFigura := sFigura + IntToHex(P[x],2);
  end;
  btmFigura.Free;
(*
  vFigura := Chr(P[0]);
  for x := 1 to (btmFigura.Height -1)*(btmFigura.Width -1) do
    vFigura := vFigura + Chr(P[x]);
*)
  for liConta := 0 to 20 do
    liIndCol[liConta] := IOPTR_NOPOINTER;

  gaParm[0] := 'PROPGAVE';
  gaParm[1] := 'NUM_PROP = ''' + msNumProp + '''';
  gaParm[2] := 'NUM_PROP';
  lsSql := dmDados.SqlVersao('NEC_0002', gaParm);
  if dmDados.giStatus <> STATUS_OK then
    Exit;
  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;
  dmDados.gsRetorno := dmDados.AtivaModo(liConexao, IOMODE_EDIT);
  dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'NUM_PROP', liIndCol[14], msNumProp);
  dmDados.gsRetorno := dmDados.AlterarColuna(liConexao, 'Figura', liIndCol[15], sFigura);
  dmDados.gsRetorno := dmDados.Alterar(liConexao, 'PROPGAVE', VAZIO);
  if dmDados.gsRetorno = IORET_FAIL then
    Exit;
  dmDados.gsRetorno := dmDados.RecallColumns(liConexao);

end;


end.
