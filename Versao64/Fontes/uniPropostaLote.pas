unit uniPropostaLote;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ComCtrls, ExtCtrls, Variants,
  uniConst, OleCtrls, FPSpread_TLB;

const
  EDT_CODIGO: string = 'mskCodigo';

type
  TfrmPropostaLote = class(TForm)
    pnlFundo: TPanel;
    grpPropostaLote: TGroupBox;
    lblCodigo: TLabel;
    lblDataProposta: TLabel;
    mskCodigo: TMaskEdit;
    mskDataProposta: TMaskEdit;
    grp_Pagamento: TGroupBox;
    lblValor_Sinal: TLabel;
    lblValor_Parcela: TLabel;
    lblValor_NumeroParcelas: TLabel;
    lblValor_Vcto1Parcela: TLabel;
    lblValor_Taxa: TLabel;
    lblValor_Desconto: TLabel;
    edtValor_Parcela: TEdit;
    edtValor_Sinal: TEdit;
    edtValor_NumeroParcelas: TEdit;
    mskValor_Vcto1Parcela: TMaskEdit;
    edtValor_Taxa: TEdit;
    edtValor_Desconto: TEdit;
    grpProposta: TGroupBox;
    chkTaxaProposta_Manutencao: TCheckBox;
    chkProposta_ManutencaoFixa: TCheckBox;
    chkProposta_Carencia: TCheckBox;
    tabProponentes: TTabSheet;
    tabBeneficiarios: TTabSheet;
    pagProponentes: TPageControl;
    tabConjugado: TTabSheet;
    tabTransformado: TTabSheet;
    grpPlano: TGroupBox;
    lblPlano_Codigo: TLabel;
    lblPlano_Corretor: TLabel;
    cboPlano_Codigo: TComboBox;
    cboPlano_Corretor: TComboBox;
    lblPlano_Lote: TLabel;
    cboPlano_Lote: TComboBox;
    chkProposta_EmitirBoleto: TCheckBox;
    sprBeneficiarios: TvaSpread;
    sprLoteTransformado: TvaSpread;
    sprLoteConjugado: TvaSpread;
    lblProponente_Nome: TLabel;
    lblProponente_Endereco: TLabel;
    lblProponente_Estado: TLabel;
    lblProponente_CPF: TLabel;
    lblProponente_RG: TLabel;
    lblProponente_CEP: TLabel;
    lblProponente_Bairro: TLabel;
    lblProponente_Cidade: TLabel;
    lblProponente_DataNascimento: TLabel;
    lblProponente_Telefone: TLabel;
    lblProponente_DDD: TLabel;
    lblProponente_Ramal: TLabel;
    edtProponente_Endereco: TEdit;
    mskProponente_CEP: TMaskEdit;
    edtProponente_Cidade: TEdit;
    edtProponente_Bairro: TEdit;
    mskProponente_DataNascimento: TMaskEdit;
    mskProponente_RG: TMaskEdit;
    mskProponente_CPF: TMaskEdit;
    mskProponente_Telefone: TMaskEdit;
    edtProponente_DDD: TEdit;
    edtProponente_Nome: TEdit;
    edtProponente_Ramal: TEdit;
    edtProponente_Profissao: TEdit;
    lblProponente_Profissao: TLabel;
    edtProponente_Observacao: TEdit;
    lblProponente_Observacao: TLabel;
    tabFollowUpCobranca: TTabSheet;
    tabFollowUpVenda: TTabSheet;
    PntComunicador: TPaintBox;
    sprFollowCobranca: TvaSpread;
    sprFollowVenda: TvaSpread;
    edtProponente_Estado: TEdit;
    sprProponentes: TvaSpread;
    lblPlano_DataPlano: TLabel;
    mskPlano_DataPlano: TMaskEdit;
    lblBloqueada: TLabel;
    edtBloqueada: TEdit;
    lblTemGaveta: TLabel;
    edtTemGaveta: TEdit;
    tabComercial: TTabSheet;
    lblSexo: TLabel;
    cboSexo: TComboBox;
    lblEstadoCivil: TLabel;
    lblPai: TLabel;
    edtPai: TEdit;
    lblConjuge: TLabel;
    edtConjuge: TEdit;
    lblMae: TLabel;
    edtMae: TEdit;
    lblEmissaoRg: TLabel;
    edtEmissaoRg: TEdit;
    lblNacionalidade: TLabel;
    edtNacionalidade: TEdit;
    lblCarenciaMeses: TLabel;
    mskCarenciaMeses: TMaskEdit;
    lblDataManut: TLabel;
    mskDataManut: TMaskEdit;
    lblIndice: TLabel;
    cboIndice: TComboBox;
    grpEnderecoComercial: TGroupBox;
    lblEnderecoComercial: TLabel;
    edtEnderecoComercial: TEdit;
    lblCEPComercial: TLabel;
    mskCEPComercial: TMaskEdit;
    lblCidadeComercial: TLabel;
    edtCidadeComercial: TEdit;
    lblFoneComercial: TLabel;
    mskFoneComercial: TMaskEdit;
    lblSituacao: TLabel;
    cboSituacao: TComboBox;
    lblBairroComercial: TLabel;
    edtBairroComercial: TEdit;
    lblEstadoComercial: TLabel;
    edtEstadoComercial: TEdit;
    lblRegimeCasamento: TLabel;
    edtRegimeCasamento: TEdit;
    lblFormaContato: TLabel;
    cboFormaContato: TComboBox;
    cboEstadoCivil: TComboBox;
    lblTaxaContrato: TLabel;
    edtTaxaContrato: TEdit;
    chkCorrespondencia: TCheckBox;
    Label1: TLabel;
    edtObservacaoTaxa: TEdit;
    cboQuadra: TComboBox;
    Label5: TLabel;
    Label2: TLabel;
    edtEMail: TEdit;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDeactivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);

    procedure cboComboChange(Sender: TObject);
    procedure cboComboClick(Sender: TObject);
    procedure cboComboEnter(Sender: TObject);
    procedure cboComboExit(Sender: TObject);
    procedure cboComboKeyPress(Sender: TObject; var Key: Char);
    procedure edtTextoChange(Sender: TObject);
    procedure edtTextoEnter(Sender: TObject);
    procedure edtTextoExit(Sender: TObject);
    procedure sprGridChange(Sender: TObject; Col, Row: Integer);
    procedure sprGridEditChange(Sender: TObject; Col, Row: Integer);
    procedure chkCheckClick(Sender: TObject);
  private
    { Private declarations }
    mrTelaVar : TumaTelaVar;

    procedure mpMostra_Gaveta;
  public
    { Public declarations }
  end;

var
  frmPropostaLote: TfrmPropostaLote;

implementation

uses uniMDI, uniDados, uniGlobal, uniTelaVar, uniLock, uniEdit, uniCombos, uniFuncoes;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.FormActivate(Sender: TObject);
begin
  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);
  grTelaVar := mrTelaVar;

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.FormClose(Sender: TObject; var Action: TCloseAction);
var
  liConta:  Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - uniPropostaLote',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
    End;
    gfrmTV := nil;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
    formMDI.gp_AtualizaBarraStatus;

  except
    dmDados.ErroTratar('FormClose - uniPropostaLote');
  end;

  // destroi a janela
  Action := caFree;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, TForm(Sender));

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.FormDeactivate(Sender: TObject);
begin
 If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.FormResize(Sender: TObject);
begin
  if TForm(Sender).WindowState <> wsMinimized Then
  begin
    if (TForm(Sender).Width < pnlFundo.Width + 39) and (TForm(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TForm(Sender).Width := pnlFundo.Width + 39;

    TForm(Sender).Refresh;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.FormShow(Sender: TObject);
begin
  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
//    mrTelaVar.P.Titulo_Tela := 'Proposta de Lote';

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    Enabled := False;

    {' estufar os combos}
    untEdit.gp_Edit_EstufaCombos(mrTelaVar.P.Sigla_Tabela, TForm(Sender));

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, TForm(Sender))
    Else
      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);

    mpMostra_Gaveta;

    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniPropostaLote');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.cboComboChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.cboComboChange(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  {' se n�o for consulta}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  If (Not mrTelaVar.Bloqueado) Or mrTelaVar.Reposicionando Then
    untCombos.gp_Combo_Seleciona(lcboCombo, 0);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.cboComboClick       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.cboComboClick(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
      if lcboCombo.Text <> VAZIO then
      begin
        { se foi combo de planos }
        if lcboCombo.Name = CBO_PLANO_CODIGO_PLT then
        begin
          mskPlano_DataPlano.Text := Copy(lcboCombo.Text,34,10);
          edtValor_Sinal.Text := FormatFloat(MK_VALOR, StrToFloat(Copy(lcboCombo.Text,7,8)));
          edtValor_Parcela.Text := FormatFloat(MK_VALOR, StrToFloat(Copy(lcboCombo.Text,18,8)));
          edtValor_NumeroParcelas.Text := Copy(lcboCombo.Text,29,2);
        end;

        {' altera��o efetuada}
        untEdit.gp_Edit_Alterado(True, mrTelaVar);
      end;
    end
    else
      If (Not mrTelaVar.Reposicionando) Then
        {' Retorna o valor inicial mantendo a ilus�o de que o campo � inalter�vel}
      lcboCombo.ItemIndex := giAreaDesfaz;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboComboClick - uniPropostaLote');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.cboComboEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.cboComboEnter(Sender: TObject);
var
  lcboCombo: TComboBox;
  lwMes:     Word;
  lwAno:     Word;
  lwDia:     Word;
  liConexao: Integer;
  liIndCol:  array[0..MAX_NUM_COLUNAS] of Integer;
  lsCampo:   string;
  lsSql:     string;
  liConta: Integer;
begin

  try
    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    lcboCombo := TComboBox(Sender);

    {' guarda o valor original do combo}
    gsAreaDesfaz := lcboCombo.Text;
  giAreaDesfaz := -1;
  if lcboCombo.Items.Count > 0 then
    {'Executa um la�o nos itens comparando-os com o pConteudo}
    for liConta := 0 to lcboCombo.Items.Count - 1 do
      if lcboCombo.Items[liConta] = gsAreaDesfaz then
      begin
        giAreaDesfaz := liConta;
        break;
      end;

    {' se nao for consulta}
    If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    begin
      { se foi combo de planos }
      if lcboCombo.Name = CBO_PLANO_CODIGO_PLT then
      begin
        DecodeDate(Date(), lwAno, lwMes, lwDia);
        if lwDia <= 15 then
          lwMes := lwMes - 1;
        if lwMes <= 0 then
        begin
          lwMes := lwMes + 12;
          lwAno := lwAno - 1;
        end;
        { planos }
        gaParm[0] := 'L';
        gaParm[1] := Copy(cboPlano_Lote.Text,1,2);
        gaParm[2] := IntToStr(lwMes) + '/' + '01' + '/' + IntToStr(lwAno);
        liConexao := 0;
        lsSQL := dmDados.SqlVersao('NEC_0003', gaParm);
        liConexao := dmDados.ExecutarSelect(lsSQL);
        if liConexao = IOPTR_NOPOINTER then
          Exit;

        if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
        begin
          dmDados.gsRetorno := dmDados.Primeiro(liConexao);

          liIndCol[0] := IOPTR_NOPOINTER;
          liIndCol[1] := IOPTR_NOPOINTER;
          liIndCol[2] := IOPTR_NOPOINTER;
          liIndCol[3] := IOPTR_NOPOINTER;
          liIndCol[4] := IOPTR_NOPOINTER;

          lcboCombo.Clear;
          while not (dmDados.gsRetorno = IORET_EOF) do
          begin
            lsCampo := dmDados.ValorColuna(liConexao, 'COD_PLANO', liIndCol[0]);
            lsCampo := lsCampo + '   ' + FormatFloat('00000.00', dmDados.ValorColuna(liConexao, 'VLR_SINAL', liIndCol[1]));
            lsCampo := lsCampo + '   ' + FormatFloat('00000.00', dmDados.ValorColuna(liConexao, 'V_PARCELAS', liIndCol[2]));
            lsCampo := lsCampo + '   ' + FormatFloat('00', dmDados.ValorColuna(liConexao, 'Q_PARCELAS', liIndCol[3]));
            lsCampo := lsCampo + '   ' + FormatDateTime('dd/mm/yyyy', dmDados.ValorColuna(liConexao, 'DATA_INCL', liIndCol[4]));
            lcboCombo.Items.Add(lsCampo);
            dmDados.gsRetorno := dmDados.Proximo(liConexao);
          end;
        end;
        dmDados.FecharConexao(liConexao);
      end;
    end;

    {' retorna o ponteiro do mouse para o default}
    Screen.Cursor := crDefault;
  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('cboComboEnter - uniPropostaLote');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.cboComboKeyPress       se for consulta nao faz nada
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.cboComboKeyPress(Sender: TObject;
  var Key: Char);
begin

  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.edtTextoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.edtTextoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = EDT_CODIGO) And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    {' atualiza o caption da janela}
    Caption := mrTelaVar.P.Titulo_Tela + ' ' + ledtEdit.Text + ' - Inclus�o';
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.edtTextoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.edtTextoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.sprGridChange       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.sprGridChange(Sender: TObject; Col,
  Row: Integer);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TformProcessos.sprGridEditChange       Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.sprGridEditChange(Sender: TObject; Col,
  Row: Integer);
var
  lgrdGrid: TvaSpread;
begin

  lgrdGrid := TvaSpread(Sender);

  lgrdGrid.Row := Row;
  lgrdGrid.Col := Col;
  if lgrdGrid.CellType = SS_CELL_TYPE_COMBOBOX then
    untCombos.gp_Combo_SpreadSeleciona(lgrdGrid, 0);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.chkCheckClick       Se for consulta nao muda
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.chkCheckClick(Sender: TObject);
begin

  {se for alteracao ou inclusao}
  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.PntComunicadorPaint(Sender: TObject);
var
  liComandoMDI: Integer;
begin

  liComandoMDI := giComando_MDI;

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, frmPropostaLote);

  if (liComandoMDI = TV_COMANDO_IR_PROXIMO) or
     (liComandoMDI = TV_COMANDO_IR_ANTERIOR) or
     (liComandoMDI = TV_COMANDO_IR_PRIMEIRO) or
     (liComandoMDI = TV_COMANDO_PROCURAR) or
     (liComandoMDI = TV_COMANDO_IR_ULTIMO) then
  begin
    mpMostra_Gaveta;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TfrmPropostaLote.edtTextoExit       quando perde o foco
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.edtTextoExit(Sender: TObject);
var
  ledtEdit: TMaskEdit;
  i, j:  Integer;
  lwAno, lwMes, lwDia: Word;
  lbData: Boolean;
begin

  ledtEdit := TMaskEdit(Sender);

  if Sender is TMaskEdit then
  begin
    { procura pela entidade }
    i := untTelaVar.gf_TelaVar_Retorna_Tabela(mrTelaVar.P.Sigla_Tabela);
    { se nao achou }
    if i = 0 then
      Exit;

    j := 1;
    { percorre todos os campos da tabela principal }
    while gaLista_Campos[i][j].sSigla_Tabela = gaLista_Parametros[i].Sigla_Tabela do
    begin
      if gaLista_Campos[i][j].sNome_Controle = ledtEdit.Name then
      begin
        if gaLista_Campos[i][j].sAlinha = ALINHA_ZERO then
          ledtEdit.Text := untFuncoes.gf_Zero_Esquerda(ledtEdit.Text, gaLista_Campos[i][j].iTamanho);
        break;
      end;
      j := j + 1;
    end;
  end;

  { se for 1o vencimento ou meses de carencia }
  if (ledtEdit.Name = 'mskDataProposta') or (ledtEdit.Name = 'mskCarenciaMeses') then
  begin
    { se nao estiver vazia }
    if mskDataProposta.Text <> VAZIO_DATA then
    begin
      { muda a data da manutencao }
      DecodeDate(StrToDate(mskDataProposta.Text), lwAno, lwMes, lwDia);
      lwMes := lwMes + StrToInt(mskCarenciaMeses.Text);
      while lwMes > 12 do
      begin
        lwAno := lwAno + 1;
        lwMes := lwMes - 12;
      end;
      { checa se e data valida }
      lbData := True;
      while lbData do
      begin
        try
          mskDataManut.Text := FormatDateTime(MK_DATATELA, EncodeDate(lwAno, lwMes, lwDia));
          lbData := False;
        except on EConvertError do
          lwDia := lwDia - 1;
        end;
      end;
    end;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.cboComboExit       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.cboComboExit(Sender: TObject);
var
  lcboCombo: TComboBox;
begin

  lcboCombo := TComboBox(Sender);

end;

{*-----------------------------------------------------------------------------
 *  TFormPadrao.mpMostra_Gaveta       Mostra se tem gaveta
 *
 *-----------------------------------------------------------------------------}
procedure TfrmPropostaLote.mpMostra_Gaveta();
var
  lsSql: string;
  liConexao: Integer;
  aSqlParms:  array[0..2] of Variant;
begin

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := True;

  aSqlParms[0] := 'Num_Prop';
  aSqlParms[1] := 'PropGave';
  aSqlParms[2] := 'Num_Prop = ''' + mskCodigo.Text + '''';
  lsSql := dmDados.SqlVersao('NEC_0000', aSqlParms);

  liConexao := dmDados.ExecutarSelect(lsSql);
  if liConexao = IOPTR_NOPOINTER then
    Exit;
  { se encontrou a op��o }
  if not dmDados.Status(liConexao, IOSTATUS_NOROWS) Then
    edtTemGaveta.Text := 'S'
  else
    edtTemGaveta.Text := 'N';
  dmDados.FecharConexao(liConexao);

  { para nao ativar o salvar }
  mrTelaVar.Reposicionando := False;

end;















end.
