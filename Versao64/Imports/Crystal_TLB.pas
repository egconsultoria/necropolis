unit Crystal_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.130  $
// File generated on 24/08/2010 21:00:23 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Windows\SysWOW64\Crystl32.OCX (1)
// LIBID: {00025600-0000-0000-C000-000000000046}
// LCID: 0
// Helpfile: C:\Windows\SysWOW64\developr.hlp
// DepndLst: 
//   (1) v1.0 stdole, (C:\Windows\system32\stdole32.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}

interface

uses ActiveX, Classes, Graphics, OleCtrls, OleServer, StdVCL, Variants, 
Windows;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  CrystalMajorVersion = 5;
  CrystalMinorVersion = 2;

  LIBID_Crystal: TGUID = '{00025600-0000-0000-C000-000000000046}';

  IID_IRowCursor: TGUID = '{9F6AA700-D188-11CD-AD48-00AA003C9CB6}';
  IID_CrystalCtrl: TGUID = '{00025614-0000-0000-C000-000000000046}';
  DIID_CrystalReportEvents: TGUID = '{00025603-0000-0000-C000-000000000046}';
  CLASS_CrystalReport: TGUID = '{00025601-0000-0000-C000-000000000046}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum DestinationConstants
type
  DestinationConstants = TOleEnum;
const
  crptToWindow = $00000000;
  crptToPrinter = $00000001;
  crptToFile = $00000002;
  crptMapi = $00000003;
  crptExchange = $00000006;

// Constants for enum WindowBorderStyleConstants
type
  WindowBorderStyleConstants = TOleEnum;
const
  crptNoBorder = $00000000;
  crptFixedSingle = $00000001;
  crptSizable = $00000002;
  crptFixedDouble = $00000003;

// Constants for enum PrintFileTypeConstants
type
  PrintFileTypeConstants = TOleEnum;
const
  crptCharSep = $00000005;
  crptCrystal = $00000007;
  crptCSV = $00000004;
  crptDIF = $00000003;
  crptExcel21 = $00000008;
  crptExcel30 = $00000009;
  crptExcel40 = $0000000A;
  crptExcel50 = $00000013;
  crptExcel50Tab = $00000019;
  crptHTML30 = $00000014;
  crptHTML32Ext = $00000015;
  crptHTML32Std = $00000016;
  crpt123WK1 = $0000000B;
  crpt123WK3 = $0000000C;
  crpt123WKS = $0000000D;
  crptODBC = $00000017;
  crptPagedText = $00000018;
  crptRecord = $00000000;
  crptRTF = $0000000F;
  crptTabSep = $00000001;
  crptTabSepText = $00000006;
  crptText = $00000002;
  crptWinWord = $00000011;

// Constants for enum ReportSourceConstants
type
  ReportSourceConstants = TOleEnum;
const
  crptReport = $00000000;
  crptTrueDBGrid = $00000001;
  crptDataControl = $00000003;

// Constants for enum PrinterCollationConstants
type
  PrinterCollationConstants = TOleEnum;
const
  crptUncollated = $00000000;
  crptCollated = $00000001;
  crptDefault = $00000002;

// Constants for enum WindowStateConstants
type
  WindowStateConstants = TOleEnum;
const
  crptNormal = $00000000;
  crptMinimized = $00000001;
  crptMaximized = $00000002;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IRowCursor = interface;
  IRowCursorDisp = dispinterface;
  CrystalCtrl = interface;
  CrystalCtrlDisp = dispinterface;
  CrystalReportEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CrystalReport = CrystalCtrl;


// *********************************************************************//
// Interface: IRowCursor
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {9F6AA700-D188-11CD-AD48-00AA003C9CB6}
// *********************************************************************//
  IRowCursor = interface(IDispatch)
    ['{9F6AA700-D188-11CD-AD48-00AA003C9CB6}']
  end;

// *********************************************************************//
// DispIntf:  IRowCursorDisp
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {9F6AA700-D188-11CD-AD48-00AA003C9CB6}
// *********************************************************************//
  IRowCursorDisp = dispinterface
    ['{9F6AA700-D188-11CD-AD48-00AA003C9CB6}']
  end;

// *********************************************************************//
// Interface: CrystalCtrl
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {00025614-0000-0000-C000-000000000046}
// *********************************************************************//
  CrystalCtrl = interface(IDispatch)
    ['{00025614-0000-0000-C000-000000000046}']
    function  Get_ReportFileName: WideString; safecall;
    procedure Set_ReportFileName(const px: WideString); safecall;
    function  Get_WindowLeft: Smallint; safecall;
    procedure Set_WindowLeft(px: Smallint); safecall;
    function  Get_WindowTop: Smallint; safecall;
    procedure Set_WindowTop(px: Smallint); safecall;
    function  Get_WindowWidth: Smallint; safecall;
    procedure Set_WindowWidth(px: Smallint); safecall;
    function  Get_WindowHeight: Smallint; safecall;
    procedure Set_WindowHeight(px: Smallint); safecall;
    function  Get_WindowTitle: WideString; safecall;
    procedure Set_WindowTitle(const px: WideString); safecall;
    function  Get_WindowControlBox: WordBool; safecall;
    procedure Set_WindowControlBox(px: WordBool); safecall;
    function  Get_WindowMaxButton: WordBool; safecall;
    procedure Set_WindowMaxButton(px: WordBool); safecall;
    function  Get_WindowMinButton: WordBool; safecall;
    procedure Set_WindowMinButton(px: WordBool); safecall;
    function  Get_PrintFileName: WideString; safecall;
    procedure Set_PrintFileName(const px: WideString); safecall;
    function  Get_SelectionFormula: WideString; safecall;
    procedure Set_SelectionFormula(const px: WideString); safecall;
    function  Get_GroupSelectionFormula: WideString; safecall;
    procedure Set_GroupSelectionFormula(const px: WideString); safecall;
    function  Get_WindowParentHandle: Integer; safecall;
    procedure Set_WindowParentHandle(px: Integer); safecall;
    function  Get_CopiesToPrinter: Smallint; safecall;
    procedure Set_CopiesToPrinter(px: Smallint); safecall;
    function  Get_Action: Smallint; safecall;
    procedure Set_Action(px: Smallint); safecall;
    function  Get_LastErrorNumber: Smallint; safecall;
    procedure Set_LastErrorNumber(px: Smallint); safecall;
    function  Get_LastErrorString: WideString; safecall;
    procedure Set_LastErrorString(const px: WideString); safecall;
    function  Get_Connect: WideString; safecall;
    procedure Set_Connect(const px: WideString); safecall;
    function  Get_SessionHandle: Integer; safecall;
    procedure Set_SessionHandle(px: Integer); safecall;
    function  Get_UserName: WideString; safecall;
    procedure Set_UserName(const px: WideString); safecall;
    function  Get_Password: WideString; safecall;
    procedure Set_Password(const px: WideString); safecall;
    function  Get_Destination: DestinationConstants; safecall;
    procedure Set_Destination(px: DestinationConstants); safecall;
    function  Get_PrintFileType: PrintFileTypeConstants; safecall;
    procedure Set_PrintFileType(px: PrintFileTypeConstants); safecall;
    function  Get_WindowBorderStyle: WindowBorderStyleConstants; safecall;
    procedure Set_WindowBorderStyle(px: WindowBorderStyleConstants); safecall;
    function  Get_BoundReportHeading: WideString; safecall;
    procedure Set_BoundReportHeading(const px: WideString); safecall;
    function  Get_BoundReportFooter: WordBool; safecall;
    procedure Set_BoundReportFooter(px: WordBool); safecall;
    function  Get_ReportSource: ReportSourceConstants; safecall;
    procedure Set_ReportSource(px: ReportSourceConstants); safecall;
    function  Get_DetailCopies: Smallint; safecall;
    procedure Set_DetailCopies(px: Smallint); safecall;
    function  Get_DiscardSavedData: WordBool; safecall;
    procedure Set_DiscardSavedData(px: WordBool); safecall;
    function  Get_EMailCCList: WideString; safecall;
    procedure Set_EMailCCList(const px: WideString); safecall;
    function  Get_EMailMessage: WideString; safecall;
    procedure Set_EMailMessage(const px: WideString); safecall;
    function  Get_EMailSubject: WideString; safecall;
    procedure Set_EMailSubject(const px: WideString); safecall;
    function  Get_EMailToList: WideString; safecall;
    procedure Set_EMailToList(const px: WideString); safecall;
    function  Get_MarginBottom: Smallint; safecall;
    procedure Set_MarginBottom(px: Smallint); safecall;
    function  Get_MarginLeft: Smallint; safecall;
    procedure Set_MarginLeft(px: Smallint); safecall;
    function  Get_MarginRight: Smallint; safecall;
    procedure Set_MarginRight(px: Smallint); safecall;
    function  Get_MarginTop: Smallint; safecall;
    procedure Set_MarginTop(px: Smallint); safecall;
    function  Get_PrintDay: Smallint; safecall;
    procedure Set_PrintDay(px: Smallint); safecall;
    function  Get_PrinterCollation: PrinterCollationConstants; safecall;
    procedure Set_PrinterCollation(px: PrinterCollationConstants); safecall;
    function  Get_PrinterCopies: Smallint; safecall;
    procedure Set_PrinterCopies(px: Smallint); safecall;
    function  Get_PrinterDriver: WideString; safecall;
    procedure Set_PrinterDriver(const px: WideString); safecall;
    function  Get_PrinterName: WideString; safecall;
    procedure Set_PrinterName(const px: WideString); safecall;
    function  Get_PrinterPort: WideString; safecall;
    procedure Set_PrinterPort(const px: WideString); safecall;
    function  Get_PrinterStartPage: Smallint; safecall;
    procedure Set_PrinterStartPage(px: Smallint); safecall;
    function  Get_PrinterStopPage: Smallint; safecall;
    procedure Set_PrinterStopPage(px: Smallint); safecall;
    function  Get_PrintFileCharSepQuote: WideString; safecall;
    procedure Set_PrintFileCharSepQuote(const px: WideString); safecall;
    function  Get_PrintFileCharSepSeparator: WideString; safecall;
    procedure Set_PrintFileCharSepSeparator(const px: WideString); safecall;
    function  Get_PrintFileUseRptDateFmt: WordBool; safecall;
    procedure Set_PrintFileUseRptDateFmt(px: WordBool); safecall;
    function  Get_PrintFileUseRptNumberFmt: WordBool; safecall;
    procedure Set_PrintFileUseRptNumberFmt(px: WordBool); safecall;
    function  Get_PrintMonth: Smallint; safecall;
    procedure Set_PrintMonth(px: Smallint); safecall;
    function  Get_PrintYear: Smallint; safecall;
    procedure Set_PrintYear(px: Smallint); safecall;
    function  Get_RecordsPrinted: Integer; safecall;
    procedure Set_RecordsPrinted(px: Integer); safecall;
    function  Get_RecordsRead: Integer; safecall;
    procedure Set_RecordsRead(px: Integer); safecall;
    function  Get_RecordsSelected: Integer; safecall;
    procedure Set_RecordsSelected(px: Integer); safecall;
    function  Get_ReportDisplayPage: Smallint; safecall;
    procedure Set_ReportDisplayPage(px: Smallint); safecall;
    function  Get_ReportLatestPage: Smallint; safecall;
    procedure Set_ReportLatestPage(px: Smallint); safecall;
    function  Get_ReportStartPage: Smallint; safecall;
    procedure Set_ReportStartPage(px: Smallint); safecall;
    function  Get_SQLQuery: WideString; safecall;
    procedure Set_SQLQuery(const px: WideString); safecall;
    function  Get_Status: Smallint; safecall;
    procedure Set_Status(px: Smallint); safecall;
    function  Get_WindowControls: WordBool; safecall;
    procedure Set_WindowControls(px: WordBool); safecall;
    function  Get_WindowState: WindowStateConstants; safecall;
    procedure Set_WindowState(px: WindowStateConstants); safecall;
    function  Get_DialogParentHandle: Integer; safecall;
    procedure Set_DialogParentHandle(px: Integer); safecall;
    function  Get_ProgressDialog: WordBool; safecall;
    procedure Set_ProgressDialog(px: WordBool); safecall;
    function  Get_ExchangeProfile: WideString; safecall;
    procedure Set_ExchangeProfile(const px: WideString); safecall;
    function  Get_ExchangePassword: WideString; safecall;
    procedure Set_ExchangePassword(const px: WideString); safecall;
    function  Get_ExchangeFolder: WideString; safecall;
    procedure Set_ExchangeFolder(const px: WideString); safecall;
    function  Get_PrintFileODBCSource: WideString; safecall;
    procedure Set_PrintFileODBCSource(const px: WideString); safecall;
    function  Get_PrintFileODBCUser: WideString; safecall;
    procedure Set_PrintFileODBCUser(const px: WideString); safecall;
    function  Get_PrintFileODBCPassword: WideString; safecall;
    procedure Set_PrintFileODBCPassword(const px: WideString); safecall;
    function  Get_PrintFileODBCTable: WideString; safecall;
    procedure Set_PrintFileODBCTable(const px: WideString); safecall;
    function  Get_PrintFileLinesPerPage: Smallint; safecall;
    procedure Set_PrintFileLinesPerPage(px: Smallint); safecall;
    function  Get_SubreportToChange: WideString; safecall;
    procedure Set_SubreportToChange(const px: WideString); safecall;
    function  Get_ReportTitle: WideString; safecall;
    procedure Set_ReportTitle(const px: WideString); safecall;
    function  Get_GridSource: WideString; safecall;
    procedure Set_GridSource(const px: WideString); safecall;
    function  Get_WindowShowGroupTree: WordBool; safecall;
    procedure Set_WindowShowGroupTree(px: WordBool); safecall;
    function  Get_WindowAllowDrillDown: WordBool; safecall;
    procedure Set_WindowAllowDrillDown(px: WordBool); safecall;
    function  Get_WindowShowNavigationCtls: WordBool; safecall;
    procedure Set_WindowShowNavigationCtls(px: WordBool); safecall;
    function  Get_WindowShowCancelBtn: WordBool; safecall;
    procedure Set_WindowShowCancelBtn(px: WordBool); safecall;
    function  Get_WindowShowPrintBtn: WordBool; safecall;
    procedure Set_WindowShowPrintBtn(px: WordBool); safecall;
    function  Get_WindowShowExportBtn: WordBool; safecall;
    procedure Set_WindowShowExportBtn(px: WordBool); safecall;
    function  Get_WindowShowZoomCtl: WordBool; safecall;
    procedure Set_WindowShowZoomCtl(px: WordBool); safecall;
    function  Get_WindowShowCloseBtn: WordBool; safecall;
    procedure Set_WindowShowCloseBtn(px: WordBool); safecall;
    function  Get_WindowShowProgressCtls: WordBool; safecall;
    procedure Set_WindowShowProgressCtls(px: WordBool); safecall;
    function  Get_WindowShowSearchBtn: WordBool; safecall;
    procedure Set_WindowShowSearchBtn(px: WordBool); safecall;
    function  Get_WindowShowPrintSetupBtn: WordBool; safecall;
    procedure Set_WindowShowPrintSetupBtn(px: WordBool); safecall;
    function  Get_WindowShowRefreshBtn: WordBool; safecall;
    procedure Set_WindowShowRefreshBtn(px: WordBool); safecall;
    function  Get_DataSource: IRowCursor; safecall;
    procedure Set_DataSource(const px: IRowCursor); safecall;
    function  Get_Formulas(index: Smallint): WideString; safecall;
    procedure Set_Formulas(index: Smallint; const px: WideString); safecall;
    function  Get_SortFields(index: Smallint): WideString; safecall;
    procedure Set_SortFields(index: Smallint; const px: WideString); safecall;
    function  Get_GroupSortFields(index: Smallint): WideString; safecall;
    procedure Set_GroupSortFields(index: Smallint; const px: WideString); safecall;
    function  Get_DataFiles(index: Smallint): WideString; safecall;
    procedure Set_DataFiles(index: Smallint; const px: WideString); safecall;
    function  Get_GroupCondition(index: Smallint): WideString; safecall;
    procedure Set_GroupCondition(index: Smallint; const px: WideString); safecall;
    function  Get_SectionMinHeight(index: Smallint): WideString; safecall;
    procedure Set_SectionMinHeight(index: Smallint; const px: WideString); safecall;
    function  Get_SectionFormat(index: Smallint): WideString; safecall;
    procedure Set_SectionFormat(index: Smallint; const px: WideString); safecall;
    function  Get_SectionLineHeight(index: Smallint): WideString; safecall;
    procedure Set_SectionLineHeight(index: Smallint; const px: WideString); safecall;
    function  Get_SectionFont(index: Smallint): WideString; safecall;
    procedure Set_SectionFont(index: Smallint; const px: WideString); safecall;
    function  Get_StoredProcParam(index: Smallint): WideString; safecall;
    procedure Set_StoredProcParam(index: Smallint; const px: WideString); safecall;
    function  Get_GraphType(index: Smallint): WideString; safecall;
    procedure Set_GraphType(index: Smallint; const px: WideString); safecall;
    function  Get_GraphData(index: Smallint): WideString; safecall;
    procedure Set_GraphData(index: Smallint; const px: WideString); safecall;
    function  Get_GraphText(index: Smallint): WideString; safecall;
    procedure Set_GraphText(index: Smallint; const px: WideString); safecall;
    function  Get_GraphOptions(index: Smallint): WideString; safecall;
    procedure Set_GraphOptions(index: Smallint; const px: WideString); safecall;
    function  Get_ParameterFields(index: Smallint): WideString; safecall;
    procedure Set_ParameterFields(index: Smallint; const px: WideString); safecall;
    function  Get_LogonInfo(index: Smallint): WideString; safecall;
    procedure Set_LogonInfo(index: Smallint; const px: WideString); safecall;
    procedure AboutBox; stdcall;
    function  PrintReport: Smallint; safecall;
    procedure PageFirst; safecall;
    procedure PageNext; safecall;
    procedure PageLast; safecall;
    procedure PagePrevious; safecall;
    function  PageCount: Smallint; safecall;
    procedure PageShow(p: Smallint); safecall;
    procedure PageZoom(z: Smallint); safecall;
    procedure PageZoomNext; safecall;
    procedure PrinterSelect; safecall;
    function  FetchSelectionFormula: WideString; safecall;
    procedure ReplaceSelectionFormula(const s: WideString); safecall;
    function  RetrieveDataFiles: Smallint; safecall;
    function  RetrieveLogonInfo: Smallint; safecall;
    procedure RetrieveSQLQuery; safecall;
    function  RetrieveStoredProcParams: Smallint; safecall;
    function  LogOnServer(const dll: WideString; const server: WideString; 
                          const database: WideString; const userid: WideString; 
                          const Password: WideString): Smallint; safecall;
    procedure LogOffServer(id: Smallint; all: WordBool); safecall;
    function  GetNSubreports: Smallint; safecall;
    function  GetNthSubreportName(n: Smallint): WideString; safecall;
    procedure Reset; safecall;
    procedure SpecifyDataSourceField(n: Smallint; const name: WideString; width: Smallint); safecall;
    procedure SetTablePrivateData(TableIndex: Smallint; DataTag: Integer; Data: OleVariant); safecall;
    property ReportFileName: WideString read Get_ReportFileName write Set_ReportFileName;
    property WindowLeft: Smallint read Get_WindowLeft write Set_WindowLeft;
    property WindowTop: Smallint read Get_WindowTop write Set_WindowTop;
    property WindowWidth: Smallint read Get_WindowWidth write Set_WindowWidth;
    property WindowHeight: Smallint read Get_WindowHeight write Set_WindowHeight;
    property WindowTitle: WideString read Get_WindowTitle write Set_WindowTitle;
    property WindowControlBox: WordBool read Get_WindowControlBox write Set_WindowControlBox;
    property WindowMaxButton: WordBool read Get_WindowMaxButton write Set_WindowMaxButton;
    property WindowMinButton: WordBool read Get_WindowMinButton write Set_WindowMinButton;
    property PrintFileName: WideString read Get_PrintFileName write Set_PrintFileName;
    property SelectionFormula: WideString read Get_SelectionFormula write Set_SelectionFormula;
    property GroupSelectionFormula: WideString read Get_GroupSelectionFormula write Set_GroupSelectionFormula;
    property WindowParentHandle: Integer read Get_WindowParentHandle write Set_WindowParentHandle;
    property CopiesToPrinter: Smallint read Get_CopiesToPrinter write Set_CopiesToPrinter;
    property Action: Smallint read Get_Action write Set_Action;
    property LastErrorNumber: Smallint read Get_LastErrorNumber write Set_LastErrorNumber;
    property LastErrorString: WideString read Get_LastErrorString write Set_LastErrorString;
    property Connect: WideString read Get_Connect write Set_Connect;
    property SessionHandle: Integer read Get_SessionHandle write Set_SessionHandle;
    property UserName: WideString read Get_UserName write Set_UserName;
    property Password: WideString read Get_Password write Set_Password;
    property Destination: DestinationConstants read Get_Destination write Set_Destination;
    property PrintFileType: PrintFileTypeConstants read Get_PrintFileType write Set_PrintFileType;
    property WindowBorderStyle: WindowBorderStyleConstants read Get_WindowBorderStyle write Set_WindowBorderStyle;
    property BoundReportHeading: WideString read Get_BoundReportHeading write Set_BoundReportHeading;
    property BoundReportFooter: WordBool read Get_BoundReportFooter write Set_BoundReportFooter;
    property ReportSource: ReportSourceConstants read Get_ReportSource write Set_ReportSource;
    property DetailCopies: Smallint read Get_DetailCopies write Set_DetailCopies;
    property DiscardSavedData: WordBool read Get_DiscardSavedData write Set_DiscardSavedData;
    property EMailCCList: WideString read Get_EMailCCList write Set_EMailCCList;
    property EMailMessage: WideString read Get_EMailMessage write Set_EMailMessage;
    property EMailSubject: WideString read Get_EMailSubject write Set_EMailSubject;
    property EMailToList: WideString read Get_EMailToList write Set_EMailToList;
    property MarginBottom: Smallint read Get_MarginBottom write Set_MarginBottom;
    property MarginLeft: Smallint read Get_MarginLeft write Set_MarginLeft;
    property MarginRight: Smallint read Get_MarginRight write Set_MarginRight;
    property MarginTop: Smallint read Get_MarginTop write Set_MarginTop;
    property PrintDay: Smallint read Get_PrintDay write Set_PrintDay;
    property PrinterCollation: PrinterCollationConstants read Get_PrinterCollation write Set_PrinterCollation;
    property PrinterCopies: Smallint read Get_PrinterCopies write Set_PrinterCopies;
    property PrinterDriver: WideString read Get_PrinterDriver write Set_PrinterDriver;
    property PrinterName: WideString read Get_PrinterName write Set_PrinterName;
    property PrinterPort: WideString read Get_PrinterPort write Set_PrinterPort;
    property PrinterStartPage: Smallint read Get_PrinterStartPage write Set_PrinterStartPage;
    property PrinterStopPage: Smallint read Get_PrinterStopPage write Set_PrinterStopPage;
    property PrintFileCharSepQuote: WideString read Get_PrintFileCharSepQuote write Set_PrintFileCharSepQuote;
    property PrintFileCharSepSeparator: WideString read Get_PrintFileCharSepSeparator write Set_PrintFileCharSepSeparator;
    property PrintFileUseRptDateFmt: WordBool read Get_PrintFileUseRptDateFmt write Set_PrintFileUseRptDateFmt;
    property PrintFileUseRptNumberFmt: WordBool read Get_PrintFileUseRptNumberFmt write Set_PrintFileUseRptNumberFmt;
    property PrintMonth: Smallint read Get_PrintMonth write Set_PrintMonth;
    property PrintYear: Smallint read Get_PrintYear write Set_PrintYear;
    property RecordsPrinted: Integer read Get_RecordsPrinted write Set_RecordsPrinted;
    property RecordsRead: Integer read Get_RecordsRead write Set_RecordsRead;
    property RecordsSelected: Integer read Get_RecordsSelected write Set_RecordsSelected;
    property ReportDisplayPage: Smallint read Get_ReportDisplayPage write Set_ReportDisplayPage;
    property ReportLatestPage: Smallint read Get_ReportLatestPage write Set_ReportLatestPage;
    property ReportStartPage: Smallint read Get_ReportStartPage write Set_ReportStartPage;
    property SQLQuery: WideString read Get_SQLQuery write Set_SQLQuery;
    property Status: Smallint read Get_Status write Set_Status;
    property WindowControls: WordBool read Get_WindowControls write Set_WindowControls;
    property WindowState: WindowStateConstants read Get_WindowState write Set_WindowState;
    property DialogParentHandle: Integer read Get_DialogParentHandle write Set_DialogParentHandle;
    property ProgressDialog: WordBool read Get_ProgressDialog write Set_ProgressDialog;
    property ExchangeProfile: WideString read Get_ExchangeProfile write Set_ExchangeProfile;
    property ExchangePassword: WideString read Get_ExchangePassword write Set_ExchangePassword;
    property ExchangeFolder: WideString read Get_ExchangeFolder write Set_ExchangeFolder;
    property PrintFileODBCSource: WideString read Get_PrintFileODBCSource write Set_PrintFileODBCSource;
    property PrintFileODBCUser: WideString read Get_PrintFileODBCUser write Set_PrintFileODBCUser;
    property PrintFileODBCPassword: WideString read Get_PrintFileODBCPassword write Set_PrintFileODBCPassword;
    property PrintFileODBCTable: WideString read Get_PrintFileODBCTable write Set_PrintFileODBCTable;
    property PrintFileLinesPerPage: Smallint read Get_PrintFileLinesPerPage write Set_PrintFileLinesPerPage;
    property SubreportToChange: WideString read Get_SubreportToChange write Set_SubreportToChange;
    property ReportTitle: WideString read Get_ReportTitle write Set_ReportTitle;
    property GridSource: WideString read Get_GridSource write Set_GridSource;
    property WindowShowGroupTree: WordBool read Get_WindowShowGroupTree write Set_WindowShowGroupTree;
    property WindowAllowDrillDown: WordBool read Get_WindowAllowDrillDown write Set_WindowAllowDrillDown;
    property WindowShowNavigationCtls: WordBool read Get_WindowShowNavigationCtls write Set_WindowShowNavigationCtls;
    property WindowShowCancelBtn: WordBool read Get_WindowShowCancelBtn write Set_WindowShowCancelBtn;
    property WindowShowPrintBtn: WordBool read Get_WindowShowPrintBtn write Set_WindowShowPrintBtn;
    property WindowShowExportBtn: WordBool read Get_WindowShowExportBtn write Set_WindowShowExportBtn;
    property WindowShowZoomCtl: WordBool read Get_WindowShowZoomCtl write Set_WindowShowZoomCtl;
    property WindowShowCloseBtn: WordBool read Get_WindowShowCloseBtn write Set_WindowShowCloseBtn;
    property WindowShowProgressCtls: WordBool read Get_WindowShowProgressCtls write Set_WindowShowProgressCtls;
    property WindowShowSearchBtn: WordBool read Get_WindowShowSearchBtn write Set_WindowShowSearchBtn;
    property WindowShowPrintSetupBtn: WordBool read Get_WindowShowPrintSetupBtn write Set_WindowShowPrintSetupBtn;
    property WindowShowRefreshBtn: WordBool read Get_WindowShowRefreshBtn write Set_WindowShowRefreshBtn;
    property DataSource: IRowCursor read Get_DataSource write Set_DataSource;
    property Formulas[index: Smallint]: WideString read Get_Formulas write Set_Formulas;
    property SortFields[index: Smallint]: WideString read Get_SortFields write Set_SortFields;
    property GroupSortFields[index: Smallint]: WideString read Get_GroupSortFields write Set_GroupSortFields;
    property DataFiles[index: Smallint]: WideString read Get_DataFiles write Set_DataFiles;
    property GroupCondition[index: Smallint]: WideString read Get_GroupCondition write Set_GroupCondition;
    property SectionMinHeight[index: Smallint]: WideString read Get_SectionMinHeight write Set_SectionMinHeight;
    property SectionFormat[index: Smallint]: WideString read Get_SectionFormat write Set_SectionFormat;
    property SectionLineHeight[index: Smallint]: WideString read Get_SectionLineHeight write Set_SectionLineHeight;
    property SectionFont[index: Smallint]: WideString read Get_SectionFont write Set_SectionFont;
    property StoredProcParam[index: Smallint]: WideString read Get_StoredProcParam write Set_StoredProcParam;
    property GraphType[index: Smallint]: WideString read Get_GraphType write Set_GraphType;
    property GraphData[index: Smallint]: WideString read Get_GraphData write Set_GraphData;
    property GraphText[index: Smallint]: WideString read Get_GraphText write Set_GraphText;
    property GraphOptions[index: Smallint]: WideString read Get_GraphOptions write Set_GraphOptions;
    property ParameterFields[index: Smallint]: WideString read Get_ParameterFields write Set_ParameterFields;
    property LogonInfo[index: Smallint]: WideString read Get_LogonInfo write Set_LogonInfo;
  end;

// *********************************************************************//
// DispIntf:  CrystalCtrlDisp
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {00025614-0000-0000-C000-000000000046}
// *********************************************************************//
  CrystalCtrlDisp = dispinterface
    ['{00025614-0000-0000-C000-000000000046}']
    property ReportFileName: WideString dispid 3;
    property WindowLeft: Smallint dispid 4;
    property WindowTop: Smallint dispid 5;
    property WindowWidth: Smallint dispid 6;
    property WindowHeight: Smallint dispid 7;
    property WindowTitle: WideString dispid 8;
    property WindowControlBox: WordBool dispid 9;
    property WindowMaxButton: WordBool dispid 10;
    property WindowMinButton: WordBool dispid 11;
    property PrintFileName: WideString dispid 12;
    property SelectionFormula: WideString dispid 13;
    property GroupSelectionFormula: WideString dispid 14;
    property WindowParentHandle: Integer dispid 15;
    property CopiesToPrinter: Smallint dispid 16;
    property Action: Smallint dispid 17;
    property LastErrorNumber: Smallint dispid 18;
    property LastErrorString: WideString dispid 19;
    property Connect: WideString dispid 20;
    property SessionHandle: Integer dispid 21;
    property UserName: WideString dispid 22;
    property Password: WideString dispid 23;
    property Destination: DestinationConstants dispid 24;
    property PrintFileType: PrintFileTypeConstants dispid 25;
    property WindowBorderStyle: WindowBorderStyleConstants dispid 26;
    property BoundReportHeading: WideString dispid 1;
    property BoundReportFooter: WordBool dispid 2;
    property ReportSource: ReportSourceConstants dispid 27;
    property DetailCopies: Smallint dispid 33;
    property DiscardSavedData: WordBool dispid 34;
    property EMailCCList: WideString dispid 35;
    property EMailMessage: WideString dispid 36;
    property EMailSubject: WideString dispid 37;
    property EMailToList: WideString dispid 38;
    property MarginBottom: Smallint dispid 40;
    property MarginLeft: Smallint dispid 41;
    property MarginRight: Smallint dispid 42;
    property MarginTop: Smallint dispid 43;
    property PrintDay: Smallint dispid 44;
    property PrinterCollation: PrinterCollationConstants dispid 45;
    property PrinterCopies: Smallint dispid 46;
    property PrinterDriver: WideString dispid 47;
    property PrinterName: WideString dispid 48;
    property PrinterPort: WideString dispid 49;
    property PrinterStartPage: Smallint dispid 50;
    property PrinterStopPage: Smallint dispid 51;
    property PrintFileCharSepQuote: WideString dispid 52;
    property PrintFileCharSepSeparator: WideString dispid 53;
    property PrintFileUseRptDateFmt: WordBool dispid 54;
    property PrintFileUseRptNumberFmt: WordBool dispid 55;
    property PrintMonth: Smallint dispid 56;
    property PrintYear: Smallint dispid 57;
    property RecordsPrinted: Integer dispid 58;
    property RecordsRead: Integer dispid 59;
    property RecordsSelected: Integer dispid 60;
    property ReportDisplayPage: Smallint dispid 61;
    property ReportLatestPage: Smallint dispid 62;
    property ReportStartPage: Smallint dispid 63;
    property SQLQuery: WideString dispid 64;
    property Status: Smallint dispid 65;
    property WindowControls: WordBool dispid 66;
    property WindowState: WindowStateConstants dispid 67;
    property DialogParentHandle: Integer dispid 79;
    property ProgressDialog: WordBool dispid 80;
    property ExchangeProfile: WideString dispid 91;
    property ExchangePassword: WideString dispid 92;
    property ExchangeFolder: WideString dispid 93;
    property PrintFileODBCSource: WideString dispid 94;
    property PrintFileODBCUser: WideString dispid 95;
    property PrintFileODBCPassword: WideString dispid 96;
    property PrintFileODBCTable: WideString dispid 97;
    property PrintFileLinesPerPage: Smallint dispid 98;
    property SubreportToChange: WideString dispid 108;
    property ReportTitle: WideString dispid 111;
    property GridSource: WideString dispid 114;
    property WindowShowGroupTree: WordBool dispid 115;
    property WindowAllowDrillDown: WordBool dispid 116;
    property WindowShowNavigationCtls: WordBool dispid 117;
    property WindowShowCancelBtn: WordBool dispid 118;
    property WindowShowPrintBtn: WordBool dispid 119;
    property WindowShowExportBtn: WordBool dispid 120;
    property WindowShowZoomCtl: WordBool dispid 121;
    property WindowShowCloseBtn: WordBool dispid 122;
    property WindowShowProgressCtls: WordBool dispid 123;
    property WindowShowSearchBtn: WordBool dispid 124;
    property WindowShowPrintSetupBtn: WordBool dispid 125;
    property WindowShowRefreshBtn: WordBool dispid 126;
    property DataSource: IRowCursor dispid 256;
    property Formulas[index: Smallint]: WideString dispid 29;
    property SortFields[index: Smallint]: WideString dispid 30;
    property GroupSortFields[index: Smallint]: WideString dispid 31;
    property DataFiles[index: Smallint]: WideString dispid 32;
    property GroupCondition[index: Smallint]: WideString dispid 68;
    property SectionMinHeight[index: Smallint]: WideString dispid 69;
    property SectionFormat[index: Smallint]: WideString dispid 70;
    property SectionLineHeight[index: Smallint]: WideString dispid 71;
    property SectionFont[index: Smallint]: WideString dispid 72;
    property StoredProcParam[index: Smallint]: WideString dispid 73;
    property GraphType[index: Smallint]: WideString dispid 74;
    property GraphData[index: Smallint]: WideString dispid 75;
    property GraphText[index: Smallint]: WideString dispid 76;
    property GraphOptions[index: Smallint]: WideString dispid 77;
    property ParameterFields[index: Smallint]: WideString dispid 81;
    property LogonInfo[index: Smallint]: WideString dispid 99;
    procedure AboutBox; dispid -552;
    function  PrintReport: Smallint; dispid 28;
    procedure PageFirst; dispid 82;
    procedure PageNext; dispid 83;
    procedure PageLast; dispid 84;
    procedure PagePrevious; dispid 85;
    function  PageCount: Smallint; dispid 86;
    procedure PageShow(p: Smallint); dispid 87;
    procedure PageZoom(z: Smallint); dispid 88;
    procedure PageZoomNext; dispid 89;
    procedure PrinterSelect; dispid 90;
    function  FetchSelectionFormula: WideString; dispid 100;
    procedure ReplaceSelectionFormula(const s: WideString); dispid 101;
    function  RetrieveDataFiles: Smallint; dispid 102;
    function  RetrieveLogonInfo: Smallint; dispid 103;
    procedure RetrieveSQLQuery; dispid 104;
    function  RetrieveStoredProcParams: Smallint; dispid 105;
    function  LogOnServer(const dll: WideString; const server: WideString; 
                          const database: WideString; const userid: WideString; 
                          const Password: WideString): Smallint; dispid 106;
    procedure LogOffServer(id: Smallint; all: WordBool); dispid 107;
    function  GetNSubreports: Smallint; dispid 109;
    function  GetNthSubreportName(n: Smallint): WideString; dispid 110;
    procedure Reset; dispid 112;
    procedure SpecifyDataSourceField(n: Smallint; const name: WideString; width: Smallint); dispid 113;
    procedure SetTablePrivateData(TableIndex: Smallint; DataTag: Integer; Data: OleVariant); dispid 127;
  end;

// *********************************************************************//
// DispIntf:  CrystalReportEvents
// Flags:     (4112) Hidden Dispatchable
// GUID:      {00025603-0000-0000-C000-000000000046}
// *********************************************************************//
  CrystalReportEvents = dispinterface
    ['{00025603-0000-0000-C000-000000000046}']
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TCrystalReport
// Help String      : Crystal Report Control
// Default Interface: CrystalCtrl
// Def. Intf. DISP? : No
// Event   Interface: CrystalReportEvents
// TypeFlags        : (34) CanCreate Control
// *********************************************************************//
  TCrystalReport = class(TOleControl)
  private
    FIntf: CrystalCtrl;
    function  GetControlInterface: CrystalCtrl;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
    function  Get_DataSource: IRowCursor;
    procedure Set_DataSource(const px: IRowCursor);
    function  Get_Formulas(index: Smallint): WideString;
    procedure Set_Formulas(index: Smallint; const px: WideString);
    function  Get_SortFields(index: Smallint): WideString;
    procedure Set_SortFields(index: Smallint; const px: WideString);
    function  Get_GroupSortFields(index: Smallint): WideString;
    procedure Set_GroupSortFields(index: Smallint; const px: WideString);
    function  Get_DataFiles(index: Smallint): WideString;
    procedure Set_DataFiles(index: Smallint; const px: WideString);
    function  Get_GroupCondition(index: Smallint): WideString;
    procedure Set_GroupCondition(index: Smallint; const px: WideString);
    function  Get_SectionMinHeight(index: Smallint): WideString;
    procedure Set_SectionMinHeight(index: Smallint; const px: WideString);
    function  Get_SectionFormat(index: Smallint): WideString;
    procedure Set_SectionFormat(index: Smallint; const px: WideString);
    function  Get_SectionLineHeight(index: Smallint): WideString;
    procedure Set_SectionLineHeight(index: Smallint; const px: WideString);
    function  Get_SectionFont(index: Smallint): WideString;
    procedure Set_SectionFont(index: Smallint; const px: WideString);
    function  Get_StoredProcParam(index: Smallint): WideString;
    procedure Set_StoredProcParam(index: Smallint; const px: WideString);
    function  Get_GraphType(index: Smallint): WideString;
    procedure Set_GraphType(index: Smallint; const px: WideString);
    function  Get_GraphData(index: Smallint): WideString;
    procedure Set_GraphData(index: Smallint; const px: WideString);
    function  Get_GraphText(index: Smallint): WideString;
    procedure Set_GraphText(index: Smallint; const px: WideString);
    function  Get_GraphOptions(index: Smallint): WideString;
    procedure Set_GraphOptions(index: Smallint; const px: WideString);
    function  Get_ParameterFields(index: Smallint): WideString;
    procedure Set_ParameterFields(index: Smallint; const px: WideString);
    function  Get_LogonInfo(index: Smallint): WideString;
    procedure Set_LogonInfo(index: Smallint; const px: WideString);
  public
    procedure AboutBox;
    function  PrintReport: Smallint;
    procedure PageFirst;
    procedure PageNext;
    procedure PageLast;
    procedure PagePrevious;
    function  PageCount: Smallint;
    procedure PageShow(p: Smallint);
    procedure PageZoom(z: Smallint);
    procedure PageZoomNext;
    procedure PrinterSelect;
    function  FetchSelectionFormula: WideString;
    procedure ReplaceSelectionFormula(const s: WideString);
    function  RetrieveDataFiles: Smallint;
    function  RetrieveLogonInfo: Smallint;
    procedure RetrieveSQLQuery;
    function  RetrieveStoredProcParams: Smallint;
    function  LogOnServer(const dll: WideString; const server: WideString; 
                          const database: WideString; const userid: WideString; 
                          const Password: WideString): Smallint;
    procedure LogOffServer(id: Smallint; all: WordBool);
    function  GetNSubreports: Smallint;
    function  GetNthSubreportName(n: Smallint): WideString;
    procedure Reset;
    procedure SpecifyDataSourceField(n: Smallint; const name: WideString; width: Smallint);
    procedure SetTablePrivateData(TableIndex: Smallint; DataTag: Integer; Data: OleVariant);
    property  ControlInterface: CrystalCtrl read GetControlInterface;
    property  DefaultInterface: CrystalCtrl read GetControlInterface;
    property Formulas[index: Smallint]: WideString read Get_Formulas write Set_Formulas;
    property SortFields[index: Smallint]: WideString read Get_SortFields write Set_SortFields;
    property GroupSortFields[index: Smallint]: WideString read Get_GroupSortFields write Set_GroupSortFields;
    property DataFiles[index: Smallint]: WideString read Get_DataFiles write Set_DataFiles;
    property GroupCondition[index: Smallint]: WideString read Get_GroupCondition write Set_GroupCondition;
    property SectionMinHeight[index: Smallint]: WideString read Get_SectionMinHeight write Set_SectionMinHeight;
    property SectionFormat[index: Smallint]: WideString read Get_SectionFormat write Set_SectionFormat;
    property SectionLineHeight[index: Smallint]: WideString read Get_SectionLineHeight write Set_SectionLineHeight;
    property SectionFont[index: Smallint]: WideString read Get_SectionFont write Set_SectionFont;
    property StoredProcParam[index: Smallint]: WideString read Get_StoredProcParam write Set_StoredProcParam;
    property GraphType[index: Smallint]: WideString read Get_GraphType write Set_GraphType;
    property GraphData[index: Smallint]: WideString read Get_GraphData write Set_GraphData;
    property GraphText[index: Smallint]: WideString read Get_GraphText write Set_GraphText;
    property GraphOptions[index: Smallint]: WideString read Get_GraphOptions write Set_GraphOptions;
    property ParameterFields[index: Smallint]: WideString read Get_ParameterFields write Set_ParameterFields;
    property LogonInfo[index: Smallint]: WideString read Get_LogonInfo write Set_LogonInfo;
  published
    property ReportFileName: WideString index 3 read GetWideStringProp write SetWideStringProp stored False;
    property WindowLeft: Smallint index 4 read GetSmallintProp write SetSmallintProp stored False;
    property WindowTop: Smallint index 5 read GetSmallintProp write SetSmallintProp stored False;
    property WindowWidth: Smallint index 6 read GetSmallintProp write SetSmallintProp stored False;
    property WindowHeight: Smallint index 7 read GetSmallintProp write SetSmallintProp stored False;
    property WindowTitle: WideString index 8 read GetWideStringProp write SetWideStringProp stored False;
    property WindowControlBox: WordBool index 9 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowMaxButton: WordBool index 10 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowMinButton: WordBool index 11 read GetWordBoolProp write SetWordBoolProp stored False;
    property PrintFileName: WideString index 12 read GetWideStringProp write SetWideStringProp stored False;
    property SelectionFormula: WideString index 13 read GetWideStringProp write SetWideStringProp stored False;
    property GroupSelectionFormula: WideString index 14 read GetWideStringProp write SetWideStringProp stored False;
    property WindowParentHandle: Integer index 15 read GetIntegerProp write SetIntegerProp stored False;
    property CopiesToPrinter: Smallint index 16 read GetSmallintProp write SetSmallintProp stored False;
    property Action: Smallint index 17 read GetSmallintProp write SetSmallintProp stored False;
    property LastErrorNumber: Smallint index 18 read GetSmallintProp write SetSmallintProp stored False;
    property LastErrorString: WideString index 19 read GetWideStringProp write SetWideStringProp stored False;
    property Connect: WideString index 20 read GetWideStringProp write SetWideStringProp stored False;
    property SessionHandle: Integer index 21 read GetIntegerProp write SetIntegerProp stored False;
    property UserName: WideString index 22 read GetWideStringProp write SetWideStringProp stored False;
    property Password: WideString index 23 read GetWideStringProp write SetWideStringProp stored False;
    property Destination: TOleEnum index 24 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property PrintFileType: TOleEnum index 25 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property WindowBorderStyle: TOleEnum index 26 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property BoundReportHeading: WideString index 1 read GetWideStringProp write SetWideStringProp stored False;
    property BoundReportFooter: WordBool index 2 read GetWordBoolProp write SetWordBoolProp stored False;
    property ReportSource: TOleEnum index 27 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property DetailCopies: Smallint index 33 read GetSmallintProp write SetSmallintProp stored False;
    property DiscardSavedData: WordBool index 34 read GetWordBoolProp write SetWordBoolProp stored False;
    property EMailCCList: WideString index 35 read GetWideStringProp write SetWideStringProp stored False;
    property EMailMessage: WideString index 36 read GetWideStringProp write SetWideStringProp stored False;
    property EMailSubject: WideString index 37 read GetWideStringProp write SetWideStringProp stored False;
    property EMailToList: WideString index 38 read GetWideStringProp write SetWideStringProp stored False;
    property MarginBottom: Smallint index 40 read GetSmallintProp write SetSmallintProp stored False;
    property MarginLeft: Smallint index 41 read GetSmallintProp write SetSmallintProp stored False;
    property MarginRight: Smallint index 42 read GetSmallintProp write SetSmallintProp stored False;
    property MarginTop: Smallint index 43 read GetSmallintProp write SetSmallintProp stored False;
    property PrintDay: Smallint index 44 read GetSmallintProp write SetSmallintProp stored False;
    property PrinterCollation: TOleEnum index 45 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property PrinterCopies: Smallint index 46 read GetSmallintProp write SetSmallintProp stored False;
    property PrinterDriver: WideString index 47 read GetWideStringProp write SetWideStringProp stored False;
    property PrinterName: WideString index 48 read GetWideStringProp write SetWideStringProp stored False;
    property PrinterPort: WideString index 49 read GetWideStringProp write SetWideStringProp stored False;
    property PrinterStartPage: Smallint index 50 read GetSmallintProp write SetSmallintProp stored False;
    property PrinterStopPage: Smallint index 51 read GetSmallintProp write SetSmallintProp stored False;
    property PrintFileCharSepQuote: WideString index 52 read GetWideStringProp write SetWideStringProp stored False;
    property PrintFileCharSepSeparator: WideString index 53 read GetWideStringProp write SetWideStringProp stored False;
    property PrintFileUseRptDateFmt: WordBool index 54 read GetWordBoolProp write SetWordBoolProp stored False;
    property PrintFileUseRptNumberFmt: WordBool index 55 read GetWordBoolProp write SetWordBoolProp stored False;
    property PrintMonth: Smallint index 56 read GetSmallintProp write SetSmallintProp stored False;
    property PrintYear: Smallint index 57 read GetSmallintProp write SetSmallintProp stored False;
    property RecordsPrinted: Integer index 58 read GetIntegerProp write SetIntegerProp stored False;
    property RecordsRead: Integer index 59 read GetIntegerProp write SetIntegerProp stored False;
    property RecordsSelected: Integer index 60 read GetIntegerProp write SetIntegerProp stored False;
    property ReportDisplayPage: Smallint index 61 read GetSmallintProp write SetSmallintProp stored False;
    property ReportLatestPage: Smallint index 62 read GetSmallintProp write SetSmallintProp stored False;
    property ReportStartPage: Smallint index 63 read GetSmallintProp write SetSmallintProp stored False;
    property SQLQuery: WideString index 64 read GetWideStringProp write SetWideStringProp stored False;
    property Status: Smallint index 65 read GetSmallintProp write SetSmallintProp stored False;
    property WindowControls: WordBool index 66 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowState: TOleEnum index 67 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property DialogParentHandle: Integer index 79 read GetIntegerProp write SetIntegerProp stored False;
    property ProgressDialog: WordBool index 80 read GetWordBoolProp write SetWordBoolProp stored False;
    property ExchangeProfile: WideString index 91 read GetWideStringProp write SetWideStringProp stored False;
    property ExchangePassword: WideString index 92 read GetWideStringProp write SetWideStringProp stored False;
    property ExchangeFolder: WideString index 93 read GetWideStringProp write SetWideStringProp stored False;
    property PrintFileODBCSource: WideString index 94 read GetWideStringProp write SetWideStringProp stored False;
    property PrintFileODBCUser: WideString index 95 read GetWideStringProp write SetWideStringProp stored False;
    property PrintFileODBCPassword: WideString index 96 read GetWideStringProp write SetWideStringProp stored False;
    property PrintFileODBCTable: WideString index 97 read GetWideStringProp write SetWideStringProp stored False;
    property PrintFileLinesPerPage: Smallint index 98 read GetSmallintProp write SetSmallintProp stored False;
    property SubreportToChange: WideString index 108 read GetWideStringProp write SetWideStringProp stored False;
    property ReportTitle: WideString index 111 read GetWideStringProp write SetWideStringProp stored False;
    property GridSource: WideString index 114 read GetWideStringProp write SetWideStringProp stored False;
    property WindowShowGroupTree: WordBool index 115 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowAllowDrillDown: WordBool index 116 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowNavigationCtls: WordBool index 117 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowCancelBtn: WordBool index 118 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowPrintBtn: WordBool index 119 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowExportBtn: WordBool index 120 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowZoomCtl: WordBool index 121 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowCloseBtn: WordBool index 122 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowProgressCtls: WordBool index 123 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowSearchBtn: WordBool index 124 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowPrintSetupBtn: WordBool index 125 read GetWordBoolProp write SetWordBoolProp stored False;
    property WindowShowRefreshBtn: WordBool index 126 read GetWordBoolProp write SetWordBoolProp stored False;
    property DataSource: IRowCursor read Get_DataSource write Set_DataSource stored False;
  end;

procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

implementation

uses ComObj;

procedure TCrystalReport.InitControlData;
const
  CControlData: TControlData2 = (
    ClassID: '{00025601-0000-0000-C000-000000000046}';
    EventIID: '';
    EventCount: 0;
    EventDispIDs: nil;
    LicenseKey: nil (*HR:$00000000*);
    Flags: $00000000;
    Version: 401);
begin
  ControlData := @CControlData;
end;

procedure TCrystalReport.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as CrystalCtrl;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TCrystalReport.GetControlInterface: CrystalCtrl;
begin
  CreateControl;
  Result := FIntf;
end;

function  TCrystalReport.Get_DataSource: IRowCursor;
begin
  Result := DefaultInterface.DataSource;
end;

procedure TCrystalReport.Set_DataSource(const px: IRowCursor);
begin
  DefaultInterface.DataSource := px;
end;

function  TCrystalReport.Get_Formulas(index: Smallint): WideString;
begin
  Result := DefaultInterface.Formulas[index];
end;

procedure TCrystalReport.Set_Formulas(index: Smallint; const px: WideString);
  { Warning: The property Formulas has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Formulas := px;
end;

function  TCrystalReport.Get_SortFields(index: Smallint): WideString;
begin
  Result := DefaultInterface.SortFields[index];
end;

procedure TCrystalReport.Set_SortFields(index: Smallint; const px: WideString);
  { Warning: The property SortFields has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.SortFields := px;
end;

function  TCrystalReport.Get_GroupSortFields(index: Smallint): WideString;
begin
  Result := DefaultInterface.GroupSortFields[index];
end;

procedure TCrystalReport.Set_GroupSortFields(index: Smallint; const px: WideString);
  { Warning: The property GroupSortFields has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GroupSortFields := px;
end;

function  TCrystalReport.Get_DataFiles(index: Smallint): WideString;
begin
  Result := DefaultInterface.DataFiles[index];
end;

procedure TCrystalReport.Set_DataFiles(index: Smallint; const px: WideString);
  { Warning: The property DataFiles has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.DataFiles := px;
end;

function  TCrystalReport.Get_GroupCondition(index: Smallint): WideString;
begin
  Result := DefaultInterface.GroupCondition[index];
end;

procedure TCrystalReport.Set_GroupCondition(index: Smallint; const px: WideString);
  { Warning: The property GroupCondition has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GroupCondition := px;
end;

function  TCrystalReport.Get_SectionMinHeight(index: Smallint): WideString;
begin
  Result := DefaultInterface.SectionMinHeight[index];
end;

procedure TCrystalReport.Set_SectionMinHeight(index: Smallint; const px: WideString);
  { Warning: The property SectionMinHeight has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.SectionMinHeight := px;
end;

function  TCrystalReport.Get_SectionFormat(index: Smallint): WideString;
begin
  Result := DefaultInterface.SectionFormat[index];
end;

procedure TCrystalReport.Set_SectionFormat(index: Smallint; const px: WideString);
  { Warning: The property SectionFormat has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.SectionFormat := px;
end;

function  TCrystalReport.Get_SectionLineHeight(index: Smallint): WideString;
begin
  Result := DefaultInterface.SectionLineHeight[index];
end;

procedure TCrystalReport.Set_SectionLineHeight(index: Smallint; const px: WideString);
  { Warning: The property SectionLineHeight has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.SectionLineHeight := px;
end;

function  TCrystalReport.Get_SectionFont(index: Smallint): WideString;
begin
  Result := DefaultInterface.SectionFont[index];
end;

procedure TCrystalReport.Set_SectionFont(index: Smallint; const px: WideString);
  { Warning: The property SectionFont has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.SectionFont := px;
end;

function  TCrystalReport.Get_StoredProcParam(index: Smallint): WideString;
begin
  Result := DefaultInterface.StoredProcParam[index];
end;

procedure TCrystalReport.Set_StoredProcParam(index: Smallint; const px: WideString);
  { Warning: The property StoredProcParam has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.StoredProcParam := px;
end;

function  TCrystalReport.Get_GraphType(index: Smallint): WideString;
begin
  Result := DefaultInterface.GraphType[index];
end;

procedure TCrystalReport.Set_GraphType(index: Smallint; const px: WideString);
  { Warning: The property GraphType has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GraphType := px;
end;

function  TCrystalReport.Get_GraphData(index: Smallint): WideString;
begin
  Result := DefaultInterface.GraphData[index];
end;

procedure TCrystalReport.Set_GraphData(index: Smallint; const px: WideString);
  { Warning: The property GraphData has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GraphData := px;
end;

function  TCrystalReport.Get_GraphText(index: Smallint): WideString;
begin
  Result := DefaultInterface.GraphText[index];
end;

procedure TCrystalReport.Set_GraphText(index: Smallint; const px: WideString);
  { Warning: The property GraphText has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GraphText := px;
end;

function  TCrystalReport.Get_GraphOptions(index: Smallint): WideString;
begin
  Result := DefaultInterface.GraphOptions[index];
end;

procedure TCrystalReport.Set_GraphOptions(index: Smallint; const px: WideString);
  { Warning: The property GraphOptions has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GraphOptions := px;
end;

function  TCrystalReport.Get_ParameterFields(index: Smallint): WideString;
begin
  Result := DefaultInterface.ParameterFields[index];
end;

procedure TCrystalReport.Set_ParameterFields(index: Smallint; const px: WideString);
  { Warning: The property ParameterFields has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ParameterFields := px;
end;

function  TCrystalReport.Get_LogonInfo(index: Smallint): WideString;
begin
  Result := DefaultInterface.LogonInfo[index];
end;

procedure TCrystalReport.Set_LogonInfo(index: Smallint; const px: WideString);
  { Warning: The property LogonInfo has a setter and a getter whose
  types do not match. Delphi was unable to generate a property of
  this sort and so is using a Variant to set the property instead. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogonInfo := px;
end;

procedure TCrystalReport.AboutBox;
begin
  DefaultInterface.AboutBox;
end;

function  TCrystalReport.PrintReport: Smallint;
begin
  Result := DefaultInterface.PrintReport;
end;

procedure TCrystalReport.PageFirst;
begin
  DefaultInterface.PageFirst;
end;

procedure TCrystalReport.PageNext;
begin
  DefaultInterface.PageNext;
end;

procedure TCrystalReport.PageLast;
begin
  DefaultInterface.PageLast;
end;

procedure TCrystalReport.PagePrevious;
begin
  DefaultInterface.PagePrevious;
end;

function  TCrystalReport.PageCount: Smallint;
begin
  Result := DefaultInterface.PageCount;
end;

procedure TCrystalReport.PageShow(p: Smallint);
begin
  DefaultInterface.PageShow(p);
end;

procedure TCrystalReport.PageZoom(z: Smallint);
begin
  DefaultInterface.PageZoom(z);
end;

procedure TCrystalReport.PageZoomNext;
begin
  DefaultInterface.PageZoomNext;
end;

procedure TCrystalReport.PrinterSelect;
begin
  DefaultInterface.PrinterSelect;
end;

function  TCrystalReport.FetchSelectionFormula: WideString;
begin
  Result := DefaultInterface.FetchSelectionFormula;
end;

procedure TCrystalReport.ReplaceSelectionFormula(const s: WideString);
begin
  DefaultInterface.ReplaceSelectionFormula(s);
end;

function  TCrystalReport.RetrieveDataFiles: Smallint;
begin
  Result := DefaultInterface.RetrieveDataFiles;
end;

function  TCrystalReport.RetrieveLogonInfo: Smallint;
begin
  Result := DefaultInterface.RetrieveLogonInfo;
end;

procedure TCrystalReport.RetrieveSQLQuery;
begin
  DefaultInterface.RetrieveSQLQuery;
end;

function  TCrystalReport.RetrieveStoredProcParams: Smallint;
begin
  Result := DefaultInterface.RetrieveStoredProcParams;
end;

function  TCrystalReport.LogOnServer(const dll: WideString; const server: WideString; 
                                     const database: WideString; const userid: WideString; 
                                     const Password: WideString): Smallint;
begin
  Result := DefaultInterface.LogOnServer(dll, server, database, userid, Password);
end;

procedure TCrystalReport.LogOffServer(id: Smallint; all: WordBool);
begin
  DefaultInterface.LogOffServer(id, all);
end;

function  TCrystalReport.GetNSubreports: Smallint;
begin
  Result := DefaultInterface.GetNSubreports;
end;

function  TCrystalReport.GetNthSubreportName(n: Smallint): WideString;
begin
  Result := DefaultInterface.GetNthSubreportName(n);
end;

procedure TCrystalReport.Reset;
begin
  DefaultInterface.Reset;
end;

procedure TCrystalReport.SpecifyDataSourceField(n: Smallint; const name: WideString; width: Smallint);
begin
  DefaultInterface.SpecifyDataSourceField(n, name, width);
end;

procedure TCrystalReport.SetTablePrivateData(TableIndex: Smallint; DataTag: Integer; 
                                             Data: OleVariant);
begin
  DefaultInterface.SetTablePrivateData(TableIndex, DataTag, Data);
end;

procedure Register;
begin
  RegisterComponents('ActiveX',[TCrystalReport]);
end;

end.
