unit uniExibeMsg;

interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Variants,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const
  { constantes utilizadas pela caixa de di�logo que exibe mensagens }
  BT_OK             = 1;
  BT_DOKCANCEL      = 2;
  BT_OKDCANCEL      = 3;
  BT_DSIMNAOCANCEL  = 4;
  BT_SIMDNAOCANCEL  = 5;
  BT_SIMNAODCANCEL  = 6;
  BT_DSIMNAO        = 7;
  BT_SIMDNAO        = 8;
  BT_DRETRYCANCEL   = 9;

  BT_HABILITADO   = 0;
  BT_PRESSIONADO  = 1;
  BT_DESABILITADO = 2;

  ICO_PARE        = 12;
  ICO_EXCLAMACAO  = 13;
  ICO_INFORMACAO  = 14;

  ALTURA_LABEL  = 13;

  LEFT_BTNOK      = 264;
  LEFT_BTNCANC    = 384;
  LEFT_BTNNAO     = 256;
  LEFT_BTNNAOCANC = 328;
  LEFT_BTNSIM     = 128;
  LEFT_BTNSIMOK   = 200;

  LARGURA_FORM = 639;
  ALTURA_FORM = 219;

  LARGURA_PAINEL = 617;
  ALTURA_PAINEL = 177;

  TOP_BTNOK = 144;

  TOP_MENSAGEM1 = 16;
  TOP_MENSAGEM2 = 40;
  TOP_MENSAGEM3 = 64;
  TOP_MENSAGEM4 = 104;

type
 {*======================================================================
 *            CLASSES
 *======================================================================}
  TdlgExibeMsg = class(TForm)
    pnlFundo: TPanel;
    lblMensagem1: TLabel;
    lblMensagem2: TLabel;
    lblMensagem3: TLabel;
    lblMensagem4: TLabel;
    btnSim: TButton;
    btnNao: TButton;
    btnCancelar: TButton;
    btnSimOkRepetir: TButton;
    btnNaoCancelar: TButton;
    btnOk: TButton;
    pnlBotao: TPanel;
    imgStop: TImage;
    imgExclamacao: TImage;
    imgInformacao: TImage;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNaoClick(Sender: TObject);
    procedure btnNaoCancelarClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnSimClick(Sender: TObject);
    procedure btnSimOkRepetirClick(Sender: TObject);
  private
    { Private declarations }
    tcMousePointer: TCursor;

    procedure TamanhoRedim;
    procedure BotoesTratar;
    procedure IconesTratar;
  public
    { Public declarations }
  end;

var
  dlgExibeMsg: TdlgExibeMsg;

implementation

uses uniDados;

{$R *.DFM}

{*----------------------------------------------------------------------
 * TdlgExibeMsg.FormShow    Mostra a janela de dialogo
 *
 *----------------------------------------------------------------------}
procedure TdlgExibeMsg.FormShow(Sender: TObject);
var
  iIndColLinha1: Integer;
  iIndColLinha2: Integer;
  iIndColLinha3: Integer;

//  iConta:         Integer; { �ndice dos labels da caixa de di�logo }
  sRetornoIO:     string;
begin
    try
      tcMousePointer := Screen.Cursor;
      dlgExibeMsg.Caption := dmDados.gsFormCaption;

      sRetornoIO := IORET_FAIL;
      { ERRO_CODIGODEFAULT sempre usa mensagem no formato padr�o }
      if dmDados.giCodMensagem <> ERRO_CODIGODEFAULT then
      begin
        { se achou }
        if not dmDados.Status(dmDados.giConMsg, IOSTATUS_EOF) then
//        if dmDados.gtbMens.FindKey([IntToStr(dmDados.giCodMensagem)]) then
          sRetornoIO := IORET_OK
      end;

      { se n�o encontrou a mensagem }
      if sRetornoIO <> IORET_OK then
      begin
        btnOk.Default := True;            { disponibiliza somente o bot�o OK }
        btnOk.Visible := True;
        btnSim.Visible := False;
        btnNao.Visible := False;
        btnCancelar.Visible := False;
        btnSimOkRepetir.Visible := False;
        btnNaoCancelar.Visible := False;

        btnOk.TabStop := True;
        btnSim.TabStop := False;
        btnNao.TabStop := False;
        btnCancelar.TabStop := False;
        btnSimOkRepetir.TabStop := False;
        btnNaoCancelar.TabStop := False;

        btnOk.Enabled := True;
        btnSim.Enabled := False;
        btnNao.Enabled := False;
        btnCancelar.Enabled := False;
        btnSimOkRepetir.Enabled := False;
        btnNaoCancelar.Enabled := False;

        if dmDados.giCodMensagem = ERRO_CODIGODEFAULT then
        begin
          lblMensagem1.Caption := IntToStr(dmDados.giCodMensagem) + ' - Erro Geral de Execu��o';
          lblMensagem2.Caption := dmDados.gaMsgParm[1];
          lblMensagem3.Caption := dmDados.gaMsgParm[2];
        end
        else
        begin
          lblMensagem1.Caption := IntToStr(dmDados.giCodMensagem) + ' - Mensagem n�o cadastrada';
          lblMensagem2.Caption := VAZIO;
          lblMensagem3.Caption := VAZIO;
        end;
      end
      else
      begin
        BotoesTratar;     { exibe Botoes escolhidos => se h� erro, faz unload }
        IconesTratar;     { exibe �cone escolhido => se h� erro, faz unload   }
        iIndColLinha1 := IOPTR_NOPOINTER;
        iIndColLinha2 := IOPTR_NOPOINTER;
        iIndColLinha3 := IOPTR_NOPOINTER;
        lblMensagem1.Caption := dmDados.LiteralSubstituir(dmDados.ValorColuna(dmDados.giConMsg,FLDMENS_LINHA1,iIndColLinha1), dmDados.gaMsgParm);
        lblMensagem2.Caption := dmDados.LiteralSubstituir(dmDados.ValorColuna(dmDados.giConMsg,FLDMENS_LINHA2,iIndColLinha2), dmDados.gaMsgParm);
        lblMensagem3.Caption := dmDados.LiteralSubstituir(dmDados.ValorColuna(dmDados.giConMsg,FLDMENS_LINHA3,iIndColLinha3), dmDados.gaMsgParm);
//        lblMensagem1.Caption := dmDados.LiteralSubstituir(dmDados.gtbMens.FieldByName(FLDMENS_LINHA1).AsString, dmDados.gaMsgParm);
//        lblMensagem2.Caption := dmDados.LiteralSubstituir(dmDados.gtbMens.FieldByName(FLDMENS_LINHA2).AsString, dmDados.gaMsgParm);
//        lblMensagem3.Caption := dmDados.LiteralSubstituir(dmDados.gtbMens.FieldByName(FLDMENS_LINHA3).AsString, dmDados.gaMsgParm);
      end;

      lblMensagem4.Caption := dmDados.gsInfoAdicionais;
      TamanhoRedim; { redimensiona a caixa de acordo com o no. de linhas com conte�do }

      { Centraliza na Tela }
      Top  := Trunc((Screen.Height - Height) / 2);
      Left := Trunc((Screen.Width - Width) / 2);
      Screen.Cursor := crDefault;
  except
    Close;
  end;
end;

{*----------------------------------------------------------------------
 * TdlgExibeMsg.BotoesTratar
 *          Exibe os bot�es pertencentes � mensagem escolhida.
 *          Os c�digos que representam os conjuntos de bot�es que
 *          devem ser exibidos est�o no GLCONST.BAS (BT_?????) e
 *          s�o utilizados de forma semelhante ao comando "MsgBox".
 *          O valor numerico dessas constantes deve ser utilizado
 *          para cadastrar os bot�es na tabela zMensagens (coluna
 *          "Botoes").
 *----------------------------------------------------------------------}
procedure TdlgExibeMsg.BotoesTratar;
var
  iIndColBotoes: Integer;
  iBotoes:       Integer;
  vBotoes:       Variant;
begin
  try
//    iBotoes := BT_OK;
    iIndColBotoes := IOPTR_NOPOINTER;
    vBotoes := dmDados.ValorColuna(dmDados.giConMsg,FLDMENS_BOTOES,iIndColBotoes);
    if VarAsType(vBotoes,VarString) = IOVAL_NOVALUE then
      iBotoes := BT_OK
    else
      iBotoes := vBotoes;

    case iBotoes of
//    case dmDados.gtbMens.FieldByName(FLDMENS_BOTOES).AsInteger of
      BT_OK:
      begin
        btnOk.Visible := True;
        btnSim.Visible := False;
        btnNao.Visible := False;
        btnCancelar.Visible := False;
        btnSimOkRepetir.Visible := False;
        btnNaoCancelar.Visible := False;

        btnOk.TabStop := True;
        btnSim.TabStop := False;
        btnNao.TabStop := False;
        btnCancelar.TabStop := False;
        btnSimOkRepetir.TabStop := False;
        btnNaoCancelar.TabStop := False;

        btnOk.Enabled := True;
        btnSim.Enabled := False;
        btnNao.Enabled := False;
        btnCancelar.Enabled := False;
        btnSimOkRepetir.Enabled := False;
        btnNaoCancelar.Enabled := False;

        btnOk.Default := True;
        btnOk.TabOrder := 0;
      end;
      BT_DOKCANCEL:
      begin
        btnCancelar.Cancel := True;
        btnOk.Visible := False;
        btnSim.Visible := False;
        btnNao.Visible := False;
        btnCancelar.Visible := False;
        btnSimOkRepetir.Visible := True;
        btnSimOkRepetir.Caption := 'OK';
        btnNaoCancelar.Visible := True;
        btnNaoCancelar.Caption := 'Cancelar';
        btnSimOkRepetir.Default := True;
        btnSimOkRepetir.TabOrder := 0;

        btnOk.TabStop := False;
        btnSim.TabStop := False;
        btnNao.TabStop := False;
        btnCancelar.TabStop := False;
        btnSimOkRepetir.TabStop := True;
        btnNaoCancelar.TabStop := True;

        btnOk.Enabled := False;
        btnSim.Enabled := False;
        btnNao.Enabled := False;
        btnCancelar.Enabled := False;
        btnSimOkRepetir.Enabled := True;
        btnNaoCancelar.Enabled := True;
      end;
      BT_OKDCANCEL:
      begin
        btnCancelar.Cancel := True;
        btnOk.Visible := False;
        btnSim.Visible := False;
        btnNao.Visible := False;
        btnCancelar.Visible := False;
        btnSimOkRepetir.Visible := True;
        btnSimOkRepetir.Caption := 'OK';
        btnNaoCancelar.Visible := True;
        btnNaoCancelar.Caption := 'Cancelar';
        btnNaoCancelar.Default := True;
        btnNaoCancelar.TabOrder := 0;

        btnOk.TabStop := False;
        btnSim.TabStop := False;
        btnNao.TabStop := False;
        btnCancelar.TabStop := False;
        btnSimOkRepetir.TabStop := True;
        btnNaoCancelar.TabStop := True;

        btnOk.Enabled := False;
        btnSim.Enabled := False;
        btnNao.Enabled := False;
        btnCancelar.Enabled := False;
        btnSimOkRepetir.Enabled := True;
        btnNaoCancelar.Enabled := True;
      end;
      BT_DSIMNAOCANCEL:
      begin
        btnCancelar.Cancel := True;
        btnOk.Visible := False;
        btnSim.Visible := True;
        btnNao.Visible := True;
        btnCancelar.Visible := True;
        btnSimOkRepetir.Visible := False;
        btnNaoCancelar.Visible := False;
        btnSim.Default := True;
        btnSim.TabOrder := 0;

        btnOk.TabStop := False;
        btnSim.TabStop := True;
        btnNao.TabStop := True;
        btnCancelar.TabStop := True;
        btnSimOkRepetir.TabStop := False;
        btnNaoCancelar.TabStop := False;

        btnOk.Enabled := False;
        btnSim.Enabled := True;
        btnNao.Enabled := True;
        btnCancelar.Enabled := True;
        btnSimOkRepetir.Enabled := False;
        btnNaoCancelar.Enabled := False;
      end;
      BT_SIMDNAOCANCEL:
      begin
        btnCancelar.Cancel := True;
        btnOk.Visible := False;
        btnSim.Visible := True;
        btnNao.Visible := True;
        btnCancelar.Visible := True;
        btnSimOkRepetir.Visible := False;
        btnNaoCancelar.Visible := False;
        btnNao.Default := True;
        btnNao.TabOrder := 0;

        btnOk.TabStop := False;
        btnSim.TabStop := True;
        btnNao.TabStop := True;
        btnCancelar.TabStop := True;
        btnSimOkRepetir.TabStop := False;
        btnNaoCancelar.TabStop := False;

        btnOk.Enabled := False;
        btnSim.Enabled := True;
        btnNao.Enabled := True;
        btnCancelar.Enabled := True;
        btnSimOkRepetir.Enabled := False;
        btnNaoCancelar.Enabled := False;
      end;
      BT_SIMNAODCANCEL:
      begin
        btnCancelar.Cancel := True;
        btnOk.Visible := False;
        btnSim.Visible := True;
        btnNao.Visible := True;
        btnCancelar.Visible := True;
        btnSimOkRepetir.Visible := False;
        btnNaoCancelar.Visible := False;
        btnCancelar.Default := True;
        btnCancelar.TabOrder := 0;

        btnOk.TabStop := False;
        btnSim.TabStop := True;
        btnNao.TabStop := True;
        btnCancelar.TabStop := True;
        btnSimOkRepetir.TabStop := False;
        btnNaoCancelar.TabStop := False;

        btnOk.Enabled := False;
        btnSim.Enabled := True;
        btnNao.Enabled := True;
        btnCancelar.Enabled := True;
        btnSimOkRepetir.Enabled := False;
        btnNaoCancelar.Enabled := False;
      end;
      BT_DSIMNAO:
      begin
        btnOk.Visible := False;
        btnSim.Visible := False;
        btnNao.Visible := False;
        btnCancelar.Visible := False;
        btnSimOkRepetir.Visible := True;
        btnSimOkRepetir.Caption := '&Sim';
        btnNaoCancelar.Visible := True;
        btnNaoCancelar.Caption := '&N�o';
        btnSimOkRepetir.Default := True;
        btnSimOkRepetir.TabOrder := 0;

        btnOk.TabStop := False;
        btnSim.TabStop := False;
        btnNao.TabStop := False;
        btnCancelar.TabStop := False;
        btnSimOkRepetir.TabStop := True;
        btnNaoCancelar.TabStop := True;

        btnOk.Enabled := False;
        btnSim.Enabled := False;
        btnNao.Enabled := False;
        btnCancelar.Enabled := False;
        btnSimOkRepetir.Enabled := True;
        btnNaoCancelar.Enabled := True;
      end;
      BT_SIMDNAO:
      begin
        btnOk.Visible := False;
        btnSim.Visible := False;
        btnNao.Visible := False;
        btnCancelar.Visible := False;
        btnSimOkRepetir.Visible := True;
        btnSimOkRepetir.Caption := '&Sim';
        btnNaoCancelar.Visible := True;
        btnNaoCancelar.Caption := '&N�o';
        btnNaoCancelar.Default := True;
        btnNaoCancelar.TabOrder := 0;

        btnOk.TabStop := False;
        btnSim.TabStop := False;
        btnNao.TabStop := False;
        btnCancelar.TabStop := False;
        btnSimOkRepetir.TabStop := True;
        btnNaoCancelar.TabStop := True;

        btnOk.Enabled := False;
        btnSim.Enabled := False;
        btnNao.Enabled := False;
        btnCancelar.Enabled := False;
        btnSimOkRepetir.Enabled := True;
        btnNaoCancelar.Enabled := True;
      end;
      BT_DRETRYCANCEL:
      begin
        btnCancelar.Cancel := True;
        btnOk.Visible := False;
        btnSim.Visible := False;
        btnNao.Visible := False;
        btnCancelar.Visible := False;
        btnSimOkRepetir.Visible := True;
        btnSimOkRepetir.Caption := 'Repetir';
        btnNaoCancelar.Visible := True;
        btnNaoCancelar.Caption := 'Cancelar';
        btnSimOkRepetir.Default := True;
        btnSimOkRepetir.TabOrder := 0;

        btnOk.TabStop := False;
        btnSim.TabStop := False;
        btnNao.TabStop := False;
        btnCancelar.TabStop := False;
        btnSimOkRepetir.TabStop := True;
        btnNaoCancelar.TabStop := True;

        btnOk.Enabled := False;
        btnSim.Enabled := False;
        btnNao.Enabled := False;
        btnCancelar.Enabled := False;
        btnSimOkRepetir.Enabled := True;
        btnNaoCancelar.Enabled := True;
      end;
    end;
  except
  end;
end;

{*----------------------------------------------------------------------
 * TdlgExibeMsg.IconesTratar  Exibe os �cones pertencentes � mensagem
 *          escolhida. Os c�digos que representam os �cones dispon�veis
 *          est�o no GLCONST.BAS (ICO_?????) e s�o utilizados de forma
 *          semelhante ao comando "MsgBox". O valor numerico dessas
 *          constantes deve ser utilizado para cadastrar o �cone na
 *          tabela zMensagens (coluna "Icone").
 *----------------------------------------------------------------------}
procedure TdlgExibeMsg.IconesTratar;
var
  iIndColIcones: Integer;
  iIcones:       Integer;
  vIcones:       Variant;
begin
  try
    iIndColIcones := IOPTR_NOPOINTER;
    vIcones := dmDados.ValorColuna(dmDados.giConMsg,FLDMENS_ICONE,iIndColIcones);
    if VarAsType(vIcones, VarString) = IOVAL_NOVALUE then
      iIcones := ICO_PARE
    else
      iIcones := vIcones;

    case iIcones of
//    case dmDados.gtbMens.FieldByName(FLDMENS_ICONE).AsInteger of
      ICO_PARE:
      begin
        imgStop.Visible := True;
        imgExclamacao.Visible := False;
        imgInformacao.Visible := False;
      end;
      ICO_EXCLAMACAO:
      begin
        imgStop.Visible := False;
        imgExclamacao.Visible := True;
        imgInformacao.Visible := False;
      end;
      ICO_INFORMACAO:
      begin
        imgStop.Visible := False;
        imgExclamacao.Visible := False;
        imgInformacao.Visible := True;
      end;
    end;
  except
  end;
end;

{*----------------------------------------------------------------------
 * TdlgExibeMsg.TamanhoRedim    Elimina as linhas em branco da mensagem e
 *                              redimensiona a caixa de di�logo de acordo
 *                              com o n�mero de linha com conte�do.
 *----------------------------------------------------------------------}
procedure TdlgExibeMsg.TamanhoRedim;
var
  iTamMax:    Integer;   { maior largura de texto encontrada na mensagem }
  iTamPixels: Integer;   { tam. a deslocar para a esquerda }
begin
  try
    iTamMax := 0;
    {Posiciona os labels na altura certa}
    lblMensagem1.Top := TOP_MENSAGEM1;
    lblMensagem2.Top := TOP_MENSAGEM2;
    lblMensagem3.Top := TOP_MENSAGEM3;
    lblMensagem4.Top := TOP_MENSAGEM4;

    { Para a Linha 1 }
    { suprime a linha inexistente }
    if Trim(lblMensagem1.Caption) = VAZIO then
    begin
      { linha de informa��es adicionais }
      lblMensagem4.Top := lblMensagem4.Top - ALTURA_LABEL;
      { altura da caixa de di�logo }
      Height := ALTURA_FORM - ALTURA_LABEL;
      { altura do painel }
      pnlFundo.Height := ALTURA_PAINEL - ALTURA_LABEL;
      { bot�es }
      btnOk.Top := TOP_BTNOK - ALTURA_LABEL;
      btnCancelar.Top := btnOk.Top;
      btnNao.Top := btnOk.Top;
      btnNaoCancelar.Top := btnOk.Top;
      btnSim.Top := btnOk.Top;
      btnSimOkRepetir.Top := btnOk.Top;
    end
    else
    begin
      { obt�m maior largura de texto }
      if Length(Trim(lblMensagem1.Caption)) > iTamMax then
        iTamMax := Length(Trim(lblMensagem1.Caption));
    end;
    { Para a Linha 2 }
    { suprime a linha inexistente }
    if Trim(lblMensagem2.Caption) = VAZIO then
    begin
      { linha de informa��es adicionais }
      lblMensagem4.Top := lblMensagem4.Top - ALTURA_LABEL;
      { altura da caixa de di�logo }
      Height := ALTURA_FORM - ALTURA_LABEL;
      { altura do painel }
      pnlFundo.Height := ALTURA_PAINEL - ALTURA_LABEL;
      { bot�es }
      btnOk.Top := TOP_BTNOK - ALTURA_LABEL;
      btnCancelar.Top := btnOk.Top;
      btnNao.Top := btnOk.Top;
      btnNaoCancelar.Top := btnOk.Top;
      btnSim.Top := btnOk.Top;
      btnSimOkRepetir.Top := btnOk.Top;
    end
    else
    begin
      { obt�m maior largura de texto }
      if Length(Trim(lblMensagem2.Caption)) > iTamMax then
        iTamMax := Length(Trim(lblMensagem2.Caption));
    end;
    { Para a Linha 3 }
    { suprime a linha inexistente }
    if Trim(lblMensagem3.Caption) = VAZIO then
    begin
      { linha de informa��es adicionais }
      lblMensagem4.Top := lblMensagem4.Top - ALTURA_LABEL;
      { altura da caixa de di�logo }
      Height := ALTURA_FORM - ALTURA_LABEL;
      { altura do painel }
      pnlFundo.Height := ALTURA_PAINEL - ALTURA_LABEL;
      { bot�es }
      btnOk.Top := TOP_BTNOK - ALTURA_LABEL;
      btnCancelar.Top := btnOk.Top;
      btnNao.Top := btnOk.Top;
      btnNaoCancelar.Top := btnOk.Top;
      btnSim.Top := btnOk.Top;
      btnSimOkRepetir.Top := btnOk.Top;
    end
    else
    begin
      { obt�m maior largura de texto }
      if Length(Trim(lblMensagem3.Caption)) > iTamMax then
        iTamMax := Length(Trim(lblMensagem3.Caption));
    end;
    { Para a Linha 4 }
    { suprime a linha inexistente }
    if Trim(lblMensagem4.Caption) = VAZIO then
    begin
      { linha de informa��es adicionais }
      lblMensagem4.Top := lblMensagem4.Top - ALTURA_LABEL;
      { altura da caixa de di�logo }
      Height := ALTURA_FORM - ALTURA_LABEL;
      { altura do painel }
      pnlFundo.Height := ALTURA_PAINEL - ALTURA_LABEL;
      { bot�es }
      btnOk.Top := TOP_BTNOK - ALTURA_LABEL;
      btnCancelar.Top := btnOk.Top;
      btnNao.Top := btnOk.Top;
      btnNaoCancelar.Top := btnOk.Top;
      btnSim.Top := btnOk.Top;
      btnSimOkRepetir.Top := btnOk.Top;
    end
    else
    begin
      { obt�m maior largura de texto }
      if Length(Trim(lblMensagem4.Caption)) > iTamMax then
        iTamMax := Length(Trim(lblMensagem4.Caption));
    end;

    if iTamMax > 60 then
      iTamPixels := 0
    else
      iTamPixels := Trunc(((60 - iTamMax) * 7) / 2);

(*    lblMensagem1.Left := lblMensagem1.Left - iTamPixels;
    lblMensagem2.Left := lblMensagem2.Left - iTamPixels;
    lblMensagem3.Left := lblMensagem3.Left - iTamPixels;
    lblMensagem4.Left := lblMensagem4.Left - iTamPixels;
*)
(*   lblMensagem1.Width := lblMensagem1.Width - iTamPixels;
    lblMensagem2.Width := lblMensagem2.Width - iTamPixels;
    lblMensagem3.Width := lblMensagem3.Width - iTamPixels;
    lblMensagem4.Width := lblMensagem4.Width - iTamPixels;
 *)
    btnOk.Left            := LEFT_BTNOK - iTamPixels;
    btnCancelar.Left      := LEFT_BTNCANC - iTamPixels;
    btnNao.Left           := LEFT_BTNNAO - iTamPixels;
    btnNaoCancelar.Left   := LEFT_BTNNAOCANC - iTamPixels;
    btnSim.Left           := LEFT_BTNSIM - iTamPixels;
    btnSimOkRepetir.Left  := LEFT_BTNSIMOK - iTamPixels;
    { ajusta largura da caixa de msg }
    Width := LARGURA_FORM - (iTamPixels * 2);
    { ajusta largura dao painel de fundo }
    pnlFundo.Width := LARGURA_PAINEL - (iTamPixels * 2);
  except
  end;
end;

{*----------------------------------------------------------------------
 * TdlgExibeMsg.FormClose    Fecha a janela de dialogo
 *
 *----------------------------------------------------------------------}
procedure TdlgExibeMsg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  lblMensagem1.Caption := VAZIO;
  lblMensagem2.Caption := VAZIO;
  lblMensagem3.Caption := VAZIO;
  lblMensagem4.Caption := VAZIO;
  Screen.Cursor := tcMousePointer;
end;

procedure TdlgExibeMsg.btnCancelarClick(Sender: TObject);
begin
  dmDados.giMsgResposta := IDCANCEL;
  Close;
end;

procedure TdlgExibeMsg.btnNaoClick(Sender: TObject);
begin
  dmDados.giMsgResposta := IDNO;
  Close;
end;

procedure TdlgExibeMsg.btnNaoCancelarClick(Sender: TObject);
begin
  if btnNaoCancelar.Caption = 'Cancelar' then
    dmDados.giMsgResposta := IDCANCEL
  else
    dmDados.giMsgResposta := IDNO;

  Close;
end;

procedure TdlgExibeMsg.btnOkClick(Sender: TObject);
begin
  dmDados.giMsgResposta := IDOK;
  Close;
end;

procedure TdlgExibeMsg.btnSimClick(Sender: TObject);
begin
  dmDados.giMsgResposta := IDYES;
  Close;
end;

procedure TdlgExibeMsg.btnSimOkRepetirClick(Sender: TObject);
begin
  if btnSimOkRepetir.Caption = 'OK' then
    dmDados.giMsgResposta := IDOK
  else
    if btnSimOkRepetir.Caption = 'Repetir' then
      dmDados.giMsgResposta := IDRETRY
    else
      dmDados.giMsgResposta := IDYES;
  Close;
end;

end.
 