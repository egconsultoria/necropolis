unit uniNaveg;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls, DB, DBTables, Variants,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}

type
 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntNaveg = class(TObject)
  private
    { Private declarations }
    procedure mp_Nav_Chave_Ler(var prChave: TumaChave;
                               prNav: TumNavegador);
    procedure mp_Nav_Nav_Checar(var prNav: TumNavegador);
    procedure mp_Nav_Chave_Checar(var prNav: TumNavegador;
                                  prChave: TumaChave);
    function  mf_Nav_Criterio(prNav: TumNavegador;
                              prChave: TumaChave): String;
    procedure mp_Nav_Flags_Reposicionar(var prNav: TumNavegador);
    function  mf_Nav_Chave_Comparar(prChave1: TumaChave;
                                    prChave2: TumaChave;
                                    prNav: TumNavegador): Integer;

  public
    { Public declarations }
    procedure gp_Nav_Inicializar;
    procedure gp_Nav_Finalizar;
    function  gf_Nav_Iniciar(var prNav: TumNavegador;
                             psSQL: String;
                             var prChave: TumaChave;
                             piOrdem: Integer): Boolean;
    function  gf_Nav_Procurar(var prNav: TumNavegador;
                              var prChave: TumaChave): Boolean;
    procedure gp_Nav_OnError(var prNav: TumNavegador;
                             piErro: Integer);
    function gf_Nav_Inserir(var prNav: TumNavegador;
                            var prChave: TumaChave): Boolean;
    function gf_Nav_Apagar(var prNav: TumNavegador;
                           var prChave: TumaChave): Boolean;
    function gf_Nav_Ir_Anterior(var prNav: TumNavegador;
                                var prChave: TumaChave): Boolean;
    function gf_Nav_Ir_Primeiro(var prNav: TumNavegador;
                                var prChave: TumaChave): Boolean;
    function gf_Nav_Ir_Proximo(var prNav: TumNavegador;
                               var prChave: TumaChave): Boolean;
    function gf_Nav_Ir_Ultimo(var prNav: TumNavegador;
                              var prChave: TumaChave): Boolean;
    function gf_Nav_Regerar(var prNav: TumNavegador;
                            var prChave: TumaChave): Boolean;
    procedure gp_Nav_Flags_Travar(var prNav: TumNavegador;
                                  pbBloquear: Boolean);
    function gf_Nav_Pegar_Anterior(var prTelaVar: TumaTelaVar): Boolean;
    function gf_Nav_Pegar_Proximo(var prTelaVar: TumaTelaVar): Boolean;

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untNaveg: TuntNaveg;

implementation

uses uniDados, uniParam;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Inicializar
'     criar o objeto
'-------------------------------------------------------------------------------------------------}
procedure TuntNaveg.gp_Nav_Inicializar();
begin

  // Cria o objeto
  untNaveg := TuntNaveg.Create;

end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Finalizar
'     Finalizar a navegacao
'-------------------------------------------------------------------------------------------------}
procedure TuntNaveg.gp_Nav_Finalizar();
begin
  // Destroi objeto
  untNaveg.Free;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Iniciar
'     Inicializar a navegacao e criar o objeto
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Iniciar(var prNav: TumNavegador;
                                  psSQL: String;
                                  var prChave: TumaChave;
                                  piOrdem: Integer): Boolean;
var
  i:         Integer;
  laChaves:  array[0..NAV_MAXCOLS] of String;
  leErro:    Exception;
  leDBErro:  EDBEngineError;
  lsSql:     String;
begin

  try
    Result := False;

    prNav.iNumCols := 0;
    prNav.iConexao := IOPTR_NOPOINTER;
    prNav.bNoRows := True;
    prNav.bBOF := True;
    prNav.bEOF := True;
    prNav.bRegistroValido := False;
    prNav.bNavegavel := False;
    prNav.bPrimeiro := False;
    prNav.bAnterior := False;
    prNav.bProximo := False;
    prNav.bUltimo := False;

    if (prChave.iNumCols < 1) or (prChave.iNumCols > NAV_MAXCOLS) then
      {' Sai com False}
      Exit;

//#######################
//    if (piConexao <> 0) and (piConexao <> IOPTR_NOPOINTER) then
//      prNav.iConexao := piConexao
//    else
//#######################
//      prNav.iConexao := dmDados.ExecutarSelect(psSQL);
//    if prNav.iConexao = IOPTR_NOPOINTER then
      {' Sai com False}
//      Exit;

    {' AllowPrevious : Talvez tenha que ser repensado pois a chave
    ' n�o deveria ser completa para o restante mas aqui deve ser (ou n�o, n�o existem updates aqui)}
    for i := 1 To prChave.iNumCols do
      laChaves[i - 1] := prChave.sNome[i];

    prNav.iNumCols := prChave.iNumCols;

    for i := 1 To prNav.iNumCols do
      prNav.iIOPTR[i] := IOPTR_NOPOINTER;

    { ordem de codigo}
    if piOrdem = 1 then
//      lsSql := 'SELECT MIN(' + prChave.sNome[piOrdem] + ') As ' + prChave.sNome[piOrdem] + ' FROM ' + dmDados.SQLTabelaNome(psSQL)
      lsSql := 'SELECT ' + prChave.sNome[piOrdem] + ',' + prChave.sNome[piOrdem+1] + ' FROM ' + dmDados.SQLTabelaNome(psSQL) +
               ' WHERE ' + prChave.sNome[piOrdem] + '= (SELECT MIN(' + prChave.sNome[piOrdem] + ') ' + ' FROM ' + dmDados.SQLTabelaNome(psSQL) + ')'
    { ordem de descricao+codigo }
    else
      lsSql := 'SELECT ' + prChave.sNome[piOrdem] + ',' + prChave.sNome[piOrdem-1] + ' FROM ' + dmDados.SQLTabelaNome(psSQL) +
               ' WHERE ' + prChave.sNome[piOrdem] + '= (SELECT MIN(' + prChave.sNome[piOrdem] + ') ' + ' FROM ' + dmDados.SQLTabelaNome(psSQL) + ')';

    prNav.iConexao := dmDados.ExecutarSelect(lsSQL);

    if dmDados.Status(prNav.iConexao, IOSTATUS_NOROWS) then
    begin
      {' Apenas para os nomes}
      prNav.rReg_Corrente := prChave;
      prNav.rReg_Primeiro := prChave;
      prNav.rReg_Ultimo := prChave;
      dmDados.FecharConexao(prNav.iConexao);
    end
    else
    begin
      prNav.bNoRows := False;
      prNav.bBOF := False;
      prNav.bEOF := False;

      prNav.rReg_Corrente := prChave;

//#######################
//      dmDados.gsRetorno := dmDados.Primeiro(prNav.iConexao);
//#######################
      mp_Nav_Chave_Ler(prNav.rReg_Corrente, prNav);
      prNav.bRegistroValido := True;
      prNav.bNavegavel := True;

      prNav.rReg_Primeiro := prNav.rReg_Corrente;
      {' Apenas para os nomes}
      prNav.rReg_Ultimo := prNav.rReg_Corrente;

      dmDados.FecharConexao(prNav.iConexao);
//      dmDados.gsRetorno := dmDados.Ultimo(prNav.iConexao);
      { ordem de codigo }
      if piOrdem = 1 then
//        lsSql := 'SELECT MAX(' + prChave.sNome[piOrdem] + ') As ' + prChave.sNome[piOrdem] + ' FROM ' + dmDados.SQLTabelaNome(psSQL)
        lsSql := 'SELECT ' + prChave.sNome[piOrdem] + ',' + prChave.sNome[piOrdem+1] + ' FROM ' + dmDados.SQLTabelaNome(psSQL) +
                 ' WHERE ' + prChave.sNome[piOrdem] + '= (SELECT MAX(' + prChave.sNome[piOrdem] + ') ' + ' FROM ' + dmDados.SQLTabelaNome(psSQL) + ')'
      { ordem de descricao+codigo }
      else
        lsSql := 'SELECT ' + prChave.sNome[piOrdem] + ',' + prChave.sNome[piOrdem-1] + ' FROM ' + dmDados.SQLTabelaNome(psSQL) +
                 ' WHERE ' + prChave.sNome[piOrdem] + '= (SELECT MAX(' + prChave.sNome[piOrdem] + ') ' + ' FROM ' + dmDados.SQLTabelaNome(psSQL) + ')';

      prNav.iConexao := dmDados.ExecutarSelect(lsSQL);

      mp_Nav_Chave_Ler(prNav.rReg_Ultimo, prNav);
      dmDados.FecharConexao(prNav.iConexao);
      if VarIsNull(prChave.vValor[1]) then
//      if prChave.vValor[1] = '0' then
        dmDados.gsRetorno := dmDados.Primeiro(prNav.iConexao)
      else
      begin
        Result := gf_Nav_Procurar(prNav, prChave);
        Exit;
      end;
    end;
    mp_Nav_Flags_Reposicionar(prNav);

    Result := True;

  except
    leErro := Exception(ExceptObject);
    if leErro <> nil then
      prNav.sUltimoErroStr := leErro.Message;
    if leErro is EDBEngineError then
    begin
      leDBErro := EDBEngineError(leErro);
      prNav.iUltimoErro := leDBErro.Errors[0].ErrorCode;
    end;

//    if Err = ERRO_NAV_EXECSELECT then
//      prNav.sUltimoErroStr := 'Lista de navega��o vazia';
//    else
//      prNav.sUltimoErroStr := eErro.Message;

    dmDados.FecharConexao(prNav.iConexao);
    prNav.iNumCols := 0;

    Result := False;
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.lp_Nav_Chave_Ler
'     Le da tabela as colunas
'-------------------------------------------------------------------------------------------------}
procedure TuntNaveg.mp_Nav_Chave_Ler(var prChave: TumaChave;
                                     prNav: TumNavegador);
var
  i: Integer;
begin
    for i := 1 To prNav.iNumCols do
      prChave.vValor[i] := dmDados.ValorColuna(prNav.iConexao, prChave.sNome[i], prNav.iIOPTR[i]);

end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Procurar
'     Le da tabela as colunas
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Procurar(var prNav: TumNavegador;
                                   var prChave: TumaChave): Boolean;
var
  lsCriterio:   String;
  i:            Integer;
  laChaves:     array[0..NAV_MAXCOLS] of String;
begin

  try
    mp_Nav_Nav_Checar(prNav);
    mp_Nav_Chave_Checar(prNav, prChave);

    if not prNav.bNoRows then
    begin
      for i := 1 To prNav.iNumCols do
        laChaves[i - 1] := prChave.sNome[i];

//      lsCriterio := mf_Nav_Criterio(prNav, prChave);

//      dmDados.gsRetorno := dmDados.AtivaPrimeiraComp(prNav.iConexao, lsCriterio, laChaves, IOFLAG_GET);

//      if not dmDados.Status(prNav.iConexao, IOSTATUS_NOMATCH) then
//      begin
//        mp_Nav_Chave_Ler(prNav.rReg_Corrente, prNav);
        prNav.rReg_Corrente := prChave;
        mp_Nav_Flags_Reposicionar(prNav);
        Result := True;
        prChave := prNav.rReg_Corrente;
//      end
//      else
//      begin
//        lsCriterio := mf_Nav_Criterio(prNav, prNav.rReg_Corrente);
//        dmDados.gsRetorno := dmDados.AtivaPrimeiraComp(prNav.iConexao, lsCriterio, laChaves, IOFLAG_GET);

//        if dmDados.Status(prNav.iConexao, IOSTATUS_NOMATCH) then
//          gp_Nav_OnError(prNav, ERRO_NAV_POSICAOINVALIDA);

//        Result := False;
//      end;
    end
    else
      Result := False;

  except
    gp_Nav_OnError(prNav, 0);
    Result := False;
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.lp_Nav_Nav_Checar
'
'-------------------------------------------------------------------------------------------------}
procedure TuntNaveg.mp_Nav_Nav_Checar(var prNav: TumNavegador);
begin
  prNav.iUltimoErro := 0;
  prNav.sUltimoErroStr := VAZIO;
  if prNav.iNumCols <= 0 then
    gp_Nav_OnError(prNav, ERRO_NAV_NAOINICIALIZADA);

end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.lp_Nav_Chave_Checar
'
'-------------------------------------------------------------------------------------------------}
procedure TuntNaveg.mp_Nav_Chave_Checar(var prNav: TumNavegador;
                                        prChave: TumaChave);
var
  i: Integer;
begin

  prNav.iUltimoErro := 0;
  prNav.sUltimoErroStr := VAZIO;
  if prNav.iNumCols <> prChave.iNumCols then
    gp_Nav_OnError(prNav, ERRO_NAV_CHAVEINVALIDA);

  for i := 1 To prNav.iNumCols do
    if (prNav.rReg_Corrente.sNome[i] <> prChave.sNome[i]) or VarIsNull(prChave.vValor[i]) then
      gp_Nav_OnError(prNav, ERRO_NAV_CHAVEINVALIDA);

end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.lf_Nav_Criterio
'
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.mf_Nav_Criterio(prNav: TumNavegador;
                                   prChave: TumaChave): String;
var
  i          : Integer;
  lsCriterio : String;
  lrCol      : array[0..NAV_MAXCOLS] of TColunaInfo;
begin

    for i := 1 To prNav.iNumCols do
      lrCol[i - 1].sNome := prChave.sNome[i];

    dmDados.gsConexaoRetorno := dmDados.ColunasInfo(prNav.iConexao, lrCol, IOCOL_TYPE);
    lsCriterio := VAZIO;
    for i := 1 To prNav.iNumCols do
    begin
        lsCriterio := lsCriterio + ' AND (' + prChave.sNome[i] + ' = ';
        case lrCol[i - 1].ftTipo of
          IOTYPE_TEXT, IOTYPE_BLOB, IOTYPE_MEMO:
            lsCriterio := lsCriterio + '''' + Trim(prChave.vValor[i]) + '''' + ')';
          IOTYPE_INTEGER, IOTYPE_SMALLINT, IOTYPE_AUTOINC:
            lsCriterio := lsCriterio + IntToStr(prChave.vValor[i]) + ')';
          IOTYPE_DATE, IOTYPE_DATETIME:
            lsCriterio := lsCriterio + CTEDB_DATE_INI + FormatDateTime(MK_DATA, prChave.vValor[i]) + CTEDB_DATE_FIM + ')';
          IOTYPE_FLOAT:
            lsCriterio := lsCriterio + dmDados.TrataDecimalBD(prChave.vValor[i], IOFLAG_VALORPARABANCO) + ')';
          IOTYPE_CURRENCY:
            lsCriterio := lsCriterio + CteDB_MONEY_INI + dmDados.TrataDecimalBD(prChave.vValor[i], IOFLAG_VALORPARABANCO) + CteDB_MONEY_FIM + ')';
        else
            lsCriterio := lsCriterio + '''' + prChave.vValor[i] + '''' + ')';
        end;
    end;
    {' Pula o primeiro AND}
    Result := Copy(lsCriterio, 5, Length(lsCriterio))
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.lp_Nav_Flags_Reposicionar
'
'-------------------------------------------------------------------------------------------------}
procedure TuntNaveg.mp_Nav_Flags_Reposicionar(var prNav: TumNavegador);
begin

  prNav.bPrimeiro := False;
  prNav.bAnterior := False;
  prNav.bProximo := False;
  prNav.bUltimo := False;
  if prNav.bNavegavel then
  begin
//    if not dmDados.Status(prNav.iConexao, IOSTATUS_NOROWS) then
//    begin
      if mf_Nav_Chave_Comparar(prNav.rReg_Corrente, prNav.rReg_Primeiro, prNav) <> NAV_IGUAL then
      begin
        prNav.bPrimeiro := True;
        prNav.bAnterior := True;
      end;
      if mf_Nav_Chave_Comparar(prNav.rReg_Corrente, prNav.rReg_Ultimo, prNav) <> NAV_IGUAL then
      begin
        prNav.bProximo := True;
        prNav.bUltimo := True;
      end;
//    end;
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.mf_Nav_Chave_Comparar
'
'-------------------------------------------------------------------------------------------------}
Function TuntNaveg.mf_Nav_Chave_Comparar(prChave1: TumaChave;
                                         prChave2: TumaChave;
                                         prNav: TumNavegador): Integer;
var
  i: Integer;
begin

  if (prChave1.iNumCols < 1) or (prChave1.iNumCols > NAV_MAXCOLS) then
    gp_Nav_OnError(prNav, ERRO_NAV_CHAVEINVALIDA);

  if (prChave2.iNumCols < 1) or (prChave2.iNumCols > NAV_MAXCOLS) then
    gp_Nav_OnError(prNav, ERRO_NAV_CHAVEINVALIDA);

  if prChave1.iNumCols <> prChave2.iNumCols then
    gp_Nav_OnError(prNav, ERRO_NAV_CHAVEINVALIDA);

  for i := 1 To prChave1.iNumCols do
  begin
    if prChave1.vValor[i] > prChave2.vValor[i] then
    begin
      Result := NAV_MAIOR;
      Exit;
    end
    else
    begin
      if prChave1.vValor[i] < prChave2.vValor[i] then
      begin
        Result := NAV_MENOR;
        Exit;
      end;
    end;
  end;
  Result := NAV_IGUAL;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gp_Nav_OnError
'
'-------------------------------------------------------------------------------------------------}
procedure TuntNaveg.gp_Nav_OnError(var prNav: TumNavegador;
                                   piErro: Integer);
var
  leErro:    Exception;
  leDBErro:  EDBEngineError;
begin
  leErro := Exception(ExceptObject);
  if leErro <> nil then
    prNav.sUltimoErroStr := leErro.Message;
  if leErro is EDBEngineError then
  begin
    leDBErro := EDBEngineError(leErro);
    prNav.iUltimoErro := leDBErro.Errors[0].ErrorCode;
  end;

  if piErro <> 0 then
    prNav.iUltimoErro := piErro;

  case piErro of
    ERRO_NAV_EXECSELECT:
      prNav.sUltimoErroStr := ERROMSG_NAV_EXECSELECT;
    ERRO_NAV_CHAVEINVALIDA:
      prNav.sUltimoErroStr := ERROMSG_NAV_CHAVEINVALIDA;
    ERRO_NAV_NAOINICIALIZADA:
      prNav.sUltimoErroStr := ERROMSG_NAV_NAOINICIALIZADA;
    ERRO_NAV_POSICAOINVALIDA:
      prNav.sUltimoErroStr := ERROMSG_NAV_POSICAOINVALIDA;
  else
      prNav.sUltimoErroStr := ERROMSG_NAV_INDETERMINADO;
  end;
  ShowMessage(prNav.sUltimoErroStr);
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Inserir
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Inserir(var prNav: TumNavegador;
                                  var prChave: TumaChave): Boolean;
var
  lrChave: TumaChave;
begin
  Result := False;
  try
    if gf_Nav_Regerar(prNav, lrChave) then
    begin
      prChave.vValor[1] := prNav.rReg_Ultimo.vValor[1];
      Result := gf_Nav_Procurar(prNav, prChave);
    end;
  except
    gp_Nav_OnError(prNav, 0);
    Result := False;
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Apagar
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Apagar(var prNav: TumNavegador;
                                 var prChave: TumaChave): Boolean;
begin
  try
    prNav.bRegistroValido := False;
    Result := gf_Nav_Regerar(prNav, prChave); // Por ora
  except
    gp_Nav_OnError(prNav, 0);
    Result := False;
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Ir_Anterior
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Ir_Anterior(var prNav: TumNavegador;
                                      var prChave: TumaChave): Boolean;
begin
  try
    mp_Nav_Nav_Checar(prNav);
    if (not prNav.bNoRows) and (prNav.bAnterior) then
    begin
      dmDados.gsRetorno := dmDados.Anterior(prNav.iConexao, 1);
      mp_Nav_Chave_Ler(prNav.rReg_Corrente, prNav);
      mp_Nav_Flags_Reposicionar(prNav);
      Result := True;
    end
    else
      Result := False;

    prChave := prNav.rReg_Corrente;

  except
    gp_Nav_OnError(prNav, 0);
    Result := False;
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Ir_Primeiro
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Ir_Primeiro(var prNav: TumNavegador;
                                      var prChave: TumaChave): Boolean;
begin
  try
    mp_Nav_Nav_Checar(prNav);
    if (not prNav.bNoRows) and (prNav.bPrimeiro) then
    begin
//      dmDados.gsRetorno := dmDados.Primeiro(prNav.iConexao);
//      mp_Nav_Chave_Ler(prNav.rReg_Corrente, prNav);
      prNav.rReg_Corrente := prNav.rReg_Primeiro;
      mp_Nav_Flags_Reposicionar(prNav);
      Result := True;
    end
    else
      Result := False;

    prChave := prNav.rReg_Corrente;

  except
    gp_Nav_OnError(prNav, 0);
    Result := False;
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Ir_Proximo
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Ir_Proximo(var prNav: TumNavegador;
                                     var prChave: TumaChave): Boolean;
begin
  try
    mp_Nav_Nav_Checar(prNav);
    if (not prNav.bNoRows) and (prNav.bProximo) then
    begin
      dmDados.gsRetorno := dmDados.Proximo(prNav.iConexao);
      mp_Nav_Chave_Ler(prNav.rReg_Corrente, prNav);
      mp_Nav_Flags_Reposicionar(prNav);
      Result := True;
    end
    else
      Result := False;

    prChave := prNav.rReg_Corrente;

  except
    gp_Nav_OnError(prNav, 0);
    Result := False;
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Ir_Ultimo
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Ir_Ultimo(var prNav: TumNavegador;
                                    var prChave: TumaChave): Boolean;
begin
  try
    mp_Nav_Nav_Checar(prNav);
    if (not prNav.bNoRows) and (prNav.bUltimo) then
    begin
//      dmDados.gsRetorno := dmDados.Ultimo(prNav.iConexao);
//      mp_Nav_Chave_Ler(prNav.rReg_Corrente, prNav);
      prNav.rReg_Corrente := prNav.rReg_Ultimo;
      mp_Nav_Flags_Reposicionar(prNav);
      Result := True;
    end
    else
      Result := False;

    prChave := prNav.rReg_Corrente;

  except
    gp_Nav_OnError(prNav, 0);
    Result := False;
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Regerar
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Regerar(var prNav: TumNavegador;
                                  var prChave: TumaChave): Boolean;
var
  lbRegistroEraValido: Boolean;
begin
  try
    mp_Nav_Nav_Checar(prNav);

    lbRegistroEraValido := prNav.bRegistroValido;
    prChave := prNav.rReg_Corrente;

//    dmDados.gsRetorno := dmDados.Atualizar(prNav.iConexao);

    prNav.bNoRows := True;
    prNav.bBOF := True;
    prNav.bEOF := True;
    prNav.bRegistroValido := False;
    prNav.bNavegavel := False;
    prNav.bPrimeiro := False;
    prNav.bAnterior := False;
    prNav.bProximo := False;
    prNav.bUltimo := False;

    if not dmDados.Status(prNav.iConexao, IOSTATUS_NOROWS) then
    begin
      prNav.bNoRows := False;
      prNav.bBOF := False;
      prNav.bEOF := False;

      prNav.bRegistroValido := True;
      prNav.bNavegavel := True;

      mp_Nav_Chave_Ler(prNav.rReg_Primeiro, prNav);
      dmDados.gsRetorno := dmDados.Ultimo(prNav.iConexao);
      mp_Nav_Chave_Ler(prNav.rReg_Ultimo, prNav);

      if lbRegistroEraValido then
      begin
        Result := gf_Nav_Procurar(prNav, prChave);
        Exit;
      end
      else
      begin
        dmDados.gsRetorno := dmDados.Primeiro(prNav.iConexao);
        prNav.rReg_Corrente := prNav.rReg_Primeiro;
        prChave := prNav.rReg_Corrente;
      end;
    end;
    Result := True;

  except
    gp_Nav_OnError(prNav, 0);
    Result := False;
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gp_Nav_Flags_Travar
'-------------------------------------------------------------------------------------------------}
procedure TuntNaveg.gp_Nav_Flags_Travar(var prNav: TumNavegador;
                                        pbBloquear: Boolean);
begin

  prNav.bNavegavel := (not pbBloquear);
  mp_Nav_Flags_Reposicionar(prNav);

end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Pegar_Anterior
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Pegar_Anterior(var prTelaVar: TumaTelaVar): Boolean;
var
  lsNome:  string;
  liConta: Integer;
  lsSql:   string;
  lsCodigo: string;
  laChaves: array[0..5] of String;
begin
  try
    mp_Nav_Nav_Checar(prTelaVar.Nav);
    if (not prTelaVar.Nav.bNoRows) and (prTelaVar.Nav.bAnterior) then
    begin
      { pegar o nome da stored procedure }
//      for liConta := 1 to High(gaLista_Parametros) do
//        if gaLista_Parametros[liConta].Indice_Tela = prTelaVar.P.Indice_Tela then
//        begin
//          lsNome := gaLista_Parametros[liConta].Coluna_Nivel;
//          break;
//        end;

      { quando for a primeira vez }
      if VarIsEmpty(prTelaVar.Nav.rReg_Corrente.vValor[1]) then
        prTelaVar.Nav.rReg_Corrente.vValor[1] := 0;

      { retorna o codigo encontrado }
//      lsCodigo := dmDados.CriarStored(lsNome, prTelaVar.P.Ordem, VarToStr(prTelaVar.Nav.rReg_Corrente.vValor[1]), 2);

      laChaves[0] := prTelaVar.P.Coluna_Codigo;
      laChaves[1] := prTelaVar.P.Coluna_Descricao;
      lsSql := untParam.gf_Param_Sql(prTelaVar.P.Sigla_Tabela, lsCodigo, laChaves);
      lsSQL := Copy(lsSQL, 1, 6) + CTEDB_TOP_1 + Copy(lsSQL, 8, Length(lsSQL));
//      if prTelaVar.P.Coluna_Nivel = 'COLUNA_DESCRICAO' then
      if prTelaVar.P.Tipo_Objeto = 'D' then
      begin
        if Pos('WHERE',lsSQL) > 0 then
          lsSQL := lsSQL + ' AND ' + prTelaVar.P.Coluna_Descricao + ' < ''' + prTelaVar.Nav.rReg_Corrente.vValor[2] + ''''
        else
          lsSQL := lsSQL + ' WHERE ' + prTelaVar.P.Coluna_Descricao + ' < ''' + prTelaVar.Nav.rReg_Corrente.vValor[2] + '''';
        lsSQL := lsSQL + ' ORDER BY Base.' + prTelaVar.P.Coluna_Descricao + ' DESC';
      end
      else
      begin
        if Pos('WHERE',lsSQL) > 0 then
          lsSQL := lsSQL + ' AND ' + prTelaVar.P.Coluna_Codigo + ' < ''' + prTelaVar.Nav.rReg_Corrente.vValor[1] + ''''
        else
          lsSQL := lsSQL + ' WHERE ' + prTelaVar.P.Coluna_Codigo + ' < ''' + prTelaVar.Nav.rReg_Corrente.vValor[1] + '''';
        lsSQL := lsSQL + ' ORDER BY Base.' + prTelaVar.P.Coluna_Codigo + ' DESC';
      end;

      prTelaVar.Nav.iConexao := dmDados.ExecutarSelect(lsSql);
      if not dmDados.Status(prTelaVar.Nav.iConexao, IOSTATUS_NOROWS) then
      begin
        mp_Nav_Chave_Ler(prTelaVar.Nav.rReg_Corrente, prTelaVar.Nav);
        mp_Nav_Flags_Reposicionar(prTelaVar.Nav);
      end;
      dmDados.FecharConexao(prTelaVar.Nav.iConexao);
      Result := True;
    end
    else
      Result := False;

  except
    dmDados.ErroTratar('gf_Nav_Pegar_Anterior - uniNaveg.PAS');
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntNaveg.gf_Nav_Pegar_Proximo
'-------------------------------------------------------------------------------------------------}
function TuntNaveg.gf_Nav_Pegar_Proximo(var prTelaVar: TumaTelaVar): Boolean;
var
  lsNome:  string;
  liConta: Integer;
  lsSql:   string;
  lsCampo: string;
  laChaves: array[0..5] of String;
begin
  try
    mp_Nav_Nav_Checar(prTelaVar.Nav);
    if (not prTelaVar.Nav.bNoRows) and (prTelaVar.Nav.bProximo) then
    begin
      { pegar o nome da stored procedure }
//      for liConta := 1 to High(gaLista_Parametros) do
//        if gaLista_Parametros[liConta].Indice_Tela = prTelaVar.P.Indice_Tela then
//        begin
//          lsNome := gaLista_Parametros[liConta].Coluna_Nivel;
//          break;
//        end;

      { quando for a primeira vez }
      if VarIsEmpty(prTelaVar.Nav.rReg_Corrente.vValor[1]) then
        prTelaVar.Nav.rReg_Corrente.vValor[1] := 0;

      { retorna o codigo encontrado }
//      lsCampo := dmDados.CriarStored(lsNome, prTelaVar.P.Ordem, VarToStr(prTelaVar.Nav.rReg_Corrente.vValor[1]), 1);

      laChaves[0] := prTelaVar.P.Coluna_Codigo;
      laChaves[1] := prTelaVar.P.Coluna_Descricao;
      lsSql := untParam.gf_Param_Sql(prTelaVar.P.Sigla_Tabela, VAZIO (*lsCampo*), laChaves);
      lsSQL := Copy(lsSQL, 1, 6) + CTEDB_TOP_1 + Copy(lsSQL, 8, Length(lsSQL));
//      if prTelaVar.P.Coluna_Nivel = 'COLUNA_DESCRICAO' then
      if prTelaVar.P.Tipo_Objeto = 'D' then
      begin
        if Pos('WHERE',lsSQL) > 0 then
          lsSQL := lsSQL + ' AND ' + prTelaVar.P.Coluna_Descricao + ' > ''' + prTelaVar.Nav.rReg_Corrente.vValor[2] + ''''
        else
          lsSQL := lsSQL + ' WHERE ' + prTelaVar.P.Coluna_Descricao + ' > ''' + prTelaVar.Nav.rReg_Corrente.vValor[2] + '''';
        lsSQL := lsSQL + ' ORDER BY Base.' + prTelaVar.P.Coluna_Descricao;
      end
      else
      begin
        if Pos('WHERE',lsSQL) > 0 then
          lsSQL := lsSQL + ' AND ' + prTelaVar.P.Coluna_Codigo + ' > ''' + prTelaVar.Nav.rReg_Corrente.vValor[1] + ''''
        else
          lsSQL := lsSQL + ' WHERE ' + prTelaVar.P.Coluna_Codigo + ' > ''' + prTelaVar.Nav.rReg_Corrente.vValor[1] + '''';
        lsSQL := lsSQL + ' ORDER BY Base.' + prTelaVar.P.Coluna_Codigo;
      end;

      prTelaVar.Nav.iConexao := dmDados.ExecutarSelect(lsSql);
      if not dmDados.Status(prTelaVar.Nav.iConexao, IOSTATUS_NOROWS) then
      begin
        mp_Nav_Chave_Ler(prTelaVar.Nav.rReg_Corrente, prTelaVar.Nav);
        mp_Nav_Flags_Reposicionar(prTelaVar.Nav);
      end;
      dmDados.FecharConexao(prTelaVar.Nav.iConexao);
      Result := True;
    end
    else
      Result := False;

  except
    dmDados.ErroTratar('gp_Nav_Pegar_Proximo - uniNaveg.PAS');
  end;
end;

end.
