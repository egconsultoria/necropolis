unit uniTelaVar;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, DB, DBTables, ExtCtrls,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}

type
 {*======================================================================
 *            RECORDS
 *======================================================================}

 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntTelaVar = class(TObject)
  private

    { Private declarations }
  public
    function gf_TelaVar_ExecSelect(psID: String;
                                   paParm: array of Variant): Integer;

    procedure gp_TelaVar_Passar(prParametro: TumParametro);
    procedure gp_TelaVar_Inicializar;
    procedure gp_TelaVar_Finalizar;
    procedure gp_TelaVar_Seta_Entidade(psEntidade: String);
    function gf_TelaVar_Retorna_Tabela(psSiglaTabela: String): Integer;
    function gf_TelaVar_Retorna_Dependente(psSiglaTabela: String): Integer;
    function gf_TelaVar_Retorna_Conteudo_Controle(psSiglaTabela: String; psNomeControle: string; psNomeValor: string): Integer;
    procedure gp_TelaVar_Mensagem_Acumular(const piMensagem: Integer;
                                           const psParam1: String;
                                           const psParam2: String;
                                           const psParam3: String);
    procedure gp_TelaVar_Toolbar_Atualizar(prTelaVar: TumaTelaVar);
    function  gf_TelaVar_Achar_Codigo(psParametro: String;
                                      var psValor: String;
                                      var psDescricao: string;
                                      pConexao: Integer;
                                      psFiltro: String): Boolean;
    procedure gp_TelaVar_Executar(piComando: Integer; psComando_Parms: String);

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untTelaVar: TuntTelaVar;

implementation

uses uniDados, uniErros, uniMDI, uniLocalizar, uniGlobal;

{'-------------------------------------------------------------------------------------------------
' TuntTelaVar.gp_TelaVar_Passar
'     valida a string
'-------------------------------------------------------------------------------------------------}
procedure TuntTelaVar.gp_TelaVar_Passar(prParametro: TumParametro);
var
  liConta: Integer;
begin

  {'Dados do Par�metro}
  grTelaVar.P := prParametro;

  {'Dados de Contexto/Execu��o}
  grTelaVar.DynaID := 0;
  grTelaVar.DynaPesquisaID := 0;
  for liConta := 1 to MAX_NUM_DYNATEMPID do
    grTelaVar.DynaTempID[liConta] := 0;

  {'   ---- Apenas alguns defaults ----}
  grTelaVar.Recurso_Retido := False;
  grTelaVar.Acesso_Nivel2 := False;
  grTelaVar.Acesso_Nivel3 := False;
  grTelaVar.Comando_MDI := ED_CONSULTA;
  grTelaVar.Cod_Visao := 'V';

end;

{'-------------------------------------------------------------------------------------------------
' TuntTelaVar.lp_TelaVar_ExecSelect
'     Abrir os snapshots globais do m�dulo se necess�rio
'-------------------------------------------------------------------------------------------------}
function TuntTelaVar.gf_TelaVar_ExecSelect(psID: String;
                                           paParm: array of Variant): Integer;
var
  lsSQL: String;
begin

  lsSQL := dmDados.SqlVersao(psID, paParm);

  if lsSQL <> VAZIO then
    Result := dmDados.ExecutarSelect(lsSQL)
  else
    Result := IOPTR_NOPOINTER;

end;
{*-----------------------------------------------------------------------------
 *  TuntTelaVar.gp_TelaVar_Inicializa
 *        Cria o objeto e inicializa variaveis
 *-----------------------------------------------------------------------------}
procedure TuntTelaVar.gp_TelaVar_Inicializar;
begin
  // cria o objeto
  untTelaVar := TuntTelaVar.Create;

end;

{*-----------------------------------------------------------------------------
 *  TuntTelaVar.gp_TelaVar_Finaliza
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
procedure TuntTelaVar.gp_TelaVar_Finalizar;
begin
  // destroi objeto
  untTelaVar.Free;
end;

{'-------------------------------------------------------------------------------------------------
' TuntTelaVar.gp_TelaVar_Seta_Entidade
'     valida a string
'-------------------------------------------------------------------------------------------------}
procedure TuntTelaVar.gp_TelaVar_Seta_Entidade(psEntidade: String);
var
  i: Word;
begin

  for i := 1 to High(gaLista_Parametros) do
    if gaLista_Parametros[i].Entidade = psEntidade then
    begin
      gp_TelaVar_Passar(gaLista_Parametros[i]);
      Exit;
    end;

  // Retorna erro
  dmDados.giStatus := 1;

end;

{'-------------------------------------------------------------------------------------------------
' TuntTelaVar.gp_TelaVar_Retorna_Tabela
'     valida a string
'-------------------------------------------------------------------------------------------------}
function TuntTelaVar.gf_TelaVar_Retorna_Tabela(psSiglaTabela: String): Integer;
var
  i: Word;
begin

  Result := 0;

  for i := 1 to High(gaLista_Parametros) do
    if gaLista_Parametros[i].Sigla_Tabela = psSiglaTabela then
    begin
      Result := i;
      Exit;
    end;

  // Retorna erro
  dmDados.giStatus := 1;

end;

{'-------------------------------------------------------------------------------------------------
' TuntTelaVar.gp_TelaVar_Retorna_Dependente
'     valida a string
'-------------------------------------------------------------------------------------------------}
function TuntTelaVar.gf_TelaVar_Retorna_Dependente(psSiglaTabela: String):Integer;
var
  i: Word;
begin

  Result := 0;

  for i := 1 to High(gaLista_Dependentes) do
    if gaLista_Dependentes[i].sSigla_Tabela = psSiglaTabela then
    begin
      Result := i;
      Exit;
    end;

  // Retorna erro
  dmDados.giStatus := 1;

end;

{'-------------------------------------------------------------------------------------------------
' TuntTelaVar.gp_TelaVar_Retorna_Conteudo_Campo
'     valida a string
'-------------------------------------------------------------------------------------------------}
function TuntTelaVar.gf_TelaVar_Retorna_Conteudo_Controle(psSiglaTabela: String; psNomeControle: string; psNomeValor: string): Integer;
var
  i,j: Word;
begin

  Result := 0;

  for i := 1 to High(gaLista_Parametros) do
    if gaLista_Parametros[i].Sigla_Tabela = psSiglaTabela then
    begin
      break;
    end;

  { se nao achou }
  if i > MAX_LIST_PARAM then
    Exit;

  j := 1;
  while gaLista_Campos[i][j].sSigla_Tabela = psSiglaTabela do
  begin
    if gaLista_Campos[i][j].sNome_Controle = psNomeControle then
    begin
      Result := j;
      Exit;
    end;
    j := j + 1;
  end;

  // Retorna erro
  dmDados.giStatus := 1;

end;

{'-------------------------------------------------------------------------------------------------
' TuntTelaVar.gp_TelaVar_Mensagem_Acumular
'---------------------------------------------------------------------------------------------
' Objetivo:
'     Acumula mensagem para mostrar no grid de erros
'---------------------------------------------------------------------------------------------
' Par�metros:
'     pMensagem  = Codigo da mensagem
'     pParam1    = 1� par�metro a ser substituido na mensagem
'     pParam2    = 2� par�metro a ser substituido na mensagem
'     pParam3    = 3� par�metro a ser substituido na mensagem
'---------------------------------------------------------------------------------------------
'-------------------------------------------------------------------------------------------------}
procedure TuntTelaVar.gp_TelaVar_Mensagem_Acumular(const piMensagem: Integer;
                                                   const psParam1: String;
                                                   const psParam2: String;
                                                   const psParam3: String);
begin
    try
      dmDados.giStatus := STATUS_OK;

      dmDados.gaMsgParm[0] := psParam1;
      dmDados.gaMsgParm[1] := psParam2;
      dmDados.gaMsgParm[2] := psParam3;
      untErros.gp_Erro_Carregar(piMensagem, dmDados.gaMsgParm);

  except
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TuntTelaVar.gp_TelaVar_Toolbar_Atualizar
'-------------------------------------------------------------------------------------------------}
procedure TuntTelaVar.gp_TelaVar_Toolbar_Atualizar(prTelaVar: TumaTelaVar);
begin
    { Atualiza a c�pia global do status deste form}
    grParam_TelaVar := prTelaVar;

    { For�a a atualiza��o da barra de bot�es}
    formMDI.gp_AtualizaBarraBotoes;
//    InvalidateRect(formMDI.cobMenu.Handle, nil, False);
    formMDI.gp_AtualizaBarraStatus;
//    InvalidateRect(formMDI.stbMDI.Handle, nil, False);
end;

{'-------------------------------------------------------------------------------------------------
' TuntTelaVar.gf_TelaVar_Achar_Codigo
'-------------------------------------------------------------------------------------------------
' Objetivo:
'     Apresentar uma caixa de di�logo de sele��o de um dos par�metros e retornar o c�digo
'-------------------------------------------------------------------------------------------------
' Par�metros:
'     pParametro  : Qual o par�metro desejado (BANCO, CARGO, etc.)
'     pContexto   : Contexto: C�digos da Empresa, Unidade, Refer�ncia, Pagamento, Etc.
'     pValor      : Vari�vel para o retorno do c�digo selecionado
'-------------------------------------------------------------------------------------------------
' Retorno:
'     True se foi selecionado um valor, False se cancelado ou houve erro
'     O c�digo em si � retornado em pValor
'-------------------------------------------------------------------------------------------------
'-------------------------------------------------------------------------------------------------}
function TuntTelaVar.gf_TelaVar_Achar_Codigo(psParametro: String;
                                             var psValor: String;
                                             var psDescricao: string;
                                             pConexao: Integer;
                                             psFiltro: String): Boolean;
begin

    { Tratamento padr�o de erros}
    try
      dmDados.giStatus := STATUS_OK;

      { Passa os par�metros para o DialogBox espec�fico}
      gsCod_Parametro := psParametro;
      gpConexao := pConexao;
      gpFiltro := psFiltro;

      dlgLocalizar.ShowModal;
      { ampulheta}
      Screen.Cursor := crHOURGLASS;
      formMDI.Refresh;

      { limpa o filtro }
      gpFiltro := VAZIO;

      { Trata e repassa o retorno}
      if Trim(gsCod_Valor) <> VAZIO then
      begin
        Result := True;
        psValor := gsCod_Valor;
        psDescricao := gsDesc_Valor;
      end
      else
        Result := False;

  except
    Result := False;
    { Mudar aqui se isto ficar fixado}
    dmDados.ErroTratar('gf_TelaVar_Achar_Codigo - uniTELAVAR.PAS');
  end;
end;

{'---------------------------------------------------------------------------------------------
' Objetivo:
'     Passa comando da MDI para a Tela Vari�vel filha
'---------------------------------------------------------------------------------------------
' Par�metros:
'     pComando       = nome do comando (1 caracter)
'     pComando_Parms = par�metros adicionais
'---------------------------------------------------------------------------------------------
' Retorno:
'     Repassa o status que a janela filha retornar
'---------------------------------------------------------------------------------------------}
procedure TuntTelaVar.gp_TelaVar_Executar(piComando: Integer; psComando_Parms: String);
var
 lPaint : TPaintBox;
begin


  try

    dmDados.giStatus := STATUS_OK;

    If (gfrmTV = nil) Then
    begin
      {' Avisa que est� bloqueado}
      dmDados.giStatus := 1;
      Exit;
    End;

    {' Comando passado}
    giComando_MDI := piComando;
    {' Par�metros passados}
    gsComando_MDI_Parms := psComando_Parms;

    {' Forma de comunica��o MDI <-> Filha}
    lPaint := TPaintBox(gfrmTV.FindComponent('PntComunicador'));
    lPaint.Refresh;
//    lPaint.Paint;
    {' Fecha o canal}
    giComando_MDI := 0;

  except
    Screen.cursor := crDEFAULT;
    dmDados.ErroTratar('gp_TelaVar_Executar - uniTELAVAR.PAS');
  end;
End;

end.
