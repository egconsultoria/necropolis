unit uniParam;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
type
 {*======================================================================
 *            RECORDS
 *======================================================================}

 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntParam = class(TObject)
  private

    { Private declarations }
    mbFeito: Boolean;

    procedure mp_Param_Lista_Ler;
  public
    { Public declarations }
    procedure gp_Param_Inicializar;
    procedure gp_Param_Finalizar;
    function  gf_Param_Sql(psNomeParametro: String;
                           psCodigo: String;
                           pasParam_Colunas: array of String): String;
    procedure gp_Param_Iniciar;
    procedure gp_Param_EstufaComboParametrosMDI(pcCombo: TComboBox; psModulo: String);
    function  gf_Param_Reter(var psNomeParametro: String;
                             pbExclusivo: Boolean): Integer;
    function  gf_Param_Pode_Excluir(prParametro: TumParametro;
                                    psCodigo: String;
                                    piConexao: Integer): Boolean;
    procedure gp_Param_Excluir_EmCascata(var prParametro: TumParametro;
                                         psCodigo: String);

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untParam: TuntParam;

implementation

uses uniDados, uniTelaVar, uniLock, uniGlobal;

{*-----------------------------------------------------------------------------
 *  TuntParam.gp_Param_Inicializa
 *        Cria o objeto e inicializa variaveis
 *-----------------------------------------------------------------------------}
procedure TuntParam.gp_Param_Inicializar;
begin
  // cria o objeto
  untParam := TuntParam.Create;

  untParam.mbFeito := False;
end;

{*-----------------------------------------------------------------------------
 *  TuntParam.gp_Param_Finaliza
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
procedure TuntParam.gp_Param_Finalizar;
begin
  // destroi objeto
  untParam.Free;
end;

{'-------------------------------------------------------------------------------------------------
' TuntParam.gf_Param_Sql
'     valida a string
'-------------------------------------------------------------------------------------------------}
function TuntParam.gf_Param_Sql(psNomeParametro: String;
                                psCodigo: String;
                                pasParam_Colunas: array of String): String;
var
  i: Integer;
  lw: Word;

  ltParametro      : TumParametro;
  lsNomeCodigo     : String;
  lsNomePar        : String;
  lsSQL            : String;
begin

  gp_Param_Iniciar;

  Result := VAZIO;

  for i := 1 To High(gaLista_Parametros) do
  begin
    if gaLista_Parametros[i].Sigla_Tabela = psNomeParametro then
    begin
      ltParametro := gaLista_Parametros[i];

      {'Analisa quais colunas da tabela ir� trazer :}
      lsSQL := 'SELECT Base.' + pasParam_Colunas[0];
      for lw := 1 To High(pasParam_Colunas) do
        if pasParam_Colunas[lw] <> VAZIO then
          lsSQL := lsSQL + ', Base.' + pasParam_Colunas[lw]
        else
          Break;

      lsSQL := lsSQL + ' FROM ';

      lsNomeCodigo := ltParametro.Coluna_Codigo;

      if ltParametro.Tipo_Objeto = 'Q' then
        lsNomePar := Copy(ltParametro.Nome_Tabela, 3, Length(ltParametro.Nome_Tabela))
      else
        lsNomePar := ltParametro.Nome_Tabela;

      psCodigo := Trim(psCodigo);

      lsSQL := lsSQL + lsNomePar + CTEDB_ALIAS + ' Base';

      if psCodigo <> VAZIO then
      begin
        lsSQL := lsSQL + ' WHERE ' + lsNomeCodigo + ' = ''' + psCodigo + '''';(* + '''';*)
        if ltParametro.Filtro <> VAZIO then
          lsSQL := lsSQL + ' AND ' + ltParametro.Filtro;
      end
      else
        if ltParametro.Filtro <> VAZIO then
          lsSQL := lsSQL + ' WHERE ' + ltParametro.Filtro;

      Result := lsSQL;
      break;
    end;
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntParam.gp_Param_Iniciar
'     Abrir os snapshots globais do m�dulo se necess�rio
'-------------------------------------------------------------------------------------------------}
procedure TuntParam.gp_Param_Iniciar;
begin
  {'Se primeira vez criar o snapshot de parametros}
  if mbFeito then
    {'J� fez uma vez n�o precisa repetir}
    Exit;

  mbFeito := True;

  mp_Param_Lista_Ler;

  {'Inicializa a global com o valor default "*" para o Select de todas as colunas}
  gaParam_Colunas[0] := '*';

end;

{'-------------------------------------------------------------------------------------------------
' TuntParam.lp_Param_Lista_Ler
'     Abrir os snapshots globais do m�dulo se necess�rio
'-------------------------------------------------------------------------------------------------}
procedure TuntParam.mp_Param_Lista_Ler;
var
  i                : Integer;
  j                : Integer;
  liParametros     : Integer;
  liDependentes    : Integer;
  liCampos         : Integer;

  liIndCol:     array[0..MAX_NUM_COLUNAS] of Integer;

begin

  for i := 0 to MAX_NUM_COLUNAS do
    liIndCol[i] := IOPTR_NOPOINTER;

  i := 1;

  liParametros := untTelaVar.gf_TelaVar_ExecSelect('PARAM_0005', gaParm);

  while not dmDados.Status(liParametros, IOSTATUS_EOF) do
  begin
    if i > MAX_LIST_PARAM then
    begin
      showMessage('Ultrapassou o numero da lista de Parametros');
      break;
    end;

    gaLista_Parametros[i].Entidade := dmDados.ValorColuna(liParametros, 'Entidade', liIndCol[0]);
    gaLista_Parametros[i].Nome_Tabela := dmDados.ValorColuna(liParametros, 'Nome_Tabela', liIndCol[1]);
    gaLista_Parametros[i].Sigla_Tabela := dmDados.ValorColuna(liParametros, 'Sigla_Tabela', liIndCol[2]);
    gaLista_Parametros[i].Cod_Seguranca := dmDados.ValorColuna(liParametros, 'Cod_Seguranca', liIndCol[3]);
    gaLista_Parametros[i].Tipo_Objeto := dmDados.ValorColuna(liParametros, 'Tipo_Objeto', liIndCol[4]);
    gaLista_Parametros[i].bTela_Especial := (dmDados.ValorColuna(liParametros, 'Tela_Variavel', liIndCol[5]) <> 'S');
    gaLista_Parametros[i].Coluna_Codigo := dmDados.ValorColuna(liParametros, 'Coluna_Codigo', liIndCol[6]);
    gaLista_Parametros[i].bCodigo_Numerico := (dmDados.ValorColuna(liParametros, 'Codigo_Numerico', liIndCol[7]) = 'S');
    gaLista_Parametros[i].Codigo_Minimo := StrToInt(dmDados.ValorColuna(liParametros, 'Codigo_Minimo', liIndCol[8]));
    gaLista_Parametros[i].Coluna_Descricao := dmDados.ValorColuna(liParametros, 'Coluna_Descricao', liIndCol[9]);
    gaLista_Parametros[i].Coluna_Nivel := dmDados.ValorColuna(liParametros, 'Coluna_Nivel', liIndCol[10]);
    gaLista_Parametros[i].Nome_Tela := dmDados.ValorColuna(liParametros, 'Tela_Padrao', liIndCol[11]);
    gaLista_Parametros[i].Indice_Tela := StrToInt(dmDados.ValorColuna(liParametros, 'Indice_Tela', liIndCol[12]));
    gaLista_Parametros[i].Titulo_Tela := dmDados.ValorColuna(liParametros, 'Descricao_Tabela', liIndCol[13]);
    gaLista_Parametros[i].Largura_Tela := StrToInt(dmDados.ValorColuna(liParametros, 'Largura_Tela', liIndCol[14]));
    gaLista_Parametros[i].Altura_Tela := StrToInt(dmDados.ValorColuna(liParametros, 'Altura_Tela', liIndCol[15]));
    gaLista_Parametros[i].Rotina_Validade := dmDados.ValorColuna(liParametros, 'Rotina_Validade', liIndCol[16]);
    gaLista_Parametros[i].HelpID_Tela := StrToInt(dmDados.ValorColuna(liParametros, 'HelpID', liIndCol[17]));
    gaLista_Parametros[i].Filtro := dmDados.ValorColuna(liParametros, 'Filtro', liIndCol[18]);
    gaLista_Parametros[i].Modulo := dmDados.ValorColuna(liParametros, 'Descricao_Modulo', liIndCol[19]);

    j := 1;

    gaParm[0] := gaLista_Parametros[i].Sigla_Tabela;
    liCampos := untTelaVar.gf_TelaVar_ExecSelect('PARAM_0011', gaParm);

    while not dmDados.Status(liCampos, IOSTATUS_EOF) do
    begin
      if j > MAX_LIST_CAMPO then
      begin
        showMessage('Ultrapassou o numero da lista de Campos');
        break;
      end;

      gaLista_Campos[i][j].sSigla_Tabela := dmDados.ValorColuna(liCampos, 'Sigla_Tabela', liIndCol[34]);
      gaLista_Campos[i][j].iOrdem := dmDados.ValorColuna(liCampos, 'Ordem', liIndCol[35]);
      gaLista_Campos[i][j].sNome_Campo := dmDados.ValorColuna(liCampos, 'Nome_Campo', liIndCol[36]);
      gaLista_Campos[i][j].sTipo_Campo := dmDados.ValorColuna(liCampos, 'Tipo_Campo', liIndCol[37]);
      gaLista_Campos[i][j].iTamanho := dmDados.ValorColuna(liCampos, 'Tamanho', liIndCol[38]);
      gaLista_Campos[i][j].sFormato := dmDados.ValorColuna(liCampos, 'Formato', liIndCol[39]);
      gaLista_Campos[i][j].sDescricao := dmDados.ValorColuna(liCampos, 'Descricao', liIndCol[40]);
      gaLista_Campos[i][j].sNome_Controle := dmDados.ValorColuna(liCampos, 'Nome_Controle', liIndCol[41]);
      gaLista_Campos[i][j].sTipo_Controle := dmDados.ValorColuna(liCampos, 'Tipo_Controle', liIndCol[42]);
      gaLista_Campos[i][j].sMascara := dmDados.ValorColuna(liCampos, 'Mascara', liIndCol[43]);
      gaLista_Campos[i][j].sCombo_Tabela := dmDados.ValorColuna(liCampos, 'Combo_Tabela', liIndCol[44]);
      gaLista_Campos[i][j].sCombo_Filtro := dmDados.ValorColuna(liCampos, 'Combo_Filtro', liIndCol[45]);
      gaLista_Campos[i][j].sEdita_Inclui := dmDados.ValorColuna(liCampos, 'Edita_Inclui', liIndCol[46]);
      gaLista_Campos[i][j].sEdita_Altera := dmDados.ValorColuna(liCampos, 'Edita_Altera', liIndCol[47]);
      gaLista_Campos[i][j].sValidade := dmDados.ValorColuna(liCampos, 'Validade', liIndCol[48]);
      gaLista_Campos[i][j].sDefault := dmDados.ValorColuna(liCampos, 'Padrao', liIndCol[49]);
      gaLista_Campos[i][j].sFuncao := dmDados.ValorColuna(liCampos, 'Funcao', liIndCol[50]);
      gaLista_Campos[i][j].sVazio := dmDados.ValorColuna(liCampos, 'Vazio', liIndCol[51]);
      gaLista_Campos[i][j].sAutomatico := dmDados.ValorColuna(liCampos, 'Automatico', liIndCol[52]);
      gaLista_Campos[i][j].sGrava := dmDados.ValorColuna(liCampos, 'Exibe', liIndCol[53]);
      gaLista_Campos[i][j].sColuna_Codigo := dmDados.ValorColuna(liCampos, 'Coluna_Codigo', liIndCol[54]);
      gaLista_Campos[i][j].sAlinha := dmDados.ValorColuna(liCampos, 'Alinha', liIndCol[55]);
      { guarda o formato para a mascara de procura da coluna codigo}
      if gaLista_Campos[i][j].sNome_Campo = gaLista_Parametros[i].Coluna_Codigo then
      begin
        gaLista_Parametros[i].Mascara_Codigo := gaLista_Campos[i][j].sFormato;
        gaLista_Parametros[i].Alinha_Codigo := gaLista_Campos[i][j].sAlinha;
        gaLista_Parametros[i].Alinha_Tamanho := gaLista_Campos[i][j].iTamanho;
      end;
      gaLista_Campos[i][j].sUnico := dmDados.ValorColuna(liCampos, 'Unico', liIndCol[55]);
      j := j + 1;

      if (dmDados.Proximo(liCampos) = IORET_EOF) or
         (dmDados.giStatus <> STATUS_OK) then
          break;
    end;
    dmDados.FecharConexao(liCampos);

    i := i + 1;
    if (dmDados.Proximo(liParametros) = IORET_EOF) or
       (dmDados.giStatus <> STATUS_OK) then
        break;
  end;
  dmDados.FecharConexao(liParametros);

  i := 1;

  liDependentes := untTelaVar.gf_TelaVar_ExecSelect('PARAM_0012', gaParm);

  while not dmDados.Status(liDependentes, IOSTATUS_EOF) do
  begin
    if i > MAX_LIST_DEPEN then
    begin
      showMessage('Ultrapassou o numero da lista de Dependentes');
      break;
    end;

    gaLista_Dependentes[i].sSigla_Tabela := dmDados.ValorColuna(liDependentes, 'Sigla_Tabela', liIndCol[20]);
    gaLista_Dependentes[i].sNumero_Tabela := dmDados.ValorColuna(liDependentes, 'Numero_Tabela', liIndCol[21]);
    gaLista_Dependentes[i].sTabela_Dependente := dmDados.ValorColuna(liDependentes, 'Tabela_Dependentes', liIndCol[22]);
    gaLista_Dependentes[i].sTipo_Cadastro := dmDados.ValorColuna(liDependentes, 'Tipo_Cadastro', liIndCol[23]);
    gaLista_Dependentes[i].sTipo_Loop := dmDados.ValorColuna(liDependentes, 'Tipo_Loop', liIndCol[24]);
    gaLista_Dependentes[i].sNome_Lista := dmDados.ValorColuna(liDependentes, 'Nome_Lista', liIndCol[25]);
    gaLista_Dependentes[i].sCodigo_SQL := dmDados.ValorColuna(liDependentes, 'Codigo_SQL', liIndCol[26]);
    gaLista_Dependentes[i].sNome_Tabela := dmDados.ValorColuna(liDependentes, 'Nome_Tabela', liIndCol[27]);
    gaLista_Dependentes[i].sColuna_Codigo := dmDados.ValorColuna(liDependentes, 'Coluna_Codigo', liIndCol[28]);
    gaLista_Dependentes[i].sValor_Codigo1 := dmDados.ValorColuna(liDependentes, 'Valor_Codigo1', liIndCol[29]);
    gaLista_Dependentes[i].sValor_Codigo2 := dmDados.ValorColuna(liDependentes, 'Valor_Codigo2', liIndCol[30]);
    gaLista_Dependentes[i].sValor_Codigo3 := dmDados.ValorColuna(liDependentes, 'Valor_Codigo3', liIndCol[31]);
    gaLista_Dependentes[i].sCampo_Ordem := dmDados.ValorColuna(liDependentes, 'Campo_Ordem', liIndCol[32]);
    gaLista_Dependentes[i].sAtualizar_Campos := dmDados.ValorColuna(liDependentes, 'Atualizar_Campos', liIndCol[33]);
    i := i + 1;

    if (dmDados.Proximo(liDependentes) = IORET_EOF) or
       (dmDados.giStatus <> STATUS_OK) then
        break;
  end;
  dmDados.FecharConexao(liDependentes);
end;

{'-------------------------------------------------------------------------------------------------
' TuntParam.gp_Param_EstufaComboParametrosMDI
'     Abrir os snapshots globais do m�dulo se necess�rio
'-------------------------------------------------------------------------------------------------}
procedure TuntParam.gp_Param_EstufaComboParametrosMDI(pcCombo: TComboBox; psModulo: String);
var
  i: Integer;
begin

  dmDados.giStatus := STATUS_OK;

  {' *** Estufa o combo de par�metros ***}
  try
    pcCombo.Items.Clear;
    for i := 1 to MAX_LIST_PARAM do
    begin
      if gaLista_Parametros[i].Entidade <> VAZIO then
        if gaLista_Parametros[i].Modulo = psModulo then
          if not gaLista_Parametros[i].bCodigo_Numerico then
            pcCombo.Items.Add(gaLista_Parametros[i].Entidade);
    end;
  except
    dmDados.ErroTratar('gp_Param_EstufaComboParametrosMDI - uniParam.PAS');
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TuntParam.gf_Param_Reter
'     Criar um "LOCK" do recurso par�metro pNomeParametro para o contexto pContexto
'-------------------------------------------------------------------------------------------------}
function TuntParam.gf_Param_Reter(var psNomeParametro: String;
                                  pbExclusivo: Boolean): Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    if pbExclusivo then
      Result := untLock.gf_Lock_Reter(psNomeParametro, PARAMETROS, LOCK_EXCLUSIVO)
    else
      Result := untLock.gf_Lock_Reter(psNomeParametro, PARAMETROS, LOCK_CONSULTA);

  except
    Result := LOCK_RETORNO_ERRO;
    dmDados.ErroTratar('gf_Param_Reter - uniParam.PAS');
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TuntParam.gf_Param_Pode_Excluir
'     Verifica se tem permissao para excluir
'-------------------------------------------------------------------------------------------------}
function TuntParam.gf_Param_Pode_Excluir(prParametro: TumParametro;
                                         psCodigo: String;
                                         piConexao: Integer): Boolean;
var
//  i: Integer;           // Vari�vel tempor�ria de uso geral
  llContagem: Longint;
  liXRef_Rs: Integer;    // Dynaset ReadOnly de rela��es
  liTeste_Rs: Integer;   // Dynaset ReadOnly de usos do registro
  lavParms: array[0..8] of Variant;
  iPtrColTabela: Integer;
  iPtrColColuna: Integer;
  iPtrColReferencia: Integer;
  iPtrColSQLCodigo: Integer;
  iPtrColContagem: Integer;
begin
  try
    dmDados.giStatus := STATUS_OK;

    lavParms[0] := prParametro.Nome_Tabela;
    lavParms[1] := prParametro.Coluna_Codigo;

    Result := True;
    liXRef_Rs := untTelaVar.gf_TelaVar_ExecSelect('INTREF_9999', lavParms);
    if liXRef_Rs = IOPTR_NOPOINTER then
      // Sai com True, nada a testar
      Exit;

    if dmDados.Status(liXRef_Rs, IOSTATUS_NOROWS) then
    begin
      dmDados.FecharConexao(liXRef_Rs);
      // Sai com True, nada a testar
      Exit;
    end;

    Result := False;

    lavParms[4] := prParametro.Nome_Tabela;
    lavParms[5] := prParametro.Coluna_Codigo;
    lavParms[8] := psCodigo;
    repeat
      iPtrColTabela := IOPTR_NOPOINTER;
      iPtrColColuna := IOPTR_NOPOINTER;
      iPtrColReferencia := IOPTR_NOPOINTER;
      iPtrColSQLCodigo := IOPTR_NOPOINTER;
      iPtrColContagem := IOPTR_NOPOINTER;

      lavParms[6] := dmDados.ValorColuna(liXRef_Rs, 'Tabela', iPtrColTabela);
      lavParms[7] := dmDados.ValorColuna(liXRef_Rs, 'Coluna', iPtrColColuna);
      if dmDados.ValorColuna(liXRef_Rs, 'Tipo_Referencia', iPtrColReferencia) = 'P0' then
      begin
        liTeste_Rs := untTelaVar.gf_TelaVar_ExecSelect(dmDados.ValorColuna(liXRef_Rs, 'SQL_Codigo', iPtrColSQLCodigo), lavParms);
        if liTeste_Rs <> IOPTR_NOPOINTER then
        begin
          llContagem := dmDados.ValorColuna(liTeste_Rs, 'Contagem', iPtrColContagem);
          if llContagem <> 0 then
          begin
            dmDados.gaMsgParm[0] := IntToStr(llContagem);
            dmDados.gaMsgParm[1] := lavParms[6];

//            i := dmDados.MensagemExibir('', 4202);
            dmDados.MensagemExibir('', 4202);
            dmDados.FecharConexao(liXRef_Rs);
            dmDados.FecharConexao(liTeste_Rs);
            // Saida com False
            Exit;
          end;
          dmDados.FecharConexao(liTeste_Rs);
        end
        else
        // por ora n�o previsto, apenas permite
      end;
    until dmDados.Proximo(liXRef_Rs) <> IORET_OK;
    dmDados.FecharConexao(liXRef_Rs);

    // Se tudo Ok, sai com True
    Result := True;

  except
    dmDados.ErroTratar('gf_Param_Pode_Excluir - uniParam.PAS');
    Result := False;
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TuntParam.gp_Param_Excluir_EmCascata
'     Excluir os registros das outras tabelas
'-------------------------------------------------------------------------------------------------}
procedure TuntParam.gp_Param_Excluir_EmCascata(var prParametro: TumParametro;
                                              psCodigo: String);
var
  lavParm: array[0..4] of Variant;
  lsSQL: string;
  liConexao: Integer;
  iPtrColTabela: Integer;
  iPtrColColuna: Integer;
  iPtrColReferencia: Integer;
  iPtrColSQLCodigo: Integer;
begin
  try
    dmDados.giStatus := STATUS_OK;

    lavParm[0] := prParametro.Nome_Tabela;
    lavParm[1] := prParametro.Coluna_Codigo;

    liConexao := untTelaVar.gf_TelaVar_ExecSelect('INTREF_9999', lavParm);
    if liConexao = IOPTR_NOPOINTER then
      // Sai com True, nada a testar
      Exit;

    if dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      dmDados.FecharConexao(liConexao);
      // Sai com True, nada a testar
      Exit;
    end;

    iPtrColTabela := IOPTR_NOPOINTER;
    iPtrColColuna := IOPTR_NOPOINTER;
    iPtrColReferencia := IOPTR_NOPOINTER;
    iPtrColSQLCodigo := IOPTR_NOPOINTER;

    repeat
      if dmDados.ValorColuna(liConexao, 'Tipo_Referencia', iPtrColReferencia) = 'P7' then
      begin
        lavParm[0] := dmDados.ValorColuna(liConexao, 'Tabela', iPtrColTabela);
        lavParm[1] := dmDados.ValorColuna(liConexao, 'Coluna', iPtrColColuna);
        lavParm[2] := psCodigo;

        lsSql := dmDados.SqlVersao(dmDados.ValorColuna(liConexao, 'SQL_Codigo', iPtrColSQLCodigo), lavParm);
        if lsSql <> VAZIO Then
          dmDados.gsRetorno := dmDados.ExecutarSQL(lsSql);
      end;
    until dmDados.Proximo(liConexao) <> IORET_OK;
    dmDados.FecharConexao(liConexao);

  except
    dmDados.ErroTratar('gp_Param_Excluir_EmCascata - uniParam.PAS');
  end;

end;

end.
