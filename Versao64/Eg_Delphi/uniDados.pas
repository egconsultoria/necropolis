unit uniDados;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, DB, DBTables, Variants,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}

 {*======================================================================
 *            RECORDS
 *======================================================================}
type
{*----------------------------------------------------------------------
 * Estrutura do Campo
 *}
  TEstrutura = Record
     iTamanho:  Integer;
     iDecimais: Integer;
     cTipo:     Char;
  end;

{*----------------------------------------------------------------------
 * Estrutura da Chave
 *}
  TChave = Record
     iQtdChave: Integer;
     iTamChave: Integer;
  end;

{*----------------------------------------------------------------------
 * Estrutura do Arquivo
 *}
  TArqEstrutura = Record
    sNomeArquivo: string;
    sNomeTabela:  string;
    aEstrutura:   array [0..MAX_NUM_ESTRUTURA] of TEstrutura;
    iQtdCampo:    Integer;
    iQtdChave:    Integer;
    iTamChave:    array [0..MAX_NUM_CHAVES] of Integer;
  end;

{*----------------------------------------------------------------------
 * Estrutura das Colunas
 *}
  TColuna = Record
    sNomeColuna:    string;
(*    iNumeroColuna:  Integer; *)
    ftTipoColuna:   TFieldType;
(*    iTamanho:       Integer; *)
    vValor:         Variant;
    bAtualizado:    Boolean;
(*    iNulo:          Integer; *)
    iProximaColuna: Integer;
    iColunaAssoc:   Integer;
  end;

{*----------------------------------------------------------------------
 * Estrutura de Conex�es para as entidades prim�rias
 *}
  TConexoes = Record
    bEmUso:              Boolean;
    iConexao:            Integer;
    sSql:                string;
    sFind:               string;
    sWhere:              string;
    sTabela:             string;
    sIndexCampos:        string;    { Campos Chaves da Tabela F�sica }
    bPosicionadoEmLinha: Boolean;
    bTemLinhas:          Boolean;
    bFimLinhas:          Boolean;
    bAchouLinha:         Boolean;
    iPtrColunas:         Integer;
    bInvalColunas:       Boolean;
    bTemAtualizacao:     Boolean;
    bRefazSQL:           Boolean;
    iModoOperacao:       Integer;
    bkMarkParaMatch:     TBookmarkStr;   { Somente para Access (BookMark) }
    sFindParaMatch:      string;
    iTransacao:          Integer;
    iTransConAssociada:  Integer;   { Conexao Associada para caso de Refresh dentro de transacao - ACCESS }
    iTransConOrigem:     Integer;
  end;

{*----------------------------------------------------------------------
 * Estrutura de Informacao das colunas
 *}
  TColunaInfo = Record
    sNome:       string;
    iOrdinalPos: Integer;
    ftTipo:      TFieldType;
    vValor:      Variant;
    iTamanho:    Integer;
  end;

{*----------------------------------------------------------------------
 * Estrutura de Informacao do Banco de Dados
 *}
  TDBInfo = Record
    sDBNome:      string;
    sTabelaNome:  string;
  end;

{*----------------------------------------------------------------------
 * Estrutura de Parametros para Stored Procedure
 *}
  TSParams = Record
    sNome:      string;
    ptTipo:     TParamType;
    ftTipoDado: TFieldType;
    vValor:     Variant;
  end;

{*======================================================================
 *            CLASSES
 *======================================================================}
  TdmDados = class(TDataModule)
  private
    { Private declarations }
    lbSaindo: Boolean;
    lbEmErro: Boolean;
    mbDentro: Boolean;
    miConfigIndCol: Integer;

    function CriarSql(const sSql: string): Integer;
    procedure FecharSql(const iID: Integer);
    procedure FinalizarConexao;
    procedure FinalizarTransacao;
    function ColunasAlloc(const iNumCols: Integer): Integer;
    function PegarConEmTransacao(var iConexao: Integer): Integer;
    function PegarDataAssoc(const iIndexCol: Integer): Variant;
    function PegarUltConAssoc(const iConexao: Integer): Integer;
    function CamposIndices(const aDBInfo: TDBInfo;
                         const sIndex: string): string;
    function NovaConexao: Integer;
    function TabelaPrimaryKey(const sTabela: string): string;
    procedure LimparColunas(const iPtrCols: Integer);
    procedure GuardarIndices(const sChaves: string;
                             var aChaveItem: array of string;
                             var iChavesMaxInd: Integer);

  public
    { Public declarations }
    giStatus:     Integer;
    giTransacao:  Integer;
    giConexao:    Integer;

    gsConexaoRetorno: String; { vari�vel de retorno das api's de IO }

    gbFLAGIO_Iniciar: Boolean;

    { Numero do Ultimo erro pela Rotina de IO }
    giErroConexao: Integer;

    gaConexao:      array[1..MAX_NUM_SQL] of TConexoes;
    gaConexaoLivre: array[1..MAX_NUM_SQL] of Integer;
    gaColunas:      array[0..MAX_NUM_COLUNAS] of TColuna;
    gaTransacoes:   array[1..MAX_NUM_SQL] of string;
    giColunasLivres: Integer;
    giColunasTotal:  Integer;

    giConMsg:         Integer;    { conexao para a tabela de mensagens }
    gaMsgParm:        array[0..MAX_NUM_PARM] of Variant;  {array de par�metros das mensagens }
    giCodMensagem:    Integer;    { chave de pesquisa da tabela de mensagens }
    giNivelMsg:       string;     { codigo de nivel de erro }
    gsInfoAdicionais: string;     { informa��es adicionais do di�logo de mensagens }
    gsFormCaption:    string;     { caption do di�logo de mensagens = ao do form que exibiu a mensagem }
    giMsgResposta:    Integer;    { resposta devolvida pelo di�logo de mensagens }
    giErroCodigo:     Integer;    { C�digo de erro para a rotina gErro_tratar }
    gsStatusMensagem: string;     { Mensagem de Erro }
    gbFlagOnError:    Boolean;    { Indica que j� est� em tratamento de erro. }
                                  { OBs: Para evitar Loops de mensagem que quer dar mensagem }
    gsRetorno : string;           { retorno da funcoes do banco }

    garEstrutura: array[0..MAX_NUM_TABELAS] of TArqEstrutura;
    gdbDados:  TDatabase;
    gtbTabelas: TTable;
    gtbCampos:  TTable;
    gtbIndices: TTable;

    gaSql:      array[1..MAX_NUM_SQL] of TQuery;
    gaSqlTrans: array[1..MAX_NUM_SQL] of Integer;

    function AbrirBanco(const sNomeBanco, sNomeSessao: string): TDatabase;
    procedure FecharBanco(var dbBanco: TDatabase);

    function  DataValida(const sData: string): Boolean;
    function  ValidaData(const sData: string): String;

    { funcoes para tratamento do IO }
    function SqlVersao(const sIDSql: string; var aParam: array of Variant): string;
    procedure IniciarBanco;
    procedure FinalizarBanco;
    procedure PrepararBanco;

    function ValorColuna(var iConexao: Integer;
                         const sColuna: string;
                         var iIndexColuna: Integer): Variant;
    function Excluir(var iConexao: Integer): string;
    function ExcluirStr(var iConexao: Integer;
                       const sTabela: string): string;
    function ExecutarSelect(const sSql: string): Integer;
    function ExecutarSQL(const sSql: string): string;
    function Primeiro(var iConexao: Integer): string;
    function Incluir(var iConexao: Integer;
                     const sTabela: string): string;
    function IncluirStr(const iConexao:Integer;
                        const sTabela: string): string;
    function Ultimo(var iConexao: Integer): string;
    function UltimoRegistro(var iConexao: Integer;
                            const sCampo: string): string;
    function Proximo(var iConexao: Integer): string;
    function Anterior(var iConexao: Integer;
                      const iQtdeLinhas: Integer): string;
    function RecallColumns(const iConexao: Integer): string;
    function Atualizar(var iConexao: Integer): string;

    function SequenciaProxVal(const sSequenciaChave: string): Integer;
    function ConfigValor(const psChave: String): String;
    procedure ConfigGrava(const psChave: String; psValor: string);

    function AtivaFiltro(var iConexao: Integer;
                         const sCriterio: string;
                         const bInicio: Boolean;
                         const iFlag: Integer): string;
    function Posiciona(var iConexao: Integer;
                       const sCampo: string;
                       const vValor: array of Variant): string;
    function AtivaPrimeiraComp(var iConexao: Integer;
                               const sCriterio: string;
                               const aColunasChave: array of string;
                               const iFlag: Integer): string;
    function AtivaModo(var iConexao: Integer;
                     const iMode: Integer): string;
    function AtivaProximaComp(var iConexao: Integer;
                              const iFlag: Integer): string;
    function Status(var iConexao: Integer;
                    const iArgumento: Integer): Boolean;
    function Alterar(var iConexao: Integer;
                     const sTabela: string;
                     const sWhere: string): string;
    function AlterarStr(var iConexao:Integer;
                        const sTabela: string;
                        const sWhere: string): string;
    function AlterarColuna(var iConexao: Integer;
                           const sColuna: string;
                           var iIndexColuna: Integer;
                           const vValor: Variant): string;
    procedure FecharConexao(var iConexao: Integer);
    procedure TransBegin(const sNome: string);
    procedure TransCommit(const sNome: string);
    procedure TransRollback(const sNome: string);

    procedure ErroTratar(const sLocalErro: string);
    function MensagemExibir(const sLocalErro: string;
                            const iCodigo: Integer): Integer;
    function LiteralSubstituir(const vMensagem: Variant;
                               const aMsgParam: array of Variant): string;

    function ColunasInfo(var iConexao: Integer;
                         var aColunasInfo: array of TColunaInfo;
                         const iFlag: Integer):string;
    function TrataDecimalBD(const vValor: Variant;
                            const iFlag: Integer): string;
    function EstufaString(const sValor: string;
                          const sString: string): string;
    function SQLTabelaNome(const sSql: string): string;
    function TirarMascara(const psValor: String): string;
    function TirarSeparador(const psValor: String): string;

    function CriarStored(const psNome: string;
                         const paParams: array of TSParams;
                         const piMaxParam: Integer): Variant;
    function PesquisaTabela: Variant;

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  dmDados: TdmDados;

implementation

uses uniExibeMsg, uniMostraErros, uniGlobal;

{$R *.DFM}

{*----------------------------------------------------------------------
 * TdmDados.AbrirBanco    Abre um banco de dados
 *
 *----------------------------------------------------------------------}
function TdmDados.AbrirBanco(const sNomeBanco, sNomeSessao: string): TDatabase;
var
  dbTempBanco:  TDatabase;
  ssTempSessao: TSession;
begin
  { Inicializa variaveis }
  dbTempBanco  := nil;
  ssTempSessao := nil;
  try
    { se a sessao existe, abre; se nao, cria uma nova }
    Sessions.OpenSession(sNomeSessao);
    ssTempSessao := Sessions.FindSession(sNomeSessao);
    Result := ssTempSessao.FindDatabase(sNomeBanco);
    if Result = nil then
    begin
      { cria um novo objeto para o banco de dados }
      dbTempBanco := TDatabase.Create(Self);
      dbTempBanco.DatabaseName    := sNomeBanco;
      dbTempBanco.AliasName       := sNomeBanco;
      dbTempBanco.SessionName     := sNomeSessao;
      dbTempBanco.KeepConnection  := True;
      dbTempBanco.LoginPrompt     := False;
(*      dbTempBanco.DriverName      := gsDbDriver;              //NOME_DRIVER;*)
      dbTempBanco.Params.Values[TEXTO_USER] := gsDbUserName;  //USER_NAME;
      dbTempBanco.Params.Values[TEXTO_PASS] := gsDbPassWord;  //PASSWORD;
    end;
    { Abre o banco de dados }
    Result := ssTempSessao.OpenDatabase(sNomeBanco);

  except
    { se ocorreu erro libera o objeto da memoria }
    dbTempBanco.Free;
    Result := nil;
    MessageDlg('Erro Fatal: Tentativa de abrir o Banco de Dados falhou !',
               mtWarning, [mbOK], 0);
    Application.Terminate;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.FecharBanco    Fechar o banco de dados
 *
 *----------------------------------------------------------------------}
procedure TdmDados.FecharBanco(var dbBanco: TDatabase);
begin
  try
    { Fecha o Banco de Dados }
    dbBanco.Close;
    { Libera o objeto da memoria }
    dbBanco.Free;
  except
    ErroTratar('FecharBanco - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.DataValida    Verifica se uma data � valida
 *
 *----------------------------------------------------------------------}
function  TdmDados.DataValida(const sData: string): Boolean;
var
//  iDigito: Integer;
  iCode:   Integer;
  iDiaAux: Integer;
  iMesAux: Integer;
  iAnoAux: Integer;

  aMeses: array[1..12] of Integer;    { vetor com o n�mero de dias de cada m�s }
begin
  aMeses[1] := 31;
  aMeses[2] := 28;
  aMeses[3] := 31;
  aMeses[4] := 30;
  aMeses[5] := 31;
  aMeses[6] := 30;
  aMeses[7] := 31;
  aMeses[8] := 31;
  aMeses[9] := 30;
  aMeses[10] := 31;
  aMeses[11] := 30;
  aMeses[12] := 31;

  Result := True;
  try
    {--------------
     Verifica��o dos d�gitos
     --------------}
    { checa todos os d�gitos }
(*    for iDigito := 1 to Length(sData) do
    begin
      { se nao forem digitos entre 0 e 9 }
      if ((Copy(sData, iDigito, 1) < '0') and (Copy(sData, iDigito, 1) > '9')) and (Copy(sData, iDigito, 1) = '/') then
      begin
        Result := False;
        { n�o testa mais nada }
        Exit;
      end;
    end;*)
    { guarda o Dia, Mes e Ano }
    Val(Copy(sData, 1, 2), iDiaAux, iCode);
    Val(Copy(sData, 4, 2), iMesAux, iCode);
    Val(Copy(sData, 7, 2), iAnoAux, iCode);

    {--------------
     Verifica��o do m�s
     --------------}
    if (iMesAux < 1) or (iMesAux > 12) then
    begin
      Result := False;
      { n�o testa mais nada }
      Exit;
    end;
    {--------------
     Verifica��o do dia
     --------------}
    { Se o valor das duas �ltimas posi��es de sData � divis�vel por 4 }
    { o ano � bissexto }
    if (iAnoAux Mod 4 = 0) and (iMesAux = 2) and (iAnoAux <> 0)then
    begin
      { dia maior que permitido no m�s => data inv�lida }
      if (iDiaAux < 1) or (iDiaAux > aMeses[iMesAux] + 1) then
      begin
        Result := False;
        { n�o testa mais nada }
        Exit;
      end;
    end
    else
    begin
      { dia maior que permitido no m�s => data inv�lida }
      if (iDiaAux < 1) or (iDiaAux > aMeses[iMesAux]) then
      begin
        Result := False;
        { n�o testa mais nada }
        Exit;
      end;
    end;
  except
    Result := False;
    ErroTratar('DataValida - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.DataValida    Procura ate a data ser valida
 *
 *----------------------------------------------------------------------}
function  TdmDados.ValidaData(const sData: string): String;
var
  lsDataAux: String;
Begin

  lsDataAux := sData;

  while not DataValida(lsDataAux) do
    lsDataAux := IntToStr(StrToInt(Copy(lsDataAux,1,2)) - 1) + Copy(lsDataAux,3,10);

  Result := lsDataAux;
  
end;
{*----------------------------------------------------------------------
 * TdmDados.CriarSql    Criar um SQL
 *
 *----------------------------------------------------------------------}
function TdmDados.CriarSql(const sSql: string): Integer;
var
  iConta: Integer;
begin
  Result := 0;
  try
    { procura pelo primeiro indice livre }
    for iConta := 1 to MAX_NUM_SQL do
      if gaSql[iConta] = nil then
        break;

    if iConta > MAX_NUM_SQL then
    begin
      MessageDlg('NovaConexao - uniDados.Pas : Ultrapassou limite do vetor ',
                  mtWarning, [mbOK], 0);
      dmDados.giStatus := -1;
      Exit;
    end;
    { criar o SQL }
    gaSql[iConta] := TQuery.Create(Self);
    gaSql[iConta].DatabaseName := gsDBNome;   //DATABASE_NAME;
    gaSql[iConta].SQL.Add(sSql);
    { se nao estiver preparado, preparar }
    if (not gaSql[iConta].Prepared) then
      gaSql[iConta].Prepare;
    gaSql[iConta].Open;
    { guarda a transacao corrente }
    gaSqlTrans[iConta] := giTransacao;
    Result := iConta;
  except
    ErroTratar('CriarSql - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.CriarStored    Executa uma Stored Procedure
 *
 *----------------------------------------------------------------------}
function TdmDados.CriarStored(const psNome: string;
                              const paParams: array of TSParams;
                              const piMaxParam: Integer): Variant;
var
  lspNavega:  TStoredProc;
  liConta:    Integer;
begin
  Result := VAZIO;
  try
    { criar o SQL }
    lspNavega := TStoredProc.Create(Self);
    lspNavega.DatabaseName := gsDBNome;   //DATABASE_NAME;
    lspNavega.StoredProcName := psNome;

    lspNavega.Params.Clear;
    for liConta := 0 to piMaxParam do
      TParam.Create(lspNavega.Params, ptInput);

    for liConta := 0 to piMaxParam do
    begin
      lspNavega.Params[liConta].Name := paParams[liConta].sNome;
      lspNavega.Params[liConta].ParamType := paParams[liConta].ptTipo;
      case paParams[liConta].ftTipoDado of
        ftSmallint, ftInteger:
          lspNavega.Params[liConta].AsInteger := paParams[liConta].vValor;
        ftString:
          lspNavega.Params[liConta].AsString := paParams[liConta].vValor;
        ftDate, ftDateTime:
          lspNavega.Params[liConta].AsDateTime := paParams[liConta].vValor;
        ftCurrency, ftFloat:
          lspNavega.Params[liConta].AsFloat := paParams[liConta].vValor;
        ftBoolean:
          lspNavega.Params[liConta].AsBoolean := paParams[liConta].vValor;
      end;
    end;
    { se nao estiver preparado, preparar }
    if (not lspNavega.Prepared) then
      lspNavega.Prepare;
    lspNavega.ExecProc;
    { retorna o codigo encontrado }
    for liConta := 0 to piMaxParam do
      if (paParams[liConta].ptTipo = ptInputOutput) or (paParams[liConta].ptTipo = ptOutput) then
        Result := lspNavega.Params[liConta].Value
      else
        Result := VAZIO;
    { fechar a stored procedure }
    lspNavega.Close;
    lspNavega.Unprepare;
    lspNavega.Free;
  except
    ErroTratar('CriarStored - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.SqlVersao    Procurar o SQL solicitado e substituir pelos
 *                        parametros correspondentes (@0, @1, @2, etc...)
 *----------------------------------------------------------------------}
function TdmDados.SqlVersao(const sIDSql: string; var aParam: array of Variant): string;
var
  sSql1:      AnsiString; { String auxiliar do texto SQL             }
  sSql2:      AnsiString; { String auxiliar do texto SQL             }
  sSql:      String;

  iIni:      Integer;     { Auxiliar para percorrer a STRING         }
  ifim:      Integer;     { Indice do parametro                      }
  iParmInd:  Integer;     { �ndice para percorrer array de par�metro }
  iCode:     Integer;
  lAscChar:  Longint;     { Valor ascii do caractere                 }
  iConexao:  Integer;
  iIndColTexto: Integer;
  lsValor:  string;
begin
  { sem erro }
  giStatus := STATUS_OK;
  try
    { Se banco nao estava conectado ainda, iniciar }
    if (gdbDados = nil) then
    begin
      gbFLAGIO_Iniciar := False;
      { abrir o banco }
      IniciarBanco;
      { se deu erro }
      if (giStatus <> STATUS_OK) then
      begin
        Result := VAZIO;
        Exit;
      end;
    end;

    sSql := 'SELECT * FROM zSQL WHERE CODIGO =''' + sIDSql + '''';

    iConexao := dmDados.ExecutarSelect(sSql);

    if dmDados.Status(iConexao, IOSTATUS_EOF) then
    { Se nao achou , erro : devolve string vazia }
//    if not gtbSQLs.FindKey([sIDSql]) then
    begin
      Result := VAZIO;
      Exit;
    end
    else
    begin
      iIndColTexto:= IOPTR_NOPOINTER;
      sSql1 := dmDados.ValorColuna(iConexao,'TEXTO',iIndColTexto);
      { guarda a string contendo o texto SQL }
//      sSql1 := gtbSQLs.FieldByName(FLDSQL_TEXTO).AsString;
      iIni  := 1;
      sSql2 := VAZIO;

      { Se achou, verifica se precisa fazer substituicoes }
      iFim := Pos('@', sSql1);
      { enquanto existirem parametros }
      while iFim <> 0 do
      begin
        { guarda o texto ate o proximo parametro }
        sSql2 := sSql2 + Copy(sSql1, iIni, iFim - iIni);
        { se nao ultrapassou o tamanho do texto }
        if (iFim + 2) <= Length(sSql1) then
        begin
          { pega o segundo caracter representando o indice do parametro }
          lAscChar := Ord(PChar(Copy(sSql1, iFim + 2, 1))[0]);
          { se for numerico � maior que 10 }
          if (lAscChar >= Ord('0')) and (lAscChar <= Ord('9')) then
          begin
            { guarda o indice do parametro >= 10 }
            Val(Copy(sSql1, iFim + 1, 2), iParmInd, iCode);
            iIni := iFim + 3;
          end
          else
          begin
            { gaurda o indice do parametro < 10 }
            Val(Copy(sSql1, iFim + 1, 1), iParmInd, iCode);
            iIni := iFim + 2;
          end;
        end
        else
        begin
          { gaurda o indice do parametro < 10 }
          Val(Copy(sSql1, iFim + 1, 1), iParmInd, iCode);
          iIni := iFim + 2;
        end;
        { susbstitui o parametro }
        case VarType(aParam[iParmInd]) of
          varString:
            sSql2 := sSql2 + aParam[iParmInd];
          varDate:
            sSql2 := sSql2 + FormatDateTime(MK_DATATIME, aParam[iParmInd]);
          varInteger:
            sSql2 := sSql2 + IntToStr(aParam[iParmInd]);
          varCurrency, varDouble:
            sSql2 := sSql2 + dmDados.TrataDecimalBD(aParam[iParmInd], IOFLAG_VALORPARABANCO);
          else
          begin
            Str(aParam[iParmInd], lsValor);
            sSql2 := sSql2 + lsValor;
          end;
        end;
//        sSql2 := sSql2 + aParam[iParmInd];
        { procura pelo proximo parametro }
        iFim := Pos('@', Copy(sSql1, iIni, Length(sSql1)));
        { se achou, continua da onde parou }
        if iFim <> 0 then
          iFim := iFim + IIni - 1;
      end;
      { se nao ultrapassou o tamanho do texto }
      if (iIni <= Length(sSql1)) then
      begin
        { acrescenta o texto faltante }
        sSql2 := sSql2 + Copy(sSql1, iIni, Length(sSql1) - iIni + 1);
      end;
    end;
    dmDados.FecharConexao(iConexao);
    Result := sSql2;

  except
    Result := VAZIO;
    ErroTratar('SqlVersao - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.IniciarBanco    Preparar e abrir o banco
 *
 *----------------------------------------------------------------------}
procedure TdmDados.IniciarBanco;
begin
  try
    { sem erro }
    giStatus := STATUS_OK;

    giErroCodigo := ERRO_CODIGODEFAULT;

    { se o banco ainda nao foi aberto }
    if gbFLAGIO_Iniciar = False then
    begin
      gbFLAGIO_Iniciar := True;
      { abrir o banco }
      PrepararBanco;
    end;
  except
    { se a tabela de Sql estiver aberta }
//    if not (gtbSqls = nil) then
//      gtbSqls.Close;
    { se o banco estiver aberto }
    if not (gdbDados = nil) then
      gdbDados.Close;
    MessageDlg('Erro fatal na inicializa��o do sistema.',
                mtWarning, [mbOK], 0);
    ErroTratar('IniciarBanco - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.FinalizarBanco    Fechar as transacoes, as conexoes e o banco
 *
 *----------------------------------------------------------------------}
procedure TdmDados.FinalizarBanco;
var
  lsSql: String;
begin
  try
    { sem erro }
    giStatus := STATUS_OK;

    { fechar as transacoes }
    FinalizarTransacao;

    {apaga os lock}
    lsSql := 'DELETE FROM LOK_Recursos WHERE ID_Processo = ' + IntToStr(giId_Processo);
    lsSql := lsSql + ' AND Rotina = ''' + EstufaString(gsRtn_Atual, '''') + '''';
    ExecutarSql(lsSql);

    { fechar as conexoes }
    FinalizarConexao;
  except
    ErroTratar('FinalizarBanco - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.FecharSql    Fechar um sql
 *
 *----------------------------------------------------------------------}
procedure TdmDados.FecharSql(const iID: Integer);
begin
  try
    { sem erro }
    giStatus := STATUS_OK;

    { se for uma conexao valida }
    if (iID > 0) and (iID <= High(gaSql)) then
    begin
      { fechar o sql }
      gaSql[iID].Close;
      gaSql[iID].Unprepare;
      gaSql[iID].Free;
      gaSql[iID] := nil;
    end;
  except
    ErroTratar('FecharSql - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.FinalizarConexao    Fechar uma conexao
 *
 *----------------------------------------------------------------------}
procedure TdmDados.FinalizarConexao;
var
  iContador:   Integer;  { Para percorrer estrutura de conex�es }
  iConexao:    Integer;
begin
  try
    { sem erro }
    giStatus := STATUS_OK;

    { percorre todas as conexoes }
    for iContador := 1 to High(gaConexao) do
    begin
      { guarda o indice da conexao }
      iConexao := gaConexao[iContador].iConexao;
      { se � uma conexao valida }
      if (iConexao <> IOPTR_NOPOINTER) then
        { fechar a conexao }
        FecharSql(iConexao);
    end;

    { fechar o banco }
    FecharBanco(gdbDados);
  except
    MessageDlg('Tem algum Dynaset/Snapshot aberto. Checar sua l�gica',
                mtWarning, [mbOK], 0);
    ErroTratar('FinalizarConexao - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.PrepararBanco    Abrir o banco e os dicionarios
 *
 *----------------------------------------------------------------------}
procedure TdmDados.PrepararBanco;
begin
  try
    { sem erro }
    giStatus := STATUS_OK;

    { indica que IO j� foi inicializado }
    gbFLAGIO_Iniciar := True;

    giErroCodigo := ERRO_CODIGODEFAULT;

    { inicia variavel global contadora de transacoes }
    giColunasLivres := IOPTR_NOPOINTER;
    giColunasTotal := 0;
    giTransacao := 0;

    If gsDBNome = VAZIO Then
      gsDBNome := DATABASE_NAME;

    { Se o banco n�o estava conectado, abrir }
    if gdbDados = nil then
      gdbDados := AbrirBanco(gsDBNome, VAZIO);

  except
    { se o banco estiver aberto, fechar }
    if not (gdbDados = nil) then
      gdbDados.Close;
    MessageDlg('Erro fatal na inicializa��o do sistema.',
                mtWarning, [mbOK], 0);
    ErroTratar('PreparaBanco - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.FinalizarTransacao    Fechar todas as transacoes
 *
 *----------------------------------------------------------------------}
procedure TdmDados.FinalizarTransacao;
begin
  try
    { sem erro }
    giStatus := STATUS_OK;

    { se tiver transacao aberta }
    if giTransacao > 0 then
      TransRollback(gaTransacoes[1]);
  except
    ErroTratar('FinalizarTransacao - uniDados.PAS');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.ColunasAlloc    Alocar um numero de colunas
 *
 *----------------------------------------------------------------------}
function TdmDados.ColunasAlloc(const iNumCols: Integer): Integer;
var
  iContador: Integer;   { Para percorrer lista livre             }
  iPointer:  Integer;   { Indica o corrente da lista             }
  iUbound:   Integer;   { Indica o tamanho corrente de gaColunas }
  iPrimPtr:  Integer;   { Primeiro da lista ligada               }
  iAnterior: Integer;   { Anterior na lista ligada               }
  bFimLista: Boolean;   { Indica Fim Lista Livre                 }
begin
  try
    iContador := 0;
    iPointer  := giColunasLivres;
    iPrimPtr  := giColunasLivres;
    iAnterior := IOPTR_NOPOINTER;
    bFimLista := False;

    { se ja tiver item na lista }
    if giColunasLivres <> IOPTR_NOPOINTER then
    begin
      { percorre ate a coluna solicitada }
      for iContador := 1 to iNumCols do
      begin
        { se nao tiver proxima coluna }
        if gaColunas[iPointer].iProximaColuna = IOPTR_NOPOINTER then
        begin
          iAnterior := iPointer;
          bFimLista := True;
          break;
        end
        else
        begin
          iAnterior := iPointer;
          iPointer  := gaColunas[iPointer].iProximaColuna;
        end;
      end;
    end;
    { N�o existem livres o suficiente.... }
    if iContador < iNumCols then
    begin
      { guarda o tamanho maximo do vetor }
      iUbound := giColunasTotal;

      { Nao existia nenhum livre ? }
      if iPrimPtr = IOPTR_NOPOINTER then
        iPrimPtr := iUbound + 1;

      { guarda o novo numero de colunas utilizadas }
      giColunasTotal := iUbound + iNumCols - iContador;

      { se achou a coluna }
      if iPointer <> IOPTR_NOPOINTER then
      begin
        gaColunas[iPointer].iProximaColuna := iUbound + 1;
        iAnterior := iPointer;
      end;

      iPointer := iUbound + 1;
      for iContador := iContador + 1 to iNumCols do
      begin
        gaColunas[iPointer].iProximaColuna := iPointer + 1;
        iAnterior := iPointer;
        iPointer  := iPointer + 1;
      end;
      giColunasLivres := IOPTR_NOPOINTER;
    end
    else
    begin
      { existe exatamente a quantidade solicitada }
      if bFimLista then
        giColunasLivres := IOPTR_NOPOINTER
      else
        { existem livres o suficiente }
        giColunasLivres := iPointer;
    end;

    { Aterra o �ltimo da fila }
    gaColunas[iAnterior].iProximaColuna := IOPTR_NOPOINTER;

    Result := iPrimPtr;

  except
    ErroTratar('ColunasAlloc - uniDados.PAS');
    Result := IOPTR_NOPOINTER;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.ColunasInfo    Preenche vetor com informacao das colunas
 *
 *----------------------------------------------------------------------}
function TdmDados.ColunasInfo(var iConexao: Integer;
                               var aColunasInfo: array of TColunaInfo;
                               const iFlag: Integer):string;
var
  wContador:     Word;
  iColuna:       Integer;
  iConexaoAux:   Integer;  { Para guardar a conexao banco }
  bTodasColunas: Boolean;
begin
  try
    { tudo certo ate aqui }
    Result := IORET_OK;

    { pega a ultima conexao }
    iConexao := PegarUltConAssoc(iConexao);

    bTodasColunas := True;
    { percorre todo o vetor }
//    for iContador := 1 to High(aColunasInfo) do
    for wContador := 0 to High(aColunasInfo) do
    begin
      { se alguma ja tiver dados, nao � para preencher todas }
      if (aColunasInfo[wContador].sNome <> VAZIO) or
         (aColunasInfo[wContador].iOrdinalPos <> 0) then
      begin
        bTodasColunas := False;
        break;
      end;
    end;

    { guarda a conexao }
    iConexaoAux := gaConexao[iConexao].iConexao;
    { se forem todas as colunas }
    if bTodasColunas = True then
    begin
      { percorre todas as colunas do sql }
      for wContador := 0 to gaSql[iConexaoAux].FieldDefs.Count - 1 do
      begin
        { monta nome }
        if (iFlag and IOCOL_NAME) = IOCOL_NAME then
          aColunasInfo[wContador].sNome := gaSql[iConexaoAux].FieldDefs.Items[wContador].Name;

        { monta a posicao }
        if (iFlag and IOCOL_ORDINAL) = IOCOL_ORDINAL then
          aColunasInfo[wContador].iOrdinalPos := gaSql[iConexaoAux].FieldDefs.Items[wContador].FieldNo;

        { monta tipo }
        if (iFlag and IOCOL_TYPE) = IOCOL_TYPE then
          aColunasInfo[wContador].ftTipo := gaSql[iConexaoAux].FieldDefs.Items[wContador].DataType;

        { monta valor }
        if (iFlag and IOCOL_VALUE) = IOCOL_VALUE then
        begin
          { se tiver registros e achou o procurado }
          if (not ((gaSql[iConexaoAux].EOF) and (gaSql[iConexaoAux].BOF)) (*and (gaSql[iConexaoAux].Found)*)) then
          begin
            { se for nulo }
            if VarIsNull(gaSql[iConexaoAux].Fields[wContador].Value) then
              { guarda vazio }
              aColunasInfo[wContador].vValor := VAZIO
            else
              { guarda o valor }
              aColunasInfo[wContador].vValor := gaSql[iConexaoAux].Fields[wContador].Value;
          end
          else
          begin
            { guarda sem linhas }
            aColunasInfo[wContador].vValor := IOVAL_NOROWS;
          end;
        end;

        { monta tamanho }
        if (iFlag and IOCOL_SIZE) = IOCOL_SIZE then
          aColunasInfo[wContador].iTamanho := gaSql[iConexaoAux].FieldDefs.Items[wContador].Size;
      end;
    end
    { se nao for todas as colunas }
    else
    begin
      { percorre todas as colunas do vetor }
      for wContador := 0 to High(aColunasInfo) do
      begin
        { se ja tiver o nome ou a posicao }
        if (aColunasInfo[wContador].sNome <> VAZIO) or
           (aColunasInfo[wContador].iOrdinalPos <> 0) then
        begin
          { se tiver o nome }
          if aColunasInfo[wContador].sNome <> VAZIO then
            iColuna := gaSql[iConexaoAux].FieldDefs.Find(aColunasInfo[wContador].sNome).FieldNo
          { se tiver a posicao }
          else
            iColuna := aColunasInfo[wContador].iOrdinalPos;

          { monta nome }
          if (iFlag and IOCOL_NAME) = IOCOL_NAME then
            aColunasInfo[wContador].sNome := gaSql[iConexaoAux].FieldDefs.Items[iColuna - 1].Name;

          { monta posicao }
          if (iFlag and IOCOL_ORDINAL) = IOCOL_ORDINAL then
            aColunasInfo[wContador].iOrdinalPos := gaSql[iConexaoAux].FieldDefs.Items[iColuna - 1].FieldNo;

          { monta tipo }
          if (iFlag and IOCOL_TYPE) = IOCOL_TYPE then
            aColunasInfo[wContador].ftTipo := gaSql[iConexaoAux].FieldDefs.Items[iColuna - 1].DataType;

          { monta valor }
          if (iFlag and IOCOL_VALUE) = IOCOL_VALUE then
          begin
            { se tiver registros e achou o procurado }
            if (not ((gaSql[iConexaoAux].EOF) and (gaSql[iConexaoAux].BOF))) (*and
               (gaSql[iConexaoAux].Found)*) then
            begin
              { se for nulo }
              if VarIsNull(gaSql[iConexaoAux].Fields[iColuna - 1].Value) then
                { guarda vazio }
                aColunasInfo[wContador].vValor := VAZIO
              else
                { guarda o valor }
                aColunasInfo[wContador].vValor := gaSql[iConexaoAux].Fields[iColuna - 1].Value;
            end
            else
              { guarda sem linhas }
              aColunasInfo[wContador].vValor := IOVAL_NOROWS;
          end;

          { monta tamanho }
          if (iFlag and IOCOL_SIZE) = IOCOL_SIZE then
            aColunasInfo[wContador].iTamanho := gaSql[iConexaoAux].FieldDefs.Items[iColuna - 1].Size;
        end;
      end;
    end;

  except
    ErroTratar('ColunasInfo - uniDados.PAS');
    FecharConexao(iConexao);
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.ValorColuna    Devolve o conteudo da coluna
 *
 *----------------------------------------------------------------------}
function TdmDados.ValorColuna(var iConexao: Integer;
                               const sColuna: string;
                               var iIndexColuna: Integer): Variant;
var
  iPtrConexao: Integer;
  iIndex:      Integer;
  vValor:      Variant;
  ftTipo:      TFieldType;
begin
  try
    { pega a ultima conexao }
    iConexao := PegarUltConAssoc(iConexao);

    { se as colunas estao invalidas }
    if gaConexao[iConexao].bInvalColunas then
    begin
      { se tem registros }
      if gaConexao[iConexao].bTemLinhas then
      begin
        giErroConexao := IOERR_NOCURRENTRECORD;
        ErroTratar('ValorColuna - uniDados.PAS');
        Exit;
      end
      else
      begin
        Result := IOVAL_NOVALUE;
        Exit;
      end;
    end;

    { guarda a conexao }
    iPtrConexao := gaConexao[iConexao].iConexao;

    { nao tem valor de coluna alterado }
    if not gaConexao[iConexao].bTemAtualizacao then
    begin
      { tem registro valido }
      if gaConexao[iConexao].bPosicionadoEmLinha then
      begin
        { guarda o indice da coluna }
//        if iIndexColuna = IOPTR_NOPOINTER then
        if sColuna <> VAZIO then
          iIndexColuna := gaSql[iPtrConexao].FieldDefs.Find(sColuna).FieldNo;

        { guarda o tipo da coluna }
        ftTipo := gaSql[iPtrConexao].FieldDefs.Items[iIndexColuna - 1].DataType;
        { guarda o valor da coluna }
        vValor := gaSql[iPtrConexao].Fields[iIndexColuna - 1].AsVariant;
        { se nao for nulo }
        if not VarIsNull(vValor) then
        begin
          Result := vValor;
        end
        { se for nulo }
        else
        begin
          { se nao for numerico }
          if (ftTipo = ftString) or (*(ftTipo = ftDate) or (ftTipo = ftDateTime) or*)
             (ftTipo = ftMemo) or (ftTipo = ftBlob) then
          begin
            Result := VAZIO;
          end
          { se for numerico ou data }
          else
          begin
            Result := 0;
          end;
        end;
      end
      { nao tem registro valido }
      else
      begin
        Result := IOVAL_NOVALUE;
      end;
      Exit;
    end
    { tem valor de coluna alterado }
    else
    begin
      iIndex := gaConexao[iConexao].iPtrColunas;

      { enquanto nao for a coluna solicitada }
      while gaColunas[iIndex].sNomeColuna <> sColuna do
      begin
        { procura na lista de colunas }
        iIndex := gaColunas[iIndex].iProximaColuna;
        if (iIndex > High(gaColunas)) or (iIndex = IOPTR_NOPOINTER) then
        begin
          { se tem registro valido }
          if gaConexao[iConexao].bPosicionadoEmLinha then
          begin
            { guarda o valor }
            vValor := gaSql[iPtrConexao].FieldByName(sColuna).AsVariant;
            { se for nulo }
            if VarIsNull(vValor) then
            begin
              Result := VAZIO;
            end
            else
            begin
              Result := vValor;
            end;
          end
          { se nao tem registro valido }
          else
          begin
            Result := IOVAL_NOVALUE;
          end;
          Exit;
        end;
      end;

      { retorna valor }
      Result := gaColunas[iIndex].vValor;
    end;
  except
    ErroTratar('ValorColuna - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Excluir    Excluir um registro de uma conexao
 *
 *----------------------------------------------------------------------}
function TdmDados.Excluir(var iConexao: Integer): string;
var
  sSql: string;
begin
  try
    { assume que deu erro }
    Result := IORET_FAIL;

    { guarda a ultima conexao }
    iConexao := PegarUltConAssoc(iConexao);

    { constroi o sql de exclusao }
    sSql := ExcluirStr(iConexao, VAZIO);
    { se falhou ou texto do sql vazio }
    if (sSql = IORET_FAIL) or (sSql = VAZIO) then
    begin
      Result := IORET_FAIL;
      Exit;
    end;

    { executa a exclusao }
    Result := ExecutarSql(sSql);
    { limpa as Colunas }
    LimparColunas(gaConexao[iConexao].iPtrColunas);
    { limpa controles da conexao }
    gaConexao[iConexao].iPtrColunas := IOPTR_NOPOINTER;
//    gaConexao[iConexao].bRefazSQL := True;
    gaConexao[iConexao].bPosicionadoEmLinha := False;
    gaConexao[iConexao].bInvalColunas := True;
    gaConexao[iConexao].bTemAtualizacao := False;
    gaConexao[iConexao].iModoOperacao := IOMODE_EDIT;

  except
    ErroTratar('Excluir - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.ExcluirStr    Montar o sql de exclusao
 *
 *----------------------------------------------------------------------}
function TdmDados.ExcluirStr(var iConexao: Integer;
                             const sTabela: string): string;
var
  iContador:    Integer;
  sSql:         string;
  aChaveItem:   array[0..MAX_NUM_COLUNAS] of string;
  iChaveMaxInd: Integer;
  aColunasInfo: array[0..MAX_NUM_COLUNAS] of TColunaInfo;
  sTabelaAux:   string;
  DBInfo:       TDBInfo;
begin
  try
    { assume que deu erro }
    Result := IORET_FAIL;

    { Inicializa variaveis}
    for iContador := 0 to MAX_NUM_COLUNAS do
    begin
      aColunasInfo[iContador].iOrdinalPos := 0;
      aColunasInfo[iContador].ftTipo := ftUnknown;
      aColunasInfo[iContador].iTamanho := 0;
    end;

    { se tiver o nome da tabela }
    if sTabela <> VAZIO then
      sTabelaAux := sTabela
    else
      sTabelaAux := gaConexao[iConexao].sTabela;

    { Se no ExecutarSelect n�o conseguir obter o nome da tabela, n�o � poss�vel }
    { executar DELETE }
    if sTabelaAux = IORET_FAIL then
    begin
      gsStatusMensagem := 'Tabela n�o identific�vel no SQL';
      ErroTratar('ExcluirStr - uniDados.Pas');
      Result := IORET_FAIL;
      Exit;
    end;

    { montar o sql }
    sSql := 'DELETE FROM ' + sTabelaAux + ' WHERE ';

    { se os campos de indice estiver vazio }
    if gaConexao[iConexao].sIndexCampos = VAZIO then
    begin
      DBInfo.sTabelaNome := sTabelaAux;
      { guarda os campos de indice }
      gaConexao[iConexao].sIndexCampos := CamposIndices(DBInfo, 'PrimaryKey');
    end;

    { desmembra chaves }
    GuardarIndices(gaConexao[iConexao].sIndexCampos, aChaveItem, iChaveMaxInd);

    { se tiver chaves de indice }
    if (iChaveMaxInd > 0) or ((iChaveMaxInd = 0) and (aChaveItem[0] <> VAZIO)) then
    begin
      { guarda em aColunasInfo o Tipo e o Valor de cada coluna da chave }
      for iContador := 0 to iChaveMaxInd do
        aColunasInfo[iContador].sNome := aChaveItem[iContador];

      { guarda as informacoes de cada coluna }
      gsConexaoRetorno := ColunasInfo(iConexao, aColunasInfo, IOCOL_ALL);
      { se falhou }
      if gsConexaoRetorno = IORET_FAIL then
      begin
        Result := IORET_FAIL;
        Exit;
      end;

      { percorre todas as colunas da chave }
      for iContador := 0 to iChaveMaxInd do
      begin
        { se na � a primeira coluna }
        if iContador > 0 then
          sSql := sSql + ' AND ';

        { monta o nome }
        sSql := sSql + aColunasInfo[iContador].sNome;
        { monta o valor dependendo do tipo }
        case aColunasInfo[iContador].ftTipo of
          ftString:
            sSql := sSql + ' = ''' + EstufaString(aColunasInfo[iContador].vValor, '''') + '''';
          ftDate, ftDateTime:
            sSql := sSql + ' = ''' + FormatDateTime(MK_DATATIME, aColunasInfo[iContador].vValor) + '''';
          ftSmallint, ftInteger:
            sSql := sSql + ' = ' + IntToStr(aColunasInfo[iContador].vValor);
          ftCurrency, ftFloat:
            sSql := sSql + ' = ' + TrataDecimalBD(aColunasInfo[iContador].vValor, IOFLAG_VALORPARABANCO);
          ftBytes:
            sSql := sSql + ' = ''' + aColunasInfo[iContador].vValor + '''';
          ftBoolean:
          begin
            if aColunasInfo[iContador].vValor then
              sSql := sSql + ' = 1'//' = True'
            else
              sSql := sSql + '= 0';//' = False';
          end;
          else
          begin
            sSql := sSql + ' = ' + VarToStr(aColunasInfo[iContador].vValor);
          end;
        end;
      end;
    end;

    Result := sSql;

  except
    ErroTratar('ExcluirStr - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.ExecutarSelect    Executar um sql do tipo SELECT
 *
 *----------------------------------------------------------------------}
function TdmDados.ExecutarSelect(const sSql: string): Integer;
var
  iConexao:   Integer;
  iIndiceSql: Integer;
begin
  try
    { assume que deu erro }
    Result := IOPTR_NOPOINTER;
    { se o texto do sql estiver vazio }
    if sSql = VAZIO then
    begin
      MessageDlg('ExecutarSelect - uniDados.Pas : Sql vazio ',
                  mtWarning, [mbOK], 0);
      Exit;
    end;

    { procura por nova conexao }
    iConexao := NovaConexao();
    { abre o sql }
    iIndiceSql := CriarSql(sSql);
    { erro na abertura do sql }
    if iIndiceSql = 0 then
    begin
      Result := IOPTR_NOPOINTER;
      Exit;
    end
    { se nao deu erro, guarda a conexao }
    else
    begin
      gaConexao[iConexao].iConexao := iIndiceSql;
    end;

    { atualiza dados da conexao }
    gaConexao[iConexao].sSql := sSql;
    gaConexao[iConexao].sTabela := SQLTabelaNome(sSql);
//    if (UpperCase(Copy(gaConexao[iConexao].sTabela,1,2)) = SIGLA_TABELAS) or
//       (UpperCase(Copy(gaConexao[iConexao].sTabela,1,3)) = SIGLA_SEGURANCA) then
    gaConexao[iConexao].sIndexCampos := TabelaPrimaryKey(gaConexao[iConexao].sTabela);
    gaConexao[iConexao].bFimLinhas := False;
    gaConexao[iConexao].bTemLinhas := True;
    gaConexao[iConexao].bPosicionadoEmLinha := True;
    gaConexao[iConexao].bInvalColunas := False;
    gaConexao[iConexao].iTransacao := giTransacao;
    gaConexao[iConexao].iTransConAssociada := IOPTR_NOPOINTER;

    { se for final de arquivo }
    if gaSql[iIndiceSql].EOF then
    begin
      gaConexao[iConexao].bTemLinhas := False;
      gaConexao[iConexao].bFimLinhas := True;
      gaConexao[iConexao].bPosicionadoEmLinha := False;
    end;
    Result := iConexao;
  except
    ErroTratar('ExecutarSelect - uniDados.PAS');
    FecharConexao(iConexao);
    Result := IOPTR_NOPOINTER;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.ExecutarSQL    Executar um sql de a��o
 *
 *----------------------------------------------------------------------}
function TdmDados.ExecutarSQL(const sSql: string): string;
var
  tqSql:  TQuery;
begin
  try
    { ate aqui tudo certo }
    Result := IORET_OK;

    { se o sql estiver vazio }
    if sSql = VAZIO then
    begin
      MessageDlg('ExecutarSql - uniDados.Pas : Sql vazio ',
                  mtWarning, [mbOK], 0);
      Exit;
    end;

    { cria e executa o sql }
    tqSql := TQuery.Create(Self);
    tqSql.DatabaseName := gsDBNome;   //DATABASE_NAME;
    tqSql.SQL.Add(sSql);
    tqSql.ExecSQL;
    tqSql.Close;
    tqSql.Free;
    tqSql := nil;

  except
    ErroTratar('ExecutarSQL - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Primeiro    Vai para o primeiro registro
 *
 *----------------------------------------------------------------------}
function TdmDados.Primeiro(var iConexao: Integer): string;
var
  iIntConexao: Integer;
begin
  try
    { assume que deu tudo certo }
    Result := IORET_OK;

    { guarda a ultima conexao }
    iConexao := PegarUltConAssoc(iConexao);

    { se precisa refazer o sql }
    if gaConexao[iConexao].bRefazSQL = True then
    begin
      { se falhou no refresh }
      if Atualizar(iConexao) = IORET_FAIL then
      begin
        ErroTratar('Primeiro - uniDados.Pas');
        Result := IORET_FAIL;
        Exit;
      end;
    end;

    gaConexao[iConexao].bFimLinhas := False;
    gaConexao[iConexao].bTemLinhas := True;
    gaConexao[iConexao].bPosicionadoEmLinha := True;

    iIntConexao := gaConexao[iConexao].iConexao;
    gsConexaoRetorno := RecallColumns(iConexao);

    { se nao tiver registros }
    if ((gaSql[iIntConexao].EOF) and (gaSql[iIntConexao].BOF)) then
    begin
      gaConexao[iConexao].bFimLinhas := True;
      gaConexao[iConexao].bTemLinhas := False;
      gaConexao[iConexao].bPosicionadoEmLinha := False;
      Result := IORET_NOROWS;
    end
    else
    begin
      { se tiver filtro }
      if gaConexao[iConexao].sFind <> VAZIO then
      begin
        { se nao achou }
        if not gaSql[iIntConexao].FindFirst then
        begin
          Result := IORET_EOF;
          gaConexao[iConexao].bFimLinhas := True;
          gaConexao[iConexao].bTemLinhas := False;
          gaConexao[iConexao].bPosicionadoEmLinha := False;
          Exit;
        end;
      end
      else
      begin
        { posiciona no primeiro registro }
        gaSql[iIntConexao].First;
        { se for final de arquivo }
        if gaSql[iIntConexao].EOF then
        begin
          Result := IORET_EOF;
          gaConexao[iConexao].bFimLinhas := True;
          gaConexao[iConexao].bTemLinhas := False;
          gaConexao[iConexao].bPosicionadoEmLinha := False;
          Exit;
        end;
      end;
    end;

    gaConexao[iConexao].bTemAtualizacao := False;
    gaConexao[iConexao].iModoOperacao := IOMODE_EDIT;

  except
    ErroTratar('Primeiro - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.PegarConEmTransacao    Retorna conexao em transacao
 *
 *----------------------------------------------------------------------}
function TdmDados.PegarConEmTransacao(var iConexao: Integer): Integer;
var
  iIntConexao: Integer;
begin
  try
    repeat
      { se nao tiver transacao pendente }
      if gaConexao[iConexao].iTransacao < giTransacao then
      begin
        { se tiver transacao associada }
        if (gaConexao[iConexao].iTransConAssociada <> IOPTR_NOPOINTER) and
           (gaConexao[iConexao].iTransConAssociada <> 0) then
        begin
          iConexao := gaConexao[iConexao].iTransConAssociada;
        end
        { se nao tiver transacao associada }
        else
        begin
          { execurtar sql }
          iIntConexao := ExecutarSelect(gaConexao[iConexao].sSql);
          { se nao tem }
          if iIntConexao = IOPTR_NOPOINTER then
          begin
            Result := IOPTR_NOPOINTER;
            Exit;
          end;
          gaConexao[iConexao].iTransConOrigem := iConexao;
          gaConexao[iConexao].iTransConAssociada := iIntConexao;
          iConexao := iIntConexao;
          break;
        end;
      end
      { se tiver transacao pendente }
      else
      begin
        { fechar a sql }
        FecharSql(gaConexao[iConexao].iConexao);
        { criar o sql }
        gaConexao[iConexao].iConexao := CriarSql(gaConexao[iConexao].sSql);
        break;
      end;
    until True;

    Result := iConexao;

  except
    ErroTratar('PegarConEmTransacao - uniDados.Pas');
    Result := IOPTR_NOPOINTER;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.PegarDataAssoc    Retornar uma data associada
 *
 *----------------------------------------------------------------------}
function TdmDados.PegarDataAssoc(const iIndexCol: Integer): Variant;
var
  sNomeColuna: string;
  iIndexAux:   Integer;
  bPM:         Boolean;
  vDataAux:    Variant;
  dtData:      TDateTime;
begin
  try
    { guarda o nome da coluna }
    sNomeColuna := gaColunas[iIndexCol].sNomeColuna;
    iIndexAux   := iIndexCol;

    { se for nulo ou vazio }
    if (VarIsNull(gaColunas[iIndexCol].vValor)) (*or (gaColunas[iIndexCol].vValor = VAZIO)*) then
    begin
      Result := VAZIO;
      Exit;
    end
    else
    begin
      Result := gaColunas[iIndexCol].vValor;
    end;

    { Se valor foi atualizado, assume o novo... }
    if gaColunas[iIndexCol].bAtualizado = True then
      Exit;

    { se nao tiver colunas associadas }
    if gaColunas[iIndexCol].iColunaAssoc = IOPTR_NOPOINTER then
    begin
      { enquanto nao for a data associada }
      while gaColunas[iIndexAux].sNomeColuna <> (CTEDB_DATE_PREFIX + sNomeColuna) do
      begin
        { guarda a proxima coluna }
        iIndexAux := gaColunas[iIndexAux].iProximaColuna;
        { se for maior ou nao tiver }
        if (iIndexAux > giColunasTotal) or (iIndexAux = IOPTR_NOPOINTER) then
        begin
          gaColunas[iIndexCol].iColunaAssoc := -1;
          break;
        end;
        gaColunas[iIndexCol].iColunaAssoc := iIndexAux;
      end
    end;

    { se a coluna associada for valida }
    if gaColunas[iIndexCol].iColunaAssoc <> -1 then
    begin
      { guarda a coluna associada }
      iIndexAux := gaColunas[iIndexCol].iColunaAssoc;
      { se for valida e nao for a coluna atual }
      if (iIndexAux <> IOPTR_NOPOINTER) and (iIndexAux <> iIndexCol) then
      begin
        { se for nulo }
        if VarIsNull(gaColunas[iIndexAux].vValor) then
        begin
          Result := VAZIO;
        end
        else
        begin
          bPM := False;
          { se for horario americano apos meio dia }
          if Copy(gaColunas[iIndexAux].vValor, Length(gaColunas[iIndexAux].vValor) - 1, 2) = 'PM' then
            bPM := True;

          { se for meia noite }
          if Copy(vDataAux, Length(vDataAux) - 7, 8) = '12:00:00' then
            vDataAux := Copy(vDataAux, Length(vDataAux) - 8, 8) + '00:00:00';

          { converte para data }
          dtData := StrToDate(vDataAux);
          { se for horario americano, soma 12 horas }
          if bPM then
            dtData := dtData + 12;
          Result := dtData;
        end;

        gaColunas[iIndexCol].iColunaAssoc := iIndexAux;
      end;
    end;

  except
    ErroTratar('PegarDataAssoc - uniDados.Pas');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.PegarUltConAssoc    Retorna a ultima conexao associada
 *
 *----------------------------------------------------------------------}
function TdmDados.PegarUltConAssoc(const iConexao: Integer): Integer;
begin
  try
    { se nao tiver conexao }
    if iConexao = IOPTR_NOPOINTER then
    begin
      Result := IOPTR_NOPOINTER;
      Exit;
    end;

    { se a transacao associada nao for valida }
    if (gaConexao[iConexao].iTransConAssociada = IOPTR_NOPOINTER) or
       (gaConexao[iConexao].iTransConAssociada = 0) then
    begin
      Result := iConexao;
    end
    { se tiver }
    else
    begin
      { continua procurando }
      Result := PegarUltConAssoc(gaConexao[iConexao].iTransConAssociada);
    end;

  except
    ErroTratar('PegarUltConAssoc - uniDados.Pas');
    Result := IOPTR_NOPOINTER;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.CamposIndices    Retorna os campos chaves
 *
 *----------------------------------------------------------------------}
function TdmDados.CamposIndices(const aDBInfo: TDBInfo;
                                 const sIndex: string): string;
var
  iConexao: Integer;    { Para guardar a conexao temporaria }
  tbTabela: TTable;
  lsRetorno : String;
begin
  try
    lsRetorno := VAZIO;

    Result := lsRetorno;

    { se nao tem o nome do banco, cria a tabela e retorna as chaves }
    if aDBInfo.sDBNome = VAZIO then
    begin
      tbTabela := TTable.Create(Self);
      //tbTabela.DatabaseName := DATABASE_NAME;
      tbTabela.DatabaseName := gsDBNome;
      tbTabela.TableName := aDBInfo.sTabelaNome;
      tbTabela.Open;
      tbTabela.IndexDefs.Update;
      if tbTabela.IndexDefs.Count > 0 then
        Result := tbTabela.IndexDefs.Items[tbTabela.IndexDefs.IndexOf(sIndex)].Fields;
      tbTabela.Free;
    end;

  except
    ErroTratar('CamposIndices - uniDados.Pas');
    Result := IORET_FAIL;
    FecharConexao(iConexao);
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Incluir    Incluir um registro na tabela
 *
 *----------------------------------------------------------------------}
function TdmDados.Incluir(var iConexao: Integer;
                          const sTabela: string): string;
var
  sSql: string;
begin
  try
    { assume que deu erro }
    Result := IORET_FAIL;

    { guarda a ultima conexao }
    iConexao := PegarUltConAssoc(iConexao);

    { se nao for para inserir }
    if gaConexao[iConexao].iModoOperacao <> IOMODE_INSERT then
    begin
      giErroConexao := IOERR_INSERTSEMSETMODE;
      Result := IORET_FAIL;
      Exit;
    end;

    { Se no ExecutarSelect n�o conseguir obter o nome da tabela, n�o � poss�vel }
    { executar INSERT }
    if gaConexao[iConexao].sTabela = IORET_FAIL then
    begin
      Result := IORET_FAIL;
      Exit;
    end;
    if gaConexao[iConexao].bTemAtualizacao = False then
    begin
      Result := IORET_OK;
      Exit;
    end;

    { pega o sql para inclusao }
    sSql := IncluirStr(iConexao, sTabela);
    { se falhou ou vazio }
    if (sSql = IORET_FAIL) or (sSql = VAZIO) then
    begin
      Result := IORET_FAIL;
      Exit;
    end;

    { executa o sql de inclusao }
    Result := ExecutarSQL(sSql);

    { limpa as Colunas }
    LimparColunas(gaConexao[iConexao].iPtrColunas);
    { atualiza a conexao }
    gaConexao[iConexao].iPtrColunas := IOPTR_NOPOINTER;
//    gaConexao[iConexao].bRefazSQL := True;
    gaConexao[iConexao].bPosicionadoEmLinha := False;
    gaConexao[iConexao].bInvalColunas := True;
    gaConexao[iConexao].bTemAtualizacao := False;
    gaConexao[iConexao].iModoOperacao := IOMODE_EDIT;

  except
    ErroTratar('Incluir - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.IncluirStr    Retorna o texto sql para inclusao
 *
 *----------------------------------------------------------------------}
function TdmDados.IncluirStr(const iConexao:Integer;
                              const sTabela: string): string;
var
  iContador:    Integer;
  iConta:       Integer;
  iPointerCols: Integer;
  sSql:         string;
  sTabelaAux:   string;
  aColuna:      array[0..MAX_NUM_COLUNAS] of string;
  aValor:       array[0..MAX_NUM_COLUNAS] of Variant;
  aTipo:        array[0..MAX_NUM_COLUNAS] of TFieldType;
begin
  try
    { assume que deu erro }
    Result := IORET_FAIL;
    sSql := VAZIO;

    { se a tabela nao estiver vazia }
    if sTabela <> VAZIO then
      sTabelaAux := sTabela
    else
      sTabelaAux := gaConexao[iConexao].sTabela;

    { guarda o ponteiro das colunas }
    iPointerCols := gaConexao[iConexao].iPtrColunas;
    iContador := 0;
    { enquanto nao tiver }
    while iPointerCols <> IOPTR_NOPOINTER do
    begin
      { se o valor da coluna foi modificado }
      if gaColunas[iPointerCols].bAtualizado = True then
      begin
        { guarda o nome da coluna }
        aColuna[iContador] := gaColunas[iPointerCols].sNomeColuna;
        { Se for campo tipo data, busca valor em coluna associada caso existir }
        if (gaColunas[iPointerCols].ftTipoColuna = ftDate) or
           (gaColunas[iPointerCols].ftTipoColuna = ftDateTime) then
          aValor[iContador] := PegarDataAssoc(iPointerCols)
        else
          aValor[iContador] := gaColunas[iPointerCols].vValor;

        { guarda o tipo da coluna }
        aTipo[iContador] := gaColunas[iPointerCols].ftTipoColuna;
        iContador := iContador + 1;
      end;
      { guarda a proxima coluna }
      iPointerCols := gaColunas[iPointerCols].iProximaColuna;
    end;

    { Se n�o tem colunas a ser atualizadas, sai da rotina }
    if (iContador = 0) and (aColuna[0] = VAZIO) then
    begin
      Result := IORET_OK;
      Exit;
    end;

    { Monta cl�usula e nome das colunas a serem inseridas }
    sSql := 'INSERT INTO ' + sTabelaAux + ' (';
    for iConta := 0 to iContador - 1 do
    begin
      if iConta > 0 then
        sSql := sSql + ', ';
      sSql := sSql + aColuna[iConta];
    end;

    { Monta valores das colunas a serem inseridas }
    sSql := sSql + ') ' + CTEDB_VALUES_INI + ' ';
    for iConta := 0 to iContador - 1 do
    begin
      if iConta > 0 then
        sSql := sSql + ', ';
      if (Trim(aValor[iConta]) = VAZIO) or (Trim(aValor[iConta]) = VAZIO_DATA) or (VarIsNull(aValor[iConta])) then
      begin
        sSql := sSql + 'Null';
      end
      else
      begin
        case aTipo[iConta] of
          ftString, ftMemo, ftBlob:
          begin
            { procura por apostrofe }
            aValor[iConta] := EstufaString(aValor[iConta], '''');
            sSql := sSql + '''' + aValor[iConta] + '''';
          end;
          ftDate, ftDateTime:
            sSql := sSql + CTEDB_DATETIME_INI + FormatDateTime(MK_DATATIME, aValor[iConta]) + CTEDB_DATETIME_FIM;
          ftSmallint, ftInteger:
            sSql := sSql + IntToStr(aValor[iConta]);
          ftCurrency, ftFloat:
            sSql := sSql + TrataDecimalBD(aValor[iConta], IOFLAG_VALORPARABANCO);
          ftBytes:
            sSql := sSql + CTEDB_BINARY_INI + aValor[iConta] + CTEDB_Binary_FIM;
          ftBoolean:
          begin
            if aValor[iConta] then
              sSql := sSql + '1'//'True'
            else
              sSql := sSql + '0';//'False';
          end;
          else
            sSql := sSql + VarToStr(aValor[iConta]);
        end;
      end;
    end;
    sSql := sSql + CTEDB_VALUES_FIM;
    Result := sSql;

  except
    ErroTratar('IncluirStr - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Ultimo    Posiciona no ultimo registro
 *
 *----------------------------------------------------------------------}
function TdmDados.Ultimo(var iConexao: Integer): string;
var
  iIntConexao: Integer;  { Para guardar a conexao }
begin
  try
    Result := IORET_OK;

    iConexao := PegarUltConAssoc(iConexao);

    if gaConexao[iConexao].bRefazSQL = True then
    begin
      if Atualizar(iConexao) = IORET_FAIL then
      begin
        ErroTratar('Ultimo - uniDados.Pas');
        FecharConexao(iConexao);
        Result := IORET_FAIL;
        Exit;
      end;
    end;

    iIntConexao := gaConexao[iConexao].iConexao;
    gsConexaoRetorno := RecallColumns(iConexao);

    if ((gaSql[iIntConexao].EOF) and (gaSql[iIntConexao].BOF)) then
    begin
      Result := IORET_EOF;
      gaConexao[iConexao].bPosicionadoEmLinha := False;
    end
    else
    begin
      if gaConexao[iConexao].sFind <> VAZIO then
      begin
        if not gaSql[iIntConexao].FindLast then
        begin
          Result := IORET_EOF;
          gaConexao[iConexao].bPosicionadoEmLinha := False;
          Exit;
        end;
      end
      else
      begin
        gaSql[iIntConexao].Last;
        gaConexao[iConexao].bPosicionadoEmLinha := True;
      end;
    end;

    gaConexao[iConexao].bTemAtualizacao := False;
    gaConexao[iConexao].iModoOperacao := IOMODE_EDIT;

  except
    ErroTratar('Ultimo - uniDados.Pas');
    FecharConexao(iConexao);
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.UltimoRegistro    Pega o valor do ultimo registro
 *
 *----------------------------------------------------------------------}
function TdmDados.UltimoRegistro(var iConexao: Integer;
                                 const sCampo: string): string;
var
  iIntConexao: Integer;  { Para guardar a conexao }
  iMaxConexao: Integer;
  iIndColuna: Integer;
  lsSQL: string;
begin
  try
    Result := IORET_OK;

    iConexao := PegarUltConAssoc(iConexao);

    if gaConexao[iConexao].bRefazSQL = True then
    begin
      if Atualizar(iConexao) = IORET_FAIL then
      begin
        ErroTratar('UltimoRegistro - uniDados.Pas');
        FecharConexao(iConexao);
        Result := IORET_FAIL;
        Exit;
      end;
    end;

    iIntConexao := gaConexao[iConexao].iConexao;
    gsConexaoRetorno := RecallColumns(iConexao);

    iIndColuna := IOPTR_NOPOINTER;
    lsSQL := 'SELECT MAX(' + sCampo + ') As CODIGO_INT FROM ' + SQLTabelaNome(gaConexao[iConexao].sSql);
    iMaxConexao := ExecutarSelect(lsSQL);
    if iMaxConexao <> IOPTR_NOPOINTER then
      Result := ValorColuna(iMaxConexao, 'CODIGO_INT', iIndColuna);
    FecharConexao(iMaxConexao);

  except
    ErroTratar('UltimoRegistro - uniDados.Pas');
    FecharConexao(iConexao);
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.NovaConexao    Retorna uma nova conexao
 *
 *----------------------------------------------------------------------}
function TdmDados.NovaConexao: Integer;
var
  iConexao:  Integer;
  iContador: Integer;
begin
  iConexao := IOPTR_NOPOINTER;
  try
    Result := IOPTR_NOPOINTER;

    for iContador := 1 to High(gaConexao) do
    begin
      if gaConexao[iContador].bEmUso = False then
      begin
        iConexao := iContador;
        break;
      end;
    end;
    if iContador > High(gaConexao) then
    begin
      MessageDlg('NovaConexao - uniDados.Pas : Ultrapassou limite do vetor ',
                  mtWarning, [mbOK], 0);
      dmDados.giStatus := -1;
      Exit;
    end;
    gaConexao[iConexao].bEmUso := True;
    gaConexao[iConexao].bTemAtualizacao := False;
    gaConexao[iConexao].iPtrColunas := IOPTR_NOPOINTER;
    gaConexao[iConexao].iConexao := IOPTR_NOPOINTER;
    gaConexao[iConexao].sSql := VAZIO;
    gaConexao[iConexao].sFind := VAZIO;
    gaConexao[iConexao].bTemLinhas := True;
    gaConexao[iConexao].bFimLinhas := False;
    gaConexao[iConexao].bAchouLinha := False;
    gaConexao[iConexao].iTransConAssociada := IOPTR_NOPOINTER;

    Result := iConexao;

  except
    ErroTratar('NovaConexao - uniDados.Pas');
    if iConexao <> IOPTR_NOPOINTER then
      gaConexao[iConexao].bEmUso := False;
    Result := IOPTR_NOPOINTER;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Proximo    Posiciona no proximo registro
 *
 *----------------------------------------------------------------------}
function TdmDados.Proximo(var iConexao: Integer): string;
var
  iIntConexao: Integer; { Para guardar a conexao }
begin
  try
    Result := IORET_OK;

    iConexao := PegarUltConAssoc(iConexao);

    if gaConexao[iConexao].bRefazSQL = True then
    begin
      if Atualizar(iConexao) = IORET_FAIL then
      begin
        ErroTratar('Proximo - MuniDados.Pas');
        Result := IORET_FAIL;
        FecharConexao(iConexao);
        Exit;
      end;
      iConexao := PegarUltConAssoc(iConexao);
    end;

    iIntConexao := gaConexao[iConexao].iConexao;
    gsConexaoRetorno := RecallColumns(iConexao);

    if ((gaSql[iIntConexao].EOF) and (gaSql[iIntConexao].BOF)) then
    begin
      Result := IORET_EOF;
      gaConexao[iConexao].bFimLinhas := True;
      gaConexao[iConexao].bTemLinhas := False;
      gaConexao[iConexao].bPosicionadoEmLinha := False;
    end
    else
    begin
      if gaConexao[iConexao].sFind <> VAZIO then
      begin
        if not gaSql[iIntConexao].FindNext then
        begin
          Result := IORET_EOF;
          gaConexao[iConexao].bFimLinhas := True;
          gaConexao[iConexao].bPosicionadoEmLinha := False;
          Exit;
        end;
      end
      else
      begin
        gaSql[iIntConexao].Next;
        if gaSql[iIntConexao].EOF then
        begin
          Result := IORET_EOF;
          gaConexao[iConexao].bFimLinhas := True;
          gaConexao[iConexao].bPosicionadoEmLinha := False;
          Exit;
        end;
      end;
    end;

    gaConexao[iConexao].bPosicionadoEmLinha := True;
    gaConexao[iConexao].bTemAtualizacao := False;
    gaConexao[iConexao].iModoOperacao := IOMODE_EDIT;

  except
    ErroTratar('Proximo - MuniDados.Pas');
    Result := IORET_FAIL;
    FecharConexao(iConexao);
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Anterior    Posiciona no registro anterior
 *
 *----------------------------------------------------------------------}
function TdmDados.Anterior(var iConexao: Integer;
                            const iQtdeLinhas: Integer): string;
var
  iContador:    Integer;
  iIntConexao:  Integer;  { Para guardar a conexao }
begin
  try
    Result := IORET_OK;

    { S� liberar quando o povo der OK }
    { Verifica se j� tinha chamado a ALLOWPREVIOUS antes }
    iConexao := PegarUltConAssoc(iConexao);

    if gaConexao[iConexao].bRefazSQL = True then
    begin
      if Atualizar(iConexao) = IORET_FAIL then
      begin
        ErroTratar('Anterior - uniDados.Pas');
        Result := IORET_FAIL;
        FecharConexao(iConexao);
        Exit;
      end;
    end;

    iIntConexao := gaConexao[iConexao].iConexao;
    gsConexaoRetorno := RecallColumns(iConexao);

    if ((gaSql[iIntConexao].EOF) and (gaSql[iIntConexao].BOF)) then
    begin
      Result := IORET_EOF;
      gaConexao[iConexao].bFimLinhas := True;
      gaConexao[iConexao].bTemLinhas := False;
      gaConexao[iConexao].bPosicionadoEmLinha := False;
    end
    else
    begin
      if gaConexao[iConexao].sFind <> VAZIO then
      begin
        if not gaSql[iIntConexao].FindPrior then
        begin
          Result := IORET_BOF;
          gaConexao[iConexao].bFimLinhas := True;
          gaConexao[iConexao].bTemLinhas := False;
          gaConexao[iConexao].bPosicionadoEmLinha := False;
          Exit;
        end;
      end
      else
      begin
        for iContador := 1 to iQtdeLinhas do
        begin
          gaSql[iIntConexao].Prior;
          if gaSql[iIntConexao].BOF then
          begin
            Result := IORET_BOF;
            gaConexao[iConexao].bPosicionadoEmLinha := False;
            Exit;
          end;
        end;
      end;
    end;

    gaConexao[iConexao].bFimLinhas := False;
    gaConexao[iConexao].bPosicionadoEmLinha := True;
    gaConexao[iConexao].bTemAtualizacao := False;
    gaConexao[iConexao].iModoOperacao := IOMODE_EDIT;

  except
    ErroTratar('Anterior - uniDados.Pas');
    Result := IORET_FAIL;
    FecharConexao(iConexao);
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.RecallColumns   Limpar os campos da conexao
 *
 *----------------------------------------------------------------------}
function TdmDados.RecallColumns(const iConexao: Integer): string;
begin
  try
    Result := IORET_OK;

    { LimparColunas gaConexao[iConexao].iPtrColunas; }
    gaConexao[iConexao].iPtrColunas := IOPTR_NOPOINTER;
    gaConexao[iConexao].iModoOperacao := IOMODE_EDIT;
    gaConexao[iConexao].bTemAtualizacao := False;
    gaConexao[iConexao].bInvalColunas := False;
    gaConexao[iConexao].bPosicionadoEmLinha := True;

  except
    ErroTratar('RecallColumns - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Atualizar    Atualizar a conexao (refresh)
 *
 *----------------------------------------------------------------------}
function TdmDados.Atualizar(var iConexao: Integer): string;
begin
  try
    Result := IORET_OK;
    { se nao tem conexao sai }
    if iConexao = 0 then
      Exit;

    gaConexao[iConexao].bRefazSQL := False;

    { Se tem transa��o ativa }
    if giTransacao > 0 then
    begin
      iConexao := PegarConEmTransacao(iConexao);
      if iConexao = IOPTR_NOPOINTER then
      begin
        Result := IORET_FAIL;
        Exit;
      end;
    end
    else
    begin
//      gaSql[gaConexao[iConexao].iConexao].Refresh;
      { fechar a sql }
      FecharSql(gaConexao[iConexao].iConexao);
      { criar o sql }
      gaConexao[iConexao].iConexao := CriarSql(gaConexao[iConexao].sSql);
    end;

    if gaConexao[iConexao].iConexao = 0 then
    begin
      Result := IORET_FAIL;
      Exit;
    end;

    gaConexao[iConexao].bFimLinhas := False;
    gaConexao[iConexao].bTemLinhas := True;
    gaConexao[iConexao].bPosicionadoEmLinha := True;

    { if pFlag = IOFLAG_GET then }
    if gaSql[gaConexao[iConexao].iConexao].EOF then
    begin
      gaConexao[iConexao].bTemLinhas := False;
      gaConexao[iConexao].bFimLinhas := True;
      gaConexao[iConexao].bPosicionadoEmLinha := False;
    end
    else
    begin
      if gaConexao[iConexao].sFind <> VAZIO then
      begin
        if not gaSql[gaConexao[iConexao].iConexao].FindFirst then
        begin
          gaConexao[iConexao].bFimLinhas := True;
          gaConexao[iConexao].bAchouLinha := False;
          gaConexao[iConexao].bPosicionadoEmLinha := False;
          Exit;
        end;
        if gaSql[gaConexao[iConexao].iConexao].EOF then
        begin
          gaConexao[iConexao].bFimLinhas := True;
          gaConexao[iConexao].bTemLinhas := False;
          gaConexao[iConexao].bPosicionadoEmLinha := False;
          Exit;
        end;
      end;
    end;

  except
    ErroTratar('Atualizar - uniDados.Pas');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.SequenciaProxVal    Retorna o proximo valor da sequencia
 *
 *----------------------------------------------------------------------}
function TdmDados.SequenciaProxVal(const sSequenciaChave: string): Integer;
const
  NUM_MAX_TENTATIVAS_LOCK = 10;
  NUM_ITERACOES_WAIT = 100;
  ERRO_LOCKED_BY_ANOTHER_USER = 10259;
var
  sSql:        string;
  lSequencia:  Longint;
  vSequencia:  Variant;
  iTentativas: Integer;
  iContador:   Integer;
  tqSeq:       TQuery;
  aSqlParms:   array[0..1] of Variant;
  bTentativa:  Boolean;
  eErro      : Exception;
  eDBErro    : EDBEngineError;
  iErro      : Integer;
  iVolta     : Integer;
  {para as mudancas}
  liConexaoIndex: Integer;
  liColunaIndex: Integer;
  laParm: array [0..5] of Variant;
  lsSql: String;
  llSequencia: LongInt;
begin
  Result := 0;

  lSequencia := 0;
  iTentativas := 0;
  try

    llSequencia := 0;
    liColunaIndex := IOPTR_NOPOINTER;

    laParm[0] := sSequenciaChave;
    lsSql := dmDados.SqlVersao('SEQ_0000', laParm);
    If dmDados.giStatus <> STATUS_OK Then
      Exit;
    liConexaoIndex := dmDados.ExecutarSelect(lsSql);

    if not dmDados.Status(liConexaoIndex, IOSTATUS_NOROWS) then
    begin
      llSequencia := ValorColuna(liConexaoIndex, 'SEQUENCIA', liColunaIndex);

      dmDados.gsRetorno := dmDados.AlterarColuna(liConexaoIndex, 'SEQUENCIA', liColunaIndex, llSequencia + 1);

      { faz a atualizacao}
      dmDados.gsRetorno := dmDados.Alterar(liConexaoIndex, 'zSequencia', VAZIO);
    end;

    FecharConexao(liConexaoIndex);

    Result := llSequencia;

(*    aSqlParms[0] := sSequenciaChave;

    sSql := SqlVersao('SEQ_0000', aSqlParms);
    { criar o SQL }
    tqSeq := TQuery.Create(Self);
    tqSeq.DatabaseName := gsDBNome;   //DATABASE_NAME;
    tqSeq.SQL.Add(sSql);
    tqSeq.RequestLive := True;
    if (not tqSeq.Prepared) then
      tqSeq.Prepare;

    {Atualiza variavel de controle de tentativas para pegar proxomo valor}
//    bTentativa := True;
    bTentativa := False;

    {enquanto nao liberar para pegar o proximo valor}
    while bTentativa do
    begin
      try
        tqSeq.Open;

        vSequencia := tqSeq.FieldByName('SEQUENCIA').AsVariant;
        lSequencia := vSequencia;
        if tqSeq.CanModify then
        begin

          tqSeq.Edit;
          tqSeq.FieldByName('SEQUENCIA').Value := lSequencia + 1;
          tqSeq.Post;
          {ocorreu tudo bem, sai do loop}
           bTentativa := False;
        end;

      except
        tqSeq.Cancel;
        tqSeq.Close;
        iErro := -1;

        eErro := Exception(ExceptObject);
        if eErro is EDBEngineError then
        begin
          eDBErro := EDBEngineError(eErro);
          iErro := eDBErro.Errors[0].ErrorCode;
        end;

        if iErro = ERRO_LOCKED_BY_ANOTHER_USER then
        begin
          iTentativas := iTentativas + 1;

          if iTentativas > NUM_MAX_TENTATIVAS_LOCK then
          begin
            MensagemExibir(VAZIO, 46); { Voc� est� utilizando o recurso, portanto n�o poder� acess�-lo. }
            if giStatus <> STATUS_OK then
            begin
              ErroTratar('SequenciaProxVal - uniDados.Pas');
              Exit;
            end;
            if giMsgResposta <> IDCANCEL then
              { Resume }
              Exit;
          end
          else
          begin
            iContador := 0;
            {um contador para passar um pouco de tempo entre uma tentativa e outra}
            for iVolta := 0 To 10000 do
              iContador := iContador;

            {tenta novamente}
            bTentativa := True;
          end;
        end;
      end;
    end;

    tqSeq.Close;
    tqSeq.Unprepare;
    tqSeq.Free;
    lSequencia := lSequencia + 1;
*)
  except
    on E:EDBEngineError do
(*    begin
      if E.Errors[0].ErrorCode = ERRO_LOCKED_BY_ANOTHER_USER then
      begin
        iTentativas := iTentativas + 1;
        if iTentativas > NUM_MAX_TENTATIVAS_LOCK then
        begin
          MensagemExibir(VAZIO, 2046); { Voc� est� utilizando o recurso, portanto n�o poder� acess�-lo. }
          if giStatus <> STATUS_OK then
          begin
            ErroTratar('SequenciaProxVal - uniDados.Pas');
            Exit;
          end;
          if giMsgResposta <> IDCANCEL then
            { Resume }
            Exit;
        end
        else
        begin
          for iContador := 0 to NUM_ITERACOES_WAIT do
            { Resume }

        end;
      end;
    end;
  *)
    ErroTratar('SequenciaProxVal - uniDados.Pas');
  end;
//  Result := lSequencia;
end;

{*----------------------------------------------------------------------
 * TdmDados.ConfigValor    Retorna o valor do paramentro
 *
 *----------------------------------------------------------------------}
function TdmDados.ConfigValor(const psChave: String): String;
var
  liConexao: Integer;
  lsSql: String;
  aSqlParms:  array[0..1] of Variant;
begin
  try
    { Indica valor n�o encontrado }
    Result := VAZIO;

    if miConfigIndCol = 0 then
      miConfigIndCol := IOPTR_NOPOINTER;

//    if (gXRs_Config = IOPTR_NOPOINTER) or (gXRs_Config = 0) then
///    begin
      lsSql := SqlVersao('GL_0000', aSqlParms);
      liConexao := ExecutarSelect(lsSql);
      if liConexao = IOPTR_NOPOINTER then
        Exit;
//    end;

    if AtivaFiltro(liConexao, 'Chave = ''' + psChave + '''', IOFLAG_FIRST, IOFLAG_GET) = IORET_NOMATCH then
      Exit;

    Result := ValorColuna(liConexao, 'Valor', miConfigIndCol);

    FecharConexao(liConexao);
  except
  end;

end;

{*----------------------------------------------------------------------
 * TdmDados.ConfigGrava    Grava o valor do paramentro
 *
 *----------------------------------------------------------------------}
procedure TdmDados.ConfigGrava(const psChave: String; psValor: string);
var
  lsSql: String;
  aSqlParms:  array[0..1] of Variant;
begin
  try
    aSqlParms[0] := psValor;
    aSqlParms[1] := psChave;
    lsSql := SqlVersao('GL_0001', aSqlParms);
    gsRetorno := ExecutarSQL(lsSql);

  except
  end;

end;

{*----------------------------------------------------------------------
 * TdmDados.AtivaFiltro    Ativar o filtro
 *
 *----------------------------------------------------------------------}
function TdmDados.AtivaFiltro(var iConexao: Integer;
                               const sCriterio: string;
                               const bInicio: Boolean;
                               const iFlag: Integer): string;
var
  iIntConexao: Integer;
begin
  try
    Result := IORET_OK;

    iConexao := PegarUltConAssoc(iConexao);

    if gaConexao[iConexao].bRefazSQL = True then
    begin
      gaConexao[iConexao].sFind := sCriterio;
      if Atualizar(iConexao) = IORET_FAIL then
      begin
        ErroTratar('AtivaFiltro - uniDados.Pas');
        FecharConexao(iConexao);
        Result := IORET_FAIL;
        Exit;
      end;
      iConexao := PegarUltConAssoc(iConexao);
    end;

    iIntConexao := gaConexao[iConexao].iConexao;
    gsConexaoRetorno := RecallColumns(iConexao);

    gaConexao[iConexao].bFimLinhas := False;
    gaConexao[iConexao].bTemLinhas := True;
    gaConexao[iConexao].bInvalColunas := False;
    gaConexao[iConexao].bPosicionadoEmLinha := True;
    gaConexao[iConexao].bAchouLinha := True;

    if Trim(sCriterio) <> VAZIO then
    begin
      gaConexao[iConexao].sFind := sCriterio;
      gaSql[iIntConexao].Filter := sCriterio;
      (*gaSql[iIntConexao].Filtered := True;*)

      { Situa��o de NOMOREROWS }
      if (gaSql[iIntConexao].EOF = True) and (gaSql[iIntConexao].BOF = True) then
      begin
        gaConexao[iConexao].bFimLinhas := True;
        gaConexao[iConexao].bTemLinhas := False;
        gaConexao[iConexao].bAchouLinha := False;
        gaConexao[iConexao].bPosicionadoEmLinha := False;
        Result := IORET_EOF;
        Exit;
      end;

      if bInicio = IOFLAG_FIRST then
        gaSql[iIntConexao].FindFirst
      else
        gaSql[iIntConexao].FindNext;

      if not gaSql[iIntConexao].Found then
      begin
        gaConexao[iConexao].bFimLinhas := True;
        gaConexao[iConexao].bTemLinhas := False;
        gaConexao[iConexao].bAchouLinha := False;
        gaConexao[iConexao].bPosicionadoEmLinha := False;
        Result := IORET_EOF;
        Exit;
      end;
    end
    else
    begin
      gaConexao[iConexao].sFind := VAZIO;
      { Situa��o de NOMOREROWS }
      if (gaSql[iIntConexao].EOF = True) and (gaSql[iIntConexao].BOF = True) then
      begin
        gaConexao[iConexao].bFimLinhas := True;
        gaConexao[iConexao].bTemLinhas := False;
        gaConexao[iConexao].bAchouLinha := False;
        gaConexao[iConexao].bPosicionadoEmLinha := False;
        Result := IORET_EOF;
        Exit;
      end;

      if bInicio = IOFLAG_FIRST then
      begin
        gaSql[iIntConexao].First;

        if gaSql[iIntConexao].EOF then
        begin
          gaConexao[iConexao].bFimLinhas := True;
          gaConexao[iConexao].bTemLinhas := False;
          gaConexao[iConexao].bAchouLinha := False;
          gaConexao[iConexao].bPosicionadoEmLinha := False;
          Result := IORET_EOF;
          Exit;
        end;
      end;
    end;

  except
    ErroTratar('AtivaFiltro - uniDados.Pas');
    FecharConexao(iConexao);
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Posiciona    Posiciona no registro
 *
 *----------------------------------------------------------------------}
function TdmDados.Posiciona(var iConexao: Integer;
                            const sCampo: string;
                            const vValor: array of Variant): string;
var
  iIntConexao: Integer;
  bAchou : Boolean;
  vPassagem:  Variant;
begin
  try
    Result := IORET_OK;

    iConexao := PegarUltConAssoc(iConexao);

    if gaConexao[iConexao].bRefazSQL = True then
    begin
      if Atualizar(iConexao) = IORET_FAIL then
      begin
        ErroTratar('Posiciona - uniDados.Pas');
        FecharConexao(iConexao);
        Result := IORET_FAIL;
        Exit;
      end;
      iConexao := PegarUltConAssoc(iConexao);
    end;

    iIntConexao := gaConexao[iConexao].iConexao;
    gsConexaoRetorno := RecallColumns(iConexao);

    gaConexao[iConexao].bFimLinhas := False;
    gaConexao[iConexao].bTemLinhas := True;
    gaConexao[iConexao].bInvalColunas := False;
    gaConexao[iConexao].bPosicionadoEmLinha := True;
    gaConexao[iConexao].bAchouLinha := True;

    if Trim(sCampo) <> VAZIO then
    begin
      vPassagem := VarArrayOf(vValor);
      if High(vValor) = 0 then
        bAchou := gaSql[iIntConexao].Locate(sCampo, vValor[0], [loCaseInsensitive, loPartialKey])
      else
        bAchou := gaSql[iIntConexao].Locate(sCampo, VarArrayOf(vValor), [loCaseInsensitive, loPartialKey]);

      { Situa��o de NOMOREROWS }
      if (gaSql[iIntConexao].EOF = True) and (gaSql[iIntConexao].BOF = True) then
      begin
        gaConexao[iConexao].bFimLinhas := True;
        gaConexao[iConexao].bTemLinhas := False;
        gaConexao[iConexao].bAchouLinha := False;
        gaConexao[iConexao].bPosicionadoEmLinha := False;
        Result := IORET_EOF;
        Exit;
      end;

      if not bAchou then
      begin
        gaConexao[iConexao].bFimLinhas := True;
        gaConexao[iConexao].bTemLinhas := False;
        gaConexao[iConexao].bAchouLinha := False;
        gaConexao[iConexao].bPosicionadoEmLinha := False;
        Result := IORET_EOF;
        Exit;
      end;
    end;

  except
    ErroTratar('Posiciona - uniDados.Pas');
    FecharConexao(iConexao);
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.AtivaPrimeiraComp    Posiciona na primeira comparacao
 *
 *----------------------------------------------------------------------}
function TdmDados.AtivaPrimeiraComp(var iConexao: Integer;
                                     const sCriterio: string;
                                     const aColunasChave: array of string;
                                     const iFlag: Integer): string;
var
  iIntConexao: Integer;
begin
  try
    Result := IORET_OK;

    iConexao := PegarUltConAssoc(iConexao);

    iIntConexao := gaConexao[iConexao].iConexao;
    gsConexaoRetorno := RecallColumns(iConexao);

    gaConexao[iConexao].bPosicionadoEmLinha := True;
    gaConexao[iConexao].bAchouLinha := True;
    gaConexao[iConexao].bFimLinhas := False;

    if Trim(sCriterio) <> VAZIO then
    begin
      gaConexao[iConexao].sFindParaMatch := sCriterio;
      { Se Tem FILTER }
      if gaConexao[iConexao].sFind <> VAZIO then
      begin
        gaSql[iIntConexao].Filter := gaConexao[iConexao].sFind + ' AND ' + sCriterio;
        gaSql[iIntConexao].FindFirst;
        gaSql[iIntConexao].Filter := gaConexao[iConexao].sFind;
      end
      else
      begin
        gaSql[iIntConexao].Filter := sCriterio;
        gaSql[iIntConexao].FindFirst;
        gaSql[iIntConexao].Filter := VAZIO;
      end;
      if not gaSql[iIntConexao].Found then
      begin
        gaConexao[iConexao].bAchouLinha := False;
        gaConexao[iConexao].bPosicionadoEmLinha := False;
        Result := IORET_NOMATCH;
        Exit;
      end
      else
      begin
        gaConexao[iConexao].bkMarkParaMatch := gaSql[iIntConexao].Bookmark;
      end;
    end;

  except
    ErroTratar('AtivaPrimeiraComp - uniDados.Pas');
//    FecharConexao(iConexao);
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.AtivaModo    Ativa modo de edicao
 *
 *----------------------------------------------------------------------}
function TdmDados.AtivaModo(var iConexao: Integer;
                             const iMode: Integer): string;
var
  iIndexCols:   Integer;
  iChaveMaxInd: Integer;
  aChaveItem:   array[0..MAX_NUM_COLUNAS] of string;
  aColunasInfo: array[0..MAX_NUM_COLUNAS] of TColunaInfo;
  sWhere:       string;
  iContador:    Integer;
  DBInfo:       TDBInfo;
begin
  try
    Result := IORET_OK;

    { Inicializa variaveis}
    for iContador := 0 to MAX_NUM_COLUNAS do
    begin
      aColunasInfo[iContador].iOrdinalPos := 0;
      aColunasInfo[iContador].ftTipo := ftUnknown;
      aColunasInfo[iContador].iTamanho := 0;
    end;

    iConexao := PegarUltConAssoc(iConexao);

    case iMode of
      IOMODE_INSERT:
      begin
        if gaConexao[iConexao].sTabela <> IORET_FAIL then
        begin
          gaConexao[iConexao].iModoOperacao := iMode;
          gaConexao[iConexao].bInvalColunas := False;
          gaConexao[iConexao].bPosicionadoEmLinha := True;
          iIndexCols := gaConexao[iConexao].iPtrColunas;
          if iIndexCols <> IOPTR_NOPOINTER then
          begin
            while iIndexCols <> IOPTR_NOPOINTER do
            begin
              gaColunas[iIndexCols].vValor := Null;
              iIndexCols := gaColunas[iIndexCols].iProximaColuna;
            end;
          end;
        end
        else
        begin
          Result := IORET_FAIL;
          Exit;
        end;
      end;
      IOMODE_EDIT:
      begin
        if gaConexao[iConexao].sTabela <> IORET_FAIL then
          gaConexao[iConexao].iModoOperacao := iMode
        else
        begin
          Result := IORET_FAIL;
          Exit;
        end;

        sWhere := VAZIO;
        if gaConexao[iConexao].sIndexCampos = VAZIO then
        begin
          DBInfo.sTabelaNome := gaConexao[iConexao].sTabela;
          gaConexao[iConexao].sIndexCampos := CamposIndices(DBInfo, 'PrimaryKey');
        end;

        { Desmembra Chaves }
        GuardarIndices(gaConexao[iConexao].sIndexCampos, aChaveItem, iChaveMaxInd);

        if (iChaveMaxInd > 0) or ((iChaveMaxInd = 0) and (aChaveItem[0] <> VAZIO)) then
        begin
          { Redimensiona array de Columns_info para obter Tipo e Valor de cada }
          { coluna da chave }
          for iContador := 0 to iChaveMaxInd do
            aColunasInfo[iContador].sNome := aChaveItem[iContador];

          gsConexaoRetorno := ColunasInfo(iConexao, aColunasInfo, IOCOL_ALL);
          if gsConexaoRetorno = IORET_FAIL then
          begin
            Result := IORET_FAIL;
            Exit;
          end;

          sWhere := ' WHERE ';
          for iContador := 0 to iChaveMaxInd do
          begin
            if iContador > 0 then
              sWhere := sWhere + ' AND ';

            sWhere := sWhere + aColunasInfo[iContador].sNome;
            if VarIsNull(aColunasInfo[iContador].vValor) then
              sWhere := sWhere + ' = Null'
            else
            begin
              case aColunasInfo[iContador].ftTipo of
                ftString:
                  sWhere := sWhere + ' = ''' + aColunasInfo[iContador].vValor + '''';
                ftDate, ftDateTime:
                  sWhere := sWhere + ' = ' + CTEDB_DATETIME_INI + FormatDateTime(MK_DATATIME, aColunasInfo[iContador].vValor) + CTEDB_DATETIME_FIM;
                ftSmallint, ftInteger:
                  sWhere := sWhere + ' = ' + IntToStr(aColunasInfo[iContador].vValor);
                ftCurrency, ftFloat:
                  sWhere := sWhere + ' = ' + TrataDecimalBD(aColunasInfo[iContador].vValor, IOFLAG_VALORPARABANCO);
                ftBytes:
                  sWhere := sWhere + ' = ' + CTEDB_BINARY_INI + aColunasInfo[iContador].vValor + CTEDB_BINARY_FIM;
                ftBoolean:
                begin
                  if aColunasInfo[iContador].vValor then
                    sWhere := sWhere + ' = 1'//' = True'
                  else
                    sWhere := sWhere + ' = 0';//' = False';
                end;
                else
                begin
                  sWhere := sWhere + ' = ' + VarToStr(aColunasInfo[iContador].vValor);
                end;
              end;
            end;
          end;
        end;

        gaConexao[iConexao].sWhere := sWhere;
      end;
    end;

  except
    ErroTratar('AtivaModo - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.AtivaProximaComp    Posiciona na proxima comparacao
 *
 *----------------------------------------------------------------------}
function TdmDados.AtivaProximaComp(var iConexao: Integer;
                                    const iFlag: Integer): string;
var
  iIntConexao: Integer;
begin
  try
    Result := IORET_OK;

    iConexao := PegarUltConAssoc(iConexao);

    iIntConexao := gaConexao[iConexao].iConexao;
    gsConexaoRetorno := RecallColumns(iConexao);

    gaConexao[iConexao].bPosicionadoEmLinha := True;
    gaConexao[iConexao].bAchouLinha := True;

    if gaConexao[iConexao].sFindParaMatch <> VAZIO then
    begin
      { Restaura �ltimo registro posicionado para Match }
      gaSql[iIntConexao].Bookmark := gaConexao[iConexao].bkMarkParaMatch;
      { Se tem FILTER }
      if gaConexao[iConexao].sFind <> VAZIO then
      begin
        gaSql[iIntConexao].Filter := gaConexao[iConexao].sFind + ' AND ' + gaConexao[iConexao].sFindParaMatch;
        gaSql[iIntConexao].FindNext;
        gaSql[iIntConexao].Filter := gaConexao[iConexao].sFind;
      end
      else
      begin
        gaSql[iIntConexao].Filter := gaConexao[iConexao].sFindParaMatch;
        gaSql[iIntConexao].FindNext;
        gaSql[iIntConexao].Filter := VAZIO;
      end;

      if not gaSql[iIntConexao].Found then
      begin
        gaConexao[iConexao].bAchouLinha := False;
        gaConexao[iConexao].bPosicionadoEmLinha := False;
        Result := IORET_NOMATCH;
        Exit;
      end
      else
        gaConexao[iConexao].bkMarkParaMatch := gaSql[iIntConexao].Bookmark;
    end;

  except
    ErroTratar('AtivaProximaComp - uniDados.Pas');
    FecharConexao(iConexao);
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.SQLTabelaNome    Retorna o nome da tabela do sql
 *
 *----------------------------------------------------------------------}
function TdmDados.SQLTabelaNome(const sSql: string): string;
var
  iPosicao:         Integer;
  iPosicaoClausula: Integer;
  iPosicaoVirgula:  Integer;
  iPosicaoBranco:   Integer;
  iPosicaoCarac10:  Integer;
  sSqlAux:          string;
begin
  try
    Result := IORET_FAIL;

    if sSql = VAZIO then
      Exit;

    { Assume Todos os caracteres em caixa alta }
    sSqlAux := UpperCase(sSql);

    iPosicao := Pos('FROM', sSqlAux) + 5;
    while (Copy(sSqlAux, iPosicao, 1) = ' ') or
          (Ord(PChar(Copy(sSqlAux, iPosicao, 1))[0]) = 10) or
          (Ord(PChar(Copy(sSqlAux, iPosicao, 1))[0]) = 13) do
      iPosicao := iPosicao + 1;

    iPosicaoClausula := Pos('WHERE', sSqlAux);
    if iPosicaoClausula = 0 then
    begin
      iPosicaoClausula := Pos('ORDER BY', sSqlAux);
      if iPosicaoClausula = 0 then
        iPosicaoClausula := Pos('GROUP BY', sSqlAux);
    end;

    iPosicaoVirgula := Pos(',', Copy(sSqlAux, iPosicao, Length(sSqlAux)));
    if iPosicaoVirgula <> 0 then
      iPosicaoVirgula := iPosicaoVirgula  + iPosicao - 1;
    iPosicaoBranco := Pos(' ', Copy(sSqlAux, iPosicao, Length(sSqlAux)));
    if iPosicaoBranco <> 0 then
      iPosicaoBranco := iPosicaoBranco  + iPosicao - 1;
    iPosicaoCarac10 := Pos(Chr(10), Copy(sSqlAux, iPosicao, Length(sSqlAux)));
    if iPosicaoCarac10 <> 0 then
      iPosicaoCarac10 := iPosicaoCarac10  + iPosicao - 1;
    if ((iPosicaoCarac10 <> 0) and (iPosicaoCarac10 < iPosicaoBranco)) or
       ((iPosicaoBranco = 0) and (iPosicaoCarac10 <> 0)) then
      iPosicaoBranco := 0;

    if iPosicaoClausula = 0 then
    begin
      if iPosicaoVirgula <> 0 then
      begin
        { Ex: "SELECT ... FROM xxxxxxx, yyyyyyy" }
        Result := IORET_FAIL;
        Exit;
      end
      else
      begin
        if iPosicaoBranco = 0 then
        begin
          { Ex: "SELECT ... FROM xxxxxx" }
          Result := Copy(sSqlAux, iPosicao, Length(sSqlAux));
          Exit;
        end
        else
        begin
          { Ex: "SELECT ... FROM xxxxxx as yyyyy" }
          Result := Copy(sSqlAux, iPosicao, iPosicaoBranco - iPosicao);
          Exit;
        end;
      end;
    end
    else
    begin
      if (iPosicaoVirgula <> 0) and (iPosicaoVirgula < iPosicaoClausula) then
      begin
        { Ex: "SELECT ... FROM xxxxxxx, yyyyyyy WHERE ..." }
        Result := IORET_FAIL;
        Exit;
      end;
      { Ex: "SELECT ... FROM xxxxxx WHERE ..." }
      if iPosicaoBranco <> 0 then
      begin
        Result := Copy(sSqlAux, iPosicao, iPosicaoBranco - iPosicao);
        Exit;
      end
      else
      begin
        Result := Copy(sSqlAux, iPosicao, iPosicaoCarac10 - iPosicao - 1);
        Exit;
      end;
    end;

  except
    ErroTratar('SQLTabelaNome - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.TabelaIndex    Retorna o PrimaryKey da tabela pedida
 *
 *----------------------------------------------------------------------}
function TdmDados.TabelaPrimaryKey(const sTabela: string): string;
var
  lsSql:          string;
  iConexaoIndex: Integer;
  iColunaPrimaryKey: Integer;
  vResposta: Variant;
  laParm: array [0..5] Of Variant;
begin
  try
    Result := VAZIO;

    if mbDentro then
       Exit;
    mbDentro := True;

    iColunaPrimaryKey := IOPTR_NOPOINTER;

    laParm[0] := sTabela;
    lsSql := dmDados.SqlVersao('PARAM_0006', laParm);
    If dmDados.giStatus <> STATUS_OK Then
      Exit;
    iConexaoIndex := dmDados.ExecutarSelect(lsSql);

    if not dmDados.Status(iConexaoIndex, IOSTATUS_NOROWS) then
      vResposta := ValorColuna(iConexaoIndex,'Coluna_Indice',iColunaPrimaryKey)
    else
      vResposta := VAZIO;

    FecharConexao(iConexaoIndex);

    Result := VarToStr(vResposta);

    mbDentro := False;

  except
    ErroTratar('TabelaPrimaryKey - uniDados.Pas');
    Result := VAZIO;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Status    retorna o status da conexao
 *
 *----------------------------------------------------------------------}
function TdmDados.Status(var iConexao: Integer;
                          const iArgumento: Integer): Boolean;
begin
  try
    iConexao := PegarUltConAssoc(iConexao);

    case iArgumento of
      IOSTATUS_EOF:
      begin
        if gaConexao[iConexao].bFimLinhas = True then
          Result := True
        else
          Result := False;
      end;

      IOSTATUS_NOMATCH:
      begin
        if gaConexao[iConexao].bAchouLinha = False then
          Result := True
        else
          Result := False;
      end;

      IOSTATUS_NOROWS:
      begin
        if gaConexao[iConexao].bTemLinhas = False then
          Result := True
        else
          Result := False;
      end;

    else
      Result := False;
    end;

  except
    ErroTratar('Status - uniDados.Pas');
    Result := False;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.TrataDecimalBD    Converte o ponto decima do banco
 *
 *----------------------------------------------------------------------}
function TdmDados.TrataDecimalBD(const vValor: Variant;
                                  const iFlag: Integer): string;
var
  sValor:    string;
  sValorAux: string;
  iIndex:    Integer;
  iContador: Integer;
begin
  try
    if not VarIsNull(vValor) then
    begin
      if iFlag = IOFLAG_VALORDOBANCO then
      begin
        sValorAux := FloatToStr(vValor);
        sValor := VAZIO;
        for iContador := 1 to Length(sValorAux) do
        begin
          if Copy(sValorAux, iContador, 1) <> ',' then
            sValor := sValor + Copy(sValorAux, iContador, 1);
        end;

        repeat
          iIndex := Pos('.', sValor);
          if iIndex <> 0 then
            sValor := Copy(sValor, 1, iIndex - 1) + ',' + Copy(sValor,iIndex + 1, Length(sValor))
          else
            break;
        until True;

        Result := sValor
      end
      else
      begin
        if iFlag = IOFLAG_VALORPARABANCO then
        begin
          sValorAux := FloatToStr(vValor);
          sValor := VAZIO;
          for iContador := 1 to Length(sValorAux) do
          begin
            if Copy(sValorAux, iContador, 1) <> '.' then
              sValor := sValor + Copy(sValorAux, iContador, 1)
          end;

          repeat
            iIndex := Pos(',', sValor);
            if iIndex <> 0 then
              sValor := Copy(sValor, 1, iIndex - 1) + '.' + Copy(sValor,iIndex + 1, Length(sValor))
            else
              break;
          until True;

          Result := sValor;
        end;
      end;
    end;

  except
    ErroTratar('TrataDecimalBD - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.Alterar    Alterar registro de um sql
 *
 *----------------------------------------------------------------------}
function TdmDados.Alterar(var iConexao: Integer;
                           const sTabela: string;
                           const sWhere: string): string;
var
  sSql: string;
begin
  try
    Result := IORET_FAIL;

    iConexao := PegarUltConAssoc(iConexao);

    if gaConexao[iConexao].iModoOperacao = IOMODE_INSERT then
    begin
      Result := Incluir(iConexao,sTabela);
      Exit;
    end;

    { Se no ExecuteSelect n�o conseguir obter o nome da tabela, n�o � poss�vel }
    { executar UPDATE }
    if gaConexao[iConexao].sTabela = IORET_FAIL then
    begin
      Result := IORET_FAIL;
      Exit;
    end;
    if gaConexao[iConexao].bTemAtualizacao = False then
    begin
      Result := IORET_OK;
      Exit;
    end;

(*    sSql := AlterarStr(iConexao, gaConexao[iConexao].sTabela, gaConexao[iConexao].sWhere);*)
    sSql := AlterarStr(iConexao, sTabela, sWhere);
    if sSql = IORET_FAIL then
    begin
      ErroTratar('Alterar - uniDados.Pas');
      Result := IORET_FAIL;
      Exit;
    end;

    if sSql <> VAZIO then
    begin
      Result := ExecutarSQL(sSql);
    end;

    { Limpa gColunas para Access }
    LimparColunas(gaConexao[iConexao].iPtrColunas);
    gaConexao[iConexao].iPtrColunas := IOPTR_NOPOINTER;

    { Limpa Flags de gConexao }
    gaConexao[iConexao].bTemAtualizacao := False;
    gaConexao[iConexao].bInvalColunas := False;

  except
    ErroTratar('Alterar - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.AlterarStr    Retorna o sql de alteracao
 *
 *----------------------------------------------------------------------}
function TdmDados.AlterarStr(var iConexao:Integer;
                             const sTabela: string;
                             const sWhere: string): string;
var
  iContador:      Integer;
  iConta:         Integer;
  aChaveItem:     array[0..MAX_NUM_COLUNAS] of string;
  sSql:           string;
  sIntWhere:      string;
  iPointerCols:   Integer;
  iChaveMaxInd:   Integer;
  aColunasInfo:   array[0..MAX_NUM_COLUNAS] of TColunaInfo;
  sIntTabela:     string;
  aColuna:        array[0..MAX_NUM_COLUNAS] of string;
  aValor:         array[0..MAX_NUM_COLUNAS] of Variant;
  aTipo:          array[0..MAX_NUM_COLUNAS] of TFieldType;
  DBInfo:         TDBInfo;
begin
  try
    Result := VAZIO;

    { Inicializa variaveis}
    for iContador := 0 to MAX_NUM_COLUNAS do
    begin
      aColunasInfo[iContador].iOrdinalPos := 0;
      aColunasInfo[iContador].ftTipo := ftUnknown;
      aColunasInfo[iContador].iTamanho := 0;
    end;

    iPointerCols := gaConexao[iConexao].iPtrColunas;
    if sTabela <> VAZIO then
      sIntTabela := sTabela
    else
      sIntTabela := gaConexao[iConexao].sTabela;

    iContador := 0;
    while iPointerCols <> IOPTR_NOPOINTER do
    begin
      if gaColunas[iPointerCols].bAtualizado = True then
      begin
        aColuna[iContador] := gaColunas[iPointerCols].sNomeColuna;
        { Se for campo tipo data, busca valor em coluna associada caso existir }
//        if (gaColunas[iPointerCols].ftTipoColuna = ftDate) or
//           (gaColunas[iPointerCols].ftTipoColuna = ftDateTime) then
//          aValor[iContador] := PegarDataAssoc(iPointerCols)
//        else
          aValor[iContador] := gaColunas[iPointerCols].vValor;

        aTipo[iContador] := gaColunas[iPointerCols].ftTipoColuna;
        iContador := iContador + 1
      end;
      iPointerCols := gaColunas[iPointerCols].iProximaColuna;
    end;

    { Se n�o tem colunas a ser atualizadas, sai da rotina }
    if (iContador = 0) and (aColuna[0] = VAZIO) then
    begin
      Result := VAZIO;
      Exit;
    end;

    sSql := 'UPDATE ' + sIntTabela + ' SET ';
    for iConta := 0 to iContador - 1 do
    begin
      if iConta > 0 then
        sSql := sSql + ', ';

      sSql := sSql + aColuna[iConta];
      if (Trim(aValor[iConta]) = VAZIO) or (Trim(aValor[iConta]) = VAZIO_DATA) or (VarIsNull(aValor[iConta])) then
//      if VarIsNull(aValor[iConta]) then
        sSql := sSql + ' = Null'
      else
      begin
        case aTipo[iConta] of
          ftString, ftMemo, ftBlob:
          begin
            if (aValor[iConta] = VAZIO) then
              sSql := sSql + ' = Null'
            else
              sSql := sSql + ' = ''' + EstufaString(aValor[iConta], '''') + '''';
          end;
          ftDate, ftDateTime:
          begin
            if (FormatDateTime(MK_DATA, aValor[iConta]) = VAZIO_DATA) or (FormatDateTime(MK_DATA, aValor[iConta]) = VAZIO) then
              sSql := sSql + ' = Null'
            else
              sSql := sSql + ' = ' + CTEDB_DATE_INI + FormatDateTime(MK_DATA, aValor[iConta]) + CTEDB_DATE_FIM;
          end;
          ftSmallint, ftInteger:
            sSql := sSql + ' = ' + VarToStr(aValor[iConta]);
          ftCurrency, ftFloat:
            sSql := sSql + ' = ' + TrataDecimalBD(aValor[iConta], IOFLAG_VALORPARABANCO);
          ftBytes:
            sSql := sSql + ' = ' + CTEDB_BINARY_INI + aValor[iConta] + CTEDB_BINARY_FIM;
          ftBoolean:
          begin
            if aValor[iConta] then
              sSql := sSql + ' = 1'//' = True'
            else
              sSql := sSql + ' = 0';//' = False';
          end;
          else
            sSql := sSql + ' = ' + VarToStr(aValor[iConta]);
        end;
      end;
    end;

    if sWhere = VAZIO then
    begin
      if gaConexao[iConexao].sIndexCampos = VAZIO then
      begin
        DBInfo.sTabelaNome := sIntTabela;
        gaConexao[iConexao].sIndexCampos := CamposIndices(DBInfo, 'PrimaryKey');
      end;

      { Desmembra Chaves }
      GuardarIndices(gaConexao[iConexao].sIndexCampos, aChaveItem, iChaveMaxInd);

      if (iChaveMaxInd > 0) or ((iChaveMaxInd = 0) and (aChaveItem[0] <> VAZIO)) then
      begin
        { Redimensiona array de Columns_info para obter Tipo e Valor de cada }
        { coluna da chave }
        for iContador := 0 to iChaveMaxInd do
          aColunasInfo[iContador].sNome := aChaveItem[iContador];

        gsConexaoRetorno := ColunasInfo(iConexao, aColunasInfo, IOCOL_ALL);
        if gsConexaoRetorno = IORET_FAIL then
        begin
          Result := IORET_FAIL;
          Exit;
        end;

        sIntWhere := ' WHERE ';
        for iContador := 0 to iChaveMaxInd do
        begin
          if iContador > 0 then
            sIntWhere := sIntWhere + ' AND ';

          sIntWhere := sIntWhere + aColunasInfo[iContador].sNome;
          case aColunasInfo[iContador].ftTipo of
            ftString:
              sIntWhere := sIntWhere + ' = ''' + EstufaString(aColunasInfo[iContador].vValor, '''') + '''';
            ftDate, ftDateTime:
              sIntWhere := sIntWhere + ' = ' + CTEDB_DATETIME_INI + FormatDateTime(MK_DATATIME, aColunasInfo[iContador].vValor) + CTEDB_DATETIME_FIM;
            ftSmallint, ftInteger, ftAutoInc:
              sIntWhere := sIntWhere + ' = ' + IntToStr(aColunasInfo[iContador].vValor);
            ftCurrency, ftFloat:
              sIntWhere := sIntWhere + ' = ' + TrataDecimalBD(aColunasInfo[iContador].vValor, IOFLAG_VALORPARABANCO);
            ftBytes:
              sIntWhere := sIntWhere + ' = ' + CTEDB_BINARY_INI + aColunasInfo[iContador].vValor + CTEDB_BINARY_FIM;
            ftBoolean:
            begin
              if aColunasInfo[iContador].vValor then
                sIntWhere := sIntWhere + ' = 1'//' = True'
              else
                sIntWhere := sIntWhere + ' = 0';//' = False';
            end;
            else
            begin
              sIntWhere := sIntWhere + ' = ' + VarToStr(aColunasInfo[iContador].vValor);
            end;
          end;
        end;
        sSql := sSql + sIntWhere;
      end;
    end
    else
      sSql := sSql + ' WHERE ' + sWhere;

    { para atualizar o registro  corrente }
//    gaConexao[iConexao].sFind := sIntWhere;

    Result := sSql;

  except
    ErroTratar('AlterarStr - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.AlterarColuna    Altera o valor de uma coluna do sql
 *
 *----------------------------------------------------------------------}
function TdmDados.AlterarColuna(var iConexao: Integer;
                                 const sColuna: string;
                                 var iIndexColuna: Integer;
                                 const vValor: Variant): string;
var
  iPtrConexao:   Integer;
  iIndex:        Integer;
  bCriaElemento: Boolean;
  iUltColuna:    Integer;
begin
  try
    Result := IORET_OK;

    iIndex := 0;

    iConexao := PegarUltConAssoc(iConexao);

    if gaConexao[iConexao].bInvalColunas = True then
    begin
      giErroConexao := IOERR_NOCURRENTRECORD;
      ErroTratar('AlterarColuna - uniDados.Pas');
      Result := IORET_FAIL;
      Exit;
    end;

    bCriaElemento := False;
    iPtrConexao := gaConexao[iConexao].iConexao;
    if not gaConexao[iConexao].bTemAtualizacao then
    begin
      bCriaElemento := True;
      iUltColuna := IOPTR_NOPOINTER;
    end
    else
    begin
      iIndex := gaConexao[iConexao].iPtrColunas;
      iUltColuna := iIndex;
//      if iIndexColuna = IOPTR_NOPOINTER then
      if sColuna <> VAZIO then
      begin
        while gaColunas[iIndex].sNomeColuna <> sColuna do
        begin
          iIndex := gaColunas[iIndex].iProximaColuna;
          if (iIndex > giColunasTotal) or (iIndex = IOPTR_NOPOINTER) then
            bCriaElemento := True;
          if (bCriaElemento = False) and (iIndex <> IOPTR_NOPOINTER) then
            iUltColuna := iIndex
          else
            break;
        end;
        iIndexColuna := iIndex;
      end
      else
        iIndex := iIndexColuna;
    end;
    if bCriaElemento = True then
    begin
      iIndex := ColunasAlloc(1);
      if iUltColuna = IOPTR_NOPOINTER then
        gaConexao[iConexao].iPtrColunas := iIndex
      else
        gaColunas[iUltColuna].iProximaColuna := iIndex;

      gaColunas[iIndex].sNomeColuna := sColuna;

      gaColunas[iIndex].ftTipoColuna := gaSql[iPtrConexao].FieldDefs.Find(sColuna).DataType;
    end;
    gaColunas[iIndex].vValor := vValor;
    gaColunas[iIndex].bAtualizado := True;
    gaConexao[iConexao].bTemAtualizacao := True;

  except
    ErroTratar('AlterarColuna - uniDados.Pas');
    Result := IORET_FAIL;
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.FecharConexao    Fechar uma conexao
 *
 *----------------------------------------------------------------------}
procedure TdmDados.FecharConexao(var iConexao: Integer);
var
  iIntConexao:  Integer;
  iLastConexao: Integer;
begin
  try
    iLastConexao := PegarUltConAssoc(iConexao);

    if (iLastConexao <> IOPTR_NOPOINTER) and (iLastConexao <> 0) then
    begin
      iIntConexao := gaConexao[iLastConexao].iConexao;
      if iIntConexao <> IOPTR_NOPOINTER then
      begin
        FecharSql(iIntConexao);
      end;

      gaConexao[iLastConexao].bEmUso := False;
      gaConexao[iLastConexao].sSql := VAZIO;
      gaConexao[iLastConexao].sFind := VAZIO;
      gaConexao[iLastConexao].bPosicionadoEmLinha := False;
      gaConexao[iLastConexao].bTemLinhas := False;
      gaConexao[iLastConexao].iTransacao := IOPTR_NOPOINTER;
      gaConexao[iLastConexao].iTransConOrigem := IOPTR_NOPOINTER;
      gaConexao[iLastConexao].iTransacao := 0;
      gaConexao[iLastConexao].sIndexCampos := VAZIO;
(*      gaConexao[iLastConexao].iConPrevious := IOPTR_NOPOINTER; *)
(*      gaConexao[iLastConexao].bAllowPrevious := False; *)
      gaConexao[iLastConexao].iConexao := IOPTR_NOPOINTER;
    end;

  except
    ErroTratar('FecharConexao - uniDados.Pas');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.LimparColunas    Limpar as colunas
 *
 *----------------------------------------------------------------------}
procedure TdmDados.LimparColunas(const iPtrCols: Integer);
var
  iPointer: Integer;   { Indica o corrente da lista }
begin
  try
    if iPtrCols <> IOPTR_NOPOINTER then
    begin
      iPointer := iPtrCols;
      while gaColunas[iPointer].iProximaColuna <> IOPTR_NOPOINTER do
      begin
        iPointer := gaColunas[iPointer].iProximaColuna;
      end;
      gaColunas[iPointer].iProximaColuna := giColunasLivres;

      giColunasLivres := iPtrCols;
    end;

  except
    ErroTratar('LimparColunas - uniDados.Pas');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.GuardarIndices    Guardar os campos chaves
 *
 *----------------------------------------------------------------------}
procedure TdmDados.GuardarIndices(const sChaves: string;
                                   var aChaveItem: array of string;
                                   var iChavesMaxInd: Integer);
var
  iIndexChv:   Integer;   { �ndice para percorrer as chaves de 0 at� CALC_MAX_CHAVES }
  sChavesAux:  string;    { auxiliar para guardar a chave concatenada }
  iPointerStr: Integer;   { pointer para percorrer a string }
begin
  try
    { Desmembra Chaves }
    iIndexChv := 0;
    sChavesAux := sChaves;

    repeat
      iPointerStr := Pos(';', sChavesAux);
      if iPointerStr <> 0 then
      begin
        aChaveItem[iIndexChv] := Trim(Copy(sChavesAux, 1, iPointerStr - 1));
        if (Copy(aChaveItem[iIndexChv], 1, 1) = '+') or (Copy(aChaveItem[iIndexChv], 1, 1) = '-') then
          aChaveItem[iIndexChv] := Copy(aChaveItem[iIndexChv], 2, Length(aChaveItem[iIndexChv]));

        sChavesAux := Trim(Copy(sChavesAux, iPointerStr + 1, Length(sChavesAux)));
        iIndexChv := iIndexChv + 1;
      end
      else
      begin
        if Length(Trim(sChavesAux)) > 0 then
        begin
          aChaveItem[iIndexChv] := Trim(sChavesAux);
          if (Copy(aChaveItem[iIndexChv], 1, 1) = '+') or (Copy(aChaveItem[iIndexChv], 1, 1) = '-') then
            aChaveItem[iIndexChv] := Copy(aChaveItem[iIndexChv], 2, Length(aChaveItem[iIndexChv]));
        end;
        break;
      end;
    until (iIndexChv > CALC_MAX_CHAVES);
    iChavesMaxInd := iIndexChv;

  except
    ErroTratar('GuardarIndices - uniDados.Pas');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.TransBegin    Iniciar uma transacao
 *
 *----------------------------------------------------------------------}
procedure TdmDados.TransBegin(const sNome: string);
begin
  giStatus := STATUS_OK;
  try
(*
    giTransacao := giTransacao + 1;
    if giTransacao > High(gaTransacoes) then
      MessageDlg('Limite maximo de transa��es alcan�ada.',
                  mtWarning, [mbOK], 0)
    else
      gaTransacoes[giTransacao] := sNome;
*)
    // se esta em transacao
    if gdbDados.InTransaction then
      gdbDados.Commit;

    { Inicia uma transacao }
    gdbDados.StartTransaction;

  except
    ErroTratar('TransBegin - uniDados.Pas');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.TransCommit    Gravar uma transacao
 *
 *----------------------------------------------------------------------}
procedure TdmDados.TransCommit(const sNome: string);
//var
//  iUltConexao: Integer;
//  iContador:   Integer;
//  iIndex:      Integer;
//  iIndexCon:   Integer;
//  iTransacao:  Integer;
//  aConexoes:   array[1..MAX_NUM_SQL] of Integer;
begin
  giStatus := STATUS_OK;
  try
(*
    // Inicializa vetor
    for iContador := 1 to MAX_NUM_SQL do
      aConexoes[iContador] := 0;

    if giTransacao > 0 then   { Se existem transa��es abertas }
    begin
      iIndex := 0;
      while gaTransacoes[iIndex] <> sNome do
      begin
        iIndex := iIndex + 1;
        if iIndex > High(gaTransacoes) then
          Exit;
      end;

      if iIndex = 0 then
        Exit;

      iIndexCon := 0;

      for iContador := 1 to High(gaConexao) do
      begin
        { Se conexao foi criada na transacao imediatamente anterior a que }
        { est� sendo fechada, fecha as associadas e assume a �ltima como  }
        { corrente }
        if gaConexao[iContador].iTransacao = iIndex - 1 then
        begin
          iUltConexao := PegarUltConAssoc(iContador);
          if iUltConexao = IOPTR_NOPOINTER then
          begin
            ErroTratar('TransCommit - uniDados.Pas');
            Exit;
          end;

          if iUltConexao <> iContador then
          begin
            { Armazena as conexoes com o Access para fechar ap�s o Commit }
            iIndexCon := iIndexCon + 1;
            aConexoes[iIndexCon] := gaConexao[iContador].iConexao;

            { Limpa gColunas caso exista }
            if gaConexao[iContador].iPtrColunas <> IOPTR_NOPOINTER then
            begin
              LimparColunas(gaConexao[iContador].iPtrColunas);
            end;

            gaConexao[iContador] := gaConexao[iUltConexao];
            gaConexao[iContador].iTransacao := 0;
            gaConexao[iContador].iPtrColunas := IOPTR_NOPOINTER;
            gaConexao[iContador].iConexao := 0;
            gaConexao[iContador].iTransConAssociada := IOPTR_NOPOINTER;
            FecharConexao(iUltConexao);
          end;
        end
        else
        begin
          if gaConexao[iContador].iTransacao >= iIndex then
          begin
            if gaConexao[iContador].iTransConOrigem <> IOPTR_NOPOINTER then
            begin
              gaConexao[gaConexao[iContador].iTransConOrigem].iTransConAssociada := IOPTR_NOPOINTER;
            end;
            iIndex := iContador;
            FecharConexao(iIndex);
          end;
        end;
      end;

      for iTransacao := giTransacao downto iIndex do
      begin
*)
        if gdbDados.InTransaction then
          gdbDados.Commit;
(*
        gaTransacoes[iTransacao] := VAZIO;
      end;
      giTransacao := iIndex;

      { Fecha os dynasets criados antes da transacao }
      for iContador := 1 to High(aConexoes) do
      begin
        if aConexoes[iContador] <> 0 then
          FecharSql(aConexoes[iContador]);
      end;
    end;
*)
  except
    ErroTratar('TransCommit - uniDados.Pas');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.TransRollback    Voltar uma transacao
 *
 *----------------------------------------------------------------------}
procedure TdmDados.TransRollback(const sNome: string);
//var
//  iContador:  Integer;
//  iIndex:     Integer;
//  iTransacao: Integer;
begin
  try
    { � normalmente chamada nos tratamentos de erro }
    { portanto n�o pode mudar gStatus nem mudar as  }
    { condi��es do erro corrente (on error)         }
(*
    { Se existem transa��es abertas }
    if giTransacao > 0 then
    begin
      iIndex := 0;
      while gaTransacoes[iIndex] <> sNome do
      begin
        iIndex := iIndex + 1;
        if iIndex > High(gaTransacoes) then
          Exit;
      end;

      if iIndex = 0 then
        Exit;

      { Trecho copiado }
      for iContador := 1 to High(gaConexao) do
      begin
        if gaConexao[iContador].iTransacao >= iIndex then
        begin
          if (gaConexao[iContador].iTransConOrigem <> IOPTR_NOPOINTER) and
             (gaConexao[iContador].iTransConOrigem <> 0) then
          begin
            gaConexao[gaConexao[iContador].iTransConOrigem].iTransConAssociada := IOPTR_NOPOINTER;
          end;
          iIndex := iContador;
          FecharConexao(iIndex);
        end;
      end;

      for iTransacao := giTransacao downto iIndex do
      begin
*)
        if gdbDados.InTransaction then
          gdbDados.Rollback;
(*
        gaTransacoes[iTransacao] := VAZIO;
      end;
      giTransacao := iIndex;
    end;
*)
  except
    ErroTratar('TransRollBack - uniDados.Pas');
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.ErroTratar    Exibir o erro carregado em giStatus e, se este for
 *                         "fatal", encerrar a aplica��o, fechando todas as
 *                         tabelas do banco "gDb".
 *  Par�metros:  "sLocalErro" deve conter a rotina+modulo ou evento+form em
 *               em que ocorreu o erro.
 *
 *----------------------------------------------------------------------}
procedure TdmDados.ErroTratar(const sLocalErro: string);
var
  iBotao:   Integer;   { bot�o clicado pelo usu�rio na caixa de mensagens }
  eErro:    Exception;
  eDBErro:  EDBEngineError;
begin
  try
    { Tem que ser antes do On Error, sen�o � zerado }
    giStatus := -1(*Err*);
    giNivelMsg := '0';
    { Se c�digo de erro = 1, assume que a mensagem espec�fica foi colocada pelo chamador }
    if giStatus <> 1 then
      gsStatusMensagem := ''(*Error*);

    eErro := Exception(ExceptObject);
    if eErro <> nil then
      gsStatusMensagem := eErro.Message;
    if eErro is EDBEngineError then
    begin
      eDBErro := EDBEngineError(eErro);
      giStatus := eDBErro.Errors[0].ErrorCode;
    end;

    if lbSaindo then
      Exit;
    if lbEmErro then
    begin
      lbSaindo := True;
      { fechar todas as tabelas comuns aos v�rios projetos e o "gDb" }
(*      FinalizarBanco;*)
      Exit;
    end;
    lbEmErro := True;

    gaMsgParm[0] := 'Execu��o';
    gaMsgParm[1] := IntToStr(giStatus);
    gaMsgParm[2] := gsStatusMensagem;
    { exibe erro na caixa de mensagens }
    iBotao := MensagemExibir(sLocalErro, giErroCodigo);

    if not dmDados.Status(giConMsg, IOSTATUS_EOF) then
    begin
      { se ocorreu erro na rotina/evento }
      if giNivelMsg = ERRO_NIVELFATAL then
      begin
        lbSaindo := True;
        { fechar todas as tabelas comuns aos v�rios projetos e o "gDb" }
(*        FinalizarBanco;*)
        Exit;
      end;
    end;

    giErroCodigo := ERRO_CODIGODEFAULT;
    lbEmErro := False;
    Exit;
  except
    lbSaindo := True;
(*    FinalizarBanco;*)
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.MensagemExibir
 * Objetivo:  Exibe uma caixa de di�logo modal (que simula um MsgBox) e
 *            retorna qual o bot�o escolhido pelo usu�rio nessa caixa.
 * Par�metros:A fun��o recebe o c�digo da mensagem como um inteiro.
 *            Al�m desse par�metro, devem estar carregadas as seguintes
 *            vari�veis globais:
 *            - gSsMens (objeto snapshot que deve ser inicializado no
 *                       form_load da aplicAcao atrav�s da
 *                       rotina "grMensagens_Iniciar")
 *            - gMsgParm (array de strings que devem conter os conte�dos
 *                       a serem substitu�dos no texto da mensagem. Isto
 *                       �, se a mensagem possuir @n dentro do texto
 *                       ele ser� substitu�do por gMsgParm(n))
 *            - gInfoAdicionais (string que ser� montada na �ltima linha
 *                       da caixa de mensagens. Esta linha deve ser utiliza-
 *                       da para mostrar c�digos de erros do VB, ACCESS,
 *                       Crystal Report, DLL's, etc.)
 * Retorno:   A fun��o retorna um inteiro que pode ser:
 *           - IDOK = 1                  ' bot�o pressionado = OK
 *           - IDCANCEL = 2              ' bot�o pressionado = Cancelar
 *           - IDYES = 6                 ' bot�o pressionado = Sim
 *           - IDNO = 7                  ' bot�o pressionado = N�o
 *----------------------------------------------------------------------}
function TdmDados.MensagemExibir (const sLocalErro: string;
                                   const iCodigo: Integer): Integer;
var
  sSql:         String;
  iIndColNivel: Integer;
  iStatus:      Integer;
begin
  Result := 0;

  try
    { Checa se j� est� tratando erro }
    if gbFlagOnError = True then
    begin
      MessageDlg('Erro Interno de Sistema : Tentativa de exibir a mensagem ' +
                  IntToStr(iCodigo) + '- ' + sLocalErro,
                  mtWarning, [mbOK], 0);
      { Considera Retorno "Cancel" para a aplica��o }
      Result := IDCANCEL;
      Exit;
    end
    else
    begin
      gbFlagOnError := True;
    end;

    { usa o caption da aplica��o que est� exibindo a mensagem }
    gsFormCaption := Screen.ActiveForm.Caption;
    giCodMensagem := iCodigo;
    gsInfoAdicionais := sLocalErro;
    iStatus := giStatus;
    { abre o sql de mensagens com o codigo correto }
    sSql := 'SELECT * FROM zMENSAGENS WHERE CODIGO = ' + IntToStr(giCodMensagem);
    giConMsg := dmDados.ExecutarSelect(sSql);

    dlgExibeMsg.ShowModal;

    { se ocorreu erro na rotina/evento }
    iIndColNivel := IOPTR_NOPOINTER;
    giNivelMsg := dmDados.ValorColuna(giConMsg,FLDMENS_NIVEL,iIndColNivel);

    dmDados.FecharConexao(giConMsg);

    Result := giMsgResposta;
    gbFlagOnError := False;
    giStatus := iStatus;

  except
  end;
end;
{*----------------------------------------------------------------------
 * TdmDados.EstufaString    Incluir uma string em outra
 *
 *----------------------------------------------------------------------}
function TdmDados.EstufaString(const sValor: string;
                                const sString: string): string;
var
  iPosicao: Integer;
  iNovaPos: Integer;
  sRetorno: string;
begin
  sRetorno := sValor;
  iPosicao := Pos(sString, sRetorno);
  while iPosicao <> 0 do
  begin
    sRetorno := Copy(sRetorno, 1, iPosicao) + sString +
                Copy(sRetorno, iPosicao + 1, Length(sRetorno));
    iNovaPos := iPosicao + 2;
    iPosicao := Pos(sString, Copy(sRetorno, iPosicao + 2, Length(sRetorno)));
    if iPosicao <> 0 then
      iPosicao := iPosicao + iNovaPos - 1;
  end;

  Result := sRetorno;

end;

{*----------------------------------------------------------------------
 * TdmDados.LiteralSubstituir   Substitui literais da linha de mensagem
 *
 *----------------------------------------------------------------------}
function TdmDados.LiteralSubstituir(const vMensagem: Variant;
                                     const aMsgParam: array of Variant): string;
var
  vMsg1: Variant;        { String auxiliar para Mensagem }
  vMsg2: Variant;        { String auxiliar para Mensagem }
  iIni:  Integer;        { Auxiliar para percorrer a STRING }
  iFim:  Integer;        { Indice do parametro }
begin
  try
    giStatus := STATUS_OK;

    Result := VAZIO;
    { se tem conte�do }
    if vMensagem <> VAZIO then
    begin
      { salva conte�do }
      vMsg1 := vMensagem;
      { inicializa in�cio da busca }
      iIni := 1;
      { posi��o da literal a ser substitu�da }
      iFim := Pos('@', vMsg1);
      { limpa Msg auxiliar }
      vMsg2 := VAZIO;

      { se encontrou alguma literal a ser substitu�da }
      while iFim <> 0 do
      begin
        { salva mensagem at� a literal "@" e concatena a literal substitu�da }
        vMsg2 := vMsg2 + Copy(vMsg1, iIni, iFim - iIni) +
                 aMsgParam[StrToInt(Copy(vMsg1, iFim + 1, 1))];
        { despreza o "@n" da mensagem }
        iIni := iFim + 2;
        { busca a pr�xima literal a ser substitu�da }
        iFim := Pos('@', Copy(vMsg1, iIni, Length(vMsg1)));
        if iFim <> 0 then
          iFim := iFim + iIni - 1;
      end;
      { se o in�cio da busca ainda est� dentro da mensagem }
      if iIni <= Length(vMsg1) then
        { concatena o restante da mensagem }
        vMsg2 := vMsg2 + Copy(vMsg1, iIni, Length(vMsg1) - iIni + 1);

      Result := vMsg2;
    end;

  except
    ErroTratar('LiteralSubstituir - uniDados.Pas')
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.TirarMascara   Tira as mascaras
 *
 *----------------------------------------------------------------------}
function TdmDados.TirarMascara(const psValor: String): string;
var
  lsRetorno: String;        { String auxiliar para Mensagem }
  liConta: Integer;        { String auxiliar para Mensagem }
  p: PChar;
begin
  try
    giStatus := STATUS_OK;

    Result := VAZIO;
    lsRetorno := VAZIO;

    liConta := 1;
    While liConta <= Length(psValor) do
    begin
        p := PChar(Copy(psValor, liConta, 1));
        If ((Ord(p^) >= 48) And (Ord(p^) <= 57)) Or ((Ord(p^) >= 65) And (Ord(p^) <= 90)) Or ((Ord(p^) >= 97) And (Ord(p^) <= 122)) or (Ord(p^) = 32) Then
          lsRetorno := lsRetorno + Copy(psValor, liConta, 1);
        liConta := liConta + 1;
    end;

    Result := lsRetorno;

  except
    ErroTratar('TirarMascara - uniDados.Pas')
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.TirarSeparador   Tira as separador de milhar
 *
 *----------------------------------------------------------------------}
function TdmDados.TirarSeparador(const psValor: String): string;
var
  lsRetorno: String;        { String auxiliar para Mensagem }
  liConta: Integer;        { String auxiliar para Mensagem }
  p: PChar;
begin
  try
    giStatus := STATUS_OK;

    Result := VAZIO;
    lsRetorno := VAZIO;

    liConta := 1;
    While liConta <= Length(psValor) do
    begin
        p := PChar(Copy(psValor, liConta, 1));
        If ((p^) <> SEPARADOR_MILHAR) Then
          lsRetorno := lsRetorno + Copy(psValor, liConta, 1);
        liConta := liConta + 1;
    end;

    Result := lsRetorno;

  except
    ErroTratar('TirarSeparador - uniDados.Pas')
  end;
end;

{*----------------------------------------------------------------------
 * TdmDados.PesquisaTabela
 *
 *----------------------------------------------------------------------}
function TdmDados.PesquisaTabela: Variant;
var
  liConexao: Integer;
  lsSql: String;
  liIndCol: Integer;
begin

  giStatus := STATUS_OK;
  Result := VAZIO;
  try
    lsSql := SqlVersao('PARAM_0009', gaParm);
    if giStatus <> STATUS_OK then
      Exit;
    liConexao := ExecutarSelect(lsSql);
    if liConexao = IOPTR_NOPOINTER then
    begin
//      Error(1);
      Exit;
    end;

    liIndCol := IOPTR_NOPOINTER;

    if not Status(liConexao, IOSTATUS_NOROWS) then
      Result := ValorColuna(liConexao, (gaParm[0]), liIndCol)
    else
      Result := IOVAL_NOROWS;
    FecharConexao(liConexao);
  except
    ErroTratar('PesquisaTabela - uniDados.pas');
  end;

end;


end.

