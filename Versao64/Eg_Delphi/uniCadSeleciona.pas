unit uniCadSeleciona;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, Buttons, Variants,
  uniConst, OleCtrls, FPSpread_TLB;

const
  STATUS_SEM_PARAM = 0;
  STATUS_PARAM_NOROWS = 1;
  STATUS_PARAM_OK = 2;

type
  TdlgCadSelecionar = class(TForm)
    pnlMetade: TPanel;
    lblChave: TLabel;
    lsvSelec: TListView;
    edtChave: TEdit;
    pnlEspera: TPanel;
    btnEncontrar: TButton;
    btnAlterar: TButton;
    btnNovo: TButton;
    btnConsultar: TButton;
    btnExcluir: TButton;
    btnCancelar: TButton;
    spbPrimeiro: TSpeedButton;
    spbPagAnterior: TSpeedButton;
    spbRegAnterior: TSpeedButton;
    spbRegProximo: TSpeedButton;
    spbPagProximo: TSpeedButton;
    spbUltimo: TSpeedButton;
    pnlLinha: TPanel;
    cboCadastro: TComboBox;
    lblCadastro: TLabel;
    cboModulos: TComboBox;
    lblModulos: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure cboCadastroChange(Sender: TObject);
    procedure cboCadastroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cboCadastroClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spbPrimeiroClick(Sender: TObject);
    procedure edtChaveChange(Sender: TObject);
    procedure btnAlterarClick(Sender: TObject);
    procedure lsvSelecColumnClick(Sender: TObject; Column: TListColumn);
    procedure btnEncontrarClick(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure lsvSelecClick(Sender: TObject);
    procedure cboModulosClick(Sender: TObject);
    procedure vasSelecBlockSelected(Sender: TObject; BlockCol, BlockRow,
      BlockCol2, BlockRow2: Integer);
  private
    { Private declarations }
    mbDentro: Boolean;
    mbDentroDentro: Boolean;
    miQualDentro: Integer;
    mbMudandoParametro: Boolean;
    miStatus: Integer;
    mbDentroRegerar: Boolean;
    masColunas_Chave: array[0..2] of String;
    mrColunasSpread: array[1..2] of TumaGrid_Coluna;
    mrID_Linhas: array[1..11] of TumId_Linhas;
    maSpeedButs: array[0..5] of TSpeedButton;
    {' Contem a conex�o para os dados da Lista (Spread)}
    mrSpreadInfo:       TumGridMontar_Info;

    procedure mp_Selec_Status_Representar;
    procedure mp_Selec_Atualizar;
    procedure mp_Selec_Regerar_Lista;
    procedure mp_Selec_KeyDown(var KeyCode: Word; Shift: TShiftState);
    procedure mp_Selec_Nav_Inicializar(var pvCodigo: Variant; var pvDescricao: Variant);

  public
    { Public declarations }
  end;

var
  dlgCadSelecionar: TdlgCadSelecionar;

implementation

uses uniCombos, uniLock, uniNaveg, uniDados, uniSeleciona, uniParam, uniSelPar,
    uniTelaVar, uniGlobal, uniFuncoes;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TdlgCadSelecionar.btnCancelarClick        Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgCadSelecionar.btnCancelarClick(Sender: TObject);
begin
  dlgCadSelecionar.Close;
end;

{*-----------------------------------------------------------------------------
 *  TdlgCadSelecionar.cboCadastroChange        Procura no combo
 *
 *-----------------------------------------------------------------------------}
procedure TdlgCadSelecionar.cboCadastroChange(Sender: TObject);
var
  lcboCombo : TComboBox;
begin

  lcboCombo := TComboBox(Sender);

  if lcboCombo.Name = 'cboCadastro' then
  begin
    untCombos.gp_Combo_Seleciona(cboCadastro, 20);
    mp_Selec_Status_Representar;
  end
  else
  begin
    cboCadastro.Enabled := False;
    cboCadastro.Text := VAZIO;
    untCombos.gp_Combo_Seleciona(cboModulos, 5);
    if cboModulos.ItemIndex <> -1 then
      cboModulosClick(Sender);
  end;
end;

{*-----------------------------------------------------------------------------
 *  TdlgCadSelecionar.cboCadastroKeyDown        verifica a tecla
 *
 *-----------------------------------------------------------------------------}
procedure TdlgCadSelecionar.cboCadastroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  { verifica a tecla}
  mp_Selec_KeyDown(Key, Shift)
end;

{*-----------------------------------------------------------------------------
 *  TdlgCadSelecionar.cboCadastroClick        atualiza a tela com a mudanca
 *
 *-----------------------------------------------------------------------------}
procedure TdlgCadSelecionar.cboCadastroClick(Sender: TObject);
begin

  {' n�o reentrante}
  if mbDentro then
  begin
    if not mbDentroDentro then
    begin
      mbDentroDentro := True;
      {' desfaz o que o usu�rio est� tentando}
      cboCadastro.ItemIndex := miQualDentro;
      mbDentroDentro := False;
    end;
    Exit;
  end;
  mbDentro := True;
  miQualDentro := cboCadastro.ItemIndex;

  {' para que o frSelec_Atualizar n�o funcione}
  mbMudandoParametro := True;

  {'Limpe campo chave das buscas anteriores}
  edtChave.Text := VAZIO;

  if cboCadastro.ItemIndex = -1 then
  begin
    miStatus := STATUS_SEM_PARAM;
    Screen.Cursor := crDefault;
    mbDentro := False;
    Exit;
  end;

  {'Inicia montagem da lista}
  Screen.Cursor := crHOURGLASS;

  untTelaVar.gp_TelaVar_Seta_Entidade(cboCadastro.Text);
//  gsCod_Parametro := grTelaVar.P.Nome_Tabela;
  gsCod_Parametro := grTelaVar.P.Sigla_Tabela;
  gpMascara := grTelaVar.p.Mascara_Codigo;
  gpAlinha := grTelaVar.p.Alinha_Codigo;
//  gpAlinhaTamanho := grTelaVar.p.Alinha_Tamanho;
  gpAlinhaTamanho := grTelaVar.p.Codigo_Minimo;

  { checa pela ordem default - se for descricao muda }
//  if grTelaVar.p.Coluna_Nivel = 'COLUNA_DESCRICAO' then
  if grTelaVar.p.Tipo_Objeto = 'D' then
  begin
    if gbCodigo then
    begin
      lsvSelecColumnClick(Sender, lsvSelec.Column[1]);
      gbCodigo := False;
     { guarda a coluna de ordenacao para navegacao }
      mrSpreadInfo.ColunaOrdem := 1;
    end;
  end
  else
  begin
    if not gbCodigo then
    begin
      lsvSelecColumnClick(Sender, lsvSelec.Column[0]);
      gbCodigo := True;
      { guarda a coluna de ordenacao para navegacao }
      mrSpreadInfo.ColunaOrdem := 0;
    end;
  end;

  {' No m�nimo isso deve ser apresentado}
  miStatus := STATUS_PARAM_NOROWS;
  {' para que o frSelec_Atualizar funcione}
  mbMudandoParametro := False;
  mp_Selec_Atualizar;

  Screen.Cursor := crDefault;
  mbDentro := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgCadSelecionar.FormShow        prepara e mostra a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgCadSelecionar.FormShow(Sender: TObject);
var
  liCoxModulo: integer;
begin

  Screen.Cursor := crHOURGLASS;

  // limpa o combo e o list view
  cboCadastro.Items.Clear;
  cboCadastro.Text := VAZIO;
  cboModulos.Items.Clear;
  cboModulos.Text := VAZIO;
  lsvSelec.Items.Clear;
  edtChave.Text := VAZIO;

  {Indica cancelamento}
  grTelaVar.DynaID := 0;
  { inicializa a varavel de conexao da tela de selecao}
  mrSpreadInfo.Conexao := IOPTR_NOPOINTER;
  mrSpreadInfo.LinhasEmUso := 0;

  {Inicializa gSsParametros e gSnapTelas}
  untParam.gp_Param_Iniciar;

  { Ajustando a largura/identifica��o das colunas}
  lsvSelec.Columns.Items[0].Width := 100;
  lsvSelec.Columns.Items[1].Width := 273;
  { Ajustando o label da caixa de pesquisa}
  lblChave.Caption := 'Digite a chave de pesquisa para a coluna "C�digo" e pressione [Encontrar]';
  gbCodigo := True;
  miStatus := STATUS_SEM_PARAM;

  { lista com os botoes de controle da lista de registros}
  maSpeedButs[0] := spbPrimeiro;
  maSpeedButs[1] := spbPagAnterior;
  maSpeedButs[2] := spbRegAnterior;
  maSpeedButs[3] := spbRegProximo;
  maSpeedButs[4] := spbPagProximo;
  maSpeedButs[5] := spbUltimo;

//  untParam.gp_Param_EstufaComboParametrosMDI(cboCadastro);
  liCoxModulo := 0;
  liCoxModulo := untCombos.gf_ComboCadastro_Estufa(cboModulos, 'SE_0011', 'DESCRICAO_MODULO', liCoxModulo, gaParm);
  dmDados.FecharConexao(liCoxModulo);

  { S� deixa habilitado o Combo de Parametros, o botao Cancelar ,
   habilitando o resto quando for escolhido o parametro}
  mp_Selec_Status_Representar;
  untSelPar.gp_SelPar_Ativar_ScrollBtns(mrSpreadInfo, maSpeedButs, lsvSelec);

  if cboCadastro.Items.Count = 1 then
  begin
    cboCadastro.ItemIndex := 0;
    cboCadastro.Enabled := False;
  end;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgCadSelecionar.FormClose       Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgCadSelecionar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  if not ((mrSpreadInfo.Conexao = 0) or (mrSpreadInfo.Conexao = IOPTR_NOPOINTER)) then
  begin
    { fecha o dynaset}
//#######################
//    dmDados.FecharConexao(mrSpreadInfo.Conexao);
//#######################
    mrSpreadInfo.Conexao := IOPTR_NOPOINTER;
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.mf_Selec_Status_Representar
'     Prepara os botoes da tela
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.mp_Selec_Status_Representar;
begin

  case miStatus of
    STATUS_SEM_PARAM:
    begin
      btnAlterar.Enabled := False;
      btnNovo.Enabled := False;
      btnConsultar.Enabled := False;
      btnExcluir.Enabled := False;
      btnEncontrar.Enabled := False;
      edtChave.Enabled := False;
      lblChave.Enabled := False;
      lsvSelec.Enabled := False;
    end;
    STATUS_PARAM_NOROWS:
    begin
      btnAlterar.Enabled := False;
      { se o objeto for somente p/ consulta}
      if grTelaVar.P.Tipo_Objeto = 'C' then
      begin
        btnNovo.Enabled := False;
        btnConsultar.Enabled := True;
      end
      else
      begin
        btnNovo.Enabled := True;
        btnConsultar.Enabled := False;
      end;
      btnExcluir.Enabled := False;
      btnEncontrar.Enabled := False;
      edtChave.Enabled := False;
      lblChave.Enabled := False;
      lsvSelec.Enabled := False;
    end;
    STATUS_PARAM_OK:
    begin
      btnAlterar.Enabled := untSelPar.gf_SelPar_Pode_Atualizar(grTelaVar.P.Cod_Seguranca, TIPO_ALTERA);
      btnConsultar.Enabled := untSelPar.gf_SelPar_Pode_Atualizar(grTelaVar.P.Cod_Seguranca, TIPO_CONSULTA);
      btnExcluir.Enabled := untSelPar.gf_SelPar_Pode_Atualizar(grTelaVar.P.Cod_Seguranca, TIPO_EXCLUI);
      btnNovo.Enabled := untSelPar.gf_SelPar_Pode_Atualizar(grTelaVar.P.Cod_Seguranca, TIPO_INCLUI);
      btnEncontrar.Enabled := (Trim(edtChave.Text) <> VAZIO);
      edtChave.Enabled := True;
      lblChave.Enabled := True;
      lsvSelec.Enabled := True;
    end;
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.mf_Selec_Atualizar
'     Se mudou regera a lista
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.mp_Selec_Atualizar;
begin

  if not mbMudandoParametro then
    mp_Selec_Regerar_Lista;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.mp_Selec_Regerar_Lista
'     regera a lista
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.mp_Selec_Regerar_Lista;
begin

  { Se est� dentro n�o permite}
  if (mbDentroRegerar or (miStatus = STATUS_SEM_PARAM)) then
    Exit;

  mbDentroRegerar := True;

  dlgCadSelecionar.Enabled := False;

  Screen.Cursor := crHOURGLASS;

  pnlEspera.Visible := True;

  dlgCadSelecionar.Refresh;

  miStatus := STATUS_PARAM_NOROWS;
  masColunas_Chave[0] := grTelaVar.P.Coluna_Codigo;
  masColunas_Chave[1] := grTelaVar.P.Coluna_Descricao;

  if untSelPar.gf_SelPar_Nova_Lista(mrSpreadInfo,
                                    mrColunasSpread,
                                    mrID_Linhas,
                                    grTelaVar.P,
                                    maSpeedButs,
                                    edtChave,
                                    lsvSelec,
                                    masColunas_Chave) then
    miStatus := STATUS_PARAM_OK;

  lsvSelec.Visible        := True;
  spbPrimeiro.Visible     := True;
  spbPagAnterior.Visible  := True;
  spbRegAnterior.Visible  := True;
  spbRegProximo.Visible   := True;
  spbPagProximo.Visible   := True;
  spbUltimo.Visible       := True;
  pnlLinha.Visible        := True;

  mp_Selec_Status_Representar;

  pnlEspera.Visible := False;

  dlgCadSelecionar.Enabled := True;

  Screen.Cursor := crDefault;
  mbDentroRegerar := False;

  if lsvSelec.CanFocus then
    lsvSelec.SetFocus;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.mp_Selec_KeyDown
'     Verifica qual tecla foi apertada
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.mp_Selec_KeyDown(var KeyCode: Word; Shift: TShiftState);
begin

  if dlgCadSelecionar.ActiveControl Is TComboBox then
    if (cboCadastro.Handle = dlgCadSelecionar.ActiveControl.Handle) then
      Exit;

  {Para filtrar o teclado e n�o estragar o cursor do spread}
  case KeyCode of
    {Primeira Pagina}
    KEY_HOME:
    begin
      {Pressionou Control-Home}
      if (Shift = [ssCtrl]) then
        spbPrimeiroClick(spbPrimeiro)
      else
        Exit;
    end;
    KEY_PRIOR:
      spbPrimeiroClick(spbPagAnterior);
    KEY_UP:
      spbPrimeiroClick(spbRegAnterior);
    KEY_DOWN:
      spbPrimeiroClick(spbRegProximo);
    KEY_NEXT:
      spbPrimeiroClick(spbPagProximo);
    KEY_END:
    begin
      {Pressionou Control-end}
      if (Shift = [ssCtrl])then
        spbPrimeiroClick(spbUltimo)
      else
        Exit;
    end;
    KEY_C:
    begin
      if (Shift = [ssAlt])then
        gbCodigo := True
      else
        Exit;
    end;
    KEY_D:
    begin
      if (Shift = [ssAlt])then
        gbCodigo := False
      else
        Exit;
    end;
  else
      {Sem zerar o Keycode}
      Exit;
  end;

  KeyCode := 0;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.spbPrimeiroClick
'     muda a lista de acordo com a tecla apertada
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.spbPrimeiroClick(Sender: TObject);
var
  lspbBotao: TSpeedButton;
begin

  pnlEspera.Visible := True;

  lspbBotao := TSpeedButton(Sender);

  edtChave.Text := VAZIO;

  if lspbBotao.Name = spbPrimeiro.Name then
    mrSpreadInfo.Acao := PRIMEIRA_PAGINA
  else if lspbBotao.Name = spbPagAnterior.Name then
        mrSpreadInfo.Acao := PAGINA_TRAS
  else if lspbBotao.Name = spbRegAnterior.Name then
        mrSpreadInfo.Acao := LINHA_TRAS
  else if lspbBotao.Name = spbRegProximo.Name then
        mrSpreadInfo.Acao := LINHA_FRENTE
  else if lspbBotao.Name = spbPagProximo.Name then
        mrSpreadInfo.Acao := PAGINA_FRENTE
  else if lspbBotao.Name = spbUltimo.Name then
        mrSpreadInfo.Acao := ULTIMA_PAGINA;

  untSeleciona.gf_Selec_Spread_Montar(lsvSelec, mrColunasSpread, mrSpreadInfo, mrID_Linhas, masColunas_Chave);

  untSelPar.gp_SelPar_Ativar_ScrollBtns(mrSpreadInfo, maSpeedButs, lsvSelec);

  if lsvSelec.CanFocus then
    lsvSelec.SetFocus;

  pnlEspera.Visible := False;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.edtChaveChange
'     Prepara a tela
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.edtChaveChange(Sender: TObject);
var
  lbChave_Valida: Boolean;
begin

  mrSpreadInfo.MudouProcura := True;
  mrSpreadInfo.ChaveProcura := Trim(edtChave.Text);
  lbChave_Valida := (mrSpreadInfo.ChaveProcura <> VAZIO);
  btnEncontrar.Enabled := lbChave_Valida;
  btnEncontrar.Default := lbChave_Valida;
  btnConsultar.Default := not lbChave_Valida;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.btnAlterarClick
'     verifica se pode alterar
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.btnAlterarClick(Sender: TObject);
label FIM_btnAlterarClick;
var
  lvCodigoSelecionado  : Variant;
  lvDescricaoSelecionado  : Variant;
  lsRecursoLock        : String;
  lsSql                : String;

  liConta:          Integer;
  liConDependentes: Integer;
  liColIndice1: Integer;
  liColIndice2: Integer;
begin

  dmDados.giStatus := STATUS_OK;

  Screen.Cursor := crHOURGLASS;
  { se a conexao nao esta pronta}
  if (mrSpreadInfo.Conexao = 0) or (mrSpreadInfo.Conexao = IOPTR_NOPOINTER) then
    goto FIM_btnAlterarClick;

  lvCodigoSelecionado := lsvSelec.Selected.Caption;
  lvDescricaoSelecionado := Trim(lsvSelec.Selected.SubItems.Text);

  if lvCodigoSelecionado = VAZIO then
    goto FIM_btnAlterarClick;

  {Chama a fun��o auxiliar de montagem da estrutura �nibus}
  untTelaVar.gp_TelaVar_Passar(grTelaVar.P);
  if dmDados.giStatus <> STATUS_OK then
    goto FIM_btnAlterarClick;

  grTelaVar.Comando_MDI := ED_CONSULTA;
  if untSelPar.gf_SelPar_Pode_Atualizar(grTelaVar.P.Cod_Seguranca, TIPO_ALTERA) then
  begin
    lsRecursoLock := grTelaVar.P.Nome_Tabela  + '_' + lvCodigoSelecionado;
    case untParam.gf_Param_Reter(lsRecursoLock, False) of
      LOCK_RETORNO_ATUALIZA:
      begin
        grTelaVar.Comando_MDI := ED_ALTERAR;
        grTelaVar.Recurso_Retido := True;
      end;
      LOCK_RETORNO_CONSULTA:
      begin
        { Go ahead}
      end;
    else { Lock_Erro ou Lock_Nao_CONSULTA}
        Screen.Cursor := crDefault;
        Exit;
    end;
  end
  else
  begin
    if not untLock.gf_Lock_Checar(lsRecursoLock) then
      { J� foi avisado}
      goto FIM_btnAlterarClick;
  end;

//#######################
  { Passa o dynaset}
//  lsSql := untParam.gf_Param_Sql(grTelaVar.P.Sigla_Tabela, VAZIO, gaParam_Colunas);
//  grTelaVar.DynaID := dmDados.ExecutarSelect(lsSql);
//#######################

  mp_Selec_Nav_Inicializar(lvCodigoSelecionado, lvDescricaoSelecionado);
//#######################
  grTelaVar.DynaID := grTelaVar.Nav.iConexao;
//#######################

  dlgCadSelecionar.Close;
  Exit;

FIM_btnAlterarClick:
  { se o recurso estiver travado}
  if grTelaVar.Recurso_Retido then
  begin
    if not untLock.gf_Lock_Liberar(lsRecursoLock) then
    begin
      { Erro interno}
    end;
  end;
  if (grTelaVar.DynaID <> 0) and (grTelaVar.DynaID <> IOPTR_NOPOINTER) then
  begin
    dmDados.FecharConexao(grTelaVar.DynaID);
    grTelaVar.DynaID := IOPTR_NOPOINTER;
  end;
  Screen.Cursor := crDefault;
  Exit;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.mp_Selec_Nav_Inicializar
'
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.mp_Selec_Nav_Inicializar(var pvCodigo: Variant; var pvDescricao: Variant);
var
  lrChave: TumaChave;
  lsSql  : String;
  laChaves: array[0..5] of String;
  lsWhere: string;
begin

  lrChave.iNumCols := 2;
  lrChave.sNome[1] := grTelaVar.P.Coluna_Codigo;
  lrChave.vValor[1] := pvCodigo;
  lrChave.sNome[2] := grTelaVar.P.Coluna_Descricao;
  lrChave.vValor[2] := pvDescricao;

  if VarIsNull(pvCodigo) then
    pvCodigo := VAZIO;

  { se estiver filtrado }
  if dmDados.gaConexao[mrSpreadInfo.Conexao].sWhere <> VAZIO then
    lsWhere := dmDados.gaConexao[mrSpreadInfo.Conexao].sWhere;

  laChaves[0] := grTelaVar.P.Coluna_Codigo;
  laChaves[1] := grTelaVar.P.Coluna_Descricao;
  lsSql := untParam.gf_Param_Sql(grTelaVar.P.Sigla_Tabela, VAZIO, laChaves);

  if lsWhere <> VAZIO then
    if Pos('WHERE', lsSql) <> 0 then
      lsSql := lsSql + ' AND ' + lsWhere
    else
      lsSql := lsSql + ' WHERE ' + lsWhere;

  if gbCodigo then
  begin
    lsSql := lsSql + ' ORDER BY Base.' + grTelaVar.p.Coluna_Codigo;
    { guarda a coluna de ordenacao para navegacao }
    mrSpreadInfo.ColunaOrdem := 0;
  end
  else
  begin
    lsSql := lsSql + ' ORDER BY Base.' + grTelaVar.p.Coluna_Descricao + ', Base.' + grTelaVar.p.Coluna_Codigo;
    { guarda a coluna de ordenacao para navegacao }
    mrSpreadInfo.ColunaOrdem := 1;
  end;

  if not untNaveg.gf_Nav_Iniciar(grTelaVar.Nav, lsSql, lrChave, mrSpreadInfo.ColunaOrdem + 1) then
    untNaveg.gp_Nav_OnError(grTelaVar.Nav, grTelaVar.Nav.iUltimoErro);

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.lsvSelecColumnClick
'
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.lsvSelecColumnClick(Sender: TObject;
  Column: TListColumn);
begin

  Screen.Cursor := crHOURGLASS;

  if Column.Index = 0 then
  begin
    lblChave.Caption := 'Digite a chave de pesquisa para a coluna ''C�digo'' e pressione [Encontrar]';
    gbCodigo := True;
    mrSpreadInfo.ColunaOrdem := 0;
  end;

  if Column.Index = 1 then
  begin
    lblChave.Caption := 'Digite a chave de pesquisa para a coluna ''Descri��o'' e pressione [Encontrar]';
    gbCodigo := False;
    mrSpreadInfo.ColunaOrdem := 1;
  end;

  mp_Selec_Atualizar;

  Screen.Cursor := crDefault;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.btnEncontrarClick
'
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.btnEncontrarClick(Sender: TObject);
begin
  
  Screen.Cursor := crHOURGLASS;
  pnlEspera.Visible := True;
  dlgCadSelecionar.Refresh;

  if gbCodigo then
  begin
    mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[1].Nome;
    if (gpAlinha <> VAZIO) and (gpAlinhaTamanho >= Length(edtChave.Text)(*<> 0*)) Then
      if gpAlinha = ALINHA_ZERO then
        edtChave.TEXT := untFuncoes.gf_Zero_Esquerda(Trim(edtChave.TEXT), gpAlinhaTamanho);
    edtChave.TEXT := mrSpreadInfo.ChaveProcura;
  end
  else
    mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[2].Nome;

  if untSeleciona.gf_Selec_Spread_Montar(lsvSelec, mrColunasSpread, mrSpreadInfo, mrID_Linhas, masColunas_Chave) then
    mrSpreadInfo.MudouProcura := False;

  untSelPar.gp_SelPar_Ativar_ScrollBtns(mrSpreadInfo, maSpeedButs, lsvSelec);

  if lsvSelec.CanFocus then
    lsvSelec.SetFocus;

  pnlEspera.Visible := False;
  Screen.Cursor := crDefault;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.btnConsultarClick
'     verifica se pode consultar
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.btnConsultarClick(Sender: TObject);
var
  lvCodigoSelecionado: Variant;
  lvDescricaoSelecionado  : Variant;
  lsSql:               string;
  lsRecursoLock:       string;

  liConta:          Integer;
  liConDependentes: Integer;
  liColIndice1:      Integer;
  liColIndice2:     Integer;
begin

  dmDados.giStatus := STATUS_OK;

  Screen.Cursor := crHOURGLASS;
  { se a conexao nao esta pronta}
  if (mrSpreadInfo.Conexao = 0) or (mrSpreadInfo.Conexao = IOPTR_NOPOINTER) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;

  if lsvSelec.Selected <> nil then
    lvCodigoSelecionado := lsvSelec.Selected.Caption
  else
    lvCodigoSelecionado := '0';
  lvDescricaoSelecionado := Trim(lsvSelec.Selected.SubItems.Text);

  if lvCodigoSelecionado = VAZIO then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;

  // Chama a fun��o auxiliar de montagem da estrutura �nibus
  untTelaVar.gp_TelaVar_Passar(grTelaVar.P);
  if dmDados.giStatus <> STATUS_OK then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;

  lsRecursoLock := grTelaVar.P.Nome_Tabela + '_' + lvCodigoSelecionado;

  if not untLock.gf_Lock_Checar(lsRecursoLock) then
  begin
    // J� foi avisado
    Screen.Cursor := crDefault;
    Exit;
  end;

//#######################
//  lsSql := untParam.gf_Param_Sql(grTelaVar.P.Sigla_Tabela, VAZIO, gaParam_Colunas);
//  grTelaVar.DynaID := dmDados.ExecutarSelect(lsSql);
//#######################

  mp_Selec_Nav_Inicializar(lvCodigoSelecionado, lvDescricaoSelecionado);
//#######################
  grTelaVar.DynaID := grTelaVar.Nav.iConexao;
//#######################

  Screen.Cursor := crDefault;

  // Fecha o dialog box e retorna
  dlgCadSelecionar.Close;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.btnExcluirClick
'     exclui um registro
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.btnExcluirClick(Sender: TObject);
label FIM_btnExcluirClick;
var
  lrChave:             TumaChave;
  lsCriterio:          String;
  lvCodigoSelecionado: Variant;
  lvDescricaoSelecionado  : Variant;
  lsRecursoLock:       String;
  lsTabela:            String;
  lsSQL:               String;
  i:                   Integer;
  laChaves:  array[0..5] of String;

  liConta:          Integer;
  liConDependentes: Integer;
  liColIndice1:      Integer;
  liColIndice2:     Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    Screen.Cursor := crHOURGLASS;

    { se a conexao nao esta pronta}
    if (mrSpreadInfo.Conexao = 0) or (mrSpreadInfo.Conexao = IOPTR_NOPOINTER) then
      goto FIM_btnExcluirClick;

    lvCodigoSelecionado := lsvSelec.Selected.Caption;
    lvDescricaoSelecionado := Trim(lsvSelec.Selected.SubItems.Text);

    if lvCodigoSelecionado = VAZIO then
      goto FIM_btnExcluirClick;

    //Chama a fun��o auxiliar de montagem da estrutura �nibus
    untTelaVar.gp_TelaVar_Passar(grTelaVar.P);
    if dmDados.giStatus <> STATUS_OK then
      goto FIM_btnExcluirClick;

// ===============================
    // Pede confirma��o ao usu�rio da exclus�o
    dmDados.gaMsgParm[0] := grTelaVar.P.Titulo_Tela + ' ' + lvCodigoSelecionado;
    i := dmDados.MensagemExibir('', 4052);
    // MsgN�o
    if i = IDNO then
      goto FIM_btnExcluirClick;
// ===============================

    grTelaVar.Comando_MDI := ED_CONSULTA;
    if untSelPar.gf_SelPar_Pode_Atualizar(grTelaVar.P.Cod_Seguranca, TIPO_EXCLUI) then
    begin
      lsRecursoLock := grTelaVar.P.Nome_Tabela + '_' + lvCodigoSelecionado;
//    lRecursoLock = gpTelaVar.P.Nome_Tabela
      case untParam.gf_Param_Reter(lsRecursoLock, False) of
        LOCK_RETORNO_ATUALIZA:
        begin
          grTelaVar.Comando_MDI := ED_ALTERAR;
          grTelaVar.Recurso_Retido := True;
        end;
        LOCK_RETORNO_CONSULTA:
          // Nao excluir
          Exit;
      else // Lock_Erro ou Lock_Nao_CONSULTA
        Screen.Cursor := crDefault;
        Exit;
      end;
    end
    else
      if not untLock.gf_Lock_Checar(lsRecursoLock) then
        // J� foi avisado
        goto FIM_btnExcluirClick;

    // Passa o dynaset
//#######################
//    lsSQL := untParam.gf_Param_Sql(grTelaVar.P.Sigla_Tabela, VAZIO, gaParam_Colunas);
//    grTelaVar.DynaID := dmDados.ExecutarSelect(lsSQL);
//#######################

    mp_Selec_Nav_Inicializar(lvCodigoSelecionado, lvDescricaoSelecionado);
//#######################
//    grTelaVar.DynaID := grTelaVar.Nav.iConexao;
//#######################
    grTelaVar.DynaID := mrSpreadInfo.Conexao;
// ===============================

    // Inicia transa��o
    dmDados.TransBegin(cboCadastro.Text + '_cmdExcluir_CLick');

    lrChave.iNumCols := 1;
    lrChave.sNome[1] := grTelaVar.P.Coluna_Codigo;
    lrChave.vValor[1] := lvCodigoSelecionado;

    laChaves[0] := grTelaVar.P.Coluna_Codigo;

    lsCriterio := grTelaVar.P.Coluna_Codigo + ' = ''' + lvCodigoSelecionado + '''';

    dmDados.gsRetorno := dmDados.AtivaPrimeiraComp(grTelaVar.DynaID, lsCriterio, laChaves, IOFLAG_GET);
    if not dmDados.Status(grTelaVar.DynaID, IOSTATUS_NOMATCH) then
    begin
      grTelaVar.Cod_Valor := lvCodigoSelecionado;
      if not untParam.gf_Param_Pode_Excluir(grTelaVar.P, grTelaVar.Cod_Valor, grTelaVar.DynaID) then
      begin
        // Rollback and Exit Function
        dmDados.TransRollback(cboCadastro.Text + '_cmdExcluir_CLick');
        dmDados.gsStatusMensagem := 'Tabela n�o identific�vel no SQL';
        DmDados.ErroTratar('btnExcluirClick - uniCadSeleciona.FRM');
        Exit;
      end;

      if grTelaVar.P.Tipo_Objeto = 'Q' then
        lsTabela := Copy(grTelaVar.P.Nome_Tabela, 3, Length(grTelaVar.P.Nome_Tabela) - 3)
      else
        lsTabela := grTelaVar.P.Nome_Tabela;

      // Excluir Em Cascata
      untParam.gp_Param_Excluir_EmCascata(grTelaVar.P, grTelaVar.Cod_Valor);

      lsSQL := dmDados.ExcluirStr(grTelaVar.DynaID, lsTabela); // gpTelaVar.P.Nome_Tabela)
      dmDados.gsRetorno := dmDados.ExecutarSQL(lsSQL);
    end;

    dmDados.TransCommit(cboCadastro.Text + '_cmdExcluir_CLick');

    dmDados.FecharConexao(grTelaVar.DynaID);
    grTelaVar.DynaID := 0;
    for liConta := 1 to MAX_NUM_DYNATEMPID do
    begin
      dmDados.FecharConexao(grTelaVar.DynaTempID[liConta]);
      grTelaVar.DynaTempID[liConta] := 0;
    end;

    mp_Selec_Regerar_Lista;

    // Se temos um recurso retido temos de liber�-lo
    if grTelaVar.Recurso_Retido then
      if not untLock.gf_Lock_Liberar(grTelaVar.P.Nome_Tabela + '_' + lvCodigoSelecionado) Then
      begin
        dmDados.gaMsgParm[0] := grTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('btnExcluirClick - UNICADSELECIONA.FRM', 4300);
      end;
FIM_btnExcluirClick:
  if grTelaVar.Recurso_Retido then
    if not untLock.gf_Lock_Liberar(lsRecursoLock) then
      // Erro interno

  if (grTelaVar.DynaID <> 0) and (grTelaVar.DynaID <> IOPTR_NOPOINTER) then
    dmDados.FecharConexao(grTelaVar.DynaID);

  Screen.Cursor := crDefault;
//====================================
  except
    Screen.Cursor := crDefault;
    dmDados.ErroTratar('btnExcluirClick - uniCadSeleciona.FRM');
  end;

  Exit;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.btnNovoClick
'     inclui um novo registro
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.btnNovoClick(Sender: TObject);
label FIM_btnNovoClick;
var
  lsSql:         String;
  lsRecursoLock: String;
  lvCodigoSelecionado: Variant;
  lvDescricaoSelecionado  : Variant;

  liConta:          Integer;
  liConDependentes: Integer;
  liColIndice1:     Integer;
  liColIndice2:     Integer;
begin

  try
    dmDados.giStatus := STATUS_OK;

    if (mrSpreadInfo.Conexao = 0) or (mrSpreadInfo.Conexao = IOPTR_NOPOINTER) then
      Exit;

    Screen.Cursor := crHOURGLASS;

    untTelaVar.gp_TelaVar_Passar(grTelaVar.P);
    if dmDados.giStatus <> STATUS_OK then
      goto FIM_btnNovoClick;

    if not untSelPar.gf_SelPar_Pode_Atualizar(grTelaVar.P.Cod_Seguranca, TIPO_INCLUI) then
    begin
      dmDados.MensagemExibir(VAZIO, 4205);
      Screen.Cursor := crDefault;
      Exit;
    end;

    lsRecursoLock := grTelaVar.P.Nome_Tabela;

//    case untParam.gf_Param_Reter(lsRecursoLock, False) of
//      LOCK_RETORNO_ATUALIZA:
//      begin
        grTelaVar.Comando_MDI := ED_NOVO;
//        grTelaVar.Recurso_Retido := True;
//      end;
//    else // Lock_Erro ou Lock_Nao_CONSULTA ou LOCK_CONSULTA
//      begin
//        Screen.Cursor := crDefault;
//        Exit;
//      end;
//    end;

    // Passa o dynaset
//#######################
//    lsSql := untParam.gf_Param_Sql(grTelaVar.P.Sigla_Tabela, VAZIO, gaParam_Colunas);
//    grTelaVar.DynaID := dmDados.ExecutarSelect(lsSql);
//#######################

    lvCodigoSelecionado := '0';
    lvDescricaoSelecionado := VAZIO;
    mp_Selec_Nav_Inicializar(lvCodigoSelecionado, lvDescricaoSelecionado);
//#######################
    grTelaVar.DynaID := grTelaVar.Nav.iConexao;
//#######################

    // Fecha o dialog box e retorna
    dlgCadSelecionar.Close;

    Exit;

FIM_btnNovoClick:
    if grTelaVar.Recurso_Retido then
      if not untLock.gf_Lock_Liberar(grTelaVar.P.Nome_Tabela + '_' + lvCodigoSelecionado) then
        // Erro interno
    dmDados.FecharConexao(grTelaVar.DynaID);
    Screen.Cursor := crDefault;

  except
    // N�o pode fazer unload enquanto se pinta
    Screen.Cursor := crDefault;
    dmDados.ErroTratar('btnNovoClick - uniCadSeleciuona.FRM');
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.lsvSelecClick
'     atualiza botoes de navegacao
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.lsvSelecClick(Sender: TObject);
begin
  { atualiza botoes de navegacao}
  untSelPar.gp_SelPar_Ativar_ScrollBtns(mrSpreadInfo, maSpeedButs, lsvSelec)

end;

{'-------------------------------------------------------------------------------------------------
' TdlgCadSelecionar.cboModulosClick
'     preenche combo de cadastro
'-------------------------------------------------------------------------------------------------}
procedure TdlgCadSelecionar.cboModulosClick(Sender: TObject);
begin

  cboCadastro.Enabled := True;
  cboCadastro.Text := VAZIO;
  untParam.gp_Param_EstufaComboParametrosMDI(cboCadastro, cboModulos.Text);

end;

procedure TdlgCadSelecionar.vasSelecBlockSelected(Sender: TObject;
  BlockCol, BlockRow, BlockCol2, BlockRow2: Integer);
begin
  { atualiza botoes de navegacao}
  untSelPar.gp_SelPar_Ativar_ScrollBtns(mrSpreadInfo, maSpeedButs, lsvSelec)

end;


end.
