unit uniCodDescrAbrev;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls,
  uniConst;

type
  TformCodDescrAbrev = class(TForm)
    pnlFundo: TPanel;
    PntComunicador: TPaintBox;
    lblCodigo: TLabel;
    lblDescricao: TLabel;
    edtCodigo: TEdit;
    edtDescricao: TEdit;
    lblAbreviatura: TLabel;
    edtAbreviatura: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtCodigoChange(Sender: TObject);
    procedure edtCodigoEnter(Sender: TObject);
    procedure PntComunicadorPaint(Sender: TObject);
    procedure edtDescricaoKeyPress(Sender: TObject; var Key: Char);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }

    {' Vari�veis de controle da inst�ncia do Form}
    mrTelaVar: TumaTelaVar;            {' vari�vel que guarda os dados da tela local}

  public
    { Public declarations }
  end;

var
  formCodDescrAbrev: TformCodDescrAbrev;

implementation

uses uniDados, uniMDI, uniTelaVar, uniLock, uniCombos, uniEdit, uniGlobal;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.FormShow(Sender: TObject);
begin

  dmDados.giStatus := STATUS_OK;

  try

    {' muda o ponteiro do mouse para ampulheta}
    Screen.cursor := crHOURGLASS;

    { redimensiona a tela}
    Width  := 408;
    Height := 99;

    {'guarda valores da TelaVar}
    mrTelaVar := grTelaVar;
    mrTelaVar.Pode_Encadear := False;
    mrTelaVar.Nav.bNavegavel := True;

    {' Monto Titulo da Janela}
    mrTelaVar.P.Titulo_Tela := 'Cadastro da ' + mrTelaVar.P.Titulo_Tela;
    Caption := mrTelaVar.P.Titulo_Tela;

    If formMDI.ActiveMDIChild = nil Then
    begin
      Top := 0;
      Left := 0;
    End;

    {' ajusta a identifica��o da tela}
    If mrTelaVar.Comando_MDI = ED_CONSULTA Then
      Caption := Caption + ' - Consulta'
    Else
      If mrTelaVar.Comando_MDI = ED_NOVO Then
        Caption := Caption + ' - Inclus�o';

    Refresh;
    { para desabilitar os eventos change dos controles}
    Enabled := False;

    { abre todas as tabelas secundarias}
    untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, CAD_CODDESCRABREV, TForm(Sender), True, 0);

    {' se for inclus�o}
    If mrTelaVar.Comando_MDI = ED_NOVO Then
      untEdit.gp_Edit_Reposicionar(ED_NOVO, mrTelaVar, CAD_CODDESCRABREV, TForm(Sender));
//    Else
//      untEdit.gp_Edit_Ressincronizar(mrTelaVar.Nav.rReg_Corrente, mrTelaVar, CAD_CODDESCRABREV, TForm(Sender), True, 0);

    untEdit.gp_Edit_Alterado(False, mrTelaVar);
    {' retorna o ponteiro do mouse para o default}
    Enabled := True;
    Screen.Cursor := crDefault;
    { Repinta a tela}
    Repaint;

  except
    screen.cursor := crDefault;
    dmDados.ErroTratar('Form_Load - uniCodDescrAbrev.PAS');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.FormActivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.FormActivate(Sender: TObject);
begin

  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.FormDeactivate       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.FormDeactivate(Sender: TObject);
begin

  If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    grParam_TelaVar.DynaID := 0;

  formMDI.pnlCadastro.Caption := VAZIO;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.FormCloseQuery       Prepara p/ fechar a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin

  {'Se temos uma tela de inclus�o ou altera��o com dados alterados}
  If (mrTelaVar.Alterado = True) And ((mrTelaVar.Comando_MDI = ED_NOVO) Or (mrTelaVar.Comando_MDI = ED_ALTERAR)) Then
    {' d�-se ao usu�rio a chance de gravar ou cancelar a saida}
    CanClose := untEdit.gf_Edit_Acionar(ACAO_FECHAR, mrTelaVar, CAD_CODDESCRABREV, formCodDescrAbrev);

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.FormResize       Prepara a tela para nao esconder os dados
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.FormResize(Sender: TObject);
begin

  if TformCodDescrAbrev(Sender).WindowState <> wsMinimized Then
  begin
    if (TformCodDescrAbrev(Sender).Width < pnlFundo.Width + 39) and (TformCodDescrAbrev(Sender).WindowState <> wsMinimized) then
      {'Aumenta o tamanho da janela p/aparecer a barra de ferr.}
      TformCodDescrAbrev(Sender).Width := pnlFundo.Width + 39;

    TformCodDescrAbrev(Sender).Refresh;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  liConta: Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    {' Se temos um recurso retido temos de liber�-lo}
    If mrTelaVar.Recurso_Retido = True Then
      If Not untLock.gf_Lock_Liberar(mrTelaVar.P.Nome_Tabela + '_' + VarToStr(mrTelaVar.Nav.rReg_Corrente.vValor[1])) Then
      begin             

        dmDados.gaMsgParm[0] := mrTelaVar.P.Nome_Tabela;
        dmDados.MensagemExibir('Form_Unload - formCodDescrAbrev.FRM',4300);
      End;

    {' Fechando todos os dynasets envolvidos}
    If grParam_TelaVar.DynaID = mrTelaVar.DynaID Then
    begin
      grParam_TelaVar.DynaID := 0;
      grParam_TelaVar.DynaPesquisaID := 0;
      for liConta := 1 to MAX_NUM_DYNATEMPID do
        grParam_TelaVar.DynaTempID[liConta] := 0;
      gfrmTV := nil;
    End;
    dmDados.FecharConexao(mrTelaVar.DynaID);
    dmDados.FecharConexao(mrTelaVar.DynaPesquisaID);
    for liConta := 1 to MAX_NUM_DYNATEMPID do
      dmDados.FecharConexao(mrTelaVar.DynaTempID[liConta]);

    dmDados.FecharConexao(mrTelaVar.Nav.iConexao);

    formMDI.pnlCadastro.Caption := VAZIO;

    {' For�a a atualiza��o das barras (Toolbar/Status)}
    formMDI.Invalidate;

    formMDI.gp_AtualizaBarraBotoes;
//    InvalidateRect(formMDI.cobMenu.Handle, nil, False);
    formMDI.gp_AtualizaBarraStatus;
//    InvalidateRect(formMDI.stbMDI.Handle, nil, False);

  except
    dmDados.ErroTratar('FormClose - uniESTOQUE.PAS');
  end;

  // destroi a janela
  Action := caFree;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.edtCodigoChange       fez mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.edtCodigoChange(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  If (Not mrTelaVar.Reposicionando) And (mrTelaVar.Comando_MDI <> ED_CONSULTA) And (Not mrTelaVar.Bloqueado) Then
    {' altera��o efetuada}
    untEdit.gp_Edit_Alterado(True, mrTelaVar);

  {' se for o campo Codigo no modo de Inclus�o}
  If (ledtEdit.Name = 'edtCodigo') And (mrTelaVar.Comando_MDI = ED_NOVO) Then
  begin
    mrTelaVar.Cod_Valor := ledtEdit.Text;
  End;
 
end;

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.edtCodigoEnter       guarda valor antigo
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.edtCodigoEnter(Sender: TObject);
var
  ledtEdit: TEdit;
begin

  ledtEdit := TEdit(Sender);

  {' guarda o valor original do combo}
  gsAreaDesfaz := ledtEdit.Text;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.PntComunicadorPaint       Chama funcao que controla os eventos
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.PntComunicadorPaint(Sender: TObject);
begin

  {' Nosso "gancho" para comunica��o entre a MDI-m�e e esta filha}
  untEdit.gp_Edit_Metodos(mrTelaVar, CAD_CODDESCRABREV, formCodDescrAbrev)

end;


{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.edtDescricaoKeyPress       Se for consulta nao edita
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.edtDescricaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  {' se for consulta n�o edita}
  If (mrTelaVar.Comando_MDI = ED_CONSULTA) Or mrTelaVar.Bloqueado Then
    Key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TformCodDescrAbrev.FormPaint       Para saber se � novo
 *
 *-----------------------------------------------------------------------------}
procedure TformCodDescrAbrev.FormPaint(Sender: TObject);
begin

  gfrmTV := TForm(Sender);
  untTelaVar.gp_TelaVar_Toolbar_Atualizar(mrTelaVar);

  formMDI.pnlCadastro.Caption := mrTelaVar.P.Titulo_Tela;

end;

end.
