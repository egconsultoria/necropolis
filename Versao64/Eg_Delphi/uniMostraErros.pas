unit uniMostraErros;

interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const
  LEFT_AJUSTE = 60; { tamanho do ajuste p/ tirar o botao Ajuda}
  ERR_LEFT_SALVAR = 64 + LEFT_AJUSTE; {'in�cio do bot�o Salvar}
  ERR_LEFT_FECHAR = 242 + LEFT_AJUSTE; {'in�cio do bot�o Fechar}
  ERR_LEFT_AJUDA = 377 + LEFT_AJUSTE;  {'in�cio do bot�o Ajuda}

type
 {*======================================================================
 *            CLASSES
 *======================================================================}
  TdlgMostraErros = class(TForm)
    grdErros: TStringGrid;
    btnFechar: TButton;
    btnAjuda: TButton;
    btnSalvar: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure btnAjudaClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgMostraErros: TdlgMostraErros;

implementation

uses uniDados;

{$R *.DFM}


{*-----------------------------------------------------------------------------
 *  TdlgMostraErros.FormShow       atualiza o help
 *
 *-----------------------------------------------------------------------------}
procedure TdlgMostraErros.FormShow(Sender: TObject);
begin

    btnAjuda.HelpContext := dlgMostraErros.HelpContext;
    Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgMostraErros.FormClose       inicializa variavel
 *
 *-----------------------------------------------------------------------------}
procedure TdlgMostraErros.FormClose(Sender: TObject;
                                    var Action: TCloseAction);
begin
    {'inicializa a vari�vel de contexto para a pr�xima chamada}
    giErros_Modo := ERRO_MODO_NORMAL;

end;

{*-----------------------------------------------------------------------------
 *  TdlgMostraErros.FormActivate       prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgMostraErros.FormActivate(Sender: TObject);
begin

    {'se for procedimento de consist�ncia (ERRO_MODO_NORMAL)=> N�O permite salvar
    'se for par�metros (ERRO_MODO_PARAMETROS) e n�o houve erro cr�tico (tipo 2) => permite salvar
    'se aplica��o de cadastramento e n�o houve erro cr�tico => permite salvar}
    If (((giErros_Modo = ERRO_MODO_PARAMETROS) Or
        (giErros_Modo = ERRO_MODO_CADASTRAIS)) And
        (gbErros_Criticos = False)) Then
    begin
        btnFechar.Caption := 'Cancelar';
        btnFechar.HelpContext := 35;
        btnSalvar.Visible := True;
        btnSalvar.Left := ERR_LEFT_SALVAR;
        btnFechar.Left := ERR_LEFT_FECHAR;
        btnAjuda.Left := ERR_LEFT_AJUDA;
        If giNumErros > 1 Then
            btnSalvar.Caption := '&Salvar com Erros'
        Else
            btnSalvar.Caption := '&Salvar com Erro';
    end
    Else
    begin
        btnFechar.HelpContext := 43;
        btnFechar.Caption := 'Fe&char';
        btnSalvar.Visible := False;
        btnFechar.Left := ERR_LEFT_FECHAR - 60;
        btnAjuda.Left := ERR_LEFT_AJUDA - 60;
    End;

    {'assume que o usu�rio n�o vai querer gravar com erros (vai escolher cancelar)}
    giErros_Ignorar := ERRO_CANCELAR;

    btnFechar.SetFocus;

end;

{*-----------------------------------------------------------------------------
 *  TdlgMostraErros.btnAjudaClick       apresenta o help
 *
 *-----------------------------------------------------------------------------}
procedure TdlgMostraErros.btnAjudaClick(Sender: TObject);
//var
//  r: LongInt;
begin

//    r := WinHelp(Me.hWnd, app.HelpFile, HELP_CONTEXT, 31)

end;

{*-----------------------------------------------------------------------------
 *  TdlgMostraErros.btnFecharClick       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgMostraErros.btnFecharClick(Sender: TObject);
begin
  {fecha a tela}
  dlgMostraErros.Close;
end;

{*-----------------------------------------------------------------------------
 *  TdlgMostraErros.btnSalvarClick      atualiza variavel para ignorar os erros
 *
 *-----------------------------------------------------------------------------}
procedure TdlgMostraErros.btnSalvarClick(Sender: TObject);
begin

  {'indica que o usu�rio deseja gravar os dados, ignorando os erros ocorridos}
  giErros_Ignorar := ERRO_IGNORAR;

  {fecha a tela}
  dlgMostraErros.Close;

end;

end.
