�
 TDLGGRUPO 0�  TPF0	TdlgGrupodlgGrupoLeft� Top� BorderIcons BorderStylebsDialogCaptionGruposClientHeight9ClientWidthFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPanelpnlGrupoLeftTopWidth�Height
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder  TLabellblGrupoLeftTopWidthHeightCaptionGrupoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel
lblRotinasLeftTopHWidthgHeightCaptionRotinas e SubRotinasFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxcboNomeGrupoLeftTop Width9HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight	MaxLength
ParentFontTabOrder OnClickcboNomeGrupoClickOnExitcboNomeGrupoExit	OnKeyDowncboNomeGrupoKeyDown
OnKeyPresscboNomeGrupoKeyPress  	TGroupBoxgboxPermissoesLeftuTopWidthiHeight� Caption
Permiss�esFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TBevelbvlDivisao1Left Top� WidthiHeight	Shape	bsTopLine  TBevelbvlDivisao2Left Top� WidthiHeight	Shape	bsTopLine  	TCheckBoxchkConsultaLeftTopWidthQHeightCaptionConsultaTabOrder OnClickchkConsultaClick  	TCheckBox	chkAlteraLeftTop2WidthQHeightCaptionAlteraTabOrderOnClickchkConsultaClick  	TCheckBox	chkIncluiLeftTopKWidthQHeightCaptionIncluiTabOrderOnClickchkConsultaClick  	TCheckBoxchkEmiteLeftTop� WidthQHeightCaptionEmiteTabOrderOnClickchkConsultaClick  	TCheckBox	chkExcluiLeftTopdWidthIHeightCaptionExcluiTabOrderOnClickchkConsultaClick  	TCheckBoxchkTodosLeftTop}WidthIHeightCaptionTodosTabOrderOnClickchkTodosClick  	TCheckBox
chkExecutaLeftTop� WidthIHeightCaptionExecutaTabOrderOnClickchkConsultaClick   	TTreeView	trvSubRotLeftTopXWidthQHeight� ReadOnly	Indent
Items.Data
m                 ��������       Item            ��������       SubItem            ��������        SubItemFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold TabOrderOnClicktrvSubRotClick
OnDblClicktrvSubRotDblClickOnKeyUptrvSubRotKeyUp
ParentFont   TButtonbtnNovoLeftTopWidthKHeightCaption&NovoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnNovoClick  TButton
btnExcluirLeft� TopWidthKHeightCaption&ExcluirEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnExcluirClick  TButton	btnFecharLeft� TopWidthKHeightCancel	CaptionFe&charFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnFecharClick  TButtonbtnAjudaLeft�TopWidthKHeightCaptionAj&udaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnAjudaClick  TButton	btnSalvarLeftXTopWidthKHeightCaption&SalvarEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnSalvarClick   