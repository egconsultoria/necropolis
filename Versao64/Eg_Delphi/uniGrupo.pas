unit uniGrupo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Grids, Outline, ComCtrls,
  uniConst;

type
  TdlgGrupo = class(TForm)
    pnlGrupo: TPanel;
    lblGrupo: TLabel;
    cboNomeGrupo: TComboBox;
    btnNovo: TButton;
    btnExcluir: TButton;
    btnFechar: TButton;
    btnAjuda: TButton;
    lblRotinas: TLabel;
    gboxPermissoes: TGroupBox;
    chkConsulta: TCheckBox;
    chkAltera: TCheckBox;
    chkInclui: TCheckBox;
    chkEmite: TCheckBox;
    btnSalvar: TButton;
    chkExclui: TCheckBox;
    chkTodos: TCheckBox;
    chkExecuta: TCheckBox;
    bvlDivisao1: TBevel;
    bvlDivisao2: TBevel;
    trvSubRot: TTreeView;
    procedure btnFecharClick(Sender: TObject);
    procedure chkTodosClick(Sender: TObject);
    procedure chkConsultaClick(Sender: TObject);
    procedure cboNomeGrupoClick(Sender: TObject);
    procedure cboNomeGrupoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cboNomeGrupoKeyPress(Sender: TObject; var Key: Char);
    procedure cboNomeGrupoExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAjudaClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure trvSubRotClick(Sender: TObject);
    procedure trvSubRotDblClick(Sender: TObject);
    procedure trvSubRotKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    mbKeyUpDown: Boolean;

    miSsSisGrup: integer;
    miDsSisSubRot: Integer;
    miDsGrupo: Integer;
    miDsUsuario_Grupo: Integer;
    miSsAceRot: Integer;
    miDsAcRotina: Integer;

    mbAlterado : Boolean;
    mbCancel: Boolean;
    msNomeAreaDesfaz: String;
    msNomeAreaDesfaz2: String;
    msNomeAreaDesfaz3: String;
    msRotina : String;
    msSubRotina : String;

    mbNovoGrupo: Boolean;
    mbNovaRotina: Boolean;
    msChave_Aux: String;
    msChave_Aux2: String;
    msChave_Aux3: String;
    mliNum_Grupo_Aux: LongInt;
    mliNum_Grupo_Sel: LongInt;
    mtnRotinaAnterior: TTreeNode;
    msMens_Val: String;

    procedure lp_ConfirmaGrupo;
    procedure lp_CancelaGrupo;
    procedure lp_ComboGrupo_Montar;
    procedure lp_Grupo_Salvar;
    procedure lp_Grupo_GravAutomatica;
    procedure lp_Grupo_Montar;
    procedure lp_Grupo_Excluir;
    procedure lp_NovoGrupo;
    procedure lp_SnapShot_AceRot_Montar;
    function lp_NodePath(Node: TTreeNode): String;
    procedure lp_Tela_Montar;
    procedure lp_NovoAceRot;
    procedure lp_AceRot_Salvar;
    procedure lp_Out_Montar;

  public
    { Public declarations }
  end;

var
  dlgGrupo: TdlgGrupo;

implementation

uses uniDados, uniMDI, uniErros;

{$R *.DFM}


{*-----------------------------------------------------------------------------
 *  TdlgGrupo.btnFecharClick               Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.btnFecharClick(Sender: TObject);
begin
  { fecha a tela}
  dlgGrupo.Close;
end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.chkTodosClick              Prepara a tela mudando alguns checks
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.chkTodosClick(Sender: TObject);
var
  liIndColPConsulta: Integer;
  liIndColPAltera: Integer;
  liIndColPInclui: Integer;
  liIndColPExclui: Integer;

begin

  Screen.Cursor := crHourGlass;

  { teve alteracao}
  mbAlterado := True;

  { se estiver marcado}
  if chkTodos.Checked then
  begin
    { Atualiza os checks}
    if chkConsulta.Enabled then
      chkConsulta.Checked := True;
    if chkAltera.Enabled then
      chkAltera.Checked := True;
    if chkInclui.Enabled then
      chkInclui.Checked := True;
    if chkExclui.Enabled then
      chkExclui.Checked := True;
    { desabilita os checks}
    chkConsulta.Enabled := False;
    chkAltera.Enabled := False;
    chkInclui.Enabled := False;
    chkExclui.Enabled := False;
  end
  else
  begin
    { habilita os checks}
    liIndColPConsulta := IOPTR_NOPOINTER;
    liIndColPAltera   := IOPTR_NOPOINTER;
    liIndColPInclui   := IOPTR_NOPOINTER;
    liIndColPExclui   := IOPTR_NOPOINTER;

    chkConsulta.Enabled   := (dmDados.ValorColuna(miDsSisSubRot, 'Consulta', liIndColPConsulta) = 'S');
    chkAltera.Enabled     := (dmDados.ValorColuna(miDsSisSubRot, 'Altera', liIndColPAltera) = 'S');
    chkInclui.Enabled     := (dmDados.ValorColuna(miDsSisSubRot, 'Inclui', liIndColPInclui) = 'S');
    chkExclui.Enabled     := (dmDados.ValorColuna(miDsSisSubRot, 'Exclui', liIndColPExclui) = 'S');
  end;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.chkConsultaClick              Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.chkConsultaClick(Sender: TObject);
begin
  { teve alteracao}
  mbAlterado := True;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.cboGrupoClick              Confirma mudanca de grupo
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.cboNomeGrupoClick(Sender: TObject);
begin

  Screen.Cursor := crHourGlass;

    If mbKeyUpDown <> True Then
        {'assume mudan�a de chave}
        lp_ConfirmaGrupo;

    mbKeyUpDown := False;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.cboGrupoKeyDown              Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.cboNomeGrupoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    Case Key of
      KEY_UP:
        mbKeyUpDown := True;
      KEY_DOWN:
        mbKeyUpDown := True;
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.cboGrupoKeyPress              Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.cboNomeGrupoKeyPress(Sender: TObject; var Key: Char);
begin

  Screen.Cursor := crHourGlass;

    Case key of
      {'foi pressionado "Enter"}
      KEY_ENTER:
      begin
        key := #0;        {' limpa a pilha de teclas digitadas}
        lp_ConfirmaGrupo;         {'assume que escolheu bot�o OK}
      end;
      {'foi pressionado "Esc"}
      KEY_ESCAPE:
      begin
        key := #0;        {' limpa a pilha de teclas digitadas}
        lp_CancelaGrupo;   {'assume que escolheu bot�o OK}
      end;
    End;

    If (Length(cboNomeGrupo.Text) > 14) And (cboNomeGrupo.SelText = VAZIO) And (key <> KEY_BACK) Then
       key := #0;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.cboGrupoExit              Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.cboNomeGrupoExit(Sender: TObject);
begin

  Screen.Cursor := crHourGlass;

    If mbKeyUpDown <> True Then
        {'assume mudan�a de chave}
        lp_ConfirmaGrupo;

    mbKeyUpDown := False;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.FormClose             Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  Screen.Cursor := crHourGlass;
  {' Verifica se teve altera��o antes de mudar de registro}
  {' Indica que n�o cancelou a opera��o}
  mbCancel := False;
  lp_Grupo_GravAutomatica;
  { se cancelou a opera��o}
  If mbCancel = True Then
    Action := caNone
  else
  begin

    dmDados.FecharConexao(miSsSisGrup);
    dmDados.FecharConexao(miDsGrupo);
    dmDados.FecharConexao(miDsSisSubRot);
    dmDados.FecharConexao(miDsUsuario_Grupo);
    dmDados.FecharConexao(miSsAceRot);
    dmDados.FecharConexao(miDsAcRotina);

  end;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.btnAjudaClick       mostra help
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.btnAjudaClick(Sender: TObject);
//var
//  r : LongInt;

begin
    { apresenta o help}
//    r := winhelp(hwnd, 'ctacess0.hlp', HELP_CONTENTS, 0);
end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_CancelaGrupo       Cancela a mudanca de novo usuario
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_CancelaGrupo;
begin
    { volta o nome que estava antes}
    cboNomeGrupo.Text := msNomeAreaDesfaz;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_ConfirmaGrupo       Confirma a mudanca de Grupo
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_ConfirmaGrupo;
var
  lsStringUpper: String;
begin

    chkConsulta.Checked   := False;
    chkAltera.Checked     := False;
    chkInclui.Checked     := False;
    chkEmite.Checked      := False;
    chkExclui.Checked     := False;
    chkTodos.Checked      := False;
    chkExecuta.Checked    := False;

    If Trim(cboNomeGrupo.Text) <> VAZIO Then
    begin
        btnSalvar.Enabled := True;
        if not mbNovoGrupo then
        begin
          btnExcluir.Enabled := True;

          chkConsulta.Enabled   := True;
          chkAltera.Enabled     := True;
          chkInclui.Enabled     := True;
          chkEmite.Enabled      := True;
          chkExclui.Enabled     := True;
          chkTodos.Enabled      := True;
          chkExecuta.Enabled    := True;

          trvSubRot.Enabled := True;
        end;

        If Trim(msNomeAreaDesfaz) <> Trim(cboNomeGrupo.Text) Then
        begin
            {'Upper Case}
            lsStringUpper := UpperCase(cboNomeGrupo.Text);
            cboNomeGrupo.Text := lsStringUpper;
            lp_Grupo_Montar;
            lp_Out_Montar;
            {'carrega area aux. p/ desfazer}
            msNomeAreaDesfaz := cboNomeGrupo.Text
        End
    end
    Else
    begin
        btnSalvar.Enabled := False;
        btnExcluir.Enabled := False;
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.btnExcluirClick       Exclui o grupo
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.btnExcluirClick(Sender: TObject);
var
  liResp: Integer;
  liConta: Integer;
  lliNum_Grupo: LongInt;
  liColNum_Grupo: Integer;
  lsCriterio: String;
begin

  Screen.Cursor := crHourGlass;

    If Trim(cboNomeGrupo.Text) <> VAZIO Then
    begin
        liConta := 0;

        liColNum_Grupo := IOPTR_NOPOINTER;

        dmDados.FecharConexao(miDsUsuario_Grupo);
        {' monta dynaset com usu�rios/grupos}
        formMDI.gp_Dynaset_UsuGrup_Montar(miDsUsuario_Grupo);
        lsCriterio := 'Nome_Grupo = ''' + Trim(cboNomeGrupo.Text) + '''';

        dmDados.gsRetorno := dmDados.AtivaFiltro(miSsSisGrup, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
        {'Se existe no SnapShot}
        If dmDados.Status(miSsSisGrup, IOSTATUS_EOF) = False Then
        begin
            lliNum_Grupo := dmDados.ValorColuna(miSsSisGrup, 'Num_Grupo', liColNum_Grupo);
            lsCriterio := 'Num_Grupo = ' + IntToStr(lliNum_Grupo);
            dmDados.gsRetorno := dmDados.AtivaFiltro(miDsUsuario_Grupo, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);

            while not dmDados.Status(miDsUsuario_Grupo, IOSTATUS_EOF) do
            begin
              liConta := liConta + 1;
              if (dmDados.Proximo(miDsUsuario_Grupo) = IORET_EOF) or
                 (dmDados.giStatus <> STATUS_OK) then
                  break;
            end;

            If liConta > 0 Then
            begin
              dmDados.gbFlagOnError := False;
              dmDados.gaMsgParm[0] := liConta;
              dmDados.MensagemExibir(VAZIO,2023);
            end
            Else
            begin
              dmDados.gbFlagOnError := False;
              dmDados.gaMsgParm[0] := cboNomeGrupo.Text;
              liResp := dmDados.MensagemExibir(VAZIO,2024);
                {'ampulheta}
                dlgGrupo.Cursor := crHourglass;

                {'Se deseja excluir}
                If liResp = IDYES Then
                begin

                    lp_Grupo_Excluir;
                    {'recria SnapShot Grupo}
                    formMDI.gp_SnapShot_SisGru_Montar(miSsSisGrup);
                    {'recria SnapShot Acesso}
                    lp_SnapShot_AceRot_Montar;
                    {' Monta Combo com nome do usu�rio}
                    lp_ComboGrupo_Montar;

                    btnSalvar.Enabled := False;
                    btnExcluir.Enabled := False;
                    cboNomeGrupo.Text := VAZIO;
                    cboNomeGrupo.SetFocus;
                End;
            End;
        End;
    End;
    dlgGrupo.Cursor := crDefault;
  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.btnNovoClick       inclui novo grupo
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.btnNovoClick(Sender: TObject);
begin

  Screen.Cursor := crHourGlass;

    {' Indica que n�o cancelou a opera��o}
    mbCancel := False;
    If (miDsGrupo <> 0) And (miDsGrupo <> IOPTR_NOPOINTER) Then
        if dmDados.Status(miDsGrupo, IOSTATUS_EOF) = False then
            {' Verifica se teve altera��o no registro anterior}
            lp_Grupo_GravAutomatica;

    {' se n�o cancelou a montagem}
    If mbCancel = False Then
    begin
        lp_NovoGrupo;

        btnSalvar.Enabled := True;
        {'remove itens e seus subordinados}
        cboNomeGrupo.Text := VAZIO;
        cboNomeGrupo.SetFocus;
    End;

  Screen.Cursor := crDefault;
end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.btnSalvarClick       salva o grupo
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.btnSalvarClick(Sender: TObject);
begin

    {'ampulheta}
  Screen.Cursor := crHourGlass;
    dlgGrupo.Cursor := crHourglass;

    If Trim(cboNomeGrupo.Text) <> VAZIO Then
    begin
      {'Consist�ncia de campos}
//      lp_Grupo_Consistir;
      If (giNumErros = 0) Then
      begin

          btnNovo.Enabled := True;
          btnExcluir.Enabled := True;
          msChave_Aux := VAZIO;

          if mbNovoGrupo then
          begin
            {'Salva o registro}
            lp_Grupo_Salvar;
            {'pode tudo}
//            lp_TodoAcess_Salvar;
          end
          else
            lp_AceRot_Salvar;

          {'recria SnapShot sis/grup}
          formMDI.gp_SnapShot_SisGru_Montar(miSsSisGrup);
          {'recria SnapShot Acesso}
          lp_SnapShot_AceRot_Montar;

          {' Monta Combo com nome dos grupos}
          lp_ComboGrupo_Montar;

          mbNovoGrupo := False;
      end;
    End;

    dlgGrupo.Cursor := crDefault;
  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.FormShow       prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.FormShow(Sender: TObject);
var
  lvSelect: Variant;
begin

  Screen.Cursor := crHourGlass;

    miSsSisGrup       := IOPTR_NOPOINTER;
    miDsGrupo         := IOPTR_NOPOINTER;
    miDsSisSubRot     := IOPTR_NOPOINTER;
    miDsUsuario_Grupo := IOPTR_NOPOINTER;
    miSsAceRot        := IOPTR_NOPOINTER;
    miDsAcRotina      := IOPTR_NOPOINTER;

    mbNovoGrupo := False;
    mbNovaRotina:= False;

    {' monta dynaset com usu�rios/grupos}
    formMDI.gp_Dynaset_UsuGrup_Montar(miDsUsuario_Grupo);
    {'recria SnapShot sistemas/grupos}
    formMDI.gp_SnapShot_SisGru_Montar(miSsSisGrup);
    {'recria SnapShot Acesso}
    lp_SnapShot_AceRot_Montar;

    {'cria snapshot}
    lvSelect := dmDados.SqlVersao('SE_0005', gaParm);
    miDsSisSubRot := dmDados.ExecutarSelect(lvSelect);

    cboNomeGrupo.ItemIndex := -1;

    trvSubRot.items.Clear;

    {' Monta Combo com nome do sistema}
    lp_ComboGrupo_Montar;
    lp_Out_Montar;
    msNomeAreaDesfaz  := VAZIO;
    msNomeAreaDesfaz2 := VAZIO;
    msNomeAreaDesfaz3 := VAZIO;

    btnSalvar.Enabled := False;
    btnExcluir.Enabled := False;

    chkConsulta.Checked   := False;
    chkAltera.Checked     := False;
    chkInclui.Checked     := False;
    chkEmite.Checked      := False;
    chkExclui.Checked     := False;
    chkTodos.Checked      := False;
    chkExecuta.Checked    := False;

    chkConsulta.Enabled   := False;
    chkAltera.Enabled     := False;
    chkInclui.Enabled     := False;
    chkEmite.Enabled      := False;
    chkExclui.Enabled     := False;
    chkTodos.Enabled      := False;
    chkExecuta.Enabled    := False;

    trvSubRot.Enabled := False;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_ComboGrupo_Montar      monta combo com grupos
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_ComboGrupo_Montar;
var
  lsNome_cbo: String;
  lsNome_Grupo: String;
  liColNome_Grupo: Integer;

begin

    liColNome_Grupo := IOPTR_NOPOINTER;

    dmDados.gsRetorno := dmDados.Primeiro(miSsSisGrup);
    lsNome_cbo := cboNomeGrupo.Text;
    cboNomeGrupo.Clear;
    cboNomeGrupo.Text := lsNome_cbo;
    while not dmDados.Status(miSsSisGrup, IOSTATUS_EOF) do
    begin
      lsNome_Grupo := dmDados.ValorColuna(miSsSisGrup, 'Nome_Grupo', liColNome_Grupo);
      cboNomeGrupo.Items.Add(lsNome_Grupo);
      if (dmDados.Proximo(miSsSisGrup) = IORET_EOF) or
         (dmDados.giStatus <> STATUS_OK) then
          break;
    end;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_Grupo_Salvar     salva o grupo
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_Grupo_Salvar;
var
  liNum_Grupo: Integer;
  liIndColGrupNome: integer;
  liIndColNum_Grupo: Integer;
  lvSelect: Variant;

begin

    {'ampulheta}
    dlgGrupo.Cursor := crHourglass;

    cboNomeGrupo.Text := UpperCase(cboNomeGrupo.Text);

    liIndColGrupNome  := IOPTR_NOPOINTER;
    liIndColNum_Grupo := IOPTR_NOPOINTER;

    {'Se n�o est� inserindo}
    If mbNovoGrupo <> True Then
       dmDados.gsRetorno := dmDados.AtivaModo(miDsGrupo,IOMODE_EDIT)
    Else
    begin
        {'Monta Chave}
        {' Se n�o � grava��o autom�tica}
        If msChave_Aux = VAZIO Then
            dmDados.gsRetorno := dmDados.AlterarColuna(miDsGrupo,'Nome_Grupo',liIndColGrupNome,Trim(cboNomeGrupo.Text))
        Else
            dmDados.gsRetorno := dmDados.AlterarColuna(miDsGrupo,'Nome_Grupo',liIndColGrupNome,Trim(msChave_Aux));


        liNum_Grupo := dmDados.SequenciaProxVal('SE_SeqGrupo');
        dmDados.gsRetorno := dmDados.AlterarColuna(miDsGrupo,'Num_Grupo',liIndColNum_Grupo,liNum_Grupo);
    End;
    dmDados.gsRetorno := dmDados.Alterar(miDsGrupo,'SEG_Grupos','Nome_Grupo = '''+Trim(cboNomeGrupo.Text)+'''');

    {'Se inseriu}
    If mbNovoGrupo = True Then
    begin
        {' Recria Dynaset}
        gaParm[0] := cboNomeGrupo.Text;
        lvSelect := dmDados.SqlVersao('SE_0010', gaParm);

        dmDados.FecharConexao(miDsGrupo);
        miDsGrupo := dmDados.ExecutarSelect(lvSelect);

    End;

    mbNovoGrupo := False;
    mbAlterado := False;
    dlgGrupo.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_Grupo_GravAutomatica       teve altera��o
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_Grupo_GravAutomatica;
var
  lsChaveEscolhida: String;
  liResp: Integer;
begin

    {se foi alterado}
    If mbAlterado = True Then
    begin
        If Trim(msNomeAreaDesfaz) <> VAZIO Then
        begin
            lsChaveEscolhida := cboNomeGrupo.Text;

            {'Pede confirma��o para salvamento}
            dmDados.gbFlagOnError := False;
            dmDados.gaMsgParm[0] := ' do Acesso a Rotina ';
            dmDados.gaMsgParm[1] := Trim(msMens_Val);
            msChave_Aux := Trim(msNomeAreaDesfaz);

            {'Voc� deseja salvar as informa��es do Grupo}
            liResp := dmDados.MensagemExibir(VAZIO,1);

            cboNomeGrupo.Text := lsChaveEscolhida;
        end
        Else
            liResp := IDNO;

        If liResp = IDYES Then
        begin
            {'Consist�ncia de campos}
//            lp_Grupo_Consistir;
            If (giNumErros = 0) Or (giErros_Ignorar <> ERRO_CANCELAR) Then
            begin
                lp_AceRot_Salvar;

                {'recria SnapShot sis/grup}
                formMDI.gp_SnapShot_SisGru_Montar(miSsSisGrup);
                {'recria SnapShot Acesso}
                lp_SnapShot_AceRot_Montar;

                {' Monta Combo com nome do Grupo}
                lp_ComboGrupo_Montar;
            end
            Else
            begin
              mbCancel := True;
              cboNomeGrupo.Text := msNomeAreaDesfaz;
              trvSubRot.Selected := mtnRotinaAnterior;
            End;
        end
        Else
        begin
          { se cancelou}
          If liResp = IDCANCEL Then
          begin
            mbCancel := True;
            cboNomeGrupo.Text := msNomeAreaDesfaz;
            trvSubRot.Selected := mtnRotinaAnterior;
            { refaz alteracao}
            lp_Tela_Montar;
          End;
        End;
    End;
    msChave_Aux := VAZIO;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_Grupo_Montar       monta a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_Grupo_Montar;
var
  lvSelect: Variant;
  lsCriterio: String;
  liColNum_Grupo: Integer;
  liLocal: Integer;
  lsCampo: String;
begin

    {' Indica que n�o cancelou a opera��o}
    mbCancel := False;

    If ((miDsGrupo <> 0) And (miDsGrupo <> IOPTR_NOPOINTER)) Then
       if dmDados.Status(miDsGrupo, IOSTATUS_EOF) = False then
         {' Verifica se teve altera��o antes de mudar de registro}
         lp_Grupo_GravAutomatica;

    msChave_Aux := msNomeAreaDesfaz;
    msChave_Aux2 := msNomeAreaDesfaz2;
    msChave_Aux3 := msNomeAreaDesfaz3;
    mliNum_Grupo_Aux := mliNum_Grupo_Sel;

    {' Se tem item para ser selecionado}
    if trvSubRot.Selected <> nil then
    begin
      lsCampo := lp_NodePath(trvSubRot.Selected);
      liLocal := AnsiPos('\',lsCampo);
//      msMens_Val := Copy(lsCampo, (liLocal + 1),Length(lsCampo));
      msMens_Val := Copy(lsCampo, (liLocal + 1),(Length(lsCampo) - (liLocal + 10)));
    end
    else
      msMens_Val := VAZIO;

    {' se n�o cancelou a montagem}
    If mbCancel = False Then
    begin
        lsCriterio := 'Nome_Grupo = ''' + Trim(cboNomeGrupo.Text) + '''';

        dmDados.gsRetorno := dmDados.AtivaFiltro(miSsSisGrup, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
        {'Se existe no SnapShot}
        if dmDados.Status(miSsSisGrup, IOSTATUS_EOF) = False then
        begin
            liColNum_Grupo := IOPTR_NOPOINTER;

            {'salva n�mero do grupo}
            mliNum_Grupo_Sel := dmDados.ValorColuna(miSsSisGrup, 'Num_Grupo', liColNum_Grupo);

            {' setar com false p/ ocorrer grava��o autom�tica ap�s clicar Novo e digitar uma chave v�lida}
            mbNovoGrupo := False;

            gaParm[0] := cboNomeGrupo.Text;
            lvSelect := dmDados.SqlVersao('SE_0010', gaParm);

            btnNovo.Enabled := True;
            btnExcluir.Enabled := True;

            dmDados.FecharConexao(miDsGrupo);
            miDsGrupo := dmDados.ExecutarSelect(lvSelect);

            {se ocorreu erro na execucao do SQL}
            if miDsGrupo = IOPTR_NOPOINTER then
            begin
              {apresenta mensagem de erro na abertura da conexao}
              dmDados.gbFlagOnError := False;
              dmDados.MensagemExibir(VAZIO,100);
              {sai da funcao}
              Exit;
            end;

            lsCriterio := 'Num_Grupo = ' + IntToStr(mliNum_Grupo_Sel) + ' and Cod_Rotina = ''' + msRotina + '''' + ' and Cod_SubRotina = ''' + msSubRotina + '''';
            dmDados.gsRetorno := dmDados.AtivaFiltro(miSsAceRot, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
            {'Se existe no SnapShot}
            if dmDados.Status(miSsAceRot, IOSTATUS_EOF) = False then
            begin
              {'se optou por novo}
              If mbNovoGrupo = True Then
                mbNovoGrupo := False;

              mbNovaRotina:= False;

              gaParm[0] := IntToStr(mliNum_Grupo_Sel);
              gaParm[1] := msRotina;
              gaParm[2] := msSubRotina;
              lvSelect := dmDados.SqlVersao('SE_0018', gaParm);

              dmDados.FecharConexao(miDsAcRotina);
              miDsAcRotina := dmDados.ExecutarSelect(lvSelect);

              {se ocorreu erro na execucao do SQL}
              if miDsAcRotina = IOPTR_NOPOINTER then
              begin
                {apresenta mensagem de erro na abertura da conexao}
                dmDados.gbFlagOnError := False;
                dmDados.MensagemExibir(VAZIO,100);
                {sai da funcao}
                Exit;
              end;

              {'Se recordset n�o criado}
              If (miDsSisSubRot = 0) Or (miDsSisSubRot = IOPTR_NOPOINTER) Then
              begin
                {'cria snapshot}
                lvSelect := dmDados.SqlVersao('SE_0005', gaParm);
                miDsSisSubRot := dmDados.ExecutarSelect(lvSelect);

                {se ocorreu erro na execucao do SQL}
                if miDsSisSubRot = IOPTR_NOPOINTER then
                begin
                  {apresenta mensagem de erro na abertura da conexao}
                  dmDados.gbFlagOnError := False;
                  dmDados.MensagemExibir(VAZIO,100);
                  {sai da funcao}
                  Exit;
                end;
              End;

              lsCriterio := 'Cod_Rotina = ''' + msRotina + ''' AND Cod_SubRotina = '''+ msSubRotina + '''';
              dmDados.gsRetorno := dmDados.AtivaFiltro(miDsSisSubRot, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);

              lp_Tela_Montar;
            end
            {'Se n�o existe no SnapShot}
            Else
            begin
              {'Se n�o optou por Novo}
              If mbNovoGrupo = False Then
              begin
                {'Se recordset n�o criado}
                If (miDsAcRotina = 0) Or (miDsAcRotina = IOPTR_NOPOINTER) Then
                begin
                  gaParm[0] := IntToStr(mliNum_Grupo_Sel);
                  gaParm[1] := msRotina;
                  gaParm[2] := msSubRotina;
                  lvSelect := dmDados.SqlVersao('SE_0018', gaParm);

                  dmDados.FecharConexao(miDsAcRotina);
                  miDsAcRotina := dmDados.ExecutarSelect(lvSelect);

                  {se ocorreu erro na execucao do SQL}
                  if miDsAcRotina = IOPTR_NOPOINTER then
                  begin
                    {apresenta mensagem de erro na abertura da conexao}
                    dmDados.gbFlagOnError := False;
                    dmDados.MensagemExibir(VAZIO,100);
                    {sai da funcao}
                    Exit;
                  end;
                End;

                {'Se recordset n�o criado}
                If (miDsSisSubRot = 0) Or (miDsSisSubRot = IOPTR_NOPOINTER) Then
                begin
                  {'cria snapshot}
                  lvSelect := dmDados.SqlVersao('SE_0005', gaParm);
                  miDsSisSubRot := dmDados.ExecutarSelect(lvSelect);

                  {se ocorreu erro na execucao do SQL}
                  if miDsSisSubRot = IOPTR_NOPOINTER then
                  begin
                    {apresenta mensagem de erro na abertura da conexao}
                    dmDados.gbFlagOnError := False;
                    dmDados.MensagemExibir(VAZIO,100);
                    {sai da funcao}
                    Exit;
                  end;
                End;

                lsCriterio := 'Cod_Rotina = ''' + msRotina + ''' AND Cod_SubRotina = '''+ msSubRotina + '''';
                dmDados.AtivaFiltro(miDsSisSubRot, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);

                {'rotina gera novo acesso a rotina}
                mbNovaRotina:= True;

                lp_NovoAceRot;
              End;
            End;
        end
        {'Se n�o existe no SnapShot}
        Else
        begin
            {'Se recordset n�o criado}
            If ((miDsGrupo = 0) Or (miDsGrupo = IOPTR_NOPOINTER)) Then
            begin
              gaParm[0] := cboNomeGrupo.Text;
              lvSelect := dmDados.SqlVersao('SE_0010', gaParm);

              dmDados.FecharConexao(miDsGrupo);
              miDsGrupo := dmDados.ExecutarSelect(lvSelect);

              {se ocorreu erro na execucao do SQL}
              if miDsGrupo = IOPTR_NOPOINTER then
              begin
                {apresenta mensagem de erro na abertura da conexao}
                dmDados.gbFlagOnError := False;
                dmDados.MensagemExibir(VAZIO,100);
                {sai da funcao}
                Exit;
              end;

            End;

            {'Se n�o optou por Novo}
            If mbNovoGrupo = False Then
              {'rotina gera novo usu�rio}
              lp_NovoGrupo;

        End;
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_NovoGrupo       monta a tela com novos dados
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_NovoGrupo;
begin

    If ((miDsGrupo <> 0) And (miDsGrupo <> IOPTR_NOPOINTER)) Then
    begin
      dmDados.gsRetorno := dmDados.AtivaModo(miDsGrupo,IOMODE_INSERT);
      mbNovoGrupo := True;
    End;
    btnNovo.Enabled := False;
    btnExcluir.Enabled := False;

    chkConsulta.Checked   := False;
    chkAltera.Checked     := False;
    chkInclui.Checked     := False;
    chkEmite.Checked      := False;
    chkExclui.Checked     := False;
    chkTodos.Checked      := False;
    chkExecuta.Checked    := False;

    chkConsulta.Enabled   := False;
    chkAltera.Enabled     := False;
    chkInclui.Enabled     := False;
    chkEmite.Enabled      := False;
    chkExclui.Enabled     := False;
    chkTodos.Enabled      := False;
    chkExecuta.Enabled    := False;

    trvSubRot.Enabled     := False;

    mbAlterado := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_Grupo_Excluir          Exclui o grupo e limpa a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_Grupo_Excluir;
var
  lsCriterio: String;
begin

    dmDados.gsRetorno := dmDados.Excluir(miDsGrupo);

    lsCriterio := 'Num_Grupo = ' + IntToStr(mliNum_Grupo_Sel);
    dmDados.gsRetorno := dmDados.AtivaFiltro(miSsAceRot, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
    dmDados.gsRetorno := dmDados.Excluir(miSsAceRot);

    {'limpa controles}
    cboNomeGrupo.Text := VAZIO;
    trvSubRot.Selected := nil;
    chkConsulta.Checked   := False;
    chkAltera.Checked     := False;
    chkInclui.Checked     := False;
    chkEmite.Checked      := False;
    chkExclui.Checked     := False;
    chkTodos.Checked      := False;
    chkExecuta.Checked    := False;

    chkConsulta.Enabled   := False;
    chkAltera.Enabled     := False;
    chkInclui.Enabled     := False;
    chkEmite.Enabled      := False;
    chkExclui.Enabled     := False;
    chkTodos.Enabled      := False;
    chkExecuta.Enabled    := False;

    mbAlterado := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_SnapShot_AceRot_Montar       Preenche SnapShot de Acesso
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_SnapShot_AceRot_Montar;
var
  lvSelect: Variant;

begin

    {' Monta SnapShot contendo todos os Cpos}
    lvSelect := dmDados.SqlVersao('SE_0017', gaParm);

    {'Recria snapshot}
    dmDados.FecharConexao(miSsAceRot);

    miSsAceRot := dmDados.ExecutarSelect(lvSelect);

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_NodePath      Para pegar o nome completo
 *                             Ex: "Item\SubItem\SubSubItem"
 *-----------------------------------------------------------------------------}
function TdlgGrupo.lp_NodePath(Node: TTreeNode): String;
begin
  if Node.Parent = nil then
    Result := Node.Text
  else
    Result := lp_NodePath(Node.Parent) + '\' + Node.Text;
end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.trvSubRotClick       Muda a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.trvSubRotClick(Sender: TObject);
var
  liLocal: Integer;
  lsCampo: String;

begin

  Screen.Cursor := crHourGlass;

    If Trim(cboNomeGrupo.Text) = VAZIO Then
        trvSubRot.Selected := nil;

    {' Se n�o tem item para ser selecionado}
    if trvSubRot.Selected = nil then
        Exit;

    If not trvSubRot.Selected.HasChildren Then
    begin
        btnSalvar.Enabled := True;
        btnExcluir.Enabled := True;

            lsCampo := lp_NodePath(trvSubRot.Selected);
            liLocal := AnsiPos('\',lsCampo);
//            msNomeAreaDesfaz2 := Copy(lsCampo, 1, 8);
            msNomeAreaDesfaz2 := Copy(lsCampo, (liLocal - 9), 8);

//            liLocal := AnsiPos('\',lsCampo);
            msNomeAreaDesfaz3 := Copy(lsCampo, (Length(lsCampo) - 8), 8);
            msRotina := msNomeAreaDesfaz2;
            msSubRotina := msNomeAreaDesfaz3;
            lp_Grupo_Montar;
    end
    Else
    begin
        btnSalvar.Enabled := False;

        If (miDsAcRotina <> 0) And (miDsAcRotina <> IOPTR_NOPOINTER) Then
          if dmDados.Status(miDsAcRotina, IOSTATUS_EOF) = False then
              {' Verifica se teve altera��o antes de mudar de registro}
              lp_Grupo_GravAutomatica;

      chkConsulta.Checked   := False;
      chkAltera.Checked     := False;
      chkInclui.Checked     := False;
      chkEmite.Checked      := False;
      chkExclui.Checked     := False;
      chkTodos.Checked      := False;
      chkExecuta.Checked    := False;

      chkConsulta.Enabled   := False;
      chkAltera.Enabled     := False;
      chkInclui.Enabled     := False;
      chkEmite.Enabled      := False;
      chkExclui.Enabled     := False;
      chkTodos.Enabled      := False;
      chkExecuta.Enabled    := False;

      mbAlterado := False;
    End;

    mtnRotinaAnterior := trvSubRot.Selected;

  Screen.Cursor := crDefault;
end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.trvSubRotDblClick       Faz Controle
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.trvSubRotDblClick(Sender: TObject);
begin
    { se nao tiver grupo}
    If Trim(cboNomeGrupo.Text) = VAZIO Then
      trvSubRot.Selected := nil
    Else
      trvSubRotClick(Sender);
end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.trvSubRotKeyUp       Atualiza a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.trvSubRotKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    Case Key of
      KEY_UP:
        trvSubRotClick(Sender);
      KEY_DOWN:
        trvSubRotClick(Sender);
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_Tela_Montar       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_Tela_Montar;
var
  liIndColConsulta: Integer;
  liIndColAltera: Integer;
  liIndColInclui: Integer;
  liIndColEmite: Integer;
  liIndColExclui: Integer;
  liIndColExecuta: Integer;

  liIndColPConsulta  : Integer;
  liIndColPAltera    : Integer;
  liIndColPInclui    : Integer;
  liIndColPEmite     : Integer;
  liIndColPExclui    : Integer;
  liIndColPExecuta   : Integer;

begin

    liIndColConsulta  := IOPTR_NOPOINTER;
    liIndColAltera    := IOPTR_NOPOINTER;
    liIndColInclui    := IOPTR_NOPOINTER;
    liIndColEmite     := IOPTR_NOPOINTER;
    liIndColExclui    := IOPTR_NOPOINTER;
    liIndColExecuta   := IOPTR_NOPOINTER;

    liIndColPConsulta  := IOPTR_NOPOINTER;
    liIndColPAltera    := IOPTR_NOPOINTER;
    liIndColPInclui    := IOPTR_NOPOINTER;
    liIndColPEmite     := IOPTR_NOPOINTER;
    liIndColPExclui    := IOPTR_NOPOINTER;
    liIndColPExecuta   := IOPTR_NOPOINTER;
    {'Monta campos n�o associados}

    if msRotina = ROT_CONSULTA then
    begin
      chkConsulta.Enabled   := (dmDados.ValorColuna(miDsSisSubRot, 'Consulta', liIndColPConsulta) = 'S');
      chkAltera.Enabled     := (dmDados.ValorColuna(miDsSisSubRot, 'Altera', liIndColPAltera) = 'S');
      chkInclui.Enabled     := (dmDados.ValorColuna(miDsSisSubRot, 'Inclui', liIndColPInclui) = 'S');
      chkEmite.Enabled      := False;
      chkExclui.Enabled     := (dmDados.ValorColuna(miDsSisSubRot, 'Exclui', liIndColPExclui) = 'S');
      chkTodos.Enabled      := True;
      chkExecuta.Enabled    := False;
    end
    else if msRotina = ROT_RELATORIO then
        begin
          chkConsulta.Enabled   := False;
          chkAltera.Enabled     := False;
          chkInclui.Enabled     := False;
          chkEmite.Enabled      := (dmDados.ValorColuna(miDsSisSubRot, 'Emite', liIndColPEmite) = 'S');
          chkExclui.Enabled     := False;
          chkTodos.Enabled      := False;
          chkExecuta.Enabled    := False;
        end
    else if msRotina = ROT_ROTINA then
        begin
          chkConsulta.Enabled   := False;
          chkAltera.Enabled     := False;
          chkInclui.Enabled     := False;
          chkEmite.Enabled      := False;
          chkExclui.Enabled     := False;
          chkTodos.Enabled      := False;
          chkExecuta.Enabled    := (dmDados.ValorColuna(miDsSisSubRot, 'Executa', liIndColPExecuta) = 'S');
        end
    else
    begin
      chkConsulta.Enabled   := False;
      chkAltera.Enabled     := False;
      chkInclui.Enabled     := False;
      chkEmite.Enabled      := False;
      chkExclui.Enabled     := False;
      chkTodos.Enabled      := False;
      chkExecuta.Enabled    := False;
    end;

    chkConsulta.Checked := dmDados.ValorColuna(miDsAcRotina, 'Consulta', liIndColConsulta);

    chkAltera.Checked := dmDados.ValorColuna(miDsAcRotina, 'Altera', liIndColAltera);

    chkInclui.Checked := dmDados.ValorColuna(miDsAcRotina, 'Inclui', liIndColInclui);

    chkEmite.Checked := dmDados.ValorColuna(miDsAcRotina, 'Emite', liIndColEmite);

    chkExclui.Checked := dmDados.ValorColuna(miDsAcRotina, 'Exclui', liIndColExclui);

    chkExecuta.Checked := dmDados.ValorColuna(miDsAcRotina, 'Executa', liIndColExecuta);

    chkTodos.Checked := False;

    mbAlterado := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_NovoAceRot       novo Acesso Rotina
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_NovoAceRot;
var
  liIndColPConsulta: integer;
  liIndColPAltera: Integer;
  liIndColPInclui: integer;
  liIndColPEmite: Integer;
  liIndColPExclui: integer;
  liIndColPExecuta: Integer;

begin

    liIndColPConsulta  := IOPTR_NOPOINTER;
    liIndColPAltera    := IOPTR_NOPOINTER;
    liIndColPInclui    := IOPTR_NOPOINTER;
    liIndColPEmite     := IOPTR_NOPOINTER;
    liIndColPExclui    := IOPTR_NOPOINTER;
    liIndColPExecuta   := IOPTR_NOPOINTER;

    dmDados.gsRetorno := dmDados.AtivaModo(miDsAcRotina,IOMODE_INSERT);
    chkConsulta.Checked   := False;
    chkAltera.Checked     := False;
    chkInclui.Checked     := False;
    chkEmite.Checked      := False;
    chkExclui.Checked     := False;
    chkTodos.Checked      := False;
    chkExecuta.Checked    := False;

    if msRotina = ROT_CONSULTA then
    begin
      chkConsulta.Enabled   := (dmDados.ValorColuna(miDsSisSubRot, 'Consulta', liIndColPConsulta) = 'S');
      chkAltera.Enabled     := (dmDados.ValorColuna(miDsSisSubRot, 'Altera', liIndColPAltera) = 'S');
      chkInclui.Enabled     := (dmDados.ValorColuna(miDsSisSubRot, 'Inclui', liIndColPInclui) = 'S');
      chkEmite.Enabled      := False;
      chkExclui.Enabled     := (dmDados.ValorColuna(miDsSisSubRot, 'Exclui', liIndColPExclui) = 'S');
      chkTodos.Enabled      := True;
      chkExecuta.Enabled    := False;
    end
    else if msRotina = ROT_RELATORIO then
        begin
          chkConsulta.Enabled   := False;
          chkAltera.Enabled     := False;
          chkInclui.Enabled     := False;
          chkEmite.Enabled      := (dmDados.ValorColuna(miDsSisSubRot, 'Emite', liIndColPEmite) = 'S');
          chkExclui.Enabled     := False;
          chkTodos.Enabled      := False;
          chkExecuta.Enabled    := False;
        end
    else if msRotina = ROT_ROTINA then
        begin
          chkConsulta.Enabled   := False;
          chkAltera.Enabled     := False;
          chkInclui.Enabled     := False;
          chkEmite.Enabled      := False;
          chkExclui.Enabled     := False;
          chkTodos.Enabled      := False;
          chkExecuta.Enabled    := (dmDados.ValorColuna(miDsSisSubRot, 'Executa', liIndColPExecuta) = 'S');
        end
    else
    begin
      chkConsulta.Enabled   := False;
      chkAltera.Enabled     := False;
      chkInclui.Enabled     := False;
      chkEmite.Enabled      := False;
      chkExclui.Enabled     := False;
      chkTodos.Enabled      := False;
      chkExecuta.Enabled    := False;
    end;


    mbAlterado := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_AceRot_Salvar       Salva Acesso Rotina
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_AceRot_Salvar;
var
  liIndColCod_SubRotina :Integer;
  liIndColCod_Rotina    :Integer;
  liIndColNum_Grupo     :Integer;
  liIndColConsulta      :Integer;
  liIndColAltera        :Integer;
  liIndColInclui        :Integer;
  liIndColEmite         :Integer;
  liIndColExclui        :Integer;
  liIndColExecuta       :Integer;
  lvSelect: Variant;

begin

  liIndColCod_SubRotina := IOPTR_NOPOINTER;
  liIndColCod_Rotina    := IOPTR_NOPOINTER;
  liIndColNum_Grupo     := IOPTR_NOPOINTER;
  liIndColConsulta      := IOPTR_NOPOINTER;
  liIndColAltera        := IOPTR_NOPOINTER;
  liIndColInclui        := IOPTR_NOPOINTER;
  liIndColEmite         := IOPTR_NOPOINTER;
  liIndColExclui        := IOPTR_NOPOINTER;
  liIndColExecuta       := IOPTR_NOPOINTER;

    {'ampulheta}
    dlgGrupo.Cursor := crHourglass;
    {'Se n�o est� inserindo}
    If mbNovaRotina <> True Then
       dmDados.gsRetorno := dmDados.AtivaModo(miDsAcRotina,IOMODE_EDIT)
    Else
    begin
        If msChave_Aux2 = VAZIO Then
        begin
            dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Num_Grupo',liIndColNum_Grupo,mliNum_Grupo_Sel);
            dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Cod_Rotina',liIndColCod_Rotina,Trim(msNomeAreaDesfaz2));
            dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Cod_SubRotina',liIndColCod_SubRotina,Trim(msNomeAreaDesfaz3));
        end
        Else
        begin
            dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Num_Grupo',liIndColNum_Grupo,mliNum_Grupo_Aux);
            dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Cod_Rotina',liIndColCod_Rotina,Trim(msChave_Aux2));
            dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Cod_SubRotina',liIndColCod_SubRotina,Trim(msChave_Aux3));
        End;
    End;
    {'Trata campos}
    dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Consulta',liIndColConsulta,chkConsulta.Checked);

    dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Altera',liIndColAltera,chkAltera.Checked);

    dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Inclui',liIndColInclui,chkInclui.Checked);

    dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Emite',liIndColEmite,chkEmite.Checked);

    dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Exclui',liIndColExclui,chkExclui.Checked);

    dmDados.gsRetorno := dmDados.AlterarColuna(miDsAcRotina,'Executa',liIndColExecuta,chkExecuta.Checked);

    dmDados.gsRetorno := dmDados.Alterar(miDsAcRotina,'SEG_Rotinas','Num_Grupo = '+IntToStr(mliNum_Grupo_Aux)+' AND Cod_Rotina = '''+msChave_Aux2+''' AND Cod_SubRotina = '''+msChave_Aux3+'''');

    {'Se inseriu}
    If mbNovaRotina = True Then
    begin
        {' Recria Dynaset}
        gaParm[0] := IntToStr(mliNum_Grupo_Sel);
        gaParm[1] := msRotina;
        gaParm[2] := msSubRotina;
        lvSelect := dmDados.SqlVersao('SE_0018', gaParm);

        dmDados.FecharConexao(miDsAcRotina);
        miDsAcRotina := dmDados.ExecutarSelect(lvSelect);

    End;
    mbNovaRotina := False;
    mbAlterado := False;
    dlgGrupo.Cursor := crDefault;
end;

{*-----------------------------------------------------------------------------
 *  TdlgGrupo.lp_Out_Montar       Mostra Acesso Rotina
 *
 *-----------------------------------------------------------------------------}
procedure TdlgGrupo.lp_Out_Montar;
var
  lsout_Sist: String;     {'Cod. Sistema}
  lsout_Roti_Ant: String;
  lsString_Aux: String;
  lsout_Desc: String;
  lsCriterio: String;
  trnNode: TTreeNode;
  lvSelect: Variant;
  liDsSisRot: Integer;

  liIndColCod_Rotina          : Integer;
  liIndColDescricao_Rotina    : Integer;
  liIndColDescricao_SubRotina : Integer;
  liIndColCod_SubRotina       : Integer;
begin

    liIndColCod_Rotina          := IOPTR_NOPOINTER;
    liIndColDescricao_Rotina    := IOPTR_NOPOINTER;
    liIndColDescricao_SubRotina := IOPTR_NOPOINTER;
    liIndColCod_SubRotina       := IOPTR_NOPOINTER;

    trvSubRot.Items.Clear;
    trnNode := nil;

    {'cria snapshot}
    lvSelect := dmDados.SqlVersao('SE_0008', gaParm);
    liDsSisRot := dmDados.ExecutarSelect(lvSelect);

    lsout_Roti_Ant := VAZIO;
    dmDados.gsRetorno := dmDados.AtivaFiltro(miDsSisSubRot, VAZIO, IOFLAG_FIRST, IOFLAG_GET);
    dmDados.Primeiro(miDsSisSubRot);
    while not dmDados.Status(miDsSisSubRot, IOSTATUS_EOF) do
    begin
        lsout_Sist := dmDados.ValorColuna(miDsSisSubRot, 'Cod_Rotina', liIndColCod_Rotina);
        If lsout_Sist <> lsout_Roti_Ant Then
        begin
            lsout_Roti_Ant := lsout_Sist;
            lsCriterio := 'Cod_Rotina = ''' + Trim(lsout_Sist) + '''';
            dmDados.gsRetorno := dmDados.AtivaFiltro(liDsSisRot, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
            {'Descri��o da Rotina}
            lsString_Aux := dmDados.ValorColuna(liDsSisRot, 'Descricao_Rotina', liIndColDescricao_Rotina);
            {'concatena c�digo e descri��o}
//            lsout_Desc := Trim(lsout_Sist) + ' - ' + Trim(lsString_Aux);
            lsout_Desc := Trim(lsString_Aux) + ' (' + Trim(lsout_Sist) + ')';

            trnNode := trvSubRot.Items.Add(nil,lsout_Desc);
        End;
//        lsString_Aux := dmDados.ValorColuna(miDsSisSubRot, 'Descricao_SubRotina', liIndColDescricao_SubRotina);

        lsout_Sist := dmDados.ValorColuna(miDsSisSubRot, 'Cod_SubRotina', liIndColCod_SubRotina);
        lsString_Aux := dmDados.ValorColuna(miDsSisSubRot, 'Descricao_SubRotina', liIndColDescricao_SubRotina);
        {'Concatena c�digo com descri��o}
//        lsout_Desc := Trim(lsout_Sist) + ' - ' + Trim(lsString_Aux);
        lsout_Desc := Trim(lsString_Aux) + ' (' + Trim(lsout_Sist) + ')';

        trvSubRot.Items.AddChild(trnNode, lsout_Desc);

        if (dmDados.Proximo(miDsSisSubRot) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
    end;

    { fecha a conexao}
    dmDados.FecharConexao(liDsSisRot);

      chkConsulta.Enabled   := False;
      chkAltera.Enabled     := False;
      chkInclui.Enabled     := False;
      chkEmite.Enabled      := False;
      chkExclui.Enabled     := False;
      chkTodos.Enabled      := False;
      chkExecuta.Enabled    := False;

end;

end.
