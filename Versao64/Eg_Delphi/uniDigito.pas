unit uniDigito;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const

  { constantes genericas }
  CONCAT_EXTENSO = 'E ';
  CONCAT_EXTENSO_FRAC = ' E ';
  CONCAT_EXTENSO_MILHAR = ', ';
  TIPO_SINGULAR = 0;
  TIPO_PLURAL = 1;
  ZERO_BINARIO = '\0';
  CARAC_ZERO = '0';

  CARAC_F_MAI = 'F';
  CARAC_M_MAI = 'M';

  { constantes para a funcao Apurar }
  ASTERISCO = '*';
  ESPACO = ' ';
  PONTO_VIRGULA = ';';
  HIFEN = '-';
  LINE_FEED = '\10\13';

{*======================================================================
 *            RECORDS
 *======================================================================}

type
 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntDigito = class(TObject)
  private
    {  strings utilizadas para montar valor por extenso }
    mbIniciaExtenso:  Boolean;
    unidade: Array[0..19] of String;
    Unid_Fem: Array[0..19] of String;
    Dezena: Array[0..9] of String;
    Centena: Array[0..9] of String;
    Cent_Fem: Array[0..9] of String;
    Def_Sing: Array[0..5] of String;
    Def_Plur: Array[0..5] of String;

    procedure mpIniciaExtenso;
    Function mfConverteTamLinhas(psTamanho: String;
                                 var pNumColunas: array of Integer): Integer;
    Function mfConverteValor(var psValorInt: String;
                             var psValorFrac: String;
                             pdValor: Double): Integer;
    Function mfPutMoeda(psUnidade: String;
                        piTipoUnidade: Integer;
                        var psMoeda: String): Boolean;
    Function mfMontaTerna(psCentena: String;
                          psDezena: String;
                          psUnidade: String;
                          pbGenero: Boolean): Boolean;
    Function mfMontaTernaFracao(psValorFrac: String;
                                pbFlagValInt: Boolean;
                                pbGenero: Boolean): Boolean;
    procedure mpDivideLinhas(paiNumColunas: array of Integer;
                             piNumLinhas: Integer);

  public
    { Public declarations }

    procedure gp_Digito_Inicializar;
    procedure gp_Digito_Finalizar;
    function  gf_DACCGC_Validar(const psCGC: String): Boolean;
    function  gf_DACCPF_Validar(const psCPF: String): Boolean;
    function  gf_DAC_Registro(const pFunc: String): String;
    function  gf_Digito_ChecarProposta(const psChave: string): Boolean;
    function  gfC_Extenso(pdValor: Double;
                         psUnidade: String;
                         psFracao: String;
                         psTamanho: String;
                         var pRetorno: String): Boolean;

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untDigito: TuntDigito;

implementation

uses uniConst, uniDados;

{*-----------------------------------------------------------------------------
 *  TuntDigito.gp_Digito_Inicializar
 *        Cria o objeto e inicializa variaveis
 *-----------------------------------------------------------------------------}
procedure TuntDigito.gp_Digito_Inicializar;
begin
  // cria o objeto
  untDigito := TuntDigito.Create;

end;

{*-----------------------------------------------------------------------------
 *  TuntDigito.gp_Digito_Finalizar
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
procedure TuntDigito.gp_Digito_Finalizar;
begin
  // destroi objeto
  untDigito.Free;
end;

{'=================================================================
'
'        Fun��es para C�lculo de D�gitos Verificadores
'
'=================================================================
'Function gfDACCGC_Validar (ByVal txtContole as String) as Integer
'Objetivo: Valida o CGC digitado pelo operador atrav�s do c�lculo
'          do digito verificador.
'          Retorna True se o CGC foi digitado corretamente ou
'          False se o D�gito Verificador n�o conferir.
'F�rmula : O n�mero do CGC � composto por:
'          N N . N N N . N N N / F F F F - C D
'          Onde:
'              . N�mero do CGC = N N . N N N . N N N
'              . Filial CGC    = F F F F
'              . 1o D�gito     = C
'              . 2o D�gito     = D
'
'          1) C�lculo do 1o D�gito Verificador (C)
'             N  N  N  N  N  N  N  N  F  F  F  F  -  C
'             *  *  *  *  *  *  *  *  *  *  *  *
'             5  4  3  2  9  8  7  6  5  4  3  2
'             =  =  =  =  =  =  =  =  =  =  =  =
'         Y = X +X +X +X +X +X +X +X +X +X +X +X
'             ----------------------------------
'                            11
'
'         C = 11 - Resto     Se Resto = 0 ou 1 D�gito = 0
'
'
'          2) C�lculo do 1o D�gito Verificador (C)
'             N  N  N  N  N  N  N  N  F  F  F  F  -  C
'             *  *  *  *  *  *  *  *  *  *  *  *     *
'             6  5  4  3  2  9  8  7  6  5  4  3     2
'             =  =  =  =  =  =  =  =  =  =  =  =     =
'         Y = X +X +X +X +X +X +X +X +X +X +X +X    +X
'             ----------------------------------------
'                                 11
'
'         C = 11 - Resto     Se Resto = 0 ou 1 D�gito = 0
'
'           3) CGC Completo = NNNNNNNNFFFFCD
'
'=================================================================
'Function gfDACPF_Validar (ByVal txtContole as String) as Integer
'Objetivo: Valida o CPF digitado pelo operador atrav�s do c�lculo
'          do digito verificador.
'          Retorna True se o CGC foi digitado corretamente ou
'          False se o D�gito Verificador n�o conferir.
'F�rmula : O n�mero do CPF � composto por:
'          N N N . N N N . N N N - C D
'          Onde:
'              . N�mero do CPF = N N . N N N . N N N
'              . 1o D�gito     = C
'              . 2o D�gito     = D
'
'          1) C�lculo do 1o D�gito Verificador (C)
'             N  N  N  N  N  N  N  N  N  -  C
'             *  *  *  *  *  *  *  *  *
'             10 9  8  7  6  5  4  3  2
'             =  =  =  =  =  =  =  =  =
'         Y = X +X +X +X +X +X +X +X +X
'             -------------------------
'                         11
'
'         C = 11 - Resto     Se Resto = 0 ou 1 D�gito = 0
'
'
'          2) C�lculo do 1o D�gito Verificador (C)
'             N  N  N  N  N  N  N  N  N  -  C
'             *  *  *  *  *  *  *  *  *     *
'             11 10 9  8  7  6  5  4  3     2
'             =  =  =  =  =  =  =  =  =     =
'         Y = X +X +X +X +X +X +X +X +X    +X
'             -------------------------------
'                         11
'
'         C = 11 - Resto     Se Resto = 0 ou 1 D�gito = 0
'
'           3) CPF Completo = NNNNNNNNNCD
'
'=================================================================}

{*-----------------------------------------------------------------------------
 *  TuntDigito.gf_DACCGC_Validar
 *        calcula e checa o dogoti do CGC
 *-----------------------------------------------------------------------------}
function TuntDigito.gf_DACCGC_Validar(const psCGC: String): Boolean;
var
  i                : Integer;
  liMultiplicador  : Integer;
  liResto          : Integer;
  liDAC1           : Integer;
  liDAC2           : Integer;
  lsCGC            : String;
  lsTemp           : string;
begin
  // retira a formata��o do valor
  for i := 1 to Length(psCGC) do
    if Pos(Copy(psCGC, i, 1), '0123456789') <> 0 then
      lsCGC := lsCGC + Copy(psCGC, i, 1);

  //Preenche com zeros a esquerda se necess�rio e extrai a parte base
  lsTemp := '';
  for i := 1 to (15 - Length(lsCGC)) - 1 do
    lsTemp := lsTemp + '0';
  lsCGC := Copy(lsTemp + lsCGC, 1, 12);

  //Inicializa
  liMultiplicador := 2;
  liResto := 0;

  //Extrai cada n�mero do CGC e Filial e calcula
  //de acordo com a f�rmula
  for i := Length(lsCGC) downto 1 do
  begin
    liResto := liResto + StrToInt(Copy(lsCGC, i, 1)) * liMultiplicador;
    liMultiplicador := liMultiplicador + 1;
    if liMultiplicador > 9 then
      liMultiplicador := 2;
  end;

  //Ajusta o resto
  liResto := liResto Mod 11;
  if (liResto = 0) or (liResto = 1) then
    liDAC1 := 0
  else
    liDAC1 := 11 - liResto;

  //Reinicializa
  liMultiplicador := 2;
  liResto := 0;

  lsCGC := lsCGC + Trim(IntToStr(liDAC1));

  for i := Length(lsCGC) downto 1 do
  begin
    liResto := liResto + StrToInt(Copy(lsCGC, i, 1)) * liMultiplicador;
    liMultiplicador := liMultiplicador + 1;
    if liMultiplicador > 9 then
      liMultiplicador := 2;
  end;

  //Ajusta o resto
  liResto := liResto Mod 11;
  if (liResto = 0) or (liResto = 1) then
    liDAC2 := 0
  else
    liDAC2 := 11 - liResto;

  //Retorna se os digitos calculados batem com os da string passada por par�metro
  if Copy(psCGC, Length(psCGC) - 1, 2) = (Trim(IntToStr(liDAC1)) + Trim(IntToStr(liDAC2))) then
    Result := True
  else
    Result := False;

end;

{*-----------------------------------------------------------------------------
 *  TuntDigito.gf_DACCPF_Validar
 *        calcula e checa o dogoti do CPF
 *-----------------------------------------------------------------------------}
function TuntDigito.gf_DACCPF_Validar(const psCPF: String): Boolean;
var
  i         : Integer;
  lsCPF     : String;
  liResto   : Integer;
  liDAC1    : Integer;
  liDAC2    : Integer;
  lsTemp    : string;
begin
  // retira a formata��o do valor
  for i := 1 to Length(psCPF) do
    if Pos(Copy(psCPF, i, 1), '0123456789') <> 0 then
      lsCPF := lsCPF + Copy(psCPF, i, 1);

  //Preenche com zeros a esquerda se necess�rio e extrai a parte base
  lsTemp := '';
  for i := 1 to (11 - Length(lsCPF)) - 1 do
    lsTemp := lsTemp + '0';
  lsCPF := Copy(lsTemp + lsCPF, 1, 9);

  //Inicializa
  liResto := 0;

  //Soma os n�meros com os pesos via For....Next
  for i := 1 to 9 do
    liResto := liResto + StrToInt(Copy(lsCPF, i, 1)) * (11 - i);

  //Ajusta o resto
  liResto := liResto Mod 11;
  if (liResto = 0) or (liResto = 1) then
    liDAC1 := 0
  else
    liDAC1 := 11 - liResto;

  //Reinicializa
  liResto := 0;

  //As pr�ximas linhas s�o id�nticas as acima, incluindo apenas
  //o d�gito verificador 1 na string, portanto alterando o valor.
  lsCPF := lsCPF + Trim(IntToStr(liDAC1));
  for i := 1 to 10 do
    liResto := liResto + StrToInt(Copy(lsCPF, i, 1)) * (12 - i);

  //Ajusta o resto
  liResto := liResto Mod 11;
  if (liResto = 0) or (liResto = 1) then
    liDAC2 := 0
  else
    liDAC2 := 11 - liResto;

  //Retorna se os digitos calculados batem com os da string passada por par�metro
  if Copy(psCPF, Length(psCPF) - 1, 2) = (Trim(IntToStr(liDAC1)) + Trim(IntToStr(liDAC2))) then
    Result := True
  else
    Result := False;

end;

{*-----------------------------------------------------------------------------
 *  TuntDigito.gf_DAC_Registro
 *        Funcao que gera o digito verificador.
 *-----------------------------------------------------------------------------}
function TuntDigito.gf_DAC_Registro(const pFunc: String): String;
var
  lDAC   : Integer;
  lResto : Integer;
  lFunc  : String;
begin
  lFunc := StringOfChar('0',10 - Length(pFunc)) + pFunc;

  lResto := StrToInt(Copy(lFunc, 10, 1)) * 2;
  lResto := lResto + StrToInt(Copy(lFunc, 9, 1)) * 3;
  lResto := lResto + StrToInt(Copy(lFunc, 8, 1)) * 4;
  lResto := lResto + StrToInt(Copy(lFunc, 7, 1)) * 5;
  lResto := lResto + StrToInt(Copy(lFunc, 6, 1)) * 6;
  lResto := lResto + StrToInt(Copy(lFunc, 5, 1)) * 7;
  lResto := lResto + StrToInt(Copy(lFunc, 4, 1)) * 8;
  lResto := lResto + StrToInt(Copy(lFunc, 3, 1)) * 9;
  lResto := lResto + StrToInt(Copy(lFunc, 2, 1)) * 2;
  lResto := lResto + StrToInt(Copy(lFunc, 1, 1)) * 3;

  lResto := lResto Mod 11;
  if (lResto = 0) Or (lResto = 1) Then
    lDAC := 0
  else
    lDAC := 11 - lResto;

  Result := pFunc + Trim(IntToStr(lDAC));
end;

{*-----------------------------------------------------------------------------
 *  TuntDigito.gf_Digito_ChecarProposta
 *        Funcao que checa o digito verificador.
 *-----------------------------------------------------------------------------}
//
// Funcao que checa o digito verificador.
//
function TuntDigito.gf_Digito_ChecarProposta(const psChave: string): Boolean;
var
  lsDigito:       string;
  lsSeqNova:      string;
  liSomatoria:    Integer;
  liVerificador:  Integer;
begin
 lsSeqNova   := Copy('00000000' + psChave, Length(Trim(psChave)) + 1, 8);
 lsDigito    := Copy(lsSeqNova, 8, 1);
 lsSeqNova   := Copy(lsSeqNova, 1, 7);
 liSomatoria := StrToInt(Copy(lsSeqNova, 7, 1)) * 2 +
                StrToInt(Copy(lsSeqNova, 6, 1)) * 3 +
                StrToInt(Copy(lsSeqNova, 5, 1)) * 4 +
                StrToInt(Copy(lsSeqNova, 4, 1)) * 7 +
                StrToInt(Copy(lsSeqNova, 3, 1)) * 8 +
                StrToInt(Copy(lsSeqNova, 2, 1)) * 9 + 13;
 liVerificador := 10 - (liSomatoria Mod 10);

 if liVerificador > 9 then
 begin
   if liVerificador < 0 then
      liVerificador := liVerificador * (-1);
   if liVerificador >= 10 then
      liVerificador := 0;
 end;

 if lsDigito = TrimLeft(IntToStr(liVerificador)) then
   Result := True
 else
   Result := False;

end;

{*-----------------------------------------------------------------------------
 *  TuntDigito.gfC_Extenso
 *
 *-----------------------------------------------------------------------------}
function TuntDigito.gfC_Extenso(pdValor: Double;
                                    psUnidade: String;
                                    psFracao: String;
                                    psTamanho: String;
                                    var pRetorno: String): Boolean;
var
  lsValorInt    : String;
  lsValorFrac   : String;
  lsUnidade     : String;
  lsFracao      : String;
  lsTerna       : String;

  lbGenero      : Boolean;
  lbFlagValInt  : Boolean;
  lbMontaTerna  : Boolean;
  liTamanhoStr  : Integer;
  liNumLinhas   : Integer;
  liContador    : Integer;
  liNumTernas   : Integer;
  liDefMilhar   : Integer;
  liDig1        : Integer;
  liDig2        : Integer;
  liDig3        : Integer;
  liValorFrac   : Integer;

  laiNumColunas: array[0..20] of Integer;
begin
  try
  dmDados.giStatus := STATUS_OK;

  // se o valor a ser convertido e zero ou nao tem unidade, entao encerro
  If pdValor = 0.0 Then
  begin
    Result := True;
    Exit;
  end;

  // inicializa vetores
  mpIniciaExtenso;

  // obtenho o numero de linhas e colunas a ser montado valor por extenso
  liNumLinhas := mfConverteTamLinhas(psTamanho, laiNumColunas);

  // obtenho a string do valor a ser convertido
  liTamanhoStr := mfConverteValor(lsValorInt, lsValorFrac, pdValor);

  // pego o genero e nome da unidade
  If pdValor > 1 Then
    lbGenero := mfPutMoeda(psUnidade, TIPO_PLURAL, lsUnidade)
  Else
    lbGenero := mfPutMoeda(psUnidade, TIPO_SINGULAR, lsUnidade);

  // inicializo gsBUFFER que contera a string por extenso
  gsBUFFER := '';
  liNumTernas := Trunc(liTamanhoStr / 3);
  liDefMilhar := liNumTernas - 1;

  lbFlagValInt := False;
  For liContador := 0 To liNumTernas - 1 do
  begin
    // monto em BUFFER o valor da terna por extenso
    lbMontaTerna := mfMontaTerna(Copy(lsValorInt, (liContador * 3) + 1, 1), Copy(lsValorInt, (liContador * 3) + 2, 1), Copy(lsValorInt, (liContador * 3) + 3, 1), lbGenero);
    lbFlagValInt := lbFlagValInt Or lbMontaTerna;
    If liDefMilhar <> 0 Then
    begin
      liDig1 := StrToInt(Copy(lsValorInt, (liContador * liNumTernas) + 1, 1));
      liDig2 := StrToInt(Copy(lsValorInt, ((liContador * liNumTernas) + 2), 1));
      liDig3 := StrToInt(Copy(lsValorInt, ((liContador * liNumTernas) + 3), 1));
      If (liDig1 * 100 + liDig2 * 10 + liDig3) <> 1 Then
        gsBUFFER := gsBUFFER + Def_Plur[liDefMilhar]
      Else
        gsBUFFER := gsBUFFER + Def_Sing[liDefMilhar];

      // se for miaor que milhar
      If liDefMilhar > 0 Then
      begin
        // guarda a  proxima terna
        lsTerna := Copy(lsValorInt, ((liContador + 1) * 3) + 1, 3);
        // se tiver dezena ou unidade
        If Copy(lsTerna, Length(lsTerna) - 1, 2) <> '00' Then
          gsBUFFER := gsBUFFER + CONCAT_EXTENSO_MILHAR
        // se nao tiver dezena e unidade, mas tiver centena
        Else If (Copy(lsTerna, 1, 1) <> '0') And (Copy(lsTerna, Length(lsTerna) - 1, 2) = '00') Then
          gsBUFFER := gsBUFFER + CONCAT_EXTENSO;
      end;
    end;
    liDefMilhar := liDefMilhar - 1;
  end;

  // coloco em BUFFER a unidade utilizada se houver
  If (Length(psUnidade) <> 0) And (lbFlagValInt) Then
  begin
    // se for mais que milhao
    If liTamanhoStr > 6 Then
    begin
      // se for numero redondo
      If Copy(lsValorInt, (liTamanhoStr - 6) + 1, 6) = '000000' Then
      begin
        gsBUFFER := gsBUFFER + ' DE ';
      end;
    end;
    gsBUFFER := gsBUFFER + lsUnidade;
  end;

  liValorFrac := StrToInt(lsValorFrac);

  // pego genero e nome da fracao
  If liValorFrac > 1 Then
    lbGenero := mfPutMoeda(psFracao, TIPO_PLURAL, lsFracao)
  Else
    lbGenero := mfPutMoeda(psFracao, TIPO_SINGULAR, lsFracao);

  If mfMontaTernaFracao(lsValorFrac, lbFlagValInt, lbGenero) Then
    gsBUFFER := gsBUFFER + lsFracao;

  mpDivideLinhas(laiNumColunas, liNumLinhas);

  // atribuo a string do valor por extenso
  pRetorno := gsBUFFER;

  Result := True;

  except
    Result := False;
    dmDados.ErroTratar('gfC_Extenso - uniDigito.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntDigito.mpIniciaExtenso
 *
 *-----------------------------------------------------------------------------}
procedure TuntDigito.mpIniciaExtenso();
begin

  If mbIniciaExtenso Then
    Exit;

  mbIniciaExtenso := True;

  unidade[0] := 'ZE-RO ';
  unidade[1] := 'UM ';
  unidade[2] := 'DOIS ';
  unidade[3] := 'TR�S ';
  unidade[4] := 'QUA-TRO ';
  unidade[5] := 'CIN-CO ';
  unidade[6] := 'SEIS ';
  unidade[7] := 'SE-TE ';
  unidade[8] := 'OI-TO ';
  unidade[9] := 'NO-VE ';
  unidade[10] := 'DEZ ';
  unidade[11] := 'ON-ZE ';
  unidade[12] := 'DO-ZE ';
  unidade[13] := 'TRE-ZE ';
  unidade[14] := 'QUA-TOR-ZE ';
  unidade[15] := 'QUIN-ZE ';
  unidade[16] := 'DE-ZES-SEIS ';
  unidade[17] := 'DE-ZES-SE-TE ';
  unidade[18] := 'DE-ZOI-TO ';
  unidade[19] := 'DE-ZE-NO-VE ';

  Unid_Fem[0] := 'ZE-RO ';
  Unid_Fem[1] := 'UMA ';
  Unid_Fem[2] := 'DUAS ';
  Unid_Fem[3] := 'TR�S ';
  Unid_Fem[4] := 'QUA-TRO ';
  Unid_Fem[5] := 'CIN-CO ';
  Unid_Fem[6] := 'SEIS ';
  Unid_Fem[7] := 'SE-TE ';
  Unid_Fem[8] := 'OI-TO ';
  Unid_Fem[9] := 'NO-VE ';
  Unid_Fem[10] := 'DEZ ';
  Unid_Fem[11] := 'ON-ZE ';
  Unid_Fem[12] := 'DO-ZE ';
  Unid_Fem[13] := 'TRE-ZE ';
  Unid_Fem[14] := 'QUA-TOR-ZE ';
  Unid_Fem[15] := 'QUIN-ZE ';
  Unid_Fem[16] := 'DE-ZES-SEIS ';
  Unid_Fem[17] := 'DE-ZES-SE-TE ';
  Unid_Fem[18] := 'DE-ZOI-TO ';
  Unid_Fem[19] := 'DE-ZE-NO-VE ';

  Dezena[0] := '';
  Dezena[1] := 'DEZ ';
  Dezena[2] := 'VIN-TE ';
  Dezena[3] := 'TRIN-TA ';
  Dezena[4] := 'QUA-REN-TA ';
  Dezena[5] := 'CIN-QUEN-TA ';
  Dezena[6] := 'SES-SEN-TA ';
  Dezena[7] := 'SE-TEN-TA ';
  Dezena[8] := 'OI-TEN-TA ';
  Dezena[9] := 'NO-VEN-TA ';

  Centena[0] := 'CEM ';
  Centena[1] := 'CEN-TO ';
  Centena[2] := 'DU-ZEN-TOS ';
  Centena[3] := 'TRE-ZEN-TOS ';
  Centena[4] := 'QUA-TRO-CEN-TOS ';
  Centena[5] := 'QUI-NHEN-TOS ';
  Centena[6] := 'SEIS-CEN-TOS ';
  Centena[7] := 'SE-TE-CEN-TOS ';
  Centena[8] := 'OI-TO-CEN-TOS ';
  Centena[9] := 'NO-VE-CEN-TOS ';

  Cent_Fem[0] := 'CEM ';
  Cent_Fem[1] := 'CEN-TO ';
  Cent_Fem[2] := 'DU-ZEN-TAS ';
  Cent_Fem[3] := 'TRE-ZEN-TAS ';
  Cent_Fem[4] := 'QUA-TRO-CEN-TAS ';
  Cent_Fem[5] := 'QUI-NHEN-TAS ';
  Cent_Fem[6] := 'SEIS-CEN-TAS ';
  Cent_Fem[7] := 'SE-TE-CEN-TAS ';
  Cent_Fem[8] := 'OI-TO-CEN-TAS ';
  Cent_Fem[9] := 'NO-VE-CEN-TAS ';

  Def_Sing[0] := '';
  Def_Sing[1] := 'MIL ';
  Def_Sing[2] := 'MI-LH�O ';
  Def_Sing[3] := 'BI-LH�O ';
  Def_Sing[4] := 'TRI-LH�O ';
  Def_Sing[5] := 'QUA-TRI-LH�O ';

  Def_Plur[0] := '';
  Def_Plur[1] := 'MIL ';
  Def_Plur[2] := 'MI-LH�ES ';
  Def_Plur[3] := 'BI-LH�ES ';
  Def_Plur[4] := 'TRI-LH�ES ';
  Def_Plur[5] := 'QUA-TRI-LH�ES ';

end;

{*-----------------------------------------------------------------------------
 *  TuntDigito.mfConverteTamLinhas
 *
 *-----------------------------------------------------------------------------}
Function TuntDigito.mfConverteTamLinhas(psTamanho: String;
                                        var pNumColunas: array of Integer): Integer;
var
  liTamanho    : Integer;
  liContaLinha : Integer;
  liPtoVirgula : Integer;
begin
  try
  dmDados.giStatus := STATUS_OK;

  liTamanho := 1;
  liContaLinha := 0;

  // armazeno o numero de colunas ( CARACTERES )
  liPtoVirgula := Pos(PONTO_VIRGULA, Copy(psTamanho, liTamanho, Length(psTamanho) - (liTamanho - 1)));
  While (liPtoVirgula <> 0) And (liContaLinha < 20) do
  begin
    pNumColunas[liContaLinha] := StrToInt(Copy(psTamanho, liTamanho, liPtoVirgula - 1));
    liTamanho := liPtoVirgula + 1;
    liPtoVirgula := Pos(PONTO_VIRGULA, Copy(psTamanho, liTamanho, Length(psTamanho) - (liTamanho - 1)));
    liContaLinha := liContaLinha + 1;
  end;
  pNumColunas[liContaLinha] := StrToInt(Copy(psTamanho, liTamanho, Length(psTamanho)));
  liContaLinha := liContaLinha + 1;

  Result := liContaLinha;

  except
    Result := 0;
    dmDados.ErroTratar('mfConverteTamLinhas - uniDigito.Pas');
  End;

End;

{*-----------------------------------------------------------------------------
 *  TuntDigito.mfConverteValor
 *
 *-----------------------------------------------------------------------------}
Function TuntDigito.mfConverteValor(Var psValorInt: String;
                                        var psValorFrac: String;
                                        pdValor: Double): Integer;
var
  ldFrac: Double;
begin
  try
  dmDados.giStatus := STATUS_OK;

  // guarda a parte inteira
  psValorInt := Trim(IntToStr(Trunc(pdValor)));

  // guarda a parte fracionaria
  ldFrac := pdValor - Int(pdValor);
  psValorFrac := Copy(FormatFloat('#.00####', ldFrac), 2, Length(FormatFloat('#.00####', ldFrac)));

  // coloco em psValorInt ZEROS a ESQUERDA para transformar em um numero multiplo de 3
  Case Length(psValorInt) Mod 3 of
    1:
      psValorInt := CARAC_ZERO + CARAC_ZERO + psValorInt;
    2:
      psValorInt := CARAC_ZERO + psValorInt;
  End;

  // retorno o tamanho da string com zeros a esquerda
  Result := Length(psValorInt);

  except
    Result := 0;
    dmDados.ErroTratar('mfConverteValor - uniDigito.Pas');
  End;

End;

{*-----------------------------------------------------------------------------
 *  TuntDigito.mfPutMoeda
 *
 *-----------------------------------------------------------------------------}
Function TuntDigito.mfPutMoeda(psUnidade: String;
                                   piTipoUnidade: Integer;
                                   var psMoeda: String): Boolean;
var
  lsMoedaSing : String;
  lsMoedaPlur : String;
  lsGenero    : String;
  liContador  : Integer;
begin
  try
  dmDados.giStatus := STATUS_OK;

  // obtenho a posicao do ponto e virgula
  liContador := Pos(PONTO_VIRGULA, psUnidade);
  If liContador <> 0 Then
  begin
    // obtenho a unidade no singular
    lsMoedaSing := Copy(psUnidade, 1, liContador - 1);
    // obtenho o plural
    lsMoedaPlur := Copy(psUnidade, liContador + 1, Length(psUnidade));
  end;

  // obtenho o genero
  liContador := Pos(PONTO_VIRGULA, Copy(psUnidade, liContador + 1, Length(psUnidade)));
  If liContador <> 0 Then
    lsGenero := Copy(psUnidade, liContador + 1, 1)
  Else
    lsGenero := CARAC_M_MAI;

  If piTipoUnidade = TIPO_PLURAL Then
    psMoeda := lsMoedaPlur
  Else
    psMoeda := lsMoedaSing;

  If UpperCase(lsGenero) = CARAC_F_MAI Then
    Result := False
  Else
    Result := True;

  except
    Result := False;
    dmDados.ErroTratar('mfPutMoeda - uniDigito.Pas');
  End;

End;

{*-----------------------------------------------------------------------------
 *  TuntDigito.mfMontaTerna
 *
 *-----------------------------------------------------------------------------}
Function TuntDigito.mfMontaTerna(psCentena: String;
                                     psDezena: String;
                                     psUnidade: String;
                                     pbGenero: Boolean): Boolean;
var
  liCentena: Integer;
  liDezena : Integer;
  liUnidade: Integer;
begin
  try
  dmDados.giStatus := STATUS_OK;

  liCentena := StrToInt(psCentena);
  liDezena := StrToInt(psDezena);
  liUnidade := StrToInt(psUnidade);

  If (liCentena = 0) And (liDezena = 0) And (liUnidade = 0) Then
  begin
    Result := False;
    Exit;
  end;

  // se centena nao e zero, concateno e coloco o caracter e no final
  If liCentena <> 0 Then
  begin
    If (liCentena = 1) And (liDezena = 0) And (liUnidade = 0) Then
    begin
      gsBUFFER := gsBUFFER + Centena[0];
    end
    Else
    begin
      If pbGenero Then
        gsBUFFER := gsBUFFER + Centena[liCentena]
      Else
        gsBUFFER := gsBUFFER + Cent_Fem[liCentena];

      If (liDezena <> 0) Or (liUnidade <> 0) Then
        gsBUFFER := gsBUFFER + CONCAT_EXTENSO;
    end;
  end;

  If liDezena >= 2 Then
  begin
    gsBUFFER := gsBUFFER + Dezena[liDezena];
    If liUnidade <> 0 Then
    begin
      gsBUFFER := gsBUFFER + CONCAT_EXTENSO;
      If pbGenero Then
        gsBUFFER := gsBUFFER + unidade[liUnidade]
      Else
        gsBUFFER := gsBUFFER + Unid_Fem[liUnidade];

    end;
  end
  Else
  begin
    If liDezena = 1 Then
    begin
      If pbGenero Then
        gsBUFFER := gsBUFFER + unidade[10 + liUnidade]
      Else
        gsBUFFER := gsBUFFER + Unid_Fem[10 + liUnidade];
    end
    Else
    begin
      If liUnidade <> 0 Then
        If pbGenero Then
          gsBUFFER := gsBUFFER + unidade[liUnidade]
        Else
          gsBUFFER := gsBUFFER + Unid_Fem[liUnidade];
    end;
  end;

  Result := True;

  except
    Result := False;
    dmDados.ErroTratar('mfMontaTerna - uniDigito.Pas');
  End;

End;

{*-----------------------------------------------------------------------------
 *  TuntDigito.mfMontaTernaFracao
 *
 *-----------------------------------------------------------------------------}
Function TuntDigito.mfMontaTernaFracao(psValorFrac: String;
                                           pbFlagValInt: Boolean;
                                           pbGenero: Boolean): Boolean;
var
  liPriDig : Integer;
  liSegDig : Integer;
begin

  try
  dmDados.giStatus := STATUS_OK;

  liPriDig := StrToInt(Copy(psValorFrac, 1, 1));
  liSegDig := StrToInt(COpy(psValorFrac, 2, 1));

  // se existe parte fracionaria
  If (liPriDig <> 0) Or (liSegDig <> 0) Then
  begin
    // se exsite parte inteira ...
    If pbFlagValInt Then
      gsBUFFER := gsBUFFER + CONCAT_EXTENSO_FRAC;

    // se a dezena for maior que 10
    If liPriDig >= 2 Then
    begin
      gsBUFFER := gsBUFFER + Dezena[liPriDig];
      // se tiver unidade
      If liSegDig <> 0 Then
      begin
        gsBUFFER := gsBUFFER + CONCAT_EXTENSO;
        If pbGenero Then
          gsBUFFER := gsBUFFER + unidade[liSegDig]
        Else
          gsBUFFER := gsBUFFER + Unid_Fem[liSegDig];
      end;
    end
    Else
    begin
      // se tiver dezena
      If liPriDig = 1 Then
      begin
        If pbGenero Then
          gsBUFFER := gsBUFFER + unidade[10 + liSegDig]
        Else
          gsBUFFER := gsBUFFER + Unid_Fem[10 + liSegDig];
      end
      Else
      begin
        // se tiver unidade
        If liSegDig <> 0 Then
          If pbGenero Then
            gsBUFFER := gsBUFFER + unidade[liSegDig]
          Else
            gsBUFFER := gsBUFFER + Unid_Fem[liSegDig];
      end;
    end;
    Result := True;
  end
  Else
  begin
    Result := False;
  end;

  except
    Result := False;
    dmDados.ErroTratar('mfMontaTernaFracao - uniDigito.Pas');
  End;

End;

{*-----------------------------------------------------------------------------
 *  TuntDigito.mpDivideLinhas
 *
 *-----------------------------------------------------------------------------}
procedure TuntDigito.mpDivideLinhas(paiNumColunas: array of Integer;
                                        piNumLinhas: Integer);
var
  lsBufferAux: String;
  lsSilaba   : String;
  lsLinha    : String;

  liTamBuffer : Integer;
  liTamStr    : Integer;
  liTamSilaba : Integer;
  liTamLinha  : Integer;
  liQtdeLinhas: Integer;
begin

  try
  dmDados.giStatus := STATUS_OK;

  liTamStr := 1;
  liQtdeLinhas := 0;

  // inicializo variaveis
  liTamBuffer := Length(gsBUFFER);

  // enquanto existir texto e nao acabar o espaco (colunas X linhas)
  While (liTamStr <= liTamBuffer) And (paiNumColunas[liQtdeLinhas] <> 0) do
  begin
    // obtenho uma silaba
    liTamSilaba := 0;
    lsSilaba := '';
    While (Copy(gsBUFFER, liTamStr, 1) <> HIFEN) And (Copy(gsBUFFER, liTamStr, 1) <> ESPACO) And (Length(gsBUFFER) >= liTamStr) do
    begin
      lsSilaba := lsSilaba + Copy(gsBUFFER, liTamStr, 1);
      liTamSilaba := liTamSilaba + 1;
      liTamStr := liTamStr + 1;
    end;

    // obtenho o tamanho da linha ja montada
    liTamLinha := Length(lsLinha);

    // se nao couber na linha
    If liTamLinha + liTamSilaba > paiNumColunas[liQtdeLinhas] Then
    begin
      If Copy(lsLinha, liTamLinha - 1, 1) <> ESPACO Then
        lsLinha := lsLinha + HIFEN + LINE_FEED
      Else
        lsLinha := lsLinha + LINE_FEED;

      lsBufferAux := lsBufferAux + lsLinha;
      lsLinha := '';
      liQtdeLinhas := liQtdeLinhas + 1;
    end;

    lsLinha := lsLinha + lsSilaba;
    // verifico se penultimo caracter da string original e ESPACO
    If Copy(gsBUFFER, liTamStr, 1) = ESPACO Then
      lsLinha := lsLinha + ESPACO;
    liTamStr := liTamStr + 1;
  end;

  // se sobrou alguma coisa em linha
  If paiNumColunas[liQtdeLinhas] <> 0 Then
    lsBufferAux := lsBufferAux + lsLinha;

  // se nao coube preenche com:teriscos
  If liTamStr < liTamBuffer Then
  begin
    liQtdeLinhas := 0;
//    liTamStr := 0;
    While paiNumColunas[liQtdeLinhas] <> 0 do
    begin
      liTamLinha := 0;
      While liTamLinha < paiNumColunas[liQtdeLinhas] do
      begin
        lsBufferAux := lsBufferAux + ASTERISCO;
        liTamLinha := liTamLinha + 1;
      end;
      lsBufferAux := lsBufferAux + LINE_FEED;
//      liTamLinha := liTamLinha + 1;
      liQtdeLinhas := liQtdeLinhas + 1;
//      liTamStr := liTamStr + liTamLinha;
    end;
  end;

  // copio para buffer definitivo
  gsBUFFER := lsBufferAux;

  except
    dmDados.ErroTratar('mpDivideLinhas - uniDigito.Pas');
  End;

End;

end.
