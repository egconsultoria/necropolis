unit uniLocalizar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, ExtCtrls,
  uniConst;

type
  TdlgLocalizar = class(TForm)
    lblCadastro: TLabel;
    pnlMetade: TPanel;
    lblChave: TLabel;
    spbPrimeiro: TSpeedButton;
    spbPagAnterior: TSpeedButton;
    spbRegAnterior: TSpeedButton;
    spbRegProximo: TSpeedButton;
    spbPagProximo: TSpeedButton;
    spbUltimo: TSpeedButton;
    lsvSelec: TListView;
    edtChave: TEdit;
    btnEncontrar: TButton;
    pnlLinha: TPanel;
    pnlEspera: TPanel;
    btnConsultar: TButton;
    btnCancelar: TButton;
    lblCad: TLabel;
    cboChaveProcura: TComboBox;
    lblChaveProcura: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spbPrimeiroClick(Sender: TObject);
    procedure edtChaveChange(Sender: TObject);
    procedure lsvSelecColumnClick(Sender: TObject; Column: TListColumn);
    procedure btnConsultarClick(Sender: TObject);
    procedure lsvSelecClick(Sender: TObject);
    procedure btnEncontrarClick(Sender: TObject);
    procedure edtChaveKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cboChaveProcuraChange(Sender: TObject);
  private
    { Private declarations }
    masColunas_Chave: array[0..2] of String;
    mrParametro: TumParametro;
    maSpeedButs: array[0..5] of TSpeedButton;
    mrColunasSpread: array[1..2] of TumaGrid_Coluna;
    mrID_Linhas: array[1..11] of TumId_Linhas;
    {' Contem a conex�o para os dados da Lista (Spread)}
    mrSpreadInfo:       TumGridMontar_Info;
    miConChaves: integer;

    procedure mp_SelPar_Desabilitar;
    procedure mp_SelPar_Habilitar;

  public
    { Public declarations }
  end;

var
  dlgLocalizar: TdlgLocalizar;

implementation

uses uniParam, uniDados, uniSelPar, uniSeleciona, uniCombos, uniFuncoes;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TdlgLocalizar.FormShow        prepara e mostra a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgLocalizar.FormShow(Sender: TObject);
var
  lbProssegue: Boolean;
  i : Word;
  lvSelect: String;
  gaParm: array[0..5] of Variant;
begin

  Screen.Cursor := crHOURGLASS;

  { limpa o combo e o list view}
  lsvSelec.Items.Clear;
  edtChave.Text := VAZIO;

  {Inicializa gSsParametros e gSnapTelas}
  untParam.gp_Param_Iniciar;

  gsCod_Valor := VAZIO;
  gsDesc_Valor := VAZIO;

  { Ajustando a largura/identifica��o das colunas}
  lsvSelec.Columns.Items[0].Width := 100;
  lsvSelec.Columns.Items[1].Width := 445;
  { Ajustando o label da caixa de pesquisa}
  lblChave.Caption := 'Digite a chave de pesquisa para a coluna "C�digo" e pressione [Encontrar]';
  gbCodigo := True;

  { lista com os botoes de controle da lista de registros}
  maSpeedButs[0] := spbPrimeiro;
  maSpeedButs[1] := spbPagAnterior;
  maSpeedButs[2] := spbRegAnterior;
  maSpeedButs[3] := spbRegProximo;
  maSpeedButs[4] := spbPagProximo;
  maSpeedButs[5] := spbUltimo;

  For i := 1 To High(gaLista_Parametros) do
  begin
//    If gaLista_Parametros[i].Nome_Tabela = gsCod_Parametro Then
    If gaLista_Parametros[i].Sigla_Tabela = gsCod_Parametro Then
    begin
      mrParametro := gaLista_Parametros[i];
      grTelaVar.P := mrParametro;
      lbProssegue := True;
      Break;
    End
    else
      lbProssegue := False;
  end;

  { se for p/ prosseguir}
  if lbProssegue then
  begin
    pnlEspera.Visible := True;

    gaParm[0] := 'zLocalizar.*';
    gaParm[1] := 'zLocalizar';
    gaParm[2] := 'Sigla_Tabela';
    gaParm[3] := mrParametro.Sigla_Tabela ;
    lvSelect := dmDados.SqlVersao('PARAM_0009', gaParm);
    miConChaves := dmDados.ExecutarSelect(lvSelect);
    cboChaveProcura.Items.Clear;
    miConChaves := untCombos.gf_ComboCadastro_Estufa(cboChaveProcura, 'PARAM_0009', 'Nome_Campo', miConChaves, gaParm);
    if cboChaveProcura.Items.Count > 0 then
    begin
      cboChaveProcura.ItemIndex := 0;
      lblChaveProcura.Visible := True;
      cboChaveProcura.Visible := True;
    end
    else
    begin
      lblChaveProcura.Visible := False;
      cboChaveProcura.Visible := False;
    end;

    mrSpreadInfo.Conexao := IOPTR_NOPOINTER;

    gpMascara := mrParametro.Mascara_Codigo;
    gpAlinha := mrParametro.Alinha_Codigo;
//    gpAlinhaTamanho := mrParametro.Alinha_Tamanho;
    gpAlinhaTamanho := mrParametro.Codigo_Minimo;

    If dmDados.giStatus <> STATUS_OK Then
    begin
      Screen.Cursor := crDefault;
      Close;
      Exit;
    End;

    mp_SelPar_Desabilitar;
    masColunas_Chave[0] := mrParametro.Coluna_Codigo;
    masColunas_Chave[1] := mrParametro.Coluna_Descricao;
    if untSelPar.gf_SelPar_Nova_Lista(mrSpreadInfo,
                                      mrColunasSpread,
                                      mrID_Linhas,
                                      mrParametro,
                                      maSpeedButs,
                                      edtChave,
                                      lsvSelec,
                                      masColunas_Chave) then
      mp_SelPar_Habilitar;

    dlgLocalizar.Caption := 'Procurar [' + mrParametro.Entidade + ']';

    lblCad.Caption := mrParametro.Entidade;

    { checa pela ordem default - se for descricao muda }
//    if mrParametro.Coluna_Nivel = 'COLUNA_DESCRICAO' then
    if mrParametro.Tipo_Objeto = 'D' then
    begin
      if gbCodigo then
      begin
        lsvSelecColumnClick(Sender, lsvSelec.Column[1]);
        gbCodigo := False;
       { guarda a coluna de ordenacao para navegacao }
        mrSpreadInfo.ColunaOrdem := 1;
      end;
    end
    else
    begin
      if not gbCodigo then
      begin
        lsvSelecColumnClick(Sender, lsvSelec.Column[0]);
        gbCodigo := True;
        { guarda a coluna de ordenacao para navegacao }
        mrSpreadInfo.ColunaOrdem := 0;
      end;
    end;

    { posiciona automaticamente no valor passado}
    if (gpProcurar <> VAZIO) and (not gbCodigo) then
    begin
      edtChave.TEXT := gpProcurar;
      btnEncontrarClick(btnEncontrar);
    end;

    pnlEspera.Visible := False;

  end;

  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgLocalizar.mp_SelPar_Desabilitar        prepara e mostra a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgLocalizar.mp_SelPar_Desabilitar;
begin

  btnConsultar.Enabled := False;
  btnEncontrar.Enabled := False;
  edtChave.Enabled := False;
  lblChave.Enabled := False;
  lsvSelec.Enabled := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgLocalizar.mp_SelPar_Habilitar        prepara e mostra a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgLocalizar.mp_SelPar_Habilitar;
begin

  btnConsultar.Enabled := True;
  btnEncontrar.Enabled := (edtChave.Text <> VAZIO);
  edtChave.Enabled := True;
  lblChave.Enabled := True;
  lsvSelec.Enabled := True;

end;

{*-----------------------------------------------------------------------------
 *  TdlgLocalizar.btnCancelarClick        Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgLocalizar.btnCancelarClick(Sender: TObject);
begin
  dlgLocalizar.Close;
end;

{*-----------------------------------------------------------------------------
 *  TdlgLocalizar.FormClose       Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgLocalizar.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  if (miConChaves <> 0) or (miConChaves <> IOPTR_NOPOINTER) then
    dmDados.FecharConexao(miConChaves);

  if not ((mrSpreadInfo.Conexao = 0) or (mrSpreadInfo.Conexao = IOPTR_NOPOINTER)) then
  begin
    { fecha o dynaset}
    dmDados.FecharConexao(mrSpreadInfo.Conexao);
    mrSpreadInfo.Conexao := IOPTR_NOPOINTER;
  end;
  gpProcurar := VAZIO;
  gpMascara := VAZIO;
  gpAlinha := VAZIO;
  gpAlinhaTamanho := 0;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgLocalizar.spbPrimeiroClick
'     muda a lista de acordo com a tecla apertada
'-------------------------------------------------------------------------------------------------}
procedure TdlgLocalizar.spbPrimeiroClick(Sender: TObject);
var
  lspbBotao: TSpeedButton;
begin

  pnlEspera.Visible := True;

  lspbBotao := TSpeedButton(Sender);

  edtChave.Text := VAZIO;

  if lspbBotao.Name = spbPrimeiro.Name then
    mrSpreadInfo.Acao := PRIMEIRA_PAGINA
  else if lspbBotao.Name = spbPagAnterior.Name then
        mrSpreadInfo.Acao := PAGINA_TRAS
  else if lspbBotao.Name = spbRegAnterior.Name then
        mrSpreadInfo.Acao := LINHA_TRAS
  else if lspbBotao.Name = spbRegProximo.Name then
        mrSpreadInfo.Acao := LINHA_FRENTE
  else if lspbBotao.Name = spbPagProximo.Name then
        mrSpreadInfo.Acao := PAGINA_FRENTE
  else if lspbBotao.Name = spbUltimo.Name then
        mrSpreadInfo.Acao := ULTIMA_PAGINA;

  untSeleciona.gf_Selec_Spread_Montar(lsvSelec, mrColunasSpread, mrSpreadInfo, mrID_Linhas, masColunas_Chave);

  untSelPar.gp_SelPar_Ativar_ScrollBtns(mrSpreadInfo, maSpeedButs, lsvSelec);

  if lsvSelec.CanFocus then
    lsvSelec.SetFocus;

  pnlEspera.Visible := False;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgLocalizar.edtChaveChange
'     Prepara a tela
'-------------------------------------------------------------------------------------------------}
procedure TdlgLocalizar.edtChaveChange(Sender: TObject);
var
  lbChave_Valida: Boolean;
begin

  mrSpreadInfo.MudouProcura := True;
  mrSpreadInfo.ChaveProcura := Trim(edtChave.Text);
  lbChave_Valida := (mrSpreadInfo.ChaveProcura <> VAZIO);
  btnEncontrar.Enabled := lbChave_Valida;
  btnEncontrar.Default := lbChave_Valida;
  btnConsultar.Default := not lbChave_Valida;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgLocalizar.btnConsultarClick
'     verifica se pode consultar
'-------------------------------------------------------------------------------------------------}
procedure TdlgLocalizar.btnConsultarClick(Sender: TObject);
begin
  Screen.Cursor := crHOURGLASS;
  gsCod_Valor := lsvSelec.Selected.Caption;
  gsDesc_Valor := Trim(lsvSelec.Selected.SubItems.Text);
  dlgLocalizar.Close;
end;

{'-------------------------------------------------------------------------------------------------
' TdlgLocalizar.lsvSelecColumnClick
'
'-------------------------------------------------------------------------------------------------}
procedure TdlgLocalizar.lsvSelecColumnClick(Sender: TObject;
  Column: TListColumn);
begin

  Screen.Cursor := crHOURGLASS;
  pnlEspera.Visible := True;
  dlgLocalizar.Refresh;

  if Column.Index = 0 then
  begin
    lblChave.Caption := 'Digite a chave de pesquisa para a coluna ''C�digo'' e pressione [Encontrar]';
    gbCodigo := True;
    if cboChaveProcura.Visible then
      cboChaveProcura.Text := 'Codigo';
  end;

  if Column.Index = 1 then
  begin
    lblChave.Caption := 'Digite a chave de pesquisa para a coluna ''Descri��o'' e pressione [Encontrar]';
    gbCodigo := False;
    if cboChaveProcura.Visible then
      cboChaveProcura.Text := 'Descricao';
  end;

  mp_SelPar_Desabilitar;
  if untSelPar.gf_SelPar_Nova_Lista(mrSpreadInfo,
                                    mrColunasSpread,
                                    mrID_Linhas,
                                    mrParametro,
                                    maSpeedButs,
                                    edtChave,
                                    lsvSelec,
                                    masColunas_Chave) then
      mp_SelPar_Habilitar;

  pnlEspera.Visible := False;

  Screen.Cursor := crDefault;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgLocalizar.btnEncontrarClick
'
'-------------------------------------------------------------------------------------------------}
procedure TdlgLocalizar.btnEncontrarClick(Sender: TObject);
var
  lsSql: String;
  liConAuxiliar : integer;
  gaParm: array[0..5] of variant;
  iIndColCodigo: integer;
  lbAchou: Boolean;
begin

  dlgLocalizar.Enabled := False;
  Screen.Cursor := crHOURGLASS;
  pnlEspera.Visible := True;
  dlgLocalizar.Refresh;

  iIndColCodigo := IOPTR_NOPOINTER;
  lbAchou := False;

  if gbCodigo then
  begin
    if (cboChaveProcura.Visible) and (cboChaveProcura.Text <> 'Codigo') and (cboChaveProcura.Text <> 'Descricao')  then
    begin
      gaParm[0] := edtChave.Text; // + CTEDB_LIKETODOSORCL;
      lsSql := untCombos.gf_ComboCadastro_Conteudo(miConChaves, 'Nome_Campo', cboChaveProcura.Text, 'SQL');
      lsSql := dmDados.LiteralSubstituir(lsSql,gaParm);
      liConAuxiliar := dmDados.ExecutarSelect(lsSql);
      mrSpreadInfo.MudouProcura := True;
      { se tem alguma coisa}
      if not dmDados.Status(liConAuxiliar, IOSTATUS_NOROWS) then
      begin
        mrSpreadInfo.ChaveProcura := Trim(dmDados.ValorColuna(liConAuxiliar, 'Codigo', iIndColCodigo));
        lbAchou := True;
      end
      else
        {Qualquer coisa p/ nao conseguir achar}
        mrSpreadInfo.ChaveProcura := TNUMEROS;

      dmDados.FecharConexao(liConAuxiliar);
      mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[1].Nome;
      if lbAchou then
      begin
        gsCod_Valor := mrSpreadInfo.ChaveProcura;
        dlgLocalizar.Close;
      end;
    end
    else
    begin
      { se for procura por codigo}
      if (cboChaveProcura.Visible) and (cboChaveProcura.Text = 'Codigo') then
      begin
        if (gpAlinha <> VAZIO) and (gpAlinhaTamanho <> 0) Then
          if gpAlinha = ALINHA_ZERO then
            edtChave.TEXT := untFuncoes.gf_Zero_Esquerda(Trim(edtChave.TEXT), gpAlinhaTamanho);
        edtChave.TEXT := mrSpreadInfo.ChaveProcura;
        mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[1].Nome;
      end
      else
        mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[2].Nome;
      { se so poder por codigo e descricao}
      if not (cboChaveProcura.Visible) then
      begin
        if (gpAlinha <> VAZIO) and (gpAlinhaTamanho <> Length(edtChave.Text)(*0*)) Then
          if gpAlinha = ALINHA_ZERO then
            edtChave.TEXT := untFuncoes.gf_Zero_Esquerda(Trim(edtChave.TEXT), gpAlinhaTamanho);
        edtChave.TEXT := mrSpreadInfo.ChaveProcura;
        mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[1].Nome;
      end;
    end;
  end
  else
  begin
    if (cboChaveProcura.Visible) and (cboChaveProcura.Text <> 'Descricao') and (cboChaveProcura.Text <> 'Codigo') then
    begin
      gaParm[0] := edtChave.Text; // + CTEDB_LIKETODOSORCL;
      lsSql := untCombos.gf_ComboCadastro_Conteudo(miConChaves, 'Nome_Campo', cboChaveProcura.Text, 'SQL');
      lsSql := dmDados.LiteralSubstituir(lsSql,gaParm);
      liConAuxiliar := dmDados.ExecutarSelect(lsSql);
      mrSpreadInfo.MudouProcura := True;
      { se tem alguma coisa}
      if not dmDados.Status(liConAuxiliar, IOSTATUS_NOROWS) then
      begin
        mrSpreadInfo.ChaveProcura := Trim(dmDados.ValorColuna(liConAuxiliar, 'Nome', iIndColCodigo));
        lbAchou := True;
      end
      else
        {Qualquer coisa p/ nao conseguir achar}
        mrSpreadInfo.ChaveProcura := Copy(TALFANUM,1,20);

      dmDados.FecharConexao(liConAuxiliar);
      mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[2].Nome;
      if lbAchou then
      begin
        gsCod_Valor := mrSpreadInfo.ChaveProcura;
        dlgLocalizar.Close;
      end;
    end
    else
    begin
      { se for procura por Descricao}
      if (cboChaveProcura.Visible) and (cboChaveProcura.Text = 'Descricao') then
        mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[2].Nome
      else
        mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[1].Nome;
      { se so poder por codigo e descricao}
      if not (cboChaveProcura.Visible) then
        mrSpreadInfo.ColunaProcura.Nome := mrColunasSpread[2].Nome
    end;
  end;

  { se nao achou - diferente para codigo ou descricao }
  if not lbAchou then
    if untSeleciona.gf_Selec_Spread_Montar(lsvSelec, mrColunasSpread, mrSpreadInfo, mrID_Linhas, masColunas_Chave) then
      mrSpreadInfo.MudouProcura := False;

  untSelPar.gp_SelPar_Ativar_ScrollBtns(mrSpreadInfo, maSpeedButs, lsvSelec);

  if lsvSelec.CanFocus then
    lsvSelec.SetFocus;

  pnlEspera.Visible := False;
  dlgLocalizar.Enabled := True;
  Screen.Cursor := crDefault;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgLocalizar.lsvSelecClick
'     atualiza botoes de navegacao
'-------------------------------------------------------------------------------------------------}
procedure TdlgLocalizar.lsvSelecClick(Sender: TObject);
begin
  { atualiza botoes de navegacao}
  untSelPar.gp_SelPar_Ativar_ScrollBtns(mrSpreadInfo, maSpeedButs, lsvSelec)

end;

{'-------------------------------------------------------------------------------------------------
' TdlgLocalizar.edtChaveKeyDown
'     atualiza botoes de navegacao
'-------------------------------------------------------------------------------------------------}
procedure TdlgLocalizar.edtChaveKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  {Para filtrar o teclado e n�o estragar o cursor do spread}
  case Key of
    {Primeira Pagina}
    KEY_HOME:
    begin
      {Pressionou Control-Home}
      if (Shift = [ssCtrl]) then
        spbPrimeiroClick(spbPrimeiro)
      else
        Exit;
    end;
    KEY_PRIOR:
      spbPrimeiroClick(spbPagAnterior);
    KEY_UP:
      spbPrimeiroClick(spbRegAnterior);
    KEY_DOWN:
      spbPrimeiroClick(spbRegProximo);
    KEY_NEXT:
      spbPrimeiroClick(spbPagProximo);
    KEY_END:
    begin
      {Pressionou Control-end}
      if (Shift = [ssCtrl])then
        spbPrimeiroClick(spbUltimo)
      else
        Exit;
    end;
    KEY_C:
    begin
      if (Shift = [ssAlt])then
        gbCodigo := True
      else
        Exit;
    end;
    KEY_D:
    begin
      if (Shift = [ssAlt])then
        gbCodigo := False
      else
        Exit;
    end;
  else
      {Sem zerar o Keycode}
      Exit;
  end;

  Key := 0;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgLocalizar.cboChaveProcuraChange
'     atualiza label
'-------------------------------------------------------------------------------------------------}
procedure TdlgLocalizar.cboChaveProcuraChange(Sender: TObject);
begin

  lblChave.Caption := 'Digite a chave de pesquisa para a coluna ''' + cboChaveProcura.Text + ''' e pressione [Encontrar]';

end;

end.
