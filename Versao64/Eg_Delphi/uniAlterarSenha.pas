unit uniAlterarSenha;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TdlgAlterarSenha = class(TForm)
    pnlAlterarSenha: TPanel;
    edtNovaSenha: TEdit;
    edtConfirmacao: TEdit;
    lblNovaSenha: TLabel;
    lblConfirmacao: TLabel;
    btnCancelar: TButton;
    btnOK: TButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    gsNovaSenha: String;
  end;

var
  dlgAlterarSenha: TdlgAlterarSenha;

implementation

uses uniDados, uniAbout, uniConst;

{$R *.DFM}

{'-------------------------------------------------------------------------------------------------
' TdlgAlterarSenha.btnCancelarClick
'     fecha a tela
'-------------------------------------------------------------------------------------------------}
procedure TdlgAlterarSenha.btnCancelarClick(Sender: TObject);
begin
  { fecha a tela}
  dlgAlterarSenha.Close;
end;

{'-------------------------------------------------------------------------------------------------
' TdlgAlterarSenha.btnOKClick
'     Confirma a mudanca de senha
'-------------------------------------------------------------------------------------------------}
procedure TdlgAlterarSenha.btnOKClick(Sender: TObject);
begin
    If Trim(edtNovaSenha.Text) <> Trim(edtConfirmacao.Text) Then
    begin
        {' Senha n�o confere}
        dmDados.MensagemExibir(VAZIO, 50);
        edtConfirmacao.SetFocus;
        edtConfirmacao.SelStart := 0;
        edtConfirmacao.SelLength := Length(edtConfirmacao.Text);
    end
    Else
    begin
        {'Salva Nova Senha}
        gsNovaSenha := UpperCase(Trim(edtNovaSenha.Text));
        dlgAlterarSenha.Close;
    End;

end;

{'-------------------------------------------------------------------------------------------------
' TdlgAlterarSenha.FormShow
'     Limpa a tela
'-------------------------------------------------------------------------------------------------}
procedure TdlgAlterarSenha.FormShow(Sender: TObject);
begin
  { limpa os edit's}
  edtNovaSenha.Text := VAZIO;
  edtConfirmacao.Text := VAZIO;
end;

end.
