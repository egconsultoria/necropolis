unit uniUsuarioSenha;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, uniGlobal;

type
  TdlgUsuarioSenha = class(TForm)
    pnlUsuarioSenha: TPanel;
    edtUsuario: TEdit;
    edtSenha: TEdit;
    lblUsuario: TLabel;
    lblSenha: TLabel;
    btnOK: TButton;
    btnCancel: TButton;
    btnAlterarSenha: TButton;
    pnlLogo: TPanel;
    Image1: TImage;
    procedure FormShow(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure edtSenhaExit(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnAlterarSenhaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    miIndColSenha      : Integer;
    miIndColAtivo      : Integer;
    miIndColAdmin      : Integer;
    miIndColCodigo     : Integer;
    miConSenha         : Integer;

    function  mf_UsuarioVerifica : boolean;
    procedure mp_Senha_Atualizar;
    procedure mp_PreenchePerm;
    procedure mp_GravaLog;
    procedure mp_INI_Carregar;
    Function mf_INI_Ler(lsSecao: String; lsParamNome: String; lsDefault: String): String;

  public
    { Public declarations }

    function gf_Senha_Criptografar(lsSenha: String): String;
    function gf_Senha_Descriptografar(lsSenha: String): String;
    procedure gp_GravaLogSaida;
  end;

var
  dlgUsuarioSenha: TdlgUsuarioSenha;

implementation

uses uniMDI, uniAbout, uniDados, uniAlterarSenha, uniConst;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.FormShow                Mostra a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.FormShow(Sender: TObject);
begin
  { ampulheta}
  Screen.cursor := crHourGlass;
  { Obtem dados do arquivo .INI}
  mp_INI_Carregar;
  { apresentAcao do sistema}
  formAbout.Show;
  {Faz o dlg aparecer mais r�pido }
  formAbout.Refresh;
  { inicializa as variaveis}
  gsNomeUsuario := VAZIO;
  glCodigoUsuario := 0;
  giId_Processo := 0;
  gbAdmin       := False;
  giNumGrupo    := 0;

  {inicializa o banco}
  dmDados.gbFLAGIO_Iniciar := False;
  dmDados.IniciarBanco;

  { Default }
  Screen.Cursor := crDefault;
end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.btnOKClick                Confere a senha
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.btnOKClick(Sender: TObject);
begin
  { coloca ampulheta}
  Screen.cursor := crHourGlass;
  { se o usuario e a senha conferem}
  if mf_UsuarioVerifica then
  begin
    { fecha a tela inicial}
    formAbout.Close;
    { esconde a tela de usu�rio}
    dlgUsuarioSenha.Close;
    { mostra a tela Principal}
//    formMDI.show;
  end
  else
    { restaura cursor }
    Screen.cursor := crDefault;
end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.edtSenhaExit                Habilita alterar senha
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.edtSenhaExit(Sender: TObject);
begin
    { se nao estiver vazio}
    If Trim(edtSenha.text) <> VAZIO Then
        { ativa botao de alterar senha}
        btnAlterarSenha.Enabled := True
    Else
        btnAlterarSenha.Enabled := False;
end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.btnCancelClick                Encerra a aplicacao
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.btnCancelClick(Sender: TObject);
begin
  { fecha a tela inicial}
  formAbout.Close;
  { fecha a tela de usu�rio}
  dlgUsuarioSenha.Close;

  formMDI.Close;
end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.btnAlterarSenhaClick         Altera a Senha
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.btnAlterarSenhaClick(Sender: TObject);
begin
  { nao deixa sempre na frente}
  dlgUsuarioSenha.FormStyle := fsNormal;
  { chama a tela de alterar senha}
  dlgAlterarSenha.ShowModal;
  { joga o foco p/ botao OK}
  btnOk.SetFocus;
  { deixa sempre na frente}
  dlgUsuarioSenha.FormStyle := fsStayOnTop;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.FormClose                finaliza o Banco
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  { grava o logOut}
//  gp_GravaLogSaida;
  { fecha o banco }
//  dmDados.FinalizarBanco;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.UsuarioVerifica                Confere a senha e o usuario
 *
 *-----------------------------------------------------------------------------}
function TdlgUsuarioSenha.mf_UsuarioVerifica : Boolean;
var
  lsSql: string;

  lsSenhaCriptografada: String;
  lsSenha: String;
  lsAtivo: String;

begin

    { ' assume resultado da fun��o False}
    Result := False;

      gaParm[0] := UpperCase(Trim(edtUsuario.text));
      lsSql := dmDados.SqlVersao('LG_0004', gaParm);
      if dmDados.giStatus = STATUS_OK then
      begin
        {abre a conexao}
        miConSenha := dmDados.ExecutarSelect(lsSql);

        {se ocorreu erro na execucao do SQL}
        if miConSenha = IOPTR_NOPOINTER then
        begin
          {apresenta mensagem de erro na abertura da conexao}
          dmDados.gbFlagOnError := False;

          dlgUsuarioSenha.FormStyle := fsNormal;
          dmDados.MensagemExibir(VAZIO,100);
          dlgUsuarioSenha.FormStyle := fsStayOnTop;

          Result := False;
          {sai da funcao}
          Exit;
        end;

        miIndColSenha      := IOPTR_NOPOINTER;
        miIndColCodigo     := IOPTR_NOPOINTER;
        miIndColAtivo      := IOPTR_NOPOINTER;
        miIndColAdmin      := IOPTR_NOPOINTER;

        {se achou alguma coisa}
        if not dmDados.Status(miConSenha, IOSTATUS_NOROWS) then
        begin
          {guarda a senha}
          lsSenha  := dmDados.ValorColuna(miConSenha, 'Senha_Usuario', miIndColSenha);
          lsAtivo  := dmDados.ValorColuna(miConSenha, 'Id_Ativo', miIndColAtivo);
          {se ocorreu erro, sai}
          if dmDados.giStatus <> STATUS_OK then
          begin
            Result := False;
            Exit;
          end;
          { se usuario e ativo}
          if lsAtivo = 'S' then
          begin
//            lsSenhaCriptografada := gf_Senha_Criptografar(UpperCase(Trim(edtSenha.Text)));
            lsSenhaCriptografada := (UpperCase(Trim(edtSenha.Text)));
            lsSenha := gf_Senha_DesCriptografar(lsSenha);

            {se a senha digitada for diferente da senha do RA}
            if lsSenha <> lsSenhaCriptografada Then
            begin
              {mostra mensagem de senha errada}
              dmDados.gbFlagOnError := False;

              dlgUsuarioSenha.FormStyle := fsNormal;
              dmDados.MensagemExibir(VAZIO,52);
              dlgUsuarioSenha.FormStyle := fsStayOnTop;

            end
            else
            begin

              if dmDados.ValorColuna(miConSenha, 'Admin', miIndColAdmin) = 'S' then
                gbAdmin := True
              else
                gbAdmin := False;

              glCodigoUsuario := dmDados.ValorColuna(miConSenha, 'Codigo', miIndColCodigo);

              gsNomeUsuario := UpperCase(Trim(edtUsuario.text));

              {' se informou nova senha}
              If Trim(dlgAlterarSenha.gsNovaSenha) <> VAZIO Then
                  {'  Atualiza senha do usu�rio}
                  mp_Senha_Atualizar;

              { preenche as permissoes}
              mp_PreenchePerm;

              mp_GravaLog;

              { retorna OK}
              Result := True;
            end;
          end
          else
          begin
            { apresenta mensagem de nao ativo}
            dmDados.gbFlagOnError := False;

            dlgUsuarioSenha.FormStyle := fsNormal;
            dmDados.MensagemExibir(VAZIO,51);
            dlgUsuarioSenha.FormStyle := fsStayOnTop;

          end;
        end
        else
        begin
          {apresenta mensagem de senha errada}
          dmDados.gbFlagOnError := False;

          dlgUsuarioSenha.FormStyle := fsNormal;
          dmDados.MensagemExibir(VAZIO,52);
          dlgUsuarioSenha.FormStyle := fsStayOnTop;

         end;
        {fecha a conexao}
        dmDados.FecharConexao(miConSenha);
      end;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.gf_Senha_Criptografar          Converte o texto p/ binario
 *
 *-----------------------------------------------------------------------------}
function TdlgUsuarioSenha.gf_Senha_Criptografar(lsSenha: String): String;
var
  lsSenhaTraduzida: String;
  lsSenhaCriptografada: String;
  liPosCaracter: Integer;
  liPos: Integer;
begin

    lsSenhaCriptografada := VAZIO;
    lsSenhaTraduzida := UpperCase(Trim(lsSenha));
    For liPos := 1 To Length(lsSenhaTraduzida) do
    begin
        liPosCaracter := AnsiPos(Copy(lsSenhaTraduzida, liPos, 1),SENHA_CRIPTOGRAFAR);
        {' Se n�o encontrou o caracter}
        If liPosCaracter = 0 Then
        begin
            lsSenhaCriptografada := ERRO_SENHAINVALIDA;
            {' retorna senha criptografada = VAZIO}
            Result := lsSenhaCriptografada;
            Exit;
        End;
        { criptografa a senha}
        lsSenhaCriptografada := lsSenhaCriptografada + IntToHex(liPosCaracter,2);
    end;
    Result := lsSenhaCriptografada

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.gfSenha_Descriptografar          Converte o binario em texto
 *
 *-----------------------------------------------------------------------------}
function TdlgUsuarioSenha.gf_Senha_Descriptografar(lsSenha: String): String;
var
  lsSenhaDescriptografada: String;
  liPosCaracter: Integer;
  liPos: Integer;
  p: PChar;
begin

    lsSenhaDescriptografada := VAZIO;

    liPos := 1;
    While liPos <= Length(lsSenha) do
    begin
      liPosCaracter := StrToInt('$' + Copy(lsSenha, liPos, 2));
      lsSenhaDescriptografada := lsSenhaDescriptografada + Copy(SENHA_CRIPTOGRAFAR, liPosCaracter, 1);
      liPos := liPos + 2;
    end;
    Result := lsSenhaDescriptografada;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.mp_Senha_Atualizar         Atualiza no banco a senha nova
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.mp_Senha_Atualizar;
var
  lsSenhaCriptografada: String;
begin

  dmDados.gsRetorno := dmDados.AtivaModo(miConSenha,IOMODE_EDIT);

  lsSenhaCriptografada := gf_Senha_Criptografar(UpperCase(dlgAlterarSenha.gsNovaSenha));

  dmDados.gsRetorno := dmDados.AlterarColuna(miConSenha,'Senha_Usuario',miIndColSenha,lsSenhaCriptografada);

    if dmDados.gsRetorno = IORET_OK then
    begin
      dmDados.gsRetorno := dmDados.Alterar(miConSenha,'SEG_Usuario','Nome_Usuario = '''+UpperCase(Trim(edtUsuario.Text))+'''');

      {se ocorreu algum erro}
      if dmDados.gsRetorno = IORET_FAIL then
      begin
        {apresenta mensagem de erro na hora de executar o SQL}
        dmDados.gbFlagOnError := False;
        dmDados.MensagemExibir(VAZIO,101);
        {fecha a conexao}
        dmDados.FecharConexao(miConSenha);
      end;
    end;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.mp_PreenchePerm        Prenche as permissoes
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.mp_PreenchePerm;
var
  lvSelect: Variant;
  liConexUsuGru: Integer;
  liConexRotinas: Integer;
  liColNum_Grupo      : Integer;
  liColCod_SubRotina  : Integer;
  liColConsulta       : Integer;
  liColInclui         : Integer;
  liColExclui         : Integer;
  liColAltera         : Integer;
  liColExecuta        : Integer;
  liColEmite          : Integer;
begin

    liConexUsuGru  := IOPTR_NOPOINTER;
    liConexRotinas := IOPTR_NOPOINTER;

    gaParm[0] := UpperCase(Trim(edtUsuario.text));
    lvSelect := dmDados.SqlVersao('SE_0007', gaParm);
    liConexUsuGru := dmDados.ExecutarSelect(lvSelect);

    liColNum_Grupo := IOPTR_NOPOINTER;

    while not dmDados.Status(liConexUsuGru, IOSTATUS_EOF) do
    begin

      giNumSubRotinas := 0;

      liColCod_SubRotina  := IOPTR_NOPOINTER;
      liColConsulta       := IOPTR_NOPOINTER;
      liColInclui         := IOPTR_NOPOINTER;
      liColExclui         := IOPTR_NOPOINTER;
      liColAltera         := IOPTR_NOPOINTER;
      liColExecuta        := IOPTR_NOPOINTER;
      liColEmite          := IOPTR_NOPOINTER;      

      giNumGrupo := dmDados.ValorColuna(liConexUsuGru, 'Num_Grupo', liColNum_Grupo);
      gaParm[0] := IntToStr(giNumGrupo);
      lvSelect := dmDados.SqlVersao('SE_0009', gaParm);
      liConexRotinas := dmDados.ExecutarSelect(lvSelect);

      while not dmDados.Status(liConexRotinas, IOSTATUS_EOF) do
      begin

        giNumSubRotinas := giNumSubRotinas + 1;

        if gaPermRotinas[giNumSubRotinas].Cod_SubRotina = VAZIO then
          gaPermRotinas[giNumSubRotinas].Cod_SubRotina := dmDados.ValorColuna(liConexRotinas, 'Cod_SubRotina', liColCod_SubRotina);
        if not gaPermRotinas[giNumSubRotinas].Consulta then
          gaPermRotinas[giNumSubRotinas].Consulta      := dmDados.ValorColuna(liConexRotinas, 'Consulta', liColConsulta);
        if not gaPermRotinas[giNumSubRotinas].Inclui then
          gaPermRotinas[giNumSubRotinas].Inclui        := dmDados.ValorColuna(liConexRotinas, 'Inclui', liColInclui);
        if not gaPermRotinas[giNumSubRotinas].Exclui then
          gaPermRotinas[giNumSubRotinas].Exclui        := dmDados.ValorColuna(liConexRotinas, 'Exclui', liColExclui);
        if not gaPermRotinas[giNumSubRotinas].Altera then
          gaPermRotinas[giNumSubRotinas].Altera        := dmDados.ValorColuna(liConexRotinas, 'Altera', liColAltera);
        if not gaPermRotinas[giNumSubRotinas].Executa then
          gaPermRotinas[giNumSubRotinas].Executa       := dmDados.ValorColuna(liConexRotinas, 'Executa', liColExecuta);
        if not gaPermRotinas[giNumSubRotinas].Emite then
          gaPermRotinas[giNumSubRotinas].Emite         := dmDados.ValorColuna(liConexRotinas, 'Emite', liColEmite);

          { se poder fazer qualquer coisa em relacao a cadastrar}
          if (gaPermRotinas[giNumSubRotinas].Consulta = True) or
             (gaPermRotinas[giNumSubRotinas].Inclui = True) or
             (gaPermRotinas[giNumSubRotinas].Exclui = True) or
             (gaPermRotinas[giNumSubRotinas].Altera = True) then
            gbPodeCadastrar := True;

          if (gaPermRotinas[giNumSubRotinas].Executa = True) then
            gbPodeExecutar := True;

          if (gaPermRotinas[giNumSubRotinas].Emite = True) then
            gbPodeEmitir := True;

        if (dmDados.Proximo(liConexRotinas) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
      end;

      if (dmDados.Proximo(liConexUsuGru) = IORET_EOF) or
         (dmDados.giStatus <> STATUS_OK) then
          break;
    end;
    {fecha a conexao}
    dmDados.FecharConexao(liConexRotinas);
    dmDados.FecharConexao(liConexUsuGru);

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.mp_GravaLog        Grava o log
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.mp_GravaLog;
var
  lvSelect: Variant;
  liConexLog: Integer;
  liIndColNome_Usuario    :Integer;
  liIndColId_Processo     :Integer;
  liIndColStatus_Processo :Integer;
  liIndColDtHora_Login    :Integer;
begin

    liIndColNome_Usuario    := IOPTR_NOPOINTER;
    liIndColId_Processo     := IOPTR_NOPOINTER;
    liIndColStatus_Processo := IOPTR_NOPOINTER;
    liIndColDtHora_Login    := IOPTR_NOPOINTER;

    giId_Processo := dmDados.SequenciaProxVal('SE_IdProcesso');

    gaParm[0] := gsNomeUsuario;
    gaParm[1] := IntToStr(giId_Processo);
    gaParm[2] := ATIVO;             {' Status do processo "Ativo"}
    lvSelect := dmDados.SqlVersao('LG_0025', gaParm);

    liConexLog := dmDados.ExecutarSelect(lvSelect);

    {' Se registro n�o existe}
    If dmDados.Status(liConexLog, IOSTATUS_EOF) Then
    begin
      dmDados.gsRetorno := dmDados.AtivaModo(liConexLog,IOMODE_INSERT);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexLog,'Nome_Usuario',liIndColNome_Usuario,gsNomeUsuario);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexLog,'Id_Processo',liIndColId_Processo,giId_Processo);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexLog,'Status_Processo',liIndColStatus_Processo,ATIVO);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexLog,'DtHora_Login',liIndColDtHora_Login,Now);
      dmDados.gsRetorno := dmDados.Incluir(liConexLog,'SEG_TabLogin');
    End;
    {fecha a conexao}
    dmDados.FecharConexao(liConexLog);
end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.gp_PreenchePerm        Prenche as permissoes
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.gp_GravaLogSaida;
var
  lvSelect: Variant;
  liConexLog: Integer;
  ldDataHora: TDateTime;
  liIndColNome_Usuario    :Integer;
  liIndColId_Processo     :Integer;
  liIndColStatus_Processo :Integer;
  liIndColDtHora_Login    :Integer;
  liIndColDtHora_LogOut   :Integer;
  lsSql: String;
begin

    liIndColNome_Usuario    := IOPTR_NOPOINTER;
    liIndColId_Processo     := IOPTR_NOPOINTER;
    liIndColStatus_Processo := IOPTR_NOPOINTER;
    liIndColDtHora_Login    := IOPTR_NOPOINTER;
    liIndColDtHora_LogOut   := IOPTR_NOPOINTER;

    gaParm[0] := Trim(gsNomeUsuario);
    gaParm[1] := IntToStr(giId_Processo);
    gaParm[2] := ATIVO;                       {' Status do processo "Ativo"}
    lvSelect := dmDados.SqlVersao('LG_0025', gaParm);

    liConexLog := dmDados.ExecutarSelect(lvSelect);

    {' Se existe o registro}
    If not dmDados.Status(liConexLog, IOSTATUS_EOF) Then
    begin

      ldDataHora := dmDados.ValorColuna(liConexLog, 'DtHora_Login', liIndColDtHora_Login);
      {fecha a conexao}
      dmDados.FecharConexao(liConexLog);
      lsSql := 'DELETE FROM SEG_TabLogin WHERE Nome_Usuario = ''' + Trim(gsNomeUsuario)+ '''';
      lsSql := lsSql + ' AND Status_Processo = ''' + ATIVO + ''' AND Id_Processo = ' + IntToStr(giId_Processo);
      dmDados.ExecutarSql(lsSql);

      gaParm[0] := Trim(gsNomeUsuario);
      gaParm[1] := IntToStr(giId_Processo);
      gaParm[2] := ENCERRADO;                       {' Status do processo "ENCERRADO"}
      lvSelect := dmDados.SqlVersao('LG_0025', gaParm);

      liConexLog := dmDados.ExecutarSelect(lvSelect);

      {' Se existe o registro}
      If dmDados.Status(liConexLog, IOSTATUS_EOF) Then
        dmDados.gsRetorno := dmDados.AtivaModo(liConexLog,IOMODE_INSERT)
      else
        dmDados.gsRetorno := dmDados.AtivaModo(liConexLog,IOMODE_EDIT);

      liIndColDtHora_Login    := IOPTR_NOPOINTER;

      dmDados.gsRetorno := dmDados.AlterarColuna(liConexLog,'Nome_Usuario',liIndColNome_Usuario,gsNomeUsuario);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexLog,'Id_Processo',liIndColId_Processo,giId_Processo);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexLog,'Status_Processo',liIndColStatus_Processo,ENCERRADO);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexLog,'DtHora_Login',liIndColDtHora_Login,ldDataHora);
      dmDados.gsRetorno := dmDados.AlterarColuna(liConexLog,'DtHora_Logout',liIndColDtHora_Logout,Now);

      dmDados.gsRetorno := dmDados.Alterar(liConexLog,'SEG_TabLogin', 'Nome_Usuario = ''' + Trim(gsNomeUsuario)+ '''' +
                      ' AND Status_Processo = ''' + ENCERRADO + ''' AND Id_Processo = ' + IntToStr(giId_Processo));

    End;
    {fecha a conexao}
    dmDados.FecharConexao(liConexLog);

End;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.mp_INI_Carregar        pega os parametros no arquivo ini
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuarioSenha.mp_INI_Carregar;
begin

  try
    dmDados.giStatus := STATUS_OK;

    gsDBDir := mf_INI_Ler('Banco de Dados', 'Diretorio', DB_PATH);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsDBNome := mf_INI_Ler('Banco de Dados', 'Nome', DATABASE_NAME);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsDbTipo := mf_INI_Ler('Banco de Dados', 'Tipo', DB_TIPO_ACCESS);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsDbUserName := mf_INI_Ler('Banco de Dados', 'UserName', USER_NAME);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsDbPassWord := mf_INI_Ler('Banco de Dados', 'PassWord', PASSWORD);
    { se tiver Senha, Descriptografa}
//    if gsDbPassWord <> VAZIO then
//      gsDbPassWord := gf_Senha_Descriptografar(gsDbPassWord);

    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsDbDriver := mf_INI_Ler('Banco de Dados', 'Driver', NOME_DRIVER);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsDbConnect := mf_INI_Ler('Banco de Dados', 'Connect', DB_CONNECT);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    If (Length(gsDBNome) > 0) And (gsDbTipo = DB_TIPO_ACCESS) Then
      gsDBNome := gsDBDir + gsDBNome;

    gsMDICaption := mf_INI_Ler('Sistema', 'Titulo', MDI_CAPTION);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsMDISubCaption := mf_INI_Ler('Sistema', 'SubTitulo', MDI_SUB_CAPTION);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsLblVersao := mf_INI_Ler('Sistema', 'Versao', LBL_VERSAO);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsLblEmpresa := mf_INI_Ler('Sistema', 'Empresa', LBL_EMPRESA);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

    gsLblSistema := mf_INI_Ler('Sistema', 'Nome', NOME_SISTEMA);
    {'se ocorreu erro na rotina/evento}
    If dmDados.giStatus <> STATUS_OK Then
      dmDados.FinalizarBanco;

  except
    dmDados.ErroTratar('mp_INI_Carregar - uniUsuarioSenha.PAS');
  end;

End;

{*-----------------------------------------------------------------------------
 *  TdlgUsuarioSenha.mf_INI_Ler        Le o parametro passado no arquivo ini
 *
 *-----------------------------------------------------------------------------}
Function TdlgUsuarioSenha.mf_INI_Ler(lsSecao: String; lsParamNome: String; lsDefault: String): String;
var
  liTamanho: Integer;
  lpcParamValor: PChar;
  lpcSecao: PChar;
  lpcParamNome: PChar;
  lpcDefault: PChar;
  liConta: LongInt;
  lsParamValor: String;
begin

  try
    dmDados.giStatus := STATUS_OK;
    lpcSecao := PChar(lsSecao);
    lpcParamNome := PChar(lsParamNome);
    lpcDefault := PChar(lsDefault);

    for liConta := 1 to 2048 do
      lsParamValor := lsParamValor + ' ';

    lpcParamValor := PChar(lsParamValor);

    liTamanho := GetPrivateProfileString(lpcSecao, lpcParamNome, lpcDefault, lpcParamValor, Length(lsParamValor), INI_NOME);
    Result := Trim(Copy(lpcParamValor, 1, liTamanho));

  except
    dmDados.ErroTratar('mf_INI_LER - uniUsuarioSenha.PAS');
  end;

End;

end.
