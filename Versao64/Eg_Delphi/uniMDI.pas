unit uniMDI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ToolWin, ComCtrls, Buttons, ExtCtrls, StdCtrls, OleCtrls, ImgList;

type
  TformMDI = class(TForm)
    MenuPrincipal: TMainMenu;
    menuCadastro: TMenuItem;
    menuEditar: TMenuItem;
    menuExibir: TMenuItem;
    menuFerramentas: TMenuItem;
    menuJanelas: TMenuItem;
    menuAjuda: TMenuItem;
    itemNovo: TMenuItem;
    itemAbrir: TMenuItem;
    itemSalvar: TMenuItem;
    itemExcluir: TMenuItem;
    itemLinha2: TMenuItem;
    itemImprimir: TMenuItem;
    itemLinha3: TMenuItem;
    itemFim: TMenuItem;
    itemVoltar: TMenuItem;
    itemVoltarDig: TMenuItem;
    itemLinha4: TMenuItem;
    itemRecortar: TMenuItem;
    itemCopiar: TMenuItem;
    itemColar: TMenuItem;
    itemPrimeiro: TMenuItem;
    itemAnterior: TMenuItem;
    itemProximo: TMenuItem;
    itemUltimo: TMenuItem;
    itemLinha6: TMenuItem;
    itemLadoHorizontal: TMenuItem;
    itemLadoVertical: TMenuItem;
    itemCascata: TMenuItem;
    itemLinha7: TMenuItem;
    itemFecharTodas: TMenuItem;
    itemIndice: TMenuItem;
    itemLocalizar: TMenuItem;
    itemLinha8: TMenuItem;
    itemSobre: TMenuItem;
    stbMDI: TStatusBar;
    itemRotinas: TMenuItem;
    itemLinha10: TMenuItem;
    cobMenu: TCoolBar;
    tobMenu: TToolBar;
    btnNovo: TToolButton;
    btnAbrir: TToolButton;
    btnSalvar: TToolButton;
    btnImprimir: TToolButton;
    NavigatorImages: TImageList;
    NavigatorHotImages: TImageList;
    btnProcurar: TToolButton;
    btnPrimeiro: TToolButton;
    btnAnterior: TToolButton;
    btnProximo: TToolButton;
    btnUltimo: TToolButton;
    pnlFundo: TPanel;
    pnlCadastro: TPanel;
    btnDivisao1: TToolButton;
    btnDivisao2: TToolButton;
    btnConsistir: TToolButton;
    btnDivisao3: TToolButton;
    itemConfigurarImpressora: TMenuItem;
    dlgConfigurarImpressora: TPrinterSetupDialog;
    itemConsistir: TMenuItem;
    itemProcurar: TMenuItem;
    itemSeguranca: TMenuItem;
    subitemUsuario: TMenuItem;
    subitemGrupos: TMenuItem;
    N1: TMenuItem;
    btnRotinas: TToolButton;
    N2: TMenuItem;
    itemFechar: TMenuItem;
    imgFundo1: TImage;
    imgFundo2: TImage;
    imgFundo3: TImage;
    Image1: TImage;
    procedure itemFimClick(Sender: TObject);
    procedure itemSobreClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure subitemGruposClick(Sender: TObject);
    procedure subitemUsuarioClick(Sender: TObject);
    procedure itemAbrirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure menuCadastroClick(Sender: TObject);
    procedure itemExcluirClick(Sender: TObject);
    procedure itemNovoClick(Sender: TObject);
    procedure itemSalvarClick(Sender: TObject);
    procedure itemColarClick(Sender: TObject);
    procedure itemCopiarClick(Sender: TObject);
    procedure itemProcurarClick(Sender: TObject);
    procedure itemRecortarClick(Sender: TObject);
    procedure menuEditarClick(Sender: TObject);
    procedure itemVoltarDigClick(Sender: TObject);
    procedure itemVoltarClick(Sender: TObject);
    procedure itemAnteriorClick(Sender: TObject);
    procedure itemProximoClick(Sender: TObject);
    procedure itemPrimeiroClick(Sender: TObject);
    procedure itemUltimoClick(Sender: TObject);
    procedure menuExibirClick(Sender: TObject);
    procedure itemConsistirClick(Sender: TObject);
    procedure itemCascataClick(Sender: TObject);
    procedure itemLadoHorizontalClick(Sender: TObject);
    procedure itemLadoVerticalClick(Sender: TObject);
    procedure itemFecharTodasClick(Sender: TObject);
    procedure menuJanelasClick(Sender: TObject);
    procedure menuFerramentasClick(Sender: TObject);
    procedure itemImprimirClick(Sender: TObject);
    procedure btnNovoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormPaint(Sender: TObject);
    procedure itemConfigurarImpressoraClick(Sender: TObject);
    procedure itemRotinasClick(Sender: TObject);
    procedure itemFecharClick(Sender: TObject);
  private
    { Private declarations }
    ma_StatusMensa: array[0..11] of String;    {Vetor p/ mensagens do statusbar}
    mbDentro:       Boolean;
    { p/ as conexoes genericas}
    maConexao: array[0..10] of Integer;
    miIndCol: array[0..25] of Integer;

    procedure mp_MDI_VisualPreparar;
    Procedure mp_MDI_FecharTodas;

  public
    { Public declarations }
    procedure gp_SnapShot_SisGru_Montar(var liConexao: Integer);
    procedure gp_Dynaset_UsuGrup_Montar(var liConexao: Integer);
    procedure gp_AtualizaBarraBotoes;
    procedure gp_AtualizaBarraStatus;
  end;

var
  formMDI: TformMDI;
  msSql: String;


implementation

uses uniAbout, uniUsuarioSenha, uniDados, uniGrupo, uniUsuario, uniCadSeleciona,
    uniConst, uniLock, uniCombos, uniSeleciona, uniSelPar, uniTelaVar, uniParam,
    uniEdit, uniDigito, uniErros, uniImprimir, uniGlobal, uniRotinas, uniProcessos{,
    uniGeraArquivo};

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TformChannelSystemMDI.itemFimClick                encerra o sistema
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemFimClick(Sender: TObject);
begin
  { encerra o sistema}
  formMDI.Close;
end;

{*-----------------------------------------------------------------------------
 *  TformChannelSystemMDI.itemSobreClick               apresenta a tela do About
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemSobreClick(Sender: TObject);
begin
  { apresenta a tela do About}
  formAbout.ShowModal;
end;

{*-----------------------------------------------------------------------------
 *  TformChannelSystemMDI.FormClose               Fecha a tela de usu�rio
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  { grava o logOut}
  dlgUsuarioSenha.gp_GravaLogSaida;
  { fecha o banco }
  dmDados.FinalizarBanco;

end;

{*-----------------------------------------------------------------------------
 *  TformChannelSystemMDI.subitemGruposClick       chama a tela de grupos
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.subitemGruposClick(Sender: TObject);
begin
  {'ampulheta}
  formMDI.Cursor := crHourglass;
  { chama a tela de grupos}
  dlgGrupo.ShowModal;
  {'Default}
  formMDI.Cursor := crDefault;
 
end;

{*-----------------------------------------------------------------------------
 *  TformChannelSystemMDI.subitemUsuarioClick       chama a tela de usuarios
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.subitemUsuarioClick(Sender: TObject);
begin
  {'ampulheta}
  formMDI.Cursor := crHourglass;
  { chama a tela de grupos}
  dlgUsuario.ShowModal;
  {'Default}
  formMDI.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TformChannelSystemMDI.gp_SnapShot_SisGru_Montar       Preenche SnapShot de Grupo
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.gp_SnapShot_SisGru_Montar(var liConexao: Integer);
var
  lvSelect: Variant;

begin

    {' Monta SnapShot contendo todos os Sistemas}
    lvSelect := dmDados.SqlVersao('SE_0003', gaParm);

    {'Recria snapshot}
    dmDados.FecharConexao(liConexao);
    liConexao := dmDados.ExecutarSelect(lvSelect);

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.mp_Dynaset_UsuGrup_Montar       Preenche SnapShot de Grupo
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.gp_Dynaset_UsuGrup_Montar(var liConexao: Integer);
var
  lvSelect: Variant;

begin

    lvSelect := dmDados.SqlVersao('SE_0006', gaParm);

    {'Recria snapshot}
    dmDados.FecharConexao(liConexao);

    liConexao := dmDados.ExecutarSelect(lvSelect);

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemAbrirClick       Apresenta dlg de cadastro
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemAbrirClick(Sender: TObject);
begin

  Screen.Cursor := crHOURGLASS;

  dlgCadSelecionar.ShowModal;

  If grTelaVar.DynaID > 0 Then
  begin
    Screen.Cursor := crHOURGLASS;
//#######################
    { para abrir a conexao no ressincronizar do Edit }
    grTelaVar.DynaID := 0;
//#######################
    If grTelaVar.P.bTela_Especial Then
      untEdit.gp_MDI_TelaVar_TelaEspecial;        
  End;

  Screen.Cursor := crDEFAULT;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.FormShow(Sender: TObject);

var
  lsAtivas: String;
  lsBloqueadas: String;

begin

  dlgUsuarioSenha.ShowModal;
  formMDI.Refresh;
  formMDI.Caption := gsLblSistema(*NOME_SISTEMA*) + ' - ' + gsLblEmpresa(*LBL_EMPRESA*);

  {'ampulheta}
  Screen.Cursor := crHOURGLASS;

  gsRtn_Atual := ROT_CONSULTA;

  {preenche vetor com mensagens p/ statusbar}
  ma_StatusMensa[0] := VAZIO;
  ma_StatusMensa[1] := ' Abre janela para cadastrar novo par�metro';
  ma_StatusMensa[2] := ' Abre novo Cadastro';
  ma_StatusMensa[3] := ' Grava dados da janela ativa';
  ma_StatusMensa[4] := ' Abre janela para imprimir relat�rios';
  ma_StatusMensa[5] := ' Procura por outro c�digo no cadastro ativo';
  ma_StatusMensa[6] := ' Mostra dados do primeiro c�digo';
  ma_StatusMensa[7] := ' Mostra dados do c�digo anterior';
  ma_StatusMensa[8] := ' Mostra dados do pr�ximo c�digo';
  ma_StatusMensa[9] := ' Mostra dados do �ltimo c�digo';
  ma_StatusMensa[10] := ' Faz as Consist�ncias da Janela Ativa';
  ma_StatusMensa[11] := ' Efetua as Rotinas de Atualiza��es dispon�veis';

  mbDentro := False;

  { cria as units}
  untCombos.gp_Combo_Inicializar;
  untEdit.gp_Edit_Inicializar;
  untDigito.gp_Digito_Inicializar;
  untLock.gp_Lock_Inicializar;
  untSeleciona.gp_Selec_Inicializar;
  untTelaVar.gp_TelaVar_Inicializar;
  untParam.gp_Param_Inicializar;
  untErros.gp_Erros_Inicializar;
  untSelPar.gp_SelPar_Inicializar;

  {' Mostra a tela}
  mp_MDI_VisualPreparar;


  //  Propostas Ativas
      gaParm[0] := 'VPROPOSTAS_ATIVAS';
      gaParm[1] := 'Ativas';
      msSql := dmDados.SqlVersao('NEC_0001', gaParm);
      maConexao[0] := dmDados.ExecutarSelect(msSql);
      if maConexao[0] = IOPTR_NOPOINTER then
        Exit;
      lsAtivas := dmDados.ValorColuna(maConexao[0], 'Ativas', miIndCol[0]);

  //  Propostas Ativas
      gaParm[0] := 'VPROPOSTAS_BLOQUEADAS';
      gaParm[1] := 'BLOQUEADAS';
      msSql := dmDados.SqlVersao('NEC_0001', gaParm);
      maConexao[1] := dmDados.ExecutarSelect(msSql);
      if maConexao[1] = IOPTR_NOPOINTER then
        Exit;
      lsBloqueadas := dmDados.ValorColuna(maConexao[1], 'Bloqueadas', miIndCol[1]);


  { mostra nome do usuario na barra de status }
  stbMDI.Panels[1].Text := gsNomeUsuario;
  stbMDI.Panels[2].Text := 'Propostas Ativas' + ' ' + '(' + lsAtivas + ')' + '  ' + 'Propostas Bloqueadas' + ' ' + '(' + lsBloqueadas + ')';


  Screen.Cursor := crDEFAULT;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.mp_MDI_VisualPreparar       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.mp_MDI_VisualPreparar;
begin

    {'Seta Estado dos Botoes da Barra}
    btnNovo.Enabled         := False;   {'desabilitado}
    btnAbrir.Enabled        := gbPodeCadastrar;    {'de acordo com o grupo}
    itemAbrir.Enabled       := gbPodeCadastrar;    {'de acordo com o grupo}
    btnSalvar.Enabled       := False;   {'desabilitado}
    btnImprimir.Enabled     := gbPodeEmitir;    {'de acordo com o grupo}
    itemImprimir.Enabled    := gbPodeEmitir;    {'de acordo com o grupo}
    btnConsistir.Enabled    := False;
    itemConsistir.Enabled   := False;
    btnProcurar.Enabled     := False;   {'desabilitado}
    btnPrimeiro.Enabled     := False;   {'desabilitado}
    btnAnterior.Enabled     := False;   {'desabilitado}
    btnProximo.Enabled      := False;   {'desabilitado}
    btnUltimo.Enabled       := False;   {'desabilitado}
    itemRotinas.Enabled     := gbPodeExecutar;   {'de acordo com o grupo}
    btnRotinas.Enabled      := gbPodeExecutar;   {'de acordo com o grupo}
    itemSeguranca.Visible   := gbAdmin;       { de acordo com o tipo de ususario}

    pnlCadastro.Caption := VAZIO;

End;

{*-----------------------------------------------------------------------------
 *  TformMDI.menuCadastroClick       prepara os itens do menu
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.menuCadastroClick(Sender: TObject);
begin

  If formMDI.ActiveMDIChild = nil Then
  begin
    itemExcluir.Enabled := False;
    itemSalvar.Enabled := False;
    itemNovo.Enabled := False;
    itemNovo.Caption := '&Novo(a)...';
    itemFechar.Enabled := False;
  end
  Else
  begin
    itemSalvar.Enabled := grParam_TelaVar.Alterado;
    itemExcluir.Enabled := (grParam_TelaVar.Comando_MDI = ED_ALTERAR) And
                           (Not grParam_TelaVar.Bloqueado) And
                           (untSelPar.gf_SelPar_Pode_Atualizar(grTelaVar.P.Cod_Seguranca, TIPO_EXCLUI));
    itemNovo.Enabled := (grParam_TelaVar.Comando_MDI = ED_ALTERAR) And
                        (untSelPar.gf_SelPar_Pode_Atualizar(grTelaVar.P.Cod_Seguranca, TIPO_INCLUI));
    itemNovo.Caption := '&Novo(a) ' + grParam_TelaVar.P.Entidade;
    itemFechar.Enabled := True;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemExcluirClick       Exclui
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemExcluirClick(Sender: TObject);
begin

  Screen.Cursor := crHOURGLASS;
  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_EXCLUIR, VAZIO);
  Screen.Cursor := crDEFAULT;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemNovoClick       novo registro
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemNovoClick(Sender: TObject);
begin

  Screen.Cursor := crHOURGLASS;

  If (gfrmTV <> nil) Then
    untTelaVar.gp_TelaVar_Executar(TV_COMANDO_INCLUIR, VAZIO);

  Screen.Cursor := crDEFAULT;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemSalvarClick       salvar
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemSalvarClick(Sender: TObject);
begin

  Screen.Cursor := crHOURGLASS;

  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_GRAVAR, VAZIO);

  Screen.Cursor := crDEFAULT;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemColarClick       Colar
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemColarClick(Sender: TObject);
var
  i: Integer;           {' Vari�vel tempor�ria de uso geral}

begin

  {'se h� texto no clipboard}
(*  If (Clipboard.GetFormat(CF_TEXT)) Then
  begin
    {'copia texto do clipboard para controle selecionado}
    i := Screen.ActiveControl.MaxLength - Len(Screen.ActiveControl.Text) + Len(Screen.ActiveControl.SelText)
    If i > 0 Then
    begin
      Screen.ActiveControl.SelText = Left$(Clipboard.GetText(CF_TEXT), i)
      Screen.ActiveControl.Text = Left$(Screen.ActiveControl.Text, Screen.ActiveControl.MaxLength)
    end
    Else
      If Screen.ActiveControl.MaxLength = 0 Then
        Screen.ActiveControl.SelText = Clipboard.GetText(CF_TEXT);
  End;
  *)
end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemCopiarClick       copiar
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemCopiarClick(Sender: TObject);
begin

  {'se h� texto selecionado}
(*  If (Screen.ActiveControl.SelText <> VAZIO) Then
    {'copia texto selecionado para o clipboard}
    Clipboard.SetText Screen.ActiveControl.SelText;
*)
end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemProcurarClick       procura
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemProcurarClick(Sender: TObject);
begin
  { ampulheta}
  Screen.Cursor := crHOURGLASS;

  { chama o metodo}
  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_PROCURAR, VAZIO);

  { volta ao normal}
  Screen.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemRecortarClick       recortar
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemRecortarClick(Sender: TObject);
begin

  {'se h� texto selecionado}
(*  If (Screen.ActiveControl.SelLength > 0) Then
  begin
    {'copia texto selecionado no clipboard}
    Clipboard.SetText Screen.ActiveControl.SelText
    {'deleta texto selecionado}
    Screen.ActiveControl.SelText = VAZIO
  End;
*)
end;

{*-----------------------------------------------------------------------------
 *  TformMDI.menuEditarClick       prepara menu
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.menuEditarClick(Sender: TObject);
var
  lbSelecao: Boolean;
  lbPermiteEdicao: Boolean;

begin

  lbPermiteEdicao := False;

  If (formMDI.ActiveMDIChild <> nil) Then
  begin
    If formMDI.ActiveMDIChild.ActiveControl is TEdit Then
      lbPermiteEdicao := (grParam_TelaVar.Comando_MDI <> ED_CONSULTA);

    itemProcurar.Enabled := True;
  end
  Else
    itemProcurar.Enabled := False;

  If lbPermiteEdicao Then
  begin
    lbSelecao := (formMDI.ActiveMDIChild.ActiveControl.GetTextLen <> 0);
    itemCopiar.Enabled := lbSelecao;
    itemRecortar.Enabled := lbSelecao;
//    itemColar.Enabled := Clipboard.GetFormat(CF_TEXT) And ActiveForm.ActiveControl.Enabled
    itemVoltarDig.Enabled := grParam_TelaVar.Alterado;
  end
  Else
  begin
    itemCopiar.Enabled := False;
    itemRecortar.Enabled := False;
    itemColar.Enabled := False;
    itemVoltarDig.Enabled := False;
  End;

  If grParam_TelaVar.DynaID > 0 Then
    itemVoltar.Enabled := ((grParam_TelaVar.Comando_MDI <> ED_CONSULTA) And grParam_TelaVar.Alterado)
  Else
    itemVoltar.Enabled := False;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemVoltarDigClick       Volta digitacao
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemVoltarDigClick(Sender: TObject);
var
  lpcDestino: PChar;
begin
  lpcDestino := #0;
  StrPCopy(lpcDestino,Trim(gsAreaDesfaz));
  Screen.ActiveControl.SetTextBuf(lpcDestino);

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemVoltarClick       Volta situacao anterior
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemVoltarClick(Sender: TObject);
begin
  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_RESSETAR, VAZIO);
end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemAnteriorClick       registro anterior
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemAnteriorClick(Sender: TObject);
begin

  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_IR_ANTERIOR, VAZIO);

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemProximoClick       proximo registro
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemProximoClick(Sender: TObject);
begin

  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_IR_PROXIMO, VAZIO);

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemPrimeiroClick       primeiro registro
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemPrimeiroClick(Sender: TObject);
begin

  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_IR_PRIMEIRO, VAZIO);

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemUltimoClick       ultimo registro
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemUltimoClick(Sender: TObject);
begin

  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_IR_ULTIMO, VAZIO);

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.menuExibirClick       prepara o menu
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.menuExibirClick(Sender: TObject);
begin

  If gfrmTV = nil Then
  begin
    itemConsistir.Enabled := False;
    itemPrimeiro.Enabled := False;
    itemAnterior.Enabled := False;
    itemProximo.Enabled := False;
    itemUltimo.Enabled := False;
  end
  Else
  begin
    itemConsistir.Enabled := (Not grParam_TelaVar.Bloqueado);
    itemPrimeiro.Enabled := grParam_TelaVar.Nav.bPrimeiro;
    itemAnterior.Enabled := grParam_TelaVar.Nav.bAnterior;
    itemProximo.Enabled := grParam_TelaVar.Nav.bProximo;
    itemUltimo.Enabled := grParam_TelaVar.Nav.bUltimo;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemConsistirClick       Consiste a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemConsistirClick(Sender: TObject);
begin

  Screen.Cursor := crHOURGLASS;

  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_CONSISTIR, VAZIO);

  Screen.Cursor := crDEFAULT;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemCascataClick       cascata
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemCascataClick(Sender: TObject);
begin

  formMDI.Cascade;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemLadoHorizontalClick       tile horizontal
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemLadoHorizontalClick(Sender: TObject);
begin

  formMDI.TileMode := tbHorizontal;
  formMDI.Tile;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemLadoVerticalClick       tile vertical
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemLadoVerticalClick(Sender: TObject);
begin

  formMDI.TileMode := tbVertical;
  formMDI.Tile;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemFecharTodasClick       fecha todas as janelas
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemFecharTodasClick(Sender: TObject);
begin

  mp_MDI_FecharTodas;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.mp_MDI_FecharTodas       fecha todas as janelas
 *
 *-----------------------------------------------------------------------------}
Procedure TformMDI.mp_MDI_FecharTodas;
var
  i: Integer;
  lsCaption: String;

begin

  for i := 0 to formMDI.MDIChildCount - 1 do
  begin
    if formMDI.Focused then
      Exit
    else
    begin
      if not (formMDI.MDIChildren[i] = nil) Then
      begin
        lsCaption := formMDI.MDIChildren[i].Caption;
        formMDI.MDIChildren[i].Close;
        {' N�o est� sendo disparado}
//        If (i < formMDI.MDIChildCount) Then
//          If lsCaption = formMDI.MDIChildren[i].Caption Then
            {' Ainda � o mesmo form ent�o cancelar}
//            Exit;
      end;
    end;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TformMDI.menuJanelasClick       Prepara o menu
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.menuJanelasClick(Sender: TObject);
begin

  If formMDI.ActiveMDIChild = nil Then
  begin
    itemCascata.Enabled := False;
    itemLadoHorizontal.Enabled := False;
    itemLadoVertical.Enabled := False;
    itemFecharTodas.Enabled := False;
  end
  Else
  begin
    itemCascata.Enabled := True;
    itemLadoHorizontal.Enabled := True;
    itemLadoVertical.Enabled := True;
    itemFecharTodas.Enabled := True;
  End;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.menuFerramentasClick       prepara o menu
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.menuFerramentasClick(Sender: TObject);
begin

  If (formMDI.ActiveMDIChild = nil) And gbAdmin Then
    itemSeguranca.Enabled := True
  Else
    itemSeguranca.Enabled := False;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemImprimirClick       abre janela p/ imprimir
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemImprimirClick(Sender: TObject);
begin
  { se tem tela filha}
  If Not (gfrmTV = Nil) Then
    { pega o codigo}
    gsRtn_Atual := grParam_TelaVar.p.Cod_Seguranca
  Else
    { codigo generico}
    gsRtn_Atual := ROT_RELATORIO;

  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_IMPRIMIR, VAZIO);

  { apresenta a tela de impressao}
  formImprimir.ShowModal;

  { Volta ao normal}
  gsRtn_Atual := ROT_CONSULTA;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.btnNovoMouseMove       preenche o statusbar
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.btnNovoMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  lbtnBotao: TToolButton;
begin

  lbtnBotao := TToolButton(Sender);
  if (((X > 5) and (Y > 5)) and
      ((X < (lbtnBotao.Width-5)) and (Y < (lbtnBotao.Height-5)))) then
    formMDI.stbMDI.Panels[0].Text := ma_StatusMensa[lbtnBotao.Tag]
  else
    formMDI.stbMDI.Panels[0].Text := VAZIO;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.gp_AtualizaBarraBotoes       prepara os botoes da barra
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.gp_AtualizaBarraBotoes;
begin

  menuEditarClick(menuEditar);

  If gfrmTV = nil Then
  begin
    btnNovo.Enabled         := False;   {'desabilitado}
    btnConsistir.Enabled    := False;
    btnSalvar.Enabled       := False;   {'desabilitado}
    btnProcurar.Enabled     := False;   {'desabilitado}
    btnPrimeiro.Enabled     := False;   {'desabilitado}
    btnAnterior.Enabled     := False;   {'desabilitado}
    btnProximo.Enabled      := False;   {'desabilitado}
    btnUltimo.Enabled       := False;   {'desabilitado}
  end
  Else
  begin
    If (grParam_TelaVar.Comando_MDI = ED_ALTERAR) Then
      btnNovo.Enabled := True
    Else
      btnNovo.Enabled := False;

    If grParam_TelaVar.Alterado Then
      btnSalvar.Enabled := True
    Else
      btnSalvar.Enabled := False;

    If grParam_TelaVar.Nav.bPrimeiro Then
      btnPrimeiro.Enabled := True
    Else
      btnPrimeiro.Enabled := False;

    If grParam_TelaVar.Nav.bAnterior Then
      btnAnterior.Enabled := True
    Else
      btnAnterior.Enabled := False;

    If grParam_TelaVar.Nav.bProximo Then
      btnProximo.Enabled := True
    Else
      btnProximo.Enabled := False;

    If grParam_TelaVar.Nav.bUltimo Then
      btnUltimo.Enabled := True
    Else
      btnUltimo.Enabled := False;

    if Not grParam_TelaVar.Bloqueado then
      btnConsistir.Enabled := True
    else
      btnConsistir.Enabled := False;

    btnProcurar.Enabled     := True;   {'habilitado}

  End;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.gp_AtualizaBarraStatus       atualiza a barra de status
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.gp_AtualizaBarraStatus;
begin

  stbMDI.Panels[0].Text := VAZIO;
  stbMDI.Panels[2].Text := '-';

  If grParam_TelaVar.DynaID > 0 Then
    stbMDI.Panels[2].Text := grParam_TelaVar.Cod_Visao;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.FormPaint       apresenta tela de selecao de tabelas
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.FormPaint(Sender: TObject);
begin
  if mbDentro = True then
    Exit;

  mbDentro := True;

  { abre a janela de cadastros }
  itemAbrirClick(Sender);
end;

{*-----------------------------------------------------------------------------
 *  TformMDI.FormPaint       apresenta tela de selecao de tabelas
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemConfigurarImpressoraClick(Sender: TObject);
begin
  dlgConfigurarImpressora.Execute;
end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemRotinasClick       apresenta tela de Rotinas
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemRotinasClick(Sender: TObject);
begin
  { se tem tela filha}
  If Not (gfrmTV = Nil) Then
    { pega o codigo}
    gsRtn_Atual := grParam_TelaVar.p.Cod_Seguranca
  Else
    { codigo generico}
    gsRtn_Atual := ROT_ROTINA;

  untTelaVar.gp_TelaVar_Executar(TV_COMANDO_ROTINAS, VAZIO);

  { apresenta a tela de impressao}
  formRotinas.ShowModal;
  formMDI.Refresh;

  { se escolheu um processo}
  if grProcessos.bValor then
  begin
    { Executa o processo}
    formProcessos.ShowModal;

    { Se confirmou o processo }
    if gbProcessoOK then
      untTelaVar.gp_TelaVar_Executar(TV_COMANDO_ROTINAS_FIM, VAZIO);
  end;

  { Volta ao normal}
  gsRtn_Atual := ROT_CONSULTA;

end;

{*-----------------------------------------------------------------------------
 *  TformMDI.itemFecharClick       Fecha o cadastro atual
 *
 *-----------------------------------------------------------------------------}
procedure TformMDI.itemFecharClick(Sender: TObject);
begin

  { Fecha a tela filha ativa}
  formMDI.ActiveMDIChild.Close;

end;


end.
