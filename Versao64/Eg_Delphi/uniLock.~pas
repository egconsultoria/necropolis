unit uniLock;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls;
{*======================================================================
 *            CONSTANTES
 *======================================================================}

{*======================================================================
 *            RECORDS
 *======================================================================}

type
 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntLock = class(TObject)
  private
    { Private declarations }
    function mf_Lock_ParametrosTratar(psRecurso: String;
                                      psTipoRecurso: String;
                                      psOperacao: String): Integer;
    function mf_Lock_DependenciaPosic(var psOperacao: String;
                                      psRecurso: String): Boolean;
    function mf_Lock_CriaItem(piTipoLock: Integer): Boolean;

  public
    { Public declarations }
    gdLockDataHora: TDateTime;
    gsLockMbNomeUsuario: String;           {'Nome do Usu�rio para MsgBox.}
    gvLockMbIdProcesso: Variant;           {'N�mero do processo do usu�rio para o MsgBox.}
    giLockMsg: Integer;
    giLockRetorno: Integer;

    {'****** Snapshots e Dynasets do LOCK}
    giSsDependencia: Integer;
    giDsRecurso: Integer;
    giDsTodosRecursos: Integer;

    function gf_Lock_Reter(var psRecurso: String;
                           psTipoRecurso: String;
                           piTipoLock: Integer): Integer;
    function gf_Lock_Liberar(psRecurso: String): Boolean;
    function gf_Lock_Checar(var psRecurso: String): Boolean;
    procedure gp_Lock_Inicializar;
    procedure gp_Lock_Finalizar;
end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untLock: TuntLock;

implementation

uses uniDados, uniConst;

{'-------------------------------------------------------------------------------------------------
' TuntLock.gf_Lock_Reter
'     Criar um "LOCK" do recurso par�metro pNomeParametro para o contexto pContexto
'-------------------------------------------------------------------------------------------------}
function TuntLock.gf_Lock_Reter(var psRecurso: String;
                                psTipoRecurso: String;
                                piTipoLock: Integer): Integer;
var
  lsSelect:           String;
  lbLOK_Eof:          Boolean;
  lsSql:              String;
  liColNome_Usuario:  Integer;
  liColId_Processo:   Integer;
  liColTipo_Lock:     Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    { variavel LockRetorno era para ser global, vamos tentar assim}
    giLockRetorno := LOCK_RETORNO_ATUALIZA;

//    dmDados.TransBegin('GLLOCK_gfRecurso_Reter');

    {' Verifica se o mesmo recurso/contexto j� est� retido}
    gaParm[0] := psRecurso;
    lsSelect := dmDados.SqlVersao('LK_0012', gaParm);
    if dmDados.giStatus <> STATUS_OK then
    begin
      Result := LOCK_RETORNO_ERRO;
      Exit;
    end;

    {' Fecha o uso anterior para evitar sumirem os recursos}
    if (giDsRecurso <> IOPTR_NOPOINTER) and (giDsRecurso <> 0) then
      dmDados.FecharConexao(giDsRecurso);

    giDsRecurso := IOPTR_NOPOINTER;

    giDsRecurso := dmDados.ExecutarSelect(lsSelect);

    if giDsRecurso = IOPTR_NOPOINTER then
    begin
      Result := LOCK_RETORNO_ERRO;
      Exit;
    end;

    lbLOK_Eof := dmDados.Status(giDsRecurso, IOSTATUS_NOROWS);

    liColNome_Usuario     := IOPTR_NOPOINTER;
    liColId_Processo      := IOPTR_NOPOINTER;
    liColTipo_Lock        := IOPTR_NOPOINTER;

    {'Implemienta��o de tentativas para Lock Exclusivo e para Tipo de Recurso = CALCULO}
    while not lbLOK_Eof do
    begin

      gsLockMbNomeUsuario := dmDados.ValorColuna(giDsRecurso, LOCK_FLD_NOME, liColNome_Usuario);
      gvLockMbIdProcesso := dmDados.ValorColuna(giDsRecurso, LOCK_FLD_PROCESSO, liColId_Processo);
      {' LOCK_RETORNO_CONSULTA, LOCK_RETORNO_ATUALIZA ou LOCK_RETORNO_NAOCONSULTA}
      giLockRetorno := dmDados.ValorColuna(giDsRecurso, LOCK_FLD_TIPO_LOCK, liColTipo_Lock);
      {'Conseguiu o recurso}
      if (giLockRetorno <> LOCK_RETORNO_NAOCONSULTA) then
        Break
      {'Se n�o conseguiu o recurso => pergunta se o usu�rio quer tentar novamente}
      else
      begin
        {' Se � o pr�prio usu�rio}
        if (gvLockMbIdProcesso = giId_Processo) then
        begin
          {' Voc� est� utilizando o recurso, portanto n�o poder� acess�-lo.}
          dmDados.gbFlagOnError := False;
          giLockMsg := dmDados.MensagemExibir(VAZIO, 2046);
          if dmDados.giStatus <> STATUS_OK then
          begin
            Result := LOCK_RETORNO_ERRO;
            Exit;
          end;
        end
        else
        begin
          dmDados.gbFlagOnError := False;
          dmDados.gaMsgParm[0] := UpperCase(gsLockMbNomeUsuario);
          dmDados.gaMsgParm[1] := VarToStr(gvLockMbIdProcesso);
          {'O recurso solicitado est� sendo utilizado pelo usu�rio X[N]. Voc� n�o poder� acess�-lo.}
          giLockMsg := dmDados.MensagemExibir(VAZIO, 2045);
          if dmDados.giStatus <> STATUS_OK then
          begin
            Result := LOCK_RETORNO_ERRO;
            Exit;
          end;
        end;
        if giLockMsg = IDCANCEL then
        begin
          Result := giLockRetorno;
//          dmDados.TransCommit('GLLOCK_gfRecurso_Reter');
          Exit;
        end;
        {' Fecha o uso anterior para evitar sumirem os recursos}
        dmDados.FecharConexao(giDsRecurso);
        giDsRecurso := IOPTR_NOPOINTER;

        giDsRecurso := dmDados.ExecutarSelect(lsSelect);
        if giDsRecurso = IOPTR_NOPOINTER then
        begin
          Result := LOCK_RETORNO_ERRO;
          Exit;
        end;
        lbLOK_Eof := dmDados.Status(giDsRecurso, IOSTATUS_NOROWS);
        {'Assume que o usu�rio pode atualizar,
        'pois pode n�o ter encontrado nenhuma linha no select}
        giLockRetorno := LOCK_RETORNO_ATUALIZA;
      end;
    end;

    {' Se registro n�o existe}
    if lbLOK_Eof then
    begin
      {' Grava registro na tabela de Lock}
      lsSql := 'INSERT into '+ LOCK_TBL_NOME + ' (' +
                                                    LOCK_FLD_NOME + ', ' +
                                                    LOCK_FLD_PROCESSO + ', ' +
                                                    LOCK_FLD_DATA + ', ' +
                                                    LOCK_FLD_RECURSO + ', ' +
                                                    LOCK_FLD_TIPO_RECURSO + ', ' +
                                                    LOCK_FLD_TIPO_LOCK + ', ' +
                                                    LOCK_FLD_ROTINA + ') ';
      lsSql := lsSql + CTEDB_VALUES_INI;
      lsSql := lsSql + '''' + gsNomeUsuario +  '''' + ', ';
      lsSql := lsSql + IntToStr(giId_Processo) + ', ';
      gdLockDataHora := Now;
      lsSql := lsSql + CTEDB_DATETIME_INI + FormatDateTime(MK_DATATIME, gdLockDataHora) + CTEDB_DATETIME_FIM + ', ';
      lsSql := lsSql + '''' + psRecurso + '''' + ', ';
      lsSql := lsSql + '''' + psTipoRecurso + '''' + ', ';
      lsSql := lsSql + '''' + IntToStr(piTipoLock) + '''' +', ';
      lsSql := lsSql + '''' + gsRtn_Atual + '''' + ' ' + CTEDB_VALUES_FIM;

      if dmDados.ExecutarSql(lsSql) <> IORET_OK then
      begin
//        dmDados.TransRollback('GLLOCK_gfRecurso_Reter');
        Result := LOCK_RETORNO_ERRO;
        dmDados.ErroTratar('gf_Recurso_Reter - uniLock');
        Exit;
      end;

      if psTipoRecurso = PARAMETROS then
      begin
        giLockRetorno := mf_Lock_ParametrosTratar(psRecurso, psTipoRecurso, LOCK_RETER);
        if dmDados.giStatus <> STATUS_OK then
        begin
          Result := LOCK_RETORNO_ERRO;
          Exit;
        end;
      end;

      {' se n�o reteve o recurso}
      if giLockRetorno <> LOCK_ATUALIZA then
        gf_Lock_Liberar(psRecurso);
    end
    else
    begin
      gsLockMbNomeUsuario := dmDados.ValorColuna(giDsRecurso, LOCK_FLD_NOME, liColNome_Usuario);
      gvLockMbIdProcesso := dmDados.ValorColuna(giDsRecurso, LOCK_FLD_PROCESSO, liColId_Processo);
      {' LOCK_RETORNO_CONSULTA, LOCK_RETORNO_ATUALIZA ou LOCK_RETORNO_NAOCONSULTA}
      giLockRetorno := dmDados.ValorColuna(giDsRecurso, LOCK_FLD_TIPO_LOCK, liColTipo_Lock);
    end;

    {' Trata resultado do Lock}
    {' Se n�o conseguiu o recurso}
    if giLockRetorno = LOCK_RETORNO_NAOCONSULTA then
    begin
      {' Se � o pr�prio usu�rio}
      if (gvLockMbIdProcesso = giId_Processo) then
      begin
        {' Voc� est� utilizando o recurso, portanto n�o poder� acess�-lo.}
        dmDados.gbFlagOnError := False;
        giLockMsg := dmDados.MensagemExibir(VAZIO, 2012);
        if dmDados.giStatus <> STATUS_OK then
        begin
          Result := LOCK_RETORNO_ERRO;
          Exit;
        end;
      end
      else
      begin
        dmDados.gbFlagOnError := False;
        dmDados.gaMsgParm[0] := Trim(UpperCase(gsLockMbNomeUsuario));
        dmDados.gaMsgParm[1] := VarToStr(gvLockMbIdProcesso);
        {'O recurso solicitado est� sendo utilizado pelo usu�rio X[N]. Voc� n�o poder� acess�-lo.}
        giLockMsg := dmDados.MensagemExibir(VAZIO, 2009);
        if dmDados.giStatus <> STATUS_OK then
        begin
          Result := LOCK_RETORNO_ERRO;
          Exit;
        end;
      end;
    end
    else
    begin
      {' Se conseguiu o recurso somente para consulta}
      if giLockRetorno = LOCK_RETORNO_CONSULTA then
      begin
        {' Se � o pr�prio usu�rio}
        if (gvLockMbIdProcesso = giId_Processo) then
        begin
          {' Voc� est� utilizando o recurso, portanto n�o poder� acess�-lo.}
          dmDados.gbFlagOnError := False;
          giLockMsg := dmDados.MensagemExibir(VAZIO, 2011);
          if dmDados.giStatus <> STATUS_OK then
          begin
            Result := LOCK_RETORNO_ERRO;
            Exit;
          end;
          {' Se n�o quer acessar para consulta}
          if giLockMsg = IDNO then
            giLockRetorno := LOCK_RETORNO_NAOCONSULTA;
        end
        else
        begin
          dmDados.gbFlagOnError := False;
          dmDados.gaMsgParm[0] := Trim(UpperCase(gsLockMbNomeUsuario));
          dmDados.gaMsgParm[1] := VarToStr(gvLockMbIdProcesso);
          {'O recurso solicitado est� sendo utilizado pelo usu�rio X[N]. Voc� n�o poder� acess�-lo.}
          giLockMsg := dmDados.MensagemExibir(VAZIO, 2010);
          if dmDados.giStatus <> STATUS_OK then
          begin
            Result := LOCK_RETORNO_ERRO;
            Exit;
          end;
        end;
      end;
    end;
    Result := giLockRetorno;

//    dmDados.TransCommit('GLLOCK_gfRecurso_Reter');

    dmDados.FecharConexao(giDsRecurso);
    giDsRecurso := IOPTR_NOPOINTER;

  except
//    dmDados.TransRollback('GLLOCK_gfRecurso_Reter');
    Result := LOCK_RETORNO_ERRO;
    dmDados.ErroTratar('gfRecurso_Reter - GLLOCK.BAS');
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntLock.gf_Lock_Liberar
'     Liberar o "LOCK" do recurso par�metro pNomeParametro para o contexto pContexto
'-------------------------------------------------------------------------------------------------}
function TuntLock.gf_Lock_Liberar(psRecurso: String): Boolean;
var
  lsSelect: String;
  lsSql: String;
  liNumProcesso: Integer;
  liColNome_Usuario     : Integer;       
  liColId_Processo      : Integer;
//  liColDtHora_Retencao  : Integer;
  liColRecurso          : Integer;
begin

  dmDados.giStatus := STATUS_OK;
  Result := False;

  try
    {' Verifica se o recurso/contexto j� est� retido}
    gaParm[0] := psRecurso;

    {' Fecha o uso anterior para evitar sumirem os recursos}
    if (giDsTodosRecursos <> IOPTR_NOPOINTER) and (giDsTodosRecursos <> 0) then
      dmDados.FecharConexao(giDsTodosRecursos);

    giDsTodosRecursos := IOPTR_NOPOINTER;

    {' Cria Dynaset com todos os registros contidos na LOK_Recursos}
    lsSelect := dmDados.SqlVersao('LK_0015', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    giDsTodosRecursos := dmDados.ExecutarSelect(lsSelect);

    liColNome_Usuario     := IOPTR_NOPOINTER;
    liColId_Processo      := IOPTR_NOPOINTER;
//    liColDtHora_Retencao  := IOPTR_NOPOINTER;
    liColRecurso          := IOPTR_NOPOINTER;

    if not dmDados.Status(giDsTodosRecursos, IOSTATUS_NOROWS) then
    begin
      lsSql := 'DELETE FROM ' + LOCK_TBL_NOME + ' where ';

      lsSql := lsSql + LOCK_FLD_NOME + ' = ''' + dmDados.ValorColuna(giDsTodosRecursos, LOCK_FLD_NOME, liColNome_Usuario) + ''' and ';
      liNumProcesso := dmDados.ValorColuna(giDsTodosRecursos, LOCK_FLD_PROCESSO, liColId_Processo);
      lsSql := lsSql + LOCK_FLD_PROCESSO + ' = ' + IntToStr(liNumProcesso) + ' and ';
//      lsSql := lsSql + LOCK_FLD_DATA + ' = ' + CTEDB_DATE_INI + FormatDateTime(MK_DATATIME, dmDados.ValorColuna(giDsTodosRecursos, LOCK_FLD_DATA, liColDtHora_Retencao)) + CTEDB_DATE_FIM + ' and ';
      lsSql := lsSql + LOCK_FLD_RECURSO + ' = ''' + dmDados.ValorColuna(giDsTodosRecursos, LOCK_FLD_RECURSO, liColRecurso) + '''';

      if dmDados.ExecutarSql(lsSql) <> IORET_OK then
      begin
        Result := False;
        dmDados.ErroTratar('gf_Lock_Liberar - uniLock');
        Exit;
      end;

      Result := True;
    end;

    {' Fecha o uso anterior para evitar sumirem os recursos}
    if (giDsTodosRecursos <> IOPTR_NOPOINTER) or (giDsTodosRecursos <> 0) then
      dmDados.FecharConexao(giDsTodosRecursos);

    giDsTodosRecursos := IOPTR_NOPOINTER;

  except
    dmDados.ErroTratar('gf_Lock_Liberar - uniLock.PAS');
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntLock.gf_Lock_Checar
'     Checar o "LOCK" do recurso par�metro
'-------------------------------------------------------------------------------------------------}
Function TuntLock.gf_Lock_Checar(var psRecurso: String): Boolean;
var
  lsSelect: String;
  liColNome_Usuario     : Integer;
  liColId_Processo      : Integer;
  liColTipo_Lock        : Integer;
begin

  Result := False;
  
  dmDados.giStatus := STATUS_OK;
  giLockRetorno := LOCK_RETORNO_CONSULTA;

  try
    {' Verifica se o recurso/contexto j� est� retido}
    gaParm[0] := psRecurso;
    gaParm[1] := LOCK_EXCLUSIVO;

    {' Cria Dynaset com todos os registros contidos na LOK_Recursos}
    lsSelect := dmDados.SqlVersao('LK_0016', gaParm);
    if dmDados.giStatus <> STATUS_OK then
      Exit;

    giDsRecurso := dmDados.ExecutarSelect(lsSelect);

    liColNome_Usuario     := IOPTR_NOPOINTER;
    liColId_Processo      := IOPTR_NOPOINTER;
    liColTipo_Lock        := IOPTR_NOPOINTER;

    {' Se registro n�o est� retido}
    if dmDados.Status(giDsRecurso, IOSTATUS_NOROWS) then
    begin
      giLockRetorno := mf_Lock_ParametrosTratar(psRecurso, PARAMETROS, LOCK_CHECAR);
      if dmDados.giStatus <> STATUS_OK then
        Exit;
    end
    {' se recurso retido com LOCK_EXCLUSIVO}
    else
    begin
      gsLockMbNomeUsuario := dmDados.ValorColuna(giDsRecurso, LOCK_FLD_NOME, liColNome_Usuario);
      gvLockMbIdProcesso := dmDados.ValorColuna(giDsRecurso, LOCK_FLD_PROCESSO, liColId_Processo);
      {' LOCK_RETORNO_CONSULTA, LOCK_RETORNO_ATUALIZA ou LOCK_RETORNO_NAOCONSULTA}
      giLockRetorno := dmDados.ValorColuna(giDsRecurso, LOCK_FLD_TIPO_LOCK, liColTipo_Lock);
    end;

    {' Trata resultado do Lock}
    {' Se n�o conseguiu o recurso}
    if giLockRetorno = LOCK_RETORNO_NAOCONSULTA then
    begin
      {' Se � o pr�prio usu�rio}
      if (gvLockMbIdProcesso = giId_Processo) then
      begin
        {' Voc� est� utilizando o recurso, portanto n�o poder� acess�-lo.}
        dmDados.gbFlagOnError := False;
        giLockMsg := dmDados.MensagemExibir(VAZIO, 2012);
        if dmDados.giStatus <> STATUS_OK then
          Exit;
      end
      else
      begin
        dmDados.gbFlagOnError := False;
        dmDados.gaMsgParm[0] := UpperCase(gsLockMbNomeUsuario);
        dmDados.gaMsgParm[1] := VarToStr(gvLockMbIdProcesso);
        {'O recurso solicitado est� sendo utilizado pelo usu�rio X[N]. Voc� n�o poder� acess�-lo.}
        giLockMsg := dmDados.MensagemExibir(VAZIO, 2009);
        if dmDados.giStatus <> STATUS_OK then
          Exit;
      end;
    end;

    dmDados.FecharConexao(giDsRecurso);
    giDsRecurso := IOPTR_NOPOINTER;

  except
    dmDados.ErroTratar('gf_Lock_Checar - UniLock.PAS');
  end;

  Result := (giLockRetorno = LOCK_RETORNO_CONSULTA);

end;

{'-------------------------------------------------------------------------------------------------
' TuntLock.mf_Lock_ParametrosTratar
'     Checar o "LOCK" do recurso par�metro
'-------------------------------------------------------------------------------------------------}
function TuntLock.mf_Lock_ParametrosTratar(psRecurso: String;
                                           psTipoRecurso: String;
                                           psOperacao: String): Integer;
var
  lsRetornoIO: String;

  {'Para �ndices das colunas no ValorColuna}
  liColTipo_Recurso: Integer;
  liColTipo_Lock: Integer;

  lsAux_tipo_Recurso: String;
  liAux_Tipo_Lock: Integer;

begin

  dmDados.giStatus := STATUS_OK;

  try
    liColTipo_Recurso := IOPTR_NOPOINTER;
    liColTipo_Lock := IOPTR_NOPOINTER;

    {' Deduz N�vel do Par�metro}
    if mf_Lock_DependenciaPosic(psOperacao, psRecurso) then
    begin
      lsRetornoIO := IORET_OK;
      {' Trata c�digos dependentes}
      {' Enquanto tiver c�digo de depend�ncia}
      while lsRetornoIO <> IORET_EOF do
      begin

        lsAux_tipo_Recurso := dmDados.ValorColuna(giSsDependencia, LOCK_FLD_TIPO_RECURSO, liColTipo_Recurso);
        liAux_Tipo_Lock := dmDados.ValorColuna(giSsDependencia, LOCK_FLD_TIPO_LOCK, liColTipo_Lock);

        if (lsAux_tipo_Recurso = FUNCIONARIO) Or (lsAux_tipo_Recurso = CALCULO) Or (lsAux_tipo_Recurso = RELATORIO) then
          if mf_Lock_CriaItem(liAux_Tipo_Lock) then
            Break;
        {' Pr�ximo recurso dependente}
        lsRetornoIO := dmDados.Proximo(giSsDependencia);
      end;
    end;
    Result := giLockRetorno;

    if (giSsDependencia <> IOPTR_NOPOINTER) or (giSsDependencia <> 0) then
      dmDados.FecharConexao(giSsDependencia);

    giSsDependencia := IOPTR_NOPOINTER;

  except
    Result := LOCK_RETORNO_ERRO;
    dmDados.ErroTratar('gf_Lock_ParametrosTratar - UniLock.PAS');
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntLock.lf_Lock_DependenciaPosic
'     cria a conexao de acordo com a operacao
'-------------------------------------------------------------------------------------------------}
function TuntLock.mf_Lock_DependenciaPosic(var psOperacao: String;
                                           psRecurso: String): Boolean;
var
  lsSelect: String;
  lpcOperacao: PChar;
begin

  lpcOperacao := PChar(psOperacao);
  Case lpcOperacao^ of
    LOCK_RETER:
    begin
      {' Verifica depend�ncia com outros Recursos}
      gaParm[0] := psRecurso;
      {' Data/Hora de Reten��o}
      gaParm[1] := CTEDB_DATETIME_INI + FormatDateTime(MK_DATATIME, gdLockDataHora) + CTEDB_DATETIME_FIM;
      {' Para desprezar o pr�prio registro que acabou de ser inserido}
      gaParm[2] := IntToStr(giId_Processo);

      {' Seleciona os registros dependentes que est�o retidos}
      lsSelect := dmDados.SqlVersao('LK_0014', gaParm);
    end;
    LOCK_CHECAR:
    Begin
      {' Verifica depend�ncia com outros Recursos
      ' ( criar SnapShot somente com recursos dependentes que estejam retidos com LOCK_EXCLUSIVO)}
      gaParm[0] := LOCK_EXCLUSIVO;
      gdLockDataHora := Now;
      {' Data/Hora de Reten��o}
      gaParm[1] := CTEDB_DATETIME_INI + FormatDateTime(MK_DATATIME, gdLockDataHora) + CTEDB_DATETIME_FIM;
      {' Para desprezar o pr�prio registro que acabou de ser inserido}
      gaParm[2] := IntToStr(giId_Processo);

      lsSelect := dmDados.SqlVersao('LK_0017', gaParm);
    end;
  end;
  giSsDependencia := dmDados.ExecutarSelect(lsSelect);

  Result := Not dmDados.Status(giSsDependencia, IOSTATUS_NOROWS);

end;

{'-------------------------------------------------------------------------------------------------
' TuntLock.mf_Lock_CriaItem
'     cria a conexao de acordo com a operacao
'-------------------------------------------------------------------------------------------------}
function TuntLock.mf_Lock_CriaItem(piTipoLock: Integer): Boolean;
var
  liColNome_Usuario: Integer;
  liColId_Processo: Integer;
begin
  Result := False;

  try
    liColNome_Usuario:= IOPTR_NOPOINTER;
    liColId_Processo:= IOPTR_NOPOINTER;

    if piTipoLock > giLockRetorno then
    begin
      {' Retorna o tipo de lock com que foi retido (LOCK_CONSULTA, LOCK_ATUALIZA OU LOCK_EXCLUSIVO)}
      giLockRetorno := piTipoLock;
      gsLockMbNomeUsuario := Trim(dmDados.ValorColuna(giSsDependencia, LOCK_FLD_NOME, liColNome_Usuario));
      gvLockMbIdProcesso := Trim(dmDados.ValorColuna(giSsDependencia, LOCK_FLD_PROCESSO, liColId_Processo));
      if giLockRetorno = LOCK_RETORNO_NAOCONSULTA then
        Result := True;
    end;

  except
    dmDados.ErroTratar('gf_Lock_CriaItem - UniLock.PAS');
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntLock.gp_Lock_Inicializar
'     cria o objeto e inicializa variaveis
'-------------------------------------------------------------------------------------------------}
procedure TuntLock.gp_Lock_Inicializar;
begin
  { cria o objeto}
  untLock := TuntLock.Create;
end;

{'-------------------------------------------------------------------------------------------------
' TuntLock.gp_Lock_Finalizar
'     destroi o objeto e zera variaveis
'-------------------------------------------------------------------------------------------------}
procedure TuntLock.gp_Lock_Finalizar;
begin
  untLock.Free;
end;

end.
