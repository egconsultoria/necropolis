�
 TDLGUSUARIO 0�  TPF0TdlgUsuario
dlgUsuarioLeftTop� BorderIcons BorderStylebsDialogCaptionUsu�rioClientHeight8ClientWidthFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPanel
pnlUsuarioLeftTopWidthHeight
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder  TLabellblNomeLeftTopWidthHeightCaptionNomeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblSenhaInicialLeft� TopWidth=HeightCaptionSenha InicialFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblDisponiveisLeftTophWidth[HeightCaptionGrupos dispon�veisFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabellblPertenceLeft(TophWidthmHeightCaptionGrupos a que pertenceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxcboNomeUsuarioLeftTop Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight	MaxLength
ParentFontTabOrder OnClickcboNomeUsuarioClickOnExitcboNomeUsuarioExit	OnKeyDowncboNomeUsuarioKeyDown
OnKeyPresscboNomeUsuarioKeyPress  	TCheckBoxchkUsuarioAtivoLeftTopHWidthaHeightCaptionUsu�rio AtivoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickchkUsuarioAtivoClick  TEditedtSenhaLeft� Top WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 	MaxLength

ParentFontPasswordChar*TabOrderOnChangeedtSenhaChange  TButtonbtnAdicionarLeft� Top� Width<HeightCaption&>>>Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnAdicionarClick  TButton
btnRemoverLeft� Top� Width<HeightCaption&<<<Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnRemoverClick  TListBoxlstGrupoDispLeftTopxWidth� HeightyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  TListBoxlstGrupoPertLeft(TopxWidth� HeightyFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeight
ParentFontSorted	TabOrder  	TCheckBoxchkAdministratorLeft� TopHWidthaHeightCaptionAdministratorFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickchkUsuarioAtivoClick   TButtonbtnNovoLeftTopWidthKHeightCaption&NovoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnNovoClick  TButton
btnExcluirLeft� TopWidthKHeightCaption&ExcluirEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnExcluirClick  TButton	btnFecharLeft� TopWidthKHeightCancel	CaptionFe&charFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnFecharClick  TButtonbtnAjudaLeft�TopWidthKHeightCaptionAj&udaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnAjudaClick  TButton	btnSalvarLeftXTopWidthKHeightCaption&SalvarEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickbtnSalvarClick   