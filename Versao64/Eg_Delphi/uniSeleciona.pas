unit uniSeleciona;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls, Buttons, DB, Variants,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}

type
 {*======================================================================
 *            RECORDS
 *======================================================================}

 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntSeleciona = class(TObject)
  private

    miPosicaoCursor: Integer;

    { Private declarations }
    function mf_Selec_Grid_Col_Traduzir(pvValor: Variant;
                                        piTipoTraducao: Integer): String;
    function mf_Selec_Criterio_Chaves_Montar(prId_Linhas: TumId_Linhas;
                                             pasColunas_Chave: array of String): String;
    function mf_Selec_String_Validar(psString: String;
                                     psTabelaValores: String): Boolean;
  public
    function gf_Selec_RecSet_Posicionar(var prGridInfo: TumGridMontar_Info;
                                        var paId_Linhas: array of TumId_Linhas;
                                        var paColunas_Chave: array of String): Boolean;
    function gf_Selec_Spread_Montar(var plsvSpread: TListView;
                                    var paColunasSpread: array of TumaGrid_Coluna;
                                    var prGridInfo: TumGridMontar_Info;
                                    var paId_Linhas: array of TumId_Linhas;
                                    var paColunas_Chave: array of String): Boolean;
    procedure gp_Selec_Inicializar;
    procedure gp_Selec_Finalizar;
end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untSeleciona: TuntSeleciona;

implementation

uses uniDados, uniMostraErros, uniSelPar;

{'-------------------------------------------------------------------------------------------------
' TuntSeleciona.gf_RecSet_Posicionar
'     Traduz o valor p/ colocar no grid
'-------------------------------------------------------------------------------------------------}
function TuntSeleciona.gf_Selec_RecSet_Posicionar(var prGridInfo: TumGridMontar_Info;
                                                  var paId_Linhas: array of TumId_Linhas;
                                                  var paColunas_Chave: array of String): Boolean;
var
  lsCriterio: String;        {'Monta o crit�rio de busca de chave}
  liContLin: Integer;        {'Contador para linhas do Grid}
  laColumns_Info: array[1..MAX_COLUMNS_INFO] of TColunaInfo;   {'Array com dados sobre colunas da conex�o}
  lbChavesIguais: Boolean;
  liContId: Integer;

  lsCriterioChvUnica: String;

  liPosicao: Integer;
  lsAuxChaveProcura: String;
  liIndColColunas: Integer;
  iContador: Integer;
begin

  Result := True;

  { Inicializa variaveis}
  for iContador := 1 to MAX_COLUMNS_INFO do
  begin
    laColumns_Info[iContador].iOrdinalPos := 0;
    laColumns_Info[iContador].ftTipo := ftUnknown;
    laColumns_Info[iContador].iTamanho := 0;
  end;

  {'Antes de Tudo , verifique se Dynaset existe}
  if (prGridInfo.Conexao = 0) or (prGridInfo.Conexao = IOPTR_NOPOINTER) then
  begin
    Result := False;
    Exit;
  end;

  {'Depois, verifique se Dynaset est� vazio}
//  if dmDados.Status(prGridInfo.Conexao, IOSTATUS_NOROWS) then
//  begin
//    Result := False;
//    Exit;
//  end;

  gbRemontaGrid := True;

  {'Obtem caracter�sticas das chaves}
  laColumns_Info[1].sNome := prGridInfo.ColunaProcura.Nome;
  dmDados.gsRetorno := dmDados.ColunasInfo(prGridInfo.Conexao, laColumns_Info, IOCOL_TYPE);

  {'-----------------------------------------------------------------
  '   Procura chave
  '-----------------------------------------------------------------}
  {'Se informou chave para procura}
  if prGridInfo.ChaveProcura <> VAZIO then
  begin
    {'Se coluna � texto ou memo}
    if (laColumns_Info[1].ftTipo = IOTYPE_TEXT) or
       (laColumns_Info[1].ftTipo = IOTYPE_MEMO) or
       (laColumns_Info[1].ftTipo = IOTYPE_BLOB) then
    begin
      {'Se caracteres inv�lidos para procura no WG2 informados na Chave}
      if (AnsiPos('%',prGridInfo.ChaveProcura) <> 0) or (AnsiPos('#',prGridInfo.ChaveProcura) <> 0) then
      begin
        gbRemontaGrid := False;

        dmDados.gbFlagOnError := False;
        dmDados.MensagemExibir(VAZIO,57);
        {'-----}
        Exit;
        {'-----}
      end;
      if (AnsiPos('*',prGridInfo.ChaveProcura) = 0) and (AnsiPos('?',prGridInfo.ChaveProcura) = 0) then
        lsCriterio := prGridInfo.ColunaProcura.Nome + ' = ' + '''' + dmDados.EstufaString(prGridInfo.ChaveProcura, '''') + CTEDB_LIKETODOS + ''''
      {'Se quer pesquisar por parte da chave}
      else
      begin
        lsAuxChaveProcura := prGridInfo.ChaveProcura;
        {'Trata caractere de procura independente de posicao}
        liPosicao := AnsiPos('*',lsAuxChaveProcura);
        while liPosicao <> 0 do
        begin
          lsAuxChaveProcura := Copy(lsAuxChaveProcura, 1, liPosicao - 1) + CTEDB_LIKETODOS + Copy(lsAuxChaveProcura, liPosicao + 1, Length(lsAuxChaveProcura));
          liPosicao := AnsiPos('*',Copy(lsAuxChaveProcura,liPosicao + 1, Length(lsAuxChaveProcura)));
        end;
        {'Trata caractere de procura em posicao espec�fica}
        liPosicao := AnsiPos('?',lsAuxChaveProcura);
        while liPosicao <> 0 do
        begin
          lsAuxChaveProcura := Copy(lsAuxChaveProcura, 1, liPosicao - 1) + CTEDB_LIKECARACTER + Copy(lsAuxChaveProcura, liPosicao + 1, Length(lsAuxChaveProcura));
          liPosicao := AnsiPos('?',Copy(lsAuxChaveProcura,liPosicao + 1, Length(lsAuxChaveProcura)));
        end;
        lsCriterio := prGridInfo.ColunaProcura.Nome + ' = ' + '''' + dmDados.EstufaString(lsAuxChaveProcura, '''') + '''';
      end;
    end
    {'se coluna � num�rica}
    else
    begin
      if not mf_Selec_String_Validar(Trim(prGridInfo.ChaveProcura), TNUMEROS) then
      begin
        {'chave informada para pesquisa n�o � num�rica}
        gbRemontaGrid := False;

        dmDados.gbFlagOnError := False;
        dmDados.MensagemExibir(VAZIO,28);
        {'-----}
        Exit;
        {'-----}
      end;
      lsCriterio := prGridInfo.ColunaProcura.Nome + ' = ' + Trim(prGridInfo.ChaveProcura);
    end;

    if prGridInfo.MudouProcura = False then
    begin
      {'Se j� havia feito procura}
      dmDados.gsRetorno := dmDados.AtivaProximaComp(prGridInfo.Conexao, 0);
      if dmDados.Status(prGridInfo.Conexao, IOSTATUS_NOMATCH) then
      begin
        {'se nao localizou}
        gbRemontaGrid := False;
        dmDados.gbFlagOnError := False;
        dmDados.MensagemExibir(VAZIO,30);
        {'-----}
        Exit;
        {'-----}
      end;
    end
    else
    begin
      {'Se primeira procura}
      untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' >= '''+dmDados.EstufaString(prGridInfo.ChaveProcura, '''')+'''', VAZIO, VAZIO);
      dmDados.gsRetorno := dmDados.AtivaPrimeiraComp(prGridInfo.Conexao, lsCriterio, paColunas_Chave, IOFLAG_GET);
      prGridInfo.MudouProcura := False;
      gbRemontaGrid := True;
    end;

    if not dmDados.Status(prGridInfo.Conexao, IOSTATUS_NOMATCH) then
    begin
      {'Primeiro verifica se j� esta na tela}
      for liContLin := 0 to prGridInfo.LinhasEmUso - 1 do
      begin
        lbChavesIguais := True;

        for liContId := 0 to High(paColunas_Chave) do
        begin
          liIndColColunas := IOPTR_NOPOINTER;
          if paColunas_Chave[liContId] <> VAZIO then
            if paId_Linhas[liContLin].Valor[liContId] <> dmDados.ValorColuna(prGridInfo.Conexao, paColunas_Chave[liContId], liIndColColunas) then
              lbChavesIguais := False;
        end;

        if lbChavesIguais then
        begin
          gbRemontaGrid := False;
          miPosicaoCursor := liContLin;
          Break;
        end
        else
          miPosicaoCursor := 0;
      end;
    end
    else
    begin
      {'se nao localizou}
      gbRemontaGrid := False;
      dmDados.gbFlagOnError := False;
      dmDados.MensagemExibir(VAZIO,29);
      {'-----}
      Exit;
      {'-----}
    end;
  end
  else
  begin
    {'-----------------------------------------------------------------
     '   Se nao for busca, avalie a acao
     '-----------------------------------------------------------------}
    case prGridInfo.Acao of
      PAGINA_FRENTE:
      begin
//        if gbCodigo then
          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' > '''+paId_Linhas[prGridInfo.LinhasEmUso - 1].Valor[prGridInfo.ColunaOrdem]+'''', VAZIO, VAZIO);
//        else
//          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' > '''+paId_Linhas[prGridInfo.LinhasEmUso - 1].Valor[prGridInfo.ColunaOrdem]+'''',
//                                                                                      ''''+paId_Linhas[prGridInfo.LinhasEmUso - 1].Valor[prGridInfo.ColunaOrdem-1]+'''', VAZIO);
        if dmDados.Status(prGridInfo.Conexao, IOSTATUS_EOF) then
          {'j� est� na �ltima linha}
          gbRemontaGrid := False;
      end;

      LINHA_FRENTE:
      begin
//        if gbCodigo then
          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' > '''+paId_Linhas[0].Valor[prGridInfo.ColunaOrdem]+'''', VAZIO, VAZIO);
//        else
//          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' > '''+paId_Linhas[0].Valor[prGridInfo.ColunaOrdem]+'''',
//                                                                                      ''''+paId_Linhas[0].Valor[prGridInfo.ColunaOrdem-1]+'''', VAZIO);
        if dmDados.Status(prGridInfo.Conexao, IOSTATUS_EOF) then
        begin
          {'Se n�o h� mais registros para frente}
          prGridInfo.UltimaPagina := True;
          gbRemontaGrid := False;
        end
        else
        begin
          {'Se h� mais registros posiciona na chave da segunda linha no grid}
          lsCriterioChvUnica := mf_Selec_Criterio_Chaves_Montar(paId_Linhas[1], paColunas_Chave);
          dmDados.gsRetorno := dmDados.AtivaPrimeiraComp(prGridInfo.Conexao, lsCriterioChvUnica, paColunas_Chave, IOFLAG_GET);
        end;
      end;

      PAGINA_TRAS:
      begin
//        if gbCodigo then
          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' < '''+paId_Linhas[0].Valor[prGridInfo.ColunaOrdem]+'''', VAZIO, ' DESC');
//        else
//          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' < '''+paId_Linhas[0].Valor[prGridInfo.ColunaOrdem]+'''',
//                                                                                      ''''+paId_Linhas[0].Valor[prGridInfo.ColunaOrdem-1]+'''', ' DESC');
        dmDados.Ultimo(prGridInfo.Conexao);
        liIndColColunas := IOPTR_NOPOINTER;
        lsCriterioChvUnica := dmDados.ValorColuna(prGridInfo.Conexao, paColunas_Chave[prGridInfo.ColunaOrdem], liIndColColunas);
        dmDados.FecharConexao(prGridInfo.Conexao);
        untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' >= '''+lsCriterioChvUnica+'''', VAZIO, VAZIO);
        {'Localiza o primeiro registro do Grid}
        lsCriterioChvUnica := mf_Selec_Criterio_Chaves_Montar(paId_Linhas[0], paColunas_Chave);
        dmDados.gsRetorno := dmDados.AtivaPrimeiraComp(prGridInfo.Conexao, lsCriterioChvUnica, paColunas_Chave, IOFLAG_GET);
        dmDados.gsRetorno := dmDados.Anterior(prGridInfo.Conexao, prGridInfo.LinhasEmUso(*High(laId_Linhas)*));
        if dmDados.gsRetorno = IORET_BOF then
          {'Se tem menos de uma pagina para voltar}
          dmDados.gsRetorno := dmDados.Primeiro(prGridInfo.Conexao)
        else
          if dmDados.gsRetorno <> IORET_OK then
            gbRemontaGrid := False;
      end;

      LINHA_TRAS:
      begin
//        if gbCodigo then
          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' < '''+paId_Linhas[0].Valor[prGridInfo.ColunaOrdem]+'''', VAZIO, ' DESC');
//        else
//          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' < '''+paId_Linhas[0].Valor[prGridInfo.ColunaOrdem]+'''',
//                                                                                      ''''+paId_Linhas[0].Valor[prGridInfo.ColunaOrdem-1]+'''', ' DESC');
//        dmDados.Ultimo(prGridInfo.Conexao);
        liIndColColunas := IOPTR_NOPOINTER;
        lsCriterioChvUnica := dmDados.ValorColuna(prGridInfo.Conexao, paColunas_Chave[prGridInfo.ColunaOrdem], liIndColColunas);
        dmDados.FecharConexao(prGridInfo.Conexao);
        untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' >= '''+lsCriterioChvUnica+'''', VAZIO, VAZIO);
        {'Localiza o primeiro registro do Grid}
        lsCriterioChvUnica := mf_Selec_Criterio_Chaves_Montar(paId_Linhas[0], paColunas_Chave);
        dmDados.gsRetorno := dmDados.AtivaPrimeiraComp(prGridInfo.Conexao, lsCriterioChvUnica, paColunas_Chave, IOFLAG_GET);
        dmDados.gsRetorno := dmDados.Anterior(prGridInfo.Conexao, 1);

        if dmDados.gsRetorno = IORET_BOF then
          {'Se tem menos de uma pagina para voltar}
          dmDados.gsRetorno := dmDados.Primeiro(prGridInfo.Conexao)
        else
          if dmDados.gsRetorno <> IORET_OK then
            gbRemontaGrid := False;

      end;
      ULTIMA_PAGINA:
      begin
//        if gbCodigo then
          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' <= '''+prGridInfo.ChvUltimaLinha[prGridInfo.ColunaOrdem]+'''', VAZIO, ' DESC');
//        else
//          untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' <= '''+prGridInfo.ChvUltimaLinha[prGridInfo.ColunaOrdem]+'''',
//                                                                                      ''''+prGridInfo.ChvUltimaLinha[prGridInfo.ColunaOrdem-1]+'''', ' DESC');
        dmDados.Ultimo(prGridInfo.Conexao);
        liIndColColunas := IOPTR_NOPOINTER;
        lsCriterioChvUnica := dmDados.ValorColuna(prGridInfo.Conexao, paColunas_Chave[prGridInfo.ColunaOrdem], liIndColColunas);
        dmDados.FecharConexao(prGridInfo.Conexao);
        untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, ' >= '''+lsCriterioChvUnica+'''', VAZIO, VAZIO);

        dmDados.gsRetorno := dmDados.Ultimo(prGridInfo.Conexao);
        dmDados.gsRetorno := dmDados.Anterior(prGridInfo.Conexao, High(paId_Linhas) - 1);
        {'Se tem menos de uma pagina para voltar}
        if dmDados.gsRetorno = IORET_BOF then
          dmDados.gsRetorno := dmDados.Primeiro(prGridInfo.Conexao)
        else
          if dmDados.gsRetorno <> IORET_OK then
            gbRemontaGrid := False;
      end;
      INICIAGRID:
      begin
        for liContId := 0 To High(paColunas_Chave) do
        begin
          liIndColColunas := IOPTR_NOPOINTER;
          if paColunas_Chave[liContId] <> VAZIO then
          begin
            prGridInfo.ChvPrimeiraLinha[liContId] := dmDados.ValorColuna(prGridInfo.Conexao, paColunas_Chave[liContId], liIndColColunas);
            prGridInfo.ChvUltimaLinha[liContId] := dmDados.UltimoRegistro(prGridInfo.Conexao, paColunas_Chave[liContId]);
          end;
        end;
      end;
      PRIMEIRA_PAGINA:
      begin
        untSelPar.gp_SelPar_Lista_Montar(prGridInfo.Conexao, grTelaVar.P, gbCodigo, VAZIO, VAZIO, VAZIO);
        dmDados.gsRetorno := dmDados.Primeiro(prGridInfo.Conexao);
      end;

      SO_ULTIMA_LINHA:
        {'posiciona �ltima linha do snap na primeira do grid}
        dmDados.gsRetorno := dmDados.Ultimo(prGridInfo.Conexao);
    end;
  end;

end;

{'-------------------------------------------------------------------------------------------------
' TuntSeleciona.gf_Spread_Montar
'     monta a lista
'-------------------------------------------------------------------------------------------------}
function TuntSeleciona.gf_Selec_Spread_Montar(var plsvSpread: TListView;
                                              var paColunasSpread: array of TumaGrid_Coluna;
                                              var prGridInfo: TumGridMontar_Info;
                                              var paId_Linhas: array of TumId_Linhas;
                                              var paColunas_Chave: array of String): Boolean;
var
  liContador: Integer;
  liContLin: Integer;        {'Contador para linhas da Lista}
  liContCol: Integer;        {'Contador para colunas da Lista}
  lbRetorno: Boolean;
  laColumns_Info: array[0..2] of TColunaInfo;    {'Array com dados sobre colunas da conex�o}
  liContId: Integer;
  lbChavesIguais: Boolean;
  liIndColColunas_Chave: Integer;
  ListItem: TListItem;
begin

  { Inicializa variaveis}
  for liContador := 0 to 2 do
  begin
    laColumns_Info[liContador].iOrdinalPos := 0;
    laColumns_Info[liContador].ftTipo := ftUnknown;
    laColumns_Info[liContador].iTamanho := 0;
  end;

  {'Assume que n�o muda posi��o do cursor}
  if plsvSpread.Selected <> nil then
    miPosicaoCursor := plsvSpread.Selected.Index
  else
    miPosicaoCursor := 0;

  Result := True;

  if prGridInfo.ColunaProcura.Nome <> VAZIO then
  begin
    laColumns_Info[0].sNome := prGridInfo.ColunaProcura.Nome;
    dmDados.gsRetorno := dmDados.ColunasInfo(prGridInfo.Conexao, laColumns_Info, IOCOL_ORDINAL);
    prGridInfo.ColunaProcura.Numero := laColumns_Info[0].iOrdinalPos;
  end;

  if prGridInfo.ChaveProcura = VAZIO then
  begin
    {'Se n�o est� em modo de procura}
    miPosicaoCursor := 0;
    Case prGridInfo.Acao of
      INICIAGRID:
      begin
        {'Se inicializa Grid
         'Obtem OrdinalPosition das colunas do Recordset a montar e da coluna chave}
        for liContCol := 0 To High(paColunasSpread) do
          laColumns_Info[liContCol + 1].sNome := paColunasSpread[liContCol].Nome;

          dmDados.gsRetorno := dmDados.ColunasInfo(prGridInfo.Conexao, laColumns_Info, IOCOL_ORDINAL);
          if dmDados.gsRetorno = IORET_FAIL then
          begin
            Result := False;
            Exit;
          end;
          for liContCol := 0 To High(paColunasSpread) do
          begin
            if paColunasSpread[liContCol].Nome <> VAZIO then
              {'Se coluna utilizada}
              paColunasSpread[liContCol].Numero := laColumns_Info[liContCol + 1].iOrdinalPos
            else
              paColunasSpread[liContCol].Numero := COLUNA_NAOUTILIZADA;
          end;
      end;
      LINHA_FRENTE:
      begin
        if plsvSpread.Selected.Index < plsvSpread.Items.Count - 1 then
        begin
          plsvSpread.Selected := plsvSpread.Items[plsvSpread.Selected.Index + 1];
          plsvSpread.ItemFocused := plsvSpread.Selected;
//          plsvSpread.SetFocus;
          Exit;
        end
        else
          miPosicaoCursor := plsvSpread.Selected.Index;

      end;
      LINHA_TRAS:
      begin
        if plsvSpread.Selected.Index > 1 then
        begin
          (*grSpread_Cursor llsvSpread, llsvSpread.ActiveRow - 1*)
          plsvSpread.Selected := plsvSpread.Items[plsvSpread.Selected.Index - 1];
          plsvSpread.ItemFocused := plsvSpread.Selected;
//          plsvSpread.SetFocus;
          Exit;
        end;
      end;
    end;
  end;

  lbRetorno := gf_Selec_RecSet_Posicionar(prGridInfo, paId_Linhas, paColunas_Chave);
  if lbRetorno = False then
  begin
    {'Se algum problema no posicionamento do recordset}
    prGridInfo.LinhasEmUso := 0;
    plsvSpread.Enabled := False;
    prGridInfo.PrimeiraPagina := True;
    prGridInfo.UltimaPagina := True;
    {'Retorna True}
    Exit;
  end
  else
  begin
    {'Posiciona cursor}
    plsvSpread.Enabled := True;
    plsvSpread.Selected := plsvSpread.Items[miPosicaoCursor];
    plsvSpread.ItemFocused := plsvSpread.Selected;
//    plsvSpread.SetFocus;

    if gbRemontaGrid = True then
    begin
      {'Se deve Remontar Grid a partir da linha chave posicionada}
      plsvSpread.Items.Clear;
      liContLin := 0;

      while (plsvSpread.Items.Count <= High(paId_Linhas)) and (not dmDados.Status(prGridInfo.Conexao, IOSTATUS_EOF)) do
      begin

        for liContId := 0 To High(paColunas_Chave) do
        begin
          liIndColColunas_Chave := IOPTR_NOPOINTER;
          if paColunas_Chave[liContId] <> VAZIO then
            paId_Linhas[liContLin].Valor[liContId] := dmDados.ValorColuna(prGridInfo.Conexao, paColunas_Chave[liContId], liIndColColunas_Chave);
        end;

        liContCol := 0;

        ListItem := plsvSpread.Items.Add;
        if paColunasSpread[liContCol].Numero <> COLUNA_NAOUTILIZADA then
        begin
          if paColunasSpread[liContCol].Traducao = GRID_TRADUCAO_NENHUMA then
            ListItem.Caption := dmDados.ValorColuna(prGridInfo.Conexao, VAZIO, paColunasSpread[liContCol].Numero)
          else
            ListItem.Caption := mf_Selec_Grid_Col_Traduzir(dmDados.ValorColuna(prGridInfo.Conexao, VAZIO, paColunasSpread[liContCol].Numero), paColunasSpread[liContCol].Traducao);
        end
        else
          ListItem.Caption := VAZIO;

        liContCol := liContCol + 1;

        while (liContCol + 1 <= plsvSpread.Columns.Count) or (liContCol <= High(paColunasSpread)) do
        begin
          if paColunasSpread[liContCol].Numero <> COLUNA_NAOUTILIZADA then
          begin
            if paColunasSpread[liContCol].Traducao = GRID_TRADUCAO_NENHUMA then
              ListItem.SubItems.Add(dmDados.ValorColuna(prGridInfo.Conexao, VAZIO, paColunasSpread[liContCol].Numero))
            else
              ListItem.SubItems.Add(mf_Selec_Grid_Col_Traduzir(dmDados.ValorColuna(prGridInfo.Conexao, VAZIO, paColunasSpread[liContCol].Numero), paColunasSpread[liContCol].Traducao));
          end
          else
            ListItem.SubItems.Add(VAZIO);

          liContCol := liContCol + 1;
        end;

        liContLin := liContLin + 1;

        if (dmDados.Proximo(prGridInfo.Conexao) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
          break;
        end;

        prGridInfo.LinhasEmUso := liContLin (*- 1*);

        {'Se ja chegou ao fim do RecordSet}
        if dmDados.Status(prGridInfo.Conexao, IOSTATUS_EOF) then
          prGridInfo.UltimaPagina := True
        else
          prGridInfo.UltimaPagina := False;

      end;
      {'Posiciona cursor}
      plsvSpread.Enabled := True;
      plsvSpread.Selected := plsvSpread.Items[miPosicaoCursor];
      plsvSpread.ItemFocused := plsvSpread.Selected;
//      plsvSpread.SetFocus;
    end;

    lbChavesIguais := True;
    if paId_Linhas[0].Valor[prGridInfo.ColunaOrdem] <> prGridInfo.ChvPrimeiraLinha[prGridInfo.ColunaOrdem] then
      lbChavesIguais := False;

    {'Se a primeira linha � a primeira do Recordset}
    if lbChavesIguais then
      prGridInfo.PrimeiraPagina := True
    else
      prGridInfo.PrimeiraPagina := False;

    lbChavesIguais := True;
    if paId_Linhas[prGridInfo.LinhasEmUso - 1].Valor[prGridInfo.ColunaOrdem] <> prGridInfo.ChvUltimaLinha[prGridInfo.ColunaOrdem] then
      lbChavesIguais := False;

    {'Se a ultima linha � a ultima do Recordset}
    if lbChavesIguais then
      prGridInfo.UltimaPagina := True
    else
      prGridInfo.UltimaPagina := False;

    Result := True;

end;

{'-------------------------------------------------------------------------------------------------
' TuntSeleciona.mf_Selec_Grid_ColTraduzir
'     Traduz o valor p/ colocar no grid
'-------------------------------------------------------------------------------------------------}
function TuntSeleciona.mf_Selec_Grid_Col_Traduzir(pvValor: Variant;
                                                  piTipoTraducao: Integer): String;
var
  p : PChar;
begin

  Result := '#ERRO#';

  Case piTipoTraducao of
    GRID_TRADUCAO_NIVELPAR:
    begin
      {'Se traduz origem de par�metros}
      Case pvValor of
        1:
          Result := 'Unid';
        2:
          Result := 'Empr';
        3:
          Result := 'Instal';
      else
          Result := '?';
      end;
    end;
    GRID_TRADUCAO_EFETIVADA:
    begin
      {' Se traduz efetiva��o de empresa/unidade}
      p := PChar(VarToStr(pvValor));
      Case p^ of
        'S':
          Result := 'Sim';
      else
          Result := 'N�o';
      end;
    end;
    GRID_TRADUCAO_CHAVE1:
    begin
      if AnsiPos(CALC_SEPARADOR_CHAVES,pvValor) > 0 then
        pvValor := Copy(pvValor, 1, AnsiPos(CALC_SEPARADOR_CHAVES,pvValor) - 1);
      Result := pvValor;
    end;
    GRID_TRADUCAO_CCUSTO:
    begin
      {' Se traduz centro de custo}
      if Trim(pvValor) = CCUSTO_VAZIO then
        Result := VAZIO
      else
        Result := Trim(pvValor);
    end;
    GRID_TRADUCAO_REFERENCIA:
      {' se traduz data para refer�ncia}
      Result := FormatDateTime('MM/YYYY', StrToDateTime(Trim(pvValor)));
    GRID_TRADUCAO_VALOR:
      Result := FormatFloat(MK_VALOR, StrToFloat(Trim(pvValor)));
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntSeleciona.lf_CriterioChaves_Montar
'     Monta o criterio
'-------------------------------------------------------------------------------------------------}
function TuntSeleciona.mf_Selec_Criterio_Chaves_Montar(prId_Linhas: TumId_Linhas;
                                                       pasColunas_Chave: array of String): String;
var
  liContId: Integer;
  lsCriterio: String;
  lsValor: String;
begin

  for liContId := 0 to High(pasColunas_Chave) do
  begin
    if pasColunas_Chave[liContId] = VAZIO then
      break;

    if liContId > 0 then
      lsCriterio := lsCriterio + ' AND ';

    lsCriterio := lsCriterio + pasColunas_Chave[liContId];
//    if prId_Linhas.Valor[liContId] = VAZIO then
    if VarIsNull(prId_Linhas.Valor[liContId]) then
      lsCriterio := lsCriterio + ' = Null'
    else
    begin
      case VarType(prId_Linhas.Valor[liContId]) of
        varString:
          lsCriterio := lsCriterio + ' = ''' + dmDados.EstufaString(prId_Linhas.Valor[liContId], '''') + '''';
        varDate:
          lsCriterio := lsCriterio + ' = ' + CTEDB_DATETIME_INI + FormatDateTime(MK_DATATIME, prId_Linhas.Valor[liContId]) + CTEDB_DATETIME_INI;
        varInteger:
          lsCriterio := lsCriterio + ' = ' + IntToStr(prId_Linhas.Valor[liContId]);
        varCurrency, varDouble:
          lsCriterio := lsCriterio + ' = ' + dmDados.TrataDecimalBD(prId_Linhas.Valor[liContId], IOFLAG_VALORPARABANCO);
        else
        begin
          Str(prId_Linhas.Valor[liContId], lsValor);
          lsCriterio := lsCriterio + ' = ' + lsValor;
        end;
      end;
    end;
  end;

  Result := lsCriterio;

end;

{'-------------------------------------------------------------------------------------------------
' TuntSeleciona.lf_String_Validar
'     valida a string
'-------------------------------------------------------------------------------------------------}
function TuntSeleciona.mf_Selec_String_Validar(psString: String;
                                               psTabelaValores: String): Boolean;
var
  liCaractere: Integer;          {'Posi��o do caractere do controle passado para Consistencia}
  liPosicao: Integer;            {'Posi��o do caractere do controle na tabela usada para Consistencia}
begin

  Result := True;
  {'Consiste primeiro caractere}
  liCaractere := 1;
  while liCaractere < Length(psString) do
  begin
    liPosicao := AnsiPos(UpperCase(Copy(psString, liCaractere, 1)), psTabelaValores);
    {'se n�o localizado o caractere na tabela}
    if liPosicao = 0 then
    begin
      {'indica que o conte�do n�o � v�lido}
      Result := False;
      {'for�a sa�da do loop}
      liCaractere := Length(psString) + 1;
    end
    else
      {'checa pr�ximo caractere}
      liCaractere := liCaractere + 1;

  end;
end;

{*-----------------------------------------------------------------------------
 *  TuntSeleciona.gp_Selec_Inicializar
 *        Cria o objeto e inicializa variaveis
 *-----------------------------------------------------------------------------}
procedure TuntSeleciona.gp_Selec_Inicializar;
begin
  { cria o objeto}
  untSeleciona := TuntSeleciona.Create;

  { Para evitar recurs�o: Inicia com False}
  untSeleciona.miPosicaoCursor := 0;
end;

{*-----------------------------------------------------------------------------
 *  TuntSeleciona.gp_Selec_Finalizar
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
procedure TuntSeleciona.gp_Selec_Finalizar;
begin
  { destroi objeto}
  untSeleciona.Free;
end;

end.
