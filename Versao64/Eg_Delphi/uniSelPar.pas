unit uniSelPar;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls, Buttons,
  uniConst;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
type
 {*======================================================================
 *            RECORDS
 *======================================================================}

 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntSelPar = class(TObject)
  private
    maParam_Colunas:    array [0..2] of String;

    { Private declarations }
  public
    { Public declarations }
    procedure gp_SelPar_Inicializar;
    procedure gp_SelPar_Finalizar;
    function gf_SelPar_Nova_Lista(var prSpreadInfo: TumGridMontar_Info;
                                  var paColunas: array of TumaGrid_Coluna;
                                  var paId_Linhas: array of TumId_Linhas;
                                  var prParametro: TumParametro;
                                  var paSpeedBs: array of TSpeedButton;
                                  var pedtChave: TEdit;
                                  var plsvSpread: TListView;
                                  var paColunas_Chave: array of String): Boolean;
    procedure gp_SelPar_Ativar_ScrollBtns(prPaginador: TumGridMontar_Info;
                                          var paSpeedBs: array of TSpeedButton;
                                          plsvSpread: TListView);
    function  gf_SelPar_Pode_Atualizar(psCod_Seguranca: String;
                                       piTipo: Integer): Boolean;
    procedure gp_SelPar_Lista_Montar(var piXLista: Integer;
                                     prParametro: TumParametro;
                                     pbOrdem: Boolean;
                                     psFiltro: string;
                                     psFiltroCodigo: string;
                                     psOrdem: string);


end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untSelPar: TuntSelPar;

implementation

uses uniDados, uniSeleciona, uniParam;

{*-----------------------------------------------------------------------------
 *  TuntSelPar.gp_SelPar_Inicializa
 *        Cria o objeto e inicializa variaveis
 *-----------------------------------------------------------------------------}
procedure TuntSelPar.gp_SelPar_Inicializar;
begin
  { cria o objeto}
  untSelPar := TuntSelPar.Create;
end;

{*-----------------------------------------------------------------------------
 *  TuntSelPar.gp_Erros_Finaliza
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
procedure TuntSelPar.gp_SelPar_Finalizar;
begin
  { destroi objeto}
  untSelPar.Free;
end;

{'-------------------------------------------------------------------------------------------------
' TuntSelPar.gf_SelPar_Nova_Lista
'     valida a string
'-------------------------------------------------------------------------------------------------}
function TuntSelPar.gf_SelPar_Nova_Lista(var prSpreadInfo: TumGridMontar_Info;
                                         var paColunas: array of TumaGrid_Coluna;
                                         var paId_Linhas: array of TumId_Linhas;
                                         var prParametro: TumParametro;
                                         var paSpeedBs: array of TSpeedButton;
                                         var pedtChave: TEdit;
                                         var plsvSpread: TListView;
                                         var paColunas_Chave: array of String): Boolean;
begin

  Screen.Cursor := crHOURGLASS;

  Result := False;

  {'Por ora se for outline ou a spreadmontar n�o der certo desabilita tudo}
  prSpreadInfo.PrimeiraPagina := True;
  prSpreadInfo.UltimaPagina := True;
  prSpreadInfo.LinhasEmUso := 0;
  gp_SelPar_Ativar_ScrollBtns(prSpreadInfo, paSpeedBs, plsvSpread);

  gp_SelPar_Lista_Montar(prSpreadInfo.Conexao, prParametro, gbCodigo, VAZIO, VAZIO, VAZIO);

  if (prSpreadInfo.Conexao = IOPTR_NOPOINTER) then
    Exit;

  {'Habilita os outros controles - ja escolheu o parametro}
  if dmDados.Status(prSpreadInfo.Conexao, IOSTATUS_NOROWS) then
  begin
    prSpreadInfo.PrimeiraPagina := True;
    prSpreadInfo.UltimaPagina := True;
    prSpreadInfo.LinhasEmUso := 0;
    gp_SelPar_Ativar_ScrollBtns(prSpreadInfo, paSpeedBs, plsvSpread);
    plsvSpread.Items.Clear;

    {'Limpe campo chave das buscas anteriores}
    pedtChave.Text := VAZIO;
    pedtChave.Enabled := False;

    Screen.Cursor := crDefault;
    Exit;
  end;

  prSpreadInfo.Acao := INICIAGRID;
  prSpreadInfo.ColunaProcura.Numero := IOPTR_NOPOINTER;
  prSpreadInfo.ColunaProcura.Nome := VAZIO;
  prSpreadInfo.MudouProcura := True;
  prSpreadInfo.ChaveProcura := VAZIO;

  paColunas[0].Numero := IOPTR_NOPOINTER;
  paColunas[1].Numero := IOPTR_NOPOINTER;
  paColunas[0].Nome := prParametro.Coluna_Codigo;
  paColunas[0].Traducao := GRID_TRADUCAO_NENHUMA;
  paColunas[1].Nome := prParametro.Coluna_Descricao;
  paColunas[1].Traducao := GRID_TRADUCAO_NENHUMA;

  if untSeleciona.gf_Selec_Spread_Montar(plsvSpread, paColunas, prSpreadInfo, paId_Linhas, paColunas_Chave) then
  begin
    gp_SelPar_Ativar_ScrollBtns(prSpreadInfo, paSpeedBs, plsvSpread);
  end;

  {'Limpe campo chave das buscas anteriores}
  pedtChave.Text := VAZIO;

  Screen.Cursor := crDefault;

  Result := True;

end;

{'-------------------------------------------------------------------------------------------------
' TuntSelPar.gp_SelPar_Ativar_ScrollBtns
'     valida a string
'-------------------------------------------------------------------------------------------------}
procedure TuntSelPar.gp_SelPar_Ativar_ScrollBtns(prPaginador: TumGridMontar_Info;
                                                 var paSpeedBs: array of TSpeedButton;
                                                 plsvSpread: TListView);
begin

  if (prPaginador.LinhasEmUso > 0) then
  begin
    if (plsvSpread.Selected <> nil) then
    begin
      if (plsvSpread.Selected.Index > 0) or (not prPaginador.PrimeiraPagina) then
      begin
        paSpeedBs[0].Enabled := True;
        paSpeedBs[2].Enabled := True;
      end
      else
      begin
        paSpeedBs[0].Enabled := False;
        paSpeedBs[2].Enabled := False;
      end;
    end
    else
    begin
      paSpeedBs[0].Enabled := False;
      paSpeedBs[2].Enabled := False;
    end;

    if not prPaginador.PrimeiraPagina then
      paSpeedBs[1].Enabled := True
    else
      paSpeedBs[1].Enabled := False;

    if not prPaginador.UltimaPagina then
      paSpeedBs[4].Enabled := True
    else
      paSpeedBs[4].Enabled := False;

    if (plsvSpread.Selected <> nil) then
    begin
      if (plsvSpread.Selected.Index < prPaginador.LinhasEmUso - 1) or (not prPaginador.UltimaPagina) then
      begin
        paSpeedBs[3].Enabled := True;
        paSpeedBs[5].Enabled := True;
      end
      else
      begin
        paSpeedBs[3].Enabled := False;
        paSpeedBs[5].Enabled := False;
      end;
    end
    else
    begin
      paSpeedBs[3].Enabled := False;
      paSpeedBs[5].Enabled := False;
    end;
  end
  else
  begin
    paSpeedBs[0].Enabled := False;
    paSpeedBs[1].Enabled := False;
    paSpeedBs[2].Enabled := False;
    paSpeedBs[3].Enabled := False;
    paSpeedBs[4].Enabled := False;
    paSpeedBs[5].Enabled := False;
  end;
end;

{'-------------------------------------------------------------------------------------------------
' TuntSelPar.lp_SelPar_Lista_Montar
'     valida a string
'-------------------------------------------------------------------------------------------------}
procedure TuntSelPar.gp_SelPar_Lista_Montar(var piXLista: Integer;
                                            prParametro: TumParametro;
                                            pbOrdem: Boolean;
                                            psFiltro: string;
                                            psFiltroCodigo: string;
                                            psOrdem: string);
var
  lsSQL: String;
  lsColuna_Ordem: String;
begin

  dmDados.FecharConexao(piXLista);
  piXLista := IOPTR_NOPOINTER;

  if prParametro.Coluna_Nivel = VAZIO then
  begin
    maParam_Colunas[0] := prParametro.Coluna_Codigo;
    maParam_Colunas[1] := prParametro.Coluna_Descricao;
  end
  else
  begin
//    maParam_Colunas[0] := prParametro.Coluna_Nivel;
//    maParam_Colunas[1] := prParametro.Coluna_Codigo;
//    maParam_Colunas[2] := prParametro.Coluna_Descricao;
    maParam_Colunas[0] := prParametro.Coluna_Codigo;
    maParam_Colunas[1] := prParametro.Coluna_Descricao;
    maParam_Colunas[2] := prParametro.Coluna_Nivel;
  end;

  lsSQL := untParam.gf_Param_Sql(prParametro.Sigla_Tabela, VAZIO, maParam_Colunas);
//  lsSQL := 'SELECT TOP 10 Base.* FROM ' + prParametro.Nome_Tabela + ' As Base';
  lsSQL := Copy(lsSQL, 1, 6) + CTEDB_TOP_10 + Copy(lsSQL, 8, Length(lsSQL));

  {' Ajusta a coluna de ordena��o}
  if pbOrdem then
    lsColuna_Ordem := prParametro.Coluna_Codigo + psOrdem
  else
    lsColuna_Ordem := prParametro.Coluna_Descricao + psOrdem + ', Base.' + prParametro.Coluna_Codigo + psOrdem;

  if psFiltro <> VAZIO then
  begin
    if prParametro.Filtro = VAZIO then
    begin
      if pbOrdem then
        lsSQL := lsSQL + ' WHERE Base.' + prParametro.Coluna_Codigo + psFiltro
      else
      begin
        if psFiltroCodigo = VAZIO then
          lsSQL := lsSQL + ' WHERE Base.' + prParametro.Coluna_Descricao + psFiltro
        else
          lsSQL := lsSQL + ' WHERE Base.' + prParametro.Coluna_Descricao + ' + Base.' + prParametro.Coluna_Codigo + psFiltro + ' + ' + psFiltroCodigo;
      end;
    end
    else
    begin
      if pbOrdem then
        lsSQL := lsSQL + ' AND Base.' + prParametro.Coluna_Codigo + psFiltro
      else
      begin
        if psFiltroCodigo = VAZIO then
          lsSQL := lsSQL + ' AND Base.' + prParametro.Coluna_Descricao + psFiltro
        else
          lsSQL := lsSQL + ' AND Base.' + prParametro.Coluna_Descricao + ' + Base.' + prParametro.Coluna_Codigo + psFiltro + ' + ' + psFiltroCodigo;
      end;
    end;
  end;

  lsSQL := lsSQL + ' ORDER BY Base.' + lsColuna_Ordem;

  piXLista := dmDados.ExecutarSelect(lsSQL);
  if piXLista = IOPTR_NOPOINTER then
    piXLista := 0;

end;

{'-------------------------------------------------------------------------------------------------
' TuntSelPar.gf_SelPar_Pode_Atualizar
'     verifica se pode Atualizar
'-------------------------------------------------------------------------------------------------}
function TuntSelPar.gf_SelPar_Pode_Atualizar(psCod_Seguranca: String;
                                             piTipo: Integer): Boolean;
var
  i: Integer;
begin

  dmDados.giStatus := STATUS_OK;
  Result := False;

  try
    for i := 1 to High(gaPermRotinas) do
    begin
      if gaPermRotinas[i].Cod_SubRotina = VAZIO then
        Break;

      if gaPermRotinas[i].Cod_SubRotina = psCod_Seguranca then
      begin
        case piTipo of
          TIPO_ALTERA:
            Result := gaPermRotinas[i].Altera;
          TIPO_INCLUI:
            Result := gaPermRotinas[i].Inclui;
          TIPO_CONSULTA:
            Result := gaPermRotinas[i].Consulta;
          TIPO_EXCLUI:
            Result := gaPermRotinas[i].Exclui;
        end;
        Break
      end;
    end;

  except
  end;

end;

end.
