unit uniErros;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
type
 {*======================================================================
 *            RECORDS
 *======================================================================}

 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntErros = class(TObject)
  private
    { Private declarations }
    msNivelAdvertencia: String;    {'salva conte�do da zConfig_G2}
    miPtrTexto1:  Integer;         {' salva �ndice para a coluna Texto1}
    miPtrTexto2:  Integer;         {'salva �ndice para a coluna Texto2}
    miPtrTexto3:  Integer;         {'salva �ndice para a coluna Texto3}
    miPtrNivel:   Integer;         {'salva �ndice para a coluna Nivel}

  public
    { Public declarations }
    procedure gp_Erros_Inicializar;
    procedure gp_Erros_Finalizar;
    procedure gp_Erro_Carregar(piCodErro: Integer;
                               pavLiteral: array of Variant);
    procedure gp_ErroData_Carregar(psLinha: String;
                                   piCodErro: Integer;
                                   psDescrData: String);
    procedure gp_GridErros_Carregar;

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untErros: TuntErros;

implementation

uses uniDados, uniConst, uniMostraErros;

{*-----------------------------------------------------------------------------
 *  TuntErros.gp_Erros_Inicializa
 *        Cria o objeto e inicializa variaveis
 *-----------------------------------------------------------------------------}
procedure TuntErros.gp_Erros_Inicializar;
begin
  { cria o objeto}
  untErros := TuntErros.Create;

  untErros.msNivelAdvertencia := VAZIO;
  untErros.miPtrTexto1 := IOPTR_NOPOINTER;
  untErros.miPtrTexto2 := IOPTR_NOPOINTER;
  untErros.miPtrTexto3 := IOPTR_NOPOINTER;
  untErros.miPtrNivel :=  IOPTR_NOPOINTER;
end;

{*-----------------------------------------------------------------------------
 *  TuntErros.gp_Erros_Finaliza
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
procedure TuntErros.gp_Erros_Finalizar;
begin
  { destroi objeto}
  untErros.Free;
end;

{*----------------------------------------------------------------------
 * TuntErros.gp_Erro_Carregar   carrega o vetor com os erros de consistencias
 *
 *----------------------------------------------------------------------}
procedure TuntErros.gp_Erro_Carregar(piCodErro: Integer;
                                     pavLiteral: array of Variant);
var
  i: Integer;   {�ndice das literais que devem ser substitu�das na mensagem}
begin

  {se est� come�ando a carregar novos erros => assume que n�o ocorrer�o erros cr�ticos}
  if giNumErros = 0 then
    gbErros_Criticos := False;

  giNumErros := giNumErros + 1;

  gaErros[giNumErros - 1].sCodErro := IntToStr(piCodErro);
  for i := 0 to 2 do
  begin
    if Trim(pavLiteral[i]) <> VAZIO then
      gaErros[giNumErros - 1].avLiteral[i] := pavLiteral[i]
    else
      gaErros[giNumErros - 1].avLiteral[i] := VAZIO;
  end;

end;

{*----------------------------------------------------------------------
 * TuntErros.gp_ErroData_Carregar   define qual o erro na data
 *
 *----------------------------------------------------------------------}
procedure TuntErros.gp_ErroData_Carregar(psLinha: String;
                                         piCodErro: Integer;
                                         psDescrData: String);
begin

(*  Dim i As Integer  '�ndice das literais que devem ser substitu�das na mensagem

  On Error GoTo ERRO_grErroData_Carregar
  gStatus = STATUS_OK

  ReDim gMsgParm(2)
  if pLinha = VAZIO then
    gMsgParm(0) = VAZIO
  else
    gMsgParm(0) = "Linha " + pLinha + ": "
  end if
  gMsgParm(1) = pDescrData
  Select Case pCodErro
    'Erros de Datas
    Case DTERR_NUMCARAC
      gp_Erro_Carregar 16, gMsgParm()
    Case DTERR_BARRAS
      gp_Erro_Carregar 17, gMsgParm()
    Case DTERR_NAONUMERICA
      gp_Erro_Carregar 18, gMsgParm()
    Case DTERR_FORMATO
      gp_Erro_Carregar 19, gMsgParm()
    Case DTERR_ANO
      gp_Erro_Carregar 20, gMsgParm()
    Case DTERR_MES
      gp_Erro_Carregar 21, gMsgParm()
    Case DTERR_DIA
      gp_Erro_Carregar 22, gMsgParm()
    Case DTERR_VAZIA
      gp_Erro_Carregar 23, gMsgParm()
    'Erros de Refer�ncias
    Case RFERR_NUMCARAC
      gp_Erro_Carregar 31, gMsgParm()
    Case RFERR_BARRAS
      gp_Erro_Carregar 32, gMsgParm()
    Case RFERR_NAONUMERICA
      gp_Erro_Carregar 33, gMsgParm()
    Case RFERR_FORMATO
      gp_Erro_Carregar 34, gMsgParm()
    Case RFERR_ANO
      gp_Erro_Carregar 35, gMsgParm()
    Case RFERR_MES
      gp_Erro_Carregar 36, gMsgParm()
    Case RFERR_VAZIA
      gp_Erro_Carregar 37, gMsgParm()
  end Select
  if gStatus <> STATUS_OK then
    GoTo FIM_grErroData_Carregar
  end if
  Exit Sub
*)
end;

{*----------------------------------------------------------------------
 * TuntErros.gp_GridErros_Carregar   Preenche o grid da tela de erros de consistencias
 *
 *----------------------------------------------------------------------}
procedure TuntErros.gp_GridErros_Carregar;
var

  lsMens:         String;  {Monta mensagem completa}
  lsAuxMens:      String;  {Auxiliar para leitura dos textos do banco}
  liLinhas:       Integer;
  liAltura_Total: Integer;
  liErro_Critico: Boolean; {BOOLEANO}
  lsRetornoIO:    String;  {Para as rotinas de IO}
  lvNivel:        Variant; {Para guardar o nivel da mensagem}
  liLinhaErro:    Integer; {linha de erro do grid}
  sSql:           String;

begin

  {' se ainda n�o leu zConfig_G2}
  if msNivelAdvertencia = VAZIO then
(*  begin
    lsNivelAdvertencia := gf_Config_Valor('Nivel_Advertencia');
    if lsNivelAdvertencia = VAZIO then*)
      msNivelAdvertencia := '5';

    {'Inicializa tamb�m os �ndices para o gfIO_ValorColuna}
    miPtrTexto1 := IOPTR_NOPOINTER;
    miPtrTexto2 := IOPTR_NOPOINTER;
    miPtrTexto3 := IOPTR_NOPOINTER;
    miPtrNivel  := IOPTR_NOPOINTER;
(*  end;*)

  dlgMostraErros.grdErros.FixedRows := 1;
  {'inicializa grid com 2 linhas}
  dlgMostraErros.grdErros.RowCount := 1;

  {' Inicializa linha fixa do Grid}
  dlgMostraErros.grdErros.ColWidths[COL_ERRO] := 49;        {'C�digo do erro}
  dlgMostraErros.grdErros.ColWidths[COL_DESCRICAO] := 364;  {'Descri��o do erro}
  dlgMostraErros.grdErros.ColWidths[COL_TIPO] := 66;        {'Advert�ncia ou Erro}
  dlgMostraErros.grdErros.ColWidths[COL_NIVEL] := 32;       {'N�vel do erro}

  dlgMostraErros.grdErros.cells[COL_ERRO,0] := 'C�digo';
  dlgMostraErros.grdErros.cells[COL_DESCRICAO,0] := 'Descri��o da Ocorr�ncia';
  dlgMostraErros.grdErros.cells[COL_TIPO,0] := 'Tipo';
  dlgMostraErros.grdErros.cells[COL_NIVEL,0] := 'N�vel';

  dlgMostraErros.grdErros.RowHeights[0] := 17;
  liAltura_Total := 17;

  { abre o sql de mensagens com o codigo correto }
  sSql := 'SELECT * FROM zMENSAGENS';
  giSsMens := dmDados.ExecutarSelect(sSql);

  for liLinhaErro := 1 To giNumErros do
  begin
    dlgMostraErros.grdErros.RowCount := dlgMostraErros.grdErros.RowCount + 1;
    dlgMostraErros.grdErros.cells[COL_ERRO,liLinhaErro] := gaErros[liLinhaErro - 1].sCodErro;

    lsRetornoIO := dmDados.AtivaFiltro(giSsMens, 'Codigo = ''' + gaErros[liLinhaErro - 1].sCodErro + '''', IOFLAG_FIRST, IOFLAG_GET);
    if lsRetornoIO = IORET_EOF then
    begin
      dlgMostraErros.grdErros.cells[COL_DESCRICAO,liLinhaErro] := 'Mensagem n�o cadastrada';
      dlgMostraErros.grdErros.cells[COL_TIPO,liLinhaErro] := VAZIO;
      dlgMostraErros.grdErros.cells[COL_NIVEL,liLinhaErro] := VAZIO;
      dlgMostraErros.grdErros.RowHeights[liLinhaErro] := 17;
      liAltura_Total := liAltura_Total + 17;
    end
    else
    begin
      lsMens := VAZIO;
      liLinhas := 0;
      lsAuxMens := dmDados.ValorColuna(giSsMens,'Linha1',miPtrTexto1);
      if lsAuxMens <> VAZIO then
      begin
        lsMens := lsMens + dmDados.LiteralSubstituir(lsAuxMens, gaErros[liLinhaErro - 1].avLiteral) + ' '; //+ '#13' + '#10';
        liLinhas := liLinhas + 17
      end;
      lsAuxMens := dmDados.ValorColuna(giSsMens,'Linha2',miPtrTexto2);
      if lsAuxMens <> VAZIO then
      begin
        lsMens := lsMens + dmDados.LiteralSubstituir(lsAuxMens, gaErros[liLinhaErro - 1].avLiteral) + ' '; //+ Chr(13) + Chr(10);
        liLinhas := liLinhas + 17
      end;
      lsAuxMens := dmDados.ValorColuna(giSsMens,'Linha3',miPtrTexto3);
      if lsAuxMens <> VAZIO then
      begin
        lsMens := lsMens + dmDados.LiteralSubstituir(lsAuxMens, gaErros[liLinhaErro - 1].avLiteral);
        liLinhas := liLinhas + 17
      end;
      dlgMostraErros.grdErros.RowHeights[liLinhaErro] := liLinhas;
      liAltura_Total := liAltura_Total + liLinhas;

      dlgMostraErros.grdErros.cells[COL_DESCRICAO,liLinhaErro] := lsMens;

      lvNivel := dmDados.ValorColuna(giSsMens,'Nivel',miPtrNivel);
      {'se mensagem n�o tem n�vel}
      if lvNivel = VAZIO then
      begin
        if giErros_Modo = ERRO_MODO_CADASTRAIS then
          {'Assume que o erro � cr�tico, n�o permitindo grava��o}
          gbErros_Criticos := True;

        dlgMostraErros.grdErros.cells[COL_TIPO,liLinhaErro] := VAZIO;
        dlgMostraErros.grdErros.cells[COL_NIVEL,liLinhaErro] := VAZIO;
      end
      else
      begin
        dlgMostraErros.grdErros.cells[COL_NIVEL,liLinhaErro] := lvNivel;

        {'se n�o � advert�ncia}
        liErro_Critico := (lvNivel < StrToInt(msNivelAdvertencia));
        {'e n�o � do compilador}
        liErro_Critico := (liErro_Critico and ((StrToInt(gaErros[liLinhaErro - 1].sCodErro) < 22000) or (StrToInt(gaErros[liLinhaErro - 1].sCodErro) >= 22500)));
        if liErro_Critico then
        begin
          {'Assume que o erro � cr�tico, n�o permitindo grava��o}
          gbErros_Criticos := True;
          dlgMostraErros.grdErros.cells[COL_TIPO,liLinhaErro] := 'Erro';
        end
        else
          dlgMostraErros.grdErros.cells[COL_TIPO,liLinhaErro] := 'Advert�ncia';

      end;
    end;
  end;

  dmDados.FecharConexao(giSsMens);

  {' S� executa se tiver alguma mensagem}
  if dlgMostraErros.grdErros.RowCount > 1 then
  begin
    if liAltura_Total > dlgMostraErros.grdErros.Height then
        {'Compensa o ScrollBar}
        dlgMostraErros.grdErros.ColWidths[COL_DESCRICAO] := 347;

    {'Fixa essa linha de topo, para que n�o role com o Scroll}
    dlgMostraErros.grdErros.FixedRows := 1;
    dlgMostraErros.ShowModal;
  end;

end;

end.
 