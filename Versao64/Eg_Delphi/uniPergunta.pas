unit uniPergunta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const

  ALTURA_FORM = 136;
  ALTURA_PNL = 57;
  TOPO_BOTAO = 76;
  TAMANHO_LISTA = 100;

type
  TdlgPergunta = class(TForm)
    btnOK: TButton;
    btnCancelar: TButton;
    pnlFundo: TPanel;
    lblTitulo: TLabel;
    lblPergunta: TLabel;
    edtParametro: TEdit;
    cboParametro: TComboBox;
    mskParametro: TMaskEdit;
    lstParametro: TListBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure lstParametroKeyPress(Sender: TObject; var Key: Char);
    procedure lstParametroClick(Sender: TObject);
  private
    { Private declarations }
    miConexSql : Integer;
    msKeyTexto : string;
    mbKeyValida: Boolean;
    
    function mf_ListaCadastro_Estufa(plLista: TListBox;
                                     psCodigoSql: String;
                                     psCampo: String;
                                     piConexao: Integer;
                                     paParm: array of variant): Integer;
    function mf_ListaCadastro_Conteudo(piConexao: Integer;
                                       psNomeCampo: String;
                                       psCodigo: String;
                                       psCampo: String): String;
  public
    { Public declarations }
    gbCancel : Boolean;

  end;

var
  dlgPergunta: TdlgPergunta;

implementation

uses uniConst, uniDados, uniCombos;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TdlgPergunta.FormShow      prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgPergunta.FormShow(Sender: TObject);
var
  lp: Pchar;
begin
  { preenche os label}
  dlgPergunta.Caption := gsImpTituloRelatorio;
  lblPergunta.Caption := grImpPergunta.sPergunta;

  { nao cancelou}
  gbCancel := False;

  lp := PChar(grImpPergunta.sTpResposta);

  edtParametro.Text := VAZIO;
  cboParametro.Items.Clear;
  cboParametro.Text := VAZIO;
  mskParametro.Text := VAZIO_DATA;
  lstParametro.Items.Clear;

  miConexSql := 0;
  msKeyTexto := VAZIO;
  mbKeyValida := False;

  if grImpPergunta.sTpResposta = RESP_LISTA then
  begin
    dlgPergunta.Height := ALTURA_FORM + TAMANHO_LISTA;
    pnlFundo.Height := ALTURA_PNL + TAMANHO_LISTA;
    btnOK.Top := TOPO_BOTAO + TAMANHO_LISTA;
    btnCancelar.Top := TOPO_BOTAO + TAMANHO_LISTA;
  end
  else
  begin
    dlgPergunta.Height := ALTURA_FORM;
    pnlFundo.Height := ALTURA_PNL;
    btnOK.Top := TOPO_BOTAO;
    btnCancelar.Top := TOPO_BOTAO;
  end;

  case lp^ of
    RESP_DATA:
    begin
      edtParametro.Visible := False;
      cboParametro.Visible := False;
      lstParametro.Visible := False;
      mskParametro.Visible := True;
      mskParametro.EditMask := '!99/99/9999;1;_';
      mskParametro.SetFocus;
    end;
    RESP_TEXTO:
    begin
      if grImpPergunta.sSql <> VAZIO then
      begin
        miConexSql := dmDados.ExecutarSelect(grImpPergunta.sSql);
        miConexSql := untCombos.gf_ComboCadastro_Estufa(cboParametro, 'PARAM_0010', 'DESCRICAO', miConexSql, gaParm);
        edtParametro.Visible := False;
        cboParametro.Visible := True;
        cboParametro.SetFocus;
        mskParametro.Visible := False;
        lstParametro.Visible := False;
      end
      else
      begin
        edtParametro.Visible := True;
        edtParametro.SetFocus;
        cboParametro.Visible := False;
        mskParametro.Visible := False;
        lstParametro.Visible := False;
      end;
    end;
    RESP_MESANO:
    begin
      edtParametro.Visible := False;
      cboParametro.Visible := False;
      lstParametro.Visible := False;
      mskParametro.Visible := True;
      mskParametro.SetFocus;
      mskParametro.EditMask := '!99/9999;1;_';
    end;
    RESP_NUMERO:
    begin
      if grImpPergunta.sSql <> VAZIO then
      begin
        miConexSql := dmDados.ExecutarSelect(grImpPergunta.sSql);
        miConexSql := untCombos.gf_ComboCadastro_Estufa(cboParametro, 'PARAM_0010', 'DESCRICAO', miConexSql, gaParm);
        edtParametro.Visible := False;
        cboParametro.Visible := True;
        cboParametro.SetFocus;
        mskParametro.Visible := False;
        lstParametro.Visible := False;
      end
      else
      begin
        edtParametro.Visible := True;
        edtParametro.SetFocus;
        cboParametro.Visible := False;
        mskParametro.Visible := False;
        lstParametro.Visible := False;
      end;
    end;
    RESP_LISTA:
    begin
      if grImpPergunta.sSql <> VAZIO then
      begin
        miConexSql := dmDados.ExecutarSelect(grImpPergunta.sSql);
        miConexSql := mf_ListaCadastro_Estufa(lstParametro, 'PARAM_0010', 'DESCRICAO', miConexSql, gaParm);
        edtParametro.Visible := False;
        cboParametro.Visible := False;
        mskParametro.Visible := False;
        lstParametro.Visible := True;
        lstParametro.SetFocus;
      end;
    end;
  end;
end;

{*-----------------------------------------------------------------------------
 *  TdlgPergunta.FormClose      fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgPergunta.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  { se a conexao estiver aberta, fecha}
  if miConexSql <> 0 then
    dmDados.FecharConexao(miConexSql);
end;

{*-----------------------------------------------------------------------------
 *  TdlgPergunta.btnCancelarClick      fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgPergunta.btnCancelarClick(Sender: TObject);
begin
  { retorna VAZIO}
  gvImpResposta := VAZIO;
  { cancelou}
  gbCancel := True;

  dlgPergunta.Close;
end;

{*-----------------------------------------------------------------------------
 *  TdlgPergunta.btnOKClick      fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgPergunta.btnOKClick(Sender: TObject);
var
  lp: Pchar;
  lwAno: Word;
  lwMes: Word;
  lwDia: Word;
  liConta: Integer;
begin

  lp := PChar(grImpPergunta.sTpResposta);

  {'Consiste dado informado pelo usu�rio}
  Case lp^ of
    RESP_DATA:
    begin
      If not dmDados.DataValida(mskParametro.Text) Then
      begin
        ShowMessage('Informe valor no formato dd/mm/aaaa !');
        Exit;
      end
      Else
      begin
        DecodeDate(StrToDateTime(mskParametro.Text),lwAno,lwMes,lwDia);
        gvImpResposta := ' Date('+IntToStr(lwAno)+','+IntToStr(lwMes)+','+IntToStr(lwDia)+')';
//        gvImpResposta := StrToDate(mskParametro.Text);
      End;
    end;
    RESP_TEXTO:
    begin
      if grImpPergunta.sSql <> VAZIO then
      begin
//        If (edtParametro.Text = VAZIO) Then
//        begin
//          ShowMessage('Informe um valor !');
//          Exit;
//        end
//        Else
          gvImpResposta := cboParametro.Text
      end
      else
      begin
//        If (edtParametro.Text = VAZIO) Then
//        begin
//          ShowMessage('Informe um valor !');
//          Exit;
//        end
//        Else
          gvImpResposta := edtParametro.Text;
      end;
    end;
    RESP_MESANO:
    begin
      If not dmDados.DataValida('01/' + mskParametro.Text) Then
      begin
        ShowMessage('Informe valor no formato mm/aaaa !');
        Exit;
      end
      Else
      begin
        gvImpResposta := mskParametro.Text;
      End;
    end;
    RESP_NUMERO:
    begin
      if grImpPergunta.sSql <> VAZIO then
      begin
        gvImpResposta := untCombos.gf_ComboCadastro_Conteudo(miConexSql, 'DESCRICAO', cboParametro.Text, 'CODIGO');
      end
      else
      begin
        If (edtParametro.Text = VAZIO) Then
        begin
          ShowMessage('Informe um valor num�rico !');
          Exit;
        end
        Else
          gvImpResposta := edtParametro.Text;
      end;
    end;
    RESP_LISTA:
    begin
      if grImpPergunta.sSql <> VAZIO then
      begin
        gvImpResposta := VAZIO;
        if (lstParametro.SelCount > 0) Then
        begin
          for liConta := 0 to lstParametro.Items.Count - 1 do
          begin
            if lstParametro.Selected[liConta] then
            begin
              if gvImpResposta <> VAZIO then
                gvImpResposta := gvImpResposta + SEPARADOR;
              gvImpResposta := gvImpResposta + lstParametro.Items[liConta];
            end;
          end;
        end;
      end;
    end;
  end;

  dlgPergunta.Close;

end;

{*-----------------------------------------------------------------------------
 *  TdlgPergunta.mf_ListaCadastro_Estufa
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
function TdlgPergunta.mf_ListaCadastro_Estufa(plLista: TListBox;
                                              psCodigoSql: String;
                                              psCampo: String;
                                              piConexao: Integer;
                                              paParm: array of variant): Integer;
var
  lsSQL:          String;
  liConexao:      Integer;
  miIndColCampo: Integer;
begin

  dmDados.giStatus := STATUS_OK;

  try
    liConexao := piConexao;
    {' se ainda nao estiver aberto}
    if liConexao = 0 then
    begin
      lsSQL := dmDados.SqlVersao(psCodigoSql, paParm);
      if dmDados.giStatus <> STATUS_OK then
      begin
        Result := 0;
        dmDados.ErroTratar('mf_ListaCadastro_Estufa - uniPergunta.PAS');
        Exit;
      end;
      liConexao := dmDados.ExecutarSelect(lsSQL);
      if liConexao = IOPTR_NOPOINTER then
      begin
        Result := 0;
        Exit;
      end;
    end;

    if not dmDados.Status(liConexao, IOSTATUS_NOROWS) then
    begin
      dmDados.gsRetorno := dmDados.Primeiro(liConexao);

      miIndColCampo := IOPTR_NOPOINTER;

      while not (dmDados.gsRetorno = IORET_EOF) do
      begin
        plLista.Items.Add(dmDados.ValorColuna(liConexao, psCampo, miIndColCampo));
        dmDados.gsRetorno := dmDados.Proximo(liConexao);
      end;
    end;

    Result := liConexao;

  except
    Result := 0;
    dmDados.ErroTratar('mf_ListaCadastro_Estufa - uniPergunta.PAS');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TdlgPergunta.mf_ListaCadastro_Conteudo
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
Function TdlgPergunta.mf_ListaCadastro_Conteudo(piConexao: Integer;
                                                psNomeCampo: String;
                                                psCodigo: String;
                                                psCampo: String): String;
var
  lsConteudo: String;
  liIndCol: Integer;

begin

  dmDados.giStatus := STATUS_OK;

  try

    {' inicializa com vazio}
    lsConteudo := VAZIO;

    if piConexao <> 0 then
    begin
      liIndCOl := IOPTR_NOPOINTER;

      {' procura pelo Campo}
//      dmDados.gsRetorn := dmDados.AtivaFiltro(piConexao, psNomeCampo + '= ''' + psCodigo + '''', IOFLAG_FIRST, IOFLAG_GET);
      dmDados.gsRetorno := dmDados.Posiciona(piConexao, psNomeCampo, [psCodigo]);
      {' se achou}
      If Not dmDados.Status(piConexao, IOSTATUS_NOROWS) Then
        {' guarda o Conteudo}
        lsConteudo := dmDados.ValorColuna(piConexao, psCampo, liIndCOl);

//      dmDados.gsRetorno := dmDados.AtivaFiltro(piConexao, VAZIO, IOFLAG_FIRST, IOFLAG_GET);
    end;

    Result := lsConteudo;

  except
    Result := VAZIO;
    dmDados.ErroTratar('mf_ListaCadastro_Conteudo - uniPergunta.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TdlgPergunta.lstParametroKeyPress
 *        guarda o texto digitado e posiciona na lista
 *-----------------------------------------------------------------------------}
procedure TdlgPergunta.lstParametroKeyPress(Sender: TObject;
  var Key: Char);
var
  liConta: Integer;
begin

  { se for letra ou numero ou espaco }
  if ((Ord(Key) >= 48) and (Ord(Key) <= 57)) or ((Ord(Key) >= 65) and (Ord(Key) <= 90)) or ((Ord(Key) >= 97) and (Ord(Key) <= 122)) (*or (Ord(Key) = 32)*) then
  begin
    msKeyTexto := msKeyTexto + Key;
    mbKeyValida := True;
  end
  else
  begin
    msKeyTexto := VAZIO;
    mbKeyValida := False;
    Exit;
  end;

  { procura pelo texto digitado e posiciona }
  if (lstParametro.Items.Count > 0) then
  begin
    for liConta := 0 to lstParametro.Items.Count - 1 do
    begin
      if UpperCase(Copy(lstParametro.Items[liConta],1,Length(msKeyTexto))) = UpperCase(msKeyTexto) then
      begin
        lstParametro.TopIndex := liConta;
        break;
      end;
    end;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TdlgPergunta.lstParametroClick
 *        limpa o texto digitado
 *-----------------------------------------------------------------------------}
procedure TdlgPergunta.lstParametroClick(Sender: TObject);
begin

  if not mbKeyValida then
    msKeyTexto := VAZIO;

  mbKeyValida := False;
end;

end.
