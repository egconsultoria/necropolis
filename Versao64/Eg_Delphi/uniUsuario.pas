unit uniUsuario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, Outline, StdCtrls, ExtCtrls, ComCtrls;

type
  TdlgUsuario = class(TForm)
    pnlUsuario: TPanel;
    lblNome: TLabel;
    cboNomeUsuario: TComboBox;
    btnNovo: TButton;
    btnExcluir: TButton;
    btnFechar: TButton;
    btnAjuda: TButton;
    btnSalvar: TButton;
    chkUsuarioAtivo: TCheckBox;
    edtSenha: TEdit;
    btnAdicionar: TButton;
    btnRemover: TButton;
    lblSenhaInicial: TLabel;
    lblDisponiveis: TLabel;
    lblPertence: TLabel;
    lstGrupoDisp: TListBox;
    lstGrupoPert: TListBox;
    chkAdministrator: TCheckBox;
    procedure btnFecharClick(Sender: TObject);
    procedure btnAdicionarClick(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnRemoverClick(Sender: TObject);
    procedure btnAjudaClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure chkUsuarioAtivoClick(Sender: TObject);
    procedure edtSenhaChange(Sender: TObject);
    procedure cboNomeUsuarioClick(Sender: TObject);
    procedure cboNomeUsuarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cboNomeUsuarioKeyPress(Sender: TObject; var Key: Char);
    procedure cboNomeUsuarioExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    lbKeyUpDown: Boolean;
    lbAlterado : Boolean;
    lbAlterOut : Boolean;
    lsChave_Aux: String;
    liDsUsuario_Grupo: Integer;
    liDsUsuario: Integer;
    liSsUsuario: Integer;
    liSsSisGrup: Integer;
    liDsSisSubRot: Integer;
    liDsSisRot: Integer;
    lbCancel: Boolean;
    lsNomeAreaDesfaz: String;
    lbSenha_Alter: Boolean;
    lbNovo: Boolean;

    liIndColSenha      : Integer;
    liIndColAtivo      : Integer;
    liIndColUserNome   : Integer;
    liIndColAdmin      : Integer;

    procedure lp_Usuario_Excluir;
    procedure lp_Usuario_Consistir;
    procedure lp_Usuario_GravAutomatica;
    procedure lp_Usuario_Montar;
    procedure lp_Usuario_Salvar;
    procedure lp_Excluir_Out;
    procedure lp_ComboNome_Montar;
    procedure lp_SnapShot_Usu_Monta;
    procedure lp_Tela_Montar;
    procedure lp_NovoUsuario;
    procedure lp_OutUsuario_Monta;
    procedure lp_Salvar_Out;
    procedure lp_ConfirmaMudanca;
    procedure lp_CancelaMudanca;
    procedure lp_Dynaset_SisSubRot_Montar;
    procedure lp_Dynaset_SisRot_Montar;

  public
    { Public declarations }
  end;

var
  dlgUsuario: TdlgUsuario;


implementation

uses uniDados, uniUsuarioSenha, uniCombos, uniMDI, uniConst, uniErros;

{$R *.DFM}


{*-----------------------------------------------------------------------------
 *  TdlgUsuario.btnFecharClick              Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.btnFecharClick(Sender: TObject);
begin
  { fecha a tela}
  dlgUsuario.Close;
end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.btnAdicionarClick            Adiciona o grupo no outline que ele pertence
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.btnAdicionarClick(Sender: TObject);
var
  liSeqin:       Integer;
  liCounter:     Integer;
  lsString_Full: String;
begin
    {'item selecionado}
    liSeqin := lstGrupoDisp.ItemIndex;
    {'se algum item selecionado}
    If liSeqin <> -1 Then
    begin
        {'item selecionado}
        lsString_Full := lstGrupoDisp.items[liSeqin];

        {'Verifica se item j� existe}
        If lstGrupoPert.items.Count > 0 Then
        begin
            {'in�cio da lista}
            liCounter := 0;
            While lstGrupoPert.items.Count > liCounter do
            begin
              If lstGrupoPert.items[liCounter] = lsString_Full Then
              begin
                  lstGrupoPert.Items.Delete(liCounter);
                  {'item j� existe}
                  Break;
              End;
              liCounter := liCounter + 1;
            end;

            lstGrupoPert.Items.add(lsString_Full);
        end
        {'Se nao existe nenhum item}
        Else
            { inclui}
            lstGrupoPert.Items.add(lsString_Full);

        { foi alterado}
        lbAlterado := True;
        lbAlterOut := True;
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.btnSalvarClick           Salva as altera��es
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.btnSalvarClick(Sender: TObject);
begin

    {'ampulheta}
    dlgUsuario.Cursor := crHourglass;

    {'Consiste dados da janela de Usuarios}
    If Trim(cboNomeUsuario.Text) <> VAZIO Then
    begin
        lp_Usuario_Consistir;
        If giNumErros = 0 Then
        begin
            btnNovo.Enabled := True;
            btnExcluir.Enabled := True;
            lsChave_Aux := VAZIO;

            {'Salva o registro}
            lp_Usuario_Salvar;

            {'recria SnapShot usuario}
            lp_SnapShot_Usu_Monta;

            {' Monta Combo com nome do usu�rio}
            lp_ComboNome_Montar;

            lbNovo := False;
        End;
    End;

    {'ampulheta}
    dlgUsuario.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.btnNovoClick           Inclui novo ususario
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.btnNovoClick(Sender: TObject);
begin
    {' Indica que n�o cancelou a opera��o}
    lbCancel := False;
    If (liDsUsuario <> 0) And (liDsUsuario <> IOPTR_NOPOINTER) Then
        if dmDados.Status(liDsUsuario, IOSTATUS_EOF) = False then
            {' Verifica se teve altera��o no registro anterior}
            lp_Usuario_GravAutomatica;

    {' se n�o cancelou a montagem}
    If lbCancel = False Then
    begin
        lp_NovoUsuario;

        btnSalvar.Enabled := True;
        {'remove itens e seus subordinados}
        lstGrupoPert.Clear;
        cboNomeUsuario.Text := VAZIO;
        cboNomeUsuario.SetFocus;
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.btnRemoverClick          Remove grupo a quem pertence
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.btnRemoverClick(Sender: TObject);
begin

    {'se tem item selecionado}
    If lstGrupoPert.ItemIndex <> -1 Then
      lstGrupoPert.Items.Delete(lstGrupoPert.ItemIndex);

    { atualiza bot�es}
    If lstGrupoPert.items.Count > 0 Then
      btnRemover.Enabled := True
    else
      btnRemover.Enabled := False;

    lbAlterOut := True;
    lbAlterado := True;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.btnAjudaClick          mostra o help
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.btnAjudaClick(Sender: TObject);
var
  r : LongInt;

begin
    { apresenta o help}
//    r := winhelp(hwnd, 'ctacess0.hlp', HELP_CONTENTS, 0);

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.btnExcluirClick          Exclui o usuario
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.btnExcluirClick(Sender: TObject);
var
  liResp: Integer;
begin

    If Trim(cboNomeUsuario.Text) <> VAZIO Then
    begin
        {'Pede confirma��o para exclus�o}
        dmDados.gbFlagOnError := False;
        dmDados.gaMsgParm[0] := ' o Usu�rio ';
        dmDados.gaMsgParm[1] := cboNomeUsuario.Text;
        liResp := dmDados.MensagemExibir(VAZIO,2);

        {'ampulheta}
        dlgUsuario.Cursor := crHourGlass;
        {'Se deseja excluir}
        If liResp = IDYES Then
        begin
            lp_Usuario_Excluir;

            {'recria SnapShot usuario}
            lp_SnapShot_Usu_Monta;

            {' Monta Combo com nome do usu�rio}
            lp_ComboNome_Montar;

            {'remove itens e seus subordinados}
            lstGrupoPert.Clear;
            cboNomeUsuario.Text := VAZIO;
            cboNomeUsuario.SetFocus;

            btnSalvar.Enabled := False;
            btnExcluir.Enabled := False;

        End;
    End;
    { DEFAULT }
    dlgUsuario.Cursor := crDefault;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Usuario_Excluir          Exclui o usuario e limpa a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Usuario_Excluir;
begin

  try

    {comeca a transacao}
//    dmDados.TransBegin('SEGUSUAR_lp_Usuario_Excluir');

    lp_Excluir_Out;

    lbAlterOut := False;

    dmDados.gsRetorno := dmDados.Excluir(liDsUsuario);

    {'limpa controles}
    chkAdministrator.Checked := False;
    chkUsuarioAtivo.Checked := False;
    cboNomeUsuario.Text := VAZIO;
    edtSenha.Text := VAZIO;
    lbAlterado := False;
    lstGrupoPert.Clear;

//    dmDados.TransCommit('SEGUSUAR_lp_Usuario_Excluir');

  except
//    dmDados.TransRollback('SEGUSUAR_lp_Usuario_Excluir');
    {apresenta mensagem de erro}
    dmDados.ErroTratar('lp_Usuario_Excluir - UNIUSUARIO.PAS');
  end;
end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Excluir_Out          Exclui o usuario
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Excluir_Out;
var
  lsCriterio : String;
begin

    If lsChave_Aux = VAZIO Then
    begin
        lsCriterio := 'Nome_Usuario = ''' + Trim(cboNomeUsuario.Text) + '''';
        dmDados.gsRetorno := dmDados.AtivaFiltro(liDsUsuario_Grupo, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
        while not dmDados.Status(liDsUsuario_Grupo, IOSTATUS_EOF) do
        begin
          dmDados.gsRetorno := dmDados.Excluir(liDsUsuario_Grupo);
//          dmDados.gsRetorno := dmDados.AtivaFiltro(liDsUsuario_Grupo, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
          dmDados.gsRetorno := dmDados.Proximo(liDsUsuario_Grupo);
        end;
    end
    Else
    begin
        lsCriterio := 'Nome_Usuario = ''' + Trim(lsChave_Aux) + '''';
        dmDados.gsRetorno := dmDados.AtivaFiltro(liDsUsuario_Grupo, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
        while not dmDados.Status(liDsUsuario_Grupo, IOSTATUS_EOF) do
        begin
          dmDados.gsRetorno := dmDados.Excluir(liDsUsuario_Grupo);
//          dmDados.gsRetorno := dmDados.AtivaFiltro(liDsUsuario_Grupo, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
          dmDados.gsRetorno := dmDados.Proximo(liDsUsuario_Grupo);
        end;
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_ComboNome_Montar         preenche o combo nome do usuario
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_ComboNome_Montar;
var
  lsNome_cbo: String;
  lsNome_Usuario: String;
  liColNome_Usuario: Integer;
begin

    liColNome_Usuario := IOPTR_NOPOINTER;

    dmDados.gsRetorno := dmDados.Primeiro(liSsUsuario);
    lsNome_cbo := cboNomeUsuario.Text;
    cboNomeUsuario.Clear;
    cboNomeUsuario.Text := lsNome_cbo;
    while not dmDados.Status(liSsUsuario, IOSTATUS_EOF) do
    begin
      lsNome_Usuario := dmDados.ValorColuna(liSsUsuario, 'Nome_Usuario', liColNome_Usuario);
      cboNomeUsuario.Items.Add(lsNome_Usuario);
      if (dmDados.Proximo(liSsUsuario) = IORET_EOF) or
         (dmDados.giStatus <> STATUS_OK) then
          break;
    end;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_SnapShot_Usu_Monta         preenche o SnapShot nome do usuario
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_SnapShot_Usu_Monta;
var
  lvSelect: Variant;
begin
    {' Monta SnapShot contendo todos os Usu�rios}
    lvSelect := dmDados.SqlVersao('SE_0001', gaParm);

    {'Recria snapshot}
    dmDados.FecharConexao(liSsUsuario);

    liSsUsuario := dmDados.ExecutarSelect(lvSelect);

    {se ocorreu erro na execucao do SQL}
    if liSsUsuario = IOPTR_NOPOINTER then
    begin
      {apresenta mensagem de erro na abertura da conexao}
      dmDados.gbFlagOnError := False;
      dmDados.MensagemExibir(VAZIO,100);
      {sai da funcao}
      Exit;
    end;


end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.chkUsuarioAtivoClick       teve altera��o
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.chkUsuarioAtivoClick(Sender: TObject);
begin
  { teve alteracao}
  lbAlterado := True;
end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Usuario_GravAutomatica       teve altera��o
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Usuario_GravAutomatica;
var
  lsChaveEscolhida: String;
  liResp: Integer;
begin

    If lbAlterado = True Then
    begin
        If Trim(lsNomeAreaDesfaz) <> VAZIO Then
        begin
            lsChaveEscolhida := cboNomeUsuario.Text;

            {'Pede confirma��o para salvamento}
            dmDados.gbFlagOnError := False;
            dmDados.gaMsgParm[0] := ' do Usu�rio ';
            dmDados.gaMsgParm[1] := Trim(lsNomeAreaDesfaz);
            lsChave_Aux := Trim(lsNomeAreaDesfaz);

            {'Voc� deseja salvar as informa��es do Usu�rio}
            liResp := dmDados.MensagemExibir(VAZIO,1);

            cboNomeUsuario.Text := lsChaveEscolhida;
        end
        Else
            liResp := IDNO;

        { se confirmou }
        If liResp = IDYES Then
        begin
            {'Consist�ncia de campos}
            lp_Usuario_Consistir;
            If (giNumErros = 0) Or (giErros_Ignorar <> ERRO_CANCELAR) Then
            begin
                lp_Usuario_Salvar;
                {'recria SnapShot usuario}
                lp_SnapShot_Usu_Monta;
                {' Monta Combo com nome do usu�rio}
                lp_ComboNome_Montar
            end
            Else
            begin
                lbCancel := True;
                cboNomeUsuario.Text := lsNomeAreaDesfaz;
            End;
        end
        Else
        begin
          { se cancelou}
          If liResp = IDCANCEL Then
          begin
            lbCancel := True;
            cboNomeUsuario.Text := lsNomeAreaDesfaz;
          End;
        End;
    End;

    lsChave_Aux := VAZIO;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Usuario_Consistir       verifica os dados
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Usuario_Consistir;
var
  lsSenhaCriptografada: String;
begin

    giNumErros := 0;

    If Trim(cboNomeUsuario.Text) = VAZIO Then
        {' Usu�rio n�o informado}
        untErros.gp_Erro_Carregar( 2015, gaParm);

    {'carrega area aux. p/ desfazer}
    lsNomeAreaDesfaz := cboNomeUsuario.Text;

    If lstGrupoPert.items.Count = 0 Then
        {' Sistema/Grupo n�o informado}
        untErros.gp_Erro_Carregar( 2016, gaParm);

    If (lbNovo = True) And (Trim(edtSenha.Text) = VAZIO) Then
        {'Campo senha deve ser preenchido}
        untErros.gp_Erro_Carregar( 2022, gaParm);

    {' Se alterou senha}
    If lbSenha_Alter = True Then
    begin
        lsSenhaCriptografada := dlgUsuarioSenha.gf_Senha_Criptografar(UpperCase(edtSenha.Text));
        If lsSenhaCriptografada = ERRO_SENHAINVALIDA Then
            {' A Senha possui caracteres inv�lidos}
            untErros.gp_Erro_Carregar( 2051, gaParm);
    End;

    If giNumErros <> 0 Then
      untErros.gp_GridErros_Carregar;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Usuario_Montar       verifica os dados
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Usuario_Montar;
var
  lvSelect: Variant;
  lsCriterio: String;

begin

    {' Indica que n�o cancelou a opera��o}
    lbCancel := False;

    If ((liDsUsuario <> 0) And (liDsUsuario <> IOPTR_NOPOINTER)) Then
       if dmDados.Status(liDsUsuario, IOSTATUS_EOF) = False then
         {' Verifica se teve altera��o antes de mudar de registro}
         lp_Usuario_GravAutomatica;

    {' se n�o cancelou a montagem}
    If lbCancel = False Then
    begin
        lsCriterio := 'Nome_Usuario = ''' + Trim(dlgUsuario.cboNomeUsuario.Text) + '''';

        dmDados.gsRetorno := dmDados.AtivaFiltro(liSsUsuario, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
        {'Se existe no SnapShot}
        if dmDados.Status(liSsUsuario, IOSTATUS_EOF) = False then
        begin
            {' setar com false p/ ocorrer grava��o autom�tica ap�s clicar Novo e digitar uma chave v�lida}
            lbNovo := False;

            gaParm[0] := dlgUsuario.cboNomeUsuario.Text;
            lvSelect := dmDados.SqlVersao('SE_0002', gaParm);

            btnNovo.Enabled := True;
            btnExcluir.Enabled := True;

            dmDados.FecharConexao(liDsUsuario);
            liDsUsuario := dmDados.ExecutarSelect(lvSelect);

            {se ocorreu erro na execucao do SQL}
            if liDsUsuario = IOPTR_NOPOINTER then
            begin
              {apresenta mensagem de erro na abertura da conexao}
              dmDados.gbFlagOnError := False;
              dmDados.MensagemExibir(VAZIO,100);
              {sai da funcao}
              Exit;
            end;

            lp_Tela_Montar;
        end
        {'Se n�o existe no SnapShot}
        Else
        begin
            {'Se recordset n�o criado}
            If ((liDsUsuario = 0) Or (liDsUsuario = IOPTR_NOPOINTER)) Then
            begin
              gaParm[0] := gsNomeUsuario;
              lvSelect := dmDados.SqlVersao('SE_0002', gaParm);

              dmDados.FecharConexao(liDsUsuario);
              liDsUsuario := dmDados.ExecutarSelect(lvSelect);

              {se ocorreu erro na execucao do SQL}
              if liDsUsuario = IOPTR_NOPOINTER then
              begin
                {apresenta mensagem de erro na abertura da conexao}
                dmDados.gbFlagOnError := False;
                dmDados.MensagemExibir(VAZIO,100);
                {sai da funcao}
                Exit;
              end;

            End;

            {'Se n�o optou por Novo}
            If lbNovo = False Then
              {'rotina gera novo usu�rio}
              lp_NovoUsuario;
        End;
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Tela_Montar       monta a tela com novos dados
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Tela_Montar;
var
  lsSenha: String;
begin

    liIndColAtivo      := IOPTR_NOPOINTER;
    liIndColSenha      := IOPTR_NOPOINTER;
    liIndColAdmin      := IOPTR_NOPOINTER;

    If dmDados.ValorColuna(liDsUsuario, 'Id_Ativo', liIndColAtivo) = 'S' Then
        chkUsuarioAtivo.Checked := True
    Else
        chkUsuarioAtivo.Checked := False;

    If dmDados.ValorColuna(liDsUsuario, 'Admin', liIndColAdmin) = 'S' Then
        chkAdministrator.Checked := True
    Else
        chkAdministrator.Checked := False;

    lsSenha := dmDados.ValorColuna(liDsUsuario, 'Senha_Usuario', liIndColSenha);
    edtSenha.Text := dlgUsuarioSenha.gf_Senha_Descriptografar(lsSenha);

    lbAlterado := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_NovoUsuario       monta a tela com novos dados
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_NovoUsuario;
begin

    If ((liDsUsuario <> 0) And (liDsUsuario <> IOPTR_NOPOINTER)) Then
    begin
      dmDados.gsRetorno := dmDados.AtivaModo(liDsUsuario,IOMODE_INSERT);
      lbNovo := True;
    End;
    btnNovo.Enabled := False;
    btnExcluir.Enabled := False;

    chkAdministrator.Checked := False;
    chkUsuarioAtivo.checked := False;
    edtSenha.Text := VAZIO;
    lbAlterado := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_OutUsuario_Monta       Limpa a lista
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_OutUsuario_Monta;
var
  liColNome_Usuario: Integer;
  liColNum_Grupo: Integer;
  liColNome_Grupo: Integer;
  lsCriterio: String;
  lsSist_Grupo: String;

begin


    {' monta lista hier�rquica com Sistema/Grupo escolhidos}
    lstGrupoPert.Clear;
    lsCriterio := 'Nome_Usuario = ''' + Trim(cboNomeUsuario.Text) + '''';
    dmDados.gsRetorno := dmDados.AtivaFiltro(liDsUsuario_Grupo, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
    {'Se encontrou usu�rio}
    If not dmDados.Status(liDsUsuario_Grupo, IOSTATUS_EOF) Then
    begin

        liColNome_Usuario := IOPTR_NOPOINTER;
        liColNum_Grupo    := IOPTR_NOPOINTER;
        liColNome_Grupo   := IOPTR_NOPOINTER;

        while (not dmDados.Status(liDsUsuario_Grupo, IOSTATUS_EOF)) and
            (dmDados.ValorColuna(liDsUsuario_Grupo, 'Nome_Usuario', liColNome_Usuario) = Trim(cboNomeUsuario.Text)) do
        begin
            lsCriterio := 'Num_Grupo = ' + IntToStr(dmDados.ValorColuna(liDsUsuario_Grupo, 'Num_Grupo', liColNum_Grupo));
            dmDados.gsRetorno := dmDados.AtivaFiltro(liSsSisGrup, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
            lsSist_Grupo := dmDados.ValorColuna(liSsSisGrup, 'Nome_Grupo', liColNome_Grupo);
            lstGrupoPert.Items.add(lsSist_Grupo);

            if (dmDados.Proximo(liDsUsuario_Grupo) = IORET_EOF) or
               (dmDados.giStatus <> STATUS_OK) then
                break;
        end;
    End;
end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Usuario_Salvar       salva as alteracoes
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Usuario_Salvar;
var
  ldData_Hora: TDateTime;
  lsSenhaCriptografada: String;
  liIndColDtHoraIni: Integer;
  liIndColDtHoraAlt: Integer;
  lvSelect: Variant;

begin

  try

    {comeca a transacao}
//    dmDados.TransBegin('SEGUSUAR_lp_Usuario_Salvar');

    {'Data e Hora do Sistema}
    ldData_Hora := Now;

    liIndColAtivo      := IOPTR_NOPOINTER;
    liIndColSenha      := IOPTR_NOPOINTER;
    liIndColAdmin      := IOPTR_NOPOINTER;
    liIndColUserNome   := IOPTR_NOPOINTER;
    liIndColDtHoraIni  := IOPTR_NOPOINTER;
    liIndColDtHoraAlt  := IOPTR_NOPOINTER;

    cboNomeUsuario.Text := UpperCase(cboNomeUsuario.Text);
    {'Se n�o est� inserindo}
    If lbNovo <> True Then
       dmDados.gsRetorno := dmDados.AtivaModo(liDsUsuario,IOMODE_EDIT)
    Else
    begin
        {' Se n�o � grava��o autom�tica}
        If lsChave_Aux = VAZIO Then
            dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario,'Nome_Usuario',liIndColUserNome,Trim(cboNomeUsuario.Text))
        Else
            dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario,'Nome_Usuario',liIndColUserNome,Trim(lsChave_Aux));
    End;
    If lbSenha_Alter = True Then
    begin
        dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario,'DtHora_Senha_Inicial',liIndColDtHoraIni,ldData_Hora);
        dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario,'DtHora_Senha_Alterada',liIndColDtHoraAlt,ldData_Hora);
        {criptografar antes de mandar para o banco}
        lsSenhaCriptografada := dlgUsuarioSenha.gf_Senha_Criptografar(UpperCase(edtSenha.Text));
        dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario,'Senha_Usuario',liIndColSenha,lsSenhaCriptografada);
    End;

    If chkUsuarioAtivo.Checked = True Then
        dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario,'Id_Ativo',liIndColAtivo,'S')
    Else
        dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario,'Id_Ativo',liIndColAtivo,'N');

    If chkAdministrator.Checked = True Then
        dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario,'Admin',liIndColAdmin,'S')
    Else
        dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario,'Admin',liIndColAdmin,'N');

    dmDados.gsRetorno := dmDados.Alterar(liDsUsuario,'SEG_Usuario','Nome_Usuario = '''+Trim(cboNomeUsuario.Text)+'''');

    If lbAlterOut = True Then
    begin
        lp_Salvar_Out;
        lbAlterOut := False;
    End;

//    dmDados.TransCommit('SEGUSUAR_lp_Usuario_Salvar');

    {'Se inseriu}
    If lbNovo = True Then
    begin
        {' Recria Dynaset}
        gaParm[0] := cboNomeUsuario.Text;
        lvSelect := dmDados.SqlVersao('SE_0002', gaParm);

        dmDados.FecharConexao(liDsUsuario);
        liDsUsuario := dmDados.ExecutarSelect(lvSelect);
    End;

    lbNovo := False;
    lbAlterado := False;

  except
//    dmDados.TransRollback('SEGUSUAR_lp_Usuario_Salvar');
    {apresenta mensagem de erro}
    dmDados.ErroTratar('lp_Usuario_Salvar - UNIUSUARIO.PAS');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Salvar_Out       salva as alteracoes na lista
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Salvar_Out;
var
    liSeqim: Integer;
    lsString_Full: String;
    liColNome_Usuario: Integer;
    liColNum_Grupo: Integer;
    liColGrupo: Integer;
    lsCriterio: String;
    lliNumGrupo: LongInt;

begin

    lp_Excluir_Out;

    liColNome_Usuario := IOPTR_NOPOINTER;
    liColNum_Grupo := IOPTR_NOPOINTER;
    liColGrupo := IOPTR_NOPOINTER;

    For liSeqim := 0 To (lstGrupoPert.items.Count - 1) do
    begin
        {'conteudo}
        lsString_Full := lstGrupoPert.items[liSeqim];

        {'Atualiza��o no DB.}
        dmDados.gsRetorno := dmDados.AtivaModo(liDsUsuario_Grupo,IOMODE_INSERT);
        {' Se n�o � grava��o autom�tica}
        If lsChave_Aux = VAZIO Then
            dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario_Grupo,'Nome_Usuario',liColNome_Usuario,Trim(cboNomeUsuario.Text))
        Else
            dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario_Grupo,'Nome_Usuario',liColNome_Usuario,Trim(lsChave_Aux));

        lsCriterio := 'Nome_Grupo = ''' + Trim(lsString_Full) + '''';

        dmDados.gsRetorno := dmDados.AtivaFiltro(liSsSisGrup, lsCriterio, IOFLAG_FIRST, IOFLAG_GET);
        { pega o numero do grupo}
        lliNumGrupo := dmDados.ValorColuna(liSsSisGrup, 'Num_Grupo', liColNum_Grupo);
        dmDados.gsRetorno := dmDados.AlterarColuna(liDsUsuario_Grupo,'Num_Grupo',liColGrupo,lliNumGrupo);

        dmDados.gsRetorno := dmDados.Incluir(liDsUsuario_Grupo,'SEG_UsuarioGrupo');
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_ConfirmaMudanca       Confirma a mudanca de usuario
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_ConfirmaMudanca;
var
  lsStringUpper: String;

begin

    If Trim(cboNomeUsuario.Text) <> VAZIO Then
    begin
        btnSalvar.Enabled := True;
        If Length(cboNomeUsuario.Text) > 15 Then
            cboNomeUsuario.Text := Copy(cboNomeUsuario.Text, 1, 15);

        lsStringUpper := UpperCase(cboNomeUsuario.Text);
        cboNomeUsuario.Text := lsStringUpper;

        lp_Usuario_Montar;

        lp_OutUsuario_Monta;

        lbSenha_Alter := False;
    end
    Else
    begin
        btnSalvar.Enabled := False;
        btnExcluir.Enabled := False;
    End;

    {'carrega area aux. p/ desfazer}
    lsNomeAreaDesfaz := cboNomeUsuario.Text;

end;
{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_CancelaMudanca       Cancela a mudanca de novo usuario
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_CancelaMudanca;
begin
    { volta o nome que estava antes}
    cboNomeUsuario.Text := lsNomeAreaDesfaz;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.edtSenhaChange              Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.edtSenhaChange(Sender: TObject);
begin
  { teve alteracao}
  lbAlterado := True;
  lbSenha_Alter := True;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.cboNomeUsuarioClick              Confirma mudanca de usuario
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.cboNomeUsuarioClick(Sender: TObject);
begin

    If lbKeyUpDown <> True Then
        {'assume mudan�a de chave}
        lp_ConfirmaMudanca;

    lbKeyUpDown := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.cboNomeUsuarioKeyDown          Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.cboNomeUsuarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    Case Key of
      KEY_UP:
        lbKeyUpDown := True;
      KEY_DOWN:
        lbKeyUpDown := True;
    End;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.cboNomeUsuarioKeyPress          Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.cboNomeUsuarioKeyPress(Sender: TObject;
  var Key: Char);
begin

    Case key of
      {'foi pressionado "Enter"}
      KEY_ENTER:
      begin
        key := #0;        {' limpa a pilha de teclas digitadas}
        lp_ConfirmaMudanca;         {'assume que escolheu bot�o OK}
      end;
      {'foi pressionado "Esc"}
      KEY_ESCAPE:
      begin
        key := #0;        {' limpa a pilha de teclas digitadas}
        lp_CancelaMudanca;   {'assume que escolheu bot�o OK}
      end;
    End;

    If (Length(cboNomeUsuario.Text) > 14) And (cboNomeUsuario.SelText = VAZIO) And (key <> KEY_BACK) Then
       key := #0;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.cboNomeUsuarioExit          Houve mudancas
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.cboNomeUsuarioExit(Sender: TObject);
begin

    If lbKeyUpDown <> True Then
        {'assume mudan�a de chave}
        lp_ConfirmaMudanca;

    lbKeyUpDown := False;

end;


{*-----------------------------------------------------------------------------
 *  TdlgUsuario.FormShow          Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.FormShow(Sender: TObject);
var
  lsSist_Grupo: String;
  liColNome_Grupo: Integer;

begin

  liDsUsuario_Grupo := IOPTR_NOPOINTER;
  liDsUsuario       := IOPTR_NOPOINTER;
  liSsUsuario       := IOPTR_NOPOINTER;
  liSsSisGrup       := IOPTR_NOPOINTER;
  liDsSisSubRot     := IOPTR_NOPOINTER;
  liDsSisRot        := IOPTR_NOPOINTER;

  {'recria SnapShot usuario_grupo}
  formMDI.gp_Dynaset_UsuGrup_Montar(liDsUsuario_Grupo);
                                          
  {'recria snapshot de sistemas_grupo}
  formMDI.gp_SnapShot_SisGru_Montar(liSsSisGrup);

  {'recria SnapShot sistema_subrotina}
  lp_Dynaset_SisSubRot_Montar;

  {'recria snapshot sistemas_rotinas}
  lp_Dynaset_SisRot_Montar;

    {'recria SnapShot usuario}
    lp_SnapShot_Usu_Monta;

    cboNomeUsuario.ItemIndex := -1;
    lstGrupoPert.Clear;
    lstGrupoDisp.Clear;

    {' Monta Combo com nome do usu�rio}
    lp_ComboNome_Montar;

    dmDados.gsRetorno := dmDados.Primeiro(liSsSisGrup);

    liColNome_Grupo := IOPTR_NOPOINTER;

    while (not dmDados.Status(liSsSisGrup, IOSTATUS_EOF)) do
    begin
        lsSist_Grupo := dmDados.ValorColuna(liSsSisGrup, 'Nome_Grupo', liColNome_Grupo);
        lstGrupoDisp.Items.add(lsSist_Grupo);

        if (dmDados.Proximo(liSsSisGrup) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
    end;

    lbAlterOut := False;

    lsNomeAreaDesfaz := VAZIO;

    btnSalvar.Enabled := False;
    btnExcluir.Enabled := False;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.FormClose       fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    {' Verifica se teve altera��o antes de mudar de registro}
    {' Indica que n�o cancelou a opera��o}
    lbCancel := False;
    lp_Usuario_GravAutomatica;
    { se cancelou a opera��o}
    If lbCancel = True Then
      Action := caNone
    else
    begin
      { fecha todas as conexoes}
      dmDados.FecharConexao(liDsUsuario_Grupo);
      dmDados.FecharConexao(liDsUsuario);
      dmDados.FecharConexao(liSsUsuario);
      dmDados.FecharConexao(liSsSisGrup);
      dmDados.FecharConexao(liDsSisSubRot);
      dmDados.FecharConexao(liDsSisRot);

    end;

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Dynaset_SisSubRot_Montar       Preenche SnapShot de Grupo
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Dynaset_SisSubRot_Montar;
var
  lvSelect: Variant;

begin

    lvSelect := dmDados.SqlVersao('SE_0005', gaParm);

    {'Recria snapshot}
    dmDados.FecharConexao(liDsSisSubRot);

    liDsSisSubRot := dmDados.ExecutarSelect(lvSelect);

end;

{*-----------------------------------------------------------------------------
 *  TdlgUsuario.lp_Dynaset_SisRot_Montar       Preenche SnapShot de Grupo
 *
 *-----------------------------------------------------------------------------}
procedure TdlgUsuario.lp_Dynaset_SisRot_Montar;
var
  lvSelect: Variant;

begin
               
    lvSelect := dmDados.SqlVersao('SE_0008', gaParm);

    {'Recria snapshot}
    dmDados.FecharConexao(liDsSisRot);

    liDsSisRot := dmDados.ExecutarSelect(lvSelect);

end;

end.
