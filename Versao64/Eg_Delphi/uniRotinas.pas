unit uniRotinas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls,
  uniConst;

const
  INICIO_PROCESSO = '00000';
  FINAL_PROCESSO  = '99999';
  
type
  TformRotinas = class(TForm)
    pnlRotinas: TPanel;
    trvRot: TTreeView;
    btnExecutar: TButton;
    btnFechar: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure trvRotClick(Sender: TObject);
    procedure trvRotKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnExecutarClick(Sender: TObject);
  private
    { Private declarations }
    mrProcessos: array[0..MAX_NUM_PROCESSOS] of TumProcesso;
    msCod_Processo: String;

    function lf_PosicionaRot(var piConta: Integer): Boolean;
    function lp_NodePath(Node: TTreeNode): String;

  public
    { Public declarations }
  end;

var
  formRotinas: TformRotinas;

implementation

uses uniDados;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TformRotinas.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformRotinas.FormShow(Sender: TObject);
var
  lsout_SubRot: String;
  lsString_Aux: String;
  lsout_Desc: String;
  trnNode: TTreeNode;
  lvSelect: Variant;
  liConexProc: Integer;
  liConta: Integer;

  liIndColCod_SubRotina       : Integer;
  liIndColDescricao_SubRotina : Integer;
  liIndColNomePainel          : Integer;
  liIndColGuardaHistorico     : Integer;
  liIndColTabelaHistorico     : Integer;

begin

  try
    dmDados.giStatus := STATUS_OK;

    {'Vamos aguardar...}
    Screen.Cursor := crHOURGLASS;

    trvRot.Items.Clear;

    grProcessos.bValor := False;
    grProcessos.sCod_Processo:= VAZIO;
    grProcessos.sDescricao:= VAZIO;
    grProcessos.sNomePainel:= VAZIO;
    grProcessos.bGuardaHistorico:= False;
    grProcessos.sTabelaHistorico:= VAZIO;

    { limpa o vetor de relatorios}
    for liConta := 0 to MAX_NUM_PROCESSOS do
    begin
      mrProcessos[liConta].bValor := False;
      mrProcessos[liConta].sCod_Processo:= VAZIO;
      mrProcessos[liConta].sDescricao:= VAZIO;
      mrProcessos[liConta].sNomePainel:= VAZIO;
      mrProcessos[liConta].bGuardaHistorico:= False;
      mrProcessos[liConta].sTabelaHistorico:= VAZIO;
    end;

    liIndColCod_SubRotina       := IOPTR_NOPOINTER;
    liIndColDescricao_SubRotina := IOPTR_NOPOINTER;
    liIndColNomePainel          := IOPTR_NOPOINTER;
    liIndColGuardaHistorico     := IOPTR_NOPOINTER;
    liIndColTabelaHistorico     := IOPTR_NOPOINTER;

    trvRot.Items.Clear;

    {'cria snapshot}
    gaParm[0] := Copy(gsRtn_Atual,1,3) + INICIO_PROCESSO;
    gaParm[1] := Copy(gsRtn_Atual,1,3) + FINAL_PROCESSO;
    gaParm[2] := IntToStr(giNumGrupo);
    lvSelect := dmDados.SqlVersao('ROT_0001', gaParm);
    liConexProc := dmDados.ExecutarSelect(lvSelect);

    liConta := 0;
    while not dmDados.Status(liConexProc, IOSTATUS_EOF) do
    begin

      lsout_SubRot := dmDados.ValorColuna(liConexProc, 'Cod_SubRotina', liIndColCod_SubRotina);
      {'Descri��o da Rotina}
      lsString_Aux := dmDados.ValorColuna(liConexProc, 'Descricao_Processo', liIndColDescricao_SubRotina);
      {'concatena c�digo e descri��o}
      lsout_Desc := Trim(lsString_Aux)+ ' (' + Trim(lsout_SubRot) + ')';

      trnNode := trvRot.Items.Add(nil,lsout_Desc);

      mrProcessos[liConta].bValor := True;
      mrProcessos[liConta].sCod_Processo:= lsout_SubRot;
      mrProcessos[liConta].sDescricao:= lsString_Aux;
      mrProcessos[liConta].sNomePainel:= dmDados.ValorColuna(liConexProc, 'Nome_Painel', liIndColNomePainel);
      mrProcessos[liConta].bGuardaHistorico:= dmDados.ValorColuna(liConexProc, 'Guarda_Historico', liIndColGuardaHistorico);
      mrProcessos[liConta].sTabelaHistorico:= dmDados.ValorColuna(liConexProc, 'Tabela_Historico', liIndColTabelaHistorico);
      liConta := liConta + 1;

      if (dmDados.Proximo(liConexProc) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
    end;

    { fecha a conexao}
    dmDados.FecharConexao(liConexProc);

    { se der, joga o focus na lista}
    if trvRot.CanFocus then
    begin
      trvRot.SetFocus;
      { se tem item}
      if trvRot.Items.Count > 0 then
      begin
        trvRot.Selected := trvRot.Items[0];
        trvRotClick(Sender);
      end;
    end;

    { se nao tiver itens}
    if trvRot.Items.Count <= 0 then
      btnExecutar.Enabled := False;

    {'Retorna o ponteiro padr�o.}
    Screen.Cursor := crDEFAULT;
  except
    dmDados.ErroTratar('FormShow - uniRotinas.Pas');
  End;

end;

{*-----------------------------------------------------------------------------
 *  TformRotinas.btnFecharClick       Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformRotinas.btnFecharClick(Sender: TObject);
begin
  formRotinas.Close;
end;

{*-----------------------------------------------------------------------------
 *  TformRotinas.trvRelClick       Muda a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformRotinas.trvRotClick(Sender: TObject);
begin

    {' Se n�o tem item para ser selecionado}
    if trvRot.Selected = nil then
        Exit;

    If not trvRot.Selected.HasChildren Then
        btnExecutar.Enabled := True
    Else
        btnExecutar.Enabled := False;

end;

{*-----------------------------------------------------------------------------
 *  TformRotinas.trvRelKeyUp       Atualiza a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformRotinas.trvRotKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    Case Key of
      KEY_UP:
        trvRotClick(Sender);
      KEY_DOWN:
        trvRotClick(Sender);
    End;

end;

{*-----------------------------------------------------------------------------
 *  TformRotinas.lf_PosicionaRel       Posiciona o vetor no relatorio escolhido
 *
 *-----------------------------------------------------------------------------}
function TformRotinas.lf_PosicionaRot(var piConta: Integer): Boolean;
var
  lsCampo: String;
begin

  { nao achou}
  Result := False;

  lsCampo := lp_NodePath(trvRot.Selected);
  msCod_Processo := Copy(lsCampo, (Length(lsCampo) - 8), 8);

  piConta := 0;

  { enquanto tiver processos}
  While mrProcessos[piConta].bValor do
  begin
    { se for o processo escolhido}
    if mrProcessos[piConta].sCod_Processo = msCod_Processo then
    begin
      Result := True;
      Exit;
    end
    else
      piConta := piConta + 1;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformRotinas.lp_NodePath      Para pegar o nome completo
 *                             Ex: "Item\SubItem\SubSubItem"
 *-----------------------------------------------------------------------------}
function TformRotinas.lp_NodePath(Node: TTreeNode): String;
begin
  if Node.Parent = nil then
    Result := Node.Text
  else
    Result := lp_NodePath(Node.Parent) + '\' + Node.Text;
end;

{*-----------------------------------------------------------------------------
 *  TformRotinas.btnExecutarClick      Executa a rotina
 *
 *-----------------------------------------------------------------------------}
procedure TformRotinas.btnExecutarClick(Sender: TObject);
var
  liConta: Integer;
begin

  { posiciona no processo}
  if lf_PosicionaRot(liConta) then
  begin
    { Guarda no registro generico}
    grProcessos := mrProcessos[liConta];
    formRotinas.Close;
  end;

end;


end.
