unit uniDatas;

{***********************************************************************
 *            INTERFACE
 ***********************************************************************}
interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, ComCtrls;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const

  { Erros de consist�ncia em Datas}
  DTERR_NUMCARAC = 1;    {mais de 8 caracteres num�ricos}
  DTERR_BARRAS = 2;      {n�mero de barras diferente de 2}
  DTERR_NAONUMERICA = 3; {caracteres diferentes de n�meros e barras}
  DTERR_FORMATO = 4;     {n�o foi poss�vel identificar os componentes dia, m�s e ano}
  DTERR_ANO = 5;         {ano deve ter 2 ou 4 d�gitos}
  DTERR_MES = 6;         {m�s n�o est� entre 1 e 12}
  DTERR_DIA = 7;         {dia n�o existe para o m�s informado}
  DTERR_VAZIA = 8;       {data n�o informada}

{ Erros de consist�ncia em Refer�ncias}
  RFERR_NUMCARAC = 9;     {mais de 6 caracteres num�ricos}
  RFERR_BARRAS = 10;      {n�mero de barras diferente de 1}
  RFERR_NAONUMERICA = 11; {caracteres diferentes de n�meros e barras}
  RFERR_FORMATO = 12;     {n�o foi poss�vel identificar os componentes dia, m�s e ano}
  RFERR_ANO = 13;         {ano deve ter 2 ou 4 d�gitos}
  RFERR_MES = 14;         {m�s n�o est� entre 1 e 12}
  RFERR_VAZIA = 15;       {refer�ncia n�o informada}

{ Par�metros para a fun��o que valida Datas e Refer�ncias}
  VALIDA_DATA = 1;
  VALIDA_REF = 2;

{*======================================================================
 *            RECORDS
 *======================================================================}

type
 {*======================================================================
 *            CLASSES
 *======================================================================}
  TuntDatas = class(TObject)
  private
    {  strings utilizadas para montar valor por extenso }

  public
    { Public declarations }

    procedure gp_Datas_Inicializar;
    procedure gp_Datas_Finalizar;
    function gf_Data_Valida(piTipo: Integer; psDataRef: String): Integer;
    function gf_Data_ValidaHora(psHoraRef: String): Integer;
    function gf_Data_RemoveCaracs(lsData: String; lsCaracs: String): String;

end;

{*======================================================================
 *            VARIAVEIS GLOBAIS PUBLICAS
 *======================================================================}
var
  untDatas: TuntDatas;

implementation

uses uniConst, uniDados;

{*-----------------------------------------------------------------------------
 *  TuntDatas.gp_Digito_Inicializar
 *        Cria o objeto e inicializa variaveis
 *-----------------------------------------------------------------------------}
procedure TuntDatas.gp_Datas_Inicializar;
begin
  // cria o objeto
  untDatas := TuntDatas.Create;

end;

{*-----------------------------------------------------------------------------
 *  TuntDatas.gp_Digito_Finalizar
 *        Destroi o objeto e zera variaveis
 *-----------------------------------------------------------------------------}
procedure TuntDatas.gp_Datas_Finalizar;
begin
  // destroi objeto
  untDatas.Free;
end;

{*-----------------------------------------------------------------------------
 *  TuntDatas.gf_Data_Valida
 *        Retira as barras e "underscores" e verifica se a data ou a
 *          refer�ncia s�o validas fisicamente. Devolve True (data/ref v�lida)
 *          ou o tipo do erro encontrado (data/ref inv�lida).
 *-----------------------------------------------------------------------------}
function TuntDatas.gf_Data_Valida(piTipo: Integer; psDataRef: String): Integer;
var
  {aux. para calcular no. de barras digitadas}
  lDataSemBarras: String;
  {contador para checar se todos os d�gitos da data s�o num�ricos}
  lDigito: Integer;

  lTamOriginal: Integer; {tamanho original da data (com as barras)}
  lTamanho: Integer;     {tamanho s� da data (sem as barras)}
  lNumBarras: Integer;   {n�mero de barras encontradas na data}
  lPosBarra1: Integer;   {posi��o da primeira barra}
  lPosBarra2: Integer;   {posi��o da segunda barra}

  lDiaAux: Longint;
  lMesAux: Longint;

  lMeses: array[1..12] of Integer;    {tabela de n�mero de dias de cada m�s}
begin
  lMeses[1] := 31;
  lMeses[2] := 28;
  lMeses[3] := 31;
  lMeses[4] := 30;
  lMeses[5] := 31;
  lMeses[6] := 30;
  lMeses[7] := 31;
  lMeses[8] := 31;
  lMeses[9] := 30;
  lMeses[10] := 31;
  lMeses[11] := 30;
  lMeses[12] := 31;

  Result := -1;    {assume data v�lida}

  try
    psDataRef := Trim(psDataRef);     {retira brancos da data parametrizada}
    lPosBarra1 := Pos('/', psDataRef);
    lPosBarra2 := lPosBarra1 + Pos('/', Copy(psDataRef, lPosBarra1 + 1, Length(psDataRef)));

    {se est� validando refer�ncia}
    if piTipo = VALIDA_REF then
    begin
      if lPosBarra1 <> 0 then   {refer�ncia foi informada com barra}
        psDataRef := '01/' + psDataRef
      else                      {refer�ncia foi informada sem barra}
        psDataRef := '01' + psDataRef;
      lPosBarra1 := Pos('/', psDataRef);
      lPosBarra2 := lPosBarra1 + Pos('/', Copy(psDataRef, lPosBarra1 + 1, Length(psDataRef)));
    end;
{--------------}
{N�o trata datas vazias}
{--------------}
    if psDataRef = VAZIO then
    begin
      Result := 0;
      Exit;
    end;

{--------------}
{Verifica��o dos d�gitos}
{--------------}
    lDataSemBarras := Trim(gf_Data_RemoveCaracs(psDataRef, '/_'));  {retira barras}
    for lDigito := 1 to Length(lDataSemBarras) do  {checa todos os d�gitos}
    begin
      if (Copy(lDataSemBarras, lDigito, 1) < '0') Or (Copy(lDataSemBarras, lDigito, 1) > '9') then
      begin
        if piTipo = VALIDA_REF then
          Result := RFERR_NAONUMERICA      {n�mero inv�lido de barras data inv�lida}
        else
          Result := DTERR_NAONUMERICA;      {n�mero inv�lido de barras data inv�lida}
        Exit;              {n�o testa mais nada}
      end;
    end;

{--------------}
{Verifica��o do n�mero de d�gitos (com e sem as barras)}
{--------------}
    lTamOriginal := Length(psDataRef); {salva tam. da data com barras}
    lTamanho := Length(lDataSemBarras);  {verifica novo tamanho da data (s/barras)}
    if lTamanho > 8 then
    begin
      {n�mero inv�lido de d�gitos => data inv�lida}
      if piTipo = VALIDA_REF then
        Result := RFERR_NUMCARAC
      else
        Result := DTERR_NUMCARAC;
      Exit;              {n�o testa mais nada}
    end;

{--------------}
{Verifica��o das barras}
{--------------}
    lNumBarras := lTamOriginal - lTamanho;    {calcula n�mero de barras}
    {ou o usu�rio informa as duas barras ou n�o informa nenhuma}
    if (lNumBarras <> 0) And (lNumBarras <> 2) then
    begin
      {n�mero inv�lido de barras => data inv�lida}
      if piTipo = VALIDA_REF then
        Result := RFERR_BARRAS
      else
        Result := DTERR_BARRAS;
      Exit;              {n�o testa mais nada}
    end;

{--------------}
{Verifica��o dos formatos}
{--------------}

    {se data = d/m/aa ou dd/m/aa ou d/mm/aa ou dd/m/aaaa ou d/mm/aaaa}
    if (lTamanho = 4) Or (lTamanho = 5) Or (lTamanho = 7) then
    begin
      if lPosBarra1 <> 0 then  {informou data com barras}
      begin
        lDiaAux := StrToInt(Copy(psDataRef, 1, lPosBarra1 - 1));
        lMesAux := StrToInt(Copy(psDataRef, lPosBarra1 + 1, lPosBarra2 - lPosBarra1 - 1));
      end
      else
      begin
        {formato n�o reconhecido}
        if piTipo = VALIDA_REF then
          Result := RFERR_FORMATO
        else
          Result := DTERR_FORMATO;
        Exit;              {n�o testa mais nada}
      end;
    end
    else
    begin
      {se data = d/m/aaaa ou dd/mm/aa ou dd/mm/aaaa}
      if (lTamanho = 6) Or (lTamanho = 8) then
      begin
        if lPosBarra1 <> 0 then  {informou data com barras}
        begin
          lDiaAux := StrToInt(Copy(psDataRef, 1, lPosBarra1 - 1));
          lMesAux := StrToInt(Copy(psDataRef, lPosBarra1 + 1, lPosBarra2 - lPosBarra1 - 1));
        end
        else
        begin
          lDiaAux := StrToInt(Copy(lDataSemBarras, 1, 2));
          lMesAux := StrToInt(Copy(lDataSemBarras, 3, 2));
        end;
      end
      else
      begin
        {formato n�o reconhecido}
        if piTipo = VALIDA_REF then
          Result := RFERR_FORMATO
        else
          Result := DTERR_FORMATO;
        Exit;           {n�o testa mais nada}
      end;
    end;
{--------------}
{Verifica��o do ano}
{--------------}
    if (lPosBarra1 <> 0) And (Length(psDataRef) - lPosBarra2 <> 2) And (Length(psDataRef) - lPosBarra2 <> 4) then
    begin
      if piTipo = VALIDA_REF then
        Result := RFERR_ANO   {ano inv�lido}
      else
        Result := DTERR_ANO;   {ano inv�lido}
      Exit;           {n�o testa mais nada}
    end;

(*--------------
 Verifica��o do m�s
 --------------*)
    if (lMesAux < 1) Or (lMesAux > 12) then
    begin
      if piTipo = VALIDA_REF then
        Result := RFERR_MES   {m�s inv�lido}
      else
        Result := DTERR_MES;   {m�s inv�lido}
      Exit;           {n�o testa mais nada}
    end;

{--------------}
{Verifica��o do dia}
{--------------}
    {se est� validando refer�ncia, n�o consiste dia}
    if piTipo = VALIDA_DATA then
    begin
      {Se o valor das duas �ltimas posi��es de psDataRef � divis�vel por 4}
      {o ano � bissexto}
      if (StrToInt(Copy(psDataRef, Length(psDataRef) - 1, Length(psDataRef))) Mod 4 = 0) And (lMesAux = 2) then
      begin
        if (lDiaAux < 1) Or (lDiaAux > lMeses[lMesAux] + 1) then
        begin
          Result := DTERR_DIA;   {dia maior que permitido no m�s => data inv�lida}
          Exit;           {n�o testa mais nada}
        end;
      end
      else
      begin
        if (lDiaAux < 1) Or (lDiaAux > lMeses[lMesAux]) then
        begin
          Result := DTERR_DIA;   {dia maior que permitido no m�s => data inv�lida}
          Exit;           {n�o testa mais nada}
        end;
      end;
    end;

  except
    dmDados.ErroTratar('gf_Data_Valida - UniDatas.Pas')
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntDatas.gf_Data_ValidaHora
 *        Retira ":". Devolve True (hora/ref v�lida)
 *          ou o tipo do erro encontrado (hora/ref inv�lida).
 *-----------------------------------------------------------------------------}
function TuntDatas.gf_Data_ValidaHora(psHoraRef: String): Integer;
var
  {aux. para calcular no. de dois pontos digitados}
  lHoraSemPontos: String;
  {contador para checar se todos os d�gitos da hora s�o num�ricos}
  lDigito: Integer;

  lTamOriginal: Integer; {tamanho original da hora (com os dois pontos)}
  lTamanho: Integer;     {tamanho s� da hora (sem os dois pontos)}
  lNumPontos: Integer;   {n�mero de dois pontos encontradas na hora}
  lPosPonto1: Integer;   {posi��o do primeiro dois pontos}

  lHoraAux: Longint;
  lMinutoAux: Longint;

begin

  Result := -1;    {assume hora v�lida}

  try
    psHoraRef := Trim(psHoraRef);     {retira brancos da hora parametrizada}
    lPosPonto1 := Pos(':', psHoraRef);

{--------------}
{N�o trata horas vazias}
{--------------}
    if psHoraRef = VAZIO then
    begin
      Result := 0;
      Exit;
    end;

{--------------}
{Verifica��o dos d�gitos}
{--------------}
    lHoraSemPontos := Trim(gf_Data_RemoveCaracs(psHoraRef, ':_'));  {retira dois pontos}
    for lDigito := 1 to Length(lHoraSemPontos) do  {checa todos os d�gitos}
    begin
      if (Copy(lHoraSemPontos, lDigito, 1) < '0') Or (Copy(lHoraSemPontos, lDigito, 1) > '9') then
      begin
        Result := DTERR_NAONUMERICA;      {n�mero inv�lido de dois pontos hora inv�lida}
        Exit;              {n�o testa mais nada}
      end;
    end;

{--------------}
{Verifica��o do n�mero de d�gitos (com e sem os dois pontos)}
{--------------}
    lTamOriginal := Length(psHoraRef); {salva tam. da hora com dois pontos}
    lTamanho := Length(lHoraSemPontos);  {verifica novo tamanho da hora (s/dois pontos)}
    if lTamanho > 4 then
    begin
      {n�mero inv�lido de d�gitos => hora inv�lida}
      Result := DTERR_NUMCARAC;
      Exit;              {n�o testa mais nada}
    end;

{--------------}
{Verifica��o dos dois pontos}
{--------------}
    lNumPontos := lTamOriginal - lTamanho;    {calcula n�mero de dois pontos}
    {ou o usu�rio informa os dois pontos ou n�o informa nenhum}
    if (lNumPontos <> 0) And (lNumPontos <> 1) then
    begin
      {n�mero inv�lido de dois pontos => hora inv�lida}
      Result := DTERR_BARRAS;
      Exit;              {n�o testa mais nada}
    end;

{--------------}
{Verifica��o dos formatos}
{--------------}

    {se hora = h:m ou hh:m ou h:mm}
    if (lTamanho = 2) Or (lTamanho = 3) then
    begin
      if lPosPonto1 <> 0 then  {informou hora com dois pontos}
      begin
        lHoraAux := StrToInt(Copy(psHoraRef, 1, lPosPonto1 - 1));
        lMinutoAux := StrToInt(Copy(psHoraRef, lPosPonto1 + 1, Length(psHoraRef)));
      end
      else
      begin
        {formato n�o reconhecido}
        Result := DTERR_FORMATO;
        Exit;              {n�o testa mais nada}
      end;
    end
    else
    begin
      {se hora = hh:mm}
      if (lTamanho = 4) then
      begin
        if lPosPonto1 <> 0 then  {informou hora com dois pontos}
        begin
          lHoraAux := StrToInt(Copy(psHoraRef, 1, lPosPonto1 - 1));
          lMinutoAux := StrToInt(Copy(psHoraRef, lPosPonto1 + 1, Length(psHoraRef)));
        end
        else
        begin
          lHoraAux := StrToInt(Copy(lHoraSemPontos, 1, 2));
          lMinutoAux := StrToInt(Copy(lHoraSemPontos, 3, 2));
        end;
      end
      else
      begin
        {formato n�o reconhecido}
        Result := DTERR_FORMATO;
        Exit;           {n�o testa mais nada}
      end;
    end;

(*--------------
 Verifica��o do minuto
 --------------*)
    if (lMinutoAux < 0) Or (lMinutoAux > 59) then
    begin
      Result := DTERR_MES;   {minuto inv�lido}
      Exit;           {n�o testa mais nada}
    end;

{--------------}
{Verifica��o da hora}
{--------------}
    {se est� validando refer�ncia, n�o consiste hora}
    if (lHoraAux < 0) Or (lHoraAux > 23) then
    begin
      Result := DTERR_DIA;   {hora maior que permitido => hora inv�lida}
      Exit;           {n�o testa mais nada}
    end;

  except
    dmDados.ErroTratar('gf_Data_ValidaHora - UniDatas.Pas')
  end;

end;

{*-----------------------------------------------------------------------------
 *  TuntDatas.gf_Data_RemoveCaracs
 *        Procura e retira o caracter "lCaracs" da string "
 *-----------------------------------------------------------------------------}
function TuntDatas.gf_Data_RemoveCaracs(lsData: String; lsCaracs: String): String;
var
  lData_Aux: String;
  lByte_Aux: String;
  lContador: Integer;
begin

  try
    dmDados.giStatus := STATUS_OK;

    lData_Aux := VAZIO;
    for lContador := 1 To Length(lsData) do
    begin
      lByte_Aux := Copy(lsData, lContador, 1);

      if Pos(lByte_Aux, Trim(lsCaracs)) = 0 then
          lData_Aux := Trim(lData_Aux) + Trim(lByte_Aux);
    end;
    Result := Trim(lData_Aux);

  except
    dmDados.ErroTratar('gf_Data_RemoveCaracs - uniDatas.Pas');
  end;
end;

end.
