unit uniConst;

interface

{*======================================================================
 *            UNITS
 *======================================================================}
uses
  DB, Forms, Graphics;

{*======================================================================
 *            CONSTANTES
 *======================================================================}
const

  { constante para mudar senha}
  SENHA_CRIPTOGRAFAR = 'ZAQ1!"XSW2@CDE3#VFR4$BGT5%NHY6MJU7&,KI8*.LO9(;P0)/-_]}[{=+|<>:?';

  FLDSQL_TEXTO = 'TEXTO';

  FLDMENS_CODIGO = 'CODIGO';
  FLDMENS_LINHA1 = 'LINHA1';
  FLDMENS_LINHA2 = 'LINHA2';
  FLDMENS_LINHA3 = 'LINHA3';
  FLDMENS_NIVEL  = 'NIVEL';
  FLDMENS_BOTOES = 'BOTOES';
  FLDMENS_ICONE  = 'ICONE';

  MAX_NUM_TABELAS   = 46;
  MAX_NUM_ESTRUTURA = 14;
  MAX_NUM_CHAVES    = 4;
  MAX_NUM_SQL       = 70;
  MAX_NUM_PARM      = 10;
  MAX_NUM_COLUNAS   = 200;
  MAX_NUM_RELATORIOS= 200;
  MAX_NUM_FORMULAS  = 200;
  MAX_ERRO_CONSIST  = 50;
  MAX_LIST_PARAM    = 150;
  MAX_LIST_DEPEN    = 100;
  MAX_LIST_CAMPO    = 50;
  MAX_COLUMNS_INFO  = 200;
  MAX_SUB_ROTINAS   = 200;
  MAX_VETOR_REGISTRO = 20;
  MAX_NUM_PERG      = 10;
  MAX_NUM_PROCESSOS = 200;
  MAX_NUM_DYNATEMPID = 12;
  MAX_NUM_CAMPOSALTERACAO = 10;

  // Cores
  TEXTO_JANELA = clBlack;
  COR_CONSULTA = clBlue;

{ *======================================================================}
  // Contantes - NAVEG
{ *======================================================================}
  NAV_MAXCOLS = 7;

  NAV_MENOR = -1;
  NAV_IGUAL = 0;
  NAV_MAIOR = 1;

  ERRO_NAV_EXECSELECT = 12991;
  ERRO_NAV_CHAVEINVALIDA = 12992;
  ERRO_NAV_NAOINICIALIZADA = 12993;
  ERRO_NAV_POSICAOINVALIDA = 12994;

  ERROMSG_NAV_EXECSELECT = 'SQL de navega��o inv�lido';
  ERROMSG_NAV_CHAVEINVALIDA = 'Chave para a navega��o inv�lida';
  ERROMSG_NAV_NAOINICIALIZADA = 'Navegador n�o inicializado';
  ERROMSG_NAV_POSICAOINVALIDA = 'Posi��o inv�lida';
  ERROMSG_NAV_INDETERMINADO = 'ERRO INDETERMINADO';

{ *======================================================================}
  // Contantes - LOCK
{ *======================================================================}
  LOCK_RETER = 'R';
  LOCK_CHECAR = 'C';

  {' Constantes de retorno da fun��o gf_Lock_Reter}
  LOCK_ERRO                 = 0;  {'AT� LIBERAR NOVA VERS�O (31/08)}
  LOCK_RETORNO_ERRO         = 0;  {' Indica erro de runtime no uso da fun��o}
  LOCK_RETORNO_ATUALIZA     = 1;  {' Indica que o recurso pode ser acessado para atualiza��o}
  LOCK_RETORNO_CONSULTA     = 2;  {' Indica que o recurso pode ser acessado para consulta}
  LOCK_RETORNO_NAOCONSULTA  = 3;  {' Indica que o recurso n�o pode ser acessado}

  LOCK_TBL_NOME = 'LOK_Recursos';

  LOCK_FLD_NOME         = 'Nome_Usuario';
  LOCK_FLD_PROCESSO     = 'Id_Processo';
  LOCK_FLD_DATA         = 'DtHora_Retencao';
  LOCK_FLD_RECURSO      = 'Recurso';
  LOCK_FLD_TIPO_RECURSO = 'Tipo_Recurso';
  LOCK_FLD_TIPO_LOCK    = 'Tipo_Lock';
  LOCK_FLD_ROTINA       = 'Rotina';

  {' Constantes que identificam o Tipo de Lock (como o recurso deve ser retido)}
  LOCK_ATUALIZA   = 1;
  LOCK_CONSULTA   = 2;
  LOCK_EXCLUSIVO  = 3;

{ *======================================================================}
  // Constantes -
{ *======================================================================}
  { codigo das rotinas}
  ROT_CONSULTA  = 'NEC00000';
  ROT_RELATORIO = 'REL00000';
  ROT_ROTINA    = 'ROT00000';
  ROT_SEGURANCA = 'SEG00000';
  {Teclado}

  KEY_UP = 38;
  KEY_DOWN = 40;
  KEY_ESCAPE = #27;
  KEY_ENTER = #13;
  KEY_BACK = #8;
  KEY_PRIOR = 33;    {'PgUp}
  KEY_NEXT = 34;     {'PgDn}
  KEY_END = 35;
  KEY_HOME = 36;
  KEY_D = 68;
  KEY_C = 67;

//  TAB_TIPOUSU = 'TIPOUSU';

  VAZIO = '';
  VAZIO_DATA = '  /  /    ';
  ASPAS = '"';
  SEPARADOR = ';';
  
{ *======================================================================}
  // Constantes - DADOS
{ *======================================================================}
  STATUS_OK = 0;

  IOPTR_NOPOINTER = -9999;

  ERRO_CODIGODEFAULT  = 1500;   { Valor default de erro para a ErroTratar }
  ERRO_NIVELFATAL     = '1';    { N�vel fatal de erro }
  ERRO_SENHAINVALIDA = '##SENHAINVALIDA';

  { Constantes para retorno das rotinas }
  IORET_OK      = '##OK';
  IORET_FAIL    = '##FAIL';
  IORET_NOROWS  = '##NOROWS';
  IORET_BOF     = '##BOF';
  IORET_EOF     = '##EOF';
  IORET_NOMATCH = '##NOMATCH';

  { Constantes para valor de coluna }
(*  IOVAL_NULL    = '##IO_NULL'; *)
  IOVAL_NOROWS  = '##IO_NOROWS';
  IOVAL_NOVALUE = '##IO_NOVALUE';

  { C�digos de Erros }
  IOERR_NOCURRENTRECORD     = 1000;
(*  IOERR_INVALIDCOLUMNNAME   = 1001; *)
  IOERR_INSERTSEMSETMODE    = 1002;
(*  IOERR_TABLEALREADYEXISTS  = 3012; *)
(*  IOERR_INDEXNOTNECESSARY   = '-1408'; *)

  { Contantes para _SETMODE() }
  IOMODE_EDIT   = 0;
  IOMODE_INSERT = 1;

  { Constantes para status da conex�o }
  IOSTATUS_EOF      = 1;
  IOSTATUS_NOMATCH  = 2;
  IOSTATUS_NOROWS   = 0;

  CTEDB_DATE_PREFIX = 'CV_';
(*  CTEDB_ORDERBY     = 'ORDER BY'; *)
(*  CTEDB_GROUPBY     = 'GROUP BY'; *)
(*  CTEDB_HAVING      = 'HAVING'; *)
(*  CTEDB_FORUPDATE   = 'FOR UPDATE'; *)

//  CTEDB_DATE_INI     = 'TO_DATE(''';
//  CTEDB_DATE_FIM     = ''',''MM/DD/YYYY'')';
  CTEDB_DATE_INI     = '''';
  CTEDB_DATE_FIM     = '''';

(*  TO_DATE('22/10/1997 09:56:02','DD/MM/YYYY HH24:MI:SS') *)
//  CTEDB_DATETIME_INI = 'TO_DATE(''';
//  CTEDB_DATETIME_FIM = ''',''MM/DD/YYYY HH24:MI:SS'')';
  CTEDB_DATETIME_INI = '''' ;
  CTEDB_DATETIME_FIM = '''' ;

  CTEDB_VALUES_INI   = 'VALUES (';
  CTEDB_VALUES_FIM   = ')';
  CteDB_MONEY_INI    = VAZIO;
  CteDB_MONEY_FIM    = VAZIO;
  CTEDB_LIKETODOS    = '*';
  CTEDB_LIKETODOSORCL= '%';
  CTEDB_LIKECARACTER = '?';
  CTEDB_LIKEDIGITO   = '#';
  CTEDB_ALIAS        = ' AS ';
//  CTEDB_ALIAS        = ' ';
  CTEDB_CONCAT       = ' + ';
  CTEDB_BINARY_INI   = '''';
  CTEDB_BINARY_FIM   = '''';
  CTEDB_TOP_10       = ' TOP 10 ';
  CTEDB_TOP_1        = ' TOP 1 ';

  { Bit-Mask para passagem de par�metros no ColumnsInfo() }
  IOCOL_NAME        = $1;
  IOCOL_TYPE        = $2;
  IOCOL_VALUE       = $4;
  IOCOL_SIZE        = $8;
  IOCOL_ORDINAL     = $10;
  IOCOL_VALPOINTER  = $20;
  IOCOL_ALL         = $FF;

  { Constantes para flag de convers�o de decimais do BD }
  IOFLAG_VALORDOBANCO   = 0;
  IOFLAG_VALORPARABANCO = 1;

  { Constantes para Tipo de Coluna padr�o no gColunas }
  IOTYPE_TEXT     = ftString;
  IOTYPE_INTEGER  = ftInteger;
  IOTYPE_SMALLINT = ftSmallint;
  IOTYPE_DATETIME = ftDateTime;
  IOTYPE_DATE     = ftDate;
  IOTYPE_CURRENCY = ftCurrency;
  IOTYPE_MEMO     = ftMemo;
  IOTYPE_BLOB     = ftBlob;
  IOTYPE_BYTES    = ftBytes;
  IOTYPE_FLOAT    = ftFloat;
  IOTYPE_AUTOINC  = ftAutoInc;

  { Constantes para flag da rotina FIND }
  IOFLAG_FIRST  = True;
  IOFLAG_NEXT   = False;

  IOFLAG_GET = 0;

  CALC_MAX_CHAVES = 6;    { N�mero m�ximo de chaves (redim 0 to 6) }

{ *======================================================================}
  // Constantes - ERROS
{ *======================================================================}
  {'chamada pelo bot�o/menu consist�ncia (qualquer aplica��o)}
  ERRO_MODO_NORMAL = 0;
  {'chamada pelas aplica��es de cadastramento em geral (ex:funcion�rio)}
  ERRO_MODO_CADASTRAIS = 1;
  {'chamada pela aplica��o de par�metros (ex: f�rmulas)}
  ERRO_MODO_PARAMETROS = 2;

  {'---- Retornos do di�logo de consistencia}
  ERRO_CANCELAR = 0;  {'usu�rio escolheu n�o gravar ou ocorreram somente erros cr�ticos}
  ERRO_IGNORAR = 1;   {'usu�rio escolheu gravar ignorando os erros apontados pela consist�ncia}

  {'n�mero da coluna de cada campo do dependente na janela de consistencias}
  COL_ERRO = 0;
  COL_DESCRICAO = 1;
  COL_TIPO = 2;
  COL_NIVEL = 3;

{ *======================================================================}
  // Constantes - SELECIONA
{ *======================================================================}
  {'Constantes para o GRID do SELECIONAR}
  SEL_MAXCOLS = 6;

  {' A��es no DYNASET para a montagem do GRID}
  PAGINA_FRENTE = 0;
  PAGINA_TRAS = 1;
  LINHA_FRENTE = 2;
  LINHA_TRAS = 3;
  INICIAGRID = 4;
  ULTIMA_PAGINA = 5;
  PRIMEIRA_PAGINA = 6;
  SO_ULTIMA_LINHA = 7;

  COLUNA_NAOUTILIZADA = -1;
  COLUNA_INDEFINIDA = -2;

  GRID_TRADUCAO_NENHUMA = 0;     {'Sem tradu��o de coluna}
  GRID_TRADUCAO_NIVELPAR = 1;    {'Tradu��o de coluna de origem de par�metros}
  GRID_TRADUCAO_EFETIVADA = 2;   {'Tradu��o de coluna de efetiva��o de empresa/unidade}
  GRID_TRADUCAO_CHAVE1 = 3;      {'Tradu��o de coluna de chaves de VALCONTA}
  GRID_TRADUCAO_CCUSTO = 4;      {'Tradu��o de coluna Centro de Custo}
  GRID_TRADUCAO_REFERENCIA = 5;  {'Tradu��o de coluna Refer�ncia}
  GRID_TRADUCAO_VALOR = 6;       {'Tradu��o de coluna Valor}

{ *======================================================================}
  // Constantes -
{ *======================================================================}
  CALC_SEPARADOR_CHAVES = '~';
  CCUSTO_VAZIO          = '~~~~~';

  { para saber se RadioButton estava marcado}
  RADIO_MARCADO     = 'SIM';
  RADIO_NAO_MARCADO = 'NAO';

  {' Mascara_Bits de edi��o (fun��o format)}
  MK_VALOR    = '##,###,###,##0.00'; {'mascara p/valores, horas e quantidades}
  MK_DATA     = 'mm/dd/yyyy';
  MK_DATATIME = 'mm/dd/yyyy hh:nn:ss';
  MK_DATATELA = 'dd/mm/yyyy';
  MK_DATATIMETELA = 'dd/mm/yyyy hh:nn:ss';

  SEPARADOR_MILHAR = '.';
                            
  {'TABELAS USADAS PARA Consistencia NUM�RICAS/ALFANUM�RICAS/ALFAB�TICAS}
  TNUMEROS  = '0123456789';
  TALFA     = ' ABCDEFGHIJKLMNOPQRSTUVXWYZ�����������';
  TALFANUM  = ' 0123456789ABCDEFGHIJKLMNOPQRSTUVXWYZ�����������';

  {' constantes para edicao}
  ED_CONSULTA = 'C';
  ED_NOVO = 'I';
  ED_ALTERAR = 'A';
  ED_EXCLUIR = 'E';
  ED_REPOSICIONAR = 'R';

  { constantes do tipo de resposta p/ as perguntas de impressao}
  RESP_DATA   = 'D';
  RESP_TEXTO  = 'T';
  RESP_MESANO = 'R';
  RESP_NUMERO = 'N';
  RESP_LISTA  = 'L';

  { constantes do tipo de alinhamento do campo}
  ALINHA_ZERO = 'Z';
  ALINHA_DIREITA = 'D';
  ALINHA_ESQUERDA = 'E';

{ *======================================================================}
  // Constantes - SELPAR
{ *======================================================================}
  { constantes para determinar se pode realizar a operacao}
  TIPO_ALTERA     = 0;
  TIPO_INCLUI     = 1;
  TIPO_CONSULTA   = 2;
  TIPO_EXCLUI     = 3;

{ *======================================================================}
  // Constantes - SEGURANCA
{ *======================================================================}
  {'Constantes relativas �s fun��es de Seguran�a}
  FUNCIONARIO = 'F';   {' Indica se Funcion�rio}
  ENCERRADO = 'E';    {' Indica Processo do usu�rio encerrado}
  ATIVO = 'A';        {' Indica Processo do usu�rio Ativo}
  CALCULO = 'C';      {' Indica que � c�lculo}
  RELATORIO = 'R';    {' Indica que � relat�rio}
  PARAMETROS = 'P';   {' Indica que � par�metros}

{ *======================================================================}
  // Constantes - TELAVAR
{ *======================================================================}
  //Tipos de a��es
  ACAO_FECHAR = 1;
  ACAO_MOVER = 2;
  ACAO_EXCLUIR = 3;
  ACAO_GRAVAR = 4;

  TV_COMANDOS = 1;                        // N�mero do primeiro comando em ordem num�rica crescente

  TV_COMANDO_CONSISTIR = 1;               // Consist�ncias
  TV_COMANDO_INCLUIR = 2;                 // Abrir tela para inclus�o de novo registro
  TV_COMANDO_ALTERAR = 3;                 // Abrir tela para alteracao de registro
  TV_COMANDO_RESSETAR = 4;                // Retornar a situa��o inicial
  TV_COMANDO_GRAVAR = 5;                  // Gravar o registro atual
  TV_COMANDO_EXCLUIR = 6;                 // Excluir o registro corrente
  TV_COMANDO_IMPRIMIR = 7;                // Preparar para impressao
  TV_COMANDO_ROTINAS = 8;                 // Preparar para execucao de rotinas
  TV_COMANDO_ROTINAS_FIM = 9;             // Finalizar a execucao de rotinas
  TV_COMANDO_CONSULTAR = 10;              // Mudar tela para consulta
  TV_COMANDO_SINCRONIZAR = 11;            // Posicionar no registro das outras telas
  TV_COMANDO_PROCURAR = 12;               // Posicionar no registro procurado
  TV_COMANDO_IR_PROXIMO = 13;             // Avan�a para o pr�ximo registro
  TV_COMANDO_IR_ANTERIOR = 14;            // Retorna para o registro anterior
  TV_COMANDO_IR_ULTIMO = 15;              // Avan�a para o �ltimo registro
  TV_COMANDO_IR_PRIMEIRO = 16;            // Retorna para o primeiro registro
  TV_COMANDO_ESCONDER_CAMPOS = 17;        //Esconde os campos listados em gpComando_MDI_Parms (Separados por ";")
  TV_COMANDO_MOSTRAR_CAMPOS = 18;         //Mostra os campos listados em gpComando_MDI_Parms (Separados por ";")
  TV_COMANDO_PEGAR_VALOR_CAMPO = 19;      //Pega o valor(gpComando_MDI_Valor) do campo listado em gpComando_MDI_Parms
  TV_COMANDO_POR_VALOR_CAMPO = 20;        //P�e o valor(gpComando_MDI_Valor) no campo listado em gpComando_MDI_Parms
  TV_COMANDO_ANEXAR_VALOR_CAMPO = 21;     //Anexa valor(gpComando_MDI_Valor) ao campo listado em gpComando_MDI_Parms
  TV_COMANDO_INSERIR_VALOR_CAMPO = 22;    //Insere valor(gpComando_MDI_Valor) no lugar selecionado do campo listado em gpComando_MDI_Parms
  TV_COMANDO_PEGAR_SELTEXT_CAMPO = 23;    //Pega o o trecho selecionado do valor(gpComando_MDI_Valor) do campo listado em gpComando_MDI_Parms
  TV_COMANDO_ATIVAR_DESATIVAR_BOTAO = 24; //gpComando_MDI_Parms � o Tag do bot�o cujo o Enabled ser� setado para gpComando_MDI_Valor
  TV_COMANDO_POR_MASCARA_CAMPO = 25;      //Muda a m�scara do Campo listado em gpComando_MDI_Parms
  TV_COMANDO_POSICIONAR_SELTEXT = 26;     //Muda qual � o texto selecionado no controle
  TV_COMANDO_PEGAR_CONSISTENCIAS_CAMPO = 27;
  TV_COMANDO_POR_CONSISTENCIAS_CAMPO = 28;

  TV_RETORNO_PARADO = 0;
  TV_RETORNO_OK = -1;
  TV_RETORNO_PROCESSANDO = -2;
  TV_RETORNO_METODO_DESCONHECIDO = -3;

  {'Spreadsheet Actions - Constantes para Spread}
  SS_ACTION_ACTIVE_CELL = 0;
  SS_ACTION_GOTO_CELL = 1;
  SS_ACTION_SELECT_BLOCK = 2;
  SS_ACTION_CLEAR = 3;
  SS_ACTION_DELETE_COL = 4;
  SS_ACTION_DELETE_ROW = 5;
  SS_ACTION_INSERT_COL = 6;
  SS_ACTION_INSERT_ROW = 7;
  SS_ACTION_LOAD_SPREAD_SHEET = 8;
  SS_ACTION_SAVE_ALL = 9;
  SS_ACTION_SAVE_VALUES = 10;
  SS_ACTION_RECALC = 11;
  SS_ACTION_CLEAR_TEXT = 12;
  SS_ACTION_PRINT = 13;
  SS_ACTION_DESELECT_BLOCK = 14;
  SS_ACTION_DSAVE = 15;
  SS_ACTION_SET_CELL_BORDER = 16;
  SS_ACTION_ADD_MULTISELBLOCK = 17;
  SS_ACTION_GET_MULTI_SELECTION = 18;
  SS_ACTION_COPY_RANGE = 19;
  SS_ACTION_MOVE_RANGE = 20;
  SS_ACTION_SWAP_RANGE = 21;
  SS_ACTION_CLIPBOARD_COPY = 22;
  SS_ACTION_CLIPBOARD_CUT = 23;
  SS_ACTION_CLIPBOARD_PASTE = 24;
  SS_ACTION_SORT = 25;
  SS_ACTION_COMBO_CLEAR = 26;
  SS_ACTION_COMBO_REMOVE = 27;
  SS_ACTION_RESET = 28;
  SS_ACTION_SEL_MODE_CLEAR = 29;
  SS_ACTION_VMODE_REFRESH = 30;
  SS_ACTION_REFRESH_BOUND = 31;
  SS_ACTION_SMARTPRINT = 32;

  {'Cell type's - Constantes para Spread}
  SS_CELL_TYPE_DATE = 0;
  SS_CELL_TYPE_EDIT = 1;
  SS_CELL_TYPE_FLOAT = 2;
  SS_CELL_TYPE_INTEGER = 3;
  SS_CELL_TYPE_PIC = 4;
  SS_CELL_TYPE_STATIC_TEXT = 5;
  SS_CELL_TYPE_TIME = 6;
  SS_CELL_TYPE_BUTTON = 7;
  SS_CELL_TYPE_COMBOBOX = 8;
  SS_CELL_TYPE_PICTURE = 9;
  SS_CELL_TYPE_CHECKBOX = 10;
  SS_CELL_TYPE_OWNER_DRAWN = 11;

  {SortKeyOrder properties settings - Constantes para Spread}
  SS_SORT_ORDER_NONE = 0;
  SS_SORT_ORDER_ASCENDING = 1;
  SS_SORT_ORDER_DESCENDING = 2;

type
 {*======================================================================
 *            RECORDS
 *======================================================================}
{ *======================================================================}
  // Records - ERROS
{ *======================================================================}
 {*----------------------------------------------------------------------
 * Estrutura de Informacao de Erro de consist�ncia
 *}
  TumErro = Record
    sCodErro:  String;
    avLiteral: array[0..2] of Variant;
  end;

{ *======================================================================}
  // Records - Rotinas
{ *======================================================================}
 {*----------------------------------------------------------------------
 * Estrutura de Informacao de Processo
 *}
  TumProcesso = Record
    bValor:  Boolean;
    sCod_Processo: String;
    sDescricao: String;
    sNomePainel: String;
    bGuardaHistorico:  Boolean;
    sTabelaHistorico: String;

  end;

{ *======================================================================}
  // Records - Imprimir
{ *======================================================================}
 {*----------------------------------------------------------------------
 * Estrutura de Informacao de Relatorios
 *}
  TumRelatorio = Record
    bValor:  Boolean;
    sCod_Ordem: String;
    sDescricao: String;
    sRel_Path: String;
    sRel_Filtro: String;
    sRel_Ordem: String;
    sRel_Stored: String;
    sPerguntas: String;
  end;

  TUmaImpPergunta = Record
    sPergunta   : String;
    sTpResposta : String;
    sTpControle : String;
    sSql        : String;
  End;

  TumaFormula = Record
    sNome: string;
    sTexto: string;
  end;

{ *======================================================================}
  // Records - NAVEG
{ *======================================================================}
  TumaChave = Record
    iNumCols                 : Integer; {' de 1 a NAV_MAXCOLS}
    sNome                    : array [1..NAV_MAXCOLS] of String;
    vValor                   : array [1..NAV_MAXCOLS] of Variant;
  end;

  TumNavegador = Record
    {' *** Info sobre a Conexao}
    iConexao                 : Integer;
    iNumCols                 : Integer; {' Se = 0 n�o foi inicializada}
    iIOPTR                   : array [1..NAV_MAXCOLS] of Integer;

    {' *** Flags para a ToolBar}
    bPrimeiro               : Boolean;
    bAnterior               : Boolean;
    bProximo                : Boolean;
    bUltimo                 : Boolean;

    bNavegavel              : Boolean; {' Se os bot�es/men�s de navega��o podem estar ativos ou n�o}

    {' *** Status}
    iUltimoErro             : Integer;
    sUltimoErroStr          : String;

    {' *** Flags de validade}
    bRegistroValido         : Boolean;

    bNoRows                 : Boolean;
    bBOF                    : Boolean;
    bEOF                    : Boolean;

    {' *** Registros importantes}
    rReg_Primeiro           : TumaChave;
    rReg_Corrente           : TumaChave;
    rReg_Ultimo             : TumaChave;
  end;

{ *======================================================================}
  // Records - PARAM
{ *======================================================================}
  TumParametro = Record
    Entidade          : String;  {' Max: 20}
    Nome_Tabela       : String;  {' Max: 25}
    Sigla_Tabela      : String;  {' Max:  3}
    Cod_Seguranca     : String;  {' Max:  8}
    Tipo_Objeto       : String;  {' Max:  1}
    bTela_Especial    : Boolean; {' Booleano convertido da nega��o de [Tela_Variavel] String*1 (S,N)}
    Coluna_Codigo     : String;  {' Max: 30}
    Mascara_Codigo    : String;  {' Max: 30}
    Alinha_Codigo     : String;  {' Max: 30}
    Alinha_Tamanho    : Integer; {' Direto}
    bCodigo_Numerico  : Boolean; {' Booleano convertido de [Codigo_Numerico] String*1 (S,N)}
    Codigo_Minimo     : Integer; {' Direto}
    Coluna_Descricao  : String;  {' Max: 30}
    Coluna_Nivel      : String;  {' Max: 30}
    Nome_Tela         : String;  {' Max:  ?}
    Indice_Tela       : Integer;  { para poder usar o comando "CASE"}
    Titulo_Tela       : String;  {' Max: 30}
    Largura_Tela      : Integer; {' Direto}
    Altura_Tela       : Integer; {' Direto}
    Rotina_Validade   : String;  {' Max: 15 ' Talvez seja eliminada}
    HelpID_Tela       : Integer; {' Coluna [HelpID]}
    Filtro            : String;
    Modulo            : String;
  end;

  TumDependente = Record
    sSigla_Tabela      : String;  {' Max:  3}
    sNumero_Tabela     : String;  {' Max:  3}
    sTabela_Dependente : String;  {' Max: 30}
    sTipo_Cadastro     : String;  {' Max:  1}
    sTipo_Loop         : String;  {' Max:  1}
    sNome_Lista        : String;  {' Max: 50}
    sCodigo_SQL        : String;  {' Max: 15}
    sNome_Tabela       : String;  {' Max: 50}
    sColuna_Codigo     : String;  {' Max:100}
    sValor_Codigo1     : String;  {' Max: 50}
    sValor_Codigo2     : String;  {' Max: 50}
    sValor_Codigo3     : String;  {' Max: 50}
    sCampo_Ordem       : String;  {' Max: 50}
    sAtualizar_Campos  : String;  {' Max:100}
  end;

  TumCampo = Record
    sSigla_Tabela      : String;  {' Max:  3 - Sigla da tabela no zTabelas}
    iOrdem             : Integer; {' Max:  2 - Ordem dos campos}
    sNome_Campo        : String;  {' Max: 50 - Nome do campo na tabela}
    sTipo_Campo        : String;  {' Max:  1 - Tipo do campo na tabela}
    iTamanho           : Integer; {' Max:  4 - Tamanho do campo na tabela, qunado for texto}
    sFormato           : String;  {' Max: 50 - Formato para mostrar na tela}
    sDescricao         : String;  {' Max: 50 - Descricao para mostrar no label}
    sNome_Controle     : String;  {' Max: 50 - Nome do controle na tela}
    sTipo_Controle     : String;  {' Max:  1 - Tipo do controle na tela}
    sMascara           : String;  {' Max: 50 - Mascara para digitacao, quando for MaskEdit}
    sCombo_Tabela      : String;  {' Max: 50 - Nome da tabela a preencher o combo}
    sCombo_Filtro      : String;  {' Max: 50 - Filtro na tabela a preencher o combo}
    sEdita_Inclui      : String;  {' Max: 50 - Se edita o controle na inclusao}
    sEdita_Altera      : String;  {' Max: 50 - Se edita o controle na alteracao}
    sValidade          : String;  {' Max: 50}
    sDefault           : String;  {' Max: 50 - Valor default do controle para inclusao}
    sFuncao            : String;  {' Max: 50}
    sVazio             : String;  {' Max:  1 - Se nao permite vazio}
    sAutomatico        : String;  {' Max:  1 - Se eh codigo automatico}
    sGrava             : String;  {' Max:  1 - Se nao eh para gravar a coluna}
    sColuna_Codigo     : String;  {' Max:  1 - Se eh coluna codigo e qual a posicao}
    sAlinha            : String;  {' Max:  1 - Se eh para alinhar o campo Z-com zeros, E-esquerda, D-direita }
    sUnico             : String;  {' Max:  1 - Se nao pode ter duplicidade}
  end;

{ *======================================================================}
  // Records - TELAVAR
{ *======================================================================}
  {' Defini��o de uma Tela Vari�vel}
  TumaTelaVar = Record
    P: TumParametro;              {' Dados b�sicos}
    {'Meta-Dados}
    bTextBoxNonBold: Integer;    {' Se os textboxes devem ser usados com letra normal ao inv�s de negrito}
    Visao: Integer;              {' Qual a vis�o de sele��o}
    {' Meta-Dados Visuais
    ' Dados}
    DynaID: Integer;             {' N�mero de acesso ao dynaset segundo IOGLPOOL}
    DynaPesquisaID: Integer;     {' N�mero de acesso ao dynaset de pesquisa (Subir)}
    DynaTempID: Array[1..MAX_NUM_DYNATEMPID] of Integer;         {' N�mero de acesso tempor�rio}

    ComplementoCaption: String;
    Cod_Valor: String;           {' Valor corrente do campo de AutoBusca}
    Cod_Visao: String;           {' Tipo da vis�o "D" = Defini��o "V" = Validade}
    Acesso_Nivel2: Boolean;      {' Se o usu�rio pode alterar itens em n�vel empresa para uma vis�o validade}
    Acesso_Nivel3: Boolean;      {' Se o usu�rio pode alterar itens em n�vel instala��o para uma vis�o validade}
    {' Suporte a um campo virtual}
    Campo_Virtual: Integer;
    Campo_Offset: Integer;
    Campo_Container: Integer;
    {' Status}
    Reposicionando: Boolean;
    Recurso_Retido: Boolean;     {' Se o recurso de altera��o do par�metro foi retido no in�cio}
    Bloqueado: Boolean;          {' Se os campos n�o podem ser alterados (como em consulta)}
    Set_Alterado: Integer;       {' Se houve inclus�o ou dele��o desde o �ltimo status}

    {' Toolbar/Menus/Navega��o}
    Alterado        : Boolean;    {' Se algum campo foi alterado pelo usu�rio}
    Pode_Encadear   : Boolean;    {' Se o campo corrente � referencia a outro par�metro}

    Nav             : TumNavegador;

    {' Limites}
    NumSeparadores: Integer;
    NumBotoes: Integer;

    {' Comando}
    Comando_MDI: String;     {' Comando/Status corrente "I" = Inclus�o / "A" = Altera��o / "C" = Consulta}
  end;

{ *======================================================================}
  // Record - SELECIONA
{ *======================================================================}
  TumaGrid_Coluna = Record
    Nome         : String;          {'Nome da coluna na conex�o}
    Ordinal      : Integer;         {'�ndice da coluna na conex�o}
    Numero       : Integer;         {'Pointer da coluna no gColunas}
    Traducao     : Integer;         {'Tipo de tradu��o para a coluna}
  end;

  TumGridMontar_Info = Record
    Conexao          : Integer;         {'N�mero da conex�o com recordset p/ grid}
    ChaveProcura     : Variant;         {'Literal a ser pesquisada no recordset}
    ColunaProcura    : TumaGrid_Coluna; { 'Nome da coluna onde deve ser realizada a procura}
    MudouProcura     : Boolean;         {'Indica se a literal de pesquisa foi alterada desde a �ltima procura}
    Acao             : Variant;         {'Acao a ser tomada no recordset (pr�x.p�g.,p�g.anterior,etc.)}
    LinhasEmUso      : Integer;         {'N�mero de linhas de dados montadas no grid}
    ChvPrimeiraLinha : array [0..SEL_MAXCOLS] of Variant;         {'Chave �nica da primeira linha da Conex�o}
    ChvUltimaLinha   : array [0..SEL_MAXCOLS] of Variant;         {'Chave �nica da ultima linha da Conex�o}
    SprPrimeiraLinha : array [0..SEL_MAXCOLS] of Variant;         {'Chave �nica da primeira linha do Spread}
    SprUltimaLinha   : array [0..SEL_MAXCOLS] of Variant;         {'Chave �nica da ultima linha do Spread}
    PrimeiraPagina   : Boolean;         {'Indica se est� na primeira p�gina do grid}
    UltimaPagina     : Boolean;         {'Indica se est� na �ltima p�gina do grid}
    ColunaOrdem      : Integer;         {'Indica qual a coluna de ordenacao}
  end;

  TumId_Linhas = Record
    Valor: array[0..SEL_MAXCOLS] of Variant;         {'Conte�do de cada Id}
  end;

{ *======================================================================}
  // Record - DADOS
{ *======================================================================}
  Tuma_PermRotina = Record
    Cod_SubRotina: String;   {' C�digo da Subrotina}
    Trata_Remun: String;       {' Trata Remuneracao (N=N�o trata, P=parcial, T=total)}
    Consulta: Boolean;          {' Indica se consulta (N= N�o consulta, S= consulta)}
    Inclui: Boolean;            {' Indica se Inclui (N= N�o Inclui, S= Inclui)}
    Exclui: Boolean;            {' Indica se Exclui (N= N�o Exclui, S= Exclui)}
    Altera: Boolean;            {' Indica se Altera (N= N�o Altera, S= Altera)}
    Executa: Boolean;           {' Indica se executa (N= N�o executa, S= executa)}
    Emite: Boolean;             {' Indica se emite relat�rio (0= N�o emite, 1= emite)}
  end;

 {*======================================================================
 *            CLASSES
 *======================================================================}
var
{ *======================================================================}
  // Variaveis - Processos
{ *======================================================================}
    grProcessos: TumProcesso;
    gbProcessoOK: Boolean;

{ *======================================================================}
  // Variaveis - Impressao
{ *======================================================================}
    grImpPergunta: TUmaImpPergunta;
    gvImpResposta: Variant;
    gsImpTituloRelatorio: String;

{ *======================================================================}
  // Variaveis - ERROS
{ *======================================================================}
    giNumErros: Integer;
    gaErros: array[0..MAX_ERRO_CONSIST] of TumErro;
    {'---- Controle do modo de opera��o do di�logo}
    giErros_Modo: Integer;
    {'---- Retornos do di�logo de consistencia}
    giErros_Ignorar: Integer;
    {'---- Indica que ocorreram erros cr�ticos e que, portanto, n�o podem ser gravados}
    gbErros_Criticos: Boolean;
{ *======================================================================}
  // Variaveis - MENSAGEM
{ *======================================================================}
    {'---- 'snapshot com todas as mensagens da tabela "zMensagem"}
    giSsMens: Integer;

{ *======================================================================}
  // Variaveis - SELPAR
{ *======================================================================}
    {' Vari�veis para passagem/recep��o de par�metros com os forms}
    gsCod_Parametro:    String;
    gsCod_Valor:        String;
    gsDesc_Valor:       String;
    gpConexao:          Integer;
    gpFiltro:           String;
    gpProcurar:         String;
    gpMascara:          String;
    gpAlinha:           String;
    gpAlinhaTamanho:    Integer;

{ *======================================================================}
  // Variaveis - TELAVR
{ *======================================================================}
    {' os disparadores de uma tela vari�vel passam os dados nesta estrutura}
    grTelaVar: TumaTelaVar;
    //' Forma de Opera��o da Consist�ncia: True = Sem consist�ncia autom�tica
    gbSem_Consistir: Boolean;

    // Defini��o da vari�vel de passagem para gfTelaVar_Executar
    giComando_MDI: Integer;
    gsComando_MDI_Parms: String;
    // gvComando_MDI_Valor: Variant;

    // Para interliga��o com a MDI e os forms auxiliares
    grParam_TelaVar: TumaTelaVar;
    gfrmTV: TForm;  // Apontador para o Form corrente

{ *======================================================================}
  // Variaveis - DADOS
{ *======================================================================}
    { variaveis de controle de permissoes}
    giNumSubRotinas:  Integer;
    gaPermRotinas:    array[1..MAX_SUB_ROTINAS] of Tuma_PermRotina;
    gbPodeCadastrar:  Boolean;
    gbPodeEmitir:     Boolean;
    gbPodeExecutar:   Boolean;
    gaParm: array[0..9] of Variant; 

{ *======================================================================}
  // Variaveis - PARAM
{ *======================================================================}
    gaParam_Colunas:    array [0..0] of String;
    gaLista_Parametros: array [1..MAX_LIST_PARAM] of TumParametro;

    gaLista_Dependentes: array [1..MAX_LIST_DEPEN] of TumDependente;
    gaLista_Campos:      array [1..MAX_LIST_PARAM, 1..MAX_LIST_CAMPO] of TumCampo;

    gbRemontaGrid:      Boolean;

    { variaveis globais de controle de ususario}
    gsNomeUsuario:  String;
    glCodigoUsuario: LongInt;
    giId_Processo:  Integer;
    gbAdmin:        Boolean;
    giNumGrupo:     Integer;

    {' C�digo da rotina em uso}
    gsRtn_Atual: String;
    gsDBDir         :  String;
    gsDbTipo        :  String;
    gsDBNome        :  String;
    gsDbUserName    :  String;
    gsDbPassWord    :  String;
    gsDbDriver      :  String;
    gsDbConnect     :  String;

    gsMDICaption    :  String;
    gsMDISubCaption :  String;
    gsLblVersao     :  String;
    gsLblEmpresa    :  String;
    gsLblSistema    :  String;

    { Para controle da tela de selecionar cadastros}
    gbCodigo: Boolean;

{ *======================================================================}
  // Variaveis - Processos
{ *======================================================================}
    gsBUFFER:   string;
    glCodigoLayout: LongInt;

implementation

end.
