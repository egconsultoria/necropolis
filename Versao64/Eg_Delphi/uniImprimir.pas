unit uniImprimir;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, OleCtrls, Crystal_TLB, DB, DBTables, Variants,
  uniConst, UCrpeClasses, UCrpe32;

type
  TformImprimir = class(TForm)
    pnlImprimir: TPanel;
    btnImprimir: TButton;
    btnVisualizar: TButton;
    btnFechar: TButton;
    trvRel: TTreeView;
    rptRelatorio: TCrpe;
    procedure FormShow(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure trvRelClick(Sender: TObject);
    procedure trvRelKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnImprimirClick(Sender: TObject);
    procedure btnVisualizarClick(Sender: TObject);
  private
    { Private declarations }
    mrRelatorio: array[0..MAX_NUM_RELATORIOS] of TumRelatorio;
    msCod_Ordem: String;
    mrFormulas: array[0..MAX_NUM_FORMULAS] of TumaFormula;

    function lf_PosicionaRel(var piConta: Integer): Boolean;
    function lp_NodePath(Node: TTreeNode): String;
    procedure lp_Formulas_Processar;
    procedure lp_Stored_Processar(piConta: Integer);

  public
    { Public declarations }
  end;

var
  formImprimir: TformImprimir;

implementation

uses uniDados, uniPergunta, uniGlobal, uniFuncoes;

{$R *.DFM}

{*-----------------------------------------------------------------------------
 *  TformImprimir.FormShow       Prepara a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.FormShow(Sender: TObject);
var
  lsRot: String;
  lsModulo: String;
  lsModuloAnt: String;
  lsRelatorio: String;
  lsRelatorioAnt: String;
  lsCod_Ordem: String;
  lsString_Aux: String;
  lsout_Desc: String;
  trnNodeModulo: TTreeNode;
  trnNodeRel: TTreeNode;
  lvSelect: Variant;
  liConexRel: Integer;
  liConta: Integer;

  liIndColCod_Rotina          : Integer;
  liIndColCod_SubRotina       : Integer;
  liIndColDescricao_SubRotina : Integer;
  liIndColCod_Ordem           : Integer;
  liIndColDescricao_Ordem     : Integer;
  liIndColRel_Path            : Integer;
  liIndColRel_Filtro          : Integer;
  liIndColRel_Ordem           : Integer;
  liIndColRel_Stored          : Integer;
  liIndColPerguntas           : Integer;
  liIndColDescricao_Modulo    : Integer;
begin

  try
    dmDados.giStatus := STATUS_OK;

    {'Vamos aguardar...}
    Screen.Cursor := crHOURGLASS;

    { limpa o vetor de relatorios}
    for liConta := 0 to MAX_NUM_RELATORIOS do
    begin
      mrRelatorio[liConta].bValor := False;
      mrRelatorio[liConta].sCod_Ordem:= VAZIO;
      mrRelatorio[liConta].sDescricao:= VAZIO;
      mrRelatorio[liConta].sRel_Path:= VAZIO;
      mrRelatorio[liConta].sRel_Filtro:= VAZIO;
      mrRelatorio[liConta].sRel_Ordem:= VAZIO;
      mrRelatorio[liConta].sRel_Stored:= VAZIO;
      mrRelatorio[liConta].sPerguntas:= VAZIO;
    end;

    liIndColCod_Rotina          := IOPTR_NOPOINTER;
    liIndColCod_SubRotina       := IOPTR_NOPOINTER;
    liIndColDescricao_SubRotina := IOPTR_NOPOINTER;
    liIndColCod_Ordem           := IOPTR_NOPOINTER;
    liIndColDescricao_Ordem     := IOPTR_NOPOINTER;
    liIndColRel_Path            := IOPTR_NOPOINTER;
    liIndColRel_Filtro          := IOPTR_NOPOINTER;
    liIndColRel_Ordem           := IOPTR_NOPOINTER;
    liIndColRel_Stored          := IOPTR_NOPOINTER;
    liIndColPerguntas           := IOPTR_NOPOINTER;
    liIndColDescricao_Modulo    := IOPTR_NOPOINTER;

    trvRel.Items.Clear;
    trnNodeModulo := nil;
    trnNodeRel  := nil;

    lsModulo        := VAZIO;
    lsModuloAnt     := VAZIO;
    lsRelatorio     := VAZIO;
    lsRelatorioAnt  := VAZIO;

    gaParm[0] := IntToStr(giNumGrupo);
    gaParm[1] := gsRtn_Atual;
    { se for todos os relatorios}
    If gsRtn_Atual = ROT_RELATORIO then
      lvSelect := dmDados.SqlVersao('IMP_0014', gaParm)
    else
      lvSelect := dmDados.SqlVersao('IMP_0010', gaParm);
    liConexRel := dmDados.ExecutarSelect(lvSelect);

    liConta := 0;
    while not dmDados.Status(liConexRel, IOSTATUS_EOF) do
    begin

      lsRot := dmDados.ValorColuna(liConexRel, 'Cod_Rotina', liIndColCod_Rotina);

      if lsRot = ROT_RELATORIO then
      begin

        lsModulo := dmDados.ValorColuna(liConexRel, 'Descricao_Modulo', liIndColDescricao_Modulo);

        if lsModulo <> lsModuloAnt then
        begin
          lsModuloAnt := lsModulo;
          trnNodeModulo := trvRel.Items.Add(nil,lsModuloAnt);
          lsRelatorioAnt  := VAZIO;
        end;

        lsRelatorio := dmDados.ValorColuna(liConexRel, 'Cod_SubRotina', liIndColCod_SubRotina);
        {'Descri��o da Rotina}
        lsString_Aux := dmDados.ValorColuna(liConexRel, 'Descricao_SubRotina', liIndColDescricao_SubRotina);
        {'concatena c�digo e descri��o}
        lsout_Desc := Trim(lsString_Aux)+ ' (' + Trim(lsRelatorio) + ')';

        if lsRelatorio <> lsRelatorioAnt then
        begin
          lsRelatorioAnt := lsRelatorio;
          trnNodeRel := trvRel.Items.AddChild(trnNodeModulo,lsout_Desc);
        end;

        lsCod_Ordem := dmDados.ValorColuna(liConexRel, 'Cod_Ordem', liIndColCod_Ordem);
        lsString_Aux := dmDados.ValorColuna(liConexRel, 'Descricao_Ordem', liIndColDescricao_Ordem);
          {'Concatena c�digo com descri��o}
        lsout_Desc := Trim(lsString_Aux) + ' (' + Trim(lsCod_Ordem) + ')';
        trvRel.Items.AddChild(trnNodeRel, lsout_Desc);

        { preenche vetor com os relatorios}
        mrRelatorio[liConta].bValor := True;
        mrRelatorio[liConta].sCod_Ordem:= lsCod_Ordem;
        mrRelatorio[liConta].sDescricao:= lsString_Aux;
        mrRelatorio[liConta].sRel_Path:= dmDados.ValorColuna(liConexRel, 'Rel_Path', liIndColRel_Path);
        mrRelatorio[liConta].sRel_Filtro:= dmDados.ValorColuna(liConexRel, 'Rel_Filtro', liIndColRel_Filtro);
        mrRelatorio[liConta].sRel_Ordem:= dmDados.ValorColuna(liConexRel, 'Rel_Ordem', liIndColRel_Ordem);
        mrRelatorio[liConta].sRel_Stored:= dmDados.ValorColuna(liConexRel, 'Rel_Stored', liIndColRel_Stored);
        mrRelatorio[liConta].sPerguntas:= dmDados.ValorColuna(liConexRel, 'Perguntas', liIndColPerguntas);
        liConta := liConta + 1;

      end;
      if (dmDados.Proximo(liConexRel) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
    end;

    { fecha a conexao}
    dmDados.FecharConexao(liConexRel);

    {'Retorna o ponteiro padr�o.}
    Screen.Cursor := crDEFAULT;
  except
    dmDados.ErroTratar('FormShow - uniImprimir.Pas');
  End;

end;

{*-----------------------------------------------------------------------------
 *  TformImprimir.btnFecharClick       Fecha a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.btnFecharClick(Sender: TObject);
begin
  formImprimir.Close;
end;

{*-----------------------------------------------------------------------------
 *  TformImprimir.trvRelClick       Muda a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.trvRelClick(Sender: TObject);
begin

    {' Se n�o tem item para ser selecionado}
    if trvRel.Selected = nil then
        Exit;

    If not trvRel.Selected.HasChildren Then
    begin
        btnImprimir.Enabled := True;
        btnVisualizar.Enabled := True;
    end
    Else
    begin
        btnImprimir.Enabled := False;
        btnVisualizar.Enabled := False;
    End;

end;

{*-----------------------------------------------------------------------------
 *  TformImprimir.trvRelKeyUp       Atualiza a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.trvRelKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    Case Key of
      KEY_UP:
        trvRelClick(Sender);
      KEY_DOWN:
        trvRelClick(Sender);
    End;

end;

{*-----------------------------------------------------------------------------
 *  TformImprimir.trvRelKeyUp       Atualiza a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.btnImprimirClick(Sender: TObject);
var
  liConta: Integer;
  liTable: Integer;
begin

  try
  if lf_PosicionaRel(liConta) then
  begin
    rptRelatorio.ReportTitle := mrRelatorio[liConta].sDescricao;
    rptRelatorio.ReportTitle := mrRelatorio[liConta].sDescricao;
    rptRelatorio.WindowButtonBar.PrintSetupBtn := True;
    rptRelatorio.ReportName := ExtractFileDir(application.ExeName) + mrRelatorio[liConta].sRel_Path;
//    rptRelatorio.Destination := crptToPrinter;
//    rptRelatorio.Connect := gsDbConnect;    //DB_CONNECT;
(*    rptRelatorio.Connect.ServerName := gsDbConnect;
    rptRelatorio.Connect.UserID := gsDbUserName;
    rptRelatorio.Connect.Password := gsDbPassWord; *)

    for liTable := 0 to rptRelatorio.Tables.Count - 1 do
      rptRelatorio.Tables[liTable].ConnectBuffer := gsDbConnect;
//    rptRelatorio.Connect.DatabaseName := gsDbNome;
    { a principio nao cancelou}
    dlgPergunta.gbCancel := False;
    lp_Formulas_Processar;
    { nao se cancelou}
    if not dlgPergunta.gbCancel then
    begin
      { processa a stored procedure }
      lp_Stored_Processar(liConta);
//      rptRelatorio.Action := 1;
      rptRelatorio.Print;
    end;
  end;
  except
   if rptRelatorio.LastErrorNumber <> 20000 then
    dmDados.ErroTratar('btnImprimirClick - uniImprimir.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformImprimir.trvRelKeyUp       Atualiza a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.btnVisualizarClick(Sender: TObject);
var
  liConta: Integer;
  liTable: Integer;
begin

  try
  if lf_PosicionaRel(liConta) then
  begin
    rptRelatorio.ReportTitle := mrRelatorio[liConta].sDescricao;
    rptRelatorio.ReportTitle := mrRelatorio[liConta].sDescricao;
    rptRelatorio.WindowButtonBar.PrintSetupBtn := True;
    rptRelatorio.ReportName := ExtractFileDir(application.ExeName) + mrRelatorio[liConta].sRel_Path;
//    rptRelatorio.Destination := crptToWindow;
//    rptRelatorio.Connect := gsDbConnect;    //DB_CONNECT;
    for liTable := 0 to rptRelatorio.Tables.Count - 1 do
      rptRelatorio.Tables[liTable].ConnectBuffer := gsDbConnect;

 (*   rptRelatorio.Connect.ServerName := gsDbConnect;
    rptRelatorio.Connect.UserID := gsDbUserName;
    rptRelatorio.Connect.Password := gsDbPassWord; *)
    { a principio nao cancelou}
    dlgPergunta.gbCancel := False;
    lp_Formulas_Processar;
    { nao se cancelou}
    if not dlgPergunta.gbCancel then
    begin
      { processa a stored procedure }
      lp_Stored_Processar(liConta);
//      rptRelatorio.Action := 1;
      rptRelatorio.Show;
    end;
  end;
  except
   if rptRelatorio.LastErrorNumber <> 20000 then
    dmDados.ErroTratar('btnVisualizarClick - uniImprimir.Pas');
  end;

end;



(*{*-----------------------------------------------------------------------------
 *  TformImprimir.trvRelKeyUp       Atualiza a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.btnImprimirClick(Sender: TObject);
var
  liConta: Integer;
  liTable: Integer;
begin

  try
  if lf_PosicionaRel(liConta) then
  begin
    rptRelatorio.ReportTitle := mrRelatorio[liConta].sDescricao;
    rptRelatorio.ReportTitle := mrRelatorio[liConta].sDescricao;
    rptRelatorio.WindowButtonBar.PrintSetupBtn := True;
    rptRelatorio.ReportName := ExtractFileDir(application.ExeName) + mrRelatorio[liConta].sRel_Path;
//    rptRelatorio.Destination := crptToPrinter;
//    rptRelatorio.Connect := gsDbConnect;    //DB_CONNECT;
    for liTable := 0 to rptRelatorio.Tables.Count - 1 do
      rptRelatorio.Tables[liTable].ConnectBuffer := gsDbConnect;
    { a principio nao cancelou}
    dlgPergunta.gbCancel := False;
    lp_Formulas_Processar;
    { nao se cancelou}
    if not dlgPergunta.gbCancel then
    begin
      { processa a stored procedure }
      lp_Stored_Processar(liConta);
//      rptRelatorio.Action := 1;
      rptRelatorio.Print;
    end;
  end;
  except
   if rptRelatorio.LastErrorNumber <> 20000 then
    dmDados.ErroTratar('btnImprimirClick - uniImprimir.Pas');
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformImprimir.trvRelKeyUp       Atualiza a tela
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.btnVisualizarClick(Sender: TObject);
var
  liConta: Integer;
  liTable: Integer;
begin

  try
  if lf_PosicionaRel(liConta) then
  begin
    rptRelatorio.ReportTitle := mrRelatorio[liConta].sDescricao;
    rptRelatorio.ReportTitle := mrRelatorio[liConta].sDescricao;
    rptRelatorio.WindowButtonBar.PrintSetupBtn := True;
    rptRelatorio.ReportName := ExtractFileDir(application.ExeName) + mrRelatorio[liConta].sRel_Path;
//    rptRelatorio.Destination := crptToWindow;
//    rptRelatorio.Connect := gsDbConnect;    //DB_CONNECT;
    for liTable := 0 to rptRelatorio.Tables.Count - 1 do
      rptRelatorio.Tables[liTable].ConnectBuffer := gsDbConnect;
    { a principio nao cancelou}
    dlgPergunta.gbCancel := False;
    lp_Formulas_Processar;
    { nao se cancelou}
    if not dlgPergunta.gbCancel then
    begin
      { processa a stored procedure }
      lp_Stored_Processar(liConta);
//      rptRelatorio.Action := 1;
      rptRelatorio.Show;
    end;
  end;
  except
   if rptRelatorio.LastErrorNumber <> 20000 then
    dmDados.ErroTratar('btnVisualizarClick - uniImprimir.Pas');
  end;

end;  *)

{*-----------------------------------------------------------------------------
 *  TformImprimir.lf_PosicionaRel       Posiciona o vetor no relatorio escolhido
 *
 *-----------------------------------------------------------------------------}
function TformImprimir.lf_PosicionaRel(var piConta: Integer): Boolean;
var
  lsCampo: String;
begin

  { nao achou}
  Result := False;

  lsCampo := lp_NodePath(trvRel.Selected);
  msCod_Ordem := Copy(lsCampo, (Length(lsCampo) - 8), 8);

  piConta := 0;

  While mrRelatorio[piConta].bValor do
  begin
    if mrRelatorio[piConta].sCod_Ordem = msCod_Ordem then
    begin
      If (Trim(mrRelatorio[piConta].sPerguntas) = 'S') Then
      begin
        gsImpTituloRelatorio := mrRelatorio[piConta].sDescricao;
//        lp_Formulas_Processar;
      end;
      Result := True;
      Exit;
    end
    else
      piConta := piConta + 1;
  end;

end;

{*-----------------------------------------------------------------------------
 *  TformImprimir.lp_NodePath      Para pegar o nome completo
 *                             Ex: "Item\SubItem\SubSubItem"
 *-----------------------------------------------------------------------------}
function TformImprimir.lp_NodePath(Node: TTreeNode): String;
begin
  if Node.Parent = nil then
    Result := Node.Text
  else
    Result := lp_NodePath(Node.Parent) + '\' + Node.Text;
end;

{*-----------------------------------------------------------------------------
 *  TformImprimir.lp_Formulas_Processar      processa as perguntas
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.lp_Formulas_Processar;
var
  lvSelect: Variant;
  liConexFormulas: Integer;
  lsCodFormula   : String;
  lsFormula      : String;
  lsFiltro       : String;

  liColCodFormula : Integer;
  liColFormula    : Integer;
  liPtrSequencia  : Integer;
  liPtrPergunta   : Integer;
  liPtrTpResposta : Integer;
  liPtrTpControle : Integer;
  liPtrSQL        : Integer;

  liConexPerguntas : Integer;
  liSequencia : Integer;
  lrPerguntas: array[0..MAX_NUM_PERG] of TUmaImpPergunta;

  liFim2     : LongInt;
  liInicio2  : LongInt;
  liFim      : LongInt;
  liInicio   : LongInt;
  liAscChar  : Integer;
  liParmInd  : Integer;
  liParmInd2 : Integer;
  lsFormula2 : String;
  lsFiltro2  : String;
  liConta : Integer;
  laParm: array[0..9] of Variant;
begin

  dmDados.giStatus := STATUS_OK;

  gaParm[0] := msCod_Ordem;
  lvSelect := dmDados.SqlVersao('IMP_0012', gaParm);
  liConexFormulas := dmDados.ExecutarSelect(lvSelect);

  liColCodFormula := IOPTR_NOPOINTER;
  liColFormula    := IOPTR_NOPOINTER;

  { limpa a formula do relatorio}
//  for liConta := 0 to 100 do
//    rptRelatorio.Formulas[liConta] := VAZIO;
   rptRelatorio.Formulas.Clear;
  { limpa o vetor de formulas}
  for liConta := 0 to MAX_NUM_FORMULAS do
  begin
    mrFormulas[liConta].sNome := VAZIO;
    mrFormulas[liConta].sTexto:= VAZIO;
  end;

  { substitui o filtro }
//  rptRelatorio.SelectionFormula := VAZIO;
  for liConta := 0 to High(mrRelatorio) do
    if mrRelatorio[liConta].sCod_Ordem = msCod_Ordem then
      break;
  lsFiltro := mrRelatorio[liConta].sRel_Filtro;
//  lsFiltro2 := VAZIO;
  liInicio2 := 1;
//  liFim2 := AnsiPos('@', Copy(lsFiltro,liInicio2,Length(lsFiltro)));
//  if liFim2 <> 0 then
//    liParmInd2 := StrToInt(Copy(lsFiltro, liFim2 + 1, 1));

  liConta := 0;

  if dmDados.Status(liConexFormulas, IOSTATUS_EOF) then
 //   rptRelatorio.Formulas[0] := VAZIO;
    rptRelatorio.Formulas.Clear;

  {'Enquanto n�o fim de f�rmulas}
  while not dmDados.Status(liConexFormulas, IOSTATUS_EOF) do
  begin
    lsCodFormula := dmDados.ValorColuna(liConexFormulas, 'Cod_Formula', liColCodFormula);
    lsFormula := dmDados.ValorColuna(liConexFormulas, 'Formula', liColFormula);
    lsFormula2 := VAZIO;
    liInicio := 1;
    liFim := AnsiPos('@', Copy(lsFormula,liInicio,Length(lsFormula)));

    {'Se tem par�metros}
    If liFim <> 0 Then
    begin
      {'====>>Carrega array com perguntas}
      liPtrSequencia  := IOPTR_NOPOINTER;
      liPtrPergunta   := IOPTR_NOPOINTER;
      liPtrTpResposta := IOPTR_NOPOINTER;
      liPtrTpControle := IOPTR_NOPOINTER;
      liPtrSQL        := IOPTR_NOPOINTER;

      gaParm[0] := msCod_Ordem;
      gaParm[1] := lsCodFormula;
      lvSelect := dmDados.SqlVersao('IMP_0013', gaParm);
      liConexPerguntas := dmDados.ExecutarSelect(lvSelect);
      While not dmDados.Status(liConexPerguntas, IOSTATUS_EOF) do
      begin
        liSequencia := dmDados.ValorColuna(liConexPerguntas, 'Sequencia', liPtrSequencia);
        If High(lrPerguntas) < liSequencia Then
        begin
          ShowMessage('Chegou no m�ximo do vetor');
          Break;
        End;
        lrPerguntas[liSequencia].sPergunta := dmDados.ValorColuna(liConexPerguntas, 'Pergunta', liPtrPergunta);
        lrPerguntas[liSequencia].sTpResposta := dmDados.ValorColuna(liConexPerguntas, 'Tipo_Resposta', liPtrTpResposta);
        lrPerguntas[liSequencia].sTpControle := dmDados.ValorColuna(liConexPerguntas, 'Tipo_Controle', liPtrTpControle);
        lrPerguntas[liSequencia].sSql := dmDados.ValorColuna(liConexPerguntas, 'SQL_Combo', liPtrSQL);
        if (dmDados.Proximo(liConexPerguntas) = IORET_EOF) or
           (dmDados.giStatus <> STATUS_OK) then
            break;
      end;
      dmDados.FecharConexao(liConexPerguntas);
    end;

    {'===>> Substitui par�metros da f�rmula}
    While liFim <> 0 do
    begin
      lsFormula2 := lsFormula2 + Copy(lsFormula, liInicio, liFim - liInicio);
      If liFim + 2 <= Length(lsFormula) Then
      begin
        liAscChar := Ord(Copy(lsFormula, liFim + 2, 1)[1]);
        If (liAscChar >= Ord('0')) And (liAscChar <= Ord('9')) Then
        begin
          liParmInd := StrToInt(Copy(lsFormula, liFim + 1, 2));
          liInicio := liFim + 3;
        end
        Else
        begin
          liParmInd := StrToInt(Copy(lsFormula, liFim + 1, 1));
          liInicio := liFim + 2;
        End;
      end
      Else
      begin
        liParmInd := StrToInt(Copy(lsFormula, liFim + 1, 2));
        liInicio := liFim + 3;
      End;
      grImpPergunta := lrPerguntas[liParmInd];
      dlgPergunta.ShowModal;

      lsFormula2 := lsFormula2 + VarToStr(gvImpResposta);
      liFim := AnsiPos('@', Copy(lsFormula,liInicio,Length(lsFormula)));
      { se cancelou}
      if dlgPergunta.gbCancel then
        break;

      { substitui o filtro }
      laParm[liParmInd] := VarToStr(gvImpResposta);
//      if (liFim2 <> 0) and (liParmInd2 = liParmInd) then
//      begin
//        lsFiltro2 := lsFiltro2 + Copy(lsFiltro, liInicio2, liFim2 - liInicio2);
//        lsFiltro2 := lsFiltro2 + VarToStr(gvImpResposta);
//        liInicio2 := liFim2 + 2;
//        liFim2 := AnsiPos('@', Copy(lsFiltro,liInicio2,Length(lsFiltro)));
//        if liFim2 <> 0 then
//        begin
//          liFim2 := liFim2 + liInicio2 - 1;
//          liParmInd2 := StrToInt(Copy(lsFiltro, liFim2 + 1, 1));
//        end;
//      end;
    end;
    If liInicio <= Length(lsFormula) Then
      lsFormula2 := lsFormula2 + Copy(lsFormula, liInicio, Length(lsFormula) - liInicio + 1);

        { acrescenta na formula do relatorio}
//    rptRelatorio.Formulas[liConta].Formula.Text := lsFormula2;
    rptRelatorio.Formulas.ByName(Copy(lsFormula2,1,AnsiPos('=',lsFormula2)-1)).Formula.Text := Copy(lsFormula2,AnsiPos('=',lsFormula2)+1,Length(lsFormula2));
    mrFormulas[liConta].sNome := Copy(lsFormula2,1,AnsiPos('=',lsFormula2)-1);
    mrFormulas[liConta].sTexto := Copy(lsFormula2,AnsiPos('=',lsFormula2)+1,Length(lsFormula2));
    liConta := liConta + 1;

    { se cancelou}
    if dlgPergunta.gbCancel then
      break;

    {'Posiciona na Pr�xima f�rmula}
    if (dmDados.Proximo(liConexFormulas) = IORET_EOF) or
       (dmDados.giStatus <> STATUS_OK) then
        break;
  End;

  { limpa a formula do relatorio}
////  rptRelatorio.Formulas[liConta] := VAZIO;
//    rptRelatorio.Formulas.Clear;

  { acrescenta no filtro do relatorio}
//  if liInicio2 <= Length(lsFiltro) Then
//    lsFiltro2 := lsFiltro2 + Copy(lsFiltro, liInicio2, Length(lsFiltro) - liInicio2 + 1);
  if lsFiltro <> VAZIO then
    lsFiltro2 := dmDados.LiteralSubstituir(lsFiltro, laParm);
  if lsFiltro2 <> VAZIO then
    //rptRelatorio.SelectionFormula := lsFiltro2;
    rptRelatorio.Selection.Formula.Text := lsFiltro2;

  dmDados.FecharConexao(liConexFormulas);

End;

{*-----------------------------------------------------------------------------
 *  TformImprimir.lp_Stored_Processar      processa a stored procedure
 *
 *-----------------------------------------------------------------------------}
procedure TformImprimir.lp_Stored_Processar(piConta: Integer);
var
  i: Integer;
  lsFormula: string;
  lwAno, lwMes, lwDia: Word;
  lsTemp: string;
  laParams: array[0..20] of TSParams;
begin

  Screen.Cursor := crHourGlass;

  if mrRelatorio[piConta].sRel_Stored <> VAZIO then
  begin
    i := 0;
    //while rptRelatorio.Formulas[i] <> VAZIO do
//    while rptRelatorio.Formulas[i].Formula.Text <> VAZIO do
    while mrFormulas[i].sTexto <> VAZIO do
    begin
//      lsFormula := rptRelatorio.Formulas[i].Formula.Text;
      lsFormula := mrFormulas[i].sTexto;
//      laParams[i].sNome := '@' + Copy(lsFormula,1,AnsiPos('=',lsFormula)-1);
//      laParams[i].sNome := '@' + rptRelatorio.Formulas[i].Name;
        laParams[i].sNome := '@' + mrFormulas[i].sNome;
//      laParams[i].ptTipo := ptInput;
      laParams[i].ptTipo := ptInput;
      if Pos('Date(', lsFormula) <> 0  then
      begin
        laParams[i].ftTipoDado := ftDateTime;
        lsFormula := Copy(lsFormula, Pos('Date(', lsFormula) + 5, Length(lsFormula) - Pos('Date(', lsFormula) - 5);
        lwAno := StrToInt(Copy(lsFormula, 1, Pos(',', lsFormula) - 1));
        lsTemp := Copy(lsFormula, Pos(',', lsFormula) + 1, Length(lsFormula));
        lwMes := StrToInt(Copy(lsTemp, 1, Pos(',', lsTemp) - 1));
        lwDia := StrToInt(Copy(lsTemp, Pos(',', lsTemp) + 1, Length(lsTemp)));
        laParams[i].vValor := EncodeDate(lwAno, lwMes, lwDia);
      end
      else
      begin
        if Pos('"',lsFormula) <> 0 then
        begin
          laParams[i].ftTipoDado := ftString;
          laParams[i].vValor := Copy(lsFormula,Pos('"',lsFormula)+1,Length(lsFormula)-Pos('"',lsFormula)-1);
        end
        else
        begin
          laParams[i].ftTipoDado := ftInteger;
//          laParams[i].vValor := StrToInt(Trim(Copy(lsFormula,Pos('=',lsFormula)+1,Length(lsFormula))));
          laParams[i].vValor := StrToInt(Trim(lsFormula));
        end;
      end;
      i := i + 1;
    end;
    dmDados.gsRetorno := dmDados.CriarStored(Copy(mrRelatorio[piConta].sRel_Stored, 1, Pos('(', mrRelatorio[piConta].sRel_Stored) - 1), laParams, i-1);
  end;

  Screen.Cursor := crDefault;
end;

end.
